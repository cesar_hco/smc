package negocio.facade.jdbc.financeiro;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.comuns.financeiro.ChequeVO;
import negocio.comuns.financeiro.FormaPagamentoPagamentoVO;
import negocio.comuns.financeiro.PagamentoVO;
import negocio.comuns.financeiro.enumeradores.LocalizacaoChequeEnum;
import negocio.comuns.financeiro.enumeradores.TipoFormaPagamentoEnum;
import negocio.facade.jdbc.arquitetura.ControleAcesso;
import negocio.interfaces.financeiro.FormaPagamentoPagamentoInterfaceFacade;

@Repository
@Scope("singleton")
@Lazy
public class FormaPagamentoPagamento extends ControleAcesso implements FormaPagamentoPagamentoInterfaceFacade {

	private static final long serialVersionUID = 1L;

	public FormaPagamentoPagamento() {
		super();
	}

	@Override
	public void validarDados(FormaPagamentoPagamentoVO obj, UsuarioVO usuarioVO) throws Exception {
		if (obj.getFormaPagamentoVO() == null || obj.getFormaPagamentoVO().getCodigo() == null || obj.getFormaPagamentoVO().getCodigo().equals(0)) {
			throw new Exception("O campo FORMA DE PAGAMENTO deve ser informado!");
		}
		if (obj.getValor() == null || obj.getValor() == BigDecimal.ZERO) {
			throw new Exception("O campo VALOR deve ser informado!");
		}

		if (!obj.getFormaPagamentoVO().getIsTipoDinheiro() && (obj.getContaCorrenteVO() == null || obj.getContaCorrenteVO().getCodigo() == null || obj.getContaCorrenteVO().getCodigo().equals(0))) {
			throw new Exception("O campo CONTA CORRENTE deve ser informado!");
		}
		
		if (obj.getFormaPagamentoVO().getIsTipoCheque() && obj.getChequeVO().getCodigo().equals(0)) {
			throw new Exception("O campo CHEQUE deve ser informado!");
		}

		if (obj.getFormaPagamentoVO().getIsTipoDinheiro()) {
			obj.setContaCorrenteVO(obj.getPagamentoVO().getContaCaixaVO());
		}

		if (!obj.getFormaPagamentoVO().getIsTipoCheque()) {
			obj.setChequeVO(null);
		}
		

	}

	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void persistir(FormaPagamentoPagamentoVO obj, UsuarioVO usuarioVO) throws Exception {
		if (obj.isNovoObj()) {
			incluir(obj, usuarioVO);
		} else {
			alterar(obj, usuarioVO);
		}
	}

	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void incluir(final FormaPagamentoPagamentoVO obj, UsuarioVO usuario) throws Exception {
		try {
			validarDados(obj, usuario);
			final String sql = "INSERT INTO FormaPagamentoPagamento( formaPagamento, pagamento, contaCorrente, valor, cheque ) " + " VALUES ( ?, ?, ?, ?, ?) returning codigo";
			obj.setCodigo((Integer) getConexao().getJdbcTemplate().query(new PreparedStatementCreator() {

				public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
					PreparedStatement sqlInserir = arg0.prepareStatement(sql);
					if (!obj.getFormaPagamentoVO().getCodigo().equals(0)) {
						sqlInserir.setInt(1, obj.getFormaPagamentoVO().getCodigo());
					} else {
						sqlInserir.setNull(1, 0);
					}
					if (!obj.getPagamentoVO().getCodigo().equals(0)) {
						sqlInserir.setInt(2, obj.getPagamentoVO().getCodigo());
					} else {
						sqlInserir.setNull(2, 0);
					}
					if (!obj.getContaCorrenteVO().getCodigo().equals(0)) {
						sqlInserir.setInt(3, obj.getContaCorrenteVO().getCodigo());
					} else {
						sqlInserir.setNull(3, 0);
					}
					sqlInserir.setBigDecimal(4, obj.getValor());
					if (!obj.getChequeVO().getCodigo().equals(0)) {
						sqlInserir.setInt(5, obj.getChequeVO().getCodigo());
					} else {
						sqlInserir.setNull(5, 0);
					}
					return sqlInserir;
				}
			}, new ResultSetExtractor() {

				public Object extractData(ResultSet arg0) throws SQLException, DataAccessException {
					if (arg0.next()) {
						obj.setNovoObj(Boolean.FALSE);
						return arg0.getInt("codigo");
					}
					return null;
				}
			}));

			obj.setNovoObj(Boolean.FALSE);
		} catch (Exception e) {
			obj.setNovoObj(Boolean.TRUE);
			throw e;
		}
	}

	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void alterar(final FormaPagamentoPagamentoVO obj, UsuarioVO usuario) throws Exception {
		try {
			validarDados(obj, usuario);
			final String sql = "UPDATE FormaPagamentoPagamento set formaPagamento=?, pagamento=?, contaCorrente=?, valor=?, cheque=? " + "WHERE ((codigo = ?))";

			getConexao().getJdbcTemplate().update(new PreparedStatementCreator() {

				public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
					PreparedStatement sqlAlterar = arg0.prepareStatement(sql);
					if (!obj.getFormaPagamentoVO().getCodigo().equals(0)) {
						sqlAlterar.setInt(1, obj.getFormaPagamentoVO().getCodigo());
					} else {
						sqlAlterar.setNull(1, 0);
					}
					if (!obj.getPagamentoVO().getCodigo().equals(0)) {
						sqlAlterar.setInt(2, obj.getPagamentoVO().getCodigo());
					} else {
						sqlAlterar.setNull(2, 0);
					}
					if (!obj.getContaCorrenteVO().getCodigo().equals(0)) {
						sqlAlterar.setInt(3, obj.getContaCorrenteVO().getCodigo());
					} else {
						sqlAlterar.setNull(3, 0);
					}
					sqlAlterar.setBigDecimal(4, obj.getValor());
					if (!obj.getChequeVO().getCodigo().equals(0)) {
						sqlAlterar.setInt(5, obj.getChequeVO().getCodigo());
					} else {
						sqlAlterar.setNull(5, 0);
					}
					sqlAlterar.setInt(6, obj.getCodigo().intValue());
					return sqlAlterar;
				}
			});

		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void excluir(FormaPagamentoPagamentoVO obj, UsuarioVO usuario) throws Exception {
		try {
			String sql = "DELETE FROM FormaPagamentoPagamento WHERE ((codigo = ?))";
			getConexao().getJdbcTemplate().update(sql, new Object[] { obj.getCodigo() });
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public List<FormaPagamentoPagamentoVO> consultar(String campoConsulta, String valorConsulta, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		List<FormaPagamentoPagamentoVO> listaConsulta = new ArrayList<FormaPagamentoPagamentoVO>(0);
		if (campoConsulta.equals("NUMERO_CONTA_CORRENTE")) {
			listaConsulta = consultarPorNumeroFormaPagamentoPagamento(valorConsulta, nivelMontarDados, usuarioVO);
		}
		if (campoConsulta.equals("DESCRICAO")) {
			listaConsulta = consultarPorDescricao(valorConsulta, nivelMontarDados, usuarioVO);
		}
		return listaConsulta;
	}

	public List<FormaPagamentoPagamentoVO> consultarPorDescricao(String nome, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		StringBuilder sb = new StringBuilder();
		sb.append(getObterConsultaNivelMontarDados(nivelMontarDados));
		sb.append(" where formaPagamentoPagamento.descricao ilike ('").append(nome).append("%') ");
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
		return montarDadosLista(tabelaResultado, nivelMontarDados, usuarioVO);
	}

	public List<FormaPagamentoPagamentoVO> consultarPorNumeroFormaPagamentoPagamento(String nome, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		StringBuilder sb = new StringBuilder();
		sb.append(getObterConsultaNivelMontarDados(nivelMontarDados));
		sb.append(" where formaPagamentoPagamento.numero ilike ('").append(nome).append("%') ");
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
		return montarDadosLista(tabelaResultado, nivelMontarDados, usuarioVO);
	}
	
	@Override
	public List<FormaPagamentoPagamentoVO> consultarPorPagamento(Integer pagamento, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		StringBuilder sb = new StringBuilder();
		sb.append(getObterConsultaNivelMontarDados(nivelMontarDados));
		sb.append(" where formaPagamentoPagamento.pagamento = ").append(pagamento);
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
		return montarDadosLista(tabelaResultado, nivelMontarDados, usuarioVO);
	}

	public StringBuilder getObterConsultaNivelMontarDados(NivelMontarDadosEnum nivelMontarDados) {
		if (nivelMontarDados.equals(NivelMontarDadosEnum.BASICO)) {
			return getDadosBasicos();
		}
		return getDadosCompletos();
	}

	@Override
	public void carregarDados(FormaPagamentoPagamentoVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		if (nivelMontarDados.equals(NivelMontarDadosEnum.BASICO)) {
			StringBuilder sb = getDadosBasicos();
			sb.append(" where formaPagamentoPagamento.codigo = ").append(obj.getCodigo());
			SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
			if (tabelaResultado.next()) {
				montarDadosBasicos(tabelaResultado, obj, nivelMontarDados, usuarioVO);
			}
		}
		if (nivelMontarDados.equals(NivelMontarDadosEnum.COMPLETO)) {
			StringBuilder sb = getDadosCompletos();
			sb.append(" where formaPagamentoPagamento.codigo = ").append(obj.getCodigo());
			SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
			if (tabelaResultado.next()) {
				montarDadosCompletos(tabelaResultado, obj, nivelMontarDados, usuarioVO);
			}
		}
	}

	public List<FormaPagamentoPagamentoVO> montarDadosLista(SqlRowSet dadosSQL, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		List<FormaPagamentoPagamentoVO> listaMembroVOs = new ArrayList<FormaPagamentoPagamentoVO>(0);
		while (dadosSQL.next()) {
			FormaPagamentoPagamentoVO obj = new FormaPagamentoPagamentoVO();
			if (nivelMontarDados.equals(NivelMontarDadosEnum.BASICO)) {
				montarDadosBasicos(dadosSQL, obj, nivelMontarDados, usuarioVO);
			} else {
				montarDadosCompletos(dadosSQL, obj, nivelMontarDados, usuarioVO);
			}
			listaMembroVOs.add(obj);
		}
		return listaMembroVOs;
	}

	public StringBuilder getDadosBasicos() {
		StringBuilder sb = new StringBuilder();
		sb.append("select formaPagamentoPagamento.*,  ");
		sb.append(" formaPagamento.codigo AS \"formaPagamento.codigo\", formaPagamento.nome AS \"formaPagamento.nome\" ");
		sb.append(" from formaPagamentoPagamento "); 
		sb.append(" inner join formaPagamento on formaPagamento.codigo = formaPagamentoPagamento.formaPagamento ");
		return sb;
	}

	public StringBuilder getDadosCompletos() {
		StringBuilder sb = new StringBuilder();
		sb.append("select formaPagamentoPagamento.*,  ");
		sb.append(" formaPagamento.codigo AS \"formaPagamento.codigo\", formaPagamento.nome AS \"formaPagamento.nome\", formaPagamento.tipoFormaPagamento AS \"formaPagamento.tipoFormaPagamento\", ");
		
		sb.append(" contaCorrente.codigo AS \"contaCorrente.codigo\", contaCorrente.digitoContaCorrente AS \"contaCorrente.digitoContaCorrente\", contaCorrente.numero AS \"contaCorrente.numero\", ");
		sb.append(" contaCorrente.carteira AS \"contaCorrente.carteira\", contaCorrente.cedente AS \"contaCorrente.cedente\", ");
		sb.append(" contaCorrente.convenio AS \"contaCorrente.convenio\", contaCorrente.banco AS \"contaCorrente.banco\", contaCorrente.nrBanco AS \"contaCorrente.nrBanco\", ");
		sb.append(" contaCorrente.agencia AS \"contaCorrente.agencia\", contaCorrente.digitoAgencia AS \"contaCorrente.digitoAgencia\", ");
		
		sb.append(" cheque.codigo AS \"cheque.codigo\", cheque.codigo AS \"cheque.numero\", cheque.valor AS \"cheque.valor\", cheque.chequeProprio AS \"cheque.chequeProprio\", ");
		sb.append(" cheque.dataVencimento AS \"cheque.dataVencimento\", cheque.empresa AS \"cheque.empresa\", cheque.localizacao AS \"cheque.localizacao\", ");
		sb.append(" cheque.localizacaoCheque AS \"cheque.localizacaoCheque\" ");
		sb.append(" from formaPagamentoPagamento "); 
		sb.append(" inner join formaPagamento on formaPagamento.codigo = formaPagamentoPagamento.formaPagamento ");
		sb.append(" left join contaCorrente on contaCorrente.codigo = formaPagamentoPagamento.contaCorrente ");
		sb.append(" left join cheque on cheque.codigo = formaPagamentoPagamento.cheque ");
		return sb;
	}

	public void montarDadosBasicos(SqlRowSet dadosSQL, FormaPagamentoPagamentoVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		obj.setCodigo(dadosSQL.getInt("codigo"));
		obj.getFormaPagamentoVO().setCodigo(dadosSQL.getInt("formaPagamento.codigo"));
		obj.getFormaPagamentoVO().setNome(dadosSQL.getString("formaPagamento.nome"));
		obj.getPagamentoVO().setCodigo(dadosSQL.getInt("pagamento"));
		obj.getContaCorrenteVO().setCodigo(dadosSQL.getInt("contaCorrente"));
		obj.setValor(dadosSQL.getBigDecimal("valor"));
		obj.getChequeVO().setCodigo(dadosSQL.getInt("cheque"));
		obj.setNovoObj(false);
	}

	public void montarDadosCompletos(SqlRowSet dadosSQL, FormaPagamentoPagamentoVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		obj.setCodigo(dadosSQL.getInt("codigo"));
		obj.getFormaPagamentoVO().setCodigo(dadosSQL.getInt("formaPagamento.codigo"));
		obj.getFormaPagamentoVO().setNome(dadosSQL.getString("formaPagamento.nome"));
		if (dadosSQL.getString("formaPagamento.tipoFormaPagamento") != null) {
			obj.getFormaPagamentoVO().setTipoFormaPagamento(TipoFormaPagamentoEnum.valueOf(dadosSQL.getString("formaPagamento.tipoFormaPagamento")));
		}
		obj.getPagamentoVO().setCodigo(dadosSQL.getInt("pagamento"));
		
		obj.getContaCorrenteVO().setCodigo(dadosSQL.getInt("contaCorrente.codigo"));
		obj.getContaCorrenteVO().setDigitoContaCorrente(dadosSQL.getString("contaCorrente.digitoContaCorrente"));
		obj.getContaCorrenteVO().setNumero(dadosSQL.getString("contaCorrente.numero"));
		obj.getContaCorrenteVO().setCarteira(dadosSQL.getString("contaCorrente.carteira"));
		obj.getContaCorrenteVO().setCedente(dadosSQL.getString("contaCorrente.cedente"));
		obj.getContaCorrenteVO().setConvenio(dadosSQL.getString("contaCorrente.convenio"));
		obj.getContaCorrenteVO().setBanco(dadosSQL.getString("contaCorrente.banco"));
		obj.getContaCorrenteVO().setNrBanco(dadosSQL.getString("contaCorrente.nrBanco"));
		obj.getContaCorrenteVO().setAgencia(dadosSQL.getString("contaCorrente.agencia"));
		obj.getContaCorrenteVO().setDigitoAgencia(dadosSQL.getString("contaCorrente.digitoAgencia"));
		
		obj.getChequeVO().setCodigo(dadosSQL.getInt("cheque.codigo"));
		obj.getChequeVO().setNumero(dadosSQL.getString("cheque.numero"));
		obj.getChequeVO().setValor(dadosSQL.getBigDecimal("cheque.valor"));
		obj.getChequeVO().setChequeProprio(dadosSQL.getBoolean("cheque.chequeProprio"));
		obj.getChequeVO().setDataVencimento(dadosSQL.getDate("cheque.dataVencimento"));
		obj.getChequeVO().getEmpresaVO().setCodigo(dadosSQL.getInt("cheque.empresa"));
		if (dadosSQL.getString("cheque.localizacao") != null) {
			obj.getChequeVO().setLocalizacao(LocalizacaoChequeEnum.valueOf(dadosSQL.getString("cheque.localizacao")));
		}
		obj.getChequeVO().getLocalizacaoChequeVO().setCodigo(dadosSQL.getInt("cheque.localizacaoCheque"));
		
		obj.setValor(dadosSQL.getBigDecimal("valor"));
		obj.setNovoObj(false);
		if (nivelMontarDados.equals(NivelMontarDadosEnum.TELA_CONSULTA)) {
			return;
		}
		return;
	}

	@Override
	public void incluirFormaPagamentoPagamentoVOs(PagamentoVO pagamentoVO, List<FormaPagamentoPagamentoVO> listaFormaPagamentoPagamentoVOs, UsuarioVO usuarioVO) throws Exception {
		for (FormaPagamentoPagamentoVO formaPagamentoPagamentoVO : listaFormaPagamentoPagamentoVOs) {
			formaPagamentoPagamentoVO.getPagamentoVO().setCodigo(pagamentoVO.getCodigo());
			if (formaPagamentoPagamentoVO.getCodigo().equals(0)) {
				incluir(formaPagamentoPagamentoVO, usuarioVO);
			} 
		}
	}
	
	@Override
	public void alterarFormaPagamentoPagamentoVOs(PagamentoVO pagamentoVO, List<FormaPagamentoPagamentoVO> listaFormaPagamentoPagamentoVOs, UsuarioVO usuarioVO) throws Exception {
		excluirContaPagarPagamentoVOs(pagamentoVO.getCodigo(), listaFormaPagamentoPagamentoVOs, usuarioVO);
		for (FormaPagamentoPagamentoVO formaPagamentoPagamentoVO : listaFormaPagamentoPagamentoVOs) {
			formaPagamentoPagamentoVO.getPagamentoVO().setCodigo(pagamentoVO.getCodigo());
			if (formaPagamentoPagamentoVO.getCodigo().equals(0)) {
				incluir(formaPagamentoPagamentoVO, usuarioVO);
			} else {
				alterar(formaPagamentoPagamentoVO, usuarioVO);
			}
		}
	}
	
	public void excluirContaPagarPagamentoVOs(Integer pagamento, List<FormaPagamentoPagamentoVO> listaFormaPagamentoPagamentoVOs, UsuarioVO usuarioVO) {
		StringBuilder sb = new StringBuilder();
		sb.append("delete from formaPagamentoPagamento where pagamento = ").append(pagamento); 
		sb.append(" and formaPagamento not in(");
		for (FormaPagamentoPagamentoVO formaPagamentoPagamentoVO : listaFormaPagamentoPagamentoVOs) {
			sb.append(formaPagamentoPagamentoVO.getFormaPagamentoVO().getCodigo()).append(", ");
		}
		sb.append("0) ");
		getConexao().getJdbcTemplate().update(sb.toString(), new Object[] { });
	}
	
	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void estornarPagamentoFormaPagamento(PagamentoVO pagamentoVO, UsuarioVO usuarioVO) throws Exception {
		for (FormaPagamentoPagamentoVO formaPagamentoPagamentoVO : pagamentoVO.getListaFormaPagamentoPagamentoVOs()) {
			if (formaPagamentoPagamentoVO.getFormaPagamentoVO().getIsTipoCheque() 
					&& formaPagamentoPagamentoVO.getChequeVO().getLocalizacao() != null 
					&& formaPagamentoPagamentoVO.getChequeVO().getLocalizacao().equals(LocalizacaoChequeEnum.COMPENSADO)) {
				throw new Exception("N�o � poss�vel estornar este PAGAMENTO, pois existe CHEQUE(S) j� COMPENSADO.");
			} else if (formaPagamentoPagamentoVO.getFormaPagamentoVO().getIsTipoCheque() && !formaPagamentoPagamentoVO.getChequeVO().getChequeProprio() 
					&& formaPagamentoPagamentoVO.getChequeVO().getLocalizacao() != null
					&& !formaPagamentoPagamentoVO.getChequeVO().getLocalizacao().equals(LocalizacaoChequeEnum.COMPENSADO)) {
				formaPagamentoPagamentoVO.getChequeVO().setLocalizacao(LocalizacaoChequeEnum.EM_CAIXA);
				formaPagamentoPagamentoVO.getChequeVO().setLocalizacaoChequeVO(pagamentoVO.getContaCaixaVO());
				getFacadeFactory().getChequeFacade().persistir(formaPagamentoPagamentoVO.getChequeVO(), usuarioVO);
			} else if (formaPagamentoPagamentoVO.getFormaPagamentoVO().getIsTipoCheque() 
					&& formaPagamentoPagamentoVO.getChequeVO().getChequeProprio() 
					&& formaPagamentoPagamentoVO.getChequeVO().getLocalizacao() != null
					&& !formaPagamentoPagamentoVO.getChequeVO().getLocalizacao().equals(LocalizacaoChequeEnum.COMPENSADO)) {
				formaPagamentoPagamentoVO.getChequeVO().setLocalizacao(LocalizacaoChequeEnum.ESTORNADO);
				formaPagamentoPagamentoVO.getChequeVO().setLocalizacaoChequeVO(null);
				getFacadeFactory().getChequeFacade().persistir(formaPagamentoPagamentoVO.getChequeVO(), usuarioVO);
			} else if (!formaPagamentoPagamentoVO.getFormaPagamentoVO().getIsTipoCheque()) {
				formaPagamentoPagamentoVO.setChequeVO(null);
			}
		}
	}
	
	@Override
	public void inicalzarDadosChequeFormaPagamentoPagamento(FormaPagamentoPagamentoVO formaPagamentoPagamentoVO, ChequeVO chequeVO, UsuarioVO usuarioVO) {
		formaPagamentoPagamentoVO.setChequeVO(chequeVO);
		formaPagamentoPagamentoVO.setValor(chequeVO.getValor());
		formaPagamentoPagamentoVO.setContaCorrenteVO(chequeVO.getLocalizacaoChequeVO());
	}
	
}
