package negocio.facade.jdbc.financeiro;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.comuns.financeiro.ContaPagarPagamentoVO;
import negocio.comuns.financeiro.ContaPagarVO;
import negocio.comuns.financeiro.PagamentoVO;
import negocio.comuns.financeiro.enumeradores.SituacaoContaPagarEnum;
import negocio.facade.jdbc.arquitetura.ControleAcesso;
import negocio.interfaces.financeiro.ContaPagarPagamentoInterfaceFacade;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Scope("singleton")
@Lazy
public class ContaPagarPagamento extends ControleAcesso implements ContaPagarPagamentoInterfaceFacade {

	private static final long serialVersionUID = 1L;

	public ContaPagarPagamento() {
		super();
	}

	public void validarDados(ContaPagarPagamentoVO obj, UsuarioVO usuarioVO) throws Exception {
		if (obj.getContaPagarVO().getCodigo().equals(0)) {
			throw new Exception("O campo CONTA A PAGAR deve ser informado.");
		}
		if (obj.getPagamentoVO().getCodigo().equals(0)) {
			throw new Exception("O campo PAGAMENTO deve ser informado.");
		}
	}

	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void persistir(ContaPagarPagamentoVO obj, UsuarioVO usuarioVO) throws Exception {
		if (obj.isNovoObj()) {
			incluir(obj, usuarioVO);
		} else {
			alterar(obj, usuarioVO);
		}
	}

	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void incluir(final ContaPagarPagamentoVO obj, UsuarioVO usuario) throws Exception {
		try {
			validarDados(obj, usuario);
			final String sql = "INSERT INTO ContaPagarPagamento( contaPagar, pagamento) "
					+ " VALUES ( ?, ?) returning codigo";
			obj.setCodigo((Integer) getConexao().getJdbcTemplate().query(new PreparedStatementCreator() {

				public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
					PreparedStatement sqlInserir = arg0.prepareStatement(sql);
					if (!obj.getContaPagarVO().getCodigo().equals(0)) {
						sqlInserir.setInt(1, obj.getContaPagarVO().getCodigo());
					} else {
						sqlInserir.setNull(1, 0);
					}
					if (!obj.getPagamentoVO().getCodigo().equals(0)) {
						sqlInserir.setInt(2, obj.getPagamentoVO().getCodigo());
					} else {
						sqlInserir.setNull(2, 0);
					}
					return sqlInserir;
				}
			}, new ResultSetExtractor() {

				public Object extractData(ResultSet arg0) throws SQLException, DataAccessException {
					if (arg0.next()) {
						obj.setNovoObj(Boolean.FALSE);
						return arg0.getInt("codigo");
					}
					return null;
				}
			}));
			
			obj.setNovoObj(Boolean.FALSE);
		} catch (Exception e) {
			obj.setNovoObj(Boolean.TRUE);
			throw e;
		}
	}

	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void alterar(final ContaPagarPagamentoVO obj, UsuarioVO usuario) throws Exception {
		try {
			validarDados(obj, usuario);
			final String sql = "UPDATE ContaPagarPagamento set contaPagar=?, pagamento=? "
					+ "WHERE ((codigo = ?))";

			getConexao().getJdbcTemplate().update(new PreparedStatementCreator() {

				public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
					PreparedStatement sqlAlterar = arg0.prepareStatement(sql);
					if (!obj.getContaPagarVO().getCodigo().equals(0)) {
						sqlAlterar.setInt(1, obj.getContaPagarVO().getCodigo());
					} else {
						sqlAlterar.setNull(1, 0);
					}
					if (!obj.getPagamentoVO().getCodigo().equals(0)) {
						sqlAlterar.setInt(2, obj.getPagamentoVO().getCodigo());
					} else {
						sqlAlterar.setNull(2, 0);
					}
					sqlAlterar.setInt(3, obj.getCodigo().intValue());
					return sqlAlterar;
				}
			});
			
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void excluir(ContaPagarPagamentoVO obj, UsuarioVO usuario) throws Exception {
		try {
			String sql = "DELETE FROM ContaPagarPagamento WHERE ((codigo = ?))";
			getConexao().getJdbcTemplate().update(sql, new Object[] { obj.getCodigo() });
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public List<ContaPagarPagamentoVO> consultar(String campoConsulta, String valorConsulta, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		List<ContaPagarPagamentoVO> listaConsulta = new ArrayList<ContaPagarPagamentoVO>(0);
		if (campoConsulta.equals("NUMERO_CONTA_CORRENTE")) {
			listaConsulta = consultarPorNumeroContaPagarPagamento(valorConsulta, nivelMontarDados, usuarioVO);
		}
		if (campoConsulta.equals("DESCRICAO")) {
			listaConsulta = consultarPorDescricao(valorConsulta, nivelMontarDados, usuarioVO);
		}
		return listaConsulta;
	}

	public List<ContaPagarPagamentoVO> consultarPorDescricao(String nome, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		StringBuilder sb = new StringBuilder();
		sb.append(getObterConsultaNivelMontarDadosLiderCelula(nivelMontarDados));
		sb.append(" where contaPagarPagamento.descricao ilike ('").append(nome).append("%') ");
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
		return montarDadosLista(tabelaResultado, nivelMontarDados, usuarioVO);
	}
	
	public List<ContaPagarPagamentoVO> consultarPorNumeroContaPagarPagamento(String nome, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		StringBuilder sb = new StringBuilder();
		sb.append(getObterConsultaNivelMontarDadosLiderCelula(nivelMontarDados));
		sb.append(" where contaPagarPagamento.numero ilike ('").append(nome).append("%') ");
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
		return montarDadosLista(tabelaResultado, nivelMontarDados, usuarioVO);
	}
	
	@Override
	public List<ContaPagarPagamentoVO> consultarPorPagamento(Integer pagamento, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		StringBuilder sb = new StringBuilder();
		sb.append(getObterConsultaNivelMontarDadosLiderCelula(nivelMontarDados));
		sb.append(" where contaPagarPagamento.pagamento = ").append(pagamento);
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
		return montarDadosLista(tabelaResultado, nivelMontarDados, usuarioVO);
	}

	
	@Override
	public void carregarDados(ContaPagarPagamentoVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		if (nivelMontarDados.equals(NivelMontarDadosEnum.BASICO)) {
			StringBuilder sb = getDadosBasicos();
			sb.append(" where contaPagarPagamento.codigo = ").append(obj.getCodigo());
			SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
			if (tabelaResultado.next()) {
				montarDadosBasicos(tabelaResultado, obj, nivelMontarDados, usuarioVO);
			}
		}
		if (nivelMontarDados.equals(NivelMontarDadosEnum.COMPLETO)) {
			StringBuilder sb = getDadosCompletos();
			sb.append(" where contaPagarPagamento.codigo = ").append(obj.getCodigo());
			SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
			if (tabelaResultado.next()) {
				montarDadosCompletos(tabelaResultado, obj, nivelMontarDados, usuarioVO);
			}
		}
	}
	
	public StringBuilder getObterConsultaNivelMontarDadosLiderCelula(NivelMontarDadosEnum nivelMontarDados) {
		if (nivelMontarDados.equals(NivelMontarDadosEnum.BASICO)) {
			return getDadosBasicos();
		}
		return getDadosCompletos();
	}

	
	public List<ContaPagarPagamentoVO> montarDadosLista(SqlRowSet dadosSQL, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		List<ContaPagarPagamentoVO> listaMembroVOs = new ArrayList<ContaPagarPagamentoVO>(0);
		while (dadosSQL.next()) {
			ContaPagarPagamentoVO obj = new ContaPagarPagamentoVO();
			if (nivelMontarDados.equals(NivelMontarDadosEnum.BASICO)) {
				montarDadosBasicos(dadosSQL, obj, nivelMontarDados, usuarioVO);
			} else {
				montarDadosCompletos(dadosSQL, obj, nivelMontarDados, usuarioVO);
			}
			listaMembroVOs.add(obj);
		}
		return listaMembroVOs;
	}
	
	public StringBuilder getDadosBasicos() {
		StringBuilder sb = new StringBuilder();
		sb.append("select contaPagarPagamento.codigo, ");
		sb.append(" contaPagar.codigo AS \"contaPagar.codigo\",  ");
		sb.append(" pagamento.codigo AS \"pagamento.codigo\" ");
		sb.append(" from contaPagarPagamento ");
		sb.append(" inner join contapagar on contaPagar.codigo = contaPagarPagamento.contaPagar ");
		sb.append(" inner join pagamento on pagamento.codigo = contaPagarPagamento.pagamento ");
		return sb;
	}
	
	public StringBuilder getDadosCompletos() {
		StringBuilder sb = new StringBuilder();
		sb.append("select contaPagarPagamento.codigo, ");
		sb.append(" contaPagar.codigo AS \"contaPagar.codigo\",  ");
		sb.append(" pagamento.codigo AS \"pagamento.codigo\" ");
		sb.append(" from contaPagarPagamento ");
		sb.append(" inner join contapagar on contaPagar.codigo = contaPagarPagamento.contaPagar ");
		sb.append(" inner join pagamento on pagamento.codigo = contaPagarPagamento.pagamento ");
		return sb;
	}
	
	public void montarDadosBasicos(SqlRowSet dadosSQL, ContaPagarPagamentoVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		obj.setCodigo(dadosSQL.getInt("codigo"));
		obj.getContaPagarVO().setCodigo(dadosSQL.getInt("contaPagar.codigo"));
		obj.getPagamentoVO().setCodigo(dadosSQL.getInt("pagamento.codigo"));
		
		getFacadeFactory().getContaPagarFacade().carregarDados(obj.getContaPagarVO(), NivelMontarDadosEnum.TODOS, usuarioVO);
		obj.setNovoObj(false);
	}
	
	public void montarDadosCompletos(SqlRowSet dadosSQL, ContaPagarPagamentoVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		obj.setCodigo(dadosSQL.getInt("codigo"));
		obj.getContaPagarVO().setCodigo(dadosSQL.getInt("contaPagar.codigo"));
		obj.getPagamentoVO().setCodigo(dadosSQL.getInt("pagamento.codigo"));
		
		getFacadeFactory().getContaPagarFacade().carregarDados(obj.getContaPagarVO(), NivelMontarDadosEnum.COMPLETO, usuarioVO);
		obj.setNovoObj(false);
		if (nivelMontarDados.equals(NivelMontarDadosEnum.TELA_CONSULTA)) {
			return;
		}
		return;
	}

	@Override
	public void incluirContaPagarPagamentoVOs(PagamentoVO pagamentoVO, List<ContaPagarPagamentoVO> listaContaPagarPagamentoVOs, UsuarioVO usuarioVO) throws Exception {
		for (ContaPagarPagamentoVO contaPagarPagamentoVO : listaContaPagarPagamentoVOs) {
			contaPagarPagamentoVO.getPagamentoVO().setCodigo(pagamentoVO.getCodigo());
			if (contaPagarPagamentoVO.getCodigo().equals(0)) {
				incluir(contaPagarPagamentoVO, usuarioVO);
			} 
		}
	}
	
	@Override
	public void alterarContaPagarPagamentoVOs(PagamentoVO pagamentoVO, List<ContaPagarPagamentoVO> listaContaPagarPagamentoVOs, UsuarioVO usuarioVO) throws Exception {
		excluirContaPagarPagamentoVOs(pagamentoVO.getCodigo(), listaContaPagarPagamentoVOs, usuarioVO);
		for (ContaPagarPagamentoVO contaPagarPagamentoVO : listaContaPagarPagamentoVOs) {
			contaPagarPagamentoVO.getPagamentoVO().setCodigo(pagamentoVO.getCodigo());
			if (contaPagarPagamentoVO.getCodigo().equals(0)) {
				incluir(contaPagarPagamentoVO, usuarioVO);
			} else {
				alterar(contaPagarPagamentoVO, usuarioVO);
			}
		}
	}
	
	public void excluirContaPagarPagamentoVOs(Integer pagamento, List<ContaPagarPagamentoVO> listaContaPagarPagamentoVOs, UsuarioVO usuarioVO) {
		StringBuilder sb = new StringBuilder();
		sb.append("delete from contaPagarPagamento where pagamento = ").append(pagamento); 
		sb.append(" and contaPagar not in(");
		for (ContaPagarPagamentoVO contaPagarPagamentoVO : listaContaPagarPagamentoVOs) {
			sb.append(contaPagarPagamentoVO.getContaPagarVO().getCodigo()).append(", ");
		}
		sb.append("0) ");
		getConexao().getJdbcTemplate().update(sb.toString(), new Object[] { });
	}
	
	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void executarEstornoPagamentoContaPagar(PagamentoVO pagamentoVO, UsuarioVO usuarioVO) throws Exception {
		try {
			for (ContaPagarPagamentoVO contaPagarPagamentoVO : pagamentoVO.getListaContaPagarPagamentoVOs()) {
				contaPagarPagamentoVO.getContaPagarVO().setSituacao(SituacaoContaPagarEnum.A_PAGAR);
				contaPagarPagamentoVO.getContaPagarVO().setDataPagamento(null);
				contaPagarPagamentoVO.getContaPagarVO().setDesconto(BigDecimal.ZERO);
				contaPagarPagamentoVO.getContaPagarVO().setJuro(BigDecimal.ZERO);
				contaPagarPagamentoVO.getContaPagarVO().setMulta(BigDecimal.ZERO);
				contaPagarPagamentoVO.getContaPagarVO().setValorPago(BigDecimal.ZERO);
				getFacadeFactory().getContaPagarFacade().persistir(contaPagarPagamentoVO.getContaPagarVO(), usuarioVO);
			}
		} catch (Exception e) {
			throw e;
		}
	}
	
	
}
