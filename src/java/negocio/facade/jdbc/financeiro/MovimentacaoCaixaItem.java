package negocio.facade.jdbc.financeiro;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.comuns.basico.ClienteVO;
import negocio.comuns.basico.FornecedorVO;
import negocio.comuns.financeiro.ChequeVO;
import negocio.comuns.financeiro.ContaCorrenteVO;
import negocio.comuns.financeiro.FormaPagamentoVO;
import negocio.comuns.financeiro.enumeradores.SituacaoCaixaEnum;
import negocio.comuns.financeiro.enumeradores.TipoMovimentacaoCaixaEnum;
import negocio.comuns.financeiro.enumeradores.TipoOrigemMovimentacaoCaixaEnum;
import negocio.comuns.financeiro.enumeradores.TipoSacadoEnum;
import negocio.comuns.utilitarias.Uteis;
import negocio.facade.jdbc.arquitetura.ControleAcesso;
import negocio.interfaces.financeiro.MovimentacaoCaixaItemInterfaceFacade;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import controle.financeiro.MovimentacaoCaixaItemVO;

@Repository
@Scope("singleton")
@Lazy
public class MovimentacaoCaixaItem extends ControleAcesso implements MovimentacaoCaixaItemInterfaceFacade {

	private static final long serialVersionUID = 1L;

	public MovimentacaoCaixaItem() {
		super();
	}

	public void validarDados(MovimentacaoCaixaItemVO obj, UsuarioVO usuarioVO) throws Exception {
	}

	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void persistir(MovimentacaoCaixaItemVO obj, UsuarioVO usuarioVO) throws Exception {
		if (obj.getCodigo().equals(0)) {
			incluir(obj, usuarioVO);
		} else {
			alterar(obj, usuarioVO);
		}
	}

	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void incluir(final MovimentacaoCaixaItemVO obj, UsuarioVO usuario) throws Exception {
		try {
			validarDados(obj, usuario);
			final String sql = "INSERT INTO MovimentacaoCaixaItem( movimentacaoCaixa, responsavel, formaPagamento, fornecedor, dataMovimentacaoCaixa, valor, codigoOrigem, tipoMovimentacaoCaixa, tipoSacado, tipoOrigem, cheque, contaCorrente) "
					+ "" + " VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) returning codigo";
			obj.setCodigo((Integer) getConexao().getJdbcTemplate().query(new PreparedStatementCreator() {

				public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
					PreparedStatement sqlInserir = arg0.prepareStatement(sql);
					if (!obj.getMovimentacaoCaixaVO().getCodigo().equals(0)) {
						sqlInserir.setInt(1, obj.getMovimentacaoCaixaVO().getCodigo());
					} else {
						sqlInserir.setNull(1, 0);
					}
					if (!obj.getResponsavelVO().getCodigo().equals(0)) {
						sqlInserir.setInt(2, obj.getResponsavelVO().getCodigo());
					} else {
						sqlInserir.setNull(2, 0);
					}
					if (!obj.getFormaPagamentoVO().getCodigo().equals(0)) {
						sqlInserir.setInt(3, obj.getFormaPagamentoVO().getCodigo());
					} else {
						sqlInserir.setNull(3, 0);
					}
					if (!obj.getFornecedorVO().getCodigo().equals(0)) {
						sqlInserir.setInt(4, obj.getFornecedorVO().getCodigo());
					} else {
						sqlInserir.setNull(4, 0);
					}
					sqlInserir.setTimestamp(5, Uteis.getDataJDBCTimestamp(obj.getDataMovimentacaoCaixa()));
					sqlInserir.setBigDecimal(6, obj.getValor());
					sqlInserir.setInt(7, obj.getCodigoOrigem());
					sqlInserir.setString(8, obj.getTipoMovimentacaoCaixa().name());
					sqlInserir.setString(9, obj.getTipoSacado().name());
					sqlInserir.setString(10, obj.getTipoOrigem().name());
					if (!obj.getChequeVO().getCodigo().equals(0)) {
						sqlInserir.setInt(11, obj.getChequeVO().getCodigo());
					} else {
						sqlInserir.setNull(11, 0);
					}
					if (!obj.getContaCorrenteVO().getCodigo().equals(0)) {
						sqlInserir.setInt(12, obj.getContaCorrenteVO().getCodigo());
					} else {
						sqlInserir.setNull(12, 0);
					}
					return sqlInserir;
				}
			}, new ResultSetExtractor() {

				public Object extractData(ResultSet arg0) throws SQLException, DataAccessException {
					if (arg0.next()) {
						obj.setNovoObj(Boolean.FALSE);
						return arg0.getInt("codigo");
					}
					return null;
				}
			}));

			obj.setNovoObj(Boolean.FALSE);
		} catch (Exception e) {
			obj.setNovoObj(Boolean.TRUE);
			throw e;
		}
	}

	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void alterar(final MovimentacaoCaixaItemVO obj, UsuarioVO usuario) throws Exception {
		try {
			validarDados(obj, usuario);
			final String sql = "UPDATE MovimentacaoCaixaItem set movimentacaoCaixa=?, responsavel=?, formaPagamento=?, fornecedor=?, dataMovimentacaoCaixa=?, valor=?, codigoOrigem=?, tipoMovimentacaoCaixa=?, tipoSacado=?, tipoOrigem=?, cheque=?, contaCorrente=? "
					+ "" + "WHERE ((codigo = ?))";

			getConexao().getJdbcTemplate().update(new PreparedStatementCreator() {

				public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
					PreparedStatement sqlAlterar = arg0.prepareStatement(sql);
					if (!obj.getMovimentacaoCaixaVO().getCodigo().equals(0)) {
						sqlAlterar.setInt(1, obj.getMovimentacaoCaixaVO().getCodigo());
					} else {
						sqlAlterar.setNull(1, 0);
					}
					if (!obj.getResponsavelVO().getCodigo().equals(0)) {
						sqlAlterar.setInt(2, obj.getResponsavelVO().getCodigo());
					} else {
						sqlAlterar.setNull(2, 0);
					}
					if (!obj.getFormaPagamentoVO().getCodigo().equals(0)) {
						sqlAlterar.setInt(3, obj.getFormaPagamentoVO().getCodigo());
					} else {
						sqlAlterar.setNull(3, 0);
					}
					if (!obj.getFornecedorVO().getCodigo().equals(0)) {
						sqlAlterar.setInt(4, obj.getFornecedorVO().getCodigo());
					} else {
						sqlAlterar.setNull(4, 0);
					}
					sqlAlterar.setTimestamp(5, Uteis.getDataJDBCTimestamp(obj.getDataMovimentacaoCaixa()));
					sqlAlterar.setBigDecimal(6, obj.getValor());
					sqlAlterar.setInt(7, obj.getCodigoOrigem());
					sqlAlterar.setString(8, obj.getTipoMovimentacaoCaixa().name());
					sqlAlterar.setString(9, obj.getTipoSacado().name());
					sqlAlterar.setString(10, obj.getTipoOrigem().name());
					if (!obj.getChequeVO().getCodigo().equals(0)) {
						sqlAlterar.setInt(11, obj.getChequeVO().getCodigo());
					} else {
						sqlAlterar.setNull(11, 0);
					}
					if (!obj.getContaCorrenteVO().getCodigo().equals(0)) {
						sqlAlterar.setInt(12, obj.getContaCorrenteVO().getCodigo());
					} else {
						sqlAlterar.setNull(12, 0);
					}
					sqlAlterar.setInt(12, obj.getCodigo().intValue());
					return sqlAlterar;
				}
			});

		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void excluir(MovimentacaoCaixaItemVO obj, UsuarioVO usuario) throws Exception {
		try {
			String sql = "DELETE FROM MovimentacaoCaixaItem WHERE ((codigo = ?))";
			getConexao().getJdbcTemplate().update(sql, new Object[] { obj.getCodigo() });
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public List<MovimentacaoCaixaItemVO> consultar(String campoConsulta, String valorConsulta, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		List<MovimentacaoCaixaItemVO> listaConsulta = new ArrayList<MovimentacaoCaixaItemVO>(0);
		if (campoConsulta.equals("NUMERO_CONTA_CORRENTE")) {
			listaConsulta = consultarPorNumeroMovimentacaoCaixaItem(valorConsulta, nivelMontarDados, usuarioVO);
		}
		if (campoConsulta.equals("DESCRICAO")) {
			listaConsulta = consultarPorDescricao(valorConsulta, nivelMontarDados, usuarioVO);
		}
		return listaConsulta;
	}

	public List<MovimentacaoCaixaItemVO> consultarPorDescricao(String nome, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		StringBuilder sb = new StringBuilder();
		sb.append(getDadosBasicos());
		sb.append(" where movimentacaoCaixaItem.descricao ilike ('").append(nome).append("%') ");
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
		return montarDadosLista(tabelaResultado, nivelMontarDados, usuarioVO);
	}

	public List<MovimentacaoCaixaItemVO> consultarPorNumeroMovimentacaoCaixaItem(String nome, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		StringBuilder sb = new StringBuilder();
		sb.append(getDadosBasicos());
		sb.append(" where movimentacaoCaixaItem.numero ilike ('").append(nome).append("%') ");
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
		return montarDadosLista(tabelaResultado, nivelMontarDados, usuarioVO);
	}

	@Override
	public void carregarDados(MovimentacaoCaixaItemVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		if (nivelMontarDados.equals(NivelMontarDadosEnum.BASICO)) {
			StringBuilder sb = getDadosBasicos();
			sb.append(" where movimentacaoCaixaItem.codigo = ").append(obj.getCodigo());
			SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
			if (tabelaResultado.next()) {
				montarDadosBasicos(tabelaResultado, obj, nivelMontarDados, usuarioVO);
			}
		}
		if (nivelMontarDados.equals(NivelMontarDadosEnum.COMPLETO)) {
			StringBuilder sb = getDadosCompletos();
			sb.append(" where movimentacaoCaixaItem.codigo = ").append(obj.getCodigo());
			SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
			if (tabelaResultado.next()) {
				montarDadosCompletos(tabelaResultado, obj, nivelMontarDados, usuarioVO);
			}
		}
	}
	
	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void incluirMovimentacaoCaixaItemVOs(Integer movimentacaoCaixa, List<MovimentacaoCaixaItemVO> listaMovimentacaoCaixaItemVOs, UsuarioVO usuario) throws Exception {
		Iterator e = listaMovimentacaoCaixaItemVOs.iterator();
		while (e.hasNext()) {
			MovimentacaoCaixaItemVO obj = (MovimentacaoCaixaItemVO) e.next();
			obj.getMovimentacaoCaixaVO().setCodigo(movimentacaoCaixa);
			incluir(obj, usuario);
		}
	}

	public List<MovimentacaoCaixaItemVO> montarDadosLista(SqlRowSet dadosSQL, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		List<MovimentacaoCaixaItemVO> listaMembroVOs = new ArrayList<MovimentacaoCaixaItemVO>(0);
		while (dadosSQL.next()) {
			MovimentacaoCaixaItemVO obj = new MovimentacaoCaixaItemVO();
			montarDadosBasicos(dadosSQL, obj, nivelMontarDados, usuarioVO);
			listaMembroVOs.add(obj);
		}
		return listaMembroVOs;
	}

	public StringBuilder getDadosBasicos() {
		StringBuilder sb = new StringBuilder();
		sb.append("select * from movimentacaoCaixaItem ");
		return sb;
	}

	public StringBuilder getDadosCompletos() {
		StringBuilder sb = new StringBuilder();
		sb.append("select * from movimentacaoCaixaItem ");
		return sb;
	}

	public void montarDadosBasicos(SqlRowSet dadosSQL, MovimentacaoCaixaItemVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		obj.setCodigo(dadosSQL.getInt("codigo"));
		obj.setNovoObj(false);
	}

	public void montarDadosCompletos(SqlRowSet dadosSQL, MovimentacaoCaixaItemVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		obj.setCodigo(dadosSQL.getInt("codigo"));
		obj.setNovoObj(false);
		if (nivelMontarDados.equals(NivelMontarDadosEnum.TELA_CONSULTA)) {
			return;
		}
		return;
	}

	@Override
	public void criarMovimentacaoCaixaItem(TipoMovimentacaoCaixaEnum tipoMovimentacaoCaixaEnum, TipoSacadoEnum tipoSacadoEnum, Integer codigoSacado, ContaCorrenteVO caixaVO, FormaPagamentoVO formaPagamentoVO, ChequeVO chequeVO, BigDecimal valor, TipoOrigemMovimentacaoCaixaEnum tipoOrigemMovimentacaoCaixaEnum, ContaCorrenteVO contaCorrenteVO, Integer codigoOrigem, UsuarioVO usuarioVO) throws Exception {
		try {
			MovimentacaoCaixaItemVO movimentacaoCaixaItemVO = new MovimentacaoCaixaItemVO();
			movimentacaoCaixaItemVO.setTipoMovimentacaoCaixa(tipoMovimentacaoCaixaEnum);
			if (tipoSacadoEnum.equals(TipoSacadoEnum.CLIENTE)) {
				movimentacaoCaixaItemVO.setClienteVO(new ClienteVO());
				movimentacaoCaixaItemVO.getClienteVO().setCodigo(codigoSacado);
			} else if (tipoSacadoEnum.equals(TipoSacadoEnum.FORNECEDOR)) {
				movimentacaoCaixaItemVO.setFornecedorVO(new FornecedorVO());
				movimentacaoCaixaItemVO.getFornecedorVO().setCodigo(codigoSacado);
			}
			movimentacaoCaixaItemVO.setFormaPagamentoVO(formaPagamentoVO);
			movimentacaoCaixaItemVO.setTipoSacado(tipoSacadoEnum);

			if (!formaPagamentoVO.getIsTipoCheque()) {
				chequeVO = null;
				movimentacaoCaixaItemVO.setChequeVO(null);
			} else {
				movimentacaoCaixaItemVO.setChequeVO(chequeVO);
			}
			if (formaPagamentoVO.getIsTipoDinheiro()) {
				 contaCorrenteVO = null;
				 movimentacaoCaixaItemVO.setContaCorrenteVO(null);
			} else {
				movimentacaoCaixaItemVO.setContaCorrenteVO(contaCorrenteVO);
			}
			movimentacaoCaixaItemVO.setDataMovimentacaoCaixa(new Date());
			movimentacaoCaixaItemVO.setValor(valor);
			movimentacaoCaixaItemVO.setTipoOrigem(tipoOrigemMovimentacaoCaixaEnum);
			movimentacaoCaixaItemVO.setCodigoOrigem(codigoOrigem);
			movimentacaoCaixaItemVO.setMovimentacaoCaixaVO(getFacadeFactory().getMovimentacaoCaixaFacade().consultarMovimentacaoCaixaPorContaCorrenteSituacao(caixaVO.getCodigo(), SituacaoCaixaEnum.ABERTO, usuarioVO));

			if (tipoMovimentacaoCaixaEnum.equals(TipoMovimentacaoCaixaEnum.SAIDA) && ((formaPagamentoVO.getIsTipoCheque() && !movimentacaoCaixaItemVO.getChequeVO().getChequeProprio()) || formaPagamentoVO.getIsTipoDinheiro())) {
				getFacadeFactory().getMovimentacaoCaixaFacade().consultarSaldoMovimentacaoCaixa(movimentacaoCaixaItemVO.getMovimentacaoCaixaVO());
				if (formaPagamentoVO.getIsTipoCheque() && movimentacaoCaixaItemVO.getMovimentacaoCaixaVO().getSaldoFinalCheque().compareTo(valor) < 0 && !movimentacaoCaixaItemVO.getChequeVO().getChequeProprio()) {
					throw new Exception("Saldo insuficiente");
				} else if (formaPagamentoVO.getIsTipoDinheiro() && movimentacaoCaixaItemVO.getMovimentacaoCaixaVO().getSaldoFinalDinheiro().compareTo(valor) < 0) {
					throw new Exception("Saldo insuficiente");
				}
			}
			getFacadeFactory().getMovimentacaoCaixaItemFacade().persistir(movimentacaoCaixaItemVO, usuarioVO);
		} catch (Exception e) {
			throw e;
		}
	}

}
