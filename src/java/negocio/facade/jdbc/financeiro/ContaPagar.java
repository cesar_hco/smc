package negocio.facade.jdbc.financeiro;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.comuns.financeiro.ContaPagarVO;
import negocio.comuns.financeiro.enumeradores.SituacaoContaPagarEnum;
import negocio.comuns.utilitarias.Uteis;
import negocio.facade.jdbc.arquitetura.ControleAcesso;
import negocio.interfaces.financeiro.ContaPagarInterfaceFacade;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Scope("singleton")
@Lazy
public class ContaPagar extends ControleAcesso implements ContaPagarInterfaceFacade {

	private static final long serialVersionUID = 1L;

	public ContaPagar() {
		super();
	}

	public void validarDados(ContaPagarVO obj, UsuarioVO usuarioVO) throws Exception {
		if (obj.getFornecedorVO().getCodigo().equals(0)) {
			throw new Exception("O campo FORNECEDOR deve ser informado!");
		}
		if (obj.getDataVencimento() == null) {
			throw new Exception("O campo DATA DE VENCIMENTO deve ser informado!");
		}
		if (obj.getValor().equals(BigDecimal.ZERO)) {
			throw new Exception("O campo VALOR deve ser informado!");
		}
		
	}

	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void persistir(ContaPagarVO obj, UsuarioVO usuarioVO) throws Exception {
		if (obj.getCodigo().equals(0)) {
			incluir(obj, usuarioVO);
		} else {
			alterar(obj, usuarioVO);
		}
	}

	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void incluir(final ContaPagarVO obj, UsuarioVO usuario) throws Exception {
		try {
			validarDados(obj, usuario);
			final String sql = "INSERT INTO ContaPagar( data, situacao, dataVencimento, dataCompetencia, valor, valorPago, juro, multa, "
					+ "" + " valorJuro, valorMulta, desconto, nrDocumento, codigoBarra, parcela, descricao, fornecedor, categoriaDespesa) "
					+ "" + " VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) returning codigo";
			obj.setCodigo((Integer) getConexao().getJdbcTemplate().query(new PreparedStatementCreator() {

				public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
					PreparedStatement sqlInserir = arg0.prepareStatement(sql);
					sqlInserir.setDate(1, Uteis.getDataJDBC(obj.getData()));
					sqlInserir.setString(2, obj.getSituacao().toString());
					sqlInserir.setDate(3, Uteis.getDataJDBC(obj.getDataVencimento()));
					sqlInserir.setDate(4, Uteis.getDataJDBC(obj.getDataCompetencia()));
					sqlInserir.setBigDecimal(5, obj.getValor());
					sqlInserir.setBigDecimal(6, obj.getValorPago());
					sqlInserir.setBigDecimal(7, obj.getJuro());
					sqlInserir.setBigDecimal(8, obj.getMulta());
					sqlInserir.setBigDecimal(9, obj.getValorJuro());
					sqlInserir.setBigDecimal(10, obj.getValorMulta());
					sqlInserir.setBigDecimal(11, obj.getDesconto());
					sqlInserir.setString(12, obj.getNrDocumento());
					sqlInserir.setString(13, obj.getCodigoBarra());
					sqlInserir.setString(14, obj.getParcela());
					sqlInserir.setString(15, obj.getDescricao());
					if (!obj.getFornecedorVO().getCodigo().equals(0)) {
						sqlInserir.setInt(16, obj.getFornecedorVO().getCodigo());
					} else {
						sqlInserir.setNull(16, 0);
					}
					if (!obj.getCategoriaDespesaVO().getCodigo().equals(0)) {
						sqlInserir.setInt(17, obj.getCategoriaDespesaVO().getCodigo());
					} else {
						sqlInserir.setNull(17, 0);
					}
					return sqlInserir;
				}
			}, new ResultSetExtractor() {

				public Object extractData(ResultSet arg0) throws SQLException, DataAccessException {
					if (arg0.next()) {
						obj.setNovoObj(Boolean.FALSE);
						return arg0.getInt("codigo");
					}
					return null;
				}
			}));

			obj.setNovoObj(Boolean.FALSE);
		} catch (Exception e) {
			obj.setNovoObj(Boolean.TRUE);
			throw e;
		}
	}

	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void alterar(final ContaPagarVO obj, UsuarioVO usuario) throws Exception {
		try {
			validarDados(obj, usuario);
			final String sql = "UPDATE ContaPagar set data=?, situacao=?, dataVencimento=?, dataCompetencia=?, valor=?, valorPago=?, juro=?, multa=?, valorJuro=?, "
					+ "valorMulta=?, desconto=?, nrDocumento=?, codigoBarra=?, parcela=?, descricao=?, fornecedor=?, categoriaDespesa=? " + "WHERE ((codigo = ?))";

			getConexao().getJdbcTemplate().update(new PreparedStatementCreator() {

				public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
					PreparedStatement sqlAlterar = arg0.prepareStatement(sql);
					sqlAlterar.setDate(1, Uteis.getDataJDBC(obj.getData()));
					sqlAlterar.setString(2, obj.getSituacao().toString());
					sqlAlterar.setDate(3, Uteis.getDataJDBC(obj.getDataVencimento()));
					sqlAlterar.setDate(4, Uteis.getDataJDBC(obj.getDataCompetencia()));
					sqlAlterar.setBigDecimal(5, obj.getValor());
					sqlAlterar.setBigDecimal(6, obj.getValorPago());
					sqlAlterar.setBigDecimal(7, obj.getJuro());
					sqlAlterar.setBigDecimal(8, obj.getMulta());
					sqlAlterar.setBigDecimal(9, obj.getValorJuro());
					sqlAlterar.setBigDecimal(10, obj.getValorMulta());
					sqlAlterar.setBigDecimal(11, obj.getDesconto());
					sqlAlterar.setString(12, obj.getNrDocumento());
					sqlAlterar.setString(13, obj.getCodigoBarra());
					sqlAlterar.setString(14, obj.getParcela());
					sqlAlterar.setString(15, obj.getDescricao());
					if (!obj.getFornecedorVO().getCodigo().equals(0)) {
						sqlAlterar.setInt(16, obj.getFornecedorVO().getCodigo());
					} else {
						sqlAlterar.setNull(16, 0);
					}
					if (!obj.getCategoriaDespesaVO().getCodigo().equals(0)) {
						sqlAlterar.setInt(17, obj.getCategoriaDespesaVO().getCodigo());
					} else {
						sqlAlterar.setNull(17, 0);
					}
					sqlAlterar.setInt(18, obj.getCodigo().intValue());
					return sqlAlterar;
				}
			});

		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void excluir(ContaPagarVO obj, UsuarioVO usuario) throws Exception {
		try {
			String sql = "DELETE FROM ContaPagar WHERE ((codigo = ?))";
			getConexao().getJdbcTemplate().update(sql, new Object[] { obj.getCodigo() });
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public List<ContaPagarVO> consultar(String campoConsulta, String valorConsulta, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		List<ContaPagarVO> listaConsulta = new ArrayList<ContaPagarVO>(0);
		if (campoConsulta.equals("FORNECEDOR")) {
			listaConsulta = consultarPorNomeFornecedor(valorConsulta, nivelMontarDados, usuarioVO);
		}
		if (campoConsulta.equals("NR_DOCUMENTO")) {
			listaConsulta = consultarPorNrDocumento(valorConsulta, nivelMontarDados, usuarioVO);
		}
		return listaConsulta;
	}

	public ContaPagarVO consultarPorCodigo(Integer codigo, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		StringBuilder sb = new StringBuilder();
		sb.append(getDadosBasicos());
		sb.append(" where contaPagar.codigo = ").append(codigo).append(" ");
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
		ContaPagarVO obj = new ContaPagarVO();
		if (tabelaResultado.next()) {
			montarDadosBasicos(tabelaResultado, obj, nivelMontarDados, usuarioVO);
		}
		return obj;
	}
	
	public List<ContaPagarVO> consultarPorNrDocumento(String nrDocumento, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		StringBuilder sb = new StringBuilder();
		sb.append(getDadosBasicos());
		sb.append(" where contaPagar.nrDocumento ilike ('").append(nrDocumento).append("%') ");
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
		return montarDadosLista(tabelaResultado, nivelMontarDados, usuarioVO);
	}

	public List<ContaPagarVO> consultarPorNomeFornecedor(String nome, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		StringBuilder sb = new StringBuilder();
		sb.append(getDadosBasicos());
		
		sb.append(" where fornecedor.nome ilike ('").append(nome).append("%') ");
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
		return montarDadosLista(tabelaResultado, nivelMontarDados, usuarioVO);
	}

	@Override
	public void carregarDados(ContaPagarVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		if (nivelMontarDados.equals(NivelMontarDadosEnum.BASICO)) {
			StringBuilder sb = getDadosBasicos();
			sb.append(" where contaPagar.codigo = ").append(obj.getCodigo());
			SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
			if (tabelaResultado.next()) {
				montarDadosBasicos(tabelaResultado, obj, nivelMontarDados, usuarioVO);
			}
		}
		if (nivelMontarDados.equals(NivelMontarDadosEnum.COMPLETO)) {
			StringBuilder sb = getDadosCompletos();
			sb.append(" where contaPagar.codigo = ").append(obj.getCodigo());
			SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
			if (tabelaResultado.next()) {
				montarDadosCompletos(tabelaResultado, obj, nivelMontarDados, usuarioVO);
			}
		}
	}

	public List<ContaPagarVO> montarDadosLista(SqlRowSet dadosSQL, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		List<ContaPagarVO> listaMembroVOs = new ArrayList<ContaPagarVO>(0);
		while (dadosSQL.next()) {
			ContaPagarVO obj = new ContaPagarVO();
			if (nivelMontarDados.equals(NivelMontarDadosEnum.BASICO)) {
				montarDadosBasicos(dadosSQL, obj, nivelMontarDados, usuarioVO);
			} else {
				montarDadosCompletos(dadosSQL, obj, nivelMontarDados, usuarioVO);
			}
			listaMembroVOs.add(obj);
		}
		return listaMembroVOs;
	}

	public StringBuilder getDadosBasicos() {
		StringBuilder sb = new StringBuilder();
		sb.append("select contaPagar.*, fornecedor.codigo AS \"fornecedor.codigo\", fornecedor.nome AS \"fornecedor.nome\" from contaPagar ");
		sb.append(" inner join fornecedor on fornecedor.codigo = contaPagar.fornecedor ");
		return sb;
	}

	public StringBuilder getDadosCompletos() {
		StringBuilder sb = new StringBuilder();
		sb.append(" select contaPagar.*, ");
		sb.append(" fornecedor.codigo AS \"fornecedor.codigo\", fornecedor.nome AS \"fornecedor.nome\", ");
		sb.append(" categoriaDespesa.codigo AS \"categoriaDespesa.codigo\", categoriaDespesa.descricao AS \"categoriaDespesa.descricao\" ");
		sb.append(" from contaPagar ");
		sb.append(" inner join fornecedor on fornecedor.codigo = contaPagar.fornecedor ");
		sb.append(" left join categoriaDespesa on categoriaDespesa.codigo = contaPagar.categoriaDespesa ");
		return sb;
	}

	public void montarDadosBasicos(SqlRowSet dadosSQL, ContaPagarVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		obj.setCodigo(dadosSQL.getInt("codigo"));
		obj.setData(dadosSQL.getDate("data"));
		if (dadosSQL.getString("situacao") != null) {
			obj.setSituacao(SituacaoContaPagarEnum.valueOf(dadosSQL.getString("situacao")));
		}
		obj.setDataVencimento(dadosSQL.getDate("dataVencimento"));
		obj.setDataCompetencia(dadosSQL.getDate("dataCompetencia"));
		obj.setValor(dadosSQL.getBigDecimal("valor"));
		obj.setValorPago(dadosSQL.getBigDecimal("valorPago"));
		obj.setJuro(dadosSQL.getBigDecimal("juro"));
		obj.setMulta(dadosSQL.getBigDecimal("multa"));
		obj.setValorJuro(dadosSQL.getBigDecimal("valorJuro"));
		obj.setValorMulta(dadosSQL.getBigDecimal("valorMulta"));
		obj.setDesconto(dadosSQL.getBigDecimal("desconto"));
		obj.setNrDocumento(dadosSQL.getString("nrDocumento"));
		obj.setCodigoBarra(dadosSQL.getString("codigoBarra"));
		obj.setParcela(dadosSQL.getString("parcela"));
		obj.setDescricao(dadosSQL.getString("descricao"));
		obj.getFornecedorVO().setCodigo(dadosSQL.getInt("fornecedor.codigo"));
		obj.getFornecedorVO().setNome(dadosSQL.getString("fornecedor.nome"));
		
		obj.setNovoObj(false);
	}

	public void montarDadosCompletos(SqlRowSet dadosSQL, ContaPagarVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		obj.setCodigo(dadosSQL.getInt("codigo"));
		obj.setData(dadosSQL.getDate("data"));
		if (dadosSQL.getString("situacao") != null) {
			obj.setSituacao(SituacaoContaPagarEnum.valueOf(dadosSQL.getString("situacao")));
		}
		obj.setDataVencimento(dadosSQL.getDate("dataVencimento"));
		obj.setDataCompetencia(dadosSQL.getDate("dataCompetencia"));
		obj.setValor(dadosSQL.getBigDecimal("valor"));
		obj.setValorPago(dadosSQL.getBigDecimal("valorPago"));
		obj.setJuro(dadosSQL.getBigDecimal("juro"));
		obj.setMulta(dadosSQL.getBigDecimal("multa"));
		obj.setValorJuro(dadosSQL.getBigDecimal("valorJuro"));
		obj.setValorMulta(dadosSQL.getBigDecimal("valorMulta"));
		obj.setDesconto(dadosSQL.getBigDecimal("desconto"));
		obj.setNrDocumento(dadosSQL.getString("nrDocumento"));
		obj.setCodigoBarra(dadosSQL.getString("codigoBarra"));
		obj.setParcela(dadosSQL.getString("parcela"));
		obj.setDescricao(dadosSQL.getString("descricao"));
		obj.getFornecedorVO().setCodigo(dadosSQL.getInt("fornecedor.codigo"));
		obj.getFornecedorVO().setNome(dadosSQL.getString("fornecedor.nome"));
		obj.getCategoriaDespesaVO().setCodigo(dadosSQL.getInt("categoriaDespesa.codigo"));
		obj.getCategoriaDespesaVO().setDescricao(dadosSQL.getString("categoriaDespesa.descricao"));
		obj.setNovoObj(false);
		if (nivelMontarDados.equals(NivelMontarDadosEnum.TELA_CONSULTA)) {
			return;
		}
		return;
	}

	@Override
	public void realizarCalculoValorMulta(ContaPagarVO obj, UsuarioVO usuarioVO) {
		if (obj.getValor().compareTo(BigDecimal.ZERO) != 0) {
			if (obj.getMulta().compareTo(BigDecimal.ZERO) != 0) {
				obj.setValorMulta(obj.getValor().multiply(obj.getMulta()).divide(BigDecimal.valueOf(100)));
				return;
			}
		}
		obj.setValorMulta(BigDecimal.ZERO);
	}
	
	@Override
	public void realizarCalculoValorJuro(ContaPagarVO obj, UsuarioVO usuarioVO) {
		if (obj.getValor().compareTo(BigDecimal.ZERO) != 0) {
			if (obj.getJuro().compareTo(BigDecimal.ZERO) != 0) {
				obj.setValorJuro(obj.getValor().multiply(obj.getJuro()).divide(BigDecimal.valueOf(100)));
				return;
			}
		}
		obj.setValorJuro(BigDecimal.ZERO);
	}
	
	@Override
	public List<ContaPagarVO> consultarContaAPagarPorFornecedorSituacaoDataVencimento(Integer fornecedor, String situacao, Date dataInicio, Date dataFinal, NivelMontarDadosEnum nivelMontarDadosEnum, UsuarioVO usuarioVO) {
		StringBuilder sb = getDadosBasicos();
		sb.append(" where contapagar.situacao = 'A_PAGAR' ");
		if (!fornecedor.equals(0)) {
			sb.append(" and fornecedor.codigo = ").append(fornecedor);
		}
		if (situacao.equals("A_VENCER")) {
			sb.append(" and contapagar.datavencimento >= current_date ");
		} else if (situacao.equals("VENCIDAS")) {
			sb.append(" and contapagar.datavencimento < current_date ");
		} else if (situacao.equals("DATA_VENCIMENTO")) {
			sb.append(" and contapagar.datavencimento >= '").append(Uteis.getDataJDBC(dataInicio)).append("' and contapagar.datavencimento <= '").append(Uteis.getDataJDBC(dataFinal)).append("' ");
		}
		
		sb.append(" order by contapagar.dataVencimento ");
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
		return montarDadosLista(tabelaResultado, nivelMontarDadosEnum, usuarioVO); 
		
		
	}

	@Override
	public void calcularValorPagar(ContaPagarVO contaPagarVO, UsuarioVO usuarioVO) {
		contaPagarVO.setValorPago(contaPagarVO.getValor().subtract(contaPagarVO.getDesconto()).add(contaPagarVO.getJuro()).add(contaPagarVO.getMulta()));
	}
	
}
