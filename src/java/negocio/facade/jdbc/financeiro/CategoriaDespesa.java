package negocio.facade.jdbc.financeiro;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.support.GenericTypeAwareAutowireCandidateResolver;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.comuns.financeiro.CategoriaDespesaVO;
import negocio.comuns.financeiro.ContaReceberVO;
import negocio.facade.jdbc.arquitetura.ControleAcesso;
import negocio.interfaces.financeiro.CategoriaDespesaInterfaceFacade;

@Repository
@Scope("singleton")
@Lazy
public class CategoriaDespesa extends ControleAcesso implements CategoriaDespesaInterfaceFacade {

	private static final long serialVersionUID = 1L;

	public CategoriaDespesa() {
		super();
	}

	public void validarDados(CategoriaDespesaVO obj, UsuarioVO usuarioVO) throws Exception {
		if (obj.getDescricao().equals("")) {
			throw new Exception("O campo DESCRI��O deve ser informado!");
		}
	}

	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void persistir(CategoriaDespesaVO obj, UsuarioVO usuarioVO) throws Exception {
		if (obj.isNovoObj()) {
			incluir(obj, usuarioVO);
		} else {
			alterar(obj, usuarioVO);
		}
	}

	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void incluir(final CategoriaDespesaVO obj, UsuarioVO usuario) throws Exception {
		try {
			validarDados(obj, usuario);
			final String sql = "INSERT INTO CategoriaDespesa( descricao, categoriaDespesaPrincipal ) "
					+ " VALUES ( ?, ?) returning codigo";
			obj.setCodigo((Integer) getConexao().getJdbcTemplate().query(new PreparedStatementCreator() {

				public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
					PreparedStatement sqlInserir = arg0.prepareStatement(sql);
					sqlInserir.setString(1, obj.getDescricao());
					if (!obj.getCategoriaDespesaPrincipalVO().getCodigo().equals(0)) {
						sqlInserir.setInt(2, obj.getCategoriaDespesaPrincipalVO().getCodigo());
					} else {
						sqlInserir.setNull(2, 0);
					}
					return sqlInserir;
				}
			}, new ResultSetExtractor() {

				public Object extractData(ResultSet arg0) throws SQLException, DataAccessException {
					if (arg0.next()) {
						obj.setNovoObj(Boolean.FALSE);
						return arg0.getInt("codigo");
					}
					return null;
				}
			}));
			
			obj.setNovoObj(Boolean.FALSE);
		} catch (Exception e) {
			obj.setNovoObj(Boolean.TRUE);
			throw e;
		}
	}

	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void alterar(final CategoriaDespesaVO obj, UsuarioVO usuario) throws Exception {
		try {
			validarDados(obj, usuario);
			final String sql = "UPDATE CategoriaDespesa set descricao=?, categoriaDespesaPrincipal=? "
					+ "WHERE ((codigo = ?))";

			getConexao().getJdbcTemplate().update(new PreparedStatementCreator() {

				public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
					PreparedStatement sqlAlterar = arg0.prepareStatement(sql);
					sqlAlterar.setString(1, obj.getDescricao());
					if (!obj.getCategoriaDespesaPrincipalVO().getCodigo().equals(0)) {
						sqlAlterar.setInt(2, obj.getCategoriaDespesaPrincipalVO().getCodigo());
					} else {
						sqlAlterar.setNull(2, 0);
					}
					sqlAlterar.setInt(3, obj.getCodigo().intValue());
					return sqlAlterar;
				}
			});
			
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void excluir(CategoriaDespesaVO obj, UsuarioVO usuario) throws Exception {
		try {
			String sql = "DELETE FROM CategoriaDespesa WHERE ((codigo = ?))";
			getConexao().getJdbcTemplate().update(sql, new Object[] { obj.getCodigo() });
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public List<CategoriaDespesaVO> consultar(String campoConsulta, String valorConsulta, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		List<CategoriaDespesaVO> listaConsulta = new ArrayList<CategoriaDespesaVO>(0);
		if (campoConsulta.equals("DESCRICAO")) {
			listaConsulta = consultarPorDescricao(valorConsulta, true, nivelMontarDados, usuarioVO);
		}
		if (campoConsulta.equals("CATEGORIA_DESPESA_PRINCIPAL")) {
			listaConsulta = consultarPorCategoriaDespesaPrincipal(valorConsulta, nivelMontarDados, usuarioVO);
		}
		if (campoConsulta.equals("SUB_CATEGORIA_POR_CATEGORIA_DESPESA_PRINCIPAL")) {
			listaConsulta = consultarSubCategoriaPorCategoriaDespesaPrincipal(valorConsulta, nivelMontarDados, usuarioVO);
		}
		
		return listaConsulta;
	}

	@Override
	public List<CategoriaDespesaVO> consultarPorDescricao(String nome, Boolean trazerCategoriaPrincial, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		StringBuilder sb = new StringBuilder();
		sb.append(getDadosBasicos());
		sb.append(" where categoriaDespesa.descricao ilike ('").append(nome).append("%') ");
		if (!trazerCategoriaPrincial) {
			sb.append(" and (categoriaDespesa.categoriaDespesaPrincipal is null or categoriaDespesa.categoriaDespesaPrincipal = 0) ");
		}
		sb.append(" order by categoriaDespesa.descricao");
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
		return montarDadosLista(tabelaResultado, nivelMontarDados, usuarioVO);
	}
	
	@Override
	public List<CategoriaDespesaVO> consultarCategoriaPrincipalPorDescricao(String descricao, Integer codigoCategoriaDespesa, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		StringBuilder sb = new StringBuilder();
		sb.append(getDadosBasicos());
		sb.append(" where categoriaDespesa.descricao ilike ('").append(descricao).append("%') ");
		sb.append(" and categoriaDespesa.categoriaDespesaPrincipal is null ");
		if (!codigoCategoriaDespesa.equals(0)) {
			sb.append(" and categoriaDespesa.codigo != ").append(codigoCategoriaDespesa);
		}
		sb.append(" order by categoriaDespesaPrincipal.descricao ");
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
		return montarDadosLista(tabelaResultado, nivelMontarDados, usuarioVO);
	}
	
	public List<CategoriaDespesaVO> consultarPorCategoriaDespesaPrincipal(String nome, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		StringBuilder sb = new StringBuilder();
		sb.append(getDadosBasicos());
		sb.append(" where categoriaDespesa.descricao ilike ('").append(nome).append("%') ");
		sb.append(" and categoriaDespesa.codigo in(select categoriaDespesa.categoriaDespesaPrincipal from categoriaDespesa )");
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
		return montarDadosLista(tabelaResultado, nivelMontarDados, usuarioVO);
	}
	
	public List<CategoriaDespesaVO> consultarSubCategoriaPorCategoriaDespesaPrincipal(String nome, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		StringBuilder sb = new StringBuilder();
		sb.append(getDadosBasicos());
		sb.append(" where categoriaDespesaPrincipal.descricao ilike ('").append(nome).append("%') ");
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
		return montarDadosLista(tabelaResultado, nivelMontarDados, usuarioVO);
	}
	
	@Override
	public void carregarDados(CategoriaDespesaVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		if (nivelMontarDados.equals(NivelMontarDadosEnum.BASICO)) {
			StringBuilder sb = getDadosBasicos();
			sb.append(" where categoriaDespesa.codigo = ").append(obj.getCodigo());
			SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
			if (tabelaResultado.next()) {
				montarDadosBasicos(tabelaResultado, obj, nivelMontarDados, usuarioVO);
			}
		}
		if (nivelMontarDados.equals(NivelMontarDadosEnum.COMPLETO)) {
			StringBuilder sb = getDadosCompletos();
			sb.append(" where categoriaDespesa.codigo = ").append(obj.getCodigo());
			SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
			if (tabelaResultado.next()) {
				montarDadosCompletos(tabelaResultado, obj, nivelMontarDados, usuarioVO);
			}
		}
	}
	
	public List<CategoriaDespesaVO> montarDadosLista(SqlRowSet dadosSQL, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		List<CategoriaDespesaVO> listaMembroVOs = new ArrayList<CategoriaDespesaVO>(0);
		while (dadosSQL.next()) {
			CategoriaDespesaVO obj = new CategoriaDespesaVO();
			montarDadosBasicos(dadosSQL, obj, nivelMontarDados, usuarioVO);
			listaMembroVOs.add(obj);
		}
		return listaMembroVOs;
	}
	
	public StringBuilder getDadosBasicos() {
		StringBuilder sb = new StringBuilder();
		sb.append(" select categoriaDespesa.*, ");
		sb.append(" categoriaDespesaPrincipal.codigo AS \"categoriaDespesaPrincipal.codigo\", categoriaDespesaPrincipal.descricao AS \"categoriaDespesaPrincipal.descricao\" ");
		sb.append(" from categoriaDespesa ");
		sb.append(" left join categoriaDespesa categoriaDespesaPrincipal on categoriaDespesaPrincipal.codigo = categoriaDespesa.categoriaDespesaPrincipal ");
		return sb;
	}
	
	public StringBuilder getDadosCompletos() {
		StringBuilder sb = new StringBuilder();
		sb.append(" select categoriaDespesa.*, ");
		sb.append(" categoriaDespesaPrincipal.codigo AS \"categoriaDespesaPrincipal.codigo\", categoriaDespesaPrincipal.descricao AS \"categoriaDespesaPrincipal.descricao\" ");
		sb.append(" from categoriaDespesa ");
		sb.append(" left join categoriaDespesa categoriaDespesaPrincipal on categoriaDespesaPrincipal.codigo = categoriaDespesa.categoriaDespesaPrincipal ");
		return sb;
	}
	
	public void montarDadosBasicos(SqlRowSet dadosSQL, CategoriaDespesaVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		obj.setCodigo(dadosSQL.getInt("codigo"));
		obj.setDescricao(dadosSQL.getString("descricao"));
		obj.getCategoriaDespesaPrincipalVO().setCodigo(dadosSQL.getInt("categoriaDespesaPrincipal"));
		obj.getCategoriaDespesaPrincipalVO().setDescricao(dadosSQL.getString("categoriaDespesaPrincipal.descricao"));
		obj.setNovoObj(false);
	}
	
	public void montarDadosCompletos(SqlRowSet dadosSQL, CategoriaDespesaVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		obj.setCodigo(dadosSQL.getInt("codigo"));
		obj.setDescricao(dadosSQL.getString("descricao"));
		obj.getCategoriaDespesaPrincipalVO().setCodigo(dadosSQL.getInt("categoriaDespesaPrincipal.codigo"));
		obj.getCategoriaDespesaPrincipalVO().setDescricao(dadosSQL.getString("categoriaDespesaPrincipal.descricao"));
		obj.setNovoObj(false);
		if (nivelMontarDados.equals(NivelMontarDadosEnum.TELA_CONSULTA)) {
			return;
		}
		return;
	}

}
