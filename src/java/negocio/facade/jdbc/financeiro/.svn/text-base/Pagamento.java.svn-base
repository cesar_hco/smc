package negocio.facade.jdbc.financeiro;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.comuns.financeiro.ContaPagarPagamentoVO;
import negocio.comuns.financeiro.ContaPagarVO;
import negocio.comuns.financeiro.FormaPagamentoPagamentoVO;
import negocio.comuns.financeiro.FormaPagamentoVO;
import negocio.comuns.financeiro.PagamentoVO;
import negocio.comuns.financeiro.enumeradores.LocalizacaoChequeEnum;
import negocio.comuns.financeiro.enumeradores.OrigemExtratoContaCorrenteEnum;
import negocio.comuns.financeiro.enumeradores.SituacaoContaPagarEnum;
import negocio.comuns.financeiro.enumeradores.TipoFormaPagamentoEnum;
import negocio.comuns.financeiro.enumeradores.TipoMovimentacaoCaixaEnum;
import negocio.comuns.financeiro.enumeradores.TipoMovimentacaoExtratoContaCorrenteEnum;
import negocio.comuns.financeiro.enumeradores.TipoOrigemMovimentacaoCaixaEnum;
import negocio.comuns.utilitarias.Uteis;
import negocio.facade.jdbc.arquitetura.ControleAcesso;
import negocio.interfaces.financeiro.PagamentoInterfaceFacade;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Scope("singleton")
@Lazy
public class Pagamento extends ControleAcesso implements PagamentoInterfaceFacade {

	private static final long serialVersionUID = 1L;

	public Pagamento() {
		super();
	}

	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void validarDados(PagamentoVO obj, UsuarioVO usuarioVO) throws Exception {
		if (obj.getDataPagamento() == null) {
			throw new Exception("O campo DATA PAGAMENTO deve ser informado!");
		}
		if (obj.getResponsavelVO() == null || obj.getResponsavelVO().getCodigo().equals(0)) {
			throw new Exception("O campo RESPONS�VEL deve ser informado!");
		}
		if (obj.getListaContaPagarPagamentoVOs() == null || obj.getListaContaPagarPagamentoVOs().isEmpty()) {
			throw new Exception("Deve ser informado ao menos uma conta a pagar!");
		}
		if (obj.getListaFormaPagamentoPagamentoVOs() == null || obj.getListaFormaPagamentoPagamentoVOs().isEmpty()) {
			throw new Exception("Deve ser informado a FORMA DE PAGAMENTO!");
		}

		if (obj.getFornecedorVO() == null || obj.getFornecedorVO().getCodigo() == null || obj.getFornecedorVO().getCodigo().equals(0)) {
			obj.setFornecedorVO(null);
		}
		if (obj.getValorTotalContaPagar().compareTo(obj.getValorTotalPago()) > 0) {
            throw new Exception("N�o � poss�vel fazer um pagamento parcial.");
        }
	}

	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void executarPagamentoContaPagar(PagamentoVO pagamentoVO, UsuarioVO usuarioVO) throws Exception {
		try {
			for (ContaPagarPagamentoVO contaPagarPagamentoVO : pagamentoVO.getListaContaPagarPagamentoVOs()) {
				contaPagarPagamentoVO.getContaPagarVO().setSituacao(SituacaoContaPagarEnum.PAGO);
				contaPagarPagamentoVO.getContaPagarVO().setDataPagamento(pagamentoVO.getDataPagamento());
				getFacadeFactory().getContaPagarFacade().alterar(contaPagarPagamentoVO.getContaPagarVO(), usuarioVO);
			}
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void persistir(PagamentoVO obj, UsuarioVO usuarioVO) throws Exception {
		if (obj.getCodigo().equals(0)) {
			executarPagamentoContaPagar(obj, usuarioVO);
			executarCriacaoMovimentacaoCaixa(obj, TipoMovimentacaoCaixaEnum.SAIDA, usuarioVO);
			incluir(obj, usuarioVO);
		} else {
			alterar(obj, usuarioVO);
		}
	}

	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void incluir(final PagamentoVO obj, UsuarioVO usuario) throws Exception {
		try {
			validarDados(obj, usuario);
			final String sql = "INSERT INTO Pagamento( dataPagamento, fornecedor, contaCaixa, responsavel, empresa, valorTotalPagar, valorTotalJuro, valorTotalMulta, valorTotalDesconto, valorTotalPago, valorTotalContaPagar, estornado) "
					+ "VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) returning codigo";
			obj.setCodigo((Integer) getConexao().getJdbcTemplate().query(new PreparedStatementCreator() {

				public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
					PreparedStatement sqlInserir = arg0.prepareStatement(sql);
					sqlInserir.setTimestamp(1, Uteis.getDataJDBCTimestamp(obj.getDataPagamento()));
					if (!obj.getFornecedorVO().getCodigo().equals(0)) {
						sqlInserir.setInt(2, obj.getFornecedorVO().getCodigo());
					} else {
						sqlInserir.setNull(2, 0);
					}
					if (!obj.getContaCaixaVO().getCodigo().equals(0)) {
						sqlInserir.setInt(3, obj.getContaCaixaVO().getCodigo());
					} else {
						sqlInserir.setNull(3, 0);
					}
					if (!obj.getResponsavelVO().getCodigo().equals(0)) {
						sqlInserir.setInt(4, obj.getResponsavelVO().getCodigo());
					} else {
						sqlInserir.setNull(4, 0);
					}
					if (!obj.getEmpresaVO().getCodigo().equals(0)) {
						sqlInserir.setInt(5, obj.getEmpresaVO().getCodigo());
					} else {
						sqlInserir.setNull(5, 0);
					}
					sqlInserir.setBigDecimal(6, obj.getValorTotalPagar());
					sqlInserir.setBigDecimal(7, obj.getValorTotalJuro());
					sqlInserir.setBigDecimal(8, obj.getValorTotalMulta());
					sqlInserir.setBigDecimal(9, obj.getValorTotalDesconto());
					sqlInserir.setBigDecimal(10, obj.getValorTotalPago());
					sqlInserir.setBigDecimal(11, obj.getValorTotalContaPagar());
					sqlInserir.setBoolean(12, obj.getEstornado());
					return sqlInserir;
				}
			}, new ResultSetExtractor() {

				public Object extractData(ResultSet arg0) throws SQLException, DataAccessException {
					if (arg0.next()) {
						obj.setNovoObj(Boolean.FALSE);
						return arg0.getInt("codigo");
					}
					return null;
				}
			}));
			getFacadeFactory().getContaPagarPagamentoFacade().incluirContaPagarPagamentoVOs(obj, obj.getListaContaPagarPagamentoVOs(), usuario);
			getFacadeFactory().getFormaPagamentoPagamentoFacade().incluirFormaPagamentoPagamentoVOs(obj, obj.getListaFormaPagamentoPagamentoVOs(), usuario);
			obj.setNovoObj(Boolean.FALSE);
		} catch (Exception e) {
			obj.setNovoObj(Boolean.TRUE);
			throw e;
		}
	}

	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void alterar(final PagamentoVO obj, UsuarioVO usuario) throws Exception {
		try {
			validarDados(obj, usuario);
			final String sql = "UPDATE Pagamento set dataPagamento=?, fornecedor=?, contaCaixa=?, responsavel=?, empresa=?, valorTotalPagar=?, valorTotalJuro=?, valorTotalMulta=?, valorTotalDesconto=?, valorTotalPago=?, valorTotalContaPagar=?, estornado=? " + "" + "WHERE ((codigo = ?))";

			getConexao().getJdbcTemplate().update(new PreparedStatementCreator() {

				public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
					PreparedStatement sqlAlterar = arg0.prepareStatement(sql);
					sqlAlterar.setTimestamp(1, Uteis.getDataJDBCTimestamp(obj.getDataPagamento()));
					if (!obj.getFornecedorVO().getCodigo().equals(0)) {
						sqlAlterar.setInt(2, obj.getFornecedorVO().getCodigo());
					} else {
						sqlAlterar.setNull(2, 0);
					}
					if (!obj.getContaCaixaVO().getCodigo().equals(0)) {
						sqlAlterar.setInt(3, obj.getContaCaixaVO().getCodigo());
					} else {
						sqlAlterar.setNull(3, 0);
					}
					if (!obj.getResponsavelVO().getCodigo().equals(0)) {
						sqlAlterar.setInt(4, obj.getResponsavelVO().getCodigo());
					} else {
						sqlAlterar.setNull(4, 0);
					}
					if (!obj.getEmpresaVO().getCodigo().equals(0)) {
						sqlAlterar.setInt(5, obj.getEmpresaVO().getCodigo());
					} else {
						sqlAlterar.setNull(5, 0);
					}
					sqlAlterar.setBigDecimal(6, obj.getValorTotalPagar());
					sqlAlterar.setBigDecimal(7, obj.getValorTotalJuro());
					sqlAlterar.setBigDecimal(8, obj.getValorTotalMulta());
					sqlAlterar.setBigDecimal(9, obj.getValorTotalDesconto());
					sqlAlterar.setBigDecimal(10, obj.getValorTotalPago());
					sqlAlterar.setBigDecimal(11, obj.getValorTotalContaPagar());
					sqlAlterar.setBoolean(12, obj.getEstornado());
					sqlAlterar.setInt(13, obj.getCodigo().intValue());
					return sqlAlterar;
				}
			});
			getFacadeFactory().getContaPagarPagamentoFacade().alterarContaPagarPagamentoVOs(obj, obj.getListaContaPagarPagamentoVOs(), usuario);
			getFacadeFactory().getFormaPagamentoPagamentoFacade().alterarFormaPagamentoPagamentoVOs(obj, obj.getListaFormaPagamentoPagamentoVOs(), usuario);
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void excluir(PagamentoVO obj, UsuarioVO usuario) throws Exception {
		try {
			String sql = "DELETE FROM Pagamento WHERE ((codigo = ?))";
			getConexao().getJdbcTemplate().update(sql, new Object[] { obj.getCodigo() });
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public List<PagamentoVO> consultar(String campoConsulta, String valorConsulta, Date dataInicio, Date dataFim, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		List<PagamentoVO> listaConsulta = new ArrayList<PagamentoVO>(0);
		if (campoConsulta.equals("FORNECEDOR")) {
			listaConsulta = consultarPorFornecedor(valorConsulta, nivelMontarDados, usuarioVO);
		}
		if (campoConsulta.equals("DATA_PAGAMENTO")) {
			listaConsulta = consultarPorDataPagamento(dataInicio, dataFim, nivelMontarDados, usuarioVO);
		}
		if (campoConsulta.equals("EMPRESA")) {
			listaConsulta = consultarPorEmpresa(valorConsulta, nivelMontarDados, usuarioVO);
		}
		return listaConsulta;
	}

	public List<PagamentoVO> consultarPorDataPagamento(Date dataInicio, Date dataFim, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		StringBuilder sb = new StringBuilder();
		sb.append(getDadosBasicos());
		sb.append(" where pagamento.dataPagamento >= '").append(Uteis.getDataJDBC(dataInicio)).append("'  ");
		sb.append(" and pagamento.dataPagamento <= '").append(Uteis.getDataJDBC(dataFim)).append("'  ");
		sb.append(" order by pagamento.dataPagamento ");
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
		return montarDadosLista(tabelaResultado, nivelMontarDados, usuarioVO);
	}

	public List<PagamentoVO> consultarPorFornecedor(String nome, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		StringBuilder sb = new StringBuilder();
		sb.append(getDadosBasicos());
		sb.append(" where fornecedor.nome ilike ('").append(nome).append("%') ");
		sb.append(" order by fornecedor.nome ");
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
		return montarDadosLista(tabelaResultado, nivelMontarDados, usuarioVO);
	}

	public List<PagamentoVO> consultarPorEmpresa(String nome, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		StringBuilder sb = new StringBuilder();
		sb.append(getDadosBasicos());
		sb.append(" where fornecedor.nome ilike ('").append(nome).append("%') ");
		sb.append(" order by fornecedor.nome ");
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
		return montarDadosLista(tabelaResultado, nivelMontarDados, usuarioVO);
	}

	@Override
	public void carregarDados(PagamentoVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		if (nivelMontarDados.equals(NivelMontarDadosEnum.BASICO)) {
			StringBuilder sb = getDadosBasicos();
			sb.append(" where pagamento.codigo = ").append(obj.getCodigo());
			SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
			if (tabelaResultado.next()) {
				montarDadosBasicos(tabelaResultado, obj, nivelMontarDados, usuarioVO);
			}
		}
		if (nivelMontarDados.equals(NivelMontarDadosEnum.COMPLETO)) {
			StringBuilder sb = getDadosCompletos();
			sb.append(" where pagamento.codigo = ").append(obj.getCodigo());
			SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
			if (tabelaResultado.next()) {
				montarDadosCompletos(tabelaResultado, obj, nivelMontarDados, usuarioVO);
			}
		}
	}

	public List<PagamentoVO> montarDadosLista(SqlRowSet dadosSQL, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		List<PagamentoVO> listaMembroVOs = new ArrayList<PagamentoVO>(0);
		while (dadosSQL.next()) {
			PagamentoVO obj = new PagamentoVO();
			montarDadosBasicos(dadosSQL, obj, nivelMontarDados, usuarioVO);
			listaMembroVOs.add(obj);
		}
		return listaMembroVOs;
	}

	public StringBuilder getDadosBasicos() {
		StringBuilder sb = new StringBuilder();
		sb.append("select pagamento.*,  ");
		sb.append(" fornecedor.codigo AS \"fornecedor.codigo\", fornecedor.nome AS \"fornecedor.nome\", ");
		sb.append(" empresa.codigo AS \"empresa.codigo\", empresa.descricao AS \"empresa.descricao\", ");
		sb.append(" responsavel.codigo AS \"responsavel.codigo\", pessoa.codigo AS \"pessoa.codigo\", pessoa.nome AS \"pessoa.nome\" ");
		sb.append(" from pagamento ");
		sb.append(" left join fornecedor on fornecedor.codigo = pagamento.fornecedor ");
		sb.append(" left join empresa on empresa.codigo = pagamento.empresa ");
		sb.append(" left join usuario AS responsavel on responsavel.codigo = pagamento.responsavel ");
		sb.append(" left join pessoa on pessoa.codigo = responsavel.pessoa ");
		return sb;
	}

	public StringBuilder getDadosCompletos() {
		StringBuilder sb = new StringBuilder();
		sb.append("select pagamento.*,  ");
		sb.append(" fornecedor.codigo AS \"fornecedor.codigo\", fornecedor.nome AS \"fornecedor.nome\", ");
		sb.append(" empresa.codigo AS \"empresa.codigo\", empresa.descricao AS \"empresa.descricao\", ");
		sb.append(" responsavel.codigo AS \"responsavel.codigo\", pessoa.codigo AS \"pessoa.codigo\", pessoa.nome AS \"pessoa.nome\", ");
		sb.append(" contaCaixa.codigo AS \"contaCaixa.codigo\", contaCaixa.descricao AS \"contaCaixa.descricao\",  contaCaixa.contaCaixa AS \"contaCaixa.contaCaixa\" ");
		sb.append(" from pagamento ");
		sb.append(" left join fornecedor on fornecedor.codigo = pagamento.fornecedor ");
		sb.append(" left join empresa on empresa.codigo = pagamento.empresa ");
		sb.append(" left join usuario AS responsavel on responsavel.codigo = pagamento.responsavel ");
		sb.append(" left join pessoa on pessoa.codigo = responsavel.pessoa ");
		sb.append(" left join contaCorrente contaCaixa on contaCaixa.codigo = pagamento.contaCaixa ");
		return sb;
	}

	public void montarDadosBasicos(SqlRowSet dadosSQL, PagamentoVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		obj.setCodigo(dadosSQL.getInt("codigo"));
		obj.setDataPagamento(dadosSQL.getTimestamp("dataPagamento"));
		obj.setValorTotalContaPagar(dadosSQL.getBigDecimal("valorTotalContaPagar"));
		obj.setValorTotalPago(dadosSQL.getBigDecimal("valorTotalPago"));
		obj.setEstornado(dadosSQL.getBoolean("estornado"));
		obj.getFornecedorVO().setCodigo(dadosSQL.getInt("fornecedor.codigo"));
		obj.getFornecedorVO().setNome(dadosSQL.getString("fornecedor.nome"));
		obj.getResponsavelVO().setCodigo(dadosSQL.getInt("responsavel.codigo"));
		obj.getResponsavelVO().getPessoaVO().setCodigo(dadosSQL.getInt("pessoa.codigo"));
		obj.getResponsavelVO().getPessoaVO().setNome(dadosSQL.getString("pessoa.nome"));
		obj.getEmpresaVO().setCodigo(dadosSQL.getInt("empresa.codigo"));
		obj.getEmpresaVO().setDescricao(dadosSQL.getString("empresa.descricao"));
		obj.setNovoObj(false);
	}

	public void montarDadosCompletos(SqlRowSet dadosSQL, PagamentoVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		obj.setCodigo(dadosSQL.getInt("codigo"));
		obj.setDataPagamento(dadosSQL.getTimestamp("dataPagamento"));
		obj.setValorTotalPagar(dadosSQL.getBigDecimal("valorTotalPagar"));
		obj.setValorTotalJuro(dadosSQL.getBigDecimal("valorTotalJuro"));
		obj.setValorTotalMulta(dadosSQL.getBigDecimal("valorTotalMulta"));
		obj.setValorTotalDesconto(dadosSQL.getBigDecimal("valorTotalDesconto"));
		obj.setValorTotalPago(dadosSQL.getBigDecimal("valorTotalPago"));
		obj.setValorTotalContaPagar(dadosSQL.getBigDecimal("valorTotalContaPagar"));
		obj.setEstornado(dadosSQL.getBoolean("estornado"));

		obj.getFornecedorVO().setCodigo(dadosSQL.getInt("fornecedor.codigo"));
		obj.getFornecedorVO().setNome(dadosSQL.getString("fornecedor.nome"));

		obj.getEmpresaVO().setCodigo(dadosSQL.getInt("empresa.codigo"));
		obj.getEmpresaVO().setDescricao(dadosSQL.getString("empresa.descricao"));

		obj.getContaCaixaVO().setCodigo(dadosSQL.getInt("contaCaixa.codigo"));
		obj.getContaCaixaVO().setDescricao(dadosSQL.getString("contaCaixa.descricao"));
		obj.getContaCaixaVO().setContaCaixa(dadosSQL.getBoolean("contaCaixa.contaCaixa"));

		obj.getResponsavelVO().setCodigo(dadosSQL.getInt("responsavel.codigo"));
		obj.getResponsavelVO().getPessoaVO().setCodigo(dadosSQL.getInt("pessoa.codigo"));
		obj.getResponsavelVO().getPessoaVO().setNome(dadosSQL.getString("pessoa.nome"));
		obj.setListaContaPagarPagamentoVOs(getFacadeFactory().getContaPagarPagamentoFacade().consultarPorPagamento(obj.getCodigo(), NivelMontarDadosEnum.COMPLETO, usuarioVO));
		obj.setListaFormaPagamentoPagamentoVOs(getFacadeFactory().getFormaPagamentoPagamentoFacade().consultarPorPagamento(obj.getCodigo(), NivelMontarDadosEnum.COMPLETO, usuarioVO));
		obj.setNovoObj(false);
		if (nivelMontarDados.equals(NivelMontarDadosEnum.TELA_CONSULTA)) {
			return;
		}
		return;
	}

	@Override
	public void adicionarContaPagarPagamento(List<ContaPagarPagamentoVO> listaContaContaPagarPagamentoVOs, ContaPagarVO contaPagarVO, UsuarioVO usuarioVO) {
		ContaPagarPagamentoVO obj = new ContaPagarPagamentoVO();
		obj.setContaPagarVO(contaPagarVO);
		int index = 0;
		for (ContaPagarPagamentoVO contaPagarPagamentoVO : listaContaContaPagarPagamentoVOs) {
			if (contaPagarPagamentoVO.getContaPagarVO().getCodigo().equals(contaPagarVO.getCodigo())) {
				listaContaContaPagarPagamentoVOs.set(index, obj);
				return;
			}
			index++;
		}
		listaContaContaPagarPagamentoVOs.add(obj);
	}

	@Override
	public void calcularTotalPagamento(PagamentoVO pagamentoVO) {
		pagamentoVO.setValorTotalContaPagar(BigDecimal.ZERO);
		pagamentoVO.setValorTotalDesconto(BigDecimal.ZERO);
		pagamentoVO.setValorTotalPagar(BigDecimal.ZERO);
		pagamentoVO.setValorTotalJuro(BigDecimal.ZERO);
		pagamentoVO.setValorTotalMulta(BigDecimal.ZERO);
		for (ContaPagarPagamentoVO contaPagarPagamentoVO : pagamentoVO.getListaContaPagarPagamentoVOs()) {
			pagamentoVO.setValorTotalDesconto(pagamentoVO.getValorTotalDesconto().add(contaPagarPagamentoVO.getContaPagarVO().getDesconto()));
			pagamentoVO.setValorTotalContaPagar(pagamentoVO.getValorTotalContaPagar().add(contaPagarPagamentoVO.getContaPagarVO().getValor()));
			pagamentoVO.setValorTotalJuro(pagamentoVO.getValorTotalJuro().add(contaPagarPagamentoVO.getContaPagarVO().getJuro()));
			pagamentoVO.setValorTotalMulta(pagamentoVO.getValorTotalMulta().add(contaPagarPagamentoVO.getContaPagarVO().getMulta()));
			pagamentoVO.setValorTotalPagar(pagamentoVO.getValorTotalPagar().add(contaPagarPagamentoVO.getContaPagarVO().getValorTotalCalculado()));
		}
	}

	@Override
	public void removerContaPagarPagamento(List<ContaPagarPagamentoVO> listaContaContaPagarPagamentoVOs, ContaPagarPagamentoVO obj) {
		int index = 0;
		for (ContaPagarPagamentoVO contaPagarPagamentoVO : listaContaContaPagarPagamentoVOs) {
			if (contaPagarPagamentoVO.getContaPagarVO().getCodigo().equals(obj.getContaPagarVO().getCodigo())) {
				listaContaContaPagarPagamentoVOs.remove(index);
				return;
			}
			index++;
		}
	}
	
	public void validarDadosAdicionarFormaPagamento(PagamentoVO pagamentoVO, FormaPagamentoPagamentoVO obj, FormaPagamentoPagamentoVO formaPagamentoPagamentoIncluirVO, UsuarioVO usuarioVO) throws Exception {
		if (obj.getFormaPagamentoVO().getCodigo().equals(formaPagamentoPagamentoIncluirVO.getFormaPagamentoVO().getCodigo()) && formaPagamentoPagamentoIncluirVO.getFormaPagamentoVO().getIsTipoCheque() && obj.getFormaPagamentoVO().getIsTipoCheque() && obj.getChequeVO().getChequeProprio() && formaPagamentoPagamentoIncluirVO.getChequeVO().getChequeProprio() && formaPagamentoPagamentoIncluirVO.getChequeVO().getContaCorrenteChequeProprioVO().getCodigo().equals(obj.getChequeVO().getContaCorrenteChequeProprioVO().getCodigo()) && formaPagamentoPagamentoIncluirVO.getChequeVO().getNumero().trim().equalsIgnoreCase(obj.getChequeVO().getNumero().trim())) {
			throw new Exception("Item j� Adicionado");
		} else if (obj.getFormaPagamentoVO().getCodigo().equals(formaPagamentoPagamentoIncluirVO.getFormaPagamentoVO().getCodigo()) && formaPagamentoPagamentoIncluirVO.getFormaPagamentoVO().getIsTipoCheque() && obj.getFormaPagamentoVO().getIsTipoCheque() && !obj.getChequeVO().getChequeProprio() && !formaPagamentoPagamentoIncluirVO.getChequeVO().getChequeProprio() && formaPagamentoPagamentoIncluirVO.getChequeVO().getContaCorrenteChequeProprioVO().getCodigo().equals(obj.getChequeVO().getContaCorrenteChequeProprioVO().getCodigo()) && formaPagamentoPagamentoIncluirVO.getChequeVO().getNumero().equals(obj.getChequeVO().getNumero())) {
			throw new Exception("Item j� Adicionado");
		} else if (obj.getFormaPagamentoVO().getCodigo().equals(formaPagamentoPagamentoIncluirVO.getFormaPagamentoVO().getCodigo()) && formaPagamentoPagamentoIncluirVO.getFormaPagamentoVO().getIsTipoDinheiro() && obj.getFormaPagamentoVO().getIsTipoDinheiro()) {
			obj.setValor(obj.getValor().add(formaPagamentoPagamentoIncluirVO.getValor()));
			calcularTotalPago(pagamentoVO);
			return;
		} else if (obj.getFormaPagamentoVO().getCodigo().equals(formaPagamentoPagamentoIncluirVO.getFormaPagamentoVO().getCodigo()) && formaPagamentoPagamentoIncluirVO.getFormaPagamentoVO().getIsTipoBoletoBancario() && obj.getFormaPagamentoVO().getIsTipoBoletoBancario() && obj.getContaCorrenteVO().getCodigo().equals(formaPagamentoPagamentoIncluirVO.getContaCorrenteVO().getCodigo())) {
			obj.setValor(obj.getValor().add(formaPagamentoPagamentoIncluirVO.getValor()));
			calcularTotalPago(pagamentoVO);
			return;
		}
		if (formaPagamentoPagamentoIncluirVO.getFormaPagamentoVO().getIsTipoCheque() && formaPagamentoPagamentoIncluirVO.getChequeVO().getNumero().trim().equals("")) {
			throw new Exception("O campo N�MERO DO CHEQUE � obrigat�rio.");
		}
	}
	
	public FormaPagamentoPagamentoVO inicializarDadosFormaPagamentoPagamento(FormaPagamentoPagamentoVO formaPagamentoPagamentoIncluirVO, UsuarioVO usuarioVO) {
		FormaPagamentoPagamentoVO obj = new FormaPagamentoPagamentoVO();
		obj.setFormaPagamentoVO(formaPagamentoPagamentoIncluirVO.getFormaPagamentoVO());
		obj.setValor(formaPagamentoPagamentoIncluirVO.getValor());
		obj.setContaCorrenteVO(formaPagamentoPagamentoIncluirVO.getContaCorrenteVO());
		obj.setChequeVO(formaPagamentoPagamentoIncluirVO.getChequeVO());
		return obj;
	}

	@Override
	public void adicionarFormaPagamento(PagamentoVO pagamentoVO, List<FormaPagamentoPagamentoVO> listaFormaPagamentoPagamentoVOs, FormaPagamentoPagamentoVO formaPagamentoPagamentoIncluirVO, UsuarioVO usuarioVO) throws Exception {
		if (!formaPagamentoPagamentoIncluirVO.getContaCorrenteVO().getCodigo().equals(0)) {
			getFacadeFactory().getContaCorrenteFacade().carregarDados(formaPagamentoPagamentoIncluirVO.getContaCorrenteVO(), NivelMontarDadosEnum.COMPLETO, usuarioVO);
		}
		getFacadeFactory().getChequeFacade().inicializarDadosChequeProprio(pagamentoVO, formaPagamentoPagamentoIncluirVO, usuarioVO);
		FormaPagamentoPagamentoVO obj = inicializarDadosFormaPagamentoPagamento(formaPagamentoPagamentoIncluirVO, usuarioVO);
		int index = 0;
		for (FormaPagamentoPagamentoVO formaPagamentoPagamentoVO : listaFormaPagamentoPagamentoVOs) {
			validarDadosAdicionarFormaPagamento(pagamentoVO, formaPagamentoPagamentoVO, formaPagamentoPagamentoIncluirVO, usuarioVO);
			if (formaPagamentoPagamentoVO.getFormaPagamentoVO().getCodigo().equals(formaPagamentoPagamentoIncluirVO.getFormaPagamentoVO().getCodigo()) && formaPagamentoPagamentoVO.getFormaPagamentoVO().getIsTipoDinheiro() && formaPagamentoPagamentoIncluirVO.getFormaPagamentoVO().getIsTipoDinheiro()) {
				formaPagamentoPagamentoVO.setValor(formaPagamentoPagamentoVO.getValor().add(formaPagamentoPagamentoIncluirVO.getValor()));
				calcularTotalPago(pagamentoVO);
				return;
			}
			if (formaPagamentoPagamentoVO.getFormaPagamentoVO().getCodigo().equals(formaPagamentoPagamentoIncluirVO.getFormaPagamentoVO().getCodigo())) {
				listaFormaPagamentoPagamentoVOs.set(index, obj);
				return;
			}
			index++;
		}
		listaFormaPagamentoPagamentoVOs.add(obj);
		calcularTotalPago(pagamentoVO);
	}

	public void calcularTotalPago(PagamentoVO pagamentoVO) {
		pagamentoVO.setValorTotalPago(BigDecimal.ZERO);
		for (FormaPagamentoPagamentoVO formaPagamentoPagamentoVO : pagamentoVO.getListaFormaPagamentoPagamentoVOs()) {
			pagamentoVO.setValorTotalPago(pagamentoVO.getValorTotalPago().add(formaPagamentoPagamentoVO.getValor()));
		}
	}

	@Override
	public void removerFormaPagamento(List<FormaPagamentoPagamentoVO> listaFormaPagamentoPagamentoVOs, FormaPagamentoPagamentoVO obj) {
		int index = 0;
		for (FormaPagamentoPagamentoVO formaPagamentoPagamentoVO : listaFormaPagamentoPagamentoVOs) {
			if (formaPagamentoPagamentoVO.getFormaPagamentoVO().getCodigo().equals(obj.getFormaPagamentoVO().getCodigo())) {
				listaFormaPagamentoPagamentoVOs.remove(index);
				return;
			}
			index++;
		}
	}

	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void executarCriacaoMovimentacaoCaixa(PagamentoVO pagamentoVO, TipoMovimentacaoCaixaEnum tipoMovimentacaoCaixaEnum, UsuarioVO usuarioVO) throws Exception {
		TipoOrigemMovimentacaoCaixaEnum tipoOrigemMovimentacaoCaixaEnum = null;
		if (tipoMovimentacaoCaixaEnum.equals(TipoMovimentacaoCaixaEnum.ENTRADA)) {
			tipoOrigemMovimentacaoCaixaEnum = TipoOrigemMovimentacaoCaixaEnum.PAGAMENTO_ESTORNO;
		} else if (tipoMovimentacaoCaixaEnum.equals(TipoMovimentacaoCaixaEnum.SAIDA)) {
			tipoOrigemMovimentacaoCaixaEnum = TipoOrigemMovimentacaoCaixaEnum.PAGAMENTO;
		}
		for (FormaPagamentoPagamentoVO formaPagamentoPagamentoVO : pagamentoVO.getListaFormaPagamentoPagamentoVOs()) {
			if (formaPagamentoPagamentoVO.getFormaPagamentoVO().getIsTipoDinheiro()) {
				getFacadeFactory().getMovimentacaoCaixaItemFacade().criarMovimentacaoCaixaItem(tipoMovimentacaoCaixaEnum, pagamentoVO.getCedenteVO(), pagamentoVO.getCodigoCedente(), pagamentoVO.getContaCaixaVO(), formaPagamentoPagamentoVO.getFormaPagamentoVO(), null, formaPagamentoPagamentoVO.getValor(), tipoOrigemMovimentacaoCaixaEnum, formaPagamentoPagamentoVO.getContaCorrenteVO(), pagamentoVO.getCodigo(), usuarioVO);
			} else if (formaPagamentoPagamentoVO.getFormaPagamentoVO().getIsTipoCheque()) {
				if (!formaPagamentoPagamentoVO.getChequeVO().getChequeProprio()) {
					getFacadeFactory().getMovimentacaoCaixaItemFacade().criarMovimentacaoCaixaItem(tipoMovimentacaoCaixaEnum, pagamentoVO.getCedenteVO(), pagamentoVO.getCodigoCedente(), pagamentoVO.getContaCaixaVO(), formaPagamentoPagamentoVO.getFormaPagamentoVO(), formaPagamentoPagamentoVO.getChequeVO(), formaPagamentoPagamentoVO.getValor(), tipoOrigemMovimentacaoCaixaEnum, formaPagamentoPagamentoVO.getContaCorrenteVO(), pagamentoVO.getCodigo(), usuarioVO);
				}
				if (formaPagamentoPagamentoVO.getChequeVO().getChequeProprio() && tipoMovimentacaoCaixaEnum.equals(TipoMovimentacaoCaixaEnum.SAIDA)) {
					formaPagamentoPagamentoVO.getChequeVO().setLocalizacao(LocalizacaoChequeEnum.PAGAMENTO);
					formaPagamentoPagamentoVO.getChequeVO().setLocalizacaoChequeVO(formaPagamentoPagamentoVO.getContaCorrenteVO());
					getFacadeFactory().getChequeFacade().persistir(formaPagamentoPagamentoVO.getChequeVO(), usuarioVO);
				} else if (!formaPagamentoPagamentoVO.getChequeVO().getChequeProprio() && tipoMovimentacaoCaixaEnum.equals(TipoMovimentacaoCaixaEnum.ENTRADA)) {
					formaPagamentoPagamentoVO.getChequeVO().setLocalizacao(LocalizacaoChequeEnum.EM_CAIXA);
					formaPagamentoPagamentoVO.getChequeVO().setLocalizacaoChequeVO(pagamentoVO.getContaCaixaVO());
					getFacadeFactory().getChequeFacade().persistir(formaPagamentoPagamentoVO.getChequeVO(), usuarioVO);
				} else if (formaPagamentoPagamentoVO.getChequeVO().getChequeProprio() && tipoMovimentacaoCaixaEnum.equals(TipoMovimentacaoCaixaEnum.ENTRADA)) {
					formaPagamentoPagamentoVO.getChequeVO().setLocalizacao(LocalizacaoChequeEnum.ESTORNADO);
					formaPagamentoPagamentoVO.getChequeVO().setLocalizacaoChequeVO(null);
					getFacadeFactory().getChequeFacade().persistir(formaPagamentoPagamentoVO.getChequeVO(), usuarioVO);
				}
			} else if (!formaPagamentoPagamentoVO.getFormaPagamentoVO().getIsTipoDinheiro() && !formaPagamentoPagamentoVO.getFormaPagamentoVO().getIsTipoCheque()
					&& !formaPagamentoPagamentoVO.getContaCorrenteVO().getCodigo().equals(0)) {
				
					getFacadeFactory().getContaCorrenteFacade().movimentarSaldoContaCorrente(formaPagamentoPagamentoVO.getContaCorrenteVO().getCodigo(), tipoMovimentacaoCaixaEnum, formaPagamentoPagamentoVO.getValor(), usuarioVO);
				
				if (tipoMovimentacaoCaixaEnum.equals(TipoMovimentacaoCaixaEnum.ENTRADA)) {
					getFacadeFactory().getExtratoContaCorrenteFacade().executarCriacaoExtratoContaCorrente(pagamentoVO.getDataPagamento(), formaPagamentoPagamentoVO.getValor(), pagamentoVO.getEmpresaVO(), formaPagamentoPagamentoVO.getContaCorrenteVO(), formaPagamentoPagamentoVO.getFormaPagamentoVO(), formaPagamentoPagamentoVO.getCodigo(), OrigemExtratoContaCorrenteEnum.PAGAMENTO, TipoMovimentacaoExtratoContaCorrenteEnum.ENTRADA, null, pagamentoVO.getNomeCedente(), pagamentoVO.getCodigoCedente(), pagamentoVO.getCedente(), usuarioVO);
				} else {
					getFacadeFactory().getExtratoContaCorrenteFacade().executarCriacaoExtratoContaCorrente(pagamentoVO.getDataPagamento(), formaPagamentoPagamentoVO.getValor(), pagamentoVO.getEmpresaVO(), formaPagamentoPagamentoVO.getContaCorrenteVO(), formaPagamentoPagamentoVO.getFormaPagamentoVO(), formaPagamentoPagamentoVO.getCodigo(), OrigemExtratoContaCorrenteEnum.PAGAMENTO, TipoMovimentacaoExtratoContaCorrenteEnum.SAIDA, null, pagamentoVO.getNomeCedente(), pagamentoVO.getCodigoCedente(), pagamentoVO.getCedente(), usuarioVO);
				}
			} else {
				throw new Exception("Forma de Recebimento n�o implementada procure o administrador do sistema...");
			}
		}
		if (pagamentoVO.getTroco() != null && pagamentoVO.getTroco().compareTo(BigDecimal.ZERO) > 0 && tipoMovimentacaoCaixaEnum.equals(TipoMovimentacaoCaixaEnum.ENTRADA)) {
			FormaPagamentoVO formaPagamentoVO = getFacadeFactory().getFormaPagamentoFacade().consultarPorTipoPagamentoUnico(TipoFormaPagamentoEnum.DINHEIRO, usuarioVO);
			getFacadeFactory().getMovimentacaoCaixaItemFacade().criarMovimentacaoCaixaItem(TipoMovimentacaoCaixaEnum.ENTRADA, pagamentoVO.getCedenteVO(), pagamentoVO.getCodigoCedente(), pagamentoVO.getContaCaixaVO(), formaPagamentoVO, null, pagamentoVO.getTroco(), TipoOrigemMovimentacaoCaixaEnum.PAGAMENTO_TROCO, null, pagamentoVO.getCodigo(), usuarioVO);

		}
		if (pagamentoVO.getTroco() != null && pagamentoVO.getTroco().compareTo(BigDecimal.ZERO) > 0 && tipoMovimentacaoCaixaEnum.equals(TipoMovimentacaoCaixaEnum.SAIDA)) {
			FormaPagamentoVO formaPagamentoVO = getFacadeFactory().getFormaPagamentoFacade().consultarPorTipoPagamentoUnico(TipoFormaPagamentoEnum.DINHEIRO, usuarioVO);
			getFacadeFactory().getMovimentacaoCaixaItemFacade().criarMovimentacaoCaixaItem(TipoMovimentacaoCaixaEnum.SAIDA, pagamentoVO.getCedenteVO(), pagamentoVO.getCodigoCedente(), pagamentoVO.getContaCaixaVO(), formaPagamentoVO, null, pagamentoVO.getTroco(), TipoOrigemMovimentacaoCaixaEnum.PAGAMENTO_ESTORNO, null, pagamentoVO.getCodigo(), usuarioVO);

		}
	}

	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void estornarPagamento(PagamentoVO pagamentoVO, UsuarioVO usuarioVO) throws Exception {
		try {
			getFacadeFactory().getFormaPagamentoPagamentoFacade().estornarPagamentoFormaPagamento(pagamentoVO, usuarioVO);
			getFacadeFactory().getContaPagarPagamentoFacade().executarEstornoPagamentoContaPagar(pagamentoVO, usuarioVO);
			executarCriacaoMovimentacaoCaixa(pagamentoVO, TipoMovimentacaoCaixaEnum.ENTRADA, usuarioVO);
			pagamentoVO.setEstornado(Boolean.TRUE);
			this.persistir(pagamentoVO, usuarioVO);
		} catch (Exception e) {
			pagamentoVO.setEstornado(Boolean.FALSE);
			throw e;
		}
	}

}
