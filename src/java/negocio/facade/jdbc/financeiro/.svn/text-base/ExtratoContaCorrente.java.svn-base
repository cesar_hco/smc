package negocio.facade.jdbc.financeiro;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import negocio.comuns.administrativo.EmpresaVO;
import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.comuns.financeiro.ChequeVO;
import negocio.comuns.financeiro.ContaCorrenteVO;
import negocio.comuns.financeiro.ExtratoContaCorrenteVO;
import negocio.comuns.financeiro.FormaPagamentoVO;
import negocio.comuns.financeiro.enumeradores.CedenteEnum;
import negocio.comuns.financeiro.enumeradores.OrigemExtratoContaCorrenteEnum;
import negocio.comuns.financeiro.enumeradores.TipoMovimentacaoExtratoContaCorrenteEnum;
import negocio.comuns.utilitarias.Uteis;
import negocio.facade.jdbc.arquitetura.ControleAcesso;
import negocio.interfaces.financeiro.ExtratoContaCorrenteInterfaceFacade;

@Repository
@Scope("singleton")
@Lazy
public class ExtratoContaCorrente extends ControleAcesso implements ExtratoContaCorrenteInterfaceFacade {

	private static final long serialVersionUID = 1L;

	public ExtratoContaCorrente() {
		super();
	}

	public void validarDados(ExtratoContaCorrenteVO obj, UsuarioVO usuarioVO) throws Exception {
	}

	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void persistir(ExtratoContaCorrenteVO obj, UsuarioVO usuarioVO) throws Exception {
		if (obj.getCodigo().equals(0)) {
			incluir(obj, usuarioVO);
		} else {
			alterar(obj, usuarioVO);
		}
	}

	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void incluir(final ExtratoContaCorrenteVO obj, UsuarioVO usuario) throws Exception {
		try {
			validarDados(obj, usuario);
			final String sql = "INSERT INTO ExtratoContaCorrente( data, valor, empresa, cheque, contaCorrente, formaPagamento, codigoOrigem, origemExtratoContaCorrente, tipoMovimentacaoExtratoContaCorrente, nomeSacado, codigoSacado, cedente ) "
					+ "" + " VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) returning codigo";
			obj.setCodigo((Integer) getConexao().getJdbcTemplate().query(new PreparedStatementCreator() {

				public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
					PreparedStatement sqlInserir = arg0.prepareStatement(sql);
					sqlInserir.setTimestamp(1, Uteis.getDataJDBCTimestamp(obj.getData()));
					sqlInserir.setBigDecimal(2, obj.getValor());
					if (!obj.getEmpresaVO().getCodigo().equals(0)) {
						sqlInserir.setInt(3, obj.getEmpresaVO().getCodigo());
					} else {
						sqlInserir.setNull(3, 0);
					}
					if (!obj.getChequeVO().getCodigo().equals(0)) {
						sqlInserir.setInt(4, obj.getChequeVO().getCodigo());
					} else {
						sqlInserir.setNull(4, 0);
					}
					if (!obj.getContaCorrenteVO().getCodigo().equals(0)) {
						sqlInserir.setInt(5, obj.getContaCorrenteVO().getCodigo());
					} else {
						sqlInserir.setNull(5, 0);
					}
					if (!obj.getFormaPagamentoVO().getCodigo().equals(0)) {
						sqlInserir.setInt(6, obj.getFormaPagamentoVO().getCodigo());
					} else {
						sqlInserir.setNull(6, 0);
					}
					sqlInserir.setInt(7, obj.getCodigoOrigem());
					sqlInserir.setString(8, obj.getOrigemExtratoContaCorrente().name());
					sqlInserir.setString(9, obj.getTipoMovimentacaoExtratoContaCorrente().name());
					sqlInserir.setString(10, obj.getNomeSacado());
					sqlInserir.setInt(11, obj.getCodigoSacado());
					sqlInserir.setString(12, obj.getCedenteEnum().name());
					return sqlInserir;
				}
			}, new ResultSetExtractor() {

				public Object extractData(ResultSet arg0) throws SQLException, DataAccessException {
					if (arg0.next()) {
						obj.setNovoObj(Boolean.FALSE);
						return arg0.getInt("codigo");
					}
					return null;
				}
			}));

			obj.setNovoObj(Boolean.FALSE);
		} catch (Exception e) {
			obj.setNovoObj(Boolean.TRUE);
			throw e;
		}
	}

	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void alterar(final ExtratoContaCorrenteVO obj, UsuarioVO usuario) throws Exception {
		try {
			validarDados(obj, usuario);
			final String sql = "UPDATE ExtratoContaCorrente set data=?, valor=?, empresa=?, cheque=?, contaCorrente=?, formaPagamento=?, codigoOrigem=?, origemExtratoContaCorrente=?, tipoMovimentacaoExtratoContaCorrente=?, nomeSacado=?, codigoSacado=?, cedente=? "
					+ " WHERE ((codigo = ?))";

			getConexao().getJdbcTemplate().update(new PreparedStatementCreator() {

				public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
					PreparedStatement sqlAlterar = arg0.prepareStatement(sql);
					sqlAlterar.setTimestamp(1, Uteis.getDataJDBCTimestamp(obj.getData()));
					sqlAlterar.setBigDecimal(2, obj.getValor());
					if (!obj.getEmpresaVO().getCodigo().equals(0)) {
						sqlAlterar.setInt(3, obj.getEmpresaVO().getCodigo());
					} else {
						sqlAlterar.setNull(3, 0);
					}
					if (!obj.getChequeVO().getCodigo().equals(0)) {
						sqlAlterar.setInt(4, obj.getChequeVO().getCodigo());
					} else {
						sqlAlterar.setNull(4, 0);
					}
					if (!obj.getContaCorrenteVO().getCodigo().equals(0)) {
						sqlAlterar.setInt(5, obj.getContaCorrenteVO().getCodigo());
					} else {
						sqlAlterar.setNull(5, 0);
					}
					if (!obj.getFormaPagamentoVO().getCodigo().equals(0)) {
						sqlAlterar.setInt(6, obj.getFormaPagamentoVO().getCodigo());
					} else {
						sqlAlterar.setNull(6, 0);
					}
					sqlAlterar.setInt(7, obj.getCodigoOrigem());
					sqlAlterar.setString(8, obj.getOrigemExtratoContaCorrente().name());
					sqlAlterar.setString(9, obj.getTipoMovimentacaoExtratoContaCorrente().name());
					sqlAlterar.setString(10, obj.getNomeSacado());
					sqlAlterar.setInt(11, obj.getCodigoSacado());
					sqlAlterar.setString(12, obj.getCedenteEnum().name());
					sqlAlterar.setInt(13, obj.getCodigo().intValue());
					return sqlAlterar;
				}
			});

		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void excluir(ExtratoContaCorrenteVO obj, UsuarioVO usuario) throws Exception {
		try {
			String sql = "DELETE FROM ExtratoContaCorrente WHERE ((codigo = ?))";
			getConexao().getJdbcTemplate().update(sql, new Object[] { obj.getCodigo() });
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public List<ExtratoContaCorrenteVO> consultar(String campoConsulta, String valorConsulta, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		List<ExtratoContaCorrenteVO> listaConsulta = new ArrayList<ExtratoContaCorrenteVO>(0);
		if (campoConsulta.equals("NUMERO_CONTA_CORRENTE")) {
			listaConsulta = consultarPorNumeroExtratoContaCorrente(valorConsulta, nivelMontarDados, usuarioVO);
		}
		if (campoConsulta.equals("DESCRICAO")) {
			listaConsulta = consultarPorDescricao(valorConsulta, nivelMontarDados, usuarioVO);
		}
		return listaConsulta;
	}

	public List<ExtratoContaCorrenteVO> consultarPorDescricao(String nome, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		StringBuilder sb = new StringBuilder();
		sb.append(getDadosBasicos());
		sb.append(" where extratoContaCorrente.descricao ilike ('").append(nome).append("%') ");
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
		return montarDadosLista(tabelaResultado, nivelMontarDados, usuarioVO);
	}

	public List<ExtratoContaCorrenteVO> consultarPorNumeroExtratoContaCorrente(String nome, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		StringBuilder sb = new StringBuilder();
		sb.append(getDadosBasicos());
		sb.append(" where extratoContaCorrente.numero ilike ('").append(nome).append("%') ");
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
		return montarDadosLista(tabelaResultado, nivelMontarDados, usuarioVO);
	}

	@Override
	public void carregarDados(ExtratoContaCorrenteVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		if (nivelMontarDados.equals(NivelMontarDadosEnum.BASICO)) {
			StringBuilder sb = getDadosBasicos();
			sb.append(" where extratoContaCorrente.codigo = ").append(obj.getCodigo());
			SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
			if (tabelaResultado.next()) {
				montarDadosBasicos(tabelaResultado, obj, nivelMontarDados, usuarioVO);
			}
		}
		if (nivelMontarDados.equals(NivelMontarDadosEnum.COMPLETO)) {
			StringBuilder sb = getDadosCompletos();
			sb.append(" where extratoContaCorrente.codigo = ").append(obj.getCodigo());
			SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
			if (tabelaResultado.next()) {
				montarDadosCompletos(tabelaResultado, obj, nivelMontarDados, usuarioVO);
			}
		}
	}

	public List<ExtratoContaCorrenteVO> montarDadosLista(SqlRowSet dadosSQL, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		List<ExtratoContaCorrenteVO> listaMembroVOs = new ArrayList<ExtratoContaCorrenteVO>(0);
		while (dadosSQL.next()) {
			ExtratoContaCorrenteVO obj = new ExtratoContaCorrenteVO();
			montarDadosBasicos(dadosSQL, obj, nivelMontarDados, usuarioVO);
			listaMembroVOs.add(obj);
		}
		return listaMembroVOs;
	}

	public StringBuilder getDadosBasicos() {
		StringBuilder sb = new StringBuilder();
		sb.append("select * from extratoContaCorrente ");
		return sb;
	}

	public StringBuilder getDadosCompletos() {
		StringBuilder sb = new StringBuilder();
		sb.append("select * from extratoContaCorrente ");
		return sb;
	}

	public void montarDadosBasicos(SqlRowSet dadosSQL, ExtratoContaCorrenteVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		obj.setCodigo(dadosSQL.getInt("codigo"));
		obj.setNovoObj(false);
	}

	public void montarDadosCompletos(SqlRowSet dadosSQL, ExtratoContaCorrenteVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		obj.setCodigo(dadosSQL.getInt("codigo"));
		obj.setNovoObj(false);
		if (nivelMontarDados.equals(NivelMontarDadosEnum.TELA_CONSULTA)) {
			return;
		}
		return;
	}

	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void executarCriacaoExtratoContaCorrente(Date data, BigDecimal valor, EmpresaVO empresaVO, ContaCorrenteVO contaCorrenteVO, FormaPagamentoVO formaPagamentoVO, Integer codigoOrigem, OrigemExtratoContaCorrenteEnum origemExtratoContaCorrente, TipoMovimentacaoExtratoContaCorrenteEnum tipoMovimentacaoExtratoContaCorrente, ChequeVO chequeVO, String nomeSacado, Integer codigoSacado, CedenteEnum cedenteEnum, UsuarioVO usuarioVO) throws Exception {
		ExtratoContaCorrenteVO extratoContaCorrenteVO = new ExtratoContaCorrenteVO(data, valor, empresaVO, contaCorrenteVO, formaPagamentoVO, codigoOrigem, origemExtratoContaCorrente, tipoMovimentacaoExtratoContaCorrente, chequeVO, nomeSacado, codigoSacado, cedenteEnum);
		getFacadeFactory().getExtratoContaCorrenteFacade().persistir(extratoContaCorrenteVO, usuarioVO);

	}

	
}
