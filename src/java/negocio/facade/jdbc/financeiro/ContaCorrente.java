package negocio.facade.jdbc.financeiro;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.comuns.financeiro.ContaCorrenteVO;
import negocio.comuns.financeiro.enumeradores.TipoMovimentacaoCaixaEnum;
import negocio.facade.jdbc.arquitetura.ControleAcesso;
import negocio.interfaces.financeiro.ContaCorrenteInterfaceFacade;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Scope("singleton")
@Lazy
public class ContaCorrente extends ControleAcesso implements ContaCorrenteInterfaceFacade {

	private static final long serialVersionUID = 1L;

	public ContaCorrente() {
		super();
	}

	public void validarDados(ContaCorrenteVO obj, UsuarioVO usuarioVO) throws Exception {
		if (obj.getContaCaixa()) {
			if (obj.getDescricao().equals("")) {
				throw new Exception("O campo DESCRI��O deve ser informado!");
			}
		} else {
			if (obj.getNumero().equals("")) {
				throw new Exception("O campo N�MERO deve ser informado!");
			}
			if (obj.getDigitoContaCorrente().equals("")) {
				throw new Exception("O campo D�GITO CONTA deve ser informado!");
			}
			if (obj.getAgencia().equals("")) {
				throw new Exception("O campo AG�NCIA deve ser informado!");
			}
			if (obj.getDigitoAgencia().equals("")) {
				throw new Exception("O campo D�GITO AG�NCIA deve ser informado!");
			}
			if (obj.getBanco().equals("")) {
				throw new Exception("O campo BANCO deve ser informado!");
			}
			if (obj.getNrBanco().equals("")) {
				throw new Exception("O campo NR.BANCO deve ser informado!");
			}
			
		}
	}

	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void persistir(ContaCorrenteVO obj, UsuarioVO usuarioVO) throws Exception {
		if (obj.isNovoObj()) {
			incluir(obj, usuarioVO);
		} else {
			alterar(obj, usuarioVO);
		}
	}

	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void incluir(final ContaCorrenteVO obj, UsuarioVO usuario) throws Exception {
		try {
			validarDados(obj, usuario);
			final String sql = "INSERT INTO ContaCorrente( descricao, digitoContaCorrente, numero, saldo, carteira, cedente, convenio, "
					+ "banco, nrBanco, agencia, digitoAgencia, contaCaixa) "
					+ " VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) returning codigo";
			obj.setCodigo((Integer) getConexao().getJdbcTemplate().query(new PreparedStatementCreator() {

				public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
					PreparedStatement sqlInserir = arg0.prepareStatement(sql);
					sqlInserir.setString(1, obj.getDescricao());
					sqlInserir.setString(2, obj.getDigitoContaCorrente());
					sqlInserir.setString(3, obj.getNumero());
					sqlInserir.setBigDecimal(4, obj.getSaldo());
					sqlInserir.setString(5, obj.getCarteira());
					sqlInserir.setString(6, obj.getCedente());
					sqlInserir.setString(7, obj.getConvenio());
					sqlInserir.setString(8, obj.getBanco());
					sqlInserir.setString(9, obj.getNrBanco());
					sqlInserir.setString(10, obj.getAgencia());
					sqlInserir.setString(11, obj.getDigitoAgencia());
					sqlInserir.setBoolean(12, obj.getContaCaixa());
					return sqlInserir;
				}
			}, new ResultSetExtractor() {

				public Object extractData(ResultSet arg0) throws SQLException, DataAccessException {
					if (arg0.next()) {
						obj.setNovoObj(Boolean.FALSE);
						return arg0.getInt("codigo");
					}
					return null;
				}
			}));
			
			obj.setNovoObj(Boolean.FALSE);
		} catch (Exception e) {
			obj.setNovoObj(Boolean.TRUE);
			throw e;
		}
	}

	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void alterar(final ContaCorrenteVO obj, UsuarioVO usuario) throws Exception {
		try {
			validarDados(obj, usuario);
			final String sql = "UPDATE ContaCorrente set descricao=?, digitoContaCorrente=?, numero=?, saldo=?, carteira=?, cedente=?, convenio=?, "
					+ "banco=?, nrBanco=?, agencia=?, digitoAgencia=?, contaCaixa=? "
					+ "WHERE ((codigo = ?))";

			getConexao().getJdbcTemplate().update(new PreparedStatementCreator() {

				public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
					PreparedStatement sqlAlterar = arg0.prepareStatement(sql);
					sqlAlterar.setString(1, obj.getDescricao());
					sqlAlterar.setString(2, obj.getDigitoContaCorrente());
					sqlAlterar.setString(3, obj.getNumero());
					sqlAlterar.setBigDecimal(4, obj.getSaldo());
					sqlAlterar.setString(5, obj.getCarteira());
					sqlAlterar.setString(6, obj.getCedente());
					sqlAlterar.setString(7, obj.getConvenio());
					sqlAlterar.setString(8, obj.getBanco());
					sqlAlterar.setString(9, obj.getNrBanco());
					sqlAlterar.setString(10, obj.getAgencia());
					sqlAlterar.setString(11, obj.getDigitoAgencia());
					sqlAlterar.setBoolean(12, obj.getContaCaixa());
					sqlAlterar.setInt(13, obj.getCodigo().intValue());
					return sqlAlterar;
				}
			});
			
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void excluir(ContaCorrenteVO obj, UsuarioVO usuario) throws Exception {
		try {
			String sql = "DELETE FROM ContaCorrente WHERE ((codigo = ?))";
			getConexao().getJdbcTemplate().update(sql, new Object[] { obj.getCodigo() });
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public List<ContaCorrenteVO> consultar(String campoConsulta, String valorConsulta, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		List<ContaCorrenteVO> listaConsulta = new ArrayList<ContaCorrenteVO>(0);
		if (campoConsulta.equals("NUMERO_CONTA_CORRENTE")) {
			listaConsulta = consultarPorNumeroContaCorrente(valorConsulta, nivelMontarDados, usuarioVO);
		}
		if (campoConsulta.equals("DESCRICAO")) {
			listaConsulta = consultarPorDescricao(valorConsulta, nivelMontarDados, usuarioVO);
		}
		return listaConsulta;
	}

	public List<ContaCorrenteVO> consultarPorDescricao(String nome, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		StringBuilder sb = new StringBuilder();
		sb.append(getDadosBasicos());
		sb.append(" where contaCorrente.descricao ilike ('").append(nome).append("%') ");
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
		return montarDadosLista(tabelaResultado, nivelMontarDados, usuarioVO);
	}
	
	public List<ContaCorrenteVO> consultarPorNumeroContaCorrente(String nome, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		StringBuilder sb = new StringBuilder();
		sb.append(getDadosBasicos());
		sb.append(" where contaCorrente.numero ilike ('").append(nome).append("%') ");
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
		return montarDadosLista(tabelaResultado, nivelMontarDados, usuarioVO);
	}
	
	@Override
	public List<ContaCorrenteVO> consultarContaCorrente(NivelMontarDadosEnum nivelMontarDados, Boolean contaCaixa, UsuarioVO usuarioVO) {
		StringBuilder sb = new StringBuilder();
		sb.append(getDadosBasicos());
		if (contaCaixa) {
			sb.append(" where contaCorrente.contaCaixa ");
		} else {
			sb.append(" where contaCorrente.contaCaixa = false ");
		}
		if (contaCaixa) {
			sb.append(" order by contaCorrente.descricao ");
		} else {
			sb.append(" order by contaCorrente.numero ");
		}
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
		return montarDadosLista(tabelaResultado, nivelMontarDados, usuarioVO);
	}
	
	@Override
	public void carregarDados(ContaCorrenteVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		if (nivelMontarDados.equals(NivelMontarDadosEnum.BASICO)) {
			StringBuilder sb = getDadosBasicos();
			sb.append(" where contaCorrente.codigo = ").append(obj.getCodigo());
			SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
			if (tabelaResultado.next()) {
				montarDadosBasicos(tabelaResultado, obj, nivelMontarDados, usuarioVO);
			}
		}
		if (nivelMontarDados.equals(NivelMontarDadosEnum.COMPLETO)) {
			StringBuilder sb = getDadosCompletos();
			sb.append(" where contaCorrente.codigo = ").append(obj.getCodigo());
			SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
			if (tabelaResultado.next()) {
				montarDadosCompletos(tabelaResultado, obj, nivelMontarDados, usuarioVO);
			}
		}
	}
	
	@Override
	public ContaCorrenteVO consultarPorChavePrimaria(Integer contaCorrente, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		StringBuilder sb = getObterConsultaNivelMontarDadosLiderCelula(nivelMontarDados);
		sb.append(" where contaCorrente.codigo = ").append(contaCorrente);
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
		ContaCorrenteVO obj = new ContaCorrenteVO();
		if (tabelaResultado.next()) {
			if (nivelMontarDados.equals(NivelMontarDadosEnum.BASICO)) {
				montarDadosBasicos(tabelaResultado, obj, nivelMontarDados, usuarioVO);
			} else {
				montarDadosCompletos(tabelaResultado, obj, nivelMontarDados, usuarioVO);
			}
		}
		return obj;
	}
	
	public StringBuilder getObterConsultaNivelMontarDadosLiderCelula(NivelMontarDadosEnum nivelMontarDados) {
		if (nivelMontarDados.equals(NivelMontarDadosEnum.BASICO)) {
			return getDadosBasicos();
		}
		return getDadosCompletos();
	}

	
	public List<ContaCorrenteVO> montarDadosLista(SqlRowSet dadosSQL, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		List<ContaCorrenteVO> listaMembroVOs = new ArrayList<ContaCorrenteVO>(0);
		while (dadosSQL.next()) {
			ContaCorrenteVO obj = new ContaCorrenteVO();
			if (nivelMontarDados.equals(NivelMontarDadosEnum.BASICO)) {
				montarDadosBasicos(dadosSQL, obj, nivelMontarDados, usuarioVO);
			} else {
				montarDadosCompletos(dadosSQL, obj, nivelMontarDados, usuarioVO);
			}
			listaMembroVOs.add(obj);
		}
		return listaMembroVOs;
	}
	
	public StringBuilder getDadosBasicos() {
		StringBuilder sb = new StringBuilder();
		sb.append("select * from contaCorrente ");
		return sb;
	}
	
	public StringBuilder getDadosCompletos() {
		StringBuilder sb = new StringBuilder();
		sb.append("select * from contaCorrente ");
		return sb;
	}
	
	public void montarDadosBasicos(SqlRowSet dadosSQL, ContaCorrenteVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		obj.setCodigo(dadosSQL.getInt("codigo"));
		obj.setDescricao(dadosSQL.getString("descricao"));
		obj.setDigitoContaCorrente(dadosSQL.getString("digitoContaCorrente"));
		obj.setNumero(dadosSQL.getString("numero"));
		obj.setSaldo(dadosSQL.getBigDecimal("saldo"));
		obj.setCarteira(dadosSQL.getString("carteira"));
		obj.setCedente(dadosSQL.getString("cedente"));
		obj.setConvenio(dadosSQL.getString("convenio"));
		obj.setBanco(dadosSQL.getString("banco"));
		obj.setNrBanco(dadosSQL.getString("nrBanco"));
		obj.setAgencia(dadosSQL.getString("agencia"));
		obj.setDigitoAgencia(dadosSQL.getString("digitoAgencia"));
		obj.setContaCaixa(dadosSQL.getBoolean("contaCaixa"));
		obj.setNovoObj(false);
	}
	
	public void montarDadosCompletos(SqlRowSet dadosSQL, ContaCorrenteVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		obj.setCodigo(dadosSQL.getInt("codigo"));
		obj.setDescricao(dadosSQL.getString("descricao"));
		obj.setDigitoContaCorrente(dadosSQL.getString("digitoContaCorrente"));
		obj.setNumero(dadosSQL.getString("numero"));
		obj.setSaldo(dadosSQL.getBigDecimal("saldo"));
		obj.setCarteira(dadosSQL.getString("carteira"));
		obj.setCedente(dadosSQL.getString("cedente"));
		obj.setConvenio(dadosSQL.getString("convenio"));
		obj.setBanco(dadosSQL.getString("banco"));
		obj.setNrBanco(dadosSQL.getString("nrBanco"));
		obj.setAgencia(dadosSQL.getString("agencia"));
		obj.setDigitoAgencia(dadosSQL.getString("digitoAgencia"));
		obj.setContaCaixa(dadosSQL.getBoolean("contaCaixa"));
		obj.getEmpresaVO().setCodigo(dadosSQL.getInt("empresa"));
		obj.setNovoObj(false);
		if (nivelMontarDados.equals(NivelMontarDadosEnum.TELA_CONSULTA)) {
			return;
		}
		return;
	}

	@Override
	 @Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Throwable.class, propagation = Propagation.REQUIRED)
	    public void alterarSaldoContaCorrente(final Integer contaCorrente, final BigDecimal saldo, UsuarioVO usuario) throws Exception {
	        final String sql = "UPDATE ContaCorrente set saldo=? WHERE ((codigo = ?))";
	        getConexao().getJdbcTemplate().update(new PreparedStatementCreator() {

	            public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
	                PreparedStatement sqlAlterar = arg0.prepareStatement(sql);
	                sqlAlterar.setBigDecimal(1, saldo);
	                sqlAlterar.setInt(2, contaCorrente.intValue());
	                return sqlAlterar;
	            }
	        });
	    }
	
	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Throwable.class, propagation = Propagation.REQUIRED)
    public void movimentarSaldoContaCorrente(Integer contaCorrente, TipoMovimentacaoCaixaEnum tipoMovimentacao, BigDecimal valor, UsuarioVO usuario) throws Exception {
        ContaCorrenteVO obj = consultarPorChavePrimaria(contaCorrente, NivelMontarDadosEnum.BASICO, usuario);
        if (obj.getCodigo().intValue() != 0) {
            if (tipoMovimentacao.equals(TipoMovimentacaoCaixaEnum.ENTRADA)) {
                obj.setSaldo(obj.getSaldo().add(valor));
            } else if (tipoMovimentacao.equals(TipoMovimentacaoCaixaEnum.SAIDA)) {
            	obj.setSaldo(obj.getSaldo().subtract(valor));
            }
            alterarSaldoContaCorrente(obj.getCodigo(), obj.getSaldo(), usuario);
        }
    }

}
