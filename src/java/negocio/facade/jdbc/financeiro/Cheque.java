package negocio.facade.jdbc.financeiro;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.comuns.financeiro.ChequeVO;
import negocio.comuns.financeiro.ContaCorrenteVO;
import negocio.comuns.financeiro.FormaPagamentoPagamentoVO;
import negocio.comuns.financeiro.PagamentoVO;
import negocio.comuns.financeiro.enumeradores.LocalizacaoChequeEnum;
import negocio.comuns.utilitarias.Uteis;
import negocio.facade.jdbc.arquitetura.ControleAcesso;
import negocio.interfaces.financeiro.ChequeInterfaceFacade;

@Repository
@Scope("singleton")
@Lazy
public class Cheque extends ControleAcesso implements ChequeInterfaceFacade {

	private static final long serialVersionUID = 1L;

	public Cheque() {
		super();
	}

	@Override
	public void validarDados(ChequeVO obj, UsuarioVO usuarioVO) throws Exception {
		if (obj.getChequeProprio()) {
			obj.setClienteVO(null);
			if (obj.getContaCorrenteChequeProprioVO() == null || obj.getContaCorrenteChequeProprioVO().getCodigo().equals(0)) {
				throw new Exception("O campo CONTA CORRENTE � obrigat�rio.");
			}
			if (obj.getNumero().trim().equals("")) {
				throw new Exception("O campo N�MERO � obrigat�rio.");
			}
		} else {
			if (obj.getClienteVO() == null || obj.getClienteVO().getCodigo() == null || obj.getClienteVO().getCodigo() == 0l) {
				throw new Exception("O campo CLIENTE deve ser informado.");
			}
			if (obj.getTitularConta() == null || obj.getTitularConta().trim().isEmpty()) {
				throw new Exception("O campo TITULAR CONTA deve ser informado.");
			}
			if (obj.getDataCadastro() == null) {
				throw new Exception("O campo DATA CADASTRO deve ser informado.");
			}
			if (obj.getBanco() == null || obj.getBanco().trim().equals("")) {
				throw new Exception("O campo BANCO deve ser informado.");
			}
			if (obj.getAgencia() == null || obj.getAgencia().trim().equals("")) {
				throw new Exception("O campo AG�NCIA deve ser informado.");
			}
			if (obj.getContaCorrente() == null || obj.getContaCorrente().trim().equals("")) {
				throw new Exception("O campo CONTA CORRENTE deve ser informado.");
			}
			if (obj.getNumero() == null || obj.getNumero().trim().equals("")) {
				throw new Exception("O campo N�MERO deve ser informado.");
			}
			if (obj.getLocalizacao() == null) {
				throw new Exception("O campo LOCALIZA��O deve ser informado.");
			}
			if ((obj.getLocalizacao().equals(LocalizacaoChequeEnum.EM_CAIXA) || obj.getLocalizacao().equals(LocalizacaoChequeEnum.BANCO)) && (obj.getLocalizacaoChequeVO() == null || obj.getLocalizacaoChequeVO().getCodigo().equals(0))) {
				throw new Exception("O campo LOCALIZA��O CHEQUE deve ser informado.");
			}
			obj.setContaCorrenteChequeProprioVO(null);
		}
		if (obj.getDataVencimento() == null) {
			throw new Exception("O campo DATA DE VENCIMENTO deve ser informado");
		}
		if (obj.getValor().equals(BigDecimal.ZERO)) {
			throw new Exception("O campo VALOR deve ser informado");
		}

	}

	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void persistir(ChequeVO obj, UsuarioVO usuarioVO) throws Exception {
		if (obj.getCodigo().equals(0)) {
			incluir(obj, usuarioVO);
		} else {
			alterar(obj, usuarioVO);
		}
	}

	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void incluir(final ChequeVO obj, UsuarioVO usuario) throws Exception {
		try {
			validarDados(obj, usuario);
			final String sql = "INSERT INTO Cheque( cliente, fornecedor, empresa, contaCorrentelocalizacaoCheque, dataCadastro, dataVencimento, banco, agencia, contaCorrente, numero, valor, " + "localizacao, codigoOrigem, dataCompensacao, responsavelRegistroCompensacao, titularConta, chequeProprio, contaCorrenteChequeProprio, localizacaoCheque) " + " VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) returning codigo";
			obj.setCodigo((Integer) getConexao().getJdbcTemplate().query(new PreparedStatementCreator() {

				public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
					PreparedStatement sqlInserir = arg0.prepareStatement(sql);
					if (!obj.getClienteVO().getCodigo().equals(0)) {
						sqlInserir.setInt(1, obj.getClienteVO().getCodigo());
					} else {
						sqlInserir.setNull(1, 0);
					}
					if (!obj.getFornecedorVO().getCodigo().equals(0)) {
						sqlInserir.setInt(2, obj.getFornecedorVO().getCodigo());
					} else {
						sqlInserir.setNull(2, 0);
					}
					if (!obj.getEmpresaVO().getCodigo().equals(0)) {
						sqlInserir.setInt(3, obj.getEmpresaVO().getCodigo());
					} else {
						sqlInserir.setNull(3, 0);
					}
					if (!obj.getContaCorrentelocalizacaoChequeVO().getCodigo().equals(0)) {
						sqlInserir.setInt(4, obj.getContaCorrentelocalizacaoChequeVO().getCodigo());
					} else {
						sqlInserir.setNull(4, 0);
					}
					sqlInserir.setDate(5, Uteis.getDataJDBC(obj.getDataCadastro()));
					sqlInserir.setDate(6, Uteis.getDataJDBC(obj.getDataVencimento()));
					sqlInserir.setString(7, obj.getBanco());
					sqlInserir.setString(8, obj.getAgencia());
					sqlInserir.setString(9, obj.getContaCorrente());
					sqlInserir.setString(10, obj.getNumero());
					sqlInserir.setBigDecimal(11, obj.getValor());
					sqlInserir.setString(12, obj.getLocalizacao().name());
					sqlInserir.setInt(13, obj.getCodigoOrigem());
					sqlInserir.setDate(14, Uteis.getDataJDBC(obj.getDataCompensacao()));
					if (!obj.getResponsavelRegistroCompensacaoVO().getCodigo().equals(0)) {
						sqlInserir.setInt(15, obj.getResponsavelRegistroCompensacaoVO().getCodigo());
					} else {
						sqlInserir.setNull(15, 0);
					}
					sqlInserir.setString(16, obj.getTitularConta());
					sqlInserir.setBoolean(17, obj.getChequeProprio());
					if (!obj.getContaCorrenteChequeProprioVO().getCodigo().equals(0)) {
						sqlInserir.setInt(18, obj.getContaCorrenteChequeProprioVO().getCodigo());
					} else {
						sqlInserir.setNull(18, 0);
					}
					if (!obj.getLocalizacaoChequeVO().getCodigo().equals(0)) {
						sqlInserir.setInt(19, obj.getLocalizacaoChequeVO().getCodigo());
					} else {
						sqlInserir.setNull(19, 0);
					}
					return sqlInserir;
				}
			}, new ResultSetExtractor() {

				public Object extractData(ResultSet arg0) throws SQLException, DataAccessException {
					if (arg0.next()) {
						obj.setNovoObj(Boolean.FALSE);
						return arg0.getInt("codigo");
					}
					return null;
				}
			}));

			obj.setNovoObj(Boolean.FALSE);
		} catch (Exception e) {
			obj.setNovoObj(Boolean.TRUE);
			throw e;
		}
	}

	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void alterar(final ChequeVO obj, UsuarioVO usuario) throws Exception {
		try {
			validarDados(obj, usuario);
			final String sql = "UPDATE Cheque set descricao=?, digitoCheque=?, numero=?, saldo=?, carteira=?, cedente=?, convenio=?, " + "banco=?, nrBanco=?, agencia=?, digitoAgencia=?, contaCaixa=? " + "WHERE ((codigo = ?))";

			getConexao().getJdbcTemplate().update(new PreparedStatementCreator() {

				public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
					PreparedStatement sqlAlterar = arg0.prepareStatement(sql);
					sqlAlterar.setInt(13, obj.getCodigo().intValue());
					return sqlAlterar;
				}
			});

		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void excluir(ChequeVO obj, UsuarioVO usuario) throws Exception {
		try {
			String sql = "DELETE FROM Cheque WHERE ((codigo = ?))";
			getConexao().getJdbcTemplate().update(sql, new Object[] { obj.getCodigo() });
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public List<ChequeVO> consultar(String campoConsulta, String valorConsulta, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		List<ChequeVO> listaConsulta = new ArrayList<ChequeVO>(0);
		if (campoConsulta.equals("NUMERO_CONTA_CORRENTE")) {
			listaConsulta = consultarPorNumeroCheque(valorConsulta, nivelMontarDados, usuarioVO);
		}
		if (campoConsulta.equals("DESCRICAO")) {
			listaConsulta = consultarPorDescricao(valorConsulta, nivelMontarDados, usuarioVO);
		}
		return listaConsulta;
	}

	public List<ChequeVO> consultarPorDescricao(String nome, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		StringBuilder sb = new StringBuilder();
		sb.append(getObterConsultaNivelMontarDadosLiderCelula(nivelMontarDados));
		sb.append(" where cheque.descricao ilike ('").append(nome).append("%') ");
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
		return montarDadosLista(tabelaResultado, nivelMontarDados, usuarioVO);
	}

	public List<ChequeVO> consultarPorNumeroCheque(String nome, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		StringBuilder sb = new StringBuilder();
		sb.append(getObterConsultaNivelMontarDadosLiderCelula(nivelMontarDados));
		sb.append(" where cheque.numero ilike ('").append(nome).append("%') ");
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
		return montarDadosLista(tabelaResultado, nivelMontarDados, usuarioVO);
	}

	@Override
	public List<ChequeVO> consultarPorNumeroDataVencimentoCheque(String numero, Date dataInicio, Date dataFim, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		StringBuilder sb = new StringBuilder();
		sb.append(getObterConsultaNivelMontarDadosLiderCelula(nivelMontarDados));
		sb.append(" where cheque.numero ilike ('").append(numero).append("%') ");
		sb.append(" and cheque.dataVencimento >= '").append(Uteis.getDataJDBC(dataInicio)).append("' ");
		sb.append(" and cheque.dataVencimento <= '").append(Uteis.getDataJDBC(dataFim)).append("' ");
		sb.append(" order by cheque.dataVencimento ");
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
		return montarDadosLista(tabelaResultado, nivelMontarDados, usuarioVO);
	}

	@Override
	public List<ChequeVO> consultarCheque(NivelMontarDadosEnum nivelMontarDados, Boolean contaCaixa, UsuarioVO usuarioVO) {
		StringBuilder sb = new StringBuilder();
		sb.append(getDadosBasicos());
		if (contaCaixa) {
			sb.append(" where cheque.contaCaixa ");
		} else {
			sb.append(" where cheque.contaCaixa = false ");
		}
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
		return montarDadosLista(tabelaResultado, nivelMontarDados, usuarioVO);
	}

	@Override
	public void carregarDados(ChequeVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		if (nivelMontarDados.equals(NivelMontarDadosEnum.BASICO)) {
			StringBuilder sb = getDadosBasicos();
			sb.append(" where cheque.codigo = ").append(obj.getCodigo());
			SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
			if (tabelaResultado.next()) {
				montarDadosBasicos(tabelaResultado, obj, nivelMontarDados, usuarioVO);
			}
		}
		if (nivelMontarDados.equals(NivelMontarDadosEnum.COMPLETO)) {
			StringBuilder sb = getDadosCompletos();
			sb.append(" where cheque.codigo = ").append(obj.getCodigo());
			SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
			if (tabelaResultado.next()) {
				montarDadosCompletos(tabelaResultado, obj, nivelMontarDados, usuarioVO);
			}
		}
	}

	public List<ChequeVO> montarDadosLista(SqlRowSet dadosSQL, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		List<ChequeVO> listaMembroVOs = new ArrayList<ChequeVO>(0);
		while (dadosSQL.next()) {
			ChequeVO obj = new ChequeVO();
			if (nivelMontarDados.equals(NivelMontarDadosEnum.BASICO)) {
				montarDadosBasicos(dadosSQL, obj, nivelMontarDados, usuarioVO);
			} else {
				montarDadosCompletos(dadosSQL, obj, nivelMontarDados, usuarioVO);
			}
			listaMembroVOs.add(obj);
		}
		return listaMembroVOs;
	}

	public StringBuilder getDadosBasicos() {
		StringBuilder sb = new StringBuilder();
		sb.append("select * from cheque ");
		return sb;
	}

	public StringBuilder getDadosCompletos() {
		StringBuilder sb = new StringBuilder();
		sb.append("select * from cheque ");
		return sb;
	}

	public void montarDadosBasicos(SqlRowSet dadosSQL, ChequeVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		obj.setCodigo(dadosSQL.getInt("codigo"));
		obj.setNovoObj(false);
	}

	public void montarDadosCompletos(SqlRowSet dadosSQL, ChequeVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		obj.setCodigo(dadosSQL.getInt("codigo"));
		obj.setNovoObj(false);
		if (nivelMontarDados.equals(NivelMontarDadosEnum.TELA_CONSULTA)) {
			return;
		}
		return;
	}

	public StringBuilder getObterConsultaNivelMontarDadosLiderCelula(NivelMontarDadosEnum nivelMontarDados) {
		if (nivelMontarDados.equals(NivelMontarDadosEnum.BASICO)) {
			return getDadosBasicos();
		}
		return getDadosCompletos();
	}
	
	@Override
	public void inicializarDadosChequeProprio(PagamentoVO pagamentoVO, FormaPagamentoPagamentoVO formaPagamentoPagamentoVO, UsuarioVO usuarioVO) throws Exception {
		if (formaPagamentoPagamentoVO.getFormaPagamentoVO().getIsTipoCheque() && formaPagamentoPagamentoVO.getChequeVO().getChequeProprio()) {
			formaPagamentoPagamentoVO.getChequeVO().setTitularConta(formaPagamentoPagamentoVO.getContaCorrenteVO().getEmpresaVO().getDescricao());
			formaPagamentoPagamentoVO.getChequeVO().setAgencia(formaPagamentoPagamentoVO.getContaCorrenteVO().getAgencia());
			formaPagamentoPagamentoVO.getChequeVO().setBanco(formaPagamentoPagamentoVO.getContaCorrenteVO().getBanco());
			formaPagamentoPagamentoVO.getChequeVO().setContaCorrente(formaPagamentoPagamentoVO.getContaCorrenteVO().getNumero());
			if (formaPagamentoPagamentoVO.getChequeVO().getDataCadastro() == null) {
				formaPagamentoPagamentoVO.getChequeVO().setDataCadastro(new Date());
			}
			formaPagamentoPagamentoVO.getChequeVO().getEmpresaVO().setCodigo(pagamentoVO.getEmpresaVO().getCodigo());
			formaPagamentoPagamentoVO.getChequeVO().getContaCorrenteChequeProprioVO().setCodigo(formaPagamentoPagamentoVO.getContaCorrenteVO().getCodigo());
			formaPagamentoPagamentoVO.getChequeVO().getContaCorrenteChequeProprioVO().setAgencia(formaPagamentoPagamentoVO.getContaCorrenteVO().getAgencia());
			formaPagamentoPagamentoVO.getChequeVO().getContaCorrenteChequeProprioVO().setBanco(formaPagamentoPagamentoVO.getContaCorrenteVO().getBanco());
			formaPagamentoPagamentoVO.getChequeVO().getContaCorrenteChequeProprioVO().setDescricao(formaPagamentoPagamentoVO.getContaCorrenteVO().getDescricao());
			formaPagamentoPagamentoVO.getChequeVO().getContaCorrenteChequeProprioVO().setNumero(formaPagamentoPagamentoVO.getContaCorrenteVO().getNumero());
			formaPagamentoPagamentoVO.getChequeVO().setValor(formaPagamentoPagamentoVO.getValor());
			formaPagamentoPagamentoVO.getChequeVO().setLocalizacao(LocalizacaoChequeEnum.PAGAMENTO);
			getFacadeFactory().getChequeFacade().validarDados(formaPagamentoPagamentoVO.getChequeVO(), usuarioVO);
		}
	}

}
