package negocio.facade.jdbc.financeiro;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.comuns.financeiro.ContaReceberVO;
import negocio.comuns.financeiro.enumeradores.SituacaoContaReceberEnum;
import negocio.comuns.financeiro.enumeradores.TipoDescontoEnum;
import negocio.comuns.utilitarias.Uteis;
import negocio.facade.jdbc.arquitetura.ControleAcesso;
import negocio.interfaces.financeiro.ContaReceberInterfaceFacade;

@Repository
@Scope("singleton")
@Lazy
public class ContaReceber extends ControleAcesso implements ContaReceberInterfaceFacade {

	private static final long serialVersionUID = 1L;

	public ContaReceber() {
		super();
	}
	
	public void validarDados(ContaReceberVO obj, UsuarioVO usuarioVO) throws Exception {
		if (obj.getClienteVO().getCodigo().equals(0)) {
			throw new Exception("O campo CLIENTE deve ser informado!");
		}
		if (obj.getValor().compareTo(BigDecimal.ZERO) == 0) {
			throw new Exception("O campo VALOR deve ser maior que zero!");
		}
		if (obj.getDataVencimento() == null) {
			throw new Exception("O campo DATA DE VENCIMENTO deve ser informado!");
		}
	}

	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void persistir(ContaReceberVO obj, UsuarioVO usuarioVO) throws Exception {
		if (obj.isNovoObj()) {
			incluir(obj, usuarioVO);
		} else {
			alterar(obj, usuarioVO);
		}
	}

	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void incluir(final ContaReceberVO obj, UsuarioVO usuario) throws Exception {
		try {
			validarDados(obj, usuario);
			final String sql = "INSERT INTO ContaReceber( dataCadastro, cliente, situacaoContaReceber, descricaoPagamento, dataVencimento, dataCompetencia,"
					+ "valor, desconto, acrescimo, multa, juro, valorMulta, valorJuro, valorRecebido, dataRecebimento, parcela, observacao, usuario, contaCorrente, "
					+ " valorTotal, tipoDesconto, servico, nossoNumero, nrDocumento, codigoBarra, linhaDigitavel, nrNotaFiscal ) "
					+ " VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) returning codigo";
			obj.setCodigo((Integer) getConexao().getJdbcTemplate().query(new PreparedStatementCreator() {

				public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
					PreparedStatement sqlInserir = arg0.prepareStatement(sql);
					sqlInserir.setDate(1, Uteis.getDataJDBC(obj.getDataCadastro()));
					if (obj.getClienteVO().getCodigo().intValue() != 0) {
						sqlInserir.setInt(2, obj.getClienteVO().getCodigo().intValue());
					} else {
						sqlInserir.setNull(2, 0);
					}
					sqlInserir.setString(3, obj.getSituacaoContaReceber().toString());
					sqlInserir.setString(4, obj.getDescricaoPagamento());
					sqlInserir.setDate(5, Uteis.getDataJDBC(obj.getDataVencimento()));
					sqlInserir.setDate(6, Uteis.getDataJDBC(obj.getDataCompetencia()));
					sqlInserir.setBigDecimal(7, obj.getValor());
					sqlInserir.setBigDecimal(8, obj.getDesconto());
					sqlInserir.setBigDecimal(9, obj.getAcrescimo());
					sqlInserir.setBigDecimal(10, obj.getMulta());
					sqlInserir.setBigDecimal(11, obj.getJuro());
					sqlInserir.setBigDecimal(12, obj.getValorMulta());
					sqlInserir.setBigDecimal(13, obj.getValorJuro());
					sqlInserir.setBigDecimal(14, obj.getValorRecebido());
					sqlInserir.setDate(15, Uteis.getDataJDBC(obj.getDataRecebimento()));
					sqlInserir.setString(16, obj.getParcela());
					sqlInserir.setString(17, obj.getObservacao());
					if (obj.getUsuarioVO().getCodigo().intValue() != 0) {
						sqlInserir.setInt(18, obj.getUsuarioVO().getCodigo().intValue());
					} else {
						sqlInserir.setNull(18, 0);
					}
					if (obj.getContaCorrenteVO().getCodigo().intValue() != 0) {
						sqlInserir.setInt(19, obj.getContaCorrenteVO().getCodigo().intValue());
					} else {
						sqlInserir.setNull(19, 0);
					}
					sqlInserir.setBigDecimal(20, obj.getValorTotal());
					sqlInserir.setString(21, obj.getTipoDesconto().toString());
					sqlInserir.setString(22, obj.getServico());
					sqlInserir.setString(23, obj.getNossoNumero());
					sqlInserir.setString(24, obj.getNrDocumento());
					sqlInserir.setString(25, obj.getCodigoBarra());
					sqlInserir.setString(26, obj.getLinhaDigitavel());
					sqlInserir.setInt(27, obj.getNrNotaFiscal());
					return sqlInserir;
				}
			}, new ResultSetExtractor() {

				public Object extractData(ResultSet arg0) throws SQLException, DataAccessException {
					if (arg0.next()) {
						obj.setNovoObj(Boolean.FALSE);
						return arg0.getInt("codigo");
					}
					return null;
				}
			}));
			
			obj.setNovoObj(Boolean.FALSE);
		} catch (Exception e) {
			obj.setNovoObj(Boolean.TRUE);
			throw e;
		}
	}

	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void alterar(final ContaReceberVO obj, UsuarioVO usuario) throws Exception {
		try {
			validarDados(obj, usuario);
			final String sql = "UPDATE ContaReceber set  dataCadastro=?, cliente=?, situacaoContaReceber=?, descricaoPagamento=?, dataVencimento=?, dataCompetencia=?, "
					+ "valor=?, desconto=?, acrescimo=?, multa=?, juro=?, valorMulta=?, valorJuro=?, valorRecebido=?, dataRecebimento=?, parcela=?, observacao=?, usuario=?,"
					+ " contaCorrente=?, valorTotal=?, tipoDesconto=?, servico=?, nossoNumero=?, nrDocumento=?, codigoBarra=?, linhaDigitavel=?, nrNotaFiscal=? "
					+ "WHERE ((codigo = ?))";

			getConexao().getJdbcTemplate().update(new PreparedStatementCreator() {

				public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
					PreparedStatement sqlAlterar = arg0.prepareStatement(sql);
					sqlAlterar.setDate(1, Uteis.getDataJDBC(obj.getDataCadastro()));
					if (obj.getClienteVO().getCodigo().intValue() != 0) {
						sqlAlterar.setInt(2, obj.getClienteVO().getCodigo().intValue());
					} else {
						sqlAlterar.setNull(2, 0);
					}
					sqlAlterar.setString(3, obj.getSituacaoContaReceber().toString());
					sqlAlterar.setString(4, obj.getDescricaoPagamento());
					sqlAlterar.setDate(5, Uteis.getDataJDBC(obj.getDataVencimento()));
					sqlAlterar.setDate(6, Uteis.getDataJDBC(obj.getDataCompetencia()));
					sqlAlterar.setBigDecimal(7, obj.getValor());
					sqlAlterar.setBigDecimal(8, obj.getDesconto());
					sqlAlterar.setBigDecimal(9, obj.getAcrescimo());
					sqlAlterar.setBigDecimal(10, obj.getMulta());
					sqlAlterar.setBigDecimal(11, obj.getJuro());
					sqlAlterar.setBigDecimal(12, obj.getValorMulta());
					sqlAlterar.setBigDecimal(13, obj.getValorJuro());
					sqlAlterar.setBigDecimal(14, obj.getValorRecebido());
					sqlAlterar.setDate(15, Uteis.getDataJDBC(obj.getDataRecebimento()));
					sqlAlterar.setString(16, obj.getParcela());
					sqlAlterar.setString(17, obj.getObservacao());
					if (obj.getUsuarioVO().getCodigo().intValue() != 0) {
						sqlAlterar.setInt(18, obj.getUsuarioVO().getCodigo().intValue());
					} else {
						sqlAlterar.setNull(18, 0);
					}
					if (obj.getContaCorrenteVO().getCodigo().intValue() != 0) {
						sqlAlterar.setInt(19, obj.getContaCorrenteVO().getCodigo().intValue());
					} else {
						sqlAlterar.setNull(19, 0);
					}
					sqlAlterar.setBigDecimal(20, obj.getValorTotal());
					sqlAlterar.setString(21, obj.getTipoDesconto().toString());
					sqlAlterar.setString(22, obj.getServico());
					sqlAlterar.setString(23, obj.getNossoNumero());
					sqlAlterar.setString(24, obj.getNrDocumento());
					sqlAlterar.setString(25, obj.getCodigoBarra());
					sqlAlterar.setString(26, obj.getLinhaDigitavel());
					sqlAlterar.setInt(27, obj.getNrNotaFiscal());
					sqlAlterar.setInt(28, obj.getCodigo().intValue());
					return sqlAlterar;
				}
			});
			
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void excluir(ContaReceberVO obj, UsuarioVO usuario) throws Exception {
		try {
			String sql = "DELETE FROM ContaReceber WHERE ((codigo = ?))";
			getConexao().getJdbcTemplate().update(sql, new Object[] { obj.getCodigo() });
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public List<ContaReceberVO> consultar(String campoConsulta, String valorConsulta, UsuarioVO usuarioVO) {
		List<ContaReceberVO> listaConsulta = new ArrayList<ContaReceberVO>(0);
		if (campoConsulta.equals("NOME_CLIENTE")) {
			listaConsulta = consutarPorNome(valorConsulta, usuarioVO);
		}
		return listaConsulta;
	}

	public List<ContaReceberVO> consutarPorNome(String nome, UsuarioVO usuarioVO) {
		StringBuilder sb = new StringBuilder();
		sb.append(getDadosBasicos());
		sb.append(" where pessoa.nome ilike ('").append(nome).append("%') ");
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
		return montarDadosLista(tabelaResultado, NivelMontarDadosEnum.BASICO, usuarioVO);
	}
	
	@Override
	public void carregarDados(ContaReceberVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		if (nivelMontarDados.equals(NivelMontarDadosEnum.BASICO)) {
			StringBuilder sb = getDadosBasicos();
			sb.append(" where contaReceber.codigo = ").append(obj.getCodigo());
			SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
			if (tabelaResultado.next()) {
				montarDadosBasicos(tabelaResultado, obj, nivelMontarDados, usuarioVO);
			}
		}
		if (nivelMontarDados.equals(NivelMontarDadosEnum.COMPLETO)) {
			StringBuilder sb = getDadosCompletos();
			sb.append(" where contaReceber.codigo = ").append(obj.getCodigo());
			SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
			if (tabelaResultado.next()) {
				montarDadosCompletos(tabelaResultado, obj, nivelMontarDados, usuarioVO);
			}
		}
	}
	
	public List<ContaReceberVO> montarDadosLista(SqlRowSet dadosSQL, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		List<ContaReceberVO> listaContaReceberVOs = new ArrayList<ContaReceberVO>(0);
		while (dadosSQL.next()) {
			ContaReceberVO obj = new ContaReceberVO();
			montarDadosBasicos(dadosSQL, obj, nivelMontarDados, usuarioVO);
			listaContaReceberVOs.add(obj);
		}
		return listaContaReceberVOs;
	}
	
	public StringBuilder getDadosBasicos() {
		StringBuilder sb = new StringBuilder();
		sb.append("select contareceber.*, cliente.codigo AS \"cliente.codigo\", pessoa.nome AS \"cliente.nome\" ");
		sb.append(" from contareceber  ");
		sb.append(" inner join cliente on cliente.codigo = contareceber.cliente ");
		sb.append(" inner join pessoa on pessoa.codigo = cliente.pessoa ");
		return sb;
	}
	
	public StringBuilder getDadosCompletos() {
		StringBuilder sb = new StringBuilder();
		sb.append("select contareceber.*, cliente.codigo AS \"cliente.codigo\", pessoa.nome AS \"cliente.nome\", ");
		sb.append(" contaCorrente.codigo AS \"contaCorrente.codigo\", contaCorrente.numero AS \"contaCorrente.numero\", contaCorrente.agencia AS \"contaCorrente.agencia\", contaCorrente.banco AS \"contaCorrente.banco\" ");
		sb.append(" from contareceber  ");
		sb.append(" inner join cliente on cliente.codigo = contareceber.cliente ");
		sb.append(" inner join pessoa on pessoa.codigo = cliente.pessoa ");
		sb.append(" left join contaCorrente on contaCorrente.codigo = contaReceber.contaCorrente ");
		
		return sb;
	}
	
	public void montarDadosBasicos(SqlRowSet dadosSQL, ContaReceberVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		obj.setCodigo(dadosSQL.getInt("codigo"));
		obj.setDataCadastro(dadosSQL.getDate("dataCadastro"));
		obj.getClienteVO().setCodigo(dadosSQL.getInt("cliente.codigo"));
		obj.getClienteVO().getPessoaVO().setNome(dadosSQL.getString("cliente.nome"));
		obj.setSituacaoContaReceber(SituacaoContaReceberEnum.valueOf(dadosSQL.getString("situacaoContaReceber")));
		obj.setDescricaoPagamento(dadosSQL.getString("descricaoPagamento"));
		obj.setDataVencimento(dadosSQL.getDate("dataVencimento"));
		obj.setDataCompetencia(dadosSQL.getDate("dataCompetencia"));
		obj.setValor(dadosSQL.getBigDecimal("valor"));
		obj.setDesconto(dadosSQL.getBigDecimal("desconto"));
		obj.setAcrescimo(dadosSQL.getBigDecimal("acrescimo"));
		obj.setMulta(dadosSQL.getBigDecimal("multa"));
		obj.setJuro(dadosSQL.getBigDecimal("juro"));
		obj.setValorMulta(dadosSQL.getBigDecimal("valorMulta"));
		obj.setValorJuro(dadosSQL.getBigDecimal("valorJuro"));
		obj.setValorRecebido(dadosSQL.getBigDecimal("valorRecebido"));
		obj.setDataRecebimento(dadosSQL.getDate("dataRecebimento"));
		obj.setParcela(dadosSQL.getString("parcela"));
		obj.setObservacao(dadosSQL.getString("observacao"));
		obj.setServico(dadosSQL.getString("servico"));
		obj.setNossoNumero(dadosSQL.getString("nossoNumero"));
		obj.setNrDocumento(dadosSQL.getString("nrDocumento"));
		obj.setCodigoBarra(dadosSQL.getString("codigoBarra"));
		obj.setLinhaDigitavel(dadosSQL.getString("linhaDigitavel"));
		obj.setNrNotaFiscal(dadosSQL.getInt("nrNotaFiscal"));
//		obj.getUsuarioVO().setCodigo(dadosSQL.getInt("usuario.codigo"));
//		obj.getUsuarioVO().getPessoaVO().setNome(dadosSQL.getString("usuario.nome"));
		
		obj.setNovoObj(false);
	}
	
	public void montarDadosCompletos(SqlRowSet dadosSQL, ContaReceberVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		obj.setCodigo(dadosSQL.getInt("codigo"));
		obj.setDataCadastro(dadosSQL.getDate("dataCadastro"));
		obj.getClienteVO().setCodigo(dadosSQL.getInt("cliente.codigo"));
		obj.getClienteVO().getPessoaVO().setNome(dadosSQL.getString("cliente.nome"));
		obj.setSituacaoContaReceber(SituacaoContaReceberEnum.valueOf(dadosSQL.getString("situacaoContaReceber")));
		obj.setDescricaoPagamento(dadosSQL.getString("descricaoPagamento"));
		obj.setDataVencimento(dadosSQL.getDate("dataVencimento"));
		obj.setDataCompetencia(dadosSQL.getDate("dataCompetencia"));
		obj.setValor(dadosSQL.getBigDecimal("valor"));
		obj.setDesconto(dadosSQL.getBigDecimal("desconto"));
		obj.setAcrescimo(dadosSQL.getBigDecimal("acrescimo"));
		obj.setMulta(dadosSQL.getBigDecimal("multa"));
		obj.setJuro(dadosSQL.getBigDecimal("juro"));
		obj.setValorMulta(dadosSQL.getBigDecimal("valorMulta"));
		obj.setValorJuro(dadosSQL.getBigDecimal("valorJuro"));
		obj.setValorRecebido(dadosSQL.getBigDecimal("valorRecebido"));
		obj.setDataRecebimento(dadosSQL.getDate("dataRecebimento"));
		obj.setParcela(dadosSQL.getString("parcela"));
		obj.setObservacao(dadosSQL.getString("observacao"));
		obj.setValorTotal(dadosSQL.getBigDecimal("valorTotal"));
		
		obj.setServico(dadosSQL.getString("servico"));
		obj.setNossoNumero(dadosSQL.getString("nossoNumero"));
		obj.setNrDocumento(dadosSQL.getString("nrDocumento"));
		obj.setCodigoBarra(dadosSQL.getString("codigoBarra"));
		obj.setLinhaDigitavel(dadosSQL.getString("linhaDigitavel"));
		obj.setNrNotaFiscal(dadosSQL.getInt("nrNotaFiscal"));
		
		if (dadosSQL.getString("tipoDesconto") != null) {
			obj.setTipoDesconto(TipoDescontoEnum.valueOf(dadosSQL.getString("tipoDesconto")));
		}
		obj.getContaCorrenteVO().setCodigo(dadosSQL.getInt("contaCorrente.codigo"));
		obj.getContaCorrenteVO().setNumero(dadosSQL.getString("contaCorrente.numero"));
		obj.getContaCorrenteVO().setAgencia(dadosSQL.getString("contaCorrente.agencia"));
		obj.getContaCorrenteVO().setBanco(dadosSQL.getString("contaCorrente.banco"));
		obj.setNovoObj(false);
		if (nivelMontarDados.equals(NivelMontarDadosEnum.TELA_CONSULTA)) {
			return;
		}
		return;
	}

	@Override
	public void realizarCalculoValorMulta(ContaReceberVO obj, UsuarioVO usuarioVO) {
		if (obj.getValor().compareTo(BigDecimal.ZERO) != 0) {
			if (obj.getMulta().compareTo(BigDecimal.ZERO) != 0) {
				obj.setValorMulta(obj.getValor().multiply(obj.getMulta()).divide(BigDecimal.valueOf(100)));
				return;
			}
		}
		obj.setValorMulta(BigDecimal.ZERO);
	}
	
	@Override
	public void realizarCalculoValorJuro(ContaReceberVO obj, UsuarioVO usuarioVO) {
		if (obj.getValor().compareTo(BigDecimal.ZERO) != 0) {
			if (obj.getJuro().compareTo(BigDecimal.ZERO) != 0) {
				obj.setValorJuro(obj.getValor().multiply(obj.getJuro()).divide(BigDecimal.valueOf(100)));
				return;
			}
		}
		obj.setValorJuro(BigDecimal.ZERO);
	}
}
