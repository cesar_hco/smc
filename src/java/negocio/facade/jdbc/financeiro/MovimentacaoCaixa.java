package negocio.facade.jdbc.financeiro;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import controle.financeiro.MovimentacaoCaixaVO;
import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.comuns.financeiro.enumeradores.SituacaoCaixaEnum;
import negocio.comuns.financeiro.enumeradores.TipoFormaPagamentoEnum;
import negocio.comuns.financeiro.enumeradores.TipoMovimentacaoCaixaEnum;
import negocio.comuns.utilitarias.Uteis;
import negocio.comuns.utilitarias.UteisFinanceiro;
import negocio.facade.jdbc.arquitetura.ControleAcesso;
import negocio.interfaces.financeiro.MovimentacaoCaixaInterfaceFacade;

@Repository
@Scope("singleton")
@Lazy
public class MovimentacaoCaixa extends ControleAcesso implements MovimentacaoCaixaInterfaceFacade {

	private static final long serialVersionUID = 1L;

	public MovimentacaoCaixa() {
		super();
	}

	public void validarDados(MovimentacaoCaixaVO obj, UsuarioVO usuarioVO) throws Exception {
		if (obj.getEmpresaVO().getCodigo().equals(0)) {
			throw new Exception("O campo EMPRESA deve ser informado.");
		}
		if (obj.getContaCaixaVO().getCodigo().equals(0)) {
			throw new Exception("O campo CONTA CAIXA deve ser informado.");
		}
	}
	
	@Override
	public void realizarAberturaCaixa(MovimentacaoCaixaVO obj, UsuarioVO usuarioVO) throws Exception {
		obj.setSituacaoCaixa(SituacaoCaixaEnum.ABERTO);
		if (usuarioVO != null && !usuarioVO.getCodigo().equals(0)) {
			obj.getResponsavelAberturaVO().setCodigo(usuarioVO.getCodigo());
		}
		incluir(obj, usuarioVO);
	}
	
	@Override
	public void realizarFechamentoCaixa(MovimentacaoCaixaVO obj, UsuarioVO usuarioVO) throws Exception {
		obj.setSituacaoCaixa(SituacaoCaixaEnum.FECHADO);
		if (usuarioVO != null && !usuarioVO.getCodigo().equals(0)) {
			obj.getResponsavelFechamentoVO().setCodigo(usuarioVO.getCodigo());
		}
		obj.setDataFechamento(new Date());
		alterar(obj, usuarioVO);
	}

	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void persistir(MovimentacaoCaixaVO obj, UsuarioVO usuarioVO) throws Exception {
		if (obj.getCodigo().equals(0)) {
			incluir(obj, usuarioVO);
		} else {
			alterar(obj, usuarioVO);
		}
	}

	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void incluir(final MovimentacaoCaixaVO obj, UsuarioVO usuario) throws Exception {
		try {
			validarDados(obj, usuario);
			final String sql = "INSERT INTO MovimentacaoCaixa( dataAbertura, responsavelAbertura, empresa, contaCaixa, saldoInicialCheque, saldoInicialDinheiro, saldoFinalCheque, "
					+ "saldoFinalDinheiro, dataFechamento, responsavelFechamento, situacaoCaixa) "
					+ " VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) returning codigo";
			obj.setCodigo((Integer) getConexao().getJdbcTemplate().query(new PreparedStatementCreator() {

				public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
					PreparedStatement sqlInserir = arg0.prepareStatement(sql);
					sqlInserir.setTimestamp(1, Uteis.getDataJDBCTimestamp(obj.getDataAbertura()));
					if (!obj.getResponsavelAberturaVO().getCodigo().equals(0) ) {
						sqlInserir.setInt(2, obj.getResponsavelAberturaVO().getCodigo());
					} else {
						sqlInserir.setNull(2, 0);
					}
					if (!obj.getEmpresaVO().getCodigo().equals(0) ) {
						sqlInserir.setInt(3, obj.getEmpresaVO().getCodigo());
					} else {
						sqlInserir.setNull(3, 0);
					}
					if (!obj.getContaCaixaVO().getCodigo().equals(0) ) {
						sqlInserir.setInt(4, obj.getContaCaixaVO().getCodigo());
					} else {
						sqlInserir.setNull(4, 0);
					}
					sqlInserir.setBigDecimal(5, obj.getSaldoInicialCheque());
					sqlInserir.setBigDecimal(6, obj.getSaldoInicialDinheiro());
					sqlInserir.setBigDecimal(7, obj.getSaldoFinalCheque());
					sqlInserir.setBigDecimal(8, obj.getSaldoFinalDinheiro());
					sqlInserir.setTimestamp(9, Uteis.getDataJDBCTimestamp(obj.getDataFechamento()));
					if (!obj.getResponsavelFechamentoVO().getCodigo().equals(0) ) {
						sqlInserir.setInt(10, obj.getResponsavelFechamentoVO().getCodigo());
					} else {
						sqlInserir.setNull(10, 0);
					}
					sqlInserir.setString(11, obj.getSituacaoCaixa().name());
					return sqlInserir;
				}
			}, new ResultSetExtractor() {

				public Object extractData(ResultSet arg0) throws SQLException, DataAccessException {
					if (arg0.next()) {
						obj.setNovoObj(Boolean.FALSE);
						return arg0.getInt("codigo");
					}
					return null;
				}
			}));
			getFacadeFactory().getMovimentacaoCaixaItemFacade().incluirMovimentacaoCaixaItemVOs(obj.getCodigo(), obj.getMovimentacaoCaixaItemVOs(), usuario);
			obj.setNovoObj(Boolean.FALSE);
		} catch (Exception e) {
			obj.setNovoObj(Boolean.TRUE);
			throw e;
		}
	}

	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void alterar(final MovimentacaoCaixaVO obj, UsuarioVO usuario) throws Exception {
		try {
			validarDados(obj, usuario);
			final String sql = "UPDATE MovimentacaoCaixa set dataAbertura=?, responsavelAbertura=?, empresa=?, contaCaixa=?, saldoInicialCheque=?, saldoInicialDinheiro=?, saldoFinalCheque=?, "
					+ "saldoFinalDinheiro=?, dataFechamento=?, responsavelFechamento=?, situacaoCaixa=? "
					+ "WHERE ((codigo = ?))";

			getConexao().getJdbcTemplate().update(new PreparedStatementCreator() {

				public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
					PreparedStatement sqlAlterar = arg0.prepareStatement(sql);
					sqlAlterar.setTimestamp(1, Uteis.getDataJDBCTimestamp(obj.getDataAbertura()));
					if (!obj.getResponsavelAberturaVO().getCodigo().equals(0) ) {
						sqlAlterar.setInt(2, obj.getResponsavelAberturaVO().getCodigo());
					} else {
						sqlAlterar.setNull(2, 0);
					}
					if (!obj.getEmpresaVO().getCodigo().equals(0) ) {
						sqlAlterar.setInt(3, obj.getEmpresaVO().getCodigo());
					} else {
						sqlAlterar.setNull(3, 0);
					}
					if (!obj.getContaCaixaVO().getCodigo().equals(0) ) {
						sqlAlterar.setInt(4, obj.getContaCaixaVO().getCodigo());
					} else {
						sqlAlterar.setNull(4, 0);
					}
					sqlAlterar.setBigDecimal(5, obj.getSaldoInicialCheque());
					sqlAlterar.setBigDecimal(6, obj.getSaldoInicialDinheiro());
					sqlAlterar.setBigDecimal(7, obj.getSaldoFinalCheque());
					sqlAlterar.setBigDecimal(8, obj.getSaldoFinalDinheiro());
					sqlAlterar.setTimestamp(9, Uteis.getDataJDBCTimestamp(obj.getDataFechamento()));
					if (!obj.getResponsavelFechamentoVO().getCodigo().equals(0) ) {
						sqlAlterar.setInt(10, obj.getResponsavelFechamentoVO().getCodigo());
					} else {
						sqlAlterar.setNull(10, 0);
					}
					sqlAlterar.setString(11, obj.getSituacaoCaixa().name());
					sqlAlterar.setInt(12, obj.getCodigo().intValue());
					return sqlAlterar;
				}
			});
			getFacadeFactory().getContaCorrenteFacade().alterarSaldoContaCorrente(obj.getContaCaixaVO().getCodigo(), obj.getSaldoFinalDinheiro().add(obj.getSaldoFinalCheque()) , usuario);
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void excluir(MovimentacaoCaixaVO obj, UsuarioVO usuario) throws Exception {
		try {
			String sql = "DELETE FROM MovimentacaoCaixa WHERE ((codigo = ?))";
			getConexao().getJdbcTemplate().update(sql, new Object[] { obj.getCodigo() });
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public List<MovimentacaoCaixaVO> consultar(String campoConsulta, String valorConsulta, Date dataInicio, Date dataFim, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		List<MovimentacaoCaixaVO> listaConsulta = new ArrayList<MovimentacaoCaixaVO>(0);
		if (campoConsulta.equals("DESCRICAO_CONTA_CAIXA")) {
			listaConsulta = consultarPorDescricaoContaCaixa(valorConsulta, nivelMontarDados, usuarioVO);
		}
		if (campoConsulta.equals("DATA_ABERTURA")) {
			listaConsulta = consultarPorDataAbertura(dataInicio, dataFim, nivelMontarDados, usuarioVO);
		}
		return listaConsulta;
	}

	public List<MovimentacaoCaixaVO> consultarPorDataAbertura(Date dataInicio, Date dataFim, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		StringBuilder sb = new StringBuilder();
		sb.append(getDadosBasicos());
		sb.append(" where movimentacaoCaixa.dataAbertura >= '").append(Uteis.getDataJDBC(dataInicio)).append("' ");
		sb.append(" and movimentacaoCaixa.dataAbertura <= ").append(Uteis.getDataJDBC(dataFim)).append("' ");
		sb.append(" order by movimentacaoCaixa.dataAbertura ");
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
		return montarDadosLista(tabelaResultado, nivelMontarDados, usuarioVO);
	}
	
	public List<MovimentacaoCaixaVO> consultarPorDescricaoContaCaixa(String descricao, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		StringBuilder sb = new StringBuilder();
		sb.append(getDadosBasicos());
		sb.append(" where contaCorrente.descricao ilike ('").append(descricao).append("%') ");
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
		return montarDadosLista(tabelaResultado, nivelMontarDados, usuarioVO);
	}
	
	@Override
	public void carregarDados(MovimentacaoCaixaVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) throws Exception {
		if (nivelMontarDados.equals(NivelMontarDadosEnum.BASICO)) {
			StringBuilder sb = getDadosBasicos();
			sb.append(" where movimentacaoCaixa.codigo = ").append(obj.getCodigo());
			SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
			if (tabelaResultado.next()) {
				montarDadosBasicos(tabelaResultado, obj, nivelMontarDados, usuarioVO);
			}
		}
		if (nivelMontarDados.equals(NivelMontarDadosEnum.COMPLETO)) {
			StringBuilder sb = getDadosCompletos();
			sb.append(" where movimentacaoCaixa.codigo = ").append(obj.getCodigo());
			SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
			if (tabelaResultado.next()) {
				montarDadosCompletos(tabelaResultado, obj, nivelMontarDados, usuarioVO);
			}
		}
	}
	
	public List<MovimentacaoCaixaVO> montarDadosLista(SqlRowSet dadosSQL, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		List<MovimentacaoCaixaVO> listaMembroVOs = new ArrayList<MovimentacaoCaixaVO>(0);
		while (dadosSQL.next()) {
			MovimentacaoCaixaVO obj = new MovimentacaoCaixaVO();
			montarDadosBasicos(dadosSQL, obj, nivelMontarDados, usuarioVO);
			listaMembroVOs.add(obj);
		}
		return listaMembroVOs;
	}
	
	public StringBuilder getDadosBasicos() {
		StringBuilder sb = new StringBuilder();
		sb.append("select movimentacaoCaixa.*, ");
		sb.append(" contaCorrente.codigo AS \"contaCaixa.codigo\", contaCorrente.descricao AS \"contaCaixa.descricao\", ");
//		RESPONSÁVEL ABERTURA
		sb.append(" usuarioResponsavelAbertura.codigo AS \"usuarioResponsavelAbertura.codigo\", ");
		sb.append(" pessoaResponsavelAbertura.codigo AS \"pessoaResponsavelAbertura.codigo\", pessoaResponsavelAbertura.nome AS \"pessoaResponsavelAbertura.nome\", ");
//		RESPONSÁVEL FECHAMENTO
		sb.append(" usuarioResponsavelFechamento.codigo AS \"usuarioResponsavelFechamento.codigo\", ");
		sb.append(" pessoaResponsavelFechamento.codigo AS \"pessoaResponsavelFechamento.codigo\", pessoaResponsavelFechamento.nome AS \"pessoaResponsavelFechamento.nome\", ");
		sb.append(" empresa.codigo AS \"empresa.codigo\", empresa.descricao AS \"empresa.descricao\" ");
		sb.append(" from movimentacaoCaixa ");
		sb.append(" inner join contaCorrente on contaCorrente.codigo = movimentacaoCaixa.contaCaixa ");
//		RESPONSÁVEL ABERTURA
		sb.append(" left join usuario usuarioResponsavelAbertura on usuarioResponsavelAbertura.codigo = movimentacaocaixa.responsavelAbertura ");
		sb.append(" left join pessoa pessoaResponsavelAbertura on pessoaResponsavelAbertura.codigo = usuarioResponsavelAbertura.pessoa ");
//		RESPONSÁVEL FECHAMENTO
		sb.append(" left join usuario usuarioResponsavelFechamento on usuarioResponsavelFechamento.codigo = movimentacaocaixa.responsavelFechamento ");
		sb.append(" left join pessoa pessoaResponsavelFechamento on pessoaResponsavelFechamento.codigo = usuarioResponsavelFechamento.pessoa ");
		
		sb.append(" left join empresa on empresa.codigo = movimentacaoCaixa.empresa ");
		return sb;
	}
	
	public StringBuilder getDadosCompletos() {
		StringBuilder sb = new StringBuilder();
		sb.append("select movimentacaoCaixa.*, ");
		sb.append(" contaCorrente.codigo AS \"contaCaixa.codigo\", contaCorrente.descricao AS \"contaCaixa.descricao\", ");
//		RESPONSÁVEL ABERTURA
		sb.append(" usuarioResponsavelAbertura.codigo AS \"usuarioResponsavelAbertura.codigo\", ");
		sb.append(" pessoaResponsavelAbertura.codigo AS \"pessoaResponsavelAbertura.codigo\", pessoaResponsavelAbertura.nome AS \"pessoaResponsavelAbertura.nome\", ");
//		RESPONSÁVEL FECHAMENTO
		sb.append(" usuarioResponsavelFechamento.codigo AS \"usuarioResponsavelFechamento.codigo\", ");
		sb.append(" pessoaResponsavelFechamento.codigo AS \"pessoaResponsavelFechamento.codigo\", pessoaResponsavelFechamento.nome AS \"pessoaResponsavelFechamento.nome\", ");
		sb.append(" empresa.codigo AS \"empresa.codigo\", empresa.descricao AS \"empresa.descricao\" ");
		sb.append(" from movimentacaoCaixa ");
		sb.append(" inner join contaCorrente on contaCorrente.codigo = movimentacaoCaixa.contaCaixa ");
//		RESPONSÁVEL ABERTURA
		sb.append(" left join usuario usuarioResponsavelAbertura on usuarioResponsavelAbertura.codigo = movimentacaocaixa.responsavelAbertura ");
		sb.append(" left join pessoa pessoaResponsavelAbertura on pessoaResponsavelAbertura.codigo = usuarioResponsavelAbertura.pessoa ");
//		RESPONSÁVEL FECHAMENTO
		sb.append(" left join usuario usuarioResponsavelFechamento on usuarioResponsavelFechamento.codigo = movimentacaocaixa.responsavelFechamento ");
		sb.append(" left join pessoa pessoaResponsavelFechamento on pessoaResponsavelFechamento.codigo = usuarioResponsavelFechamento.pessoa ");
		
		sb.append(" left join empresa on empresa.codigo = movimentacaoCaixa.empresa ");
		return sb;
	}
	
	public void montarDadosBasicos(SqlRowSet dadosSQL, MovimentacaoCaixaVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		obj.setCodigo(dadosSQL.getInt("codigo"));
		obj.setDataAbertura(dadosSQL.getDate("dataAbertura"));
		obj.getResponsavelAberturaVO().setCodigo(dadosSQL.getInt("usuarioResponsavelAbertura.codigo"));
		obj.getResponsavelAberturaVO().getPessoaVO().setCodigo(dadosSQL.getInt("pessoaResponsavelAbertura.codigo"));
		obj.getResponsavelAberturaVO().getPessoaVO().setNome(dadosSQL.getString("pessoaResponsavelAbertura.nome"));
		obj.getEmpresaVO().setCodigo(dadosSQL.getInt("empresa.codigo"));
		obj.getEmpresaVO().setDescricao(dadosSQL.getString("empresa.descricao"));
		obj.getContaCaixaVO().setCodigo(dadosSQL.getInt("contaCaixa.codigo"));
		obj.getContaCaixaVO().setDescricao(dadosSQL.getString("contaCaixa.descricao"));
		obj.setSaldoInicialCheque(dadosSQL.getBigDecimal("saldoInicialCheque"));
		obj.setSaldoInicialDinheiro(dadosSQL.getBigDecimal("saldoInicialDinheiro"));
		obj.setSaldoFinalCheque(dadosSQL.getBigDecimal("saldoFinalCheque"));
		obj.setSaldoFinalDinheiro(dadosSQL.getBigDecimal("saldoFinalDinheiro"));
		obj.setDataFechamento(dadosSQL.getDate("dataFechamento"));
		obj.getResponsavelFechamentoVO().setCodigo(dadosSQL.getInt("usuarioResponsavelFechamento.codigo"));
		obj.getResponsavelFechamentoVO().getPessoaVO().setCodigo(dadosSQL.getInt("pessoaResponsavelAbertura.codigo"));
		obj.getResponsavelFechamentoVO().getPessoaVO().setNome(dadosSQL.getString("pessoaResponsavelAbertura.nome"));
		if (dadosSQL.getString("situacaoCaixa") != null) {
			obj.setSituacaoCaixa(SituacaoCaixaEnum.valueOf(dadosSQL.getString("situacaoCaixa")));
		}
		obj.setNovoObj(false);
	}
	
	public void montarDadosCompletos(SqlRowSet dadosSQL, MovimentacaoCaixaVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) throws Exception {
		obj.setCodigo(dadosSQL.getInt("codigo"));
		obj.setDataAbertura(dadosSQL.getDate("dataAbertura"));
		obj.setDataFechamento(dadosSQL.getDate("dataFechamento"));
		obj.getResponsavelAberturaVO().setCodigo(dadosSQL.getInt("usuarioResponsavelAbertura.codigo"));
		obj.getResponsavelAberturaVO().getPessoaVO().setCodigo(dadosSQL.getInt("pessoaResponsavelAbertura.codigo"));
		obj.getResponsavelAberturaVO().getPessoaVO().setNome(dadosSQL.getString("pessoaResponsavelAbertura.nome"));
		obj.getEmpresaVO().setCodigo(dadosSQL.getInt("empresa.codigo"));
		obj.getEmpresaVO().setDescricao(dadosSQL.getString("empresa.descricao"));
		obj.getContaCaixaVO().setCodigo(dadosSQL.getInt("contaCaixa.codigo"));
		obj.getContaCaixaVO().setDescricao(dadosSQL.getString("contaCaixa.descricao"));
		obj.setSaldoInicialCheque(dadosSQL.getBigDecimal("saldoInicialCheque"));
		obj.setSaldoInicialDinheiro(dadosSQL.getBigDecimal("saldoInicialDinheiro"));
		
		obj.setSaldoFinalCheque(consultarSaldoChequeFechamentoCaixa(obj.getContaCaixaVO().getCodigo(), obj.getDataAbertura(), obj.getDataFechamento(), obj.getCodigo(), usuarioVO.getCodigo()));
		obj.setSaldoFinalDinheiro(consultarSaldoDinheiroFechamentoCaixa(obj.getContaCaixaVO().getCodigo(), obj.getDataAbertura(), obj.getDataFechamento(), obj.getCodigo(), usuarioVO.getCodigo()));
		
		obj.getResponsavelFechamentoVO().setCodigo(dadosSQL.getInt("usuarioResponsavelFechamento.codigo"));
		obj.getResponsavelFechamentoVO().getPessoaVO().setCodigo(dadosSQL.getInt("pessoaResponsavelAbertura.codigo"));
		obj.getResponsavelFechamentoVO().getPessoaVO().setNome(dadosSQL.getString("pessoaResponsavelAbertura.nome"));
		if (dadosSQL.getString("situacaoCaixa") != null) {
			obj.setSituacaoCaixa(SituacaoCaixaEnum.valueOf(dadosSQL.getString("situacaoCaixa")));
		}
		obj.setNovoObj(false);
		if (nivelMontarDados.equals(NivelMontarDadosEnum.TELA_CONSULTA)) {
			return;
		}
		return;
	}

	@Override
    public MovimentacaoCaixaVO consultarMovimentacaoCaixaPorContaCorrenteSituacao(Integer contaCorrente, SituacaoCaixaEnum situacaoCaixaEnum, UsuarioVO usuarioVO) throws Exception {
        StringBuilder sb = getDadosCompletos();
        sb.append(" where situacaocaixa = '").append(situacaoCaixaEnum.name()).append("' ");
        sb.append(" and movimentacaoCaixa.contacaixa = ").append(contaCorrente);
        sb.append(" and dataabertura::date >= '").append(Uteis.getDataComHoraSetadaParaPrimeiroMinutoDia(new Date())).append("' ");
        sb.append(" and dataabertura::date <= '").append(Uteis.getDataComHoraSetadaParaUltimoMinutoDia(new Date())).append("' ");
        sb.append(" limit 1 ");
        SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
        MovimentacaoCaixaVO obj = new MovimentacaoCaixaVO();
        if (tabelaResultado.next()) {
        	montarDadosCompletos(tabelaResultado, obj, NivelMontarDadosEnum.COMPLETO, usuarioVO);
        	return obj;
        }
        if (obj.getCodigo().equals(0) && situacaoCaixaEnum.equals(SituacaoCaixaEnum.ABERTO)) {
        	throw new Exception("Não foi encontrado nenhum caixa aberto para efetivar a transação.");
        }
        return null;
    }
	
    public void consultarSaldoMovimentacaoCaixa(MovimentacaoCaixaVO movimentacaoCaixaVO) throws Exception {
        StringBuilder sb = new StringBuilder("");
        sb.append(" select tipoFormaPagamento, saldoInicialDinheiro, saldoInicialCheque,  sum(saida) as saida, sum(entrada) as entrada ");
        sb.append(" FROM (");
        sb.append(" SELECT distinct fp.tipoFormaPagamento, tipoMovimentacaoCaixa,saldoInicialDinheiro, saldoInicialCheque, ");
        sb.append(" CASE WHEN(tipoMovimentacaoCaixa = '").append(TipoMovimentacaoCaixaEnum.ENTRADA).append("') THEN sum(mci.valor) ELSE 0.0 END AS entrada, ");
        sb.append(" CASE WHEN(tipoMovimentacaoCaixa = '").append(TipoMovimentacaoCaixaEnum.SAIDA).append("') THEN sum(mci.valor) ELSE 0.0 END AS saida ");
        sb.append(" FROM MovimentacaoCaixaItem mci ");
        sb.append(" inner join movimentacaoCaixa mc on mci.movimentacaoCaixa = mc.codigo ");
        sb.append(" inner join formaPagamento fp on mci.formaPagamento = fp.codigo ");
        sb.append(" left join cheque cheque on mci.cheque = cheque.codigo ");
        sb.append(" WHERE mc.codigo = ").append(movimentacaoCaixaVO.getCodigo());
        sb.append(" and ((fp.tipoFormaPagamento = ('").append(TipoFormaPagamentoEnum.DINHEIRO).append("')) or ");
        sb.append(" (fp.tipoFormaPagamento = '").append(TipoFormaPagamentoEnum.CHEQUE).append("' and cheque.chequeProprio = false )) ");
        sb.append(" group by fp.tipoFormaPagamento, tipoMovimentacaoCaixa, saldoInicialDinheiro, saldoInicialCheque ");
        sb.append(" ) as t group by  tipoFormaPagamento, saldoInicialDinheiro, saldoInicialCheque ");
        SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
        movimentacaoCaixaVO.setSaldoFinalCheque(UteisFinanceiro.arredondar(movimentacaoCaixaVO.getSaldoInicialCheque(),2));
        movimentacaoCaixaVO.setSaldoFinalDinheiro(UteisFinanceiro.arredondar(movimentacaoCaixaVO.getSaldoInicialDinheiro(),2));
        while (tabelaResultado.next()) {
        	if (TipoFormaPagamentoEnum.valueOf(tabelaResultado.getString("tipoFormaPagamento")).equals(TipoFormaPagamentoEnum.CHEQUE)) {
            	movimentacaoCaixaVO.setSaldoFinalCheque(UteisFinanceiro.arredondar(((BigDecimal)tabelaResultado.getBigDecimal("saldoInicialCheque")).add((BigDecimal) tabelaResultado.getBigDecimal("entrada")).subtract((BigDecimal) tabelaResultado.getBigDecimal("saida")),2));
            } else if (TipoFormaPagamentoEnum.valueOf((String) tabelaResultado.getString("tipoFormaPagamento")).equals(TipoFormaPagamentoEnum.DINHEIRO)) {
            	movimentacaoCaixaVO.setSaldoFinalDinheiro(UteisFinanceiro.arredondar(((BigDecimal)tabelaResultado.getBigDecimal("saldoInicialDinheiro")).add((BigDecimal) tabelaResultado.getBigDecimal("entrada")).subtract((BigDecimal) tabelaResultado.getBigDecimal("saida")),2));
            }
        }

    }
    
    public BigDecimal consultarSaldoDinheiroFechamentoCaixa(Integer codigoCaixa, Date dataAbertura, Date dataFechamento, Integer codigoMovimentacaoCaixa, Integer usuario) throws Exception {
		StringBuilder sql = new StringBuilder();
		sql.append("select (select sum(valor) from movimentacaocaixaitem mci ");
		sql.append("inner join formapagamento fp on mci.formapagamento = fp.codigo ");
		sql.append("inner join movimentacaoCaixa mc on mc.codigo = mci.movimentacaoCaixa ");
		sql.append("inner join contacorrente cc on mc.contacaixa = cc.codigo ");
		if (Uteis.getDataBD0000(dataFechamento).equals(Uteis.getDataBD0000(dataAbertura))) {
			sql.append("where fp.tipoFormaPagamento = '"+TipoFormaPagamentoEnum.DINHEIRO.toString()+"' and mci.tipoMovimentacaoCaixa = '"+TipoMovimentacaoCaixaEnum.ENTRADA.name()+"' and mc.codigo <= " + codigoMovimentacaoCaixa + " and cc.codigo = " + codigoCaixa + " and datamovimentacaocaixa <= '" + Uteis.getDataBD2359(dataFechamento) + "')::numeric as t ");
		} else {
			sql.append("where fp.tipoFormaPagamento = '"+TipoFormaPagamentoEnum.DINHEIRO.toString()+"' and mci.tipoMovimentacaoCaixa = '"+TipoMovimentacaoCaixaEnum.ENTRADA.name()+"' and mc.codigo <= " + codigoMovimentacaoCaixa + " and cc.codigo = " + codigoCaixa + " and datamovimentacaocaixa <= '" + Uteis.getDataBD0000(dataFechamento) + "')::numeric as t ");
		}

		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sql.toString());
		sql = new StringBuilder();
		sql.append("select (select sum(valor) from movimentacaocaixaitem mci ");
		sql.append("inner join formapagamento fp on mci.formapagamento = fp.codigo ");
		sql.append("inner join movimentacaoCaixa mc on mc.codigo = mci.movimentacaoCaixa ");
		sql.append("inner join contacorrente cc on mc.contacaixa = cc.codigo ");
		if (Uteis.getDataBD0000(dataFechamento).equals(Uteis.getDataBD0000(dataAbertura))) {
			sql.append("where fp.tipoFormaPagamento = '"+TipoFormaPagamentoEnum.DINHEIRO.toString()+"' and mci.tipoMovimentacaoCaixa = '"+TipoMovimentacaoCaixaEnum.SAIDA.name()+"' and mc.codigo <= " + codigoMovimentacaoCaixa + " and cc.codigo = " + codigoCaixa + " and datamovimentacaocaixa <= '" + Uteis.getDataBD2359(dataFechamento) + "')::numeric as t ");
		} else {
			sql.append("where fp.tipoFormaPagamento = '"+TipoFormaPagamentoEnum.DINHEIRO.toString()+"' and mci.tipoMovimentacaoCaixa = '"+TipoMovimentacaoCaixaEnum.SAIDA.name()+"' and mc.codigo <= " + codigoMovimentacaoCaixa + " and cc.codigo = " + codigoCaixa + " and datamovimentacaocaixa <= '" + Uteis.getDataBD0000(dataFechamento) + "')::numeric as t ");
		}
		SqlRowSet tabelaResultado2 = getConexao().getJdbcTemplate().queryForRowSet(sql.toString());
		BigDecimal entrada = BigDecimal.ZERO.setScale(2);
		BigDecimal saida = BigDecimal.ZERO.setScale(2);
		if (tabelaResultado.next()) {
			entrada = tabelaResultado.getBigDecimal("t") == null ? BigDecimal.ZERO : tabelaResultado.getBigDecimal("t");
		}
		if (tabelaResultado2.next()) {
			saida = tabelaResultado2.getBigDecimal("t") == null ? BigDecimal.ZERO : tabelaResultado2.getBigDecimal("t");
		}
		return entrada.subtract(saida);
	}

	public BigDecimal consultarSaldoChequeFechamentoCaixa(Integer codigoCaixa, Date dataAbertura, Date dataFechamento, Integer codigoMovimentacaoCaixa, Integer usuario) throws Exception {
		StringBuilder sql = new StringBuilder();
		sql.append("select (select sum(valor) from movimentacaocaixaitem mci ");
		sql.append("inner join formapagamento fp on mci.formapagamento = fp.codigo ");
		sql.append("inner join movimentacaoCaixa mc on mc.codigo = mci.movimentacaoCaixa ");
		sql.append("inner join contacorrente cc on mc.contacaixa = cc.codigo ");
		if (Uteis.getDataBD0000(dataFechamento).equals(Uteis.getDataBD0000(dataAbertura))) {
			sql.append("where fp.tipoFormaPagamento = '"+TipoFormaPagamentoEnum.CHEQUE.toString()+"' and mci.tipoMovimentacaoCaixa = '"+TipoMovimentacaoCaixaEnum.ENTRADA.name()+"' and mc.codigo <= " + codigoMovimentacaoCaixa + "  and cc.codigo = " + codigoCaixa + " and datamovimentacaocaixa <= '" + Uteis.getDataBD2359(dataFechamento) + "')::numeric as t ");
		} else {
			sql.append("where fp.tipoFormaPagamento = '"+TipoFormaPagamentoEnum.CHEQUE.toString()+"' and mci.tipoMovimentacaoCaixa = '"+TipoMovimentacaoCaixaEnum.ENTRADA.name()+"' and mc.codigo <= " + codigoMovimentacaoCaixa + "  and cc.codigo = " + codigoCaixa + " and datamovimentacaocaixa <= '" + Uteis.getDataBD0000(dataFechamento) + "')::numeric as t ");
		}

		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sql.toString());
		sql = new StringBuilder();
		sql.append("select (select sum(valor) from movimentacaocaixaitem mci ");
		sql.append("inner join formapagamento fp on mci.formapagamento = fp.codigo ");
		sql.append("inner join movimentacaoCaixa mc on mc.codigo = mci.movimentacaoCaixa ");
		sql.append("inner join contacorrente cc on mc.contacaixa = cc.codigo ");
		if (Uteis.getDataBD0000(dataFechamento).equals(Uteis.getDataBD0000(dataAbertura))) {
			sql.append("where fp.tipoFormaPagamento = '"+TipoFormaPagamentoEnum.CHEQUE.toString()+"' and mci.tipoMovimentacaoCaixa = '"+TipoMovimentacaoCaixaEnum.SAIDA.name()+"' and mc.codigo <= " + codigoMovimentacaoCaixa + " and cc.codigo = " + codigoCaixa + " and datamovimentacaocaixa <= '" + Uteis.getDataBD2359(dataFechamento) + "')::numeric as t ");
		} else {
			sql.append("where fp.tipoFormaPagamento = '"+TipoFormaPagamentoEnum.CHEQUE.toString()+"' and mci.tipoMovimentacaoCaixa = '"+TipoMovimentacaoCaixaEnum.SAIDA.name()+"' and mc.codigo <= " + codigoMovimentacaoCaixa + " and cc.codigo = " + codigoCaixa + " and datamovimentacaocaixa <= '" + Uteis.getDataBD0000(dataFechamento) + "')::numeric as t ");
		}

		SqlRowSet tabelaResultado2 = getConexao().getJdbcTemplate().queryForRowSet(sql.toString());
		BigDecimal entrada = BigDecimal.ZERO.setScale(2);
		BigDecimal saida = BigDecimal.ZERO.setScale(2);
		if (tabelaResultado.next()) {
			entrada = tabelaResultado.getBigDecimal("t") == null ? BigDecimal.ZERO : tabelaResultado.getBigDecimal("t");
		}
		if (tabelaResultado2.next()) {
			saida = tabelaResultado2.getBigDecimal("t") == null ? BigDecimal.ZERO : tabelaResultado2.getBigDecimal("t");
		}
		return entrada.subtract(saida);
	}
	
	public StringBuilder getObterConsultaNivelMontarDados(NivelMontarDadosEnum nivelMontarDados) {
		if (nivelMontarDados.equals(NivelMontarDadosEnum.BASICO)) {
			return getDadosBasicos();
		}
		return getDadosCompletos();
	}
	
	@Override
	public MovimentacaoCaixaVO consultarUltimoCaixaPorContaCaixa(Integer contaCaixa, NivelMontarDadosEnum nivelMontarDadosEnum, UsuarioVO usuarioVO) throws Exception {
		StringBuilder sb = getObterConsultaNivelMontarDados(nivelMontarDadosEnum);
		sb.append(" where movimentacaoCaixa.contaCaixa = ").append(contaCaixa);
		sb.append(" order by movimentacaoCaixa.dataAbertura desc, codigo desc limit 1");
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
		MovimentacaoCaixaVO obj = new MovimentacaoCaixaVO();
		if (tabelaResultado.next()) {
			if (nivelMontarDadosEnum.equals(NivelMontarDadosEnum.BASICO)) {
				montarDadosBasicos(tabelaResultado, obj, nivelMontarDadosEnum, usuarioVO);
			} else {
				montarDadosCompletos(tabelaResultado, obj, nivelMontarDadosEnum, usuarioVO);
			}
		}
		return obj;
	}
}
