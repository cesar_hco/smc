package negocio.facade.jdbc.agenda;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.agenda.ValorHorasVO;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;

import negocio.comuns.utilitarias.Uteis;
import negocio.facade.jdbc.arquitetura.ControleAcesso;
import negocio.interfaces.administrativo.FuncionarioInterfaceFacade;
import negocio.interfaces.agenda.ValorHorasInterfaceFacade;
import negocio.interfaces.basico.ClienteInterfaceFacade;

@Repository
@Scope("singleton")
@Lazy
public class ValorHoras extends ControleAcesso implements ValorHorasInterfaceFacade{

	/**
	 * @author cesar henrique
	 */
	private static final long serialVersionUID = 1L;

	public ValorHoras() {
		super();
	}
	
	public void validarDados(ValorHorasVO obj, UsuarioVO usuarioVO) throws Exception {
		if (obj.getValorHora().equals("")) {
			throw new Exception("O campo Valor deve ser informado!");
		}
	}

	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void incluir(ValorHorasVO obj, UsuarioVO usuarioVO) throws Exception {
		if (obj.isNovoObj()) {
			persistir(obj, usuarioVO);
		} else {
			alterar(obj, usuarioVO);
		}
	}

	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void persistir(final ValorHorasVO obj, UsuarioVO usuario) throws Exception {
		try {
			validarDados(obj, usuario);
			final String sql = "INSERT INTO valorhoras( valorHora ,dataAtualizacao ,ativado) "
					+ " VALUES ( ?, ?, ?) returning codigo";
			obj.setCodigo((Integer) getConexao().getJdbcTemplate().query(new PreparedStatementCreator() {

				public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
					PreparedStatement sqlInserir = arg0.prepareStatement(sql);
					
					sqlInserir.setDouble(1, obj.getValorHora());
					sqlInserir.setDate(2, Uteis.getDataJDBC(obj.getDataAtualizacao()));
					sqlInserir.setBoolean(3, obj.isAtivado());
					
					return sqlInserir;
				}
			}, new ResultSetExtractor() {

				public Object extractData(ResultSet arg0) throws SQLException, DataAccessException {
					if (arg0.next()) {
						obj.setNovoObj(Boolean.FALSE);
						return arg0.getInt("codigo");
					}
					return null;
				}
			}));
			
			obj.setNovoObj(Boolean.FALSE);
		} catch (Exception e) {
			obj.setNovoObj(Boolean.TRUE);
			
			throw e;
		}
	}

	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void alterar(final ValorHorasVO obj, UsuarioVO usuario) throws Exception {
		try {
			validarDados(obj, usuario);
			
			final String sql = "UPDATE valorHoras set valorHora = ? ,dataAtualizacao = ? ,ativado = ? WHERE ((codigo = ?))";

			getConexao().getJdbcTemplate().update(new PreparedStatementCreator() {

				public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
					PreparedStatement sqlAlterar = arg0.prepareStatement(sql);
					
					sqlAlterar.setDouble(1, obj.getValorHora());
					sqlAlterar.setDate(2, Uteis.getDataJDBC(obj.getDataAtualizacao()));
					sqlAlterar.setBoolean(3, obj.isAtivado());
					
					return sqlAlterar;
				}
			});
			
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void excluir(ValorHorasVO obj, UsuarioVO usuario) throws Exception {
		try {
			String sql = "DELETE FROM valorHoras WHERE ((codigo = ?))";
			getConexao().getJdbcTemplate().update(sql, new Object[] { obj.getCodigo() });
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public List<ValorHorasVO> consultarPorCodigo(String campoConsulta, Integer valorConsulta, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		List<ValorHorasVO> listaConsulta = new ArrayList<ValorHorasVO>(0);
		if (campoConsulta.equals("CODIGO")) {
			listaConsulta = consultarPorCodigo(valorConsulta, nivelMontarDados, usuarioVO);
		}
		return listaConsulta;
	}

	public List<ValorHorasVO> consultarPorCodigo(Integer codigo, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		StringBuilder sb = new StringBuilder();
		sb.append(getDadosBasicos());
		sb.append("where valorHoras.codigo = ").append(codigo);
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
		return montarDadosLista(tabelaResultado, nivelMontarDados, usuarioVO);
	}
	
	
	
	public List<ValorHorasVO> montarDadosLista(SqlRowSet dadosSQL, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		List<ValorHorasVO> listaMembroVOs = new ArrayList<ValorHorasVO>(0);
		while (dadosSQL.next()) {
			ValorHorasVO obj = new ValorHorasVO();
			montarDados(dadosSQL, obj, nivelMontarDados, usuarioVO);
			listaMembroVOs.add(obj);
		}
		return listaMembroVOs;
	}
	
	public StringBuilder getDadosBasicos() {
		StringBuilder sb = new StringBuilder();
		sb.append("select codigo, valorHora ,dataAtualizacao ,ativado ");
		sb.append(" from valorHoras ");
		
		return sb;
	}
	
	
	
	public void montarDados(SqlRowSet dadosSQL, ValorHorasVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		obj.setCodigo(dadosSQL.getInt("codigo"));
		obj.setValorHora(dadosSQL.getDouble("valorHora"));
		obj.setDataAtualizacao(dadosSQL.getDate("dataAtualizacao"));
		obj.setAtivado(dadosSQL.getBoolean("ativado"));
		
		obj.setNovoObj(false);
	}
	
	

}
