package negocio.facade.jdbc.basico;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.comuns.basico.ClienteVO;
import negocio.comuns.basico.FornecedorVO;
import negocio.facade.jdbc.arquitetura.ControleAcesso;
import negocio.interfaces.basico.ClienteInterfaceFacade;
import negocio.interfaces.basico.FornecedorInterfaceFacade;

@Repository
@Scope("singleton")
@Lazy
public class Fornecedor extends ControleAcesso implements FornecedorInterfaceFacade{

	private static final long serialVersionUID = 1L;

	public Fornecedor() {
		super();
	}
	
	public void validarDados(FornecedorVO obj, UsuarioVO usuarioVO) throws Exception {
		if (obj.getNome().equals("")) {
			throw new Exception("O campo NOME deve ser informado!");
		}
	}

	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void persistir(FornecedorVO obj, UsuarioVO usuarioVO) throws Exception {
		if (obj.isNovoObj()) {
			incluir(obj, usuarioVO);
		} else {
			alterar(obj, usuarioVO);
		}
	}

	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void incluir(final FornecedorVO obj, UsuarioVO usuario) throws Exception {
		try {
			validarDados(obj, usuario);
			final String sql = "INSERT INTO Fornecedor( nome, cnpj, inscricaoEstadual, cep, endereco, bairro, complemento, cidade, telefone1, telefone2) "
					+ " VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) returning codigo";
			obj.setCodigo((Integer) getConexao().getJdbcTemplate().query(new PreparedStatementCreator() {

				public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
					PreparedStatement sqlInserir = arg0.prepareStatement(sql);
					sqlInserir.setString(1, obj.getNome());
					sqlInserir.setString(2, obj.getCnpj());
					sqlInserir.setString(3, obj.getInscricaoEstadual());
					sqlInserir.setString(4, obj.getCep());
					sqlInserir.setString(5, obj.getEndereco());
					sqlInserir.setString(6, obj.getBairro());
					sqlInserir.setString(7, obj.getComplemento());
					if (obj.getCidadeVO().getCodigo().intValue() != 0) {
						sqlInserir.setInt(8, obj.getCidadeVO().getCodigo().intValue());
					} else {
						sqlInserir.setNull(8, 0);
					}
					sqlInserir.setString(9, obj.getTelefone1());
					sqlInserir.setString(10, obj.getTelefone2());
					
					return sqlInserir;
				}
			}, new ResultSetExtractor() {

				public Object extractData(ResultSet arg0) throws SQLException, DataAccessException {
					if (arg0.next()) {
						obj.setNovoObj(Boolean.FALSE);
						return arg0.getInt("codigo");
					}
					return null;
				}
			}));
			
			obj.setNovoObj(Boolean.FALSE);
		} catch (Exception e) {
			obj.setNovoObj(Boolean.TRUE);
			throw e;
		}
	}

	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void alterar(final FornecedorVO obj, UsuarioVO usuario) throws Exception {
		try {
			validarDados(obj, usuario);
			final String sql = "UPDATE Fornecedor set nome=?, cnpj=?, inscricaoEstadual=?, cep=?, endereco=?, bairro=?, complemento=?, cidade=?, telefone1=?, telefone2=? WHERE ((codigo = ?))";

			getConexao().getJdbcTemplate().update(new PreparedStatementCreator() {

				public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
					PreparedStatement sqlAlterar = arg0.prepareStatement(sql);
					sqlAlterar.setString(1, obj.getNome());
					sqlAlterar.setString(2, obj.getCnpj());
					sqlAlterar.setString(3, obj.getInscricaoEstadual());
					sqlAlterar.setString(4, obj.getCep());
					sqlAlterar.setString(5, obj.getEndereco());
					sqlAlterar.setString(6, obj.getBairro());
					sqlAlterar.setString(7, obj.getComplemento());
					if (obj.getCidadeVO().getCodigo().intValue() != 0) {
						sqlAlterar.setInt(8, obj.getCidadeVO().getCodigo().intValue());
					} else {
						sqlAlterar.setNull(8, 0);
					}
					sqlAlterar.setString(9, obj.getTelefone1());
					sqlAlterar.setString(10, obj.getTelefone2());
					sqlAlterar.setInt(11, obj.getCodigo().intValue());
					return sqlAlterar;
				}
			});
			
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void excluir(FornecedorVO obj, UsuarioVO usuario) throws Exception {
		try {
			String sql = "DELETE FROM Fornecedor WHERE ((codigo = ?))";
			getConexao().getJdbcTemplate().update(sql, new Object[] { obj.getCodigo() });
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public List<FornecedorVO> consultar(String campoConsulta, String valorConsulta, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		List<FornecedorVO> listaConsulta = new ArrayList<FornecedorVO>(0);
		if (campoConsulta.equals("NOME")) {
			listaConsulta = consultarPorNome(valorConsulta, nivelMontarDados, usuarioVO);
		}
		return listaConsulta;
	}

	public List<FornecedorVO> consultarPorNome(String nome, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		StringBuilder sb = new StringBuilder();
		sb.append(getDadosBasicos());
		sb.append(" where fornecedor.nome ilike ('").append(nome).append("%') ");
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
		return montarDadosLista(tabelaResultado, nivelMontarDados, usuarioVO);
	}
	
	@Override
	public void carregarDados(FornecedorVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		if (nivelMontarDados.equals(NivelMontarDadosEnum.BASICO)) {
			StringBuilder sb = getDadosBasicos();
			sb.append(" where fornecedor.codigo = ").append(obj.getCodigo());
			SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
			if (tabelaResultado.next()) {
				montarDadosBasicos(tabelaResultado, obj, nivelMontarDados, usuarioVO);
			}
		}
		if (nivelMontarDados.equals(NivelMontarDadosEnum.COMPLETO)) {
			StringBuilder sb = getDadosCompletos();
			sb.append(" where fornecedor.codigo = ").append(obj.getCodigo());
			SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
			if (tabelaResultado.next()) {
				montarDadosCompletos(tabelaResultado, obj, nivelMontarDados, usuarioVO);
			}
		}
	}
	
	public List<FornecedorVO> montarDadosLista(SqlRowSet dadosSQL, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		List<FornecedorVO> listaMembroVOs = new ArrayList<FornecedorVO>(0);
		while (dadosSQL.next()) {
			FornecedorVO obj = new FornecedorVO();
			montarDadosBasicos(dadosSQL, obj, nivelMontarDados, usuarioVO);
			listaMembroVOs.add(obj);
		}
		return listaMembroVOs;
	}
	
	public StringBuilder getDadosBasicos() {
		StringBuilder sb = new StringBuilder();
		sb.append("select fornecedor.*, ");
		sb.append(" cidade.codigo AS \"cidade.codigo\", cidade.nome AS \"cidade.nome\" ");
		sb.append(" from fornecedor ");
		sb.append(" left join cidade on cidade.codigo = fornecedor.cidade ");
		return sb;
	}
	
	public StringBuilder getDadosCompletos() {
		StringBuilder sb = new StringBuilder();
		sb.append("select fornecedor.*, ");
		sb.append(" cidade.codigo AS \"cidade.codigo\", cidade.nome AS \"cidade.nome\" ");
		sb.append(" from fornecedor ");
		sb.append(" left join cidade on cidade.codigo = fornecedor.cidade ");
		return sb;
	}
	
	public void montarDadosBasicos(SqlRowSet dadosSQL, FornecedorVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		obj.setCodigo(dadosSQL.getInt("codigo"));
		obj.setNome(dadosSQL.getString("nome"));
		obj.setCnpj(dadosSQL.getString("cnpj"));
		obj.setInscricaoEstadual(dadosSQL.getString("inscricaoEstadual"));
		obj.setCep(dadosSQL.getString("cep"));
		obj.setEndereco(dadosSQL.getString("endereco"));
		obj.setBairro(dadosSQL.getString("bairro"));
		obj.setComplemento(dadosSQL.getString("complemento"));
		obj.getCidadeVO().setCodigo(dadosSQL.getInt("cidade.codigo"));
		obj.getCidadeVO().setNome(dadosSQL.getString("cidade.nome"));
		obj.setTelefone1(dadosSQL.getString("telefone1"));
		obj.setTelefone2(dadosSQL.getString("telefone2"));
		obj.setNovoObj(false);
	}
	
	public void montarDadosCompletos(SqlRowSet dadosSQL, FornecedorVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		obj.setCodigo(dadosSQL.getInt("codigo"));
		obj.setNome(dadosSQL.getString("nome"));
		obj.setCnpj(dadosSQL.getString("cnpj"));
		obj.setInscricaoEstadual(dadosSQL.getString("inscricaoEstadual"));
		obj.setCep(dadosSQL.getString("cep"));
		obj.setEndereco(dadosSQL.getString("endereco"));
		obj.setBairro(dadosSQL.getString("bairro"));
		obj.setComplemento(dadosSQL.getString("complemento"));
		obj.getCidadeVO().setCodigo(dadosSQL.getInt("cidade.codigo"));
		obj.getCidadeVO().setNome(dadosSQL.getString("cidade.nome"));
		obj.setTelefone1(dadosSQL.getString("telefone1"));
		obj.setTelefone2(dadosSQL.getString("telefone2"));
		obj.setNovoObj(false);
		if (nivelMontarDados.equals(NivelMontarDadosEnum.TELA_CONSULTA)) {
			return;
		}
		return;
	}

}
