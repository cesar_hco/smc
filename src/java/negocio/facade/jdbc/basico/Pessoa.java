/**
 * 
 */
package negocio.facade.jdbc.basico;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.comuns.basico.PessoaVO;
import negocio.comuns.utilitarias.Uteis;
import negocio.facade.jdbc.arquitetura.ControleAcesso;
import negocio.interfaces.basico.PessoaInterfaceFacade;

/**
 * @author Carlos Eug�nio
 *
 */
@Repository
@Scope("singleton")
@Lazy
public class Pessoa extends ControleAcesso implements PessoaInterfaceFacade{

	private static final long serialVersionUID = 1L;

	public Pessoa() {
		super();
	}

	public void validarDados(PessoaVO obj) throws Exception {
		if (obj.getNome().equals("")) {
			throw new Exception("O campo NOME deve ser informado!");
		}
	}

	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void persistir(PessoaVO obj, UsuarioVO usuarioVO) throws Exception {
		if (obj.isNovoObj()) {
			incluirFuncionario(obj, usuarioVO);
		} else {
			alterar(obj, usuarioVO);
		}
	}

	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void incluirFuncionario(final PessoaVO obj, UsuarioVO usuario) throws Exception {
		try {
			validarDados(obj);
			final String sql = "INSERT INTO Pessoa( nome, cpf, rg, celular, telefone1, telefone2, cep, endereco, setor, cidade, dataNascimento, email , funcionario , cliente) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? , true , false) returning codigo";
			obj.setCodigo((Integer) getConexao().getJdbcTemplate().query(new PreparedStatementCreator() {

				public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
					PreparedStatement sqlInserir = arg0.prepareStatement(sql);
					sqlInserir.setString(1, obj.getNome());
					sqlInserir.setString(2, obj.getCpf());
					sqlInserir.setString(3, obj.getRg());
					sqlInserir.setString(4, obj.getCelular());
					sqlInserir.setString(5, obj.getTelefone1());
					sqlInserir.setString(6, obj.getTelefone2());
					sqlInserir.setString(7, obj.getCep());
					sqlInserir.setString(8, obj.getEndereco());
					sqlInserir.setString(9, obj.getSetor());
					
					if (obj.getCidadeVO().getCodigo().intValue() != 0) {
						sqlInserir.setInt(10, obj.getCidadeVO().getCodigo().intValue());
					} else {
						sqlInserir.setNull(10, 0);
					}
					sqlInserir.setDate(11, Uteis.getDataJDBC(obj.getDataNascimento()));
					sqlInserir.setString(12, obj.getEmail());
				/*	sqlInserir.setBoolean(13, obj.isFuncionario());
					sqlInserir.setBoolean(14, obj.isCliente());*/

					return sqlInserir;
				}
			}, new ResultSetExtractor() {

				public Object extractData(ResultSet arg0) throws SQLException, DataAccessException {
					if (arg0.next()) {
						obj.setNovoObj(Boolean.FALSE);
						return arg0.getInt("codigo");
					}
					return null;
				}
			}));
			obj.setNovoObj(Boolean.FALSE);
		} catch (Exception e) {
			obj.setNovoObj(Boolean.TRUE);
			throw e;
		}
	}
	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void incluirCliente(final PessoaVO obj, UsuarioVO usuario) throws Exception {
		try {
			validarDados(obj);
			final String sql = "INSERT INTO Pessoa( nome, cpf, rg, celular, telefone1, telefone2, cep, endereco, setor, cidade, dataNascimento, email , funcionario , cliente) VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? , ? , ?) returning codigo";
			obj.setCodigo((Integer) getConexao().getJdbcTemplate().query(new PreparedStatementCreator() {

				public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
					PreparedStatement sqlInserir = arg0.prepareStatement(sql);
					sqlInserir.setString(1, obj.getNome());
					sqlInserir.setString(2, obj.getCpf());
					sqlInserir.setString(3, obj.getRg());
					sqlInserir.setString(4, obj.getCelular());
					sqlInserir.setString(5, obj.getTelefone1());
					sqlInserir.setString(6, obj.getTelefone2());
					sqlInserir.setString(7, obj.getCep());
					sqlInserir.setString(8, obj.getEndereco());
					sqlInserir.setString(9, obj.getSetor());
					
					if (obj.getCidadeVO().getCodigo().intValue() != 0) {
						sqlInserir.setInt(10, obj.getCidadeVO().getCodigo().intValue());
					} else {
						sqlInserir.setNull(10, 0);
					}
					sqlInserir.setDate(11, Uteis.getDataJDBC(obj.getDataNascimento()));
					sqlInserir.setString(12, obj.getEmail());
					sqlInserir.setBoolean(13, obj.isFuncionario());
					sqlInserir.setBoolean(14, obj.isCliente());

					return sqlInserir;
				}
			}, new ResultSetExtractor() {

				public Object extractData(ResultSet arg0) throws SQLException, DataAccessException {
					if (arg0.next()) {
						obj.setNovoObj(Boolean.FALSE);
						return arg0.getInt("codigo");
					}
					return null;
				}
			}));
			obj.setNovoObj(Boolean.FALSE);
		} catch (Exception e) {
			obj.setNovoObj(Boolean.TRUE);
			throw e;
		}
	}
	
	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void alterar(final PessoaVO obj, UsuarioVO usuario) throws Exception {
		try {

			validarDados(obj);
			final String sql = "UPDATE Pessoa set nome=?, cpf=?, rg=?, celular=?, telefone1 = ?, telefone2=?, cep=?, endereco=?, setor = ?, cidade = ?, dataNascimento=?, email=? WHERE ((codigo = ?))";

			getConexao().getJdbcTemplate().update(new PreparedStatementCreator() {

				public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
					PreparedStatement sqlAlterar = arg0.prepareStatement(sql);
					sqlAlterar.setString(1, obj.getNome());
					sqlAlterar.setString(2, obj.getCpf());
					sqlAlterar.setString(3, obj.getRg());
					sqlAlterar.setString(4, obj.getCelular());
					sqlAlterar.setString(5, obj.getTelefone1());
					sqlAlterar.setString(6, obj.getTelefone2());
					sqlAlterar.setString(7, obj.getCep());
					sqlAlterar.setString(8, obj.getEndereco());
					sqlAlterar.setString(9, obj.getSetor());
					if (obj.getCidadeVO().getCodigo().intValue() != 0) {
						sqlAlterar.setInt(10, obj.getCidadeVO().getCodigo().intValue());
					} else {
						sqlAlterar.setNull(10, 0);
					}
					sqlAlterar.setDate(11, Uteis.getDataJDBC(obj.getDataNascimento()));
					sqlAlterar.setString(12, obj.getEmail());
					sqlAlterar.setInt(13, obj.getCodigo().intValue());
					return sqlAlterar;
				}
			});
		} catch (Exception e) {
			throw e;
		}
	}

	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void excluir(PessoaVO obj, UsuarioVO usuario) throws Exception {
		try {
			String sql = "DELETE FROM Pessoa WHERE ((codigo = ?))";
			getConexao().getJdbcTemplate().update(sql, new Object[] { obj.getCodigo() });
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public List<PessoaVO> consultar(String campoConsulta, String valorConsulta, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		List<PessoaVO> listaConsulta = new ArrayList<PessoaVO>(0);
		if (campoConsulta.equals("NOME")) {
			listaConsulta = consutarPorNome(valorConsulta, usuarioVO);
		}
		
		return listaConsulta;
	}

	public List<PessoaVO> consutarPorNome(String nome, UsuarioVO usuarioVO) {
		StringBuilder sb = new StringBuilder();
		sb.append("select pessoa.codigo, pessoa.nome, pessoa.cpf, pessoa.rg, pessoa.celular, pessoa.telefone1, pessoa.telefone2, pessoa.cep, pessoa.endereco, pessoa.dataNascimento, ");
		sb.append(" cidade.codigo AS \"cidade.codigo\", cidade.nome AS \"cidade.nome\" ,pessoa.setor ,pessoa.email ");
		
		sb.append(" from pessoa ");
		sb.append(" left join cidade on cidade.codigo = pessoa.cidade ");
		sb.append(" where pessoa.nome ilike ('").append(nome).append("%') ");
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
		List<PessoaVO> listaPredioVOs = new ArrayList<PessoaVO>(0);
		while (tabelaResultado.next()) {
			PessoaVO obj = new PessoaVO();
			obj.setCodigo(tabelaResultado.getInt("codigo"));
			obj.setNome(tabelaResultado.getString("nome"));
			obj.setCpf(tabelaResultado.getString("cpf"));
			obj.setRg(tabelaResultado.getString("rg"));
			obj.setCelular(tabelaResultado.getString("celular"));
			obj.setTelefone1(tabelaResultado.getString("telefone1"));
			obj.setTelefone2(tabelaResultado.getString("telefone2"));
			obj.setCep(tabelaResultado.getString("cep"));
			obj.setEndereco(tabelaResultado.getString("endereco"));
			obj.getCidadeVO().setCodigo(tabelaResultado.getInt("cidade.codigo"));
			obj.getCidadeVO().setNome(tabelaResultado.getString("cidade.nome"));
			obj.setSetor(tabelaResultado.getString("setor"));
			obj.setEmail(tabelaResultado.getString("email"));
			obj.setDataNascimento(tabelaResultado.getDate("dataNascimento"));
			listaPredioVOs.add(obj);
		}
		return listaPredioVOs;
	}
	@Override
	public List<PessoaVO> consultarFuncionarioUsuario(String campoConsulta, String valorConsulta, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		List<PessoaVO> listaConsulta = new ArrayList<PessoaVO>(0);
		if (campoConsulta.equals("NOME")) {
			listaConsulta = consutarPorNomeFuncionarioUsuario(valorConsulta, usuarioVO);
		}
		
		return listaConsulta;
	}

	public List<PessoaVO> consutarPorNomeFuncionarioUsuario(String nome, UsuarioVO usuarioVO) {
		StringBuilder sb = new StringBuilder();
		sb.append("select pessoa.codigo, pessoa.nome, pessoa.cpf, pessoa.rg, pessoa.celular, pessoa.telefone1, pessoa.telefone2, pessoa.cep, pessoa.endereco, pessoa.dataNascimento, ");
		sb.append(" cidade.codigo AS \"cidade.codigo\", cidade.nome AS \"cidade.nome\" ,pessoa.setor ,pessoa.email ");
		
		sb.append(" from pessoa ");
		sb.append(" left join cidade on cidade.codigo = pessoa.cidade ");
		sb.append(" where pessoa.nome ilike ('").append(nome).append("%') ");
		sb.append(" and pessoa.funcionario = 'true' ");
		
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
		List<PessoaVO> listaPredioVOs = new ArrayList<PessoaVO>(0);
		while (tabelaResultado.next()) {
			PessoaVO obj = new PessoaVO();
			obj.setCodigo(tabelaResultado.getInt("codigo"));
			obj.setNome(tabelaResultado.getString("nome"));
			obj.setCpf(tabelaResultado.getString("cpf"));
			obj.setRg(tabelaResultado.getString("rg"));
			obj.setCelular(tabelaResultado.getString("celular"));
			obj.setTelefone1(tabelaResultado.getString("telefone1"));
			obj.setTelefone2(tabelaResultado.getString("telefone2"));
			obj.setCep(tabelaResultado.getString("cep"));
			obj.setEndereco(tabelaResultado.getString("endereco"));
			obj.getCidadeVO().setCodigo(tabelaResultado.getInt("cidade.codigo"));
			obj.getCidadeVO().setNome(tabelaResultado.getString("cidade.nome"));
			obj.setSetor(tabelaResultado.getString("setor"));
			obj.setEmail(tabelaResultado.getString("email"));
			obj.setDataNascimento(tabelaResultado.getDate("dataNascimento"));
			listaPredioVOs.add(obj);
		}
		return listaPredioVOs;
	}
}
