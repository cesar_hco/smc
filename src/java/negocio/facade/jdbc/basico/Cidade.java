package negocio.facade.jdbc.basico;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.basico.CidadeVO;
import negocio.comuns.basico.EstadoVO;
import negocio.facade.jdbc.arquitetura.ControleAcesso;
import negocio.interfaces.basico.CidadeInterfaceFacade;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Classe de persistência que encapsula todas as operações de manipulação dos
 * dados da classe <code>CidadeVO</code>. Responsável por implementar operações
 * como incluir, alterar, excluir e consultar pertinentes a classe
 * <code>CidadeVO</code>. Encapsula toda a interação com o banco de dados.
 * 
 * @see CidadeVO
 * @see ControleAcesso
 */
@Repository
@Scope("singleton")
@Lazy
public class Cidade extends ControleAcesso implements CidadeInterfaceFacade {

	protected static String idEntidade;

	public Cidade() throws Exception {
		super();
		setIdEntidade("Cidade");
	}


	public CidadeVO novo() throws Exception {
		Cidade.incluir(getIdEntidade());
		CidadeVO obj = new CidadeVO();
		return obj;
	}
	
	public void validarDados(CidadeVO obj) throws Exception {
		
	}


	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void incluir(final CidadeVO obj, UsuarioVO usuarioVO) throws Exception {
		try {
			validarDados(obj);
			Cidade.incluir(getIdEntidade(), true, usuarioVO);

			final String sql = "INSERT INTO Cidade( nome, estado, codigoinep, codigoibge ) VALUES ( ?, ?, ?, ? ) returning codigo";
			obj.setCodigo((Integer) getConexao().getJdbcTemplate().query(new PreparedStatementCreator() {

				public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
					PreparedStatement sqlInserir = arg0.prepareStatement(sql);
					sqlInserir.setString(1, obj.getNome());
					sqlInserir.setInt(2, obj.getEstado().getCodigo());
					sqlInserir.setInt(3, obj.getCodigoInep());
					sqlInserir.setString(4, obj.getCodigoIBGE());
					return sqlInserir;
				}
			}, new ResultSetExtractor() {

				public Object extractData(ResultSet arg0) throws SQLException, DataAccessException {
					if (arg0.next()) {
						obj.setNovoObj(Boolean.FALSE);
						return arg0.getInt("codigo");
					}
					return null;
				}
			}));

			obj.setNovoObj(Boolean.FALSE);
		} catch (Exception e) {
			obj.setNovoObj(true);
			throw e;
		}
	}


	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void alterar(final CidadeVO obj, UsuarioVO usuarioVO) throws Exception {
		try {
			validarDados(obj);
			Cidade.alterar(getIdEntidade(), true, usuarioVO);
			final String sql = "UPDATE Cidade set nome=?, estado=?, codigoinep=?, codigoibge=? WHERE ((codigo = ?))";
			getConexao().getJdbcTemplate().update(new PreparedStatementCreator() {

				public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
					PreparedStatement sqlAlterar = arg0.prepareStatement(sql);
					sqlAlterar.setString(1, obj.getNome());
					sqlAlterar.setInt(2, obj.getEstado().getCodigo());
					sqlAlterar.setInt(3, obj.getCodigoInep());
					sqlAlterar.setString(4, obj.getCodigoIBGE());
					sqlAlterar.setInt(5, obj.getCodigo().intValue());
					return sqlAlterar;
				}
			});

		} catch (Exception e) {
			throw e;
		}
	}

	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void alterarCodigoIBGECidade(final CidadeVO obj) throws Exception {
		try {
			validarDados(obj);
			Cidade.alterar(getIdEntidade());
			final String sql = "UPDATE Cidade set codigoibge=? WHERE ((codigo = ?))";
			getConexao().getJdbcTemplate().update(new PreparedStatementCreator() {

				public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
					PreparedStatement sqlAlterar = arg0.prepareStatement(sql);
					sqlAlterar.setString(1, obj.getCodigoIBGE());
					sqlAlterar.setInt(2, obj.getCodigo().intValue());
					return sqlAlterar;
				}
			});

		} catch (Exception e) {
			throw e;
		}
	}

	
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void excluir(CidadeVO obj, UsuarioVO usuarioVO) throws Exception {
		try {
			Cidade.excluir(getIdEntidade(), true, usuarioVO);
			String sql = "DELETE FROM Cidade WHERE ((codigo = ?))";
			getConexao().getJdbcTemplate().update(sql, new Object[] { obj.getCodigo() });
		} catch (Exception e) {
			throw e;
		}
	}

//	public List consultarPorEstado(String valorConsulta, boolean controlarAcesso, UsuarioVO usuario) throws Exception {
//		ControleAcesso.consultar(getIdEntidade(), controlarAcesso, usuario);
//		String sqlStr = "SELECT * FROM Cidade INNER JOIN estado on estado.codigo = cidade.estado WHERE lower (estado.nome) like('" + valorConsulta.toLowerCase() + "%') ORDER BY estado";
//		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sqlStr);
//		return (montarDadosConsulta(tabelaResultado, Uteis.NIVELMONTARDADOS_DADOSBASICOS, usuario));
//
//	}
//
//	
//	public List consultarPorNome(String valorConsulta, boolean controlarAcesso, UsuarioVO usuario) throws Exception {
//		ControleAcesso.consultar(getIdEntidade(), controlarAcesso, usuario);
//		String sqlStr = "SELECT * FROM Cidade WHERE lower (sem_acentos(nome)) ilike(sem_acentos('" + valorConsulta.toLowerCase() + "%')) ORDER BY nome";
//		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sqlStr);
//		return (montarDadosConsulta(tabelaResultado, Uteis.NIVELMONTARDADOS_DADOSBASICOS, usuario));
//	}
//
//	public List consultarPorNomeCodigoEstado(String valorConsulta, boolean controlarAcesso, Integer estado, UsuarioVO usuario) throws Exception {
//		ControleAcesso.consultar(getIdEntidade(), controlarAcesso, usuario);
//		String sqlStr = "SELECT * FROM Cidade WHERE lower (nome) like('" + valorConsulta.toLowerCase() + "%') AND estado = " + estado + " ORDER BY nome";
//		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sqlStr);
//		return (montarDadosConsulta(tabelaResultado, Uteis.NIVELMONTARDADOS_DADOSBASICOS, usuario));
//	}
//
//	
//	public List consultarPorCodigo(Integer valorConsulta, boolean controlarAcesso, UsuarioVO usuario) throws Exception {
//		ControleAcesso.consultar(getIdEntidade(), controlarAcesso, usuario);
//		String sqlStr = "SELECT * FROM Cidade WHERE codigo = " + valorConsulta.intValue() + " ORDER BY codigo";
//		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sqlStr);
//		return (montarDadosConsulta(tabelaResultado, Uteis.NIVELMONTARDADOS_DADOSBASICOS, usuario));
//	}
//
//	public List consultarPorCodigoEstado(Integer valorConsulta, boolean controlarAcesso, UsuarioVO usuario) throws Exception {
//		ControleAcesso.consultar(getIdEntidade(), controlarAcesso, usuario);
//		String sqlStr = "SELECT * FROM Cidade INNER JOIN Estado ON cidade.estado = estado.codigo WHERE estado.codigo = " + valorConsulta.intValue() + " ORDER BY cidade.nome";
//		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sqlStr);
//		return (montarDadosConsulta(tabelaResultado, Uteis.NIVELMONTARDADOS_DADOSBASICOS, usuario));
//	}

	
	private StringBuffer getSQLPadraoConsultaBasica() {
		StringBuffer str = new StringBuffer();
		str.append("SELECT cidade.codigo, cidade.nome, estado.sigla AS \"estado.sigla\", estado.nome AS \"estado.nome\", estado.codigo AS \"estado.codigo\" FROM cidade ");
		str.append("INNER JOIN estado ON estado.codigo = cidade.estado ");
		return str;
	}

	public List<CidadeVO> consultaRapidaPorNomeAutoComplete(String valorConsulta, int limit, boolean controlarAcesso, int nivelMontarDados, UsuarioVO usuario) throws Exception {
		ControleAcesso.consultar(getIdEntidade(), controlarAcesso, usuario);
		StringBuffer sqlStr = new StringBuffer();
		sqlStr.append("select distinct cidade.codigo, cidade.nome ");
		sqlStr.append("from cidade ");
		sqlStr.append("WHERE lower(cidade.nome) like('%");
		sqlStr.append(valorConsulta.toLowerCase());
		sqlStr.append("%') ");
		sqlStr.append(" ORDER BY cidade.nome ");
		sqlStr.append(" limit ").append(limit);
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sqlStr.toString());
		sqlStr = null;
		List<CidadeVO> listaCidades = new ArrayList<CidadeVO>();
		while (tabelaResultado.next()) {
			CidadeVO obj = new CidadeVO();
			obj.setCodigo(new Integer(tabelaResultado.getInt("codigo")));
			obj.setNome(tabelaResultado.getString("nome"));
			listaCidades.add(obj);
		}
		return listaCidades;
	}

	public List<CidadeVO> consultaRapidaPorCodigo(Integer codCidade, boolean controlarAcesso, UsuarioVO usuario) throws Exception {
		ControleAcesso.consultar(getIdEntidade(), controlarAcesso, usuario);
		StringBuffer sqlStr = getSQLPadraoConsultaBasica();
		sqlStr.append("WHERE cidade.codigo = ");
		sqlStr.append(codCidade.intValue());
		sqlStr.append(" ORDER BY cidade.codigo");
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sqlStr.toString());
		return montarDadosConsultaRapida(tabelaResultado);
	}

	
	public List<CidadeVO> consultaRapidaPorNome(String valorConsulta, boolean controlarAcesso, UsuarioVO usuario) throws Exception {
		ControleAcesso.consultar(getIdEntidade(), controlarAcesso, usuario);
		StringBuffer sqlStr = getSQLPadraoConsultaBasica();
		sqlStr.append("WHERE lower(sem_acentos(cidade.nome)) ilike(sem_acentos('");
		sqlStr.append(valorConsulta.toLowerCase());
		sqlStr.append("%'))");
		sqlStr.append(" ORDER BY cidade.nome");
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sqlStr.toString());
		return montarDadosConsultaRapida(tabelaResultado);
	}

	public CidadeVO consultarCidadeUnidadeEnsinoPorMatricula(String valorConsulta, boolean controlarAcesso, int nivelMontarDados, UsuarioVO usuario) throws Exception {
		ControleAcesso.consultar(getIdEntidade(), controlarAcesso, usuario);
		StringBuffer sqlStr = getSQLPadraoConsultaBasica();
		sqlStr.append("inner join unidadeEnsino on unidadeEnsino.cidade = cidade.codigo ");
		sqlStr.append("inner join matricula on matricula.unidadeensino = unidadeensino.codigo ");
		sqlStr.append("where lower(matricula) = '");
		sqlStr.append(valorConsulta.toLowerCase());
		sqlStr.append("'");
		sqlStr.append(" ORDER BY cidade.nome");
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sqlStr.toString());
		if (!tabelaResultado.next()) {
			return null;
		}
		CidadeVO cidadeVO = new CidadeVO();
		montarDadosBasico(cidadeVO, tabelaResultado);
		return cidadeVO;
	}

	public CidadeVO consultarCidadePorUnidadeEnsino(Integer valorConsulta, boolean controlarAcesso, int nivelMontarDados, UsuarioVO usuario) throws Exception {
		ControleAcesso.consultar(getIdEntidade(), controlarAcesso, usuario);
		StringBuffer sqlStr = getSQLPadraoConsultaBasica();
		sqlStr.append("inner join unidadeEnsino on unidadeEnsino.cidade = cidade.codigo ");
		sqlStr.append("where unidadeEnsino.codigo = ");
		sqlStr.append(valorConsulta);
		sqlStr.append(" ORDER BY cidade.nome");
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sqlStr.toString());
		if (!tabelaResultado.next()) {
			return null;
		}
		CidadeVO cidadeVO = new CidadeVO();
		montarDadosBasico(cidadeVO, tabelaResultado);
		return cidadeVO;
	}

	public List<CidadeVO> montarDadosConsultaRapida(SqlRowSet tabelaResultado) throws Exception {
		List<CidadeVO> vetResultado = new ArrayList<CidadeVO>(0);
		while (tabelaResultado.next()) {
			CidadeVO obj = new CidadeVO();
			montarDadosBasico(obj, tabelaResultado);
			vetResultado.add(obj);
		}
		return vetResultado;
	}

	
	private void montarDadosBasico(CidadeVO obj, SqlRowSet dadosSQL) throws Exception {
		obj.setCodigo(new Integer(dadosSQL.getInt("codigo")));
		obj.setNome(dadosSQL.getString("nome"));

		obj.getEstado().setCodigo(new Integer(dadosSQL.getInt("estado.codigo")));
		obj.getEstado().setNome(dadosSQL.getString("estado.nome"));
		obj.getEstado().setSigla(dadosSQL.getString("estado.sigla"));
	}

	
	public static List montarDadosConsulta(SqlRowSet tabelaResultado, int nivelMontarDados, UsuarioVO usuario) throws Exception {
		List vetResultado = new ArrayList(0);
		while (tabelaResultado.next()) {
			vetResultado.add(montarDados(tabelaResultado, usuario));
		}
		return vetResultado;
	}

	
	public static CidadeVO montarDados(SqlRowSet dadosSQL, UsuarioVO usuario) throws Exception {
		CidadeVO obj = new CidadeVO();
		obj.setCodigo(new Integer(dadosSQL.getInt("codigo")));
		obj.setNome(dadosSQL.getString("nome"));
		obj.setCep(dadosSQL.getString("cep"));
		obj.getEstado().setCodigo(new Integer(dadosSQL.getInt("estado")));
		obj.setCodigoInep(dadosSQL.getInt("codigoinep"));
		obj.setCodigoIBGE(dadosSQL.getString("codigoibge"));
		obj.setCodigoDistrito(dadosSQL.getInt("codigoDistrito"));
		montarDadosEstado(obj, usuario);
		obj.setNovoObj(Boolean.FALSE);
		return obj;
	}

	
	public static void montarDadosEstado(CidadeVO obj, UsuarioVO usuario) throws Exception {
		if (obj.getEstado().getCodigo().intValue() == 0) {
			obj.setEstado(new EstadoVO());
			return;
		}
		obj.setEstado(getFacadeFactory().getEstadoFacade().consultarPorChavePrimaria(obj.getEstado().getCodigo(), usuario));
	}


	
	
	public static String getIdEntidade() {
		return Cidade.idEntidade;
	}

	
	public void setIdEntidade(String idEntidade) {
		Cidade.idEntidade = idEntidade;
	}


	/* (non-Javadoc)
	 * @see negocio.interfaces.basico.CidadeInterfaceFacade#consultarPorChavePrimaria(java.lang.Integer, boolean, negocio.comuns.basico.UsuarioVO)
	 */
	@Override
	public CidadeVO consultarPorChavePrimaria(Integer codigo, boolean controlarAcesso, UsuarioVO usuario) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}


	/* (non-Javadoc)
	 * @see negocio.interfaces.basico.CidadeInterfaceFacade#consultarPorCodigo(java.lang.Integer, boolean, negocio.comuns.basico.UsuarioVO)
	 */
	@Override
	public List consultarPorCodigo(Integer valorConsulta, boolean controlarAcesso, UsuarioVO usuario) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}


	/* (non-Javadoc)
	 * @see negocio.interfaces.basico.CidadeInterfaceFacade#consultarPorNome(java.lang.String, boolean, negocio.comuns.basico.UsuarioVO)
	 */
	@Override
	public List consultarPorNome(String valorConsulta, boolean controlarAcesso, UsuarioVO usuario) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}


	/* (non-Javadoc)
	 * @see negocio.interfaces.basico.CidadeInterfaceFacade#consultarPorNomeCodigoEstado(java.lang.String, boolean, java.lang.Integer, negocio.comuns.basico.UsuarioVO)
	 */
	@Override
	public List consultarPorNomeCodigoEstado(String valorConsulta, boolean controlarAcesso, Integer estado, UsuarioVO usuario) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}


	/* (non-Javadoc)
	 * @see negocio.interfaces.basico.CidadeInterfaceFacade#consultarPorSiglaEstado(java.lang.String, boolean, int, negocio.comuns.basico.UsuarioVO)
	 */
	@Override
	public List consultarPorSiglaEstado(String valorConsulta, boolean controlarAcesso, int nivelMontarDados, UsuarioVO usuario) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}


	/* (non-Javadoc)
	 * @see negocio.interfaces.basico.CidadeInterfaceFacade#consultarPorCodigoEstado(java.lang.Integer, boolean, negocio.comuns.basico.UsuarioVO)
	 */
	@Override
	public List consultarPorCodigoEstado(Integer valorConsulta, boolean controlarAcesso, UsuarioVO usuario) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}


	/* (non-Javadoc)
	 * @see negocio.interfaces.basico.CidadeInterfaceFacade#consultarPorCep(java.lang.String, boolean, int, negocio.comuns.basico.UsuarioVO)
	 */
	@Override
	public List consultarPorCep(String valorConsulta, boolean controlarAcesso, int nivelMontarDados, UsuarioVO usuario) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}


	/* (non-Javadoc)
	 * @see negocio.interfaces.basico.CidadeInterfaceFacade#consultarPorEstado(java.lang.String, boolean, negocio.comuns.basico.UsuarioVO)
	 */
	@Override
	public List consultarPorEstado(String valorConsulta, boolean controlarAcesso, UsuarioVO usuario) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

}
