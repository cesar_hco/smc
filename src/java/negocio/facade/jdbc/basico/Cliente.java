package negocio.facade.jdbc.basico;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.comuns.basico.ClienteVO;
import negocio.facade.jdbc.arquitetura.ControleAcesso;
import negocio.interfaces.basico.ClienteInterfaceFacade;

@Repository
@Scope("singleton")
@Lazy
public class Cliente extends ControleAcesso implements ClienteInterfaceFacade {

	private static final long serialVersionUID = 1L;

	public Cliente() {
		super();
	}
	
	public void validarDados(ClienteVO obj, UsuarioVO usuarioVO) throws Exception {
		if (obj.getPessoaVO().getNome().equals("")) {
			throw new Exception("O campo NOME deve ser informado!");
		}
	}

	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void persistir(ClienteVO obj, UsuarioVO usuarioVO) throws Exception {
		if (obj.isNovoObj()) {
			incluir(obj, usuarioVO);
		} else {
			alterar(obj, usuarioVO);
		}
	}

	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void incluir(final ClienteVO obj, UsuarioVO usuario) throws Exception {
		try {
			validarDados(obj, usuario);
			getFacadeFactory().getPessoaFacade().incluirCliente(obj.getPessoaVO(), usuario);
			final String sql = "INSERT INTO Cliente( pessoa, observacao, limiteCredito, percentualMaximoDesconto, icms, percentualLimiteCliente) "
					+ " VALUES ( ?, ?, ?, ?, ?, ?) returning codigo";
			obj.setCodigo((Integer) getConexao().getJdbcTemplate().query(new PreparedStatementCreator() {

				public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
					PreparedStatement sqlInserir = arg0.prepareStatement(sql);
					if (obj.getPessoaVO().getCodigo().intValue() != 0) {
						sqlInserir.setInt(1, obj.getPessoaVO().getCodigo().intValue());
					} else {
						sqlInserir.setNull(1, 0);
					}
					sqlInserir.setString(2, obj.getObservacao());
					sqlInserir.setBigDecimal(3, obj.getLimiteCredito());
					sqlInserir.setBigDecimal(4, obj.getPercentualMaximoDesconto());
					sqlInserir.setBigDecimal(5, obj.getIcms());
					sqlInserir.setBigDecimal(6, obj.getPercentualLimiteCliente());
					return sqlInserir;
				}
			}, new ResultSetExtractor() {

				public Object extractData(ResultSet arg0) throws SQLException, DataAccessException {
					if (arg0.next()) {
						obj.setNovoObj(Boolean.FALSE);
						return arg0.getInt("codigo");
					}
					return null;
				}
			}));
			
			obj.setNovoObj(Boolean.FALSE);
		} catch (Exception e) {
			obj.setNovoObj(Boolean.TRUE);
			obj.getPessoaVO().setNovoObj(false);
			throw e;
		}
	}

	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void alterar(final ClienteVO obj, UsuarioVO usuario) throws Exception {
		try {
			validarDados(obj, usuario);
			getFacadeFactory().getPessoaFacade().alterar(obj.getPessoaVO(), usuario);
			final String sql = "UPDATE Cliente set pessoa=?, observacao=?, limiteCredito=?, percentualMaximoDesconto=?, icms=?, percentualLimiteCliente=? WHERE ((codigo = ?))";

			getConexao().getJdbcTemplate().update(new PreparedStatementCreator() {

				public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
					PreparedStatement sqlAlterar = arg0.prepareStatement(sql);
					if (obj.getPessoaVO().getCodigo().intValue() != 0) {
						sqlAlterar.setInt(1, obj.getPessoaVO().getCodigo().intValue());
					} else {
						sqlAlterar.setNull(1, 0);
					}
					sqlAlterar.setString(2, obj.getObservacao());
					sqlAlterar.setBigDecimal(3, obj.getLimiteCredito());
					sqlAlterar.setBigDecimal(4, obj.getPercentualMaximoDesconto());
					sqlAlterar.setBigDecimal(5, obj.getIcms());
					sqlAlterar.setBigDecimal(6, obj.getPercentualLimiteCliente());
					sqlAlterar.setInt(7, obj.getCodigo().intValue());
					return sqlAlterar;
				}
			});
			
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void excluir(ClienteVO obj, UsuarioVO usuario) throws Exception {
		try {
			String sql = "DELETE FROM Cliente WHERE ((codigo = ?))";
			getConexao().getJdbcTemplate().update(sql, new Object[] { obj.getCodigo() });
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public List<ClienteVO> consultar(String campoConsulta, String valorConsulta, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		List<ClienteVO> listaConsulta = new ArrayList<ClienteVO>(0);
		if (campoConsulta.equals("NOME")) {
			listaConsulta = consultarPorNome(valorConsulta, nivelMontarDados, usuarioVO);
		}
		return listaConsulta;
	}

	public List<ClienteVO> consultarPorNome(String nome, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		StringBuilder sb = new StringBuilder();
		sb.append(getDadosBasicos());
		sb.append(" where pessoa.nome ilike ('").append(nome).append("%') ");
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
		return montarDadosLista(tabelaResultado, nivelMontarDados, usuarioVO);
	}
	
	@Override
	public void carregarDados(ClienteVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		if (nivelMontarDados.equals(NivelMontarDadosEnum.BASICO)) {
			StringBuilder sb = getDadosBasicos();
			sb.append(" where membro.codigo = ").append(obj.getCodigo());
			SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
			if (tabelaResultado.next()) {
				montarDadosBasicos(tabelaResultado, obj, nivelMontarDados, usuarioVO);
			}
		}
		if (nivelMontarDados.equals(NivelMontarDadosEnum.COMPLETO)) {
			StringBuilder sb = getDadosCompletos();
			sb.append(" where membro.codigo = ").append(obj.getCodigo());
			SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
			if (tabelaResultado.next()) {
				montarDadosCompletos(tabelaResultado, obj, nivelMontarDados, usuarioVO);
			}
		}
	}
	
	public List<ClienteVO> montarDadosLista(SqlRowSet dadosSQL, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		List<ClienteVO> listaMembroVOs = new ArrayList<ClienteVO>(0);
		while (dadosSQL.next()) {
			ClienteVO obj = new ClienteVO();
			montarDadosBasicos(dadosSQL, obj, nivelMontarDados, usuarioVO);
			listaMembroVOs.add(obj);
		}
		return listaMembroVOs;
	}
	
	public StringBuilder getDadosBasicos() {
		StringBuilder sb = new StringBuilder();
		sb.append("select cliente.codigo, cliente.observacao, cliente.limiteCredito, cliente.percentualMaximoDesconto, cliente.icms, cliente.percentualLimiteCliente, ");
		sb.append(" pessoa.codigo AS \"pessoa.codigo\",  pessoa.nome AS \"pessoa.nome\", pessoa.email AS \"pessoa.email\", ");
		sb.append(" pessoa.dataNascimento AS \"pessoa.dataNascimento\", pessoa.cpf AS \"pessoa.cpf\", pessoa.rg AS \"pessoa.rg\", pessoa.celular AS \"pessoa.celular\",  ");
		sb.append(" pessoa.telefone1 AS \"pessoa.telefone1\", pessoa.telefone2 AS \"pessoa.telefone2\", pessoa.cep AS \"pessoa.cep\", pessoa.endereco AS \"pessoa.endereco\", pessoa.setor AS \"pessoa.setor\", ");
		sb.append(" cidade.codigo AS \"cidade.codigo\", cidade.nome AS \"cidade.nome\" ");
		sb.append(" from cliente ");
		sb.append(" inner join pessoa on pessoa.codigo = cliente.pessoa ");
		sb.append(" left join cidade on cidade.codigo = pessoa.cidade ");
		return sb;
	}
	
	public StringBuilder getDadosCompletos() {
		StringBuilder sb = new StringBuilder();
		sb.append("select cliente.codigo, cliente.observacao, cliente.limiteCredito, cliente.percentualMaximoDesconto, cliente.icms, cliente.percentualLimiteCliente, ");
		sb.append(" pessoa.codigo AS \"pessoa.codigo\",  pessoa.nome AS \"pessoa.nome\", pessoa.email AS \"pessoa.email\", ");
		sb.append(" pessoa.dataNascimento AS \"pessoa.dataNascimento\", pessoa.cpf AS \"pessoa.cpf\", pessoa.rg AS \"pessoa.rg\", pessoa.celular AS \"pessoa.celular\",  ");
		sb.append(" pessoa.telefone1 AS \"pessoa.telefone1\", pessoa.telefone2 AS \"pessoa.telefone2\", pessoa.cep AS \"pessoa.cep\", pessoa.endereco AS \"pessoa.endereco\", pessoa.setor AS \"pessoa.setor\", ");
		sb.append(" cidade.codigo AS \"cidade.codigo\", cidade.nome AS \"cidade.nome\" ");
		sb.append(" from cliente ");
		sb.append(" inner join pessoa on pessoa.codigo = cliente.pessoa ");
		sb.append(" left join cidade on cidade.codigo = pessoa.cidade ");
		return sb;
	}
	
	public void montarDadosBasicos(SqlRowSet dadosSQL, ClienteVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		obj.setCodigo(dadosSQL.getInt("codigo"));
		obj.setObservacao(dadosSQL.getString("observacao"));
		obj.setLimiteCredito(dadosSQL.getBigDecimal("limiteCredito"));
		obj.setPercentualMaximoDesconto(dadosSQL.getBigDecimal("percentualMaximoDesconto"));
		obj.setIcms(dadosSQL.getBigDecimal("icms"));
		obj.setPercentualLimiteCliente(dadosSQL.getBigDecimal("percentualLimiteCliente"));
		
		//Dados Pessoa
		obj.getPessoaVO().setCodigo(dadosSQL.getInt("pessoa.codigo"));
		obj.getPessoaVO().setNome(dadosSQL.getString("pessoa.nome"));
		obj.getPessoaVO().setDataNascimento(dadosSQL.getDate("pessoa.dataNascimento"));
		obj.getPessoaVO().setCpf(dadosSQL.getString("pessoa.cpf"));
		obj.getPessoaVO().setRg(dadosSQL.getString("pessoa.rg"));
		obj.getPessoaVO().setCelular(dadosSQL.getString("pessoa.celular"));
		obj.getPessoaVO().setTelefone1(dadosSQL.getString("pessoa.telefone1"));
		obj.getPessoaVO().setTelefone2(dadosSQL.getString("pessoa.telefone2"));
		obj.getPessoaVO().setCep(dadosSQL.getString("pessoa.cep"));
		obj.getPessoaVO().setEndereco(dadosSQL.getString("pessoa.endereco"));
		obj.getPessoaVO().setSetor(dadosSQL.getString("pessoa.setor"));
		obj.getPessoaVO().getCidadeVO().setCodigo(dadosSQL.getInt("cidade.codigo"));
		obj.getPessoaVO().getCidadeVO().setNome(dadosSQL.getString("cidade.nome"));
		obj.getPessoaVO().setEmail(dadosSQL.getString("pessoa.email"));
		obj.setNovoObj(false);
	}
	
	public void montarDadosCompletos(SqlRowSet dadosSQL, ClienteVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		obj.setCodigo(dadosSQL.getInt("codigo"));
		obj.setObservacao(dadosSQL.getString("observacao"));
		obj.setLimiteCredito(dadosSQL.getBigDecimal("limiteCredito"));
		obj.setPercentualMaximoDesconto(dadosSQL.getBigDecimal("percentualMaximoDesconto"));
		obj.setIcms(dadosSQL.getBigDecimal("icms"));
		obj.setPercentualLimiteCliente(dadosSQL.getBigDecimal("percentualLimiteCliente"));
		
		//Dados Pessoa
		obj.getPessoaVO().setCodigo(dadosSQL.getInt("pessoa.codigo"));
		obj.getPessoaVO().setNome(dadosSQL.getString("pessoa.nome"));
		obj.getPessoaVO().setDataNascimento(dadosSQL.getDate("pessoa.dataNascimento"));
		obj.getPessoaVO().setCpf(dadosSQL.getString("pessoa.cpf"));
		obj.getPessoaVO().setRg(dadosSQL.getString("pessoa.rg"));
		obj.getPessoaVO().setCelular(dadosSQL.getString("pessoa.celular"));
		obj.getPessoaVO().setTelefone1(dadosSQL.getString("pessoa.telefone1"));
		obj.getPessoaVO().setTelefone2(dadosSQL.getString("pessoa.telefone2"));
		obj.getPessoaVO().setCep(dadosSQL.getString("pessoa.cep"));
		obj.getPessoaVO().setEndereco(dadosSQL.getString("pessoa.endereco"));
		obj.getPessoaVO().setSetor(dadosSQL.getString("pessoa.setor"));
		obj.getPessoaVO().getCidadeVO().setCodigo(dadosSQL.getInt("cidade.codigo"));
		obj.getPessoaVO().getCidadeVO().setNome(dadosSQL.getString("cidade.nome"));
		obj.getPessoaVO().setEmail(dadosSQL.getString("pessoa.email"));
		obj.setNovoObj(false);
		if (nivelMontarDados.equals(NivelMontarDadosEnum.TELA_CONSULTA)) {
			return;
		}
		return;
	}

}
