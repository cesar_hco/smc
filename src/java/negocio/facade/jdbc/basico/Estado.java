package negocio.facade.jdbc.basico;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.basico.EstadoVO;
import negocio.comuns.basico.PaizVO;
import negocio.comuns.basico.enumeradores.RegiaoEnum;
import negocio.facade.jdbc.arquitetura.ControleAcesso;
import negocio.interfaces.basico.EstadoInterfaceFacade;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


/**
 * Classe de persist�ncia que encapsula todas as opera��es de manipula��o dos dados da classe <code>EstadoVO</code>.
 * Respons�vel por implementar opera��es como incluir, alterar, excluir e consultar pertinentes a classe <code>EstadoVO</code>.
 * Encapsula toda a intera��o com o banco de dados.
 * @see EstadoVO
 * @see ControleAcesso
 */

@Repository @Scope("singleton") @Lazy 
public class Estado extends ControleAcesso implements EstadoInterfaceFacade {

    /**
	 * 
	 */
	private static final long serialVersionUID = -2147711325199635532L;
	protected static String idEntidade;

    public Estado() throws Exception {
        super();
        setIdEntidade("Estado");
    }

    
    public EstadoVO novo() throws Exception {
        Estado.incluir(getIdEntidade());
        EstadoVO obj = new EstadoVO();
        return obj;
    }

  
    @Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public void incluir(final EstadoVO obj, UsuarioVO usuarioVO) throws Exception {
        try {
            EstadoVO.validarDados(obj);
            Estado.incluir(getIdEntidade(), true, usuarioVO);
            final String sql = "INSERT INTO Estado( sigla, nome, paiz, codigoinep, regiao ) VALUES ( ?, ?, ?, ?, ? ) returning codigo";
             obj.setCodigo((Integer) getConexao().getJdbcTemplate().query(new PreparedStatementCreator() {
                public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
     		  PreparedStatement sqlInserir = arg0.prepareStatement(sql);
                  sqlInserir.setString(1, obj.getSigla());
                  sqlInserir.setString(2, obj.getNome());
                  if (obj.getPaiz().getCodigo().intValue() != 0) {
                      sqlInserir.setInt(3, obj.getPaiz().getCodigo().intValue());
                  } else {
                      sqlInserir.setNull(3, 0);
                  }
                  sqlInserir.setInt(4, obj.getCodigoInep());
                  if(obj.getRegiao() != null){
                	  sqlInserir.setString(5, obj.getRegiao().name());
                  }else{
                	  sqlInserir.setNull(5, 0);
                  }
                  return sqlInserir;
                }

             }, new ResultSetExtractor<Integer>() {
                public Integer extractData(ResultSet arg0) throws SQLException, DataAccessException {
                        if (arg0.next()) {
                                obj.setNovoObj(Boolean.FALSE);
                                return arg0.getInt("codigo");
                        }
                        return null;
                }


             }));

            obj.setNovoObj(Boolean.FALSE);
        } catch (Exception e) {
            obj.setNovoObj(Boolean.TRUE);
            throw e;
        }
    }

   

    @Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public void alterar(final EstadoVO obj, UsuarioVO usuarioVO) throws Exception {
        try {
            EstadoVO.validarDados(obj);
            Estado.alterar(getIdEntidade(), true, usuarioVO);
            final String sql = "UPDATE Estado set sigla=?, nome=?, paiz=?, codigoinep=?, regiao = ? WHERE ((codigo = ?))";
            getConexao().getJdbcTemplate().update(new PreparedStatementCreator() {
                public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
                        PreparedStatement sqlAlterar = arg0.prepareStatement(sql);
                        sqlAlterar.setString(1, obj.getSigla());
                        sqlAlterar.setString(2, obj.getNome());
                        if (obj.getPaiz().getCodigo().intValue() != 0) {
                            sqlAlterar.setInt(3, obj.getPaiz().getCodigo().intValue());
                        } else {
                            sqlAlterar.setNull(3, 0);
                        }
                        sqlAlterar.setInt(4, obj.getCodigoInep());                        
                        if(obj.getRegiao() != null){
                      	  sqlAlterar.setString(5, obj.getRegiao().name());
                        }else{
                        	sqlAlterar.setNull(5, 0);
                        }
                        sqlAlterar.setInt(6, obj.getCodigo().intValue());
                        return sqlAlterar;
                }

            });

        } catch (Exception e) {
            throw e;
       }
    }

    

    @Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public void excluir(EstadoVO obj, UsuarioVO usuarioVO) throws Exception {
        try {
            Estado.excluir(getIdEntidade(), true, usuarioVO);
            String sql = "DELETE FROM Estado WHERE ((codigo = ?))";
            getConexao().getJdbcTemplate().update(sql, new Object[] {obj.getCodigo()});
        } catch (Exception e) {
            throw e;
        }
    }

   
    public List<EstadoVO> consultarPorCodigoPaiz(Integer valorConsulta, boolean controlarAcesso, int nivelMontarDados,UsuarioVO usuario) throws Exception {
        ControleAcesso.consultar(getIdEntidade(), controlarAcesso, usuario);
        StringBuilder sqlStr = new StringBuilder("SELECT Estado.* FROM Estado, Paiz WHERE Estado.paiz = Paiz.codigo  ");
        if(valorConsulta != null && valorConsulta > 0){
        	sqlStr.append(" and Paiz.codigo = " + valorConsulta + "");
        }
        sqlStr.append(" ORDER BY Paiz.nome, Estado.sigla");
        SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sqlStr.toString());
        return montarDadosConsulta(tabelaResultado, nivelMontarDados,usuario);

    }

   
    public List<EstadoVO> consultarPorNomePaiz(String valorConsulta, boolean controlarAcesso, int nivelMontarDados,UsuarioVO usuario) throws Exception {
        ControleAcesso.consultar(getIdEntidade(), controlarAcesso, usuario);
        String sqlStr = "SELECT Estado.* FROM Estado, Paiz WHERE Estado.paiz = Paiz.codigo and upper( Paiz.nome ) like('" + valorConsulta.toUpperCase() + "%') ORDER BY Paiz.nome";
        SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sqlStr);
        return montarDadosConsulta(tabelaResultado, nivelMontarDados,usuario);
    }

    
    public List<EstadoVO> consultarPorNome(String valorConsulta, boolean controlarAcesso, int nivelMontarDados,UsuarioVO usuario) throws Exception {
        ControleAcesso.consultar(getIdEntidade(), controlarAcesso, usuario);
        String sqlStr = "SELECT * FROM Estado WHERE upper( nome ) like('" + valorConsulta.toUpperCase() + "%') ORDER BY nome";
        SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sqlStr);
        return montarDadosConsulta(tabelaResultado, nivelMontarDados,usuario);
    }

   
    public List<EstadoVO> consultarPorSigla(String valorConsulta, boolean controlarAcesso, int nivelMontarDados,UsuarioVO usuario) throws Exception {
        ControleAcesso.consultar(getIdEntidade(), controlarAcesso, usuario);
        String sqlStr = "SELECT * FROM Estado WHERE upper( sigla ) like('" + valorConsulta.toUpperCase() + "%') ORDER BY sigla";
        SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sqlStr);
        return montarDadosConsulta(tabelaResultado, nivelMontarDados,usuario);
    }

   
    public List<EstadoVO> consultarPorCodigo(Integer valorConsulta, boolean controlarAcesso, int nivelMontarDados,UsuarioVO usuario) throws Exception {
        ControleAcesso.consultar(getIdEntidade(), controlarAcesso, usuario);
        String sqlStr = "SELECT * FROM Estado WHERE codigo = " + valorConsulta.intValue() + " ORDER BY codigo";
        SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sqlStr);
        return montarDadosConsulta(tabelaResultado, nivelMontarDados,usuario);
    }
    
    
    public EstadoVO consultarPorCodigoCidade(Integer valorConsulta, boolean controlarAcesso, int nivelMontarDados,UsuarioVO usuario) throws Exception {
        ControleAcesso.consultar(getIdEntidade(), controlarAcesso, usuario);
        String sql = "SELECT * FROM estado, cidade WHERE cidade.estado = estado.codigo AND cidade.codigo = " + valorConsulta;
        SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sql);
        if (!tabelaResultado.next()) {
            throw new Exception("Dados N�o Encontrados ( Estado ).");
        }
        return montarDados(tabelaResultado, usuario);
    }

    /**
     * Respons�vel por montar os dados de v�rios objetos, resultantes de uma consulta ao banco de dados (<code>ResultSet</code>).
     * Faz uso da opera��o <code>montarDados</code> que realiza o trabalho para um objeto por vez.
     * @return  List Contendo v�rios objetos da classe <code>EstadoVO</code> resultantes da consulta.
     */
    public static List<EstadoVO> montarDadosConsulta(SqlRowSet tabelaResultado, int nivelMontarDados,UsuarioVO usuario) throws Exception {
        List<EstadoVO> vetResultado = new ArrayList<EstadoVO>(0);
        while (tabelaResultado.next()) {
            vetResultado.add(montarDados(tabelaResultado, usuario));
        }
        tabelaResultado = null;
        return vetResultado;
    }

    /**
     * Respons�vel por montar os dados resultantes de uma consulta ao banco de dados (<code>ResultSet</code>)
     * em um objeto da classe <code>EstadoVO</code>.
     * @return  O objeto da classe <code>EstadoVO</code> com os dados devidamente montados.
     */
    public static EstadoVO montarDados(SqlRowSet dadosSQL, UsuarioVO usuario) throws Exception {
        EstadoVO obj = new EstadoVO();
        obj.setCodigo(new Integer(dadosSQL.getInt("codigo")));
        obj.setSigla(dadosSQL.getString("sigla"));
        obj.setNome(dadosSQL.getString("nome"));
        obj.getPaiz().setCodigo(new Integer(dadosSQL.getInt("paiz")));
        String regiao = dadosSQL.getString("regiao");
        if ((regiao != null) && 
            (!regiao.trim().isEmpty()) &&
            (!obj.getSigla().trim().isEmpty())) {
            try {
                obj.setRegiao(RegiaoEnum.valueOf(regiao));
            } catch (Exception e) {
            }
        }
        obj.setCodigoInep(dadosSQL.getInt("codigoinep"));
        obj.setCodigoIBGE(dadosSQL.getString("codigoibge"));
        obj.setNovoObj(Boolean.FALSE);
      
        montarDadosPaiz(obj,usuario);
        return obj;
    }

    /**
     * Opera��o respons�vel por montar os dados de um objeto da classe <code>PaizVO</code> relacionado ao objeto <code>EstadoVO</code>.
     * Faz uso da chave prim�ria da classe <code>PaizVO</code> para realizar a consulta.
     * @param obj  Objeto no qual ser� montado os dados consultados.
     */
    public static void montarDadosPaiz(EstadoVO obj,UsuarioVO usuario) throws Exception {
        if (obj.getPaiz().getCodigo().intValue() == 0) {
            obj.setPaiz(new PaizVO());
            return;
        }
        obj.setPaiz(getFacadeFactory().getPaizFacade().consultarPorChavePrimaria(obj.getPaiz().getCodigo(), false,usuario));
    }

    /**
     * Opera��o respons�vel por localizar um objeto da classe <code>EstadoVO</code>
     * atrav�s de sua chave prim�ria. 
     * @exception Exception Caso haja problemas de conex�o ou localiza��o do objeto procurado.
     */
    public EstadoVO consultarPorChavePrimaria(Integer codigoPrm, UsuarioVO usuario) throws Exception {
        ControleAcesso.consultar(getIdEntidade(), false,usuario);
        String sql = "SELECT * FROM Estado WHERE codigo = " + codigoPrm;
        SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sql);
        if (!tabelaResultado.next()) {
            throw new Exception("Dados N�o Encontrados ( Estado ).");
        }
        return montarDados(tabelaResultado, usuario);
    }


    /**
     * Opera��o repons�vel por retornar o identificador desta classe.
     * Este identificar � utilizado para verificar as permiss�es de acesso as opera��es desta classe.
     */
    public static String getIdEntidade() {
        return Estado.idEntidade;
    }

    /**
     * Opera��o repons�vel por definir um novo valor para o identificador desta classe.
     * Esta altera��o deve ser poss�vel, pois, uma mesma classe de neg�cio pode ser utilizada com objetivos
     * distintos. Assim ao se verificar que Como o controle de acesso � realizado com base neste identificador, 
     */
    public void setIdEntidade(String idEntidade) {
        Estado.idEntidade = idEntidade;
    }


}