package negocio.facade.jdbc.basico;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.basico.PaizVO;
import negocio.facade.jdbc.arquitetura.ControleAcesso;
import negocio.interfaces.basico.PaizInterfaceFacade;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Scope("singleton")
@Lazy 
public class Paiz extends ControleAcesso implements PaizInterfaceFacade {

    protected static String idEntidade;

    public Paiz() throws Exception {
        super();
        setIdEntidade("Pais");
    }

    public void validarDados(PaizVO obj) throws Exception {
    	
    }

  
    @Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public void incluir(final PaizVO obj,UsuarioVO usuario) throws Exception {
        try {
            Paiz.incluir(getIdEntidade(), true,usuario);
            validarDados(obj);
            final String sql = "INSERT INTO Paiz( nome, nacao, siglainep, nacionalidade ) VALUES ( ?, ?, ?, ? ) returning codigo";
            obj.setCodigo((Integer) getConexao().getJdbcTemplate().query(new PreparedStatementCreator() {

                public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
                    PreparedStatement sqlInserir = arg0.prepareStatement(sql);
                    sqlInserir.setString(1, obj.getNome());
                    sqlInserir.setBoolean(2, obj.isNacao());
                    sqlInserir.setString(3, obj.getSiglaInep());
                    sqlInserir.setString(4, obj.getNacionalidade());
                    return sqlInserir;
                }
            }, new ResultSetExtractor() {

                public Object extractData(ResultSet arg0) throws SQLException, DataAccessException {
                    if (arg0.next()) {
                        obj.setNovoObj(Boolean.FALSE);
                        return arg0.getInt("codigo");
                    }
                    return null;
                }
            }));
        } catch (Exception e) {
            obj.setNovoObj(true);
            throw e;
        }
    }

   
    @Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public void alterar(final PaizVO obj) throws Exception {
        try {
            Paiz.alterar(getIdEntidade());
            validarDados(obj);
            final String sql = "UPDATE Paiz set nome=?, nacao=?, siglainep=?, nacionalidade=? WHERE ((codigo = ?))";
            getConexao().getJdbcTemplate().update(new PreparedStatementCreator() {

                public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
                    PreparedStatement sqlAlterar = arg0.prepareStatement(sql);
                    sqlAlterar.setString(1, obj.getNome());
                    sqlAlterar.setBoolean(2, obj.isNacao());
                    sqlAlterar.setString(3, obj.getSiglaInep());
                    sqlAlterar.setString(4, obj.getNacionalidade());
                    sqlAlterar.setInt(5, obj.getCodigo().intValue());
                    return sqlAlterar;
                }
            });
            
        } catch (Exception e) {
            throw e;
        }
    }

  
    @Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
    public void excluir(PaizVO obj) throws Exception {
        try {
            Paiz.excluir(getIdEntidade());
            String sql = "DELETE FROM Paiz WHERE ((codigo = ?))";
            getConexao().getJdbcTemplate().update(sql, new Object[]{obj.getCodigo()});
        } catch (Exception e) {
            throw e;
        }
    }

    
    public List<PaizVO> consultarPorNome(String valorConsulta, boolean controlarAcesso,UsuarioVO usuario) throws Exception {
        ControleAcesso.consultar(getIdEntidade(), controlarAcesso, usuario);
        String sqlStr = "SELECT * FROM Paiz WHERE lower (nome) like('" + valorConsulta.toLowerCase() + "%') ORDER BY nome";
        SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sqlStr);
        return (montarDadosConsulta(tabelaResultado));
    }

   
    public List consultarPorCodigo(Integer valorConsulta, boolean controlarAcesso,UsuarioVO usuario) throws Exception {
        ControleAcesso.consultar(getIdEntidade(), controlarAcesso, usuario);
        String sqlStr = "SELECT * FROM Paiz WHERE codigo >= " + valorConsulta.intValue() + " ORDER BY codigo";
        SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sqlStr);
        return (montarDadosConsulta(tabelaResultado));
    }

   
    public static List montarDadosConsulta(SqlRowSet tabelaResultado) throws Exception {
        List vetResultado = new ArrayList(0);
        while (tabelaResultado.next()) {
            vetResultado.add(montarDados(tabelaResultado));
        }
        return vetResultado;
    }

    
    public static PaizVO montarDados(SqlRowSet dadosSQL) throws Exception {
        PaizVO obj = new PaizVO();
        obj.setCodigo(new Integer(dadosSQL.getInt("codigo")));
        obj.setNome(dadosSQL.getString("nome"));
        obj.setNacao(dadosSQL.getBoolean("nacao"));
        obj.setSiglaInep(dadosSQL.getString("siglainep"));
        obj.setNacionalidade(dadosSQL.getString("nacionalidade"));
        obj.setNovoObj(Boolean.FALSE);
        return obj;
    }

    
    public PaizVO consultarPorChavePrimaria(Integer codigoPrm, boolean controlarAcesso,UsuarioVO usuario) throws Exception {
        ControleAcesso.consultar(getIdEntidade(), false,usuario);
        String sql = "SELECT * FROM Paiz WHERE codigo = ?";
        SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sql, new Object[]{codigoPrm});
        if (!tabelaResultado.next()) {
            throw new Exception("Dados N�o Encontrados.");
        }
        return (montarDados(tabelaResultado));
    }

    
    public static String getIdEntidade() {
        return Paiz.idEntidade;
    }

    
    public void setIdEntidade(String idEntidade) {
        Paiz.idEntidade = idEntidade;
    }
    
}
