/**
 * 
 */
package negocio.facade.jdbc.administrativo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import negocio.comuns.administrativo.PerfilUsuarioVO;
import negocio.comuns.administrativo.PermissaoUsuarioMenuSubModuloVO;
import negocio.comuns.administrativo.PermissaoUsuarioMenuVO;
import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.facade.jdbc.arquitetura.ControleAcesso;
import negocio.interfaces.administrativo.PermissaoUsuarioMenuInterfaceFacade;
import negocio.interfaces.administrativo.PermissaoUsuarioMenuSubModuloInterfaceFacade;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Carlos Eug�nio
 *
 */
@Repository
@Scope("singleton")
@Lazy
public class PermissaoUsuarioMenuSubModulo extends ControleAcesso implements PermissaoUsuarioMenuSubModuloInterfaceFacade {

	private static final long serialVersionUID = 1L;

	public PermissaoUsuarioMenuSubModulo() {
		super();
	}
	
	public void validarDados(PermissaoUsuarioMenuSubModuloVO obj, UsuarioVO usuarioVO) throws Exception {
		
	}

	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void persistir(PermissaoUsuarioMenuSubModuloVO obj, UsuarioVO usuarioVO) throws Exception {
		if (obj.isNovoObj()) {
			incluir(obj, usuarioVO);
		} else {
			alterar(obj, usuarioVO);
		}
	}

	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void incluir(final PermissaoUsuarioMenuSubModuloVO obj, UsuarioVO usuario) throws Exception {
		try {
			validarDados(obj, usuario);
			final String sql = "INSERT INTO PermissaoUsuarioMenuSubModulo( permissaoUsuarioMenu, nomeSubModulo, permissaoSubModulo, descricaoSubModulo, nomeMenu ) "
					+ " VALUES ( ?, ?, ?, ?, ?) returning codigo";
			obj.setCodigo((Integer) getConexao().getJdbcTemplate().query(new PreparedStatementCreator() {

				public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
					PreparedStatement sqlInserir = arg0.prepareStatement(sql);
					if (obj.getPermissaoUsuarioMenuVO().getCodigo().intValue() != 0) {
						sqlInserir.setInt(1, obj.getPermissaoUsuarioMenuVO().getCodigo().intValue());
					} else {
						sqlInserir.setNull(1, 0);
					}
					sqlInserir.setString(2, obj.getNomeSubModulo());
					sqlInserir.setBoolean(3, obj.getPermissaoSubModulo());
					sqlInserir.setString(4, obj.getDescricaoSubModulo());
					sqlInserir.setString(5, obj.getNomeMenu());
					return sqlInserir;
				}
			}, new ResultSetExtractor() {

				public Object extractData(ResultSet arg0) throws SQLException, DataAccessException {
					if (arg0.next()) {
						obj.setNovoObj(Boolean.FALSE);
						return arg0.getInt("codigo");
					}
					return null;
				}
			}));
			
			obj.setNovoObj(Boolean.FALSE);
		} catch (Exception e) {
			obj.setNovoObj(Boolean.TRUE);
			throw e;
		}
	}

	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void alterar(final PermissaoUsuarioMenuSubModuloVO obj, UsuarioVO usuario) throws Exception {
		try {
			validarDados(obj, usuario);
			final String sql = "UPDATE PermissaoUsuarioMenuSubModulo set permissaoUsuarioMenu=?, nomeSubModulo=?, permissaoSubModulo=?, descricaoSubModulo=?, nomeMenu=? "
					+ "WHERE ((codigo = ?))";

			getConexao().getJdbcTemplate().update(new PreparedStatementCreator() {

				public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
					PreparedStatement sqlAlterar = arg0.prepareStatement(sql);
					if (obj.getPermissaoUsuarioMenuVO().getCodigo().intValue() != 0) {
						sqlAlterar.setInt(1, obj.getPermissaoUsuarioMenuVO().getCodigo().intValue());
					} else {
						sqlAlterar.setNull(1, 0);
					}
					sqlAlterar.setString(2, obj.getNomeSubModulo());
					sqlAlterar.setBoolean(3, obj.getPermissaoSubModulo());
					sqlAlterar.setString(4, obj.getDescricaoSubModulo());
					sqlAlterar.setString(5, obj.getNomeMenu());
					sqlAlterar.setInt(6, obj.getCodigo().intValue());
					return sqlAlterar;
				}
			});
			
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void excluir(PermissaoUsuarioMenuSubModuloVO obj, UsuarioVO usuario) throws Exception {
		try {
			String sql = "DELETE FROM PermissaoUsuarioMenuSubModulo WHERE ((codigo = ?))";
			getConexao().getJdbcTemplate().update(sql, new Object[] { obj.getCodigo() });
		} catch (Exception e) {
			throw e;
		}
	}
	
	@Override
	public List<PermissaoUsuarioMenuSubModuloVO> consultarPorPermissaoUsuarioMenu(Integer permissaoUsuarioMenu, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		StringBuilder sb = new StringBuilder();
		sb.append(getDadosBasicos());
		sb.append(" where PermissaoUsuarioMenuSubModulo.PermissaoUsuarioMenu = ").append(permissaoUsuarioMenu);
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
		return montarDadosLista(tabelaResultado, nivelMontarDados, usuarioVO);
	}
	
	@Override
	public void carregarDados(PermissaoUsuarioMenuSubModuloVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		if (nivelMontarDados.equals(NivelMontarDadosEnum.BASICO)) {
			StringBuilder sb = getDadosBasicos();
			sb.append(" where PermissaoUsuarioMenuSubModulo.codigo = ").append(obj.getCodigo());
			SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
			if (tabelaResultado.next()) {
				montarDadosBasicos(tabelaResultado, obj, nivelMontarDados, usuarioVO);
			}
		}
		if (nivelMontarDados.equals(NivelMontarDadosEnum.COMPLETO)) {
			StringBuilder sb = getDadosCompletos();
			sb.append(" where PermissaoUsuarioMenuSubModulo.codigo = ").append(obj.getCodigo());
			SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
			if (tabelaResultado.next()) {
				montarDadosCompletos(tabelaResultado, obj, nivelMontarDados, usuarioVO);
			}
		}
	}
	
	public List<PermissaoUsuarioMenuSubModuloVO> montarDadosLista(SqlRowSet dadosSQL, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		List<PermissaoUsuarioMenuSubModuloVO> listaMembroVOs = new ArrayList<PermissaoUsuarioMenuSubModuloVO>(0);
		while (dadosSQL.next()) {
			PermissaoUsuarioMenuSubModuloVO obj = new PermissaoUsuarioMenuSubModuloVO();
			montarDadosBasicos(dadosSQL, obj, nivelMontarDados, usuarioVO);
			listaMembroVOs.add(obj);
		}
		return listaMembroVOs;
	}
	
	public StringBuilder getDadosBasicos() {
		StringBuilder sb = new StringBuilder();
		sb.append("select PermissaoUsuarioMenuSubModulo.* ");
		sb.append(" from PermissaoUsuarioMenuSubModulo ");
		return sb;
	}
	
	public StringBuilder getDadosCompletos() {
		StringBuilder sb = new StringBuilder();
		sb.append("select PermissaoUsuarioMenuSubModulo.* ");
		sb.append(" from PermissaoUsuarioMenuSubModulo ");
		return sb;
	}
	
	public void montarDadosBasicos(SqlRowSet dadosSQL, PermissaoUsuarioMenuSubModuloVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		obj.setCodigo(dadosSQL.getInt("codigo"));
		obj.getPermissaoUsuarioMenuVO().setCodigo(dadosSQL.getInt("permissaoUsuarioMenu"));
		obj.setNomeSubModulo(dadosSQL.getString("nomeSubModulo"));
		obj.setPermissaoSubModulo(dadosSQL.getBoolean("permissaoSubModulo"));
		obj.setDescricaoSubModulo(dadosSQL.getString("descricaoSubModulo"));
		obj.setNomeMenu(dadosSQL.getString("nomeMenu"));
		obj.setNovoObj(false);
	}
	
	public void montarDadosCompletos(SqlRowSet dadosSQL, PermissaoUsuarioMenuSubModuloVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		obj.setCodigo(dadosSQL.getInt("codigo"));
		obj.getPermissaoUsuarioMenuVO().setCodigo(dadosSQL.getInt("permissaoUsuarioMenu"));
		obj.setNomeSubModulo(dadosSQL.getString("nomeSubModulo"));
		obj.setPermissaoSubModulo(dadosSQL.getBoolean("permissaoSubModulo"));
		obj.setDescricaoSubModulo(dadosSQL.getString("descricaoSubModulo"));
		obj.setNomeMenu(dadosSQL.getString("nomeMenu"));
		obj.setNovoObj(false);
		if (nivelMontarDados.equals(NivelMontarDadosEnum.TELA_CONSULTA)) {
			return;
		}
		return;
	}
	
	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void incluirPermissaoUsuarioMenuPorPerfilUsuario(List<PermissaoUsuarioMenuSubModuloVO> listaPermissaoUsuarioMenuSubModuloVOs, PermissaoUsuarioMenuVO permissaoUsuarioMenuVO, UsuarioVO usuarioVO) throws Exception {
		for (PermissaoUsuarioMenuSubModuloVO permissaoUsuarioMenuSubModuloVO : listaPermissaoUsuarioMenuSubModuloVOs) {
			permissaoUsuarioMenuSubModuloVO.getPermissaoUsuarioMenuVO().setCodigo(permissaoUsuarioMenuVO.getCodigo());
			if (permissaoUsuarioMenuSubModuloVO.getCodigo().equals(0)) {
				incluir(permissaoUsuarioMenuSubModuloVO, usuarioVO);
			} else {
				alterar(permissaoUsuarioMenuSubModuloVO, usuarioVO);
			}
		}
	}
	
	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void alterarPermissaoUsuarioMenuPorPerfilUsuario(List<PermissaoUsuarioMenuSubModuloVO> listaPermissaoUsuarioMenuSubModuloVOs, PermissaoUsuarioMenuVO permissaoUsuarioMenuVO, UsuarioVO usuarioVO) throws Exception {
		excluirPermissaoUsuarioMenuSubModuloPorPermissaoUsuario(listaPermissaoUsuarioMenuSubModuloVOs, permissaoUsuarioMenuVO.getCodigo(), usuarioVO);
		for (PermissaoUsuarioMenuSubModuloVO permissaoUsuarioMenuSubModuloVO : listaPermissaoUsuarioMenuSubModuloVOs) {
			permissaoUsuarioMenuSubModuloVO.getPermissaoUsuarioMenuVO().setCodigo(permissaoUsuarioMenuVO.getCodigo());
			if (permissaoUsuarioMenuSubModuloVO.getCodigo().equals(0)) {
				incluir(permissaoUsuarioMenuSubModuloVO, usuarioVO);
			} else {
				alterar(permissaoUsuarioMenuSubModuloVO, usuarioVO);
			}
		}
	}
	
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void excluirPermissaoUsuarioMenuSubModuloPorPermissaoUsuario(List<PermissaoUsuarioMenuSubModuloVO> listaPermissaoUsuarioMenuSubModuloVOs, Integer perfilUsuario, UsuarioVO usuario) throws Exception {
		StringBuilder sb = new StringBuilder();  
		sb.append("DELETE FROM PermissaoUsuarioMenuSubModulo WHERE (permissaoUsuarioMenu = ?) ");
		boolean primeiraVez = true;
		for (PermissaoUsuarioMenuSubModuloVO permissaoUsuarioMenuSubModuloVO : listaPermissaoUsuarioMenuSubModuloVOs) {
			if (primeiraVez) {
				sb.append(" and codigo not in(").append(permissaoUsuarioMenuSubModuloVO.getCodigo());
				primeiraVez = false;
			} else {
				sb.append(", ").append(permissaoUsuarioMenuSubModuloVO.getCodigo());
			}
		}
		if (!listaPermissaoUsuarioMenuSubModuloVOs.isEmpty()) {
			sb.append(") ");
		}
		getConexao().getJdbcTemplate().update(sb.toString(), new Object[] { perfilUsuario });
	}
	
}
