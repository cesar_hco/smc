/**
 * 
 */
package negocio.facade.jdbc.administrativo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import negocio.comuns.administrativo.UsuarioEmpresaPerfilUsuarioVO;
import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.facade.jdbc.arquitetura.ControleAcesso;
import negocio.interfaces.administrativo.UsuarioEmpresaPerfilUsuarioInterfaceFacade;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Carlos Eug�nio
 *
 */
@Repository
@Scope("singleton")
@Lazy
public class UsuarioEmpresaPerfilUsuario extends ControleAcesso implements UsuarioEmpresaPerfilUsuarioInterfaceFacade {

	private static final long serialVersionUID = 1L;

	public UsuarioEmpresaPerfilUsuario() {
		super();
	}

	public void validarDados(UsuarioEmpresaPerfilUsuarioVO obj, UsuarioVO usuarioVO) throws Exception {
//		if (obj.getPessoaVO().getNome().equals("")) {
//			throw new Exception("O campo NOME deve ser informado!");
//		}
	}

	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void persistir(UsuarioEmpresaPerfilUsuarioVO obj, UsuarioVO usuarioVO) throws Exception {
		if (obj.isNovoObj()) {
			incluir(obj, usuarioVO);
		} else {
			alterar(obj, usuarioVO);
		}
	}

	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void incluir(final UsuarioEmpresaPerfilUsuarioVO obj, UsuarioVO usuario) throws Exception {
		try {
			validarDados(obj, usuario);
			final String sql = "INSERT INTO UsuarioEmpresaPerfilUsuario( usuario, empresa, perfilUsuario) "
					+ " VALUES ( ?, ?, ?) returning codigo";
			obj.setCodigo((Integer) getConexao().getJdbcTemplate().query(new PreparedStatementCreator() {

				public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
					PreparedStatement sqlInserir = arg0.prepareStatement(sql);
					if (obj.getUsuarioVO().getCodigo().intValue() != 0) {
						sqlInserir.setInt(1, obj.getUsuarioVO().getCodigo().intValue());
					} else {
						sqlInserir.setNull(1, 0);
					}
					if (obj.getEmpresaVO().getCodigo().intValue() != 0) {
						sqlInserir.setInt(2, obj.getEmpresaVO().getCodigo().intValue());
					} else {
						sqlInserir.setNull(2, 0);
					}
					if (obj.getPerfilUsuarioVO().getCodigo().intValue() != 0) {
						sqlInserir.setInt(3, obj.getPerfilUsuarioVO().getCodigo().intValue());
					} else {
						sqlInserir.setNull(3, 0);
					}
					return sqlInserir;
				}
			}, new ResultSetExtractor() {

				public Object extractData(ResultSet arg0) throws SQLException, DataAccessException {
					if (arg0.next()) {
						obj.setNovoObj(Boolean.FALSE);
						return arg0.getInt("codigo");
					}
					return null;
				}
			}));
			obj.setNovoObj(Boolean.FALSE);
		} catch (Exception e) {
			obj.setNovoObj(Boolean.TRUE);
			throw e;
		}
	}

	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void alterar(final UsuarioEmpresaPerfilUsuarioVO obj, UsuarioVO usuario) throws Exception {
		try {
			validarDados(obj, usuario);
			final String sql = "UPDATE UsuarioEmpresaPerfilUsuario set usuario=?, empresa=?, perfilUsuario=? WHERE ((codigo = ?))";

			getConexao().getJdbcTemplate().update(new PreparedStatementCreator() {

				public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
					PreparedStatement sqlAlterar = arg0.prepareStatement(sql);
					if (obj.getUsuarioVO().getCodigo().intValue() != 0) {
						sqlAlterar.setInt(1, obj.getUsuarioVO().getCodigo().intValue());
					} else {
						sqlAlterar.setNull(1, 0);
					}
					if (obj.getEmpresaVO().getCodigo().intValue() != 0) {
						sqlAlterar.setInt(2, obj.getEmpresaVO().getCodigo().intValue());
					} else {
						sqlAlterar.setNull(2, 0);
					}
					if (obj.getPerfilUsuarioVO().getCodigo().intValue() != 0) {
						sqlAlterar.setInt(3, obj.getPerfilUsuarioVO().getCodigo().intValue());
					} else {
						sqlAlterar.setNull(3, 0);
					}
					sqlAlterar.setInt(4, obj.getCodigo().intValue());
					return sqlAlterar;
				}
			});
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void excluir(UsuarioEmpresaPerfilUsuarioVO obj, UsuarioVO usuario) throws Exception {
		try {
			String sql = "DELETE FROM UsuarioEmpresaPerfilUsuario WHERE ((codigo = ?))";
			getConexao().getJdbcTemplate().update(sql, new Object[] { obj.getCodigo() });
		} catch (Exception e) {
			throw e;
		}
	}
	
	@Override
	public List<UsuarioEmpresaPerfilUsuarioVO> consultarPorUsuario(Integer usuario, NivelMontarDadosEnum nivelMontarDadosEnum, UsuarioVO usuarioVO) {
		StringBuilder sb = new StringBuilder();
		sb.append(getDadosBasicos());
		sb.append(" where usuarioEmpresaPerfilUsuario.usuario = ").append(usuario);
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
		return montarDadosLista(tabelaResultado, NivelMontarDadosEnum.BASICO, usuarioVO);
		
	}
	
	@Override
	public void carregarDados(UsuarioEmpresaPerfilUsuarioVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		if (nivelMontarDados.equals(NivelMontarDadosEnum.BASICO)) {
			StringBuilder sb = getDadosBasicos();
			sb.append(" where pastor.codigo = ").append(obj.getCodigo());
			SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
			if (tabelaResultado.next()) {
				montarDadosBasicos(tabelaResultado, obj, nivelMontarDados, usuarioVO);
			}
		}
		if (nivelMontarDados.equals(NivelMontarDadosEnum.COMPLETO)) {
			StringBuilder sb = getDadosCompletos();
			sb.append(" where pastor.codigo = ").append(obj.getCodigo());
			SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
			if (tabelaResultado.next()) {
				montarDadosCompletos(tabelaResultado, obj, nivelMontarDados, usuarioVO);
			}
		}
	}
	
	public List<UsuarioEmpresaPerfilUsuarioVO> montarDadosLista(SqlRowSet dadosSQL, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		List<UsuarioEmpresaPerfilUsuarioVO> listaMembroVOs = new ArrayList<UsuarioEmpresaPerfilUsuarioVO>(0);
		while (dadosSQL.next()) {
			UsuarioEmpresaPerfilUsuarioVO obj = new UsuarioEmpresaPerfilUsuarioVO();
			montarDadosBasicos(dadosSQL, obj, nivelMontarDados, usuarioVO);
			listaMembroVOs.add(obj);
		}
		return listaMembroVOs;
	}
	
	public StringBuilder getDadosBasicos() {
		StringBuilder sb = new StringBuilder();
		sb.append("select usuarioEmpresaPerfilUsuario.codigo,   ");
		sb.append(" usuario.codigo AS \"usuario.codigo\", ");
		sb.append(" empresa.codigo AS \"empresa.codigo\", empresa.nome AS \"empresa.nome\", ");
		sb.append(" perfilUsuario.codigo AS \"perfilUsuario.codigo\", perfilUsuario.nome AS \"perfilUsuario.nome\" ");
		sb.append(" from usuarioEmpresaPerfilUsuario ");
		sb.append(" inner join usuario on usuario.codigo = usuarioEmpresaPerfilUsuario.usuario ");
		sb.append(" inner join empresa on empresa.codigo = usuarioEmpresaPerfilUsuario.empresa ");
		sb.append(" inner join perfilUsuario on perfilUsuario.codigo = usuarioEmpresaPerfilUsuario.perfilUsuario ");
		return sb;
	}
	
	public StringBuilder getDadosCompletos() {
		StringBuilder sb = new StringBuilder();
		sb.append("select usuarioEmpresaPerfilUsuario.codigo,   ");
		sb.append(" usuario.codigo AS \"usuario.codigo\", ");
		sb.append(" empresa.codigo AS \"empresa.codigo\", empresa.nome AS \"empresa.nome\", ");
		sb.append(" perfilUsuario.codigo AS \"perfilUsuario.codigo\", perfilUsuario.nome AS \"perfilUsuario.nome\" ");
		sb.append(" from usuarioEmpresaPerfilUsuario ");
		sb.append(" inner join usuario on usuario.codigo = usuarioEmpresaPerfilUsuario.usuario ");
		sb.append(" inner join empresa on empresa.codigo = usuarioEmpresaPerfilUsuario.empresa ");
		sb.append(" inner join perfilUsuario on perfilUsuario.codigo = usuarioEmpresaPerfilUsuario.perfilUsuario ");
		return sb;
	}
	
	public void montarDadosBasicos(SqlRowSet dadosSQL, UsuarioEmpresaPerfilUsuarioVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		obj.setCodigo(dadosSQL.getInt("codigo"));
		obj.getUsuarioVO().setCodigo(dadosSQL.getInt("usuario.codigo"));
		obj.getEmpresaVO().setCodigo(dadosSQL.getInt("empresa.codigo"));
		obj.getEmpresaVO().setDescricao(dadosSQL.getString("empresa.descricao"));
		
		obj.getPerfilUsuarioVO().setCodigo(dadosSQL.getInt("perfilUsuario.codigo"));
		obj.getPerfilUsuarioVO().setNome(dadosSQL.getString("perfilUsuario.nome"));
		obj.setNovoObj(false);
	}
	
	public void montarDadosCompletos(SqlRowSet dadosSQL, UsuarioEmpresaPerfilUsuarioVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		obj.setCodigo(dadosSQL.getInt("codigo"));
		return;
	}
	
	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void incluirUsuarioEmpresaPerfilUsuarioVOs(List<UsuarioEmpresaPerfilUsuarioVO> listaUsuarioEmpresaPerfilUsuarioVOs, UsuarioVO usuarioVO, UsuarioVO usuarioLogado) throws Exception {
		for (UsuarioEmpresaPerfilUsuarioVO usuarioEmpresaPerfilUsuarioVO : listaUsuarioEmpresaPerfilUsuarioVOs) {
			usuarioEmpresaPerfilUsuarioVO.getUsuarioVO().setCodigo(usuarioVO.getCodigo());
			if (usuarioEmpresaPerfilUsuarioVO.getCodigo().equals(0)) {
				incluir(usuarioEmpresaPerfilUsuarioVO, usuarioVO);
			} else {
				alterar(usuarioEmpresaPerfilUsuarioVO, usuarioVO);
			}
		}
	}
	
	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void alterarUsuarioEmpresaPerfilUsuarioVOs(List<UsuarioEmpresaPerfilUsuarioVO> listaUsuarioEmpresaPerfilUsuarioVOs, UsuarioVO usuarioVO, UsuarioVO usuarioLogado) throws Exception {
		excluirPermissaoUsuarioMenuPorPerfilUsuario(listaUsuarioEmpresaPerfilUsuarioVOs, usuarioVO.getCodigo(), usuarioVO);
		for (UsuarioEmpresaPerfilUsuarioVO usuarioEmpresaPerfilUsuarioVO : listaUsuarioEmpresaPerfilUsuarioVOs) {
			usuarioEmpresaPerfilUsuarioVO.getUsuarioVO().setCodigo(usuarioVO.getCodigo());
			if (usuarioEmpresaPerfilUsuarioVO.getCodigo().equals(0)) {
				incluir(usuarioEmpresaPerfilUsuarioVO, usuarioVO);
			} else {
				alterar(usuarioEmpresaPerfilUsuarioVO, usuarioVO);
			}
		}
	}
	
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void excluirPermissaoUsuarioMenuPorPerfilUsuario(List<UsuarioEmpresaPerfilUsuarioVO> listaUsuarioEmpresaPerfilUsuarioVOs, Integer usuario, UsuarioVO usuarioLogado) throws Exception {
		StringBuilder sb = new StringBuilder();  
		sb.append("DELETE FROM UsuarioEmpresaPerfilUsuario WHERE (usuario = ?) ");
		boolean primeiraVez = true;
		for (UsuarioEmpresaPerfilUsuarioVO usuarioEmpresaPerfilUsuarioVO : listaUsuarioEmpresaPerfilUsuarioVOs) {
			if (primeiraVez) {
				sb.append(" and codigo not in(").append(usuarioEmpresaPerfilUsuarioVO.getCodigo());
				primeiraVez = false;
			} else {
				sb.append(", ").append(usuarioEmpresaPerfilUsuarioVO.getCodigo());
			}
		}
		if (!listaUsuarioEmpresaPerfilUsuarioVOs.isEmpty()) {
			sb.append(") ");
		}
		getConexao().getJdbcTemplate().update(sb.toString(), new Object[] { usuario });
	}
	
}
