/**
 * 
 */
package negocio.facade.jdbc.administrativo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import negocio.comuns.administrativo.PerfilUsuarioVO;
import negocio.comuns.administrativo.PermissaoUsuarioMenuVO;
import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.facade.jdbc.arquitetura.ControleAcesso;
import negocio.interfaces.administrativo.PermissaoUsuarioMenuInterfaceFacade;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Carlos Eug�nio
 *
 */
@Repository
@Scope("singleton")
@Lazy
public class PermissaoUsuarioMenu extends ControleAcesso implements PermissaoUsuarioMenuInterfaceFacade {

	private static final long serialVersionUID = 1L;

	public PermissaoUsuarioMenu() {
		super();
	}

	public void validarDados(PermissaoUsuarioMenuVO obj, UsuarioVO usuarioVO) throws Exception {

	}

	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void persistir(PermissaoUsuarioMenuVO obj, UsuarioVO usuarioVO) throws Exception {
		if (obj.isNovoObj()) {
			incluir(obj, usuarioVO);
		} else {
			alterar(obj, usuarioVO);
		}
	}

	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void incluir(final PermissaoUsuarioMenuVO obj, UsuarioVO usuario) throws Exception {
		try {
			validarDados(obj, usuario);
			final String sql = "INSERT INTO PermissaoUsuarioMenu( perfilUsuario, nomeModulo, permissaoModulo, descricaoModulo, nomeMenu ) VALUES ( ?, ?, ?, ?, ?) returning codigo";
			obj.setCodigo((Integer) getConexao().getJdbcTemplate().query(new PreparedStatementCreator() {

				public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
					PreparedStatement sqlInserir = arg0.prepareStatement(sql);
					if (obj.getPerfilUsuarioVO().getCodigo().intValue() != 0) {
						sqlInserir.setInt(1, obj.getPerfilUsuarioVO().getCodigo().intValue());
					} else {
						sqlInserir.setNull(1, 0);
					}
					sqlInserir.setString(2, obj.getNomeModulo());
					sqlInserir.setBoolean(3, obj.getPermissaoModulo());
					sqlInserir.setString(4, obj.getDescricaoModulo());
					sqlInserir.setString(5, obj.getNomeMenu());
					return sqlInserir;
				}
			}, new ResultSetExtractor() {

				public Object extractData(ResultSet arg0) throws SQLException, DataAccessException {
					if (arg0.next()) {
						obj.setNovoObj(Boolean.FALSE);
						return arg0.getInt("codigo");
					}
					return null;
				}
			}));
			getFacadeFactory().getPermissaoUsuarioMenuSubModuloFacade().incluirPermissaoUsuarioMenuPorPerfilUsuario(obj.getPermissaoUsuarioMenuSubModuloVOs(), obj, usuario);
			obj.setNovoObj(Boolean.FALSE);
		} catch (Exception e) {
			obj.setNovoObj(Boolean.TRUE);
			throw e;
		}
	}

	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void alterar(final PermissaoUsuarioMenuVO obj, UsuarioVO usuario) throws Exception {
		try {
			validarDados(obj, usuario);
			final String sql = "UPDATE PermissaoUsuarioMenu set perfilUsuario=?, nomeModulo=?, permissaoModulo=?, descricaoModulo=?, nomeMenu=? WHERE ((codigo = ?))";

			getConexao().getJdbcTemplate().update(new PreparedStatementCreator() {

				public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
					PreparedStatement sqlAlterar = arg0.prepareStatement(sql);
					if (obj.getPerfilUsuarioVO().getCodigo().intValue() != 0) {
						sqlAlterar.setInt(1, obj.getPerfilUsuarioVO().getCodigo().intValue());
					} else {
						sqlAlterar.setNull(1, 0);
					}
					sqlAlterar.setString(2, obj.getNomeModulo());
					sqlAlterar.setBoolean(3, obj.getPermissaoModulo());
					sqlAlterar.setString(4, obj.getDescricaoModulo());
					sqlAlterar.setString(5, obj.getNomeMenu());
					sqlAlterar.setInt(6, obj.getCodigo().intValue());
					return sqlAlterar;
				}
			});
			getFacadeFactory().getPermissaoUsuarioMenuSubModuloFacade().alterarPermissaoUsuarioMenuPorPerfilUsuario(obj.getPermissaoUsuarioMenuSubModuloVOs(), obj, usuario);
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void excluir(PermissaoUsuarioMenuVO obj, UsuarioVO usuario) throws Exception {
		try {
			String sql = "DELETE FROM PermissaoUsuarioMenu WHERE ((codigo = ?))";
			getConexao().getJdbcTemplate().update(sql, new Object[] { obj.getCodigo() });
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public List<PermissaoUsuarioMenuVO> consultarPorPerfilUsuario(Integer perfilUsuario, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		StringBuilder sb = new StringBuilder();
		if (nivelMontarDados.equals(NivelMontarDadosEnum.BASICO)) {
			sb.append(getDadosBasicos());
		} else {
			sb.append(getDadosCompletos());
		}
		sb.append(" where PermissaoUsuarioMenu.perfilUsuario = ").append(perfilUsuario);
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
		return montarDadosLista(tabelaResultado, nivelMontarDados, usuarioVO);
	}

	@Override
	public void carregarDados(PermissaoUsuarioMenuVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		if (nivelMontarDados.equals(NivelMontarDadosEnum.BASICO)) {
			StringBuilder sb = getDadosBasicos();
			sb.append(" where PermissaoUsuarioMenu.codigo = ").append(obj.getCodigo());
			SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
			if (tabelaResultado.next()) {
				montarDadosBasicos(tabelaResultado, obj, nivelMontarDados, usuarioVO);
			}
		}
		if (nivelMontarDados.equals(NivelMontarDadosEnum.COMPLETO)) {
			StringBuilder sb = getDadosCompletos();
			sb.append(" where PermissaoUsuarioMenu.codigo = ").append(obj.getCodigo());
			SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
			if (tabelaResultado.next()) {
				montarDadosCompletos(tabelaResultado, obj, nivelMontarDados, usuarioVO);
			}
		}
	}

	public List<PermissaoUsuarioMenuVO> montarDadosLista(SqlRowSet dadosSQL, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		List<PermissaoUsuarioMenuVO> listaMembroVOs = new ArrayList<PermissaoUsuarioMenuVO>(0);
		while (dadosSQL.next()) {
			PermissaoUsuarioMenuVO obj = new PermissaoUsuarioMenuVO();
			if (nivelMontarDados.equals(NivelMontarDadosEnum.BASICO)) {
				montarDadosBasicos(dadosSQL, obj, nivelMontarDados, usuarioVO);
			} else {
				montarDadosCompletos(dadosSQL, obj, nivelMontarDados, usuarioVO);
			}
			listaMembroVOs.add(obj);
		}
		return listaMembroVOs;
	}

	public StringBuilder getDadosBasicos() {
		StringBuilder sb = new StringBuilder();
		sb.append("select permissaoUsuarioMenu.* ");
		sb.append(" from permissaoUsuarioMenu ");
		return sb;
	}

	public StringBuilder getDadosCompletos() {
		StringBuilder sb = new StringBuilder();
		sb.append("select permissaoUsuarioMenu.* ");
		sb.append(" from permissaoUsuarioMenu ");
		return sb;
	}

	public void montarDadosBasicos(SqlRowSet dadosSQL, PermissaoUsuarioMenuVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		obj.setCodigo(dadosSQL.getInt("codigo"));
		obj.getPerfilUsuarioVO().setCodigo(dadosSQL.getInt("perfilUsuario"));
		obj.setNomeModulo(dadosSQL.getString("nomeModulo"));
		obj.setDescricaoModulo(dadosSQL.getString("descricaoModulo"));
		obj.setNomeMenu(dadosSQL.getString("nomeMenu"));
		obj.setPermissaoModulo(dadosSQL.getBoolean("permissaoModulo"));
		obj.setNovoObj(false);
	}

	public void montarDadosCompletos(SqlRowSet dadosSQL, PermissaoUsuarioMenuVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		obj.setCodigo(dadosSQL.getInt("codigo"));
		obj.getPerfilUsuarioVO().setCodigo(dadosSQL.getInt("perfilUsuario"));
		obj.setNomeModulo(dadosSQL.getString("nomeModulo"));
		obj.setPermissaoModulo(dadosSQL.getBoolean("permissaoModulo"));
		obj.setDescricaoModulo(dadosSQL.getString("descricaoModulo"));
		obj.setNomeMenu(dadosSQL.getString("nomeMenu"));
		obj.setPermissaoUsuarioMenuSubModuloVOs(getFacadeFactory().getPermissaoUsuarioMenuSubModuloFacade().consultarPorPermissaoUsuarioMenu(obj.getCodigo(), nivelMontarDados, usuarioVO));
		obj.setNovoObj(false);
		if (nivelMontarDados.equals(NivelMontarDadosEnum.TELA_CONSULTA)) {
			return;
		}
		return;
	}
	
	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void incluirPermissaoUsuarioMenuPorPerfilUsuario(List<PermissaoUsuarioMenuVO> listaPermissaoUsuarioMenuVOs, PerfilUsuarioVO perfilUsuarioVO, UsuarioVO usuarioVO) throws Exception {
		for (PermissaoUsuarioMenuVO permissaoUsuarioMenuVO : listaPermissaoUsuarioMenuVOs) {
			permissaoUsuarioMenuVO.getPerfilUsuarioVO().setCodigo(perfilUsuarioVO.getCodigo());
			if (permissaoUsuarioMenuVO.getCodigo().equals(0)) {
				incluir(permissaoUsuarioMenuVO, usuarioVO);
			} else {
				alterar(permissaoUsuarioMenuVO, usuarioVO);
			}
		}
	}
	
	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void alterarPermissaoUsuarioMenuPorPerfilUsuario(List<PermissaoUsuarioMenuVO> listaPermissaoUsuarioMenuVOs, PerfilUsuarioVO perfilUsuarioVO, UsuarioVO usuarioVO) throws Exception {
		excluirPermissaoUsuarioMenuPorPerfilUsuario(listaPermissaoUsuarioMenuVOs, perfilUsuarioVO.getCodigo(), usuarioVO);
		for (PermissaoUsuarioMenuVO permissaoUsuarioMenuVO : listaPermissaoUsuarioMenuVOs) {
			permissaoUsuarioMenuVO.getPerfilUsuarioVO().setCodigo(perfilUsuarioVO.getCodigo());
			if (permissaoUsuarioMenuVO.getCodigo().equals(0)) {
				incluir(permissaoUsuarioMenuVO, usuarioVO);
			} else {
				alterar(permissaoUsuarioMenuVO, usuarioVO);
			}
		}
	}
	
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void excluirPermissaoUsuarioMenuPorPerfilUsuario(List<PermissaoUsuarioMenuVO> listaPermissaoUsuarioMenuVOs, Integer perfilUsuario, UsuarioVO usuario) throws Exception {
		StringBuilder sb = new StringBuilder();  
		sb.append("DELETE FROM PermissaoUsuarioMenu WHERE (perfilUsuario = ?) ");
		boolean primeiraVez = true;
		for (PermissaoUsuarioMenuVO permissaoUsuarioMenuVO : listaPermissaoUsuarioMenuVOs) {
			if (primeiraVez) {
				sb.append(" and codigo not in(").append(permissaoUsuarioMenuVO.getCodigo());
				primeiraVez = false;
			} else {
				sb.append(", ").append(permissaoUsuarioMenuVO.getCodigo());
			}
		}
		if (!listaPermissaoUsuarioMenuVOs.isEmpty()) {
			sb.append(") ");
		}
		getConexao().getJdbcTemplate().update(sb.toString(), new Object[] { perfilUsuario });
	}

}
