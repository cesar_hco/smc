package negocio.facade.jdbc.administrativo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import negocio.comuns.administrativo.ConvenioVO;
import negocio.comuns.administrativo.PacienteVO;
import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.facade.jdbc.arquitetura.ControleAcesso;
import negocio.interfaces.administrativo.ConveniointerfaceFacade;
import negocio.interfaces.administrativo.PacienteInterfaceFacade;

@Repository
@Scope("singleton")
@Lazy
public class Convenio extends ControleAcesso implements ConveniointerfaceFacade{

	@Override
	public List<ConvenioVO> consultar(String campoConsulta, String valorConsulta, NivelMontarDadosEnum nivelMontarDados,UsuarioVO usuarioVO) {
		StringBuilder sb = new StringBuilder();
		sb.append(getDadosBasicos());
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
		return montarDadosLista(tabelaResultado, nivelMontarDados, usuarioVO);
		
	}

	@Override
	public void carregarDados(ConvenioVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		// TODO Auto-generated method stub
		
	}
	
	public StringBuilder getDadosBasicos() {
		StringBuilder sb = new StringBuilder();
		sb.append("select  * from Convenio");
		return sb;
	}
	
	public void montarDados(SqlRowSet dadosSQL, ConvenioVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		
		obj.setCodigo(dadosSQL.getInt("codigo"));
		obj.setDescricao(dadosSQL.getString("descricao"));
//		obj.getEmpresa().setCodigo(dadosSQL.getInt("empresa"));
		obj.setAtivo(dadosSQL.getBoolean("ativo"));
	}
	
	public List<ConvenioVO> montarDadosLista(SqlRowSet dadosSQL, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		List<ConvenioVO> ConvenioVOs = new ArrayList<ConvenioVO>(0);
		while (dadosSQL.next()) {
						
			ConvenioVO obj = new ConvenioVO();
			
			montarDados(dadosSQL, obj, nivelMontarDados, usuarioVO);
			ConvenioVOs.add(obj);
		}
		return ConvenioVOs;
	}
	

}
