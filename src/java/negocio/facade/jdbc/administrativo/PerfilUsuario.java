/**
 * 
 */
package negocio.facade.jdbc.administrativo;

import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import negocio.comuns.administrativo.PerfilUsuarioVO;
import negocio.comuns.administrativo.PermissaoAcessoMenuVO;
import negocio.comuns.administrativo.PermissaoUsuarioMenuSubModuloVO;
import negocio.comuns.administrativo.PermissaoUsuarioMenuVO;
import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.administrativo.enumeradores.PermissaoPerfilUsuarioEnum;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.facade.jdbc.arquitetura.ControleAcesso;
import negocio.interfaces.administrativo.PerfilUsuarioInterfaceFacade;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Carlos Eug�nio
 *
 */
@Repository
@Scope("singleton")
@Lazy
public class PerfilUsuario extends ControleAcesso implements PerfilUsuarioInterfaceFacade {

	private static final long serialVersionUID = 1L;

	public PerfilUsuario() {
		super(); 
	}
	
	public void validarDados(PerfilUsuarioVO obj, UsuarioVO usuarioVO) throws Exception {
		if (obj.getNome().equals("")) {
			throw new Exception("O campo NOME deve ser informado!");
		}
	}

	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void persistir(PerfilUsuarioVO obj, UsuarioVO usuarioVO) throws Exception {
		if (obj.isNovoObj()) {
			incluir(obj, usuarioVO);
		} else {
			alterar(obj, usuarioVO);
		}
	}

	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void incluir(final PerfilUsuarioVO obj, UsuarioVO usuario) throws Exception {
		try {
			validarDados(obj, usuario);
			final String sql = "INSERT INTO PerfilUsuario( nome) VALUES ( ? ) returning codigo";
			obj.setCodigo((Integer) getConexao().getJdbcTemplate().query(new PreparedStatementCreator() {

				public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
					PreparedStatement sqlInserir = arg0.prepareStatement(sql);
					sqlInserir.setString(1, obj.getNome());

					return sqlInserir;
				}
			}, new ResultSetExtractor() {

				public Object extractData(ResultSet arg0) throws SQLException, DataAccessException {
					if (arg0.next()) {
						obj.setNovoObj(Boolean.FALSE);
						return arg0.getInt("codigo");
					}
					return null;
				}
			}));
			getFacadeFactory().getPermissaoUsuarioMenuFacade().incluirPermissaoUsuarioMenuPorPerfilUsuario(obj.getPermissaoUsuarioMenuVOs(), obj, usuario);
			obj.setNovoObj(Boolean.FALSE);
		} catch (Exception e) {
			obj.setNovoObj(Boolean.TRUE);
			throw e;
		}
	}

	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void alterar(final PerfilUsuarioVO obj, UsuarioVO usuario) throws Exception {
		try {
			validarDados(obj, usuario);
			final String sql = "UPDATE PerfilUsuario set nome=? WHERE ((codigo = ?))";

			getConexao().getJdbcTemplate().update(new PreparedStatementCreator() {

				public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
					PreparedStatement sqlAlterar = arg0.prepareStatement(sql);
					sqlAlterar.setString(1, obj.getNome());
					sqlAlterar.setInt(2, obj.getCodigo().intValue());
					return sqlAlterar;
				}
			});
			getFacadeFactory().getPermissaoUsuarioMenuFacade().alterarPermissaoUsuarioMenuPorPerfilUsuario(obj.getPermissaoUsuarioMenuVOs(), obj, usuario);
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void excluir(PerfilUsuarioVO obj, UsuarioVO usuario) throws Exception {
		try {
			String sql = "DELETE FROM PerfilUsuario WHERE ((codigo = ?))";
			getConexao().getJdbcTemplate().update(sql, new Object[] { obj.getCodigo() });
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public List<PerfilUsuarioVO> consultar(String campoConsulta, String valorConsulta, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		List<PerfilUsuarioVO> listaConsulta = new ArrayList<PerfilUsuarioVO>(0);
		if (campoConsulta.equals("NOME")) {
			listaConsulta = consultarPorNome(valorConsulta, nivelMontarDados, usuarioVO);
		}
		return listaConsulta;
	}

	public List<PerfilUsuarioVO> consultarPorNome(String nome, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		StringBuilder sb = new StringBuilder();
		sb.append(getDadosBasicos());
		sb.append(" where PerfilUsuario.nome ilike ('").append(nome).append("%') ");
		sb.append(" order by nome ");
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
		return montarDadosLista(tabelaResultado, nivelMontarDados, usuarioVO);
	}
	
	@Override
	public void carregarDados(PerfilUsuarioVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		if (nivelMontarDados.equals(NivelMontarDadosEnum.BASICO)) {
			StringBuilder sb = getDadosBasicos();
			sb.append(" where PerfilUsuario.codigo = ").append(obj.getCodigo());
			SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
			if (tabelaResultado.next()) {
				montarDadosBasicos(tabelaResultado, obj, nivelMontarDados, usuarioVO);
			}
		}
		if (nivelMontarDados.equals(NivelMontarDadosEnum.COMPLETO)) {
			StringBuilder sb = getDadosCompletos();
			sb.append(" where PerfilUsuario.codigo = ").append(obj.getCodigo());
			SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
			if (tabelaResultado.next()) {
				montarDadosCompletos(tabelaResultado, obj, nivelMontarDados, usuarioVO);
			}
		}
	}
	
	public List<PerfilUsuarioVO> montarDadosLista(SqlRowSet dadosSQL, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		List<PerfilUsuarioVO> listaMembroVOs = new ArrayList<PerfilUsuarioVO>(0);
		while (dadosSQL.next()) {
			PerfilUsuarioVO obj = new PerfilUsuarioVO();
			montarDadosBasicos(dadosSQL, obj, nivelMontarDados, usuarioVO);
			listaMembroVOs.add(obj);
		}
		return listaMembroVOs;
	}
	
	public StringBuilder getDadosBasicos() {
		StringBuilder sb = new StringBuilder();
		sb.append("select codigo, nome ");
		sb.append(" from perfilUsuario ");
		return sb;
	}
	
	public StringBuilder getDadosCompletos() {
		StringBuilder sb = new StringBuilder();
		sb.append("select codigo, nome ");
		sb.append(" from perfilUsuario ");
		return sb;
	}
	
	public void montarDadosBasicos(SqlRowSet dadosSQL, PerfilUsuarioVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		obj.setCodigo(dadosSQL.getInt("codigo"));
		obj.setNome(dadosSQL.getString("nome"));
		obj.setNovoObj(false);
	}
	
	public void montarDadosCompletos(SqlRowSet dadosSQL, PerfilUsuarioVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		obj.setCodigo(dadosSQL.getInt("codigo"));
		obj.setNome(dadosSQL.getString("nome"));
		obj.setPermissaoUsuarioMenuVOs(getFacadeFactory().getPermissaoUsuarioMenuFacade().consultarPorPerfilUsuario(obj.getCodigo(), NivelMontarDadosEnum.COMPLETO, usuarioVO));
		obj.setNovoObj(false);
		if (nivelMontarDados.equals(NivelMontarDadosEnum.TELA_CONSULTA)) {
			return;
		}
		return;
	}
	
	@Override
	public PermissaoAcessoMenuVO montarPermissoesMenu(PerfilUsuarioVO perfilUsuarioVO) {
		try {
			// M�todo que monta a �rvore de permiss�es.
			carregarDados(perfilUsuarioVO, NivelMontarDadosEnum.COMPLETO, null);
		} catch (Exception ex) {
			Logger.getLogger(PerfilUsuario.class.getName()).log(Level.SEVERE, null, ex);
		}
		PermissaoAcessoMenuVO permissaoAcessoMenuVO = new PermissaoAcessoMenuVO();
		for (PermissaoUsuarioMenuVO permissaoVO : perfilUsuarioVO.getPermissaoUsuarioMenuVOs()) {
			try {
				Class classeGenerica = Class.forName(permissaoVO.getClass().getName());
				Method metodoGet = classeGenerica.getMethod("getNomeMenu");
				metodoGet.invoke(permissaoVO).toString();
				Class permissaoMenu = Class.forName(permissaoAcessoMenuVO.getClass().getName());
				Method metodoSet = permissaoMenu.getMethod("set" + metodoGet.invoke(permissaoVO).toString(), Boolean.class);
				if (permissaoVO.getPermissaoModulo()) {
					metodoSet.invoke(permissaoAcessoMenuVO, Boolean.TRUE);
				}
				
				for (PermissaoUsuarioMenuSubModuloVO permissaoSubModuloVO : permissaoVO.getPermissaoUsuarioMenuSubModuloVOs()) {
					Class classeGenericaSubModulo = Class.forName(permissaoSubModuloVO.getClass().getName());
					Method metodoGetSubModulo = classeGenericaSubModulo.getMethod("getNomeMenu");
					metodoGetSubModulo.invoke(permissaoSubModuloVO).toString();
					
					if (permissaoSubModuloVO.getPermissaoSubModulo()) {
						metodoSet = permissaoMenu.getMethod("set" + metodoGetSubModulo.invoke(permissaoSubModuloVO).toString(), Boolean.class);
						metodoSet.invoke(permissaoAcessoMenuVO, Boolean.TRUE);
					}
				}
			} catch (Exception e) {
			}
		}
		return permissaoAcessoMenuVO;
	}
	
	@Override
	public PermissaoAcessoMenuVO montarPermissoesMenuCargoAdministrativo() {
		try {
			// M�todo que monta a �rvore de permiss�es.
		} catch (Exception ex) {
			Logger.getLogger(PerfilUsuario.class.getName()).log(Level.SEVERE, null, ex);
		}
		PermissaoAcessoMenuVO permissaoAcessoMenuVO = new PermissaoAcessoMenuVO();
		List<PermissaoPerfilUsuarioEnum> listaModuloEnumVOs = (PermissaoPerfilUsuarioEnum.getListaPorMenuEnum(""));
		for (PermissaoPerfilUsuarioEnum permissaoPerfilUsuarioEnum : listaModuloEnumVOs) {
			
		
			try {
				Class classeGenerica = Class.forName(permissaoPerfilUsuarioEnum.getClass().getName());
				Method metodoGet = classeGenerica.getMethod("getKey");
				metodoGet.invoke(permissaoPerfilUsuarioEnum).toString();
				Class permissaoMenu = Class.forName(permissaoAcessoMenuVO.getClass().getName());
				Method metodoSet = permissaoMenu.getMethod("set" + metodoGet.invoke(permissaoPerfilUsuarioEnum).toString(), Boolean.class);
				metodoSet.invoke(permissaoAcessoMenuVO, Boolean.TRUE);
				
			} catch (Exception e) {
			}
		}
	
		return permissaoAcessoMenuVO;
	}
}
