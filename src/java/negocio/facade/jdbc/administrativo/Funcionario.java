package negocio.facade.jdbc.administrativo;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import negocio.comuns.administrativo.FuncionarioVO;
import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.comuns.basico.PessoaVO;
import negocio.comuns.utilitarias.Uteis;
import negocio.facade.jdbc.arquitetura.ControleAcesso;
import negocio.interfaces.administrativo.FuncionarioInterfaceFacade;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Scope("singleton")
@Lazy
public class Funcionario extends ControleAcesso implements FuncionarioInterfaceFacade {

	private static final long serialVersionUID = 1L;

	public Funcionario() {
		super();
	}

	public void validarDados(FuncionarioVO obj, UsuarioVO usuarioVO) throws Exception {
	}

	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void persistir(FuncionarioVO obj, UsuarioVO usuarioVO) throws Exception {
		if (obj.isNovoObj()) {
			incluir(obj, usuarioVO);
		} else {
			alterar(obj, usuarioVO);
		}
	}

	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void incluir(final FuncionarioVO obj, UsuarioVO usuario) throws Exception {
		try {
			validarDados(obj, usuario);
			getFacadeFactory().getPessoaFacade().incluirFuncionario(obj.getPessoaVO(), usuario);
			final String sql = "INSERT INTO Funcionario ( pessoa , datacadastro ) VALUES ( ? , ? ) returning codigo";
			obj.setCodigo((Integer) getConexao().getJdbcTemplate().query(new PreparedStatementCreator() {

				public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
					PreparedStatement sqlInserir = arg0.prepareStatement(sql);
					if (obj.getPessoaVO().getCodigo().intValue() != 0) {
						sqlInserir.setInt(1, obj.getPessoaVO().getCodigo().intValue());
					} else {
						sqlInserir.setNull(1, 0);
					}
					sqlInserir.setDate(2, Uteis.getDataJDBC(obj.getDataCadastro()));
					
					
					return sqlInserir;
				}
			}, new ResultSetExtractor() {

				public Object extractData(ResultSet arg0) throws SQLException, DataAccessException {
					if (arg0.next()) {
						obj.setNovoObj(Boolean.FALSE);
						return arg0.getInt("codigo");
						
					}
					return null;
				}
			}));
			
			obj.setNovoObj(Boolean.FALSE);
		} catch (Exception e) {
			obj.setNovoObj(Boolean.TRUE);
			throw e;
		}
	}

	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void alterar(final FuncionarioVO obj, UsuarioVO usuario) throws Exception {
		try {
			validarDados(obj, usuario);
			getFacadeFactory().getPessoaFacade().alterar(obj.getPessoaVO(), usuario);
			
			
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void excluir(FuncionarioVO obj, UsuarioVO usuario) throws Exception {
		try {
			String sql = "DELETE FROM Funcionario WHERE ((codigo = ?))";
			getConexao().getJdbcTemplate().update(sql, new Object[] { obj.getCodigo() });
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public List<FuncionarioVO> consultar(String campoConsulta, String valorConsulta, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		List<FuncionarioVO> listaConsulta = new ArrayList<FuncionarioVO>(0);
		
		if (campoConsulta.equals("NOME")) {
			listaConsulta = consultarPorNome(valorConsulta, nivelMontarDados, usuarioVO);
		}
		return listaConsulta;
	}

	public List<FuncionarioVO> consultarPorNome(String nome, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		StringBuilder sb = new StringBuilder();
		sb.append(getDadosBasicos());
		sb.append(" where pessoa.nome ilike ('").append(nome).append("%') ");
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
		return montarDadosLista(tabelaResultado, nivelMontarDados, usuarioVO);
	}
	
	
	
	@Override
	public void carregarDados(FuncionarioVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		if (nivelMontarDados.equals(NivelMontarDadosEnum.BASICO)) {
			StringBuilder sb = getDadosBasicos();
			sb.append(" where funcionario.codigo = ").append(obj.getCodigo());
			SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
			if (tabelaResultado.next()) {
				montarDadosBasicos(tabelaResultado, obj, nivelMontarDados, usuarioVO);
			}
		}
		if (nivelMontarDados.equals(NivelMontarDadosEnum.COMPLETO)) {
			StringBuilder sb = getDadosCompletos();
			sb.append(" where funcionario.codigo = ").append(obj.getCodigo());
			SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
			if (tabelaResultado.next()) {
				montarDadosCompletos(tabelaResultado, obj, nivelMontarDados, usuarioVO);
			}
		}
	}
	
	public List<FuncionarioVO> montarDadosLista(SqlRowSet dadosSQL, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		List<FuncionarioVO> listaMembroVOs = new ArrayList<FuncionarioVO>(0);
		while (dadosSQL.next()) {
						
			FuncionarioVO obj = new FuncionarioVO();
			
			montarDadosBasicos(dadosSQL, obj, nivelMontarDados, usuarioVO);
			listaMembroVOs.add(obj);
		}
		return listaMembroVOs;
	}
	
	public StringBuilder getDadosBasicos() {
		StringBuilder sb = new StringBuilder();
		sb.append("select  funcionario.codigo, funcionario.datacadastro  ,  ");
		sb.append(" pessoa.codigo AS \"pessoa.codigo\",  pessoa.nome AS \"pessoa.nome\", pessoa.email AS \"pessoa.email\", ");
		sb.append(" pessoa.dataNascimento AS \"pessoa.dataNascimento\", pessoa.cpf AS \"pessoa.cpf\", pessoa.rg AS \"pessoa.rg\", pessoa.celular AS \"pessoa.celular\",  ");
		sb.append(" pessoa.telefone1 AS \"pessoa.telefone1\", pessoa.telefone2 AS \"pessoa.telefone2\", pessoa.cep AS \"pessoa.cep\", pessoa.endereco AS \"pessoa.endereco\", pessoa.setor AS \"pessoa.setor\", ");
		sb.append(" cidade.codigo AS \"cidade.codigo\", cidade.nome AS \"cidade.nome\" ");
		sb.append(" from funcionario "); 
		sb.append("INNER join pessoa on  funcionario.pessoa = pessoa.codigo ");
		sb.append("left join cidade on pessoa.cidade = cidade.codigo "); 
		
		
		return sb;
	}
	
	public StringBuilder getDadosCompletos() {
		StringBuilder sb = new StringBuilder();
		sb.append("select * from funcionario "); 
		sb.append(" inner join pessoa on  funcionario.pessoa = pessoa.codigo");
		sb.append(" LEFT join usuario on usuario.pessoa = pessoa.codigo ");
		return sb;
	}
	
	public void montarDadosBasicos(SqlRowSet dadosSQL, FuncionarioVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		
		obj.setCodigo(dadosSQL.getInt("codigo"));
		obj.setDataCadastro(dadosSQL.getDate("datacadastro"));
		//Dados Pessoa
		        obj.getPessoaVO().setCodigo(dadosSQL.getInt("pessoa.codigo"));
		        obj.getPessoaVO().setNome(dadosSQL.getString("pessoa.nome"));
				obj.getPessoaVO().setDataNascimento(dadosSQL.getDate("pessoa.datanascimento"));
				obj.getPessoaVO().setCpf(dadosSQL.getString("pessoa.cpf"));
				obj.getPessoaVO().setRg(dadosSQL.getString("pessoa.rg"));
				obj.getPessoaVO().setCelular(dadosSQL.getString("pessoa.celular"));
				obj.getPessoaVO().setTelefone1(dadosSQL.getString("pessoa.telefone1"));
				obj.getPessoaVO().setTelefone2(dadosSQL.getString("pessoa.telefone2"));
				obj.getPessoaVO().setCep(dadosSQL.getString("pessoa.cep"));
				obj.getPessoaVO().setEndereco(dadosSQL.getString("pessoa.endereco"));
				obj.getPessoaVO().setSetor(dadosSQL.getString("pessoa.setor"));
				obj.getPessoaVO().getCidadeVO().setCodigo(dadosSQL.getInt("cidade.codigo"));
				obj.getPessoaVO().getCidadeVO().setNome(dadosSQL.getString("cidade.nome"));
				obj.getPessoaVO().setEmail(dadosSQL.getString("pessoa.email"));
		
		obj.setNovoObj(false);
	}
	
	public void montarDadosCompletos(SqlRowSet dadosSQL, FuncionarioVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		 
		obj.setCodigo(dadosSQL.getInt("codigo"));
		obj.setDataCadastro(dadosSQL.getDate("datacadastro"));
		//Dados Pessoa
				obj.getPessoaVO().setCodigo(dadosSQL.getInt("pessoa.codigo"));
				obj.getPessoaVO().setNome(dadosSQL.getString("pessoa.nome"));
				obj.getPessoaVO().setDataNascimento(dadosSQL.getDate("pessoa.dataNascimento"));
				obj.getPessoaVO().setCpf(dadosSQL.getString("pessoa.cpf"));
				obj.getPessoaVO().setRg(dadosSQL.getString("pessoa.rg"));
				obj.getPessoaVO().setCelular(dadosSQL.getString("pessoa.celular"));
				obj.getPessoaVO().setTelefone1(dadosSQL.getString("pessoa.telefone1"));
				obj.getPessoaVO().setTelefone2(dadosSQL.getString("pessoa.telefone2"));
				obj.getPessoaVO().setCep(dadosSQL.getString("pessoa.cep"));
				obj.getPessoaVO().setEndereco(dadosSQL.getString("pessoa.endereco"));
				obj.getPessoaVO().setSetor(dadosSQL.getString("pessoa.setor"));
				obj.getPessoaVO().getCidadeVO().setCodigo(dadosSQL.getInt("cidade.codigo"));
				obj.getPessoaVO().getCidadeVO().setNome(dadosSQL.getString("cidade.nome"));
				obj.getPessoaVO().setEmail(dadosSQL.getString("pessoa.email"));
		
		obj.setNovoObj(false);
		if (nivelMontarDados.equals(NivelMontarDadosEnum.TELA_CONSULTA)) {
			return;
		}
		return;
	}
	
	@Override
	public FuncionarioVO consultarFuncionarioPorCodigoUsuario(Integer usuario, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		StringBuilder sb = new StringBuilder();
		sb.append("select funcionario.codigo, pessoa.codigo AS \"pessoa.codigo\", pessoa.nome AS \"pessoa.nome\" ,pessoa.cpf AS \"pessoa.cpf\" , funcionario.datacadastro  AS \"dataCadastro\" ");
		sb.append(" from funcionario ");
		sb.append(" inner join pessoa on pessoa.codigo = funcionario.pessoa ");
		sb.append(" LEFT join usuario on usuario.pessoa = pessoa.codigo ");
		sb.append(" where usuario.codigo = ").append(usuario);
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
		FuncionarioVO obj = new FuncionarioVO();
		if (tabelaResultado.next()) {
			obj.setCodigo(tabelaResultado.getInt("codigo"));
			obj.getPessoaVO().setCodigo(tabelaResultado.getInt("pessoa.codigo"));
			obj.getPessoaVO().setNome(tabelaResultado.getString("pessoa.nome"));
			obj.getPessoaVO().setCpf(tabelaResultado.getString("pessoa.cpf"));
			obj.setDataCadastro(tabelaResultado.getDate("dataCadastro"));
		}
		return obj;
	}

}
