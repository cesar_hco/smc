package negocio.facade.jdbc.administrativo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import negocio.comuns.administrativo.PacienteVO;
import negocio.comuns.administrativo.PacienteVO;
import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.comuns.basico.PessoaVO;
import negocio.comuns.utilitarias.Uteis;
import negocio.facade.jdbc.arquitetura.ControleAcesso;
import negocio.interfaces.administrativo.PacienteInterfaceFacade;

@Repository
@Scope("singleton")
@Lazy
public class Paciente extends ControleAcesso implements PacienteInterfaceFacade {

	private static final long serialVersionUID = 1L;

	public Paciente() {
		super();
	}

	public void validarDados(PacienteVO obj, UsuarioVO usuarioVO) throws Exception {
	}

	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void persistir(PacienteVO obj, UsuarioVO usuarioVO) throws Exception {
		if (obj.isNovoObj()) {
			incluir(obj, usuarioVO);
		} else {
			alterar(obj, usuarioVO);
		}
	}


	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void incluir(final PacienteVO obj, UsuarioVO usuario) throws Exception {
		try {
			validarDados(obj, usuario);
			getFacadeFactory().getPessoaFacade().incluirCliente(obj.getPessoa(), usuario);
			final String sql = "INSERT INTO Paciente ( pessoa ,  observacao , numeroConvenio,convenio ) VALUES (  ?  , ? , ? , ?) returning codigo";
			obj.setCodigo((Integer) getConexao().getJdbcTemplate().query(new PreparedStatementCreator() {

				public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
					PreparedStatement sqlInserir = arg0.prepareStatement(sql);
					if (obj.getPessoa().getCodigo().intValue() != 0) {
						sqlInserir.setInt(1, obj.getPessoa().getCodigo().intValue());
					} else {
						sqlInserir.setNull(1, 0);
					}
					
					sqlInserir.setString(2, obj.getObservacao().toString());
					sqlInserir.setString(3, obj.getNumeroConvenio().toString());
					sqlInserir.setInt(4, obj.getConvenio().getCodigo().intValue());
					
					return sqlInserir;
				}
			}, new ResultSetExtractor() {

				public Object extractData(ResultSet arg0) throws SQLException, DataAccessException {
					if (arg0.next()) {
						obj.setNovoObj(Boolean.FALSE);
						return arg0.getInt("codigo");
						
					}
					return null;
				}
			}));
			
			obj.setNovoObj(Boolean.FALSE);
		} catch (Exception e) {
			obj.setNovoObj(Boolean.TRUE);
			throw e;
		}
	}


	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void alterar(final PacienteVO obj, UsuarioVO usuario) throws Exception {
		try {
			validarDados(obj, usuario);
			getFacadeFactory().getPessoaFacade().alterar(obj.getPessoa(), usuario);
			
			final String sql = "UPDATE paciente set observacao = ?, convenio = ?, numeroconvenio = ? WHERE ((codigo = ?))";

			getConexao().getJdbcTemplate().update(new PreparedStatementCreator() {

				public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
					PreparedStatement sqlAlterar = arg0.prepareStatement(sql);
					sqlAlterar.setString(1, obj.getObservacao());
					sqlAlterar.setInt(2, obj.getConvenio().getCodigo());
					sqlAlterar.setString(3, obj.getNumeroConvenio());
					sqlAlterar.setInt(4, obj.getCodigo().intValue());
					return sqlAlterar;
				}
			});
		} catch (Exception e) {
			throw e;
		}
	}


	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void excluir(PacienteVO obj, UsuarioVO usuario) throws Exception {
		try {
			String sql = "DELETE FROM Paciente WHERE ((codigo = ?))";
			getConexao().getJdbcTemplate().update(sql, new Object[] { obj.getCodigo() });
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public List<PacienteVO> consultar(String campoConsulta, String valorConsulta, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		List<PacienteVO> listaConsulta = new ArrayList<PacienteVO>(0);
		
		if (campoConsulta.equals("NOME")) {
			listaConsulta = consultarPorNome(valorConsulta, nivelMontarDados, usuarioVO);
		}
		return listaConsulta;
	}

	public List<PacienteVO> consultarPorNome(String nome, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		StringBuilder sb = new StringBuilder();
		sb.append(getDadosBasicos());
		sb.append(" where pessoa.nome ilike ('").append(nome).append("%') ");
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
		return montarDadosLista(tabelaResultado, nivelMontarDados, usuarioVO);
	}
	
	@Override
	public void carregarDados(PacienteVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		if (nivelMontarDados.equals(NivelMontarDadosEnum.BASICO)) {
			StringBuilder sb = getDadosBasicos();
			sb.append(" where paciente.codigo = ").append(obj.getCodigo());
			SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
			if (tabelaResultado.next()) {
				montarDadosBasicos(tabelaResultado, obj, nivelMontarDados, usuarioVO);
			}
		}
		if (nivelMontarDados.equals(NivelMontarDadosEnum.COMPLETO)) {
			StringBuilder sb = getDadosCompletos();
			sb.append(" where paciente.codigo = ").append(obj.getCodigo());
			SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
			if (tabelaResultado.next()) {
				montarDadosCompletos(tabelaResultado, obj, nivelMontarDados, usuarioVO);
			}
		}
	}
	
	public List<PacienteVO> montarDadosLista(SqlRowSet dadosSQL, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		List<PacienteVO> listaMembroVOs = new ArrayList<PacienteVO>(0);
		while (dadosSQL.next()) {
						
			PacienteVO obj = new PacienteVO();
			
			montarDadosBasicos(dadosSQL, obj, nivelMontarDados, usuarioVO);
			listaMembroVOs.add(obj);
		}
		return listaMembroVOs;
	}
	
	public StringBuilder getDadosBasicos() {
		StringBuilder sb = new StringBuilder();
		sb.append("select  paciente.codigo, paciente.observacao, paciente.numeroConvenio, ");
		sb.append(" pessoa.codigo AS \"pessoa.codigo\",  pessoa.nome AS \"pessoa.nome\", pessoa.email AS \"pessoa.email\", ");
		sb.append(" pessoa.dataNascimento AS \"pessoa.dataNascimento\", pessoa.cpf AS \"pessoa.cpf\", pessoa.rg AS \"pessoa.rg\", pessoa.celular AS \"pessoa.celular\",  ");
		sb.append(" pessoa.telefone1 AS \"pessoa.telefone1\", pessoa.telefone2 AS \"pessoa.telefone2\", pessoa.cep AS \"pessoa.cep\", pessoa.endereco AS \"pessoa.endereco\", pessoa.setor AS \"pessoa.setor\", ");
		sb.append(" cidade.codigo AS \"cidade.codigo\", cidade.nome AS \"cidade.nome\" ,");
		sb.append(" convenio.codigo as \"convenio.codigo\" , convenio.empresa as \"convenio.empresa\",convenio.ativo as \"convenio.ativo\",convenio.descricao as \"convenio.descricao\" "); 
		sb.append(" from paciente "); 
		sb.append("INNER join pessoa on  paciente.pessoa = pessoa.codigo ");
		sb.append("left join cidade on pessoa.cidade = cidade.codigo "); 
		sb.append("left join convenio on paciente.convenio = convenio.codigo "); 
		
		
		return sb;
	}
	
	public StringBuilder getDadosCompletos() {
		StringBuilder sb = new StringBuilder();
		sb.append("select * from paciente "); 
		sb.append(" inner join pessoa on  paciente.pessoa = pessoa.codigo");
		sb.append(" LEFT join convenio on paciente.convenio = convenio.codigo ");
		return sb;
	}
	
	public void montarDadosBasicos(SqlRowSet dadosSQL, PacienteVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		
		obj.setCodigo(dadosSQL.getInt("codigo"));
		obj.setObservacao(dadosSQL.getString("observacao"));
		obj.setNumeroConvenio(dadosSQL.getString("numeroConvenio"));
		
		//Dados Pessoa
		        obj.getPessoa().setCodigo(dadosSQL.getInt("pessoa.codigo"));
		        obj.getPessoa().setNome(dadosSQL.getString("pessoa.nome"));
				obj.getPessoa().setDataNascimento(dadosSQL.getDate("pessoa.datanascimento"));
				obj.getPessoa().setCpf(dadosSQL.getString("pessoa.cpf"));
				obj.getPessoa().setRg(dadosSQL.getString("pessoa.rg"));
				obj.getPessoa().setCelular(dadosSQL.getString("pessoa.celular"));
				obj.getPessoa().setTelefone1(dadosSQL.getString("pessoa.telefone1"));
				obj.getPessoa().setTelefone2(dadosSQL.getString("pessoa.telefone2"));
				obj.getPessoa().setCep(dadosSQL.getString("pessoa.cep"));
				obj.getPessoa().setEndereco(dadosSQL.getString("pessoa.endereco"));
				obj.getPessoa().setSetor(dadosSQL.getString("pessoa.setor"));
				obj.getPessoa().getCidadeVO().setCodigo(dadosSQL.getInt("cidade.codigo"));
				obj.getPessoa().getCidadeVO().setNome(dadosSQL.getString("cidade.nome"));
				obj.getPessoa().setEmail(dadosSQL.getString("pessoa.email"));
		//Dados Convenio
		
				obj.getConvenio().setCodigo(dadosSQL.getInt("convenio.codigo"));
				obj.getConvenio().setDescricao(dadosSQL.getString("convenio.descricao"));
				obj.getConvenio().getEmpresa().setCodigo(dadosSQL.getInt("convenio.empresa"));
				obj.getConvenio().setAtivo(dadosSQL.getBoolean("convenio.ativo"));
				
				
		obj.setNovoObj(false);
	}
	
	public void montarDadosCompletos(SqlRowSet dadosSQL, PacienteVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		 
		obj.setCodigo(dadosSQL.getInt("codigo"));
		obj.setObservacao(dadosSQL.getString("observacao"));
		obj.setNumeroConvenio(dadosSQL.getString("numeroConvenio"));
		
		//Dados Pessoa
				obj.getPessoa().setCodigo(dadosSQL.getInt("pessoa.codigo"));
				obj.getPessoa().setNome(dadosSQL.getString("pessoa.nome"));
				obj.getPessoa().setDataNascimento(dadosSQL.getDate("pessoa.dataNascimento"));
				obj.getPessoa().setCpf(dadosSQL.getString("pessoa.cpf"));
				obj.getPessoa().setRg(dadosSQL.getString("pessoa.rg"));
				obj.getPessoa().setCelular(dadosSQL.getString("pessoa.celular"));
				obj.getPessoa().setTelefone1(dadosSQL.getString("pessoa.telefone1"));
				obj.getPessoa().setTelefone2(dadosSQL.getString("pessoa.telefone2"));
				obj.getPessoa().setCep(dadosSQL.getString("pessoa.cep"));
				obj.getPessoa().setEndereco(dadosSQL.getString("pessoa.endereco"));
				obj.getPessoa().setSetor(dadosSQL.getString("pessoa.setor"));
				obj.getPessoa().getCidadeVO().setCodigo(dadosSQL.getInt("cidade.codigo"));
				obj.getPessoa().getCidadeVO().setNome(dadosSQL.getString("cidade.nome"));
				obj.getPessoa().setEmail(dadosSQL.getString("pessoa.email"));
		
				//Dados Convenio
				
				obj.getConvenio().setCodigo(dadosSQL.getInt("convenio.codigo"));
				obj.getConvenio().setDescricao(dadosSQL.getString("convenio.descricao"));
				obj.getConvenio().getEmpresa().setCodigo(dadosSQL.getInt("convenio.empresa"));
				obj.getConvenio().setAtivo(dadosSQL.getBoolean("convenio.ativo"));
				
				
		obj.setNovoObj(false);
		if (nivelMontarDados.equals(NivelMontarDadosEnum.TELA_CONSULTA)) {
			return;
		}
		return;
	}
	
		
}
