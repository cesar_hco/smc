/**
 * 
 */
package negocio.facade.jdbc.administrativo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import negocio.comuns.administrativo.EmpresaVO;
import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.facade.jdbc.arquitetura.ControleAcesso;
import negocio.interfaces.administrativo.EmpresaInterfaceFacade;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Carlos Eug�nio
 *
 */
@Repository
@Scope("singleton")
@Lazy
public class Empresa extends ControleAcesso implements EmpresaInterfaceFacade {

	private static final long serialVersionUID = 1L;

	public Empresa() {
		super();
	}
	public void validarDados(EmpresaVO obj, UsuarioVO usuarioVO) throws Exception {
		if (obj.getDescricao().equals("")) {
			throw new Exception("O campo DESCRI��O deve ser informado!");
		}
	}

	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void persistir(EmpresaVO obj, UsuarioVO usuarioVO) throws Exception {
		if (obj.isNovoObj()) {
			incluir(obj, usuarioVO);
		} else {
			alterar(obj, usuarioVO);
		}
	}

	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void incluir(final EmpresaVO obj, UsuarioVO usuario) throws Exception {
		try {
			validarDados(obj, usuario);
			final String sql = "INSERT INTO Empresa( descricao, nomeFantasia, cep, endereco, setor, complemento, cidade, inscricaoEstadual, cnpj, telefone1, telefone2) "
					+ " VALUES ( ?, ?, ?, ?, ?, ?,? ,? ,?,?,?) returning codigo";
			obj.setCodigo((Integer) getConexao().getJdbcTemplate().query(new PreparedStatementCreator() {

				public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
					PreparedStatement sqlInserir = arg0.prepareStatement(sql);
					sqlInserir.setString(1, obj.getDescricao());
					sqlInserir.setString(2, obj.getNomeFantasia());
					sqlInserir.setString(3, obj.getCep());
					sqlInserir.setString(4, obj.getEndereco());
					sqlInserir.setString(5, obj.getSetor());
					sqlInserir.setString(6, obj.getComplemento());
					
					if (obj.getCidadeVO().getCodigo().intValue() != 0) {
						sqlInserir.setInt(7, obj.getCidadeVO().getCodigo().intValue());
					} else {
						sqlInserir.setNull(7, 0);
					}
					sqlInserir.setString(8, obj.getInscricaoEstadual());
					sqlInserir.setString(9, obj.getCnpj());
					sqlInserir.setString(10, obj.getTelefone1());
					sqlInserir.setString(11, obj.getTelefone2());
					
					return sqlInserir;
				}
			}, new ResultSetExtractor() {

				public Object extractData(ResultSet arg0) throws SQLException, DataAccessException {
					if (arg0.next()) {
						obj.setNovoObj(Boolean.FALSE);
						return arg0.getInt("codigo");
					}
					return null;
				}
			}));
			
			obj.setNovoObj(Boolean.FALSE);
		} catch (Exception e) {
			obj.setNovoObj(Boolean.TRUE);
			throw e;
		}
	}

	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void alterar(final EmpresaVO obj, UsuarioVO usuario) throws Exception {
		try {
			validarDados(obj, usuario);
			final String sql = "UPDATE Empresa set descricao=?, nomeFantasia=?, cep=?, endereco=?, setor=?, complemento=?, cidade=?, inscricaoEstadual=?, cnpj=?, telefone1=?, telefone2=? WHERE ((codigo = ?))";

			getConexao().getJdbcTemplate().update(new PreparedStatementCreator() {

				public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
					PreparedStatement sqlAlterar = arg0.prepareStatement(sql);
					sqlAlterar.setString(1, obj.getDescricao());
					sqlAlterar.setString(2, obj.getNomeFantasia());
					sqlAlterar.setString(3, obj.getCep());
					sqlAlterar.setString(4, obj.getEndereco());
					sqlAlterar.setString(5, obj.getSetor());
					sqlAlterar.setString(6, obj.getComplemento());
					
					if (obj.getCidadeVO().getCodigo().intValue() != 0) {
						sqlAlterar.setInt(7, obj.getCidadeVO().getCodigo().intValue());
					} else {
						sqlAlterar.setNull(7, 0);
					}
					sqlAlterar.setString(8, obj.getInscricaoEstadual());
					sqlAlterar.setString(9, obj.getCnpj());
					sqlAlterar.setString(10, obj.getTelefone1());
					sqlAlterar.setString(11, obj.getTelefone2());
					sqlAlterar.setInt(12, obj.getCodigo().intValue());
					return sqlAlterar;
				}
			});
			
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void excluir(EmpresaVO obj, UsuarioVO usuario) throws Exception {
		try {
			String sql = "DELETE FROM Empresa WHERE ((codigo = ?))";
			getConexao().getJdbcTemplate().update(sql, new Object[] { obj.getCodigo() });
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public List<EmpresaVO> consultar(String campoConsulta, String valorConsulta, NivelMontarDadosEnum nivelMontarDados, Integer empresaLogado, UsuarioVO usuarioVO) {
		List<EmpresaVO> listaConsulta = new ArrayList<EmpresaVO>(0);
		if (campoConsulta.equals("DESCRICAO")) {
			listaConsulta = consultarPorDescricao(valorConsulta, nivelMontarDados, usuarioVO);
		}
		return listaConsulta;
	}

	@Override
	public List<EmpresaVO> consultarPorDescricao(String nome, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		StringBuilder sb = new StringBuilder();
		sb.append(getDadosBasicos());
		sb.append(" where empresa.descricao ilike ('").append(nome).append("%') ");
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
		return montarDadosLista(tabelaResultado, nivelMontarDados, usuarioVO);
	}
	
	@Override
	public void carregarDados(EmpresaVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		if (nivelMontarDados.equals(NivelMontarDadosEnum.BASICO)) {
			StringBuilder sb = getDadosBasicos();
			sb.append(" where empresa.codigo = ").append(obj.getCodigo());
			SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
			if (tabelaResultado.next()) {
				montarDadosBasicos(tabelaResultado, obj, nivelMontarDados, usuarioVO);
			}
		}
		if (nivelMontarDados.equals(NivelMontarDadosEnum.COMPLETO)) {
			StringBuilder sb = getDadosCompletos();
			sb.append(" where empresa.codigo = ").append(obj.getCodigo());
			SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
			if (tabelaResultado.next()) {
				montarDadosCompletos(tabelaResultado, obj, nivelMontarDados, usuarioVO);
			}
		}
	}
	
	public List<EmpresaVO> montarDadosLista(SqlRowSet dadosSQL, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		List<EmpresaVO> listaMembroVOs = new ArrayList<EmpresaVO>(0);
		while (dadosSQL.next()) {
			EmpresaVO obj = new EmpresaVO();
			montarDadosBasicos(dadosSQL, obj, nivelMontarDados, usuarioVO);
			listaMembroVOs.add(obj);
		}
		return listaMembroVOs;
	}
	
	public StringBuilder getDadosBasicos() {
		StringBuilder sb = new StringBuilder();
		sb.append("select empresa.*, ");
		sb.append(" cidade.codigo AS \"cidade.codigo\", cidade.nome AS \"cidade.nome\" ");
		sb.append(" from empresa ");
		sb.append(" left join cidade on cidade.codigo = empresa.cidade ");
		return sb;
	}
	
	public StringBuilder getDadosCompletos() {
		StringBuilder sb = new StringBuilder();
		sb.append("select empresa.*, ");
		sb.append(" cidade.codigo AS \"cidade.codigo\", cidade.nome AS \"cidade.nome\" ");
		sb.append(" from empresa ");
		sb.append(" left join cidade on cidade.codigo = empresa.cidade ");
		return sb;
	}
	
	public void montarDadosBasicos(SqlRowSet dadosSQL, EmpresaVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		obj.setCodigo(dadosSQL.getInt("codigo"));
		obj.setDescricao(dadosSQL.getString("descricao"));
		obj.setNomeFantasia(dadosSQL.getString("nomeFantasia"));
		obj.setCnpj(dadosSQL.getString("cnpj"));
		obj.setInscricaoEstadual(dadosSQL.getString("inscricaoEstadual"));
		obj.setCep(dadosSQL.getString("cep"));
		obj.setEndereco(dadosSQL.getString("endereco"));
		obj.setSetor(dadosSQL.getString("setor"));
		obj.setComplemento(dadosSQL.getString("complemento"));
		obj.getCidadeVO().setCodigo(dadosSQL.getInt("cidade.codigo"));
		obj.getCidadeVO().setNome(dadosSQL.getString("cidade.nome"));
		obj.setTelefone1(dadosSQL.getString("telefone1"));
		obj.setTelefone2(dadosSQL.getString("telefone2"));
		obj.setNovoObj(false);
	}
	
	public void montarDadosCompletos(SqlRowSet dadosSQL, EmpresaVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		obj.setCodigo(dadosSQL.getInt("codigo"));
		obj.setDescricao(dadosSQL.getString("descricao"));
		obj.setNomeFantasia(dadosSQL.getString("nomeFantasia"));
		obj.setCnpj(dadosSQL.getString("cnpj"));
		obj.setInscricaoEstadual(dadosSQL.getString("inscricaoEstadual"));
		obj.setCep(dadosSQL.getString("cep"));
		obj.setEndereco(dadosSQL.getString("endereco"));
		obj.setSetor(dadosSQL.getString("setor"));
		obj.setComplemento(dadosSQL.getString("complemento"));
		obj.getCidadeVO().setCodigo(dadosSQL.getInt("cidade.codigo"));
		obj.getCidadeVO().setNome(dadosSQL.getString("cidade.nome"));
		obj.setTelefone1(dadosSQL.getString("telefone1"));
		obj.setTelefone2(dadosSQL.getString("telefone2"));
		obj.setNovoObj(false);
		if (nivelMontarDados.equals(NivelMontarDadosEnum.TELA_CONSULTA)) {
			return;
		}
		return;
	}
}
