/**
 * 
 */
package negocio.facade.jdbc.administrativo;

import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import negocio.comuns.administrativo.EmpresaVO;
import negocio.comuns.administrativo.FuncionarioVO;
import negocio.comuns.administrativo.PerfilUsuarioVO;
import negocio.comuns.administrativo.UsuarioEmpresaPerfilUsuarioVO;
import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.administrativo.enumeradores.TipoUsuarioEnum;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.comuns.utilitarias.Uteis;
import negocio.facade.jdbc.arquitetura.ControleAcesso;
import negocio.interfaces.basico.UsuarioInterfaceFacade;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Carlos Eug�nio
 *
 */
@Repository
@Scope("singleton")
@Lazy
public class Usuario extends ControleAcesso implements UsuarioInterfaceFacade {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Usuario() {
		super();
	}

	public void validarDados(UsuarioVO obj, UsuarioVO usuarioLogado) throws Exception {
		if (obj.getPessoaVO().getCodigo().equals(0)) {
			throw new Exception("Deve ser informado um tipo de usu�rio!");
		}
		if (obj.getUserName().equals("")) {
			throw new Exception("O campo USERNAME deve ser informado!");
		}
		if (obj.getSenha().equals("")) {
			throw new Exception("O campo SENHA deve ser informado!");
		}
	}

	public void validarDadosUnicidadeUsuario(UsuarioVO usuarioVO) throws Exception {
		Boolean existeUsuarioComUserName = consultarExistenciaUsuarioPorUserName(usuarioVO.getCodigo(), usuarioVO.getUserName(), null);
		if (existeUsuarioComUserName) {
			throw new Exception("J� existe um usu�rio cadastrado com esse username. Favor digitar outro username.");
		}
	}

	public Boolean consultarExistenciaUsuarioPorUserName(Integer usuario, String userName, UsuarioVO usuarioLogadoVO) {
		StringBuilder sb = new StringBuilder();
		sb.append("select codigo from Usuario where sem_acentos(upper(username)) = '").append(Uteis.removeCaractersEspeciais(userName.toUpperCase())).append("' ");
		sb.append(" and codigo != ").append(usuario);
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
		if (tabelaResultado.next()) {
			return true;
		}
		return false;
	}

	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void persistir(UsuarioVO obj, UsuarioVO usuarioVO) throws Exception {
		if (obj.isNovoObj()) {
			incluir(obj, usuarioVO);
		} else {
			alterar(obj, usuarioVO);
		}
	}

	@Override
	public void inicializarDadosPessoaUsuario(UsuarioVO usuarioVO, FuncionarioVO funcionarioVO) {
		if (usuarioVO.getTipoUsuario().equals(TipoUsuarioEnum.FUNCIONARIO)) {
			usuarioVO.getPessoaVO().setCodigo(funcionarioVO.getPessoaVO().getCodigo());
			usuarioVO.getPessoaVO().setNome(funcionarioVO.getPessoaVO().getNome());
		} 
	}

	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void incluir(final UsuarioVO obj, UsuarioVO usuarioLogado) throws Exception {
		try {
			validarDados(obj, usuarioLogado);
			validarDadosUnicidadeUsuario(obj);
			final String sql = "INSERT INTO Usuario( pessoa, tipoUsuario, userName, senha, exerceCargoAdministrativo) VALUES ( ?, ?, ?, ?, ?) returning codigo";
			obj.setCodigo((Integer) getConexao().getJdbcTemplate().query(new PreparedStatementCreator() {

				public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
					PreparedStatement sqlInserir = arg0.prepareStatement(sql);
					try {
						if (obj.getPessoaVO().getCodigo().intValue() != 0) {
							sqlInserir.setInt(1, obj.getPessoaVO().getCodigo().intValue());
						} else {
							sqlInserir.setNull(1, 0);
						}
						sqlInserir.setString(2, obj.getTipoUsuario().toString());
						sqlInserir.setString(3, obj.getUserName());

						sqlInserir.setString(4, Uteis.encriptar(obj.getSenha()));
						sqlInserir.setBoolean(5, obj.getExerceCargoAdministrativo());
					} catch (UnsupportedEncodingException e) {
						return null;
					}
					return sqlInserir;
				}
			}, new ResultSetExtractor() {

				public Object extractData(ResultSet arg0) throws SQLException, DataAccessException {
					if (arg0.next()) {
						obj.setNovoObj(Boolean.FALSE);
						return arg0.getInt("codigo");
					}
					return null;
				}
			}));
			getFacadeFactory().getUsuarioEmpresaPerfilUsuarioFacade().incluirUsuarioEmpresaPerfilUsuarioVOs(obj.getUsuarioEmpresaPerfilUsuarioVOs(), obj, usuarioLogado);
			obj.setNovoObj(Boolean.FALSE);
		} catch (Exception e) {
			obj.setNovoObj(Boolean.TRUE);
			throw e;
		}
	}

	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void alterar(final UsuarioVO obj, UsuarioVO usuarioLogado) throws Exception {
		try {
			validarDados(obj, usuarioLogado);
			validarDadosUnicidadeUsuario(obj);
			final String sql = "UPDATE Usuario set pessoa=?, tipoUsuario=?, userName=?, senha=?, exerceCargoAdministrativo=? WHERE ((codigo = ?))";

			getConexao().getJdbcTemplate().update(new PreparedStatementCreator() {

				public PreparedStatement createPreparedStatement(Connection arg0) throws SQLException {
					PreparedStatement sqlAlterar = arg0.prepareStatement(sql);
					try {
						if (obj.getPessoaVO().getCodigo().intValue() != 0) {
							sqlAlterar.setInt(1, obj.getPessoaVO().getCodigo().intValue());
						} else {
							sqlAlterar.setNull(1, 0);
						}
						sqlAlterar.setString(2, obj.getTipoUsuario().toString());
						
						if (obj.getAlterarSenha() == false) {
                            sqlAlterar.setString(3, Uteis.encriptar(obj.getSenha()));
                        } else {
                            sqlAlterar.setString(3, obj.getSenha());
                        }
						
						
						sqlAlterar.setString(3, obj.getUserName());
						
                            sqlAlterar.setString(4, Uteis.encriptar(obj.getSenha()));
                        
						sqlAlterar.setBoolean(5, obj.getExerceCargoAdministrativo());
					} catch (UnsupportedEncodingException e) {
						return null;
					}
					sqlAlterar.setInt(6, obj.getCodigo().intValue());
					return sqlAlterar;
				}
			});
			getFacadeFactory().getUsuarioEmpresaPerfilUsuarioFacade().alterarUsuarioEmpresaPerfilUsuarioVOs(obj.getUsuarioEmpresaPerfilUsuarioVOs(), obj, usuarioLogado);
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public void excluir(UsuarioVO obj, UsuarioVO usuario) throws Exception {
		try {
			String sql = "DELETE FROM Usuario WHERE ((codigo = ?))";
			getConexao().getJdbcTemplate().update(sql, new Object[] { obj.getCodigo() });
		} catch (Exception e) {
			throw e;
		}
	}

	@Override
	public List<UsuarioVO> consultar(String campoConsulta, String valorConsulta, UsuarioVO usuarioVO) {
		List<UsuarioVO> listaConsulta = new ArrayList<UsuarioVO>(0);
		if (campoConsulta.equals("NOME")) {
			listaConsulta = consutarPorNome(valorConsulta, usuarioVO);
		}
		return listaConsulta;
	}

	public List<UsuarioVO> consultarTipoUsuario(String campoConsulta, String valorConsulta, TipoUsuarioEnum tipoUsuario, UsuarioVO usuarioVO) {
		List<UsuarioVO> listaConsulta = new ArrayList<UsuarioVO>(0);
		if (campoConsulta.equals("NOME")) {
			listaConsulta = consutarPorNome(valorConsulta, usuarioVO);
		}
		return listaConsulta;
	}

	public List<UsuarioVO> consutarPorNome(String nome, UsuarioVO usuarioVO) {
		StringBuilder sb = new StringBuilder();
		sb.append(getDadosBasicos());
		sb.append(" where pessoa.nome ilike ('").append(nome).append("%') ");
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
		return montarDadosLista(tabelaResultado, NivelMontarDadosEnum.BASICO, usuarioVO);
	}

	@Override
	public List<UsuarioVO> consultarPorEmpresa(Integer predio, NivelMontarDadosEnum nivelMontarDadosEnum, UsuarioVO usuarioVO) {
		StringBuilder sb = new StringBuilder();
		sb.append(getDadosBasicos());
		sb.append(" inner join predioPastor on predioPastor.pastor = pastor.codigo ");
		sb.append(" where predioPastor.predio = ").append(predio);
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
		return montarDadosLista(tabelaResultado, NivelMontarDadosEnum.BASICO, usuarioVO);

	}

	@Override
	public void carregarDados(UsuarioVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		if (nivelMontarDados.equals(NivelMontarDadosEnum.BASICO)) {
			StringBuilder sb = getDadosBasicos();
			sb.append(" where usuario.codigo = ").append(obj.getCodigo());
			SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
			if (tabelaResultado.next()) {
				montarDadosBasicos(tabelaResultado, obj, nivelMontarDados, usuarioVO);
			}
		}
	
		
		if (nivelMontarDados.equals(NivelMontarDadosEnum.COMPLETO)) {
			StringBuilder sb = getDadosCompletos();
			sb.append(" where usuario.codigo = ").append(obj.getCodigo());
			SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
			if (tabelaResultado.next()) {
				montarDadosCompletos(tabelaResultado, obj, nivelMontarDados, usuarioVO);
			}
		}
	}
	@Override
	public void carregarDadosTeste(UsuarioVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		if (nivelMontarDados.equals(NivelMontarDadosEnum.BASICO)) {
			StringBuilder sb = getDadosBasicos();
			sb.append(" where usuario.pessoa = ").append(obj.getCodigo());
			SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
			if (tabelaResultado.next()) {
				montarDadosBasicos(tabelaResultado, obj, nivelMontarDados, usuarioVO);
			}
		}
	
		
		if (nivelMontarDados.equals(NivelMontarDadosEnum.COMPLETO)) {
			StringBuilder sb = getDadosCompletos();
			sb.append(" where pessoa.codigo = ").append(obj.getPessoaVO().getCodigo());
			SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
			if (tabelaResultado.next()) {
				montarDadosCompletos(tabelaResultado, obj, nivelMontarDados, usuarioVO);
			}
		}
	}
	

	public List<UsuarioVO> montarDadosLista(SqlRowSet dadosSQL, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		List<UsuarioVO> listaUsuarioVOs = new ArrayList<UsuarioVO>(0);
		while (dadosSQL.next()) {
			UsuarioVO obj = new UsuarioVO();
			montarDadosBasicos(dadosSQL, obj, nivelMontarDados, usuarioVO);
			listaUsuarioVOs.add(obj);
		}
		return listaUsuarioVOs;
	}

	public StringBuilder getDadosBasicos() {
		StringBuilder sb = new StringBuilder();
		sb.append("select usuario.codigo, usuario.tipoUsuario, usuario.userName, usuario.senha, usuario.exerceCargoAdministrativo, ");
		sb.append(" pessoa.codigo AS \"pessoa.codigo\",  pessoa.nome AS \"pessoa.nome\", pessoa.email AS \"pessoa.email\", ");
		sb.append(" pessoa.dataNascimento AS \"pessoa.dataNascimento\", pessoa.cpf AS \"pessoa.cpf\", pessoa.rg AS \"pessoa.rg\", pessoa.celular AS \"pessoa.celular\",  ");
		sb.append(" pessoa.telefone1 AS \"pessoa.telefone1\", pessoa.telefone2 AS \"pessoa.telefone2\", pessoa.cep AS \"pessoa.cep\", pessoa.endereco AS \"pessoa.endereco\", pessoa.setor AS \"pessoa.setor\" ");
		sb.append(" from usuario ");
		sb.append(" inner join pessoa on pessoa.codigo = usuario.pessoa ");
		return sb;
	}

	public StringBuilder getDadosCompletos() {
		StringBuilder sb = new StringBuilder();
		sb.append("select usuario.codigo, usuario.tipoUsuario, usuario.userName, cast(usuario.senha as text) as \"senha\", usuario.exerceCargoAdministrativo, ");
		sb.append(" pessoa.codigo AS \"pessoa.codigo\",  pessoa.nome AS \"pessoa.nome\", pessoa.email AS \"pessoa.email\", ");
		sb.append(" pessoa.dataNascimento AS \"pessoa.dataNascimento\", pessoa.cpf AS \"pessoa.cpf\", pessoa.rg AS \"pessoa.rg\", pessoa.celular AS \"pessoa.celular\",  ");
		sb.append(" pessoa.telefone1 AS \"pessoa.telefone1\", pessoa.telefone2 AS \"pessoa.telefone2\", pessoa.cep AS \"pessoa.cep\", pessoa.endereco AS \"pessoa.endereco\", pessoa.setor AS \"pessoa.setor\" ");
		sb.append(" from usuario ");
		sb.append(" inner join pessoa on pessoa.codigo = usuario.pessoa ");
		return sb;
	}

	public void montarDadosBasicos(SqlRowSet dadosSQL, UsuarioVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		obj.setCodigo(dadosSQL.getInt("codigo"));
		if (dadosSQL.getString("tipoUsuario") != null) {
			obj.setTipoUsuario(TipoUsuarioEnum.valueOf(dadosSQL.getString("tipoUsuario")));
		}
		obj.setUserName(dadosSQL.getString("userName"));
		obj.setSenha(dadosSQL.getString("senha"));
		
		
		obj.setExerceCargoAdministrativo(dadosSQL.getBoolean("exerceCargoAdministrativo"));
		// Dados Pessoa
		obj.getPessoaVO().setCodigo(dadosSQL.getInt("pessoa.codigo"));
		obj.getPessoaVO().setNome(dadosSQL.getString("pessoa.nome"));
		obj.getPessoaVO().setDataNascimento(dadosSQL.getDate("pessoa.dataNascimento"));
		obj.getPessoaVO().setCpf(dadosSQL.getString("pessoa.cpf"));
		obj.getPessoaVO().setRg(dadosSQL.getString("pessoa.rg"));
		obj.getPessoaVO().setCelular(dadosSQL.getString("pessoa.celular"));
		obj.getPessoaVO().setTelefone1(dadosSQL.getString("pessoa.telefone1"));
		obj.getPessoaVO().setTelefone2(dadosSQL.getString("pessoa.telefone2"));
		obj.getPessoaVO().setCep(dadosSQL.getString("pessoa.cep"));
		obj.getPessoaVO().setEndereco(dadosSQL.getString("pessoa.endereco"));
		obj.getPessoaVO().setSetor(dadosSQL.getString("pessoa.setor"));
		obj.getPessoaVO().setEmail(dadosSQL.getString("pessoa.email"));
		obj.setNovoObj(false);
	}

	public void montarDadosCompletos(SqlRowSet dadosSQL, UsuarioVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) {
		obj.setCodigo(dadosSQL.getInt("codigo"));
		if (dadosSQL.getString("tipoUsuario") != null) {
			obj.setTipoUsuario(TipoUsuarioEnum.valueOf(dadosSQL.getString("tipoUsuario")));
		}
		
		obj.setUserName(dadosSQL.getString("userName"));
		obj.setSenha(dadosSQL.getString("senha"));
		obj.setExerceCargoAdministrativo(dadosSQL.getBoolean("exerceCargoAdministrativo"));
		// Dados Pessoa
		obj.getPessoaVO().setCodigo(dadosSQL.getInt("pessoa.codigo"));
		obj.getPessoaVO().setNome(dadosSQL.getString("pessoa.nome"));
		obj.getPessoaVO().setDataNascimento(dadosSQL.getDate("pessoa.dataNascimento"));
		obj.getPessoaVO().setCpf(dadosSQL.getString("pessoa.cpf"));
		obj.getPessoaVO().setRg(dadosSQL.getString("pessoa.rg"));
		obj.getPessoaVO().setCelular(dadosSQL.getString("pessoa.celular"));
		obj.getPessoaVO().setTelefone1(dadosSQL.getString("pessoa.telefone1"));
		obj.getPessoaVO().setTelefone2(dadosSQL.getString("pessoa.telefone2"));
		obj.getPessoaVO().setCep(dadosSQL.getString("pessoa.cep"));
		obj.getPessoaVO().setEndereco(dadosSQL.getString("pessoa.endereco"));
		obj.getPessoaVO().setSetor(dadosSQL.getString("pessoa.setor"));
		obj.getPessoaVO().setEmail(dadosSQL.getString("pessoa.email"));
//		obj.setUsuarioPredioPerfilUsuarioVOs(getFacadeFactory().getUsuarioPredioPerfilUsuarioFacade().consultarPorUsuario(obj.getCodigo(), NivelMontarDadosEnum.BASICO, usuarioVO));
		obj.setNovoObj(false);
		if (nivelMontarDados.equals(NivelMontarDadosEnum.TELA_CONSULTA)) {
			return;
		}
		return;
	}

	public void validarDadoAdicaoEmpresaPerfilUsuario(EmpresaVO empresaVO, PerfilUsuarioVO perfilUsuarioVO) throws Exception {
		if (empresaVO.getCodigo().equals(0)) {
			throw new Exception("O campo EMPRESA deve ser informado!");
		}
		if (perfilUsuarioVO.getCodigo().equals(0)) {
			throw new Exception("O campo PERFIL USU�RIO deve ser informado!");
		}
	}

	@Override
	public void adicionarEmpresaPerfilUsuario(List<UsuarioEmpresaPerfilUsuarioVO> listaUsuarioEmpresaPerfilUsuarioVOs, EmpresaVO empresaVO, PerfilUsuarioVO perfilUsuarioVO, UsuarioVO usuarioLogado) throws Exception {
		validarDadoAdicaoEmpresaPerfilUsuario(empresaVO, perfilUsuarioVO);
		int index = 0;
		for (UsuarioEmpresaPerfilUsuarioVO usuarioEmpresaPerfilUsuarioVO : listaUsuarioEmpresaPerfilUsuarioVOs) {
			if (usuarioEmpresaPerfilUsuarioVO.getEmpresaVO().getCodigo().equals(empresaVO.getCodigo())) {
				listaUsuarioEmpresaPerfilUsuarioVOs.set(index, usuarioEmpresaPerfilUsuarioVO);
				return;
			}
			index++;
		}
		UsuarioEmpresaPerfilUsuarioVO obj = new UsuarioEmpresaPerfilUsuarioVO();
		obj.getEmpresaVO().setCodigo(empresaVO.getCodigo());
		obj.getEmpresaVO().setDescricao(empresaVO.getDescricao());
		obj.getPerfilUsuarioVO().setCodigo(perfilUsuarioVO.getCodigo());
		obj.getPerfilUsuarioVO().setNome(perfilUsuarioVO.getNome());
		listaUsuarioEmpresaPerfilUsuarioVOs.add(obj);
	}

	@Override
	public void removerEmpresaPerfilUsuario(List<UsuarioEmpresaPerfilUsuarioVO> listaUsuarioEmpresaPerfilUsuarioVOs, UsuarioEmpresaPerfilUsuarioVO obj, UsuarioVO usuarioVO) {
		int index = 0;
		for (UsuarioEmpresaPerfilUsuarioVO usuarioEmpresaPerfilUsuarioExistente : listaUsuarioEmpresaPerfilUsuarioVOs) {
			if (usuarioEmpresaPerfilUsuarioExistente.getEmpresaVO().getCodigo().equals(obj.getEmpresaVO().getCodigo()) && usuarioEmpresaPerfilUsuarioExistente.getPerfilUsuarioVO().getCodigo().equals(obj.getPerfilUsuarioVO().getCodigo())) {
				listaUsuarioEmpresaPerfilUsuarioVOs.remove(index);
				return;
			}
			index++;
		}
	}
	
	
	
}
