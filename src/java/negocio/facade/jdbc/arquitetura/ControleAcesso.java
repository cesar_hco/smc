package negocio.facade.jdbc.arquitetura;

import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.utilitarias.Uteis;
import negocio.interfaces.arquitetura.ControleAcessoInterfaceFacade;

import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

public abstract class ControleAcesso extends SuperFacadeJDBC implements ControleAcessoInterfaceFacade {

	public ControleAcesso() {
	}

	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public static void incluir(String idEntidade, UsuarioVO usuario) throws Exception {
		incluir(idEntidade, true, usuario);
	}

	@Transactional(readOnly = false, isolation = Isolation.READ_COMMITTED, rollbackFor = Exception.class, propagation = Propagation.REQUIRED)
	public static void incluir(String idEntidade, boolean verificarAcesso, UsuarioVO usuario) throws Exception {
	}

	public static void alterar(String idEntidade, UsuarioVO usuario) throws Exception {
		alterar(idEntidade, true, usuario);
	}

	public static void alterar(String idEntidade, boolean verificarAcesso, UsuarioVO usuario) throws Exception {
	}

	public static void excluir(String idEntidade, UsuarioVO usuario) throws Exception {
		excluir(idEntidade, true, usuario);
	}

	public static void excluir(String idEntidade, boolean verificarAcesso, UsuarioVO usuario) throws Exception {
	}

	public static void consultar(String idEntidade, boolean verificarAcesso, UsuarioVO usuario) throws Exception {
	}

	public static UsuarioVO verificarLoginUsuario(String username, String senha, boolean encriptar, int nivelMontarDados) throws Exception {
		StringBuilder sqlStr = getSQLConsultaCompletaUsuario().append(" WHERE ((username = ?) AND (senha = ?))");
		SqlRowSet tabelaResultado = null;
		UsuarioVO user = null;
		try {
			senha = Uteis.encriptar(senha);
			tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sqlStr.toString(), new Object[] { username, senha });
			if (!tabelaResultado.next()) {
				throw new Exception("Usu�rio e/ou senha incorretos!");
			}

			user = montarDadosUsuarioLogado(tabelaResultado);
			return user;
		} catch (Exception e) {
			Uteis.removerObjetoMemoria(user);
			throw e;
		} finally {
			tabelaResultado = null;
			sqlStr = null;
		}
	}
	
	 public static StringBuilder getSQLConsultaCompletaUsuario() {
	        StringBuilder sb = new StringBuilder();
	        sb.append(" select usuario.codigo as \"usuario.codigo\", usuario.username as \"usuario.username\", usuario.senha AS \"usuario.senha\",  ");
	        sb.append(" pessoa.codigo AS \"pessoa.codigo\", pessoa.nome AS \"pessoa.nome\", pessoa.dataNascimento AS \"pessoa.dataNascimento\", pessoa.cpf AS \"pessoa.cpf\", ");
	        sb.append(" pessoa.rg AS \"pessoa.rg\", pessoa.celular AS \"pessoa.celular\", pessoa.telefone1 AS \"pessoa.telefone1\", pessoa.telefone2 AS \"pessoa.telefone2\", ");
	        sb.append(" pessoa.cep AS \"pessoa.cep\", pessoa.endereco AS \"pessoa.endereco\", pessoa.setor AS \"pessoa.setor\", pessoa.email AS \"pessoa.email\", ");
	        sb.append(" cidade.codigo AS \"cidade.codigo\", cidade.nome AS \"cidade.nome\" ");
	        sb.append(" from usuario ");
	        sb.append(" inner join pessoa on pessoa.codigo = usuario.pessoa ");
	        sb.append(" left join cidade on cidade.codigo = pessoa.cidade ");
	        return sb;
	    }
	 
	 public static UsuarioVO montarDadosUsuarioLogado(SqlRowSet dadosSQL) {
		 UsuarioVO obj = new UsuarioVO();
		 obj.setCodigo(dadosSQL.getInt("usuario.codigo"));
		 obj.setUserName(dadosSQL.getString("usuario.username"));
		 obj.setSenha(dadosSQL.getString("usuario.senha"));
		 
		 obj.getPessoaVO().setCodigo(dadosSQL.getInt("pessoa.codigo"));
		 obj.getPessoaVO().setNome(dadosSQL.getString("pessoa.nome"));
		 obj.getPessoaVO().setDataNascimento(dadosSQL.getDate("pessoa.dataNascimento"));
		 obj.getPessoaVO().setCpf(dadosSQL.getString("pessoa.cpf"));
		 obj.getPessoaVO().setRg(dadosSQL.getString("pessoa.rg"));
		 obj.getPessoaVO().setCelular(dadosSQL.getString("pessoa.celular"));
		 obj.getPessoaVO().setTelefone1(dadosSQL.getString("pessoa.telefone1"));
		 obj.getPessoaVO().setTelefone2(dadosSQL.getString("pessoa.telefone2"));
		 obj.getPessoaVO().setCep(dadosSQL.getString("pessoa.cep"));
		 obj.getPessoaVO().setEndereco(dadosSQL.getString("pessoa.endereco"));
		 obj.getPessoaVO().setSetor(dadosSQL.getString("pessoa.setor"));
		 obj.getPessoaVO().setEmail(dadosSQL.getString("pessoa.email"));
		 obj.getPessoaVO().getCidadeVO().setCodigo(dadosSQL.getInt("cidade.codigo"));
		 obj.getPessoaVO().getCidadeVO().setNome(dadosSQL.getString("cidade.nome"));
		 
		 return obj;
	 }
	 

}
