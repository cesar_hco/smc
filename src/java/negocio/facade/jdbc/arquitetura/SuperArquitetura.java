package negocio.facade.jdbc.arquitetura;

import java.io.File;
import java.io.Serializable;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

import negocio.comuns.administrativo.UsuarioVO;

import org.apache.log4j.Logger;

import controle.arquitetura.LoginControle;

public class SuperArquitetura extends FacadeManager implements Serializable {

	private UsuarioVO usuarioVO;
	public String diretorioPastaWeb;

	private static Logger logger;
	public static final long serialVersionUID = 1L;

	/**
	 * M�todo respons�vel por obter o usu�rio logado no sistema.
	 */
	public UsuarioVO getUsuarioLogado() throws Exception {
		setUsuarioVO(getLoginControle().getUsuarioVO());
		return getUsuarioVO();
	}

	public LoginControle getLoginControle() throws Exception {
		LoginControle loginControle = (LoginControle) context().getExternalContext().getSessionMap().get("LoginControle");
		return loginControle;
	}

	protected FacesContext context() {
		return (FacesContext.getCurrentInstance());
	}

	protected String internacionalizar(String mensagem) {
		String propriedade = obterArquivoPropriedades(mensagem);
		ResourceBundle bundle = ResourceBundle.getBundle(propriedade, getLocale(), getCurrentLoader(propriedade));
		try {
			return bundle.getString(mensagem);
		} catch (MissingResourceException e) {
			return mensagem;
		}
	}

	public String obterArquivoPropriedades(String mensagem) {
		if (mensagem.startsWith("msg")) {
			return "propriedades.Mensagens";
		} else if (mensagem.startsWith("enum")) {
			return "propriedades.Enum";
		} else if (mensagem.startsWith("prt")) {
			return "propriedades.Aplicacao";
		} else if (mensagem.startsWith("menu")) {
			return "propriedades.Menu";
		} else if (mensagem.startsWith("btn")) {
			return "propriedades.Botoes";
		} else {
			return "propriedades.Mensagens";
		}
	}

	protected static ClassLoader getCurrentLoader(Object fallbackClass) {
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		if (loader == null) {
			loader = fallbackClass.getClass().getClassLoader();
		}
		return loader;
	}

	protected String getMensagemInternalizacao(String mensagemID) {
		String mensagem = "(" + mensagemID + ") Mensagem n�o localizada nas propriedades de internaliza��o.";
		ResourceBundle bundle = null;
		Locale locale = null;
		String nomeBundle = context().getApplication().getMessageBundle();
		if (nomeBundle != null) {
			locale = context().getViewRoot().getLocale();
			bundle = ResourceBundle.getBundle(nomeBundle, locale, getCurrentLoader(nomeBundle));
			try {
				mensagem = bundle.getString(mensagemID);
				return mensagem;
			} catch (MissingResourceException e) {
				return mensagem;
			}
		}
		return mensagem;
	}

	public Locale getLocale() {
		return context().getViewRoot().getLocale();
	}

	/**
	 * @return the usuarioVO
	 */
	public UsuarioVO getUsuarioVO() {
		if (usuarioVO == null) {
			usuarioVO = new UsuarioVO();
		}
		return usuarioVO;
	}

	/**
	 * @param usuarioVO
	 *            the usuarioVO to set
	 */
	public void setUsuarioVO(UsuarioVO usuarioVO) {
		this.usuarioVO = usuarioVO;
	}

	/**
	 * @return the logger
	 */
	public static Logger getLogger() {
		if (logger == null) {
			logger = Logger.getLogger(SuperArquitetura.class);
		}
		return logger;
	}

	/**
	 * @param aLogger
	 *            the logger to set
	 */
	public static void setLogger(Logger aLogger) {
		logger = aLogger;
	}
	
	public String getCaminhoPastaWeb() {
        if (diretorioPastaWeb == null || diretorioPastaWeb.equals("")) {
            if(context() == null){
                diretorioPastaWeb = SuperArquitetura.class.getResource("").getFile();
                if(diretorioPastaWeb.contains("WEB-INF")){
                    diretorioPastaWeb = diretorioPastaWeb.substring(0, diretorioPastaWeb.indexOf("WEB-INF"));
                }
            }else{
                ServletContext servletContext = (ServletContext) this.context().getExternalContext().getContext();
                diretorioPastaWeb = servletContext.getRealPath("");
            }
            
        }
        return diretorioPastaWeb;
    }
	
	public String getCaminhoClassesAplicacao() throws Exception {
        String caminhoBaseAplicacaoPrm = getCaminhoPastaWeb() + File.separator + "WEB-INF" + File.separator + "classes";
        return caminhoBaseAplicacaoPrm;
    }
}
