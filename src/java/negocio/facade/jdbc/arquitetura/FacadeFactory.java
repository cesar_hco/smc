/**
 * 
 */
package negocio.facade.jdbc.arquitetura;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import negocio.interfaces.administrativo.ConveniointerfaceFacade;
import negocio.interfaces.administrativo.EmpresaInterfaceFacade;
import negocio.interfaces.administrativo.FuncionarioInterfaceFacade;
import negocio.interfaces.administrativo.PacienteInterfaceFacade;
import negocio.interfaces.administrativo.PerfilUsuarioInterfaceFacade;
import negocio.interfaces.administrativo.PermissaoUsuarioMenuInterfaceFacade;
import negocio.interfaces.administrativo.PermissaoUsuarioMenuSubModuloInterfaceFacade;
import negocio.interfaces.administrativo.UsuarioEmpresaPerfilUsuarioInterfaceFacade;
import negocio.interfaces.agenda.FolgaFuncionarioInterfaceFacade;
import negocio.interfaces.agenda.HorasClientesInterfaceFacade;
import negocio.interfaces.agenda.HorasInterfaceFacade;
import negocio.interfaces.agenda.ValorHorasInterfaceFacade;
import negocio.interfaces.basico.CidadeInterfaceFacade;
import negocio.interfaces.basico.ClienteInterfaceFacade;
import negocio.interfaces.basico.EstadoInterfaceFacade;
import negocio.interfaces.basico.FornecedorInterfaceFacade;
import negocio.interfaces.basico.PaizInterfaceFacade;
import negocio.interfaces.basico.PessoaInterfaceFacade;
import negocio.interfaces.basico.UsuarioInterfaceFacade;
import negocio.interfaces.financeiro.CategoriaDespesaInterfaceFacade;
import negocio.interfaces.financeiro.ChequeInterfaceFacade;
import negocio.interfaces.financeiro.ContaCorrenteInterfaceFacade;
import negocio.interfaces.financeiro.ContaPagarInterfaceFacade;
import negocio.interfaces.financeiro.ContaPagarPagamentoInterfaceFacade;
import negocio.interfaces.financeiro.ContaReceberInterfaceFacade;
import negocio.interfaces.financeiro.ExtratoContaCorrenteInterfaceFacade;
import negocio.interfaces.financeiro.FormaPagamentoInterfaceFacade;
import negocio.interfaces.financeiro.FormaPagamentoPagamentoInterfaceFacade;
import negocio.interfaces.financeiro.MovimentacaoCaixaInterfaceFacade;
import negocio.interfaces.financeiro.MovimentacaoCaixaItemInterfaceFacade;
import negocio.interfaces.financeiro.PagamentoInterfaceFacade;
import relatorio.negocio.interfaces.financeiro.ContaPagarRelInterfaceFacade;

/**
 * @author Carlos Eug�nio
 *
 */
@Service
@Scope("singleton")
@Lazy
public class FacadeFactory  implements Serializable {

	private static final long serialVersionUID = 9184862930017801890L;
	
	@Autowired
	private ClienteInterfaceFacade clienteFacade;
	
	@Autowired
	private PessoaInterfaceFacade pessoaFacade;
	@Autowired
	private CidadeInterfaceFacade cidadeFacade;
	@Autowired
	private EstadoInterfaceFacade estadoFacade;
	@Autowired
	private PaizInterfaceFacade paizFacade;
	@Autowired
	private ContaReceberInterfaceFacade contaReceberFacade;
	@Autowired
	private ContaCorrenteInterfaceFacade contaCorrenteFacade;
	@Autowired
	private FornecedorInterfaceFacade fornecedorFacade;
	@Autowired
	private EmpresaInterfaceFacade empresaFacade;
	@Autowired
	private PerfilUsuarioInterfaceFacade perfilUsuarioFacade;
	@Autowired
	private ContaPagarInterfaceFacade contaPagarFacade;
	@Autowired
	private CategoriaDespesaInterfaceFacade categoriaDespesaFacade;
	@Autowired
	private PagamentoInterfaceFacade pagamentoFacade;
	@Autowired
	private FormaPagamentoInterfaceFacade formaPagamentoFacade;
	@Autowired
	private FormaPagamentoPagamentoInterfaceFacade formaPagamentoPagamentoFacade;
	@Autowired
	private MovimentacaoCaixaInterfaceFacade movimentacaoCaixaFacade;
	@Autowired
	private MovimentacaoCaixaItemInterfaceFacade movimentacaoCaixaItemFacade;
	@Autowired
	private ExtratoContaCorrenteInterfaceFacade extratoContaCorrenteFacade;
	@Autowired
	private ChequeInterfaceFacade chequeFacade;
	@Autowired
	private ContaPagarPagamentoInterfaceFacade contaPagarPagamentoFacade;
	@Autowired
	private PermissaoUsuarioMenuInterfaceFacade permissaoUsuarioMenuFacade;
	@Autowired
	private PermissaoUsuarioMenuSubModuloInterfaceFacade permissaoUsuarioMenuSubModuloFacade;
	@Autowired
	private UsuarioInterfaceFacade usuarioFacade;
	@Autowired
	private FuncionarioInterfaceFacade funcionarioFacade;
	@Autowired
	private UsuarioEmpresaPerfilUsuarioInterfaceFacade usuarioEmpresaPerfilUsuarioFacade;
	@Autowired
	private ContaPagarRelInterfaceFacade contaPagarRelFacade;
/*	@Autowired
	private FolgaFuncionarioInterfaceFacade folgaFuncionarioInterfaceFacade;
	@Autowired
	private HorasClientesInterfaceFacade horasClientesInterfaceFacade;
	@Autowired
	private HorasInterfaceFacade horasInterfaceFacade;
*/	@Autowired
	private ValorHorasInterfaceFacade valorHorasFacade;
	@Autowired
	private PacienteInterfaceFacade PacienteFacade;
	@Autowired
	private ConveniointerfaceFacade ConveniointerfaceFacade;

	public PessoaInterfaceFacade getPessoaFacade() {
		return pessoaFacade;
	}

	public void setPessoaFacade(PessoaInterfaceFacade pessoaFacade) {
		this.pessoaFacade = pessoaFacade;
	}

	public CidadeInterfaceFacade getCidadeFacade() {
		return cidadeFacade;
	}

	public void setCidadeFacade(CidadeInterfaceFacade cidadeFacade) {
		this.cidadeFacade = cidadeFacade;
	}

	public EstadoInterfaceFacade getEstadoFacade() {
		return estadoFacade;
	}

	public void setEstadoFacade(EstadoInterfaceFacade estadoFacade) {
		this.estadoFacade = estadoFacade;
	}

	public PaizInterfaceFacade getPaizFacade() {
		return paizFacade;
	}

	public void setPaizFacade(PaizInterfaceFacade paizFacade) {
		this.paizFacade = paizFacade;
	}

	public ClienteInterfaceFacade getClienteFacade() {
		return clienteFacade;
	}

	public void setClienteFacade(ClienteInterfaceFacade clienteFacade) {
		this.clienteFacade = clienteFacade;
	}

	public ContaReceberInterfaceFacade getContaReceberFacade() {
		return contaReceberFacade;
	}

	public void setContaReceberFacade(ContaReceberInterfaceFacade contaReceberFacade) {
		this.contaReceberFacade = contaReceberFacade;
	}

	public ContaCorrenteInterfaceFacade getContaCorrenteFacade() {
		return contaCorrenteFacade;
	}

	public void setContaCorrenteFacade(ContaCorrenteInterfaceFacade contaCorrenteFacade) {
		this.contaCorrenteFacade = contaCorrenteFacade;
	}

	public FornecedorInterfaceFacade getFornecedorFacade() {
		return fornecedorFacade;
	}

	public void setFornecedorFacade(FornecedorInterfaceFacade fornecedorFacade) {
		this.fornecedorFacade = fornecedorFacade;
	}

	public EmpresaInterfaceFacade getEmpresaFacade() {
		return empresaFacade;
	}

	public void setEmpresaFacade(EmpresaInterfaceFacade empresaFacade) {
		this.empresaFacade = empresaFacade;
	}

	public PerfilUsuarioInterfaceFacade getPerfilUsuarioFacade() {
		return perfilUsuarioFacade;
	}

	public void setPerfilUsuarioFacade(PerfilUsuarioInterfaceFacade perfilUsuarioFacade) {
		this.perfilUsuarioFacade = perfilUsuarioFacade;
	}

	public ContaPagarInterfaceFacade getContaPagarFacade() {
		return contaPagarFacade;
	}

	public void setContaPagarFacade(ContaPagarInterfaceFacade contaPagarFacade) {
		this.contaPagarFacade = contaPagarFacade;
	}

	public CategoriaDespesaInterfaceFacade getCategoriaDespesaFacade() {
		return categoriaDespesaFacade;
	}

	public void setCategoriaDespesaFacade(CategoriaDespesaInterfaceFacade categoriaDespesaFacade) {
		this.categoriaDespesaFacade = categoriaDespesaFacade;
	}

	public PagamentoInterfaceFacade getPagamentoFacade() {
		return pagamentoFacade;
	}

	public void setPagamentoFacade(PagamentoInterfaceFacade pagamentoFacade) {
		this.pagamentoFacade = pagamentoFacade;
	}

	public FormaPagamentoInterfaceFacade getFormaPagamentoFacade() {
		return formaPagamentoFacade;
	}

	public void setFormaPagamentoFacade(FormaPagamentoInterfaceFacade formaPagamentoFacade) {
		this.formaPagamentoFacade = formaPagamentoFacade;
	}

	public FormaPagamentoPagamentoInterfaceFacade getFormaPagamentoPagamentoFacade() {
		return formaPagamentoPagamentoFacade;
	}

	public void setFormaPagamentoPagamentoFacade(FormaPagamentoPagamentoInterfaceFacade formaPagamentoPagamentoFacade) {
		this.formaPagamentoPagamentoFacade = formaPagamentoPagamentoFacade;
	}

	public MovimentacaoCaixaInterfaceFacade getMovimentacaoCaixaFacade() {
		return movimentacaoCaixaFacade;
	}

	public MovimentacaoCaixaItemInterfaceFacade getMovimentacaoCaixaItemFacade() {
		return movimentacaoCaixaItemFacade;
	}

	public void setMovimentacaoCaixaFacade(MovimentacaoCaixaInterfaceFacade movimentacaoCaixaFacade) {
		this.movimentacaoCaixaFacade = movimentacaoCaixaFacade;
	}

	public void setMovimentacaoCaixaItemFacade(MovimentacaoCaixaItemInterfaceFacade movimentacaoCaixaItemFacade) {
		this.movimentacaoCaixaItemFacade = movimentacaoCaixaItemFacade;
	}

	public ExtratoContaCorrenteInterfaceFacade getExtratoContaCorrenteFacade() {
		return extratoContaCorrenteFacade;
	}

	public void setExtratoContaCorrenteFacade(ExtratoContaCorrenteInterfaceFacade extratoContaCorrenteFacade) {
		this.extratoContaCorrenteFacade = extratoContaCorrenteFacade;
	}

	public ChequeInterfaceFacade getChequeFacade() {
		return chequeFacade;
	}

	public void setChequeFacade(ChequeInterfaceFacade chequeFacade) {
		this.chequeFacade = chequeFacade;
	}

	public ContaPagarPagamentoInterfaceFacade getContaPagarPagamentoFacade() {
		return contaPagarPagamentoFacade;
	}

	public void setContaPagarPagamentoFacade(ContaPagarPagamentoInterfaceFacade contaPagarPagamentoFacade) {
		this.contaPagarPagamentoFacade = contaPagarPagamentoFacade;
	}

	public PermissaoUsuarioMenuInterfaceFacade getPermissaoUsuarioMenuFacade() {
		return permissaoUsuarioMenuFacade;
	}

	public PermissaoUsuarioMenuSubModuloInterfaceFacade getPermissaoUsuarioMenuSubModuloFacade() {
		return permissaoUsuarioMenuSubModuloFacade;
	}

	public void setPermissaoUsuarioMenuFacade(PermissaoUsuarioMenuInterfaceFacade permissaoUsuarioMenuFacade) {
		this.permissaoUsuarioMenuFacade = permissaoUsuarioMenuFacade;
	}

	public void setPermissaoUsuarioMenuSubModuloFacade(PermissaoUsuarioMenuSubModuloInterfaceFacade permissaoUsuarioMenuSubModuloFacade) {
		this.permissaoUsuarioMenuSubModuloFacade = permissaoUsuarioMenuSubModuloFacade;
	}

	public UsuarioInterfaceFacade getUsuarioFacade() {
		return usuarioFacade;
	}

	public void setUsuarioFacade(UsuarioInterfaceFacade usuarioFacade) {
		this.usuarioFacade = usuarioFacade;
	}

	public FuncionarioInterfaceFacade getFuncionarioFacade() {
		return funcionarioFacade;
	}

	public void setFuncionarioFacade(FuncionarioInterfaceFacade funcionarioFacade) {
		this.funcionarioFacade = funcionarioFacade;
	}

	public UsuarioEmpresaPerfilUsuarioInterfaceFacade getUsuarioEmpresaPerfilUsuarioFacade() {
		return usuarioEmpresaPerfilUsuarioFacade;
	}

	public void setUsuarioEmpresaPerfilUsuarioFacade(UsuarioEmpresaPerfilUsuarioInterfaceFacade usuarioEmpresaPerfilUsuarioFacade) {
		this.usuarioEmpresaPerfilUsuarioFacade = usuarioEmpresaPerfilUsuarioFacade;
	}

	public ContaPagarRelInterfaceFacade getContaPagarRelFacade() {
		return contaPagarRelFacade;
	}

	public void setContaPagarRelFacade(ContaPagarRelInterfaceFacade contaPagarRelFacade) {
		this.contaPagarRelFacade = contaPagarRelFacade;
	}

/*	public FolgaFuncionarioInterfaceFacade getFolgaFuncionarioInterfaceFacade() {
		return folgaFuncionarioInterfaceFacade;
	}

	public void setFolgaFuncionarioInterfaceFacade(FolgaFuncionarioInterfaceFacade folgaFuncionarioInterfaceFacade) {
		this.folgaFuncionarioInterfaceFacade = folgaFuncionarioInterfaceFacade;
	}

	public HorasClientesInterfaceFacade getHorasClientesInterfaceFacade() {
		return horasClientesInterfaceFacade;
	}

	public void setHorasClientesInterfaceFacade(HorasClientesInterfaceFacade horasClientesInterfaceFacade) {
		this.horasClientesInterfaceFacade = horasClientesInterfaceFacade;
	}

	public HorasInterfaceFacade getHorasInterfaceFacade() {
		return horasInterfaceFacade;
	}

	public void setHorasInterfaceFacade(HorasInterfaceFacade horasInterfaceFacade) {
		this.horasInterfaceFacade = horasInterfaceFacade;
	}

*/	public ValorHorasInterfaceFacade getvalorHorasFacade() {
		return valorHorasFacade;
	}

	public void setValorHorasInterfaceFacade(ValorHorasInterfaceFacade valorHorasFacade) {
		this.valorHorasFacade = valorHorasFacade;
	}

	public PacienteInterfaceFacade getPacienteFacade() {
		return PacienteFacade;
	}

	public void setPacienteFacade(PacienteInterfaceFacade pacienteFacade) {
		this.PacienteFacade = pacienteFacade;
	}

	public ConveniointerfaceFacade getConveniointerfaceFacade() {
		return ConveniointerfaceFacade;
	}

	public void setConveniointerfaceFacade(ConveniointerfaceFacade conveniointerfaceFacade) {
		this.ConveniointerfaceFacade = conveniointerfaceFacade;
	}
	
	
	
}
