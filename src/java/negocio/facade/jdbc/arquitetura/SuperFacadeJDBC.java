package negocio.facade.jdbc.arquitetura;

import java.io.File;
import java.io.Serializable;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * Respons�vel por implementar caracter�sticas comuns relativas a conex�o com o banco de dados.
 */
public abstract class SuperFacadeJDBC  implements Serializable {

    public String diretorioPastaWeb;
    private static FacadeFactory facadeFactory;
    protected final Lock bloqueio = new ReentrantLock();
    public static final long serialVersionUID = 1L;

    @Autowired
    public void setFacadeFactory(FacadeFactory facadeFactory) {
        SuperFacadeJDBC.facadeFactory = facadeFactory;
    }

    public static FacadeFactory getFacadeFactory() {
        return facadeFactory;
    }

    private static Conexao conexao;

    public static Conexao getConexao() {
        return conexao;
    }

    @Autowired
    public void setConexao(Conexao conexao) {
        SuperFacadeJDBC.conexao = conexao;
    }

    public SuperFacadeJDBC() {
    }

    public static void incluir(String idEntidade) throws Exception {
    }

    public static void alterar(String idEntidade) throws Exception {
        // ControleAcesso.alterar(idEntidade);
    }

    public static void excluir(String idEntidade) throws Exception {
    }

    public static void consultar(String idEntidade) throws Exception {
    }

    public static void emitirRelatorio(String idEntidade) throws Exception {
    }

    public static void verificarPermissaoUsuarioFuncionalidade(String funcionalidade) throws Exception {
    }

    public static String getIdEntidade() {
        return "Entidade";
    }

    protected FacesContext context() {
        return (FacesContext.getCurrentInstance());
    }

    public String getCaminhoPastaWeb() {
        if (diretorioPastaWeb == null || diretorioPastaWeb.equals("")) {
            ServletContext servletContext = (ServletContext) this.context().getExternalContext().getContext();
            diretorioPastaWeb = servletContext.getRealPath("");
        }
        return diretorioPastaWeb;
    }

    public String getCaminhoClassesAplicacao() throws Exception {
        String caminhoBaseAplicacaoPrm = getCaminhoPastaWeb() + File.separator + "WEB-INF" + File.separator + "classes";
        return caminhoBaseAplicacaoPrm;
    }
    
    
}
