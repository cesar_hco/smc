package negocio.comuns.agenda.enumeradores;

import negocio.comuns.utilitarias.UteisJSF;

public enum SituacaoHorasliente {

	ABERTO, CONCLUIDO, CANCELADO;
	
	public String getValorApresentar(){        
        return UteisJSF.internacionalizar("enum_SituacaoHorasCliente_"+this.name());
    }
	
}
