package negocio.comuns.agenda.enumeradores;

import negocio.comuns.utilitarias.UteisJSF;

public enum SituacaoAgendaEnum {

	AGENDADO , CANCELADO , CONCLUIDO , NAO_ATENDIDO , NAO_ATENDIDO_COBRADO;
	
	public String getValorApresentar(){        
        return UteisJSF.internacionalizar("enum_SituacaoAgendaEnum_"+this.name());
    }
	
}
