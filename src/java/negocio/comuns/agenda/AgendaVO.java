package negocio.comuns.agenda;

import java.io.Serializable;
import java.util.Date;

import negocio.comuns.administrativo.ConvenioVO;
import negocio.comuns.administrativo.FuncionarioVO;
import negocio.comuns.administrativo.PacienteVO;
import negocio.comuns.administrativo.ProcedimentoVO;
import negocio.comuns.agenda.enumeradores.SituacaoAgendaEnum;
import negocio.comuns.arquitetura.SuperVO;
import negocio.comuns.utilitarias.UteisJSF;

public class AgendaVO extends SuperVO implements Serializable{

	private Integer codigo;
	private FuncionarioVO funcionario;
	private  PacienteVO paciente;
	private ConvenioVO convenio;
	private String numeroLiberacaoConvenio;
	private SituacaoAgendaEnum situacao;
	private String observacao;
	private ProcedimentoVO procedimento;
	private Date data;
	private String horarioIncial;
	private String horarioFinal;
	
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public FuncionarioVO getFuncionario() {
		return funcionario;
	}
	public void setFuncionario(FuncionarioVO funcionario) {
		this.funcionario = funcionario;
	}
	public PacienteVO getPaciente() {
		return paciente;
	}
	public void setPaciente(PacienteVO paciente) {
		this.paciente = paciente;
	}
	public ConvenioVO getConvenio() {
		return convenio;
	}
	public void setConvenio(ConvenioVO convenio) {
		this.convenio = convenio;
	}
	public String getNumeroLiberacaoConvenio() {
		return numeroLiberacaoConvenio;
	}
	public void setNumeroLiberacaoConvenio(String numeroLiberacaoConvenio) {
		this.numeroLiberacaoConvenio = numeroLiberacaoConvenio;
	}
	public SituacaoAgendaEnum getSituacao() {
		return situacao;
	}
	public void setSituacao(SituacaoAgendaEnum situacao) {
		this.situacao = situacao;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public ProcedimentoVO getProcedimento() {
		return procedimento;
	}
	public void setProcedimento(ProcedimentoVO procedimento) {
		this.procedimento = procedimento;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public String getHorarioIncial() {
		return horarioIncial;
	}
	public void setHorarioIncial(String horarioIncial) {
		this.horarioIncial = horarioIncial;
	}
	public String getHorarioFinal() {
		return horarioFinal;
	}
	public void setHorarioFinal(String horarioFinal) {
		this.horarioFinal = horarioFinal;
	}
	
	
	
	
	
	
	
}
