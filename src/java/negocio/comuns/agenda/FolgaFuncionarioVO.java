package negocio.comuns.agenda;

import java.io.Serializable;
import java.util.Date;

import negocio.comuns.administrativo.FuncionarioVO;
import negocio.comuns.arquitetura.SuperVO;

public class FolgaFuncionarioVO extends SuperVO implements Serializable {

	/**
	 * @author Cesar Henriques
	 */
	private static final long serialVersionUID = 1L;

	private Integer codigo;
	private Date folgaFuncionario;
	private FuncionarioVO funcionarioVO;
	private String descricaoFolga;
	private String observacaoFolga;
	
	
	public FolgaFuncionarioVO(){
		super();
	}


	public Integer getCodigo() {
		if (codigo == null) {
			codigo = 0;
		}
		return codigo;
	}


	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}


	public Date getFolgaFuncionario() {
		return folgaFuncionario;
	}


	public void setFolgaFuncionario(Date folgaFuncionario) {
		this.folgaFuncionario = folgaFuncionario;
	}


	public FuncionarioVO getFuncionarioVO() {
		if (funcionarioVO == null) {
			funcionarioVO = new FuncionarioVO();
		}
		return funcionarioVO;
	}


	public void setFuncionarioVO(FuncionarioVO funcionarioVO) {
		this.funcionarioVO = funcionarioVO;
	}


	public String getDescricaoFolga() {
		
		return descricaoFolga;
	}


	public void setDescricaoFolga(String descricaoFolga) {
		this.descricaoFolga = descricaoFolga;
	}


	public String getObservacaoFolga() {
		if (observacaoFolga == null) {
			observacaoFolga = " ";
		}
		return observacaoFolga;
	}


	public void setObservacaoFolga(String observacaoFolga) {
		this.observacaoFolga = observacaoFolga;
	}
	
	
	
}
