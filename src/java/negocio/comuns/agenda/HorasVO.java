package negocio.comuns.agenda;

import java.io.Serializable;

import negocio.comuns.arquitetura.SuperVO;

public class HorasVO extends SuperVO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private Integer codigo;
	private Double horas;
	
	
	public HorasVO() {
		super();
	}
	
	public Integer getCodigo() {
		if (codigo == null) {
			codigo = 0;
		}
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public Double getHoras() {
		return horas;
	}
	public void setHoras(Double horas) {
		this.horas = horas;
	}
	
	
	
}
