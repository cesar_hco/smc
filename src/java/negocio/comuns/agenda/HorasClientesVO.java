package negocio.comuns.agenda;

import java.io.Serializable;

import negocio.comuns.agenda.enumeradores.SituacaoHorasliente;
import negocio.comuns.arquitetura.SuperVO;
import negocio.comuns.basico.ClienteVO;
import negocio.comuns.financeiro.enumeradores.SituacaoContaReceberEnum;

public class HorasClientesVO extends SuperVO implements Serializable {
 
	/**
	 * @author cesar henrique
	 */
	private static final long serialVersionUID = 1L;
	private Integer codigo;
	private ClienteVO clienteVO;
	private HorasVO horasVO;
	private double saldoHorasCliente;
	private String descricaoQuantidadeHoras;
	private String observacao;
	private SituacaoHorasliente situacaoHorasCliente;
	
	
	public Integer getCodigo() {
		if (codigo == null) {
			codigo = 0;
		}
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public ClienteVO getClienteVO() {
		return clienteVO;
	}
	public void setClienteVO(ClienteVO clienteVO) {
		this.clienteVO = clienteVO;
	}
	public HorasVO getHorasVO() {
		if (horasVO == null) {
			horasVO = new HorasVO();
		}
		return horasVO;
	}
	public void setHorasVO(HorasVO horasVO) {
		this.horasVO = horasVO;
	}
	public double getSaldoHorasCliente() {
		return saldoHorasCliente;
	}
	public void setSaldoHorasCliente(double saldoHorasCliente) {
		this.saldoHorasCliente = saldoHorasCliente;
	}
	public String getDescricaoQuantidadeHoras() {
		return descricaoQuantidadeHoras;
	}
	public void setDescricaoQuantidadeHoras(String descricaoQuantidadeHoras) {
		this.descricaoQuantidadeHoras = descricaoQuantidadeHoras;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public SituacaoHorasliente getSituacaoHorasCliente() {
		if (situacaoHorasCliente == null) {
			situacaoHorasCliente = SituacaoHorasliente.ABERTO;
		}
		return situacaoHorasCliente;
	}
	public void setSituacaoHorasCliente(SituacaoHorasliente situacaoHorasCliente) {
		this.situacaoHorasCliente = situacaoHorasCliente;
	}
	
	
	
	
	
}
