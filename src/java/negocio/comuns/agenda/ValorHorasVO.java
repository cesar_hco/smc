package negocio.comuns.agenda;

import java.io.Serializable;
import java.util.Date;

import negocio.comuns.arquitetura.SuperVO;

public class ValorHorasVO extends SuperVO implements Serializable {

	/**
	 *  @author Cesar Henrique
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer codigo;
    private Double valorHora;
    private Date dataAtualizacao;
    private boolean ativado;
    
    public ValorHorasVO(){
    	super();
    }
	
    public Integer getCodigo() {
		if (codigo == null) {
			codigo = 0;
		}
    	return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public Double getValorHora() {
		return valorHora;
	}
	public void setValorHora(Double valorHora) {
		this.valorHora = valorHora;
	}
	public Date getDataAtualizacao() {
		return dataAtualizacao;
	}
	public void setDataAtualizacao(Date dataAtualizacao) {
		this.dataAtualizacao = dataAtualizacao;
	}
	public boolean isAtivado() {
		return ativado;
	}
	public void setAtivado(boolean ativado) {
		this.ativado = ativado;
	}
    
    
    
    
	
}
