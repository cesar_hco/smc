package negocio.comuns.arquitetura;

/**
 * SuperClasse padr�o para classes do tipo VO - Value Object (Design Pattern: Transfer Object).
 * Classe somente com atributos representando dados da entidade de neg�cio correspondente.
 * Implementa um controle simples para indicar se um objeto � novo (ainda n�o gravado no banco de dados)
 * ou, caso contr�rio, o objeto em quest�o j� existe no banco de dados.
 * Permite mapear se um objeto deve ser inclu�do ou alterado no BD.
 */
import java.io.Serializable;
import java.util.Date;
import java.util.Observable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

@XmlAccessorType(XmlAccessType.NONE)
public class SuperVO extends Observable implements Cloneable, Serializable {

	protected Boolean novoObj;
	protected Boolean validarDados;
	/** Atributo respons�vel por manter o estado cosistente do objeto */
	private Date updated;
	private Boolean controlarConcorrencia;

	/** Creates a new instance of SuperDAO */
	public SuperVO() {
		setNovoObj(Boolean.TRUE);
		setValidarDados(Boolean.TRUE);
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	/**
	 * Retorna um status indicando se o objeto � novo (ainda n�o foi gravado no
	 * BD) ou n�o.
	 * 
	 * @return boolean True indica que o objeto � novo e portanto ainda n�o foi
	 *         gravado no BD. False indica que o objeto j� existe no BD e deve
	 *         ser alterado.
	 */
	public Boolean getNovoObj() {
		return novoObj;
	}

	public Boolean isNovoObj() {
		return novoObj;
	}

	/**
	 * Define um status indicando se o objeto � novo (ainda n�o foi gravado no
	 * BD) ou n�o.
	 * 
	 * @param novoObj
	 *            True indica que o objeto � novo e portanto ainda n�o foi
	 *            gravado no BD. False indica que o objeto j� existe no BD e
	 *            deve ser alterado.
	 */
	public void setNovoObj(Boolean novoObj) {
		this.novoObj = novoObj;
	}

	public Boolean isValidarDados() {
		return validarDados;
	}

	public void setValidarDados(Boolean validarDados) {
		this.validarDados = validarDados;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
		// setChanged();
	}

	public void setChanged() {
		// super.setChanged();
	}

	public Date getUpdated() {
		if (updated == null) {
			updated = new Date();
		}
		return updated;
	}

	public void setControlarConcorrencia(Boolean controlarConcorrencia) {
		this.controlarConcorrencia = controlarConcorrencia;
	}

	public Boolean getControlarConcorrencia() {
		if (controlarConcorrencia == null) {
			controlarConcorrencia = Boolean.FALSE;
		}
		return controlarConcorrencia;
	}

}
