/**
 * 
 */
package negocio.comuns.arquitetura.enumeradores;

/**
 * @author Carlos Eug�nio
 *
 */
public enum MensagemAlertaEnum {
	
	SUCESSO, 
	ERRO,
	ATENCAO;

}
