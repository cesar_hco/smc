package negocio.comuns.arquitetura.enumeradores;

public enum NivelMontarDadosEnum {
	
	COMBOBOX,
	BASICO,
	SUBORDINADA,
	TELA_CONSULTA,
	COMPLETO,
	TODOS,

}
