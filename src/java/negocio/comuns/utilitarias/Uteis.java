package negocio.comuns.utilitarias;

import java.awt.Color;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.Collator;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.Normalizer;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.faces.context.FacesContext;
import javax.mail.AuthenticationFailedException;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Part;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import javax.servlet.ServletContext;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;

import sun.misc.BASE64Encoder;

public class Uteis implements Serializable {

	public static final int BLOQUEIO_VERIFICAR = 0;
	public static final int BLOQUEIO_GERAR = 1;
	private static String loginMail;
	private static String senhaMail;
	public static final int CONSULTAR = 0;
	public static final int INCLUIR = 1;
	public static final int ALTERAR = 2;
	public static final int EXCLUIR = 3;
	public static final int PROXIMO = 5;
	public static final int ANTERIOR = 6;
	public static final int INICIALIZAR = 7;
	public static final int EXISTENTE = 8;
	public static final int NOVO = 9;
	public static final int ULTIMO = 10;
	public static final int PRIMEIRO = 11;
	public static final int EMITIRRELATORIO = 12;
	public static final int GRAVAR = 13;
	public static final String PERMISSAOTOTAL = "(" + NOVO + ")" + "(" + INCLUIR + ")" + "(" + ALTERAR + ")" + "(" + EXCLUIR + ")" + "(" + CONSULTAR + ")" + "(" + EMITIRRELATORIO + ")";
	public static final String PERMISSAOINCLUIR = "(" + NOVO + ")" + "(" + INCLUIR + ")";
	public static final String PERMISSAOCONSULTAR = "(" + CONSULTAR + ")" + "(" + EMITIRRELATORIO + ")" + "(" + PROXIMO + ")" + "(" + ANTERIOR + ")";
	public static final String PERMISSAOALTERAR = "(" + ALTERAR + ")";
	public static final String PERMISSAOEXCLUIR = "(" + EXCLUIR + ")";
	public static final int TAMANHOLISTA = 10;
	public static final int NIVELMONTARDADOS_TODOS = 0;
	public static final int NIVELMONTARDADOS_DADOSBASICOS = 1;
	public static final int NIVELMONTARDADOS_PROCESSAMENTO = 6;
	public static final int NIVELMONTARDADOS_DADOSMINIMOS = 3;
	public static final int NIVELMONTARDADOS_DADOSCONSULTA = 4;
	public static final int NIVELMONTARDADOS_DADOSCONSULTARTODOS = 5;
	public static final int NIVELMONTARDADOS_DADOSENTIDADESUBORDINADAS = 2;
	public static final int NIVELMONTARDADOS_DADOSLOGIN = 8;
	public static final int NIVELMONTARDADOS_COMBOBOX = 7;
	public static final int NIVELMONTARDADOS_JOBATRASODEVOLUCAO = 9;
	public static final char A_AGUDO = UteisJSF.internacionalizar("prt_A_AGUDO").charAt(0);
	public static final char A_AGUDOMAIUSCULO = UteisJSF.internacionalizar("prt_A_AGUDOMAIUSCULO").charAt(0);
	public static final char A_CRASE = UteisJSF.internacionalizar("prt_A_CRASE").charAt(0);
	public static final char A_CIRCUNFLEXO = UteisJSF.internacionalizar("prt_A_CIRCUNFLEXO").charAt(0);
	public static final char A_TIO = UteisJSF.internacionalizar("prt_A_TIO").charAt(0);
	public static final char E_AGUDO = UteisJSF.internacionalizar("prt_E_AGUDO").charAt(0);
	public static final char E_CIRCUNFLEXO = UteisJSF.internacionalizar("prt_E_CIRCUNFLEXO").charAt(0);
	public static final char I_AGUDO = UteisJSF.internacionalizar("prt_I_AGUDO").charAt(0);
	public static final char O_AGUDO = UteisJSF.internacionalizar("prt_O_AGUDO").charAt(0);
	public static final char O_TIO = UteisJSF.internacionalizar("prt_O_TIO").charAt(0);
	public static final char O_CRASE = UteisJSF.internacionalizar("prt_O_CRASE").charAt(0);
	public static final char U_AGUDO = UteisJSF.internacionalizar("prt_U_AGUDO").charAt(0);
	public static final char U_TREMA = UteisJSF.internacionalizar("prt_U_TREMA").charAt(0);
	public static final char C_CEDILHA = UteisJSF.internacionalizar("prt_C_CEDILHA").charAt(0);
	public static final boolean realizarUpperCaseDadosAntesPersistencia = false;
	private List<File> listaImagem = new ArrayList<File>();
	private Map<String, File> mapaAnexo = new HashMap<String, File>();
	/** Index of the first accent character **/
	private static final int MIN = 192;
	/** Index of the last accent character **/
	private static final int MAX = 255;
	/** used to save the link between with or without accents **/
	@SuppressWarnings("UseOfObsoleteCollectionType")
	private static final Vector map = initMap();
	/** Usado na nova arquitetura **/
	public static final String ERRO = "ERRO";
	public static final String SUCESSO = "SUCESSO";
	public static final String ALERTA = "ALERTA";
	public static final HashMap<String, String> ARQUIVOS_CONTROLE_COBRANCA = new HashMap<String, String>(0);
	private static final long ONE_DAY = 24 * 60 * 60 * 1000;

	/** Creates a new instance of CfgPadrao */
	public Uteis() {
	}

	public static Color gerarCorAleatoria(Color mix) {
		Color color = null;
		Random random = new Random();
		int red = random.nextInt(256);
		int green = random.nextInt(256);
		int blue = random.nextInt(256);
		try {

			int red2 = random.nextInt(256);
			int green2 = random.nextInt(256);
			int blue2 = random.nextInt(256);
			// mix the color
			if (mix != null) {
				red = (red + green2) / 4 + (mix.getBlue() / 2) + random.nextInt(256);
				green = (green + blue2) / 4 + (mix.getRed() / 2) + random.nextInt(256);
				blue = (blue + red2) / 4 + (mix.getGreen() / 2) + random.nextInt(256);
			}
			if (red >= 256 || red < 1) {
				red = random.nextInt(256);
			}
			if (blue >= 256 || blue < 1) {
				blue = random.nextInt(256);
			}
			if (green >= 256 || green < 1) {
				green = random.nextInt(256);
			}
			color = new Color(red, green, blue);
		} catch (Exception e) {
		} finally {
			return color;
		}

	}

	@SuppressWarnings({ "UseOfObsoleteCollectionType", "RedundantStringConstructorCall" })
	private static Vector initMap() {
		Vector result = new Vector();
		String car = null;

		car = new String("A"); //$NON-NLS-1$
		result.add(car); /* '\u00C0' alt-0192 */
		result.add(car); /* '\u00C1' alt-0193 */
		result.add(car); /* '\u00C2' alt-0194 */
		result.add(car); /* '\u00C3' alt-0195 */
		result.add(car); /* '\u00C4' alt-0196 */
		result.add(car); /* '\u00C5' alt-0197 */
		car = new String("AE"); //$NON-NLS-1$
		result.add(car); /* '\u00C6' alt-0198 */
		car = new String("C"); //$NON-NLS-1$
		result.add(car); /* '\u00C7' alt-0199 */
		car = new String("E"); //$NON-NLS-1$
		result.add(car); /* '\u00C8' alt-0200 */
		result.add(car); /* '\u00C9' alt-0201 */
		result.add(car); /* '\u00CA' alt-0202 */
		result.add(car); /* '\u00CB' alt-0203 */
		car = new String("I"); //$NON-NLS-1$
		result.add(car); /* '\u00CC' alt-0204 */
		result.add(car); /* '\u00CD' alt-0205 */
		result.add(car); /* '\u00CE' alt-0206 */
		result.add(car); /* '\u00CF' alt-0207 */
		car = new String("D"); //$NON-NLS-1$
		result.add(car); /* '\u00D0' alt-0208 */
		car = new String("N"); //$NON-NLS-1$
		result.add(car); /* '\u00D1' alt-0209 */
		car = new String("O"); //$NON-NLS-1$
		result.add(car); /* '\u00D2' alt-0210 */
		result.add(car); /* '\u00D3' alt-0211 */
		result.add(car); /* '\u00D4' alt-0212 */
		result.add(car); /* '\u00D5' alt-0213 */
		result.add(car); /* '\u00D6' alt-0214 */
		car = new String("*"); //$NON-NLS-1$
		result.add(car); /* '\u00D7' alt-0215 */
		car = new String("0"); //$NON-NLS-1$
		result.add(car); /* '\u00D8' alt-0216 */
		car = new String("U"); //$NON-NLS-1$
		result.add(car); /* '\u00D9' alt-0217 */
		result.add(car); /* '\u00DA' alt-0218 */
		result.add(car); /* '\u00DB' alt-0219 */
		result.add(car); /* '\u00DC' alt-0220 */
		car = new String("Y"); //$NON-NLS-1$
		result.add(car); /* '\u00DD' alt-0221 */
		car = new String("_"); //$NON-NLS-1$
		result.add(car); /* '\u00DE' alt-0222 */
		car = new String("B"); //$NON-NLS-1$
		result.add(car); /* '\u00DF' alt-0223 */
		car = new String("a"); //$NON-NLS-1$
		result.add(car); /* '\u00E0' alt-0224 */
		result.add(car); /* '\u00E1' alt-0225 */
		result.add(car); /* '\u00E2' alt-0226 */
		result.add(car); /* '\u00E3' alt-0227 */
		result.add(car); /* '\u00E4' alt-0228 */
		result.add(car); /* '\u00E5' alt-0229 */
		car = new String("ae"); //$NON-NLS-1$
		result.add(car); /* '\u00E6' alt-0230 */
		car = new String("c"); //$NON-NLS-1$
		result.add(car); /* '\u00E7' alt-0231 */
		car = new String("e"); //$NON-NLS-1$
		result.add(car); /* '\u00E8' alt-0232 */
		result.add(car); /* '\u00E9' alt-0233 */
		result.add(car); /* '\u00EA' alt-0234 */
		result.add(car); /* '\u00EB' alt-0235 */
		car = new String("i"); //$NON-NLS-1$
		result.add(car); /* '\u00EC' alt-0236 */
		result.add(car); /* '\u00ED' alt-0237 */
		result.add(car); /* '\u00EE' alt-0238 */
		result.add(car); /* '\u00EF' alt-0239 */
		car = new String("d"); //$NON-NLS-1$
		result.add(car); /* '\u00F0' alt-0240 */
		car = new String("n"); //$NON-NLS-1$
		result.add(car); /* '\u00F1' alt-0241 */
		car = new String("o"); //$NON-NLS-1$
		result.add(car); /* '\u00F2' alt-0242 */
		result.add(car); /* '\u00F3' alt-0243 */
		result.add(car); /* '\u00F4' alt-0244 */
		result.add(car); /* '\u00F5' alt-0245 */
		result.add(car); /* '\u00F6' alt-0246 */
		car = new String("/"); //$NON-NLS-1$
		result.add(car); /* '\u00F7' alt-0247 */
		car = new String("0"); //$NON-NLS-1$
		result.add(car); /* '\u00F8' alt-0248 */
		car = new String("u"); //$NON-NLS-1$
		result.add(car); /* '\u00F9' alt-0249 */
		result.add(car); /* '\u00FA' alt-0250 */
		result.add(car); /* '\u00FB' alt-0251 */
		result.add(car); /* '\u00FC' alt-0252 */
		car = new String("y"); //$NON-NLS-1$
		result.add(car); /* '\u00FD' alt-0253 */
		car = new String("_"); //$NON-NLS-1$
		result.add(car); /* '\u00FE' alt-0254 */
		car = new String("y"); //$NON-NLS-1$
		result.add(car); /* '\u00FF' alt-0255 */
		result.add(car); /* '\u00FF' alt-0255 */

		return result;
	}

	@SuppressWarnings("CallToThreadDumpStack")
	public static String gerarUpperCasePrimeiraLetraDasPalavras(String s) {
		try {
			if (s == null) {
				return "";
			}
			for (int i = 0; i < s.length(); i++) {

				if (i == 0) {
					s = String.format("%s%s", Character.toUpperCase(s.charAt(0)), s.substring(1));
				}

				if (!Character.isLetterOrDigit(s.charAt(i))) {
					try {
						if (s.substring(i + 1, i + 4).lastIndexOf(" ") == 2 || s.substring(i + 1, i + 3).lastIndexOf(" ") == 1) {
							continue;
						}
					} catch (Exception e) {
					}
					if (i + 1 < s.length()) {
						s = String.format("%s%s%s", s.subSequence(0, i + 1), Character.toUpperCase(s.charAt(i + 1)), s.substring(i + 2));
					}
				}
			}
			return s;
		} catch (Exception e) {
			return "";
		}
	}

	
	public static String alterarpreposicaoParaMinusculo(String valor) {
		try {
			valor = valor.replaceAll(" Dos ", " dos ");
			valor = valor.replaceAll(" Das ", " das ");
			valor = valor.replaceAll(" De ", " de ");
			valor = valor.replaceAll(" Da ", " da ");
			valor = valor.replaceAll(" E ", " e ");
			valor = valor.replaceAll(" A ", " a ");
			valor = valor.replaceAll(" I ", " i ");
			valor = valor.replaceAll(" O ", " o ");
			valor = valor.replaceAll(" O ", " o ");
			valor = valor.replaceAll(" D'Arc ", " D'arc ");
			valor = valor.replaceAll(" Sant'Ana ", " Sant'ana ");
			return valor;
		} catch (Exception e) {
			return "";
		}
	}

	public static Date gerarDataDiaMesAno(Integer dia, Integer mes, Integer ano) throws Exception {
		String ini = dia + "/" + Uteis.getMesReferencia(mes, ano);
		return Uteis.getDateSemHora(Uteis.getDate(ini));
	}

	public static Date gerarDataInicioMes(Integer mes, Integer ano) throws Exception {
		String ini = "01/" + Uteis.getMesReferencia(mes, ano);
		return Uteis.getDateSemHora(Uteis.getDate(ini));

	}

	public static Date gerarDataFimMes(Integer mes, Integer ano) throws Exception {
		String fim = Uteis.getMesReferencia(mes, ano);
		Integer qtdDiasMes = Uteis.obterNrDiasNoMes(Uteis.getDate("01/" + fim));
		fim = qtdDiasMes + "/" + fim;
		return Uteis.getDateHoraFinalDia(Uteis.getDate(fim));
	}

	public static int obterNrDiasNoMes(Date dataPrm) {
		Calendar dataCalendario = Calendar.getInstance();
		dataCalendario.setTime(dataPrm);
		int numeroDias = dataCalendario.getActualMaximum(Calendar.DAY_OF_MONTH);
		return numeroDias;
	}

	public static Integer obterQuantidadeMesesEntreDatas(Date dataInicio, Date dataFim) throws Exception {
		Integer qtde = 0;
		while (dataInicio.before(dataFim) || dataInicio.equals(dataFim)) {
			dataInicio = Uteis.getDataFutura(dataInicio, GregorianCalendar.MONTH, 1);
			qtde++;
		}
		return qtde;
	}

	/**
	 * Adiciona um anexo ao email.
	 *
	 * @param nomeAnexo
	 * @param anexo
	 * @return
	 */
	public Uteis addAnexo(String nomeAnexo, File anexo) {
		this.mapaAnexo.put(nomeAnexo, anexo);
		return this;
	}

	/**
	 * Adiciona todos os arquivos contidos no diretorio informado na lista de
	 * imagens.
	 *
	 * @param diretorioImagens
	 * @return
	 */
	@SuppressWarnings("ManualArrayToCollectionCopy")
	public Uteis addImagensEmDiretorio(File diretorioImagens) {
		for (File file : diretorioImagens.listFiles()) {
			listaImagem.add(file);
		}
		return this;
	}

	/**
	 * Adiciona um arquivo de imagem ao email.
	 *
	 * @param imagem
	 * @return
	 */
	public Uteis addImagem(File imagem) {
		this.listaImagem.add(imagem);
		return this;
	}

	public static String removerAcentos(String text) {
		StringBuilder result = new StringBuilder(text);

		for (int bcl = 0; bcl < result.length(); bcl++) {
			int carVal = text.charAt(bcl);
			if (carVal >= MIN && carVal <= MAX) { // Remplacement
				String newVal = (String) map.get(carVal - MIN);
				result.replace(bcl, bcl + 1, newVal);
			}
		}
		return result.toString();
	}

	public static String substituirTagCasoExista(String text, Double valor) {
		while (text.contains(")%]")) {
			int posIni = text.indexOf("[(");
			int posFim = text.indexOf(")%]");
			String valorStr = text.substring(posIni + 2, posFim);
			String tagStr = text.substring(posIni, posFim + 3);
			valorStr = valorStr.replace(",", ".");
			Double vlr = Double.parseDouble(valorStr);
			vlr = valor * vlr;
			text = text.replace(tagStr, formatarDoubleParaMoeda(vlr));
		}
		return text;
	}

	public static String trocarHashTag(String hashTag, String palavra, String texto) {
		String novoTexto = texto.replace(hashTag, palavra);
		return novoTexto;
	}

	public static String trocarLetraAcentuadaPorCodigoHtml(String texto) {
		texto = texto.replaceAll("ï¿½", "&Aacute;");
		texto = texto.replaceAll("ï¿½", "&aacute;");
		texto = texto.replaceAll("ï¿½", "&Acirc;");
		texto = texto.replaceAll("ï¿½", "&acirc;");
		texto = texto.replaceAll("ï¿½", "&Agrave;");
		texto = texto.replaceAll("ï¿½", "&agrave;");
		texto = texto.replaceAll("ï¿½", "&Aring;");
		texto = texto.replaceAll("ï¿½", "&aring;");
		texto = texto.replaceAll("ï¿½", "&Atilde;");
		texto = texto.replaceAll("ï¿½", "&atilde;");
		texto = texto.replaceAll("ï¿½", "&Auml;");
		texto = texto.replaceAll("ï¿½", "&auml;");
		texto = texto.replaceAll("ï¿½", "&Eacute;");
		texto = texto.replaceAll("ï¿½", "&eacute;");
		texto = texto.replaceAll("ï¿½", "&Ecirc;");
		texto = texto.replaceAll("ï¿½", "&ecirc;");
		texto = texto.replaceAll("ï¿½", "&Egrave;");
		texto = texto.replaceAll("ï¿½", "&egrave;");
		texto = texto.replaceAll("ï¿½", "&Euml;");
		texto = texto.replaceAll("ï¿½", "&euml;");
		texto = texto.replaceAll("ï¿½", "&Iacute;");
		texto = texto.replaceAll("ï¿½", "&iacute;");
		texto = texto.replaceAll("ï¿½", "&Icirc;");
		texto = texto.replaceAll("ï¿½", "&icirc;");
		texto = texto.replaceAll("ï¿½", "&Igrave;");
		texto = texto.replaceAll("ï¿½", "&igrave;");
		texto = texto.replaceAll("ï¿½", "&Iuml;");
		texto = texto.replaceAll("ï¿½", "&iuml;");
		texto = texto.replaceAll("ï¿½", "&Oacute;");
		texto = texto.replaceAll("ï¿½", "&oacute;");
		texto = texto.replaceAll("ï¿½", "&Ocirc;");
		texto = texto.replaceAll("ï¿½", "&ocirc;");
		texto = texto.replaceAll("ï¿½", "&Ograve;");
		texto = texto.replaceAll("ï¿½", "&ograve;");
		texto = texto.replaceAll("ï¿½", "&Otilde;");
		texto = texto.replaceAll("ï¿½", "&otilde;");
		texto = texto.replaceAll("ï¿½", "&Ouml;");
		texto = texto.replaceAll("ï¿½", "&ouml;");
		texto = texto.replaceAll("ï¿½", "&Uacute;");
		texto = texto.replaceAll("ï¿½", "&uacute;");
		texto = texto.replaceAll("ï¿½", "&Ucirc;");
		texto = texto.replaceAll("ï¿½", "&ucirc;");
		texto = texto.replaceAll("ï¿½", "&Ugrave;");
		texto = texto.replaceAll("ï¿½", "&ugrave;");
		texto = texto.replaceAll("ï¿½", "&Uuml;");
		texto = texto.replaceAll("ï¿½", "&uuml;");
		texto = texto.replaceAll("ï¿½", "&Ccedil;");
		texto = texto.replaceAll("ï¿½", "&ccedil;");
		return texto;
	}

//	public static class OrdenaListaBalanceteRelVOPorData implements Comparator<BalanceteRelVO> {
//
//		@SuppressWarnings("CallToThreadDumpStack")
//		public int compare(BalanceteRelVO obj1, BalanceteRelVO obj2) {
//			try {
//				return (obj1.getData()).compareTo(obj2.getData());
//			} catch (ParseException e) {
//			}
//			return 0;
//		}
//	}

//	public static class OrdenaListaContaReceberVOPorDataVencimento implements Comparator<ContaReceberVO> {
//
//		@SuppressWarnings("CallToThreadDumpStack")
//		public int compare(ContaReceberVO obj1, ContaReceberVO obj2) {
//			try {
//				return (obj1.getDataVencimento()).compareTo(obj2.getDataVencimento());
//			} catch (Exception e) {
//			}
//			return 0;
//		}
//	}
//
//	public static class OrdenaListaMatriculaPeriodoVencimentoVOPorDataVencimento implements Comparator<MatriculaPeriodoVencimentoVO> {
//
//		@SuppressWarnings("CallToThreadDumpStack")
//		public int compare(MatriculaPeriodoVencimentoVO obj1, MatriculaPeriodoVencimentoVO obj2) {
//			try {
//				return (obj1.getDataVencimento()).compareTo(obj2.getDataVencimento());
//			} catch (Exception e) {
//			}
//			return 0;
//		}
//	}

	public static String removeCaractersEspeciais(String string) {
		if (!string.contains("''")) {
			if (string.contains("'")) {
				string = string.replaceAll("'", "''");
			}
		}
		string = string.replaceAll("[�����]", "A");
		string = string.replaceAll("[�����]", "a");
		string = string.replaceAll("[����]", "E");
		string = string.replaceAll("[����]", "e");
		string = string.replaceAll("����", "I");
		string = string.replaceAll("�", "I");
		string = string.replaceAll("����", "i");
		string = string.replaceAll("[�����]", "O");
		string = string.replaceAll("[�����]", "o");
		string = string.replaceAll("[����]", "U");
		string = string.replaceAll("[����]", "u");
		string = string.replaceAll("�", "C");
		string = string.replaceAll("�", "c");
		string = string.replaceAll("[��]", "y");
		string = string.replaceAll("�", "Y");
		string = string.replaceAll("�", "n");
		string = string.replaceAll("�", "N");
		string = string.replaceAll("\\\\", "");
		string = string.replaceAll("['<>|/]�", "");
		string = string.replaceAll(" - ", " ");
		string = string.replaceAll("-", " ");
		string = string.replaceAll("- ", "");
		string = string.replaceAll(" -", "");
		// texto = texto.replaceAll("[ ]", "");
		string = string.replaceAll("[-#$%�&*()_+={}?.,:;���^~�`�@!\"]", "");
		return string;
	}

	public static String removeCaractersEspeciais2(String string) {
		if (!string.contains("''")) {
			if (string.contains("'")) {
				string = string.replaceAll("'", "''");
			}
		}
		string = string.replaceAll("\\\\", "");
//		string = string.replaceAll("['<>|/]¦", "");
		string = string.replaceAll("['<>|/]�", "");
		// texto = texto.replaceAll("[ ]", "");
//		string = string.replaceAll("[-#$%¨&*()_+={}?.,:;º°ª^~´`§@!\"]", "");
		string = string.replaceAll("[#$%�&*()_+={}?.,:;���^~�`�@!\"]", "");
		return string;
	}

	public static String retirarMascaraCPF(String cpf) {
		return cpf.replace(".", "").replace("-", "");
	}

	public static String retirarMascaraTelefone(String telefone) {
		return telefone.replace("-", "").replace(")", "").replace("(", "").replace(".", "");
	}

	public static String adicionarMascaraCPF(String cpf) {
		cpf = cpf.substring(0, 3).concat(".").concat(cpf.substring(3, 6).concat(".").concat(cpf.substring(6, 9).concat("-").concat(cpf.substring(9))));
		return cpf;
	}

	public static String retirarMascaraCNPJ(String cnpj) {
		return cnpj.replace(".", "").replace("-", "").replace("/", "");
	}

	public static String formatarDecimalDuasCasas(Double valor) {
		return formatarDecimal(valor, "0.00#");
	}

	public static String formatarDecimal(Double valor, String pattern) {
		DecimalFormat df = new DecimalFormat(pattern);
		if (valor != null) {
			return df.format(valor);
		}
		return "";
	}

	public static Integer getDataQuantidadeMesesEntreData(Date dataInicio, Date dataFim) {
		Calendar calInicio = Calendar.getInstance();
		Calendar calFim = Calendar.getInstance();
		calInicio.setTime(dataInicio);
		calFim.setTime(dataFim);
		int nrMes = calFim.get(Calendar.MONTH) - calInicio.get(Calendar.MONTH);
		dataFim = null;
		dataInicio = null;
		calFim = null;
		calInicio = null;
		return nrMes;
	}

	public static Date getData(String data, String pattern) throws ParseException {
		try {
			if (data != null) {
				SimpleDateFormat formatador = new SimpleDateFormat(pattern);
				return formatador.parse(data);
			}
		} catch (Exception e) {
		}
		return null;
	}

	public static String getData(Date data, String pattern) {
		if (data != null) {
			SimpleDateFormat formatador = new SimpleDateFormat(pattern, new Locale("pt", "BR"));
			return formatador.format(data);
		}
		return "";
	}

	public static String getData(Date data, String pattern, Date ultimaDataValida) {
		if (ultimaDataValida != null && ultimaDataValida.before(data)) {
			return getData(ultimaDataValida, pattern);
		} else {
			return getData(data, pattern);
		}
	}

	public static String getData(Date data, String pattern, Date ultimaDataValida, Date primeiraDataValida) {
		if (ultimaDataValida != null && data != null && ultimaDataValida.before(data)) {
			return getData(ultimaDataValida, pattern);
		} else if (primeiraDataValida != null && data != null && primeiraDataValida.after(data)) {
			return getData(primeiraDataValida, pattern);
		} else {
			return getData(data, pattern);
		}
	}

	public static Date getDataPassada(Date dataInicial, int quantidadeRegredir) throws Exception {
		// GregorianCalendar gc = new GregorianCalendar();
		// gc.setTime(dataInicial);
		// gc.roll(Calendar.MONTH, quantidadeAvancar * -1);
		// return gc.getTime();
		return obterDataPassada(dataInicial, quantidadeRegredir);
	}

	public static Date getDataFutura(Date dataInicial, int field, int quantidadeAvancar) throws Exception {
		if (quantidadeAvancar == 0) {
			return dataInicial;
		}
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(dataInicial);
		gc.add(field, quantidadeAvancar);
		return gc.getTime();
	}

	public static String obterCpfValido(String cpf) {
		if (verificaCPF(cpf)) {
			return cpf;
		} else {
			return "";
		}
	}

	public static boolean verificaCPF(String cpf) {

		try {
			cpf = retirarMascaraCPF(cpf);

			if ((cpf.equals("00000000000")) || (cpf.equals("11111111111")) || (cpf.equals("22222222222")) || (cpf.equals("33333333333")) || (cpf.equals("44444444444")) || (cpf.equals("55555555555")) || (cpf.equals("66666666666")) || (cpf.equals("77777777777")) || (cpf.equals("88888888888")) || (cpf.equals("99999999999"))) {
				return false;
			}

			int count, soma, x, y, CPF[] = new int[11];

			if (cpf.length() != 11) {
				return false;
			}

			soma = 0;
			for (count = 0; count < 11; count++) {
				CPF[count] = 0;
			}

			char vetorChar[] = new char[11];
			String temp, CPFvalido = "";

			for (count = 0; count < 11; count++) {
				// Transformar String em vetor de caracteres
				vetorChar = cpf.toCharArray();
				// Transformar cada caractere em String
				temp = String.valueOf(vetorChar[count]);
				// Transformar String em inteiro e jogar no vetor
				CPF[count] = Integer.parseInt(temp);
			}
			// MÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÂ¢Ã¢â‚¬Å¾Ã‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¾Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ€šÃ‚Â¡ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â©todo
			// da
			// ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÂ¢Ã¢â‚¬Å¾Ã‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¾Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ€šÃ‚Â¡ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¡rvore
			// para obter o x
			for (count = 0; count < 9; count++) {
				// Pegar soma da
				// permutaÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÂ¢Ã¢â‚¬Å¾Ã‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¾Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ€šÃ‚Â¡ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â§ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÂ¢Ã¢â‚¬Å¾Ã‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¾Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ€šÃ‚Â¡ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â£o
				// dos
				// dÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÂ¢Ã¢â‚¬Å¾Ã‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¾Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ€šÃ‚Â¡ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â­gitos
				// do CPF
				soma += CPF[count] * (10 - count);
			}

			// se o resto da
			// divisÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÂ¢Ã¢â‚¬Å¾Ã‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¾Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ€šÃ‚Â¡ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â£o
			// der 0 ou 1, x
			// terÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÂ¢Ã¢â‚¬Å¾Ã‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¾Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ€šÃ‚Â¡ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¡
			// dois
			// dÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÂ¢Ã¢â‚¬Å¾Ã‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¾Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ€šÃ‚Â¡ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â­gitos
			if (soma % 11 < 2) {
				x = 0;
			} // x
				// nÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÂ¢Ã¢â‚¬Å¾Ã‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¾Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ€šÃ‚Â¡ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â£o
				// pode ter dois
				// dÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÂ¢Ã¢â‚¬Å¾Ã‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¾Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ€šÃ‚Â¡ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â­gitos
			else {
				x = 11 - (soma % 11);
			} // obtendo o
				// penÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÂ¢Ã¢â‚¬Å¾Ã‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¾Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ€šÃ‚Â¡ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Âºltimo
				// dÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÂ¢Ã¢â‚¬Å¾Ã‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¾Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ€šÃ‚Â¡ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â­gito
				// do CPF

			CPF[9] = x;

			// MÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÂ¢Ã¢â‚¬Å¾Ã‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¾Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ€šÃ‚Â¡ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â©todo
			// da
			// ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÂ¢Ã¢â‚¬Å¾Ã‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¾Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ€šÃ‚Â¡ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¡rvore
			// para obter o y
			soma = 0;
			for (count = 0; count < 10; count++) {
				// Pegar soma da
				// permutaÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÂ¢Ã¢â‚¬Å¾Ã‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¾Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ€šÃ‚Â¡ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â§ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÂ¢Ã¢â‚¬Å¾Ã‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¾Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ€šÃ‚Â¡ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â£o
				// dos
				// dÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÂ¢Ã¢â‚¬Å¾Ã‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¾Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ€šÃ‚Â¡ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â­gitos
				// do CPF
				soma += CPF[count] * (11 - count);
			}

			// se o resto da
			// divisÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÂ¢Ã¢â‚¬Å¾Ã‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¾Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ€šÃ‚Â¡ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â£o
			// der 0 ou 1, y
			// terÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÂ¢Ã¢â‚¬Å¾Ã‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¾Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ€šÃ‚Â¡ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¡
			// dois
			// dÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÂ¢Ã¢â‚¬Å¾Ã‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¾Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ€šÃ‚Â¡ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â­gitos
			if (soma % 11 < 2) {
				y = 0;
			} // y
				// nÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÂ¢Ã¢â‚¬Å¾Ã‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¾Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ€šÃ‚Â¡ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â£o
				// pode ter dois
				// dÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÂ¢Ã¢â‚¬Å¾Ã‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¾Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ€šÃ‚Â¡ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â­gitos
			else {
				y = 11 - (soma % 11);
			} // obtendo o
				// ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÂ¢Ã¢â‚¬Å¾Ã‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¾Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ€šÃ‚Â¡ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Âºltimo
				// dÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÂ¢Ã¢â‚¬Å¾Ã‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¾Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ€šÃ‚Â¡ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â­gito
				// do CPF

			CPF[10] = y;
			soma = 0;

			// Verificando se o cpf informado
			// ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÂ¢Ã¢â‚¬Å¾Ã‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¾Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ€šÃ‚Â¡ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â©
			// vÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÂ¢Ã¢â‚¬Å¾Ã‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¾Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ€šÃ‚Â¡ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¡lido
			// para retornar resultado
			// ao programa
			for (count = 0; count < 11; count++) {
				CPFvalido += String.valueOf(CPF[count]);
			}
			if (cpf.compareTo(CPFvalido) == 0) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}

	public static String gerarCPFValido() {
		try {
			Double numeroRandomico = Math.random();
			String cpf = numeroRandomico.toString();
			cpf = cpf.substring(cpf.lastIndexOf(".") + 1, 11);
			char vetorChar[] = new char[11];
			String temp, CPFvalido = "";
			int count, x, y, CPF[] = new int[11];
			int soma = 0;

			for (count = 0; count < 9; count++) {
				vetorChar = cpf.toCharArray();
				temp = String.valueOf(vetorChar[count]);
				CPF[count] = Integer.parseInt(temp);
			}

			CPF[9] = 0;
			CPF[10] = 0;

			for (count = 0; count < 9; count++) {
				soma += CPF[count] * (10 - count);
			}

			if (soma % 11 < 2) {
				x = 0;
			} else {
				x = 11 - (soma % 11);
			}

			CPF[9] = x;
			soma = 0;

			for (count = 0; count < 10; count++) {
				soma += CPF[count] * (11 - count);
			}

			if (soma % 11 < 2) {
				y = 0;
			} else {
				y = 11 - (soma % 11);
			}

			CPF[10] = y;
			for (count = 0; count < 11; count++) {
				CPFvalido += String.valueOf(CPF[count]);
			}

			return CPFvalido;
		} catch (Exception e) {
			return "";
		}
	}

	public static String getNomeResumido(String nome) {
		List<String> preposicoes2 = new ArrayList(0);
		preposicoes2.add("dos");
		preposicoes2.add("das");
		preposicoes2.add("de");
		preposicoes2.add("da");
		preposicoes2.add("e");
		preposicoes2.add("a");
		preposicoes2.add("i");
		preposicoes2.add("o");
		preposicoes2.add("DOS");
		preposicoes2.add("DAS");
		preposicoes2.add("DE");
		preposicoes2.add("DA");
		preposicoes2.add("E");
		preposicoes2.add("A");
		preposicoes2.add("I");
		preposicoes2.add("O");
		String[] nomes = nome.split(" ");
		String nomeResumido = "";
		Integer indice = 1;
		nomeResumido += nomes[0] + " ";
		while (indice < nomes.length - 1) {
			if (preposicoes2.contains(nomes[indice])) {
				nomes[indice] = "";
			} else if (nomes[indice].length() > 2) {
				nomes[indice] = nomes[indice].substring(0, 1).concat(".");
			}
			nomeResumido += nomes[indice] + " ";
			indice = indice + 1;
		}
		preposicoes2 = null;
		nomeResumido += nomes[nomes.length - 1] + " ";
		return nomeResumido;

	}

	public static String getNomeArquivo(String caminhoCompleto) {
		int indice = caminhoCompleto.lastIndexOf(File.separator);
		return caminhoCompleto.substring(indice + 1);
	}

	public static String getNomeArquivoComUnidadeEnsino(String nomeArquivo, Integer unidadeEnsino) {
		return nomeArquivo.concat("{").concat(unidadeEnsino.toString()).concat("}");
	}

	public static String getStringValida(String texto) {
		if (isAtributoPreenchido(texto)) {
			return texto;
		}
		return "";
	}

	public static String getExtensaoDeArquivo(File arquivo) {
		return getExtensaoDeArquivo(arquivo.getName());
	}

	public static String getExtensaoDeArquivo(String nomeArquivo) {
		return nomeArquivo.substring(nomeArquivo.length() - 4).replace(".", "");
	}

	public static String preencherComZerosPosicoesVagas(String padrao, int tamanhoGeracao) {
		if (tamanhoGeracao > padrao.length()) {
			int nrPosicoesPreencher = tamanhoGeracao - padrao.length();
			while (nrPosicoesPreencher > 0) {
				padrao = "0" + padrao;
				nrPosicoesPreencher--;
			}
		}
		return padrao;
	}

	/**
	 * Metodo retorna um new date com o numero de meses a menos que a data atual
	 *
	 * @param numeroMeses
	 * @return Esta mudando somente os meses
	 */
	public static Date getNewDateComMesesAMenos(int numeroMeses) {
		GregorianCalendar dataInicio = new GregorianCalendar();
		dataInicio.roll(GregorianCalendar.MONTH, -numeroMeses);
		if (dataInicio.get(GregorianCalendar.MONTH) == 11) {
			dataInicio.roll(GregorianCalendar.YEAR, -1);
		}
		return dataInicio.getTime();
	}

	/**
	 * Diminui os meses e anos se
	 * necessÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚�
	 * �â„¢ÃƒÆ’Ã¢â‚�
	 * �Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã�
	 * �â€™ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã‚Â ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚�
	 * �Å¡Ã‚Â¬ÃƒÂ¢Ã¢â‚�
	 * �Å¾Ã‚Â¢ÃƒÆ’Ã†â€™Ãƒâ�
	 * � Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚�
	 * �Å¡Ã‚Â¬Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ€š
	 * Ã‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â�
	 * �ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ�
	 * �Ã…Â¾Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã�
	 * �â€™Ãƒâ€ Ã¢â‚¬
	 * â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã�
	 * �â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã�
	 * �Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ€šÃ‚Â�
	 * �ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬
	 * â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚�
	 * �Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â�
	 * �ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€�
	 * �Â¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¡rio
	 */
	public static Date getObterDataComMesesAMenos(Date data, int numeroMeses) {
		GregorianCalendar dataInicio = new GregorianCalendar();
		dataInicio.setTime(data);
		dataInicio.add(GregorianCalendar.MONTH, -numeroMeses);
		return dataInicio.getTime();
	}

	/**
	 * Metodo retorna um new date com o um mes a mais que a data atual
	 *
	 * @param numeroMeses
	 * @return
	 */
	public static Date getNewDateComUmMesAMais() {
		GregorianCalendar dataInicio = new GregorianCalendar();
		dataInicio.roll(GregorianCalendar.MONTH, +1);
		if (dataInicio.get(GregorianCalendar.MONTH) == 0) {
			dataInicio.roll(GregorianCalendar.YEAR, +1);
		}
		return dataInicio.getTime();
	}

	/**
	 * Retorna valor inteiro para uma string e retira da string qualquer coisa
	 * que nao seja numero. otima para tratar input text.
	 *
	 * @param numero
	 * @return
	 */
	public static Integer getValorInteiro(String numero) {
		numero = numero.replaceAll("\\D", "");
		if (isAtributoPreenchido(numero)) {
			return Integer.parseInt(numero);
		}
		return 0;
	}

	public static Long getValorLong(String numero) {
		numero = numero.replaceAll("\\D", "");
		if (isAtributoPreenchido(numero)) {
			return Long.parseLong(numero);
		}
		return 0L;
	}

	public static double getValorDoubleComCasasDecimais(String numero) {
		numero = numero.replaceAll("\\D", "");
		if (isAtributoPreenchido(numero)) {
			return Double.parseDouble(numero) / 100;
		}
		return 0d;
	}

	public static boolean isAtributoPreenchido(String texto) {
		return texto != null && !texto.trim().equals("0") && !texto.isEmpty();
	}

	public static boolean isAtributoPreenchido(List<?> lista) {
		return lista != null && !lista.isEmpty();
	}

//	public static boolean isAtributoPreenchido(Object objeto) {
//		try {
//			if (objeto != null) {
//				Integer i = (Integer) UtilReflexao.invocarMetodoGet(objeto, "codigo");
//				return (i != null && !i.equals(0)) || i == null;
//			}
//			return false;
//		} catch (Exception e) {
//			return false;
//		}
//	}		

	public static boolean isAtributoPreenchido(Integer inteiro) {
		return inteiro != null && !inteiro.equals(0);
	}

	public static boolean isAtributoPreenchido(Double d) {
		return d != null && !d.equals(0.0);
	}

	public static boolean isAtributoPreenchido(Date data) {
		return data != null;
	}

	public static boolean isAtributoPreenchido(java.sql.Date data) {
		return data != null;
	}

	public static boolean isAtributoPreenchido(Boolean booleano) {
		return booleano != null;
	}

	public static java.sql.Date getSQLData(java.util.Date dataConverter) {
		if (dataConverter == null) {
			return null;
		}
		java.sql.Date dataSQL = new java.sql.Date(dataConverter.getTime());
		return dataSQL;
	}

	public static String getDataComHora(java.util.Date dataConverter) {
		if (dataConverter == null) {
			return "";
		}
		DateFormat formatador = DateFormat.getDateInstance(DateFormat.SHORT);
		String dataStr = formatador.format(dataConverter);

		DateFormat formatadorHora = DateFormat.getTimeInstance(DateFormat.SHORT);
		dataStr += " " + formatadorHora.format(dataConverter);

		return dataStr;
	}

	public static String encriptar(String senha) throws UnsupportedEncodingException {
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256"); // "MD5"
			md.update(senha.getBytes());
			return converterParaHexa(md.digest());
		} catch (NoSuchAlgorithmException e) {
			return null;
		}
	}

	private static String converterParaHexa(byte[] bytes) {
		StringBuilder s = new StringBuilder();
		for (int i = 0; i < bytes.length; i++) {
			int parteAlta = ((bytes[i] >> 4) & 0xf) << 4;
			int parteBaixa = bytes[i] & 0xf;
			if (parteAlta == 0) {
				s.append('0');
			}
			s.append(Integer.toHexString(parteAlta | parteBaixa));
		}
		return s.toString();
	}

	public static String getDataAno4Digitos(java.util.Date dataConverter) {
		String dataStr = "";
		if (dataConverter != null) {
			DateFormat formatador = DateFormat.getDateInstance(DateFormat.MEDIUM);
			dataStr = formatador.format(dataConverter);
		}
		return dataStr;
	}

	public static Date getDataVencimentoPadrao(Integer diaPadrao, Date dataPadrao, int nrMesesProgredir) throws Exception {
		int mes = Uteis.getMesData(dataPadrao);
		int ano = Uteis.getAnoData(dataPadrao);
		String diaFinal = String.valueOf(diaPadrao);

		mes = mes + nrMesesProgredir;
		if (mes > 12) {
			mes = mes - 12;
			ano = ano + 1;
		}

		String mesFinal = String.valueOf(mes);
		String anoFinal = String.valueOf(ano);

		if (String.valueOf(mesFinal).length() == 1) {
			mesFinal = "0" + mesFinal;
		}
		if (String.valueOf(diaFinal).length() == 1) {
			diaFinal = "0" + diaFinal;
		}
		String dataFinal = diaFinal + "/" + mesFinal + "/" + anoFinal;
		return Uteis.getDateSemHora(Uteis.getDate(dataFinal));
	}

	public static Date getDataVencimentoDiaPorMesAno(int diaPadrao, int mes, int ano, int nrMesesProgredir) throws Exception {

		String diaFinal = String.valueOf(diaPadrao);

		mes = mes + nrMesesProgredir;
		if (mes > 12) {
			mes = mes - 12;
			ano = ano + 1;
		}

		String mesFinal = String.valueOf(mes);
		String anoFinal = String.valueOf(ano);

		if (String.valueOf(mesFinal).length() == 1) {
			mesFinal = "0" + mesFinal;
		}
		if (String.valueOf(diaFinal).length() == 1) {
			diaFinal = "0" + diaFinal;
		}
		String dataFinal = diaFinal + "/" + mesFinal + "/" + anoFinal;
		return Uteis.getDateSemHora(Uteis.getDate(dataFinal));
	}

	public static int getCalculaQuantidadeMesesEntreDatas(Date dataInicio, Date dataTermino) throws Exception {
		int total = 0;

		String dataIni = Uteis.getData(dataInicio);
		dataInicio = Uteis.getDate(dataIni);

		String dataFim = Uteis.getData(dataTermino);
		dataTermino = Uteis.getDate(dataFim);

		int nrDiasEntreDatas = (int) Uteis.nrDiasEntreDatas(dataTermino, dataInicio);
		total = nrDiasEntreDatas / 30;

		return total;
	}

	public static int getObterQuantidadeParcelasEntreDias(Date dataInicio, Date dataTermino, int diaVencimento) throws Exception {
		int meses = 0;
		int mesesCompletos = getMesData(dataTermino) - getMesData(dataInicio) - 1;

		int diaMesInicio = getDiaMesData(dataInicio);
		int diaMesTermino = getDiaMesData(dataTermino);

		if (diaVencimento >= diaMesInicio) {
			meses += 1;
		}
		if (diaVencimento <= diaMesTermino) {
			meses += 1;
		}
		return meses + mesesCompletos;
	}

	public static int getCalculaQuantidadeAnosEntreDatas(Date dataInicio, Date dataTermino) throws Exception {
		int total = 0;

		String dataIni = Uteis.getData(dataInicio);
		dataInicio = Uteis.getDate(dataIni);

		String dataFim = Uteis.getData(dataTermino);
		dataTermino = Uteis.getDate(dataFim);

		int nrDiasEntreDatas = (int) Uteis.nrDiasEntreDatas(dataTermino, dataInicio);
		total = nrDiasEntreDatas / 365;

		return total;
	}

	public static String getCompletarNumeroComZero(Integer tamanhoDesejado, Integer numero) throws Exception {
		String numFinal = "";
		int tamanhoPreenchimento = tamanhoDesejado - String.valueOf(numero).length();
		while (tamanhoPreenchimento > 0) {
			numFinal += "0";
			tamanhoPreenchimento--;
		}
		numFinal += String.valueOf(numero);
		return numFinal;
	}

	public static String obterZeroEsquerda(String valor) throws Exception {
		String zeroEsquerda = "";
		int tamanho = valor.length();
		int cont = 0;
		if (valor.contains("0")) {
			while (cont < tamanho) {
				if (valor.charAt(cont) == '0') {
					zeroEsquerda += valor.charAt(cont);
				} else {
					cont = tamanho;
				}
				cont++;
			}
		}
		return zeroEsquerda;
	}

	public static java.sql.Date getDataJDBC(java.util.Date dataConverter) {
		if (dataConverter == null) {
			return null;
			// dataConverter = new Date();
		}
		java.sql.Date dataSQL = new java.sql.Date(dataConverter.getTime());
		return dataSQL;
	}

	public static java.sql.Timestamp getDataComHoraSetadaParaUltimoMinutoDia(java.util.Date dataConverter) {
		if (dataConverter == null) {
			return null;
		}
		Calendar c = Calendar.getInstance();
		c.setTime(dataConverter);
		c.set(Calendar.HOUR, 23);
		c.set(Calendar.MINUTE, 59);
		c.set(Calendar.SECOND, 59);
		c.set(Calendar.MILLISECOND, 59);
		return new java.sql.Timestamp(c.getTime().getTime());
	}

	public static java.sql.Timestamp getDataComHoraSetadaParaPrimeiroMinutoDia(java.util.Date dataConverter) {
		if (dataConverter == null) {
			return null;
		}
		Calendar c = Calendar.getInstance();
		c.setTime(dataConverter);
		c.set(Calendar.HOUR, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		return new java.sql.Timestamp(c.getTime().getTime());
	}

	public static Date getDataUltimoDiaMes(java.util.Date dataConverter) {
		Calendar c = Calendar.getInstance();
		c.setTime(dataConverter);
		c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
		return c.getTime();
	}

	public static Date getDataPrimeiroDiaMes(java.util.Date dataConverter) {
		Calendar c = Calendar.getInstance();
		c.setTime(dataConverter);
		c.set(Calendar.DAY_OF_MONTH, c.getActualMinimum(Calendar.DAY_OF_MONTH));
		return c.getTime();
	}

	public static java.sql.Timestamp getDataJDBCTimestamp(java.util.Date dataConverter) {
		if (dataConverter == null) {
			return null;
		}
		java.sql.Timestamp dataSQL = new java.sql.Timestamp(dataConverter.getTime());
		return dataSQL;
	}

	public static String getDataFormatoBD(java.util.Date dataConverter) {
		return getDataBD(dataConverter, "bd");
	}

	public static String getDataBD(java.util.Date dataConverter, String padrao) {
		if (dataConverter == null) {
			return ("");
		}
		String dataStr;
		if (padrao.equals("bd")) {
			Locale aLocale = new Locale("pt", "BR");
			SimpleDateFormat formatador = new SimpleDateFormat("yyyy.MM.dd", aLocale);
			dataStr = formatador.format(dataConverter);
		} else {
			DateFormat formatador = DateFormat.getDateInstance(DateFormat.SHORT);
			dataStr = formatador.format(dataConverter);
		}
		return (dataStr);
	}

	public static String getDataBD2Digitos(java.util.Date dataConverter, String padrao) {
		if (dataConverter == null) {
			return ("");
		}
		String dataStr;
		if (padrao.equals("bd")) {
			Locale aLocale = new Locale("pt", "BR");
			SimpleDateFormat formatador = new SimpleDateFormat("yy.MM.dd", aLocale);
			dataStr = formatador.format(dataConverter);
		} else {
			DateFormat formatador = DateFormat.getDateInstance(DateFormat.SHORT);
			dataStr = formatador.format(dataConverter);
		}
		return (dataStr);
	}

	public static String getDataAplicandoFormatacao(Date data, String mascara) {
		if (data == null) {
			return "";
		}
		Locale aLocale = new Locale("pt", "BR");
		SimpleDateFormat formatador = new SimpleDateFormat(mascara, aLocale);
		String dataStr = formatador.format(data);
		return dataStr;
	}

	public static Date getDataVencimentoPadrao(Integer dataPadrao) throws Exception {
		int dia = Uteis.getDiaMesData(new Date());
		int mes = Uteis.getMesDataAtual();
		int ano = Uteis.getAnoData(new Date());
		String diaFinal = String.valueOf(dataPadrao);
		if (dia > dataPadrao.intValue()) {
			mes = mes + 1;
			if (mes > 12) {
				mes = 1;
				ano = ano + 1;
			}
		}
		String mesFinal = String.valueOf(mes);
		String anoFinal = String.valueOf(ano);

		if (String.valueOf(mesFinal).length() == 1) {
			mesFinal = "0" + mesFinal;
		}
		if (String.valueOf(diaFinal).length() == 1) {
			diaFinal = "0" + diaFinal;
		}
		String dataFinal = diaFinal + "/" + mesFinal + "/" + String.valueOf(ano);
		return Uteis.getDate(dataFinal);
	}

	/*
	 * Defini-se a
	 * mascarÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚�
	 * �Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€
	 * ™ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã‚Â ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å�
	 * �Ã‚Â¬ÃƒÂ¢Ã¢â‚�
	 * �Å¾Ã‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€�
	 * �Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚�
	 * �Å¡Ã‚Â¬Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ€š
	 * Ã‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â�
	 * �ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ�
	 * �Ã…Â¾Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã�
	 * �â€™Ãƒâ€ Ã¢â‚¬
	 * â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã�
	 * �â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã�
	 * �Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ€šÃ‚Â�
	 * �ÃƒÆ’Ã�
	 * �â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚�
	 * �
	 * Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€�
	 * �Â¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¡ a ser aplicada
	 * a data atual de acordo com o
	 * padrÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚�
	 * �â„¢ÃƒÆ’Ã¢â‚�
	 * �Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã�
	 * �â€™ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã‚Â ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚�
	 * �Å¡Ã‚Â¬ÃƒÂ¢Ã¢â‚�
	 * �Å¾Ã‚Â¢ÃƒÆ’Ã†â€™Ãƒâ�
	 * � Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚�
	 * �Å¡Ã‚Â¬Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ€š
	 * Ã‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â�
	 * �ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ�
	 * �Ã…Â¾Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã�
	 * �â€™Ãƒâ€ Ã¢â‚¬
	 * â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã�
	 * �â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã�
	 * �Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ€šÃ‚Â�
	 * �ÃƒÆ’Ã�
	 * �â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚�
	 * �
	 * Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€�
	 * �Â¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â£o - dd/mm/yyyy
	 * ou MM.yy.dd e assim por diante
	 */
	public static String getDataAtualAplicandoFormatacao(String mascara) {
		Date hoje = new Date();
		return getDataAplicandoFormatacao(hoje, mascara);
	}

	public static String getData(java.util.Date dataConverter) {
		if (dataConverter == null) {
			return "";
		}
		return (getDataBD(dataConverter, ""));
	}

	public static String getData2Digitos(java.util.Date dataConverter) {
		return (getDataBD2Digitos(dataConverter, ""));
	}

	public static java.util.Date getDateHoraFinalDia(Date dataPrm) {
		try {
			// java.util.Date valorData = null;
			// DateFormat formatador =
			// DateFormat.getDateInstance(DateFormat.SHORT);
			// valorData = formatador.parse(Uteis.getData(dataPrm));
			Calendar cal = Calendar.getInstance();

			cal.setTime(dataPrm);
			cal.set(Calendar.HOUR_OF_DAY, 23);
			cal.set(Calendar.MINUTE, 59);
			cal.set(Calendar.SECOND, 59);

			return cal.getTime();
		} catch (Exception e) {
			return new Date();
		}
	}

	public static java.util.Date getDateSemHora(Date dataPrm) throws Exception {
		java.util.Date valorData = null;
		DateFormat formatador = DateFormat.getDateInstance(DateFormat.SHORT);
		valorData = formatador.parse(Uteis.getData(dataPrm));
		Calendar cal = Calendar.getInstance();

		cal.setTime(valorData);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);

		return cal.getTime();
	}

	public static Date getDataMinutos(Date data) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(data);
		cal.set(Calendar.HOUR, 10);
		return cal.getTime();
	}

	public static Date getDataMinutosHoraDia(Date data) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(data);
		cal.set(Calendar.HOUR_OF_DAY, 10);
		return cal.getTime();
	}

	public static java.util.Date getDate(String data) throws Exception {
		java.util.Date valorData = null;
		DateFormat formatador = DateFormat.getDateInstance(DateFormat.SHORT);
		valorData = formatador.parse(data);
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		int hora = cal.get(Calendar.HOUR_OF_DAY);
		int minuto = cal.get(Calendar.MINUTE);
		int segundo = cal.get(Calendar.SECOND);

		cal.setTime(valorData);
		cal.set(Calendar.HOUR_OF_DAY, hora);
		cal.set(Calendar.MINUTE, minuto);
		cal.set(Calendar.SECOND, segundo);

		return cal.getTime();
	}

	/**
	 * MÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬
	 * Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€
	 * ™ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã‚Â ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å�
	 * �Ã‚Â¬ÃƒÂ¢Ã¢â‚�
	 * �Å¾Ã‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€�
	 * �Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚�
	 * �Å¡Ã‚Â¬Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ€š
	 * Ã‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â�
	 * �ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ�
	 * �Ã…Â¾Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã�
	 * �â€™Ãƒâ€ Ã¢â‚¬
	 * â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã�
	 * �â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã�
	 * �Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ€šÃ‚Â�
	 * �ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬
	 * â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚�
	 * �Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â�
	 * �ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€�
	 * �Â¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â©todo que recebe
	 * uma data como String, e a retorna como um Date, considerando o ultimo dia
	 * do
	 * mÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚�
	 * �Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€
	 * ™ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã‚Â ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å�
	 * �Ã‚Â¬ÃƒÂ¢Ã¢â‚�
	 * �Å¾Ã‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€�
	 * �Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚�
	 * �Å¡Ã‚Â¬Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ€š
	 * Ã‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â�
	 * �ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ�
	 * �Ã…Â¾Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã�
	 * �â€™Ãƒâ€ Ã¢â‚¬
	 * â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã�
	 * �â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã�
	 * �Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ€šÃ‚Â�
	 * �ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬
	 * â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚�
	 * �Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â�
	 * �ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€�
	 * �Â¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Âªs. Ou seja, ao
	 * receber uma data do tipo 31/02/2011, ele nunca
	 * retornarÃƒÆ’Ã†â€
	 * ™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚�
	 * �Â ÃƒÂ¢Ã¢â€�
	 * �Â¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã‚Â 
	 * ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÂ¢Ã¢â‚�
	 * �Å¾Ã‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬
	 * â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚�
	 * �Å¡Ã‚Â¬Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ€š
	 * Ã‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â�
	 * �ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ�
	 * �Ã…Â¾Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã�
	 * �â€™Ãƒâ€ Ã¢â‚¬
	 * â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã�
	 * �â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã�
	 * �Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ€šÃ‚Â�
	 * �ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬
	 * â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚�
	 * �Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â�
	 * �ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€�
	 * �Â¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¡ 03/03/2011, e
	 * sim o
	 * ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚�
	 * �Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€
	 * ™ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã‚Â ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å�
	 * �Ã‚Â¬ÃƒÂ¢Ã¢â‚�
	 * �Å¾Ã‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€�
	 * �Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚�
	 * �Å¡Ã‚Â¬Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ€š
	 * Ã‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â�
	 * �ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ�
	 * �Ã…Â¾Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã�
	 * �â€™Ãƒâ€ Ã¢â‚¬
	 * â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã�
	 * �â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã�
	 * �Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ€šÃ‚Â�
	 * �ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬
	 * â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚�
	 * �Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â�
	 * �ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€�
	 * �Â¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Âºltimo dia do
	 * mÃƒÆ’Ã�
	 * �â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â�
	 * �ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã‚Â�
	 * �ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÂ¢Ã¢â‚�
	 * �Å¾Ã‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬
	 * â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚�
	 * �Å¡Ã‚Â¬Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ€š
	 * Ã‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â�
	 * �ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ�
	 * �Ã…Â¾Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã�
	 * �â€™Ãƒâ€ Ã¢â‚¬
	 * â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã�
	 * �â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã�
	 * �Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ€šÃ‚Â�
	 * �ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬
	 * â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚�
	 * �Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â�
	 * �ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€�
	 * �Â¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Âªs 02,
	 * 28/02/2011 ou 29/02/2011, dependendo do ano.
	 * 
	 * @author Murillo Parreira
	 * @param data
	 *            String da data.
	 * @return dataConsiderandoUltimoDiaDoMes Date da data.
	 * @throws Exception
	 */
	public static java.util.Date getDataConsiderandoUltimoDiaDoMes(String data) throws Exception {
		Integer ano = Integer.valueOf(data.substring(data.lastIndexOf("/") + 1, data.length()));
		Integer mes = Integer.valueOf(data.substring(data.indexOf("/") + 1, data.lastIndexOf("/")));
		Integer dia = Integer.valueOf(data.substring(0, data.indexOf("/")));
		Calendar cal = new GregorianCalendar(ano, mes - 1, 1);
		Integer ultimoDiaDoMes = Integer.valueOf(cal.getActualMaximum(Calendar.DAY_OF_MONTH));
		if (dia > ultimoDiaDoMes) {
			dia = ultimoDiaDoMes;
		}
		String dataConsiderandoUltimoDiaDoMes = dia.toString() + "/" + mes.toString() + "/" + ano.toString();
		SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy");
		return formatador.parse(dataConsiderandoUltimoDiaDoMes);
	}

	public static java.util.Date getDate(String data, Locale local) throws Exception {
		java.util.Date valorData = new Date();
		if (local == null) {
			DateFormat formatador = DateFormat.getDateInstance(DateFormat.SHORT);
			valorData = formatador.parse(data);
		} else {
			DateFormat formatador = DateFormat.getDateInstance(DateFormat.SHORT, local);
			valorData = formatador.parse(data);
		}
		return valorData;
	}

	public static String getSemestreAtual() {
		GregorianCalendar gc = new GregorianCalendar();
		int mes = gc.get(GregorianCalendar.MONTH) + 1;
		if (mes > 7) {
			return "2";
		} else if (mes == 7) {
			int dia = getDiaMesData(new Date());
			if (dia <= 20) {
				return "1";
			} else {
				return "2";
			}
		} else {
			return "1";
		}
	}

	public static String getSemestreData(Date data) {
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(data);
		int mes = gc.get(GregorianCalendar.MONTH) + 1;
		if (mes > 7) {
			return "2";
		} else if (mes == 7) {
			int dia = getDiaMesData(data);
			if (dia <= 20) {
				return "1";
			} else {
				return "2";
			}
		} else {
			return "1";
		}
	}

	/**
	 * Esta rotina Ã© capaz de calcular, por exmeplo, que serÃ¡ o dÃ©cimo
	 * dia Ãºtil do mÃªs. Ou seja calcula uma data especÃ­fica, utilizando
	 * com referÃªncia uma quantidade de dias Ãºteis para frente. Dado uma
	 * data, o sistema Ã© capaz de calcular 10 dias (nrDiasUteisCalcular)
	 * Ãºtis para frente desta data. JÃ¡ descontando, sÃ¡bados e domingos.
	 * 
	 * @param dataInicial
	 * @param nrDiasUteisCalcular
	 * @return
	 * @throws Exception
	 */
	public static Date getDataAvancandoNumeroEspecificoDiasUteisInicioMes(Date dataVctoBase, Integer nrDiasUteisAvancar) throws Exception {
		Calendar calInicial = Calendar.getInstance();
		calInicial.setTime(Uteis.getDataPrimeiroDiaMes(dataVctoBase));
		calInicial.set(Calendar.HOUR, 0);
		calInicial.set(Calendar.MINUTE, 0);
		calInicial.set(Calendar.SECOND, 0);
		calInicial.set(Calendar.MILLISECOND, 0);

		Integer diasUteisComputados = 1;
		int nrDiasAvancados = 0;
		// Nao faz sentido avancar mais do que trinta dias, pois nÃ£o existem
		// 30 dias uteis no mes.
		for (int i = 1; ((diasUteisComputados < nrDiasUteisAvancar) && (nrDiasAvancados < 30)); calInicial.add(Calendar.DATE, i)) {
			if ((calInicial.get(Calendar.DAY_OF_WEEK) != 1) && (calInicial.get(Calendar.DAY_OF_WEEK) != 7)) { // sabado
																												// ou
																												// domingo
				diasUteisComputados++;
			}
			if (calInicial.getTime().compareTo(dataVctoBase) >= 0) {
				return dataVctoBase;
			}
			nrDiasAvancados++;
		}
		return calInicial.getTime();
	}

	public static Integer getCalculaDiasUteis(Date dataInicial, Date dataFinal) throws Exception {
		Calendar calInicial = Calendar.getInstance();
		Calendar calFinal = Calendar.getInstance();
		calInicial.setTime(dataInicial);
		calFinal.setTime(dataFinal);
		calInicial.set(Calendar.HOUR, 0);
		calInicial.set(Calendar.MINUTE, 0);
		calInicial.set(Calendar.SECOND, 0);
		calInicial.set(Calendar.MILLISECOND, 0);
		calFinal.set(Calendar.HOUR, 0);
		calFinal.set(Calendar.MINUTE, 0);
		calFinal.set(Calendar.SECOND, 0);
		calFinal.set(Calendar.MILLISECOND, 0);
		Integer diasUteis = 0;
		Integer dias = 0;
		for (int i = 1; calInicial.compareTo(calFinal) <= 0; calInicial.add(Calendar.DATE, i)) {
			if (calInicial.get(Calendar.DAY_OF_WEEK) != 1 && calInicial.get(Calendar.DAY_OF_WEEK) != 7) { // sabado
																											// ou
																											// domingo
				diasUteis++;
			}
			dias++;
		}

		return diasUteis;
	}

	public static Integer getCalculaDiasUteisConsiderandoSabado(Date dataInicial, Date dataFinal) throws Exception {
		Integer diasUteis = 0;
		Integer diaSemana = 0;
		while (getDataJDBC(dataInicial).compareTo(getDataJDBC(dataFinal)) <= 0) {
			diaSemana = Uteis.getDiaSemana(dataInicial);
			if (diaSemana != 1) { // sabado ou domingo
				diasUteis++;
			}
			dataInicial = Uteis.obterDataAvancada(dataInicial, 1);
		}
		return diasUteis;
	}

	public static Integer getCalculaDiasNaoUteis(Date dataInicial, Date dataFinal) throws Exception {

		Integer diasNaoUteis = 0;
		Integer diaSemana = 0;
		while (getDataJDBC(dataInicial).compareTo(getDataJDBC(dataFinal)) <= 0) {
			diaSemana = Uteis.getDiaSemana(dataInicial);
			if (diaSemana == 1 || diaSemana == 7) { // sabado ou domingo
				diasNaoUteis++;
			}
			dataInicial = Uteis.obterDataAvancada(dataInicial, 1);
		}
		return diasNaoUteis;
	}

	public static String getAnoDataAtual() {
		return getData(new Date(), "yyyy");
	}

	public static String getAno(Date data) {
		return getData(data, "yyyy");
	}

	public static String getDataAtual() {
		Date hoje = new Date();
		return (Uteis.getData(hoje));
	}

	public static String getHoraAtual() {
		return getData(new Date(), "HH:mm");
	}

	public static Date getDateTime(Date data, int hora, int minuto, int segundo) {
		Calendar cal = Calendar.getInstance();
		if (data == null) {
			data = new Date();
		}
		cal.setTime(data);
		cal.set(Calendar.HOUR_OF_DAY, hora);
		cal.set(Calendar.MINUTE, minuto);
		cal.set(Calendar.SECOND, segundo);

		return cal.getTime();
	}

	public static String obterDataFormatoTextoddMMyyyy(Date data) {
		try {
			return getData(data, "dd/MM/yyyy");
		} catch (Exception e) {
			return "";
		}
	}

	public static String obterDiferencaHorasDuasDatas(Date dataInicial, Date dataFinal) {
		long diferenca = dataFinal.getTime() - dataInicial.getTime();
		return (diferenca / 1000 / 60 / 60) + ":" + (diferenca / 1000 / 60) + ":" + (diferenca / 1000);
	}

	public static String obterDataFormatoTextoddMMyy(Date data) {
		try {
			return getData(data, "dd/MM/yy");
		} catch (Exception e) {
			return "";
		}
	}

	public static String obterPrimeiroNomeConcatenadoSobreNome(String nome, Integer qtdeSobrenome) {
		try {
			List<String> preposicoes2 = new ArrayList(0);
			preposicoes2.add("dos");
			preposicoes2.add("das");
			preposicoes2.add("de");
			preposicoes2.add("da");
			preposicoes2.add("e");
			preposicoes2.add("a");
			preposicoes2.add("i");
			preposicoes2.add("o");
			String[] listaNome = nome.trim().split(" ");
			StringBuilder resultado = new StringBuilder();
			for (int i = 0; i < listaNome.length; i++) {

				String s = listaNome[i];
				if (s.trim().isEmpty()) {
					continue;
				}
				if (i == 0) {
					resultado.append(s);
				} else if (preposicoes2.contains(s)) {
					resultado.append("");
				} else {
					resultado.append(s.subSequence(0, 1)).append(".");
				}
				resultado.append(" ");
				if (i == qtdeSobrenome) {
					return resultado.toString();
				}

			}
			return resultado.toString();
		} catch (Exception e) {
		}
		return "";
	}

	public static String obterDataFormatoTextoddMM(Date data) {
		try {
			return getData(data, "dd/MM");
		} catch (Exception e) {
			return "";
		}
	}

	public static String getHoraMinutoComMascara(Date data) {
		if (data == null) {
			return "";
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(data);
		String hora = String.valueOf(cal.get(Calendar.HOUR_OF_DAY));
		String minuto = String.valueOf(cal.get(Calendar.MINUTE));
		if (hora.length() == 1) {
			hora = "0" + hora;
		}
		if (minuto.length() == 1) {
			minuto = "0" + minuto;
		}

		return hora + ":" + minuto;
	}

	public static String gethoraHHMMSS(Date data) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(data);
		String hora = String.valueOf(cal.get(Calendar.HOUR_OF_DAY));
		String minuto = String.valueOf(cal.get(Calendar.MINUTE));
		String segundo = String.valueOf(cal.get(Calendar.SECOND));

		return hora + minuto + segundo;
	}

	public static String gethoraHHMMSS(String tempo) {

		String hora = tempo.substring(0, 2);
		String minuto = tempo.substring(3, 5);
		Integer hora1 = Integer.parseInt(hora);
		Integer minuto1 = Integer.parseInt(minuto);
		if (hora1 > 23) {
			tempo = "";
		}
		if (minuto1 > 59) {
			tempo = "";
		}
		return tempo;
	}

	public static String somarHorario(String horario, Integer minuto) {
		String hh = horario.substring(0, 2);
		String mm = horario.substring(3, 5);
		Integer hhi = Integer.parseInt(hh);
		Integer mmi = Integer.parseInt(mm);

		mmi += minuto;

		while (mmi > 59) {
			mmi -= 60;
			hhi += 1;
		}
		while (hhi > 23) {
			hhi -= 24;
			hhi += 1;
		}

		if (mmi.intValue() == 0) {
			mm = "00";

		} else if (mmi.intValue() == 1) {
			mm = "01";

		} else if (mmi.intValue() == 2) {
			mm = "02";

		} else if (mmi.intValue() == 3) {
			mm = "03";

		} else if (mmi.intValue() == 4) {
			mm = "04";

		} else if (mmi.intValue() == 5) {
			mm = "05";

		} else if (mmi.intValue() == 6) {
			mm = "06";

		} else if (mmi.intValue() == 7) {
			mm = "07";

		} else if (mmi.intValue() == 8) {
			mm = "08";

		} else if (mmi.intValue() == 9) {
			mm = "09";

		} else {
			mm = String.valueOf(mmi);
		}

		if (hhi.intValue() == 0) {
			hh = "00";

		} else if (hhi.intValue() == 1) {
			hh = "01";

		} else if (hhi.intValue() == 2) {
			hh = "02";

		} else if (hhi.intValue() == 3) {
			hh = "03";

		} else if (hhi.intValue() == 4) {
			hh = "04";

		} else if (hhi.intValue() == 5) {
			hh = "05";

		} else if (hhi.intValue() == 6) {
			hh = "06";

		} else if (hhi.intValue() == 7) {
			hh = "07";

		} else if (hhi.intValue() == 8) {
			hh = "08";

		} else if (hhi.intValue() == 9) {
			hh = "09";

		} else {
			hh = String.valueOf(hhi);
		}

		return hh + ":" + mm;
	}

	public static String retirarSinaisSimbolosEspacoString(String label) {
		label = label.replaceAll(" ", "");
		label = label.replaceAll("-", "");
		label = label.replaceAll(",", "");
		label = label.replaceAll("_", "");
		return label;
	}

	public static String retirarAcentuacao(String prm) {
		String nova = "";
		for (int i = 0; i < prm.length(); i++) {
			if (prm.charAt(i) == Uteis.A_AGUDO || prm.charAt(i) == Uteis.A_CIRCUNFLEXO || prm.charAt(i) == Uteis.A_CRASE || prm.charAt(i) == Uteis.A_TIO) {
				nova += "a";
			} else if (prm.charAt(i) == Uteis.A_AGUDOMAIUSCULO) {
				nova += "A";
			} else if (prm.charAt(i) == Uteis.E_AGUDO || prm.charAt(i) == Uteis.E_CIRCUNFLEXO) {
				nova += "e";
			} else if (prm.charAt(i) == Uteis.I_AGUDO) {
				nova += "i";
			} else if (prm.charAt(i) == Uteis.O_AGUDO || prm.charAt(i) == Uteis.O_TIO || prm.charAt(i) == Uteis.O_CRASE) {
				nova += "o";
			} else if (prm.charAt(i) == Uteis.U_AGUDO || prm.charAt(i) == Uteis.U_TREMA) {
				nova += "u";
			} else if (prm.charAt(i) == Uteis.C_CEDILHA) {
				nova += "c";
				// } else if (Character.isSpaceChar(prm.charAt(i))){
				// nova += "_";
			} else {
				nova += prm.charAt(i);
			}
		}
		return (nova);
	}

	public static String retirarAcentuacaoAndCaracteresEspeciasRegex(String texto) {
		texto = texto.replaceAll(UteisJSF.internacionalizar("prt_Letra_A"), "A");
		texto = texto.replaceAll(UteisJSF.internacionalizar("prt_Letra_a"), "a");
		texto = texto.replaceAll(UteisJSF.internacionalizar("prt_Letra_E"), "E");
		texto = texto.replaceAll(UteisJSF.internacionalizar("prt_Letra_e"), "e");
		texto = texto.replaceAll(UteisJSF.internacionalizar("prt_Letra_I"), "I");
		texto = texto.replaceAll(UteisJSF.internacionalizar("prt_Letra_i"), "i");
		texto = texto.replaceAll(UteisJSF.internacionalizar("prt_Letra_O"), "O");
		texto = texto.replaceAll(UteisJSF.internacionalizar("prt_Letra_o"), "o");
		texto = texto.replaceAll(UteisJSF.internacionalizar("prt_Letra_U"), "U");
		texto = texto.replaceAll(UteisJSF.internacionalizar("prt_Letra_u"), "u");
		texto = texto.replaceAll(UteisJSF.internacionalizar("prt_Letra_C"), "C");
		texto = texto.replaceAll(UteisJSF.internacionalizar("prt_Letra_c"), "c");
		texto = texto.replaceAll(UteisJSF.internacionalizar("prt_Letra_y"), "y");
		texto = texto.replaceAll(UteisJSF.internacionalizar("prt_Letra_Y"), "Y");
		texto = texto.replaceAll(UteisJSF.internacionalizar("prt_Letra_n"), "n");
		texto = texto.replaceAll(UteisJSF.internacionalizar("prt_Letra_N"), "N");
		texto = texto.replaceAll(UteisJSF.internacionalizar("prt_Letra_D"), "D");
		texto = texto.replaceAll(UteisJSF.internacionalizar("prt_Letra_Esp1"), "");
		texto = texto.replaceAll(UteisJSF.internacionalizar("prt_Letra_Esp2"), "");
		texto = texto.replaceAll(UteisJSF.internacionalizar("prt_Letra_Esp3"), "");

		texto = texto.replaceAll(UteisJSF.internacionalizar("prt_Letra_Esp4"), "a");// acento
																					// ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¡
		texto = texto.replaceAll(UteisJSF.internacionalizar("prt_Letra_Esp5"), "a");// acento
																					// ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â£
		texto = texto.replaceAll(UteisJSF.internacionalizar("prt_Letra_Esp6"), "A");// acento
																					// ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â£
		texto = texto.replaceAll(UteisJSF.internacionalizar("prt_Letra_Esp7"), "a");// acento
																					// ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â£
		texto = texto.replaceAll(UteisJSF.internacionalizar("prt_Letra_Esp8"), "a");// acento
																					// ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¢

		texto = texto.replaceAll(UteisJSF.internacionalizar("prt_Letra_Esp9"), "a");// acento
																					// ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¡
		texto = texto.replaceAll(UteisJSF.internacionalizar("prt_Letra_Esp10"), "a");// acento
																						// ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â 
		texto = texto.replaceAll(UteisJSF.internacionalizar("prt_Letra_Esp11"), "e");// acento
																						// ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â©
		texto = texto.replaceAll(UteisJSF.internacionalizar("prt_Letra_Esp12"), "e");// acento
																						// ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Âª
		texto = texto.replaceAll(UteisJSF.internacionalizar("prt_Letra_Esp13"), "e");// acento
																						// ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¨
		texto = texto.replaceAll(UteisJSF.internacionalizar("prt_Letra_Esp14"), "i");// acento
																						// ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â­
		texto = texto.replaceAll(UteisJSF.internacionalizar("prt_Letra_Esp15"), "i");// acento
																						// ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¬
		texto = texto.replaceAll(UteisJSF.internacionalizar("prt_Letra_Esp16"), "i");// acento
																						// ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â®
		texto = texto.replaceAll(UteisJSF.internacionalizar("prt_Letra_Esp17"), "o");// acento
																						// ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â³
		texto = texto.replaceAll(UteisJSF.internacionalizar("prt_Letra_Esp18"), "o");// acento
																						// ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â²
		texto = texto.replaceAll(UteisJSF.internacionalizar("prt_Letra_Esp19"), "o");// acento
																						// ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Âµ
		texto = texto.replaceAll(UteisJSF.internacionalizar("prt_Letra_Esp20"), "o");// acento
																						// ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â´
		texto = texto.replaceAll(UteisJSF.internacionalizar("prt_Letra_Esp21"), "u");// acento
																						// ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Âº
		texto = texto.replaceAll(UteisJSF.internacionalizar("prt_Letra_Esp22"), "u");// acento
																						// ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¹
		texto = texto.replaceAll(UteisJSF.internacionalizar("prt_Letra_Esp23"), "u");// acento
																						// ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â»
		texto = texto.replaceAll(UteisJSF.internacionalizar("prt_Letra_Esp24"), "c");// acento
																						// ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â§
		texto = texto.replaceAll(UteisJSF.internacionalizar("prt_Letra_Esp25"), "C");// acento
																						// ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â§
		texto = texto.replaceAll(UteisJSF.internacionalizar("prt_Letra_Esp26"), "c");// acento
																						// ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â§
		texto = texto.replaceAll(UteisJSF.internacionalizar("prt_Letra_Esp27"), "");
		texto = texto.replaceAll("" + Uteis.A_AGUDO, "a");
		texto = texto.replaceAll("" + Uteis.A_AGUDOMAIUSCULO, "A");
		texto = texto.replaceAll("" + Uteis.A_CIRCUNFLEXO, "a");
		texto = texto.replaceAll("" + Uteis.A_CRASE, "a");
		texto = texto.replaceAll("" + Uteis.A_TIO, "a");
		texto = texto.replaceAll("" + Uteis.C_CEDILHA, "c");
		texto = texto.replaceAll("" + Uteis.E_AGUDO, "e");
		texto = texto.replaceAll("" + Uteis.E_CIRCUNFLEXO, "e");
		texto = texto.replaceAll("" + Uteis.I_AGUDO, "i");
		texto = texto.replaceAll("" + Uteis.O_AGUDO, "o");
		texto = texto.replaceAll("" + Uteis.O_CRASE, "o");
		texto = texto.replaceAll("" + Uteis.O_TIO, "o");
		texto = texto.replaceAll("" + Uteis.U_AGUDO, "u");
		texto = texto.replaceAll("" + Uteis.U_TREMA, "u");
		// texto =
		// texto.replaceAll(UteisJSF.internacionalizar("prt_Letra_Esp27"),
		// "c");//acento
		// ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â§
		// texto =
		// texto.replaceAll(UteisJSF.internacionalizar("prt_Letra_Esp28"),
		// "C");//acento
		// ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â§
		// texto =
		// texto.replaceAll(UteisJSF.internacionalizar("prt_Letra_Esp29"),
		// "a");//acento
		// ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â§
		// texto =
		// texto.replaceAll(UteisJSF.internacionalizar("prt_Letra_Esp30"),
		// "C");//acento
		// ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â§
		// texto =
		// texto.replaceAll(UteisJSF.internacionalizar("prt_Letra_Esp31"),
		// "O");//acento
		// ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â§

		return texto;
	}

	public static String getFormatoHoraMinuto(String valor) {
		if (valor.length() > 5) {
			return "";
		} else {
			if (valor.length() == 1) {
				return "0" + valor + ":00";
			} else if (valor.length() == 2) {
				return valor + ":00";
			} else if (valor.length() == 3) {
				return valor + "00";
			} else if (valor.length() == 4) {
				return valor + "0";
			} else {
				return valor;
			}
		}
	}

	public static String getCalculodeHora(String tempo, String tempo1, String tempo2, Integer nrAulas, Integer duracao) {
		Double resultado;
		resultado = (double) duracao * nrAulas;
		int res1 = (int) (resultado / 60);
		double res2 = ((resultado % 60));

		String hora = tempo.substring(0, 2);
		String minuto = tempo.substring(3, 5);
		Integer hora1 = Integer.parseInt(hora);
		Integer minuto1 = Integer.parseInt(minuto);

		String intervalohora = tempo1.substring(0, 2);
		String intervalominuto = tempo1.substring(3, 5);
		Integer intervalohora1 = Integer.parseInt(intervalohora);
		Integer intervalominuto1 = Integer.parseInt(intervalominuto);

		String intervalofinalhora = tempo2.substring(0, 2);
		String intervalofinalminuto = tempo2.substring(3, 5);
		Integer intervalofinalhora1 = Integer.parseInt(intervalofinalhora);
		Integer intervalofinalminuto1 = Integer.parseInt(intervalofinalminuto);

		intervalohora1 = intervalofinalhora1 - intervalohora1;
		intervalominuto1 = intervalofinalminuto1 - intervalominuto1;
		hora1 = hora1 + res1 + intervalohora1;
		minuto1 = (int) (minuto1 + res2 + intervalominuto1);

		if (hora1 > 23) {
			hora1 = hora1 - 23;
			hora1 = hora1 - 1;
		}
		if (minuto1 > 59) {
			minuto1 = minuto1 - 60;
			hora1 = hora1 + 1;
			minuto1 = minuto1 + 0;
		}

		if (minuto1.intValue() == 0) {
			tempo = String.valueOf(hora1) + ":00";

		} else if (minuto1.intValue() == 1) {
			tempo = String.valueOf(hora1) + ":01";

		} else if (minuto1.intValue() == 2) {
			tempo = String.valueOf(hora1) + ":02";

		} else if (minuto1.intValue() == 3) {
			tempo = String.valueOf(hora1) + ":03";

		} else if (minuto1.intValue() == 4) {
			tempo = String.valueOf(hora1) + ":04";

		} else if (minuto1.intValue() == 5) {
			tempo = String.valueOf(hora1) + ":05";

		} else if (minuto1.intValue() == 6) {
			tempo = String.valueOf(hora1) + ":06";

		} else if (minuto1.intValue() == 7) {
			tempo = String.valueOf(hora1) + ":07";

		} else if (minuto1.intValue() == 8) {
			tempo = String.valueOf(hora1) + ":08";

		} else if (minuto1.intValue() == 9) {
			tempo = String.valueOf(hora1) + ":09";

		} else {
			tempo = String.valueOf(hora1) + ":" + String.valueOf(minuto1);
		}

		return tempo;

	}

	public static String getCalculodeHoraSemIntervalo(String tempo, Integer nrAulas, Integer duracao) {
		Double resultado;
		resultado = (double) duracao * nrAulas;
		int res1 = (int) (resultado / 60);
		double res2 = ((resultado % 60));

		String hora = tempo.substring(0, 2);
		String minuto = tempo.substring(3, 5);
		Integer hora1 = Integer.parseInt(hora);
		Integer minuto1 = Integer.parseInt(minuto);

		hora1 = hora1 + res1;
		minuto1 = (int) (minuto1 + res2);

		if (hora1 > 23) {
			hora1 = hora1 - 23;
			hora1 = hora1 - 1;
		}
		if (minuto1 > 59) {
			minuto1 = minuto1 - 60;
			hora1 = hora1 + 1;
			minuto1 = minuto1 + 0;
		}

		if (minuto1 >= 0 && minuto1 <= 9) {
			minuto = "0" + String.valueOf(minuto1);
		} else {
			minuto = String.valueOf(minuto1);
		}

		if (hora1 >= 0 && hora1 <= 9) {
			hora = "0" + String.valueOf(hora1);
		} else {
			hora = String.valueOf(hora1);
		}

		tempo = hora + ":" + minuto;
		return tempo;

	}

	public static double arredondar(double valor, int casas, int abaixo) {
		valor = (new BigDecimal(valor).setScale(casas, BigDecimal.ROUND_HALF_UP)).doubleValue();
		return valor;
		/*
		 * double arredondado = valor; arredondado *= (Math.pow(10, casas)); if
		 * (abaixo == 0) { arredondado = Math.ceil(arredondado); } else {
		 * arredondado = Math.floor(arredondado); } arredondado /= (Math.pow(10,
		 * casas)); return arredondado;
		 */
	}

	public static double arredondarAbaixo(double valor, int casas, int abaixo) {
		valor = (new BigDecimal(valor).setScale(casas, BigDecimal.ROUND_DOWN)).doubleValue();
		return valor;
		/*
		 * double arredondado = valor; arredondado *= (Math.pow(10, casas)); if
		 * (abaixo == 0) { arredondado = Math.ceil(arredondado); } else {
		 * arredondado = Math.floor(arredondado); } arredondado /= (Math.pow(10,
		 * casas)); return arredondado;
		 */
	}

	public static double arrendondarForcando2CadasDecimais(double valor) {
		boolean negativo = false;
		if (valor < 0) {
			negativo = true;
		}
		valor = Uteis.arredondar(valor, 2, 1);
		String valorStr = String.valueOf(valor);

		String inteira = valorStr.substring(0, valorStr.indexOf("."));
		String extensao = valorStr.substring(valorStr.indexOf(".") + 1, valorStr.length());
		if (extensao.length() == 1) {
			extensao += "0";
		}
		valorStr = Uteis.removerMascara(inteira) + "." + extensao;
		Double valorFinal = Double.parseDouble(valorStr);
		if (negativo) {
			valorFinal = valorFinal * -1;
		}
		return valorFinal;
	}

	public static String arrendondarForcando2CadasDecimaisStr(double valor) {
		boolean negativo = false;
		if (valor < 0) {
			negativo = true;
		}
		if (negativo) {
			valor = valor * -1;
		}
		valor = Uteis.arredondar(valor, 2, 1);
		String valorStr = String.valueOf(valor);

		String inteira = valorStr.substring(0, valorStr.indexOf("."));
		String extensao = valorStr.substring(valorStr.indexOf(".") + 1, valorStr.length());
		if (extensao.length() == 1) {
			extensao += "0";
		}
		valorStr = Uteis.removerMascara(inteira) + "." + extensao;
		return valorStr;
	}

	public static String formatarDoubleParaMoeda(double num) {
		NumberFormat number = NumberFormat.getCurrencyInstance();
		return number.format(num);
	}

	public static Date obterDataFuturaMesContabilConsiderandoDiaInicial(Date dataInicial, int nrTotalDiasProgredir) throws Exception {
		if (dataInicial == null) {
			return null;
		}
		int nrMesesProgredir = nrTotalDiasProgredir / 30;
		int nrDiasProgredir = nrTotalDiasProgredir - (30 * nrMesesProgredir);

		int dia = Uteis.getDiaMesData(dataInicial);
		int mes = Uteis.getMesData(dataInicial);
		int ano = Uteis.getAnoData(dataInicial);

		// PROGREDINDO OS ANOS
		if (nrMesesProgredir > 12) {
			while (nrMesesProgredir > 12) {
				ano++;
				nrMesesProgredir += -12;
			}
		}

		// PROGREDINDO OS MESES
		mes += nrMesesProgredir;
		if (mes > 12) {
			mes -= 12;
			ano++;
		}

		// PROGREDINDO OS DIAS;
		boolean incrementarMes = false;
		if ((dia + nrDiasProgredir) > 30) {
			dia = (dia + nrDiasProgredir) - 30;
			incrementarMes = true;
		} else {
			dia = dia + nrDiasProgredir;
		}

		if (incrementarMes) {
			mes++;
			if (mes == 13) {
				mes = 1;
				ano++;
			}
		}
		dia--;
		Date dataFutura = Uteis.getDate(dia + "/" + mes + "/" + ano);
		return dataFutura;
	}

	public static long nrDiasEntreDatas(Date dataInicial, Date dataFinal) {
		long dias = 0;
		if (dataInicial != null && dataFinal != null) {
			dias = (dataInicial.getTime() - dataFinal.getTime()) / (1000 * 60 * 60 * 24);
		}
		return dias;
	}

	public static Date obterDataPassada(Date dataInicial, long nrDiasRegredir) {
		long dataEmDias = dataInicial.getTime() / (1000 * 60 * 60 * 24);
		dataEmDias = dataEmDias - nrDiasRegredir;
		Date dataFutura = new Date(dataEmDias * (1000 * 60 * 60 * 24));
		return dataFutura;
	}

	public static Date obterDataAvancadaPorDiaPorMesPorAno(Integer dia, Integer mes, Integer ano, Integer nrMesRegredir) {
		Calendar calendar = Calendar.getInstance();
		try {
			calendar.set(Calendar.DAY_OF_MONTH, dia);
			calendar.set(Calendar.MONTH, (mes - 1));
			calendar.set(Calendar.YEAR, ano);
			calendar.add(Calendar.DAY_OF_MONTH, nrMesRegredir);
			return calendar.getTime();
		} finally {
			calendar = null;
		}
	}

	/**
	 * MÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬
	 * Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€
	 * ™ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã‚Â ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å�
	 * �Ã‚Â¬ÃƒÂ¢Ã¢â‚�
	 * �Å¾Ã‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€�
	 * �Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚�
	 * �Å¡Ã‚Â¬Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ€š
	 * Ã‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â�
	 * �ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ�
	 * �Ã…Â¾Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã�
	 * �â€™Ãƒâ€ Ã¢â‚¬
	 * â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã�
	 * �â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã�
	 * �Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ€šÃ‚Â�
	 * �ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬
	 * â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚�
	 * �Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â�
	 * �ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€�
	 * �Â¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â©todo que recebe
	 * uma data base e calcula a data final com base em uma quantidade de dias a
	 * regredir.
	 * 
	 * @param dataBase
	 * @param nrDiasRegredir
	 * @return
	 */
	public static Date obterDataAntigaPorMes(Date dataBase, Integer nrMesRegredir) {
		Calendar calendar = new GregorianCalendar();
		try {
			nrMesRegredir = (-1) * nrMesRegredir;
			calendar.setTime(dataBase);
			calendar.add(Calendar.MONTH, nrMesRegredir);
			return calendar.getTime();
		} finally {
			calendar = null;
		}
	}

	public static Date obterDataAvancadaPorMes(Date dataBase, Integer nrMesRegredir) {
		Calendar calendar = new GregorianCalendar();
		try {
			calendar.setTime(dataBase);
			calendar.add(Calendar.MONTH, nrMesRegredir);
			return calendar.getTime();
		} finally {
			calendar = null;
		}
	}

	/**
	 * MÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬
	 * Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€
	 * ™ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã‚Â ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å�
	 * �Ã‚Â¬ÃƒÂ¢Ã¢â‚�
	 * �Å¾Ã‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€�
	 * �Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚�
	 * �Å¡Ã‚Â¬Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ€š
	 * Ã‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â�
	 * �ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ�
	 * �Ã…Â¾Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã�
	 * �â€™Ãƒâ€ Ã¢â‚¬
	 * â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã�
	 * �â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã�
	 * �Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ€šÃ‚Â�
	 * �ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬
	 * â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚�
	 * �Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â�
	 * �ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€�
	 * �Â¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â©todo que recebe
	 * uma data base e calcula a data final com base em uma quantidade de dias a
	 * regredir.
	 * 
	 * @param dataBase
	 * @param nrDiasRegredir
	 * @return
	 */
	public static Date obterDataAntiga(Date dataBase, Integer nrDiasRegredir) {
		Calendar calendar = new GregorianCalendar();
		try {
			nrDiasRegredir = (-1) * nrDiasRegredir;
			calendar.setTime(dataBase);
			calendar.add(Calendar.DAY_OF_MONTH, nrDiasRegredir);
			return calendar.getTime();
		} finally {
			calendar = null;
		}
	}

	public static String realizarSomahora(String minuto) {
		Calendar calendar = new GregorianCalendar();
		Date data = new Date();
		try {
			// calendar.setTime(dataBase);
			// calendar.add(Calendar.DAY_OF_MONTH, nrDiasProgredir);
			// return calendar.getTime();
		} finally {
			calendar = null;
		}
		return "";
	}

	/**
	 * MÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬
	 * Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€
	 * ™ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã‚Â ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å�
	 * �Ã‚Â¬ÃƒÂ¢Ã¢â‚�
	 * �Å¾Ã‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€�
	 * �Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚�
	 * �Å¡Ã‚Â¬Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ€š
	 * Ã‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â�
	 * �ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ�
	 * �Ã…Â¾Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã�
	 * �â€™Ãƒâ€ Ã¢â‚¬
	 * â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã�
	 * �â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã�
	 * �Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ€šÃ‚Â�
	 * �ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬
	 * â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚�
	 * �Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â�
	 * �ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€�
	 * �Â¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â©todo que recebe
	 * uma data base e calcula a data final com base em uma quantidade de dias a
	 * regredir.
	 * 
	 * @param dataBase
	 * @param nrDiasRegredir
	 * @return
	 */
	public static String obterHoraAvancada(String horaInicial, Integer minutos) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(horaInicial.substring(0, 2)));
		cal.set(Calendar.MINUTE, Integer.parseInt(horaInicial.substring(3, 5)));
		cal.set(Calendar.SECOND, 0);
		cal.add(Calendar.MINUTE, minutos);

		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		return sdf.format(cal.getTime()).toString();
	}

	public static String obterHoraRegredida(String horaInicial, Integer minutos) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(horaInicial.substring(0, 2)));
		cal.set(Calendar.MINUTE, Integer.parseInt(horaInicial.substring(3, 5)));
		cal.set(Calendar.SECOND, 0);
		cal.add(Calendar.MINUTE, -minutos);

		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		return sdf.format(cal.getTime()).toString();
	}

	public static Date obterDataAvancada(Date dataBase, Integer nrDiasProgredir) {
		Calendar calendar = new GregorianCalendar();
		try {
			calendar.setTime(dataBase);
			calendar.add(Calendar.DAY_OF_MONTH, nrDiasProgredir);
			return calendar.getTime();
		} finally {
			calendar = null;
		}
	}

	public static Date obterDataFutura(Date dataInicial, long nrDiasProgredir) {
		long dataEmDias = dataInicial.getTime() / (1000 * 60 * 60 * 24);
		dataEmDias = dataEmDias + nrDiasProgredir;
		Date dataFutura = new Date(dataEmDias * (1000 * 60 * 60 * 24));
		return dataFutura;
	}

	public static String removerEspacosFinalString(String str) {
		if (str == null) {
			return null;
		}
		if (str.equalsIgnoreCase("")) {
			return str;
		}
		String ultimoChar = str.substring(str.length() - 1);
		while ((ultimoChar.equals(" ")) && (str.length() > 0)) {
			str = str.substring(0, str.length() - 1);
			if (str.length() > 0) {
				ultimoChar = str.substring(str.length() - 1);
			}
		}
		return str;
	}

	public static String getMesReferenciaData(Date dataPrm) {
		Calendar dataCalendar = Calendar.getInstance();
		dataCalendar.setTime(dataPrm);

		int ano = dataCalendar.get(Calendar.YEAR);
		int mes = dataCalendar.get(Calendar.MONTH) + 1;

		String mesStr = String.valueOf(mes);
		if (mesStr.length() == 1) {
			mesStr = "0" + mesStr;
		}
		String mesRef = mesStr + "/" + ano;
		return mesRef;
	}

	public static String getMesReferencia(int mes, int ano) {
		String mesStr = String.valueOf(mes);
		if (mesStr.length() == 1) {
			mesStr = "0" + mesStr;
		}
		mesStr = mesStr + "/" + String.valueOf(ano);
		return mesStr;
	}

	public static String getMesReferenciaExtenso(String mes) {
		if (mes.equals("01") || mes.equals("1")) {
			return "Janeiro";
		}
		if (mes.equals("02") || mes.equals("2")) {
			return "Fevereiro";
		}
		if (mes.equals("03") || mes.equals("3")) {
			return "Mar�o";
		}
		if (mes.equals("04") || mes.equals("4")) {
			return "Abril";
		}
		if (mes.equals("05") || mes.equals("5")) {
			return "Maio";
		}
		if (mes.equals("06") || mes.equals("6")) {
			return "Junho";
		}
		if (mes.equals("07") || mes.equals("7")) {
			return "Julho";
		}
		if (mes.equals("08") || mes.equals("8")) {
			return "Agosto";
		}
		if (mes.equals("09") || mes.equals("9")) {
			return "Setembro";
		}
		if (mes.equals("10")) {
			return "Outubro";
		}
		if (mes.equals("11")) {
			return "Novembro";
		}
		if (mes.equals("12")) {
			return "Dezembro";
		}
		return "";
	}

	public static Integer getMesReferencia(String mes) {
		if (mes.equals("JANEIRO")) {
			return 1;
		}
		if (mes.equals("FEVEREIRO")) {
			return 2;
		}
		if (mes.equals("MAR�O") || mes.equals("MARCO")) {
			return 3;
		}
		if (mes.equals("ABRIL")) {
			return 4;
		}
		if (mes.equals("MAIO")) {
			return 5;
		}
		if (mes.equals("JUNHO")) {
			return 6;
		}
		if (mes.equals("JULHO")) {
			return 7;
		}
		if (mes.equals("AGOSTO")) {
			return 8;
		}
		if (mes.equals("SETEMBRO")) {
			return 9;
		}
		if (mes.equals("OUTUBRO")) {
			return 10;
		}
		if (mes.equals("NOVEMBRO")) {
			return 11;
		}
		if (mes.equals("DEZEMBRO")) {
			return 12;
		}
		return 0;
	}

	public static Integer getAnoPlanoOrcamentario(String mesAno) {
		int ano = Integer.parseInt(mesAno.substring(mesAno.lastIndexOf("- ") + 2));
		return ano;
	}

	public static int compareMesReferencia(String mesInicial, String mesFinal) {
		String mesInicialOrdenado = mesInicial.substring(mesInicial.indexOf("/") + 1) + mesInicial.substring(0, mesInicial.indexOf("/"));
		String mesFinalOrdenado = mesFinal.substring(mesFinal.indexOf("/") + 1) + mesFinal.substring(0, mesFinal.indexOf("/"));
		return mesInicialOrdenado.compareTo(mesFinalOrdenado);
	}

	public static String getDataDiaMesAnoConcatenado() {
		String dataAtual = Uteis.getDataAtual();
		String ano = "";
		String mes = "";
		String dia = "";
		int cont = 1;
		while (cont != 3) {
			int posicao = dataAtual.lastIndexOf("/");
			if (posicao != -1) {
				cont++;
				if (cont == 2) {
					ano = dataAtual.substring(posicao + 1);
					dataAtual = dataAtual.substring(0, posicao);
				} else if (cont == 3) {
					mes = dataAtual.substring(posicao + 1);
					dia = dataAtual.substring(0, posicao);
				}
			}
		}
		return dia + mes + ano;
	}
        
        public static String getDiaMesPorExtensoEAno(Date data, Boolean mesMinusculo) {
		String dataExt = "";
		if (data != null) {
			if (mesMinusculo == null || !mesMinusculo) {
				dataExt += getData(data, "dd 'de' MMMMM 'de' yyyy");
			} else {
				dataExt += (getData(data, "dd 'de' MMMMM 'de' yyyy")).toLowerCase();
			}
		}
		return dataExt;
	}        

	public static String getDataCidadeDiaMesPorExtensoEAno(String cidade, Date data, Boolean mesMinusculo) {
		return getDataCidadeDiaMesPorExtensoEAno(cidade, "", data, mesMinusculo);
	}
	
	public static String getDataCidadeDiaMesPorExtensoEAno(String cidade, String estado, Date data, Boolean mesMinusculo) {
		String dataExt = "";
		if (data != null) {
			if (!cidade.equals("")) {
				dataExt += cidade + ", ";
			}
			if (!estado.equals("")) {
				dataExt += estado + ", ";
			}
			if (mesMinusculo == null || !mesMinusculo) {
				dataExt += getData(data, "dd 'de' MMMMM 'de' yyyy");
			} else {
				dataExt += (getData(data, "dd 'de' MMMMM 'de' yyyy")).toLowerCase();
			}
		}
		return dataExt;
	}

	public static String getDataCidadeEstadoDiaMesPorExtensoEAno(String cidade, String estadoSigla, Date data, Boolean mesMinusculo) {
		String dataExt = "";
		if (data != null) {
			if (!cidade.equals("")) {
				dataExt += cidade + "-" + estadoSigla + ", ";
			}
			if (mesMinusculo == null || !mesMinusculo) {
				dataExt += getData(data, "dd 'de' MMMMM 'de' yyyy");
			} else {
				dataExt += (getData(data, "dd 'de' MMMMM 'de' yyyy")).toLowerCase();
			}
		}
		return dataExt;
	}

	public static String getDataMesAnoConcatenado() {
		// return MM/AAAA
		int mesAtual = Calendar.getInstance().get(Calendar.MONTH) + 1;
		int anoAtual = Calendar.getInstance().get(Calendar.YEAR);
		String mesAtualStr = String.valueOf(mesAtual);
		if (mesAtualStr.length() == 1) {
			mesAtualStr = "0" + mesAtualStr;
		}
		return mesAtualStr + "/" + anoAtual;
		/*
		 * String dataAtual = Uteis.getDataAtual(); String ano = ""; String mes
		 * = ""; int cont = 1; while (cont != 3) { int posicao =
		 * dataAtual.lastIndexOf("/"); if (posicao != -1) { cont++; if (cont ==
		 * 2) { ano = dataAtual.substring(posicao + 1); dataAtual =
		 * dataAtual.substring(0, posicao); } else if (cont == 3) { mes =
		 * dataAtual.substring(posicao + 1); } } } return (mes + "/" + ano);
		 */
	}

	public static String getDataMesAnoConcatenado(Date dataPrm) {
		// return MM/AAAA
		Calendar dataCalendar = Calendar.getInstance();
		dataCalendar.setTime(dataPrm);
		int mesAtual = dataCalendar.get(Calendar.MONTH) + 1;
		int anoAtual = dataCalendar.get(Calendar.YEAR);
		String mesAtualStr = String.valueOf(mesAtual);
		if (mesAtualStr.length() == 1) {
			mesAtualStr = "0" + mesAtualStr;
		}
		return mesAtualStr + "/" + anoAtual;

	}

	public static String removerMascara(String campo) {
		String campoSemMascara = "";
		for (int i = 0; i < campo.length(); i++) {
			if ((campo.charAt(i) != ',') && (campo.charAt(i) != '.') && (campo.charAt(i) != '-') && (campo.charAt(i) != ':') && (campo.charAt(i) != '/')) {
				campoSemMascara = campoSemMascara + campo.substring(i, i + 1);
			}
		}
		return campoSemMascara;
	}

	public static String aplicarMascara(String dado, String mascara) {
		if (dado == null) {
			return dado;
		}
		if (dado.equals("")) {
			return dado;
		}
		if (dado.length() == mascara.length()) {
			return dado;
		}
		dado = removerMascara(dado);
		int posDado = 0;
		String dadoComMascara = "";
		for (int i = 0; i < mascara.length(); i++) {
			if (posDado >= dado.length()) {
				break;
			}
			String caracter = mascara.substring(i, i + 1);
			if (caracter.equals("9")) {
				dadoComMascara = dadoComMascara + dado.substring(posDado, posDado + 1);
				posDado++;
			} else {
				dadoComMascara = dadoComMascara + caracter;
			}
		}
		return dadoComMascara;
	}

	public static String getDoubleFormatado(Double valor) {
		if (valor == null) {
			return "";
		}
		NumberFormat nf = NumberFormat.getInstance();
		nf.setMinimumFractionDigits(2);
		return nf.format(valor);
	}

	public static int getDiaMesData(Date dataPrm) {
		Calendar dataCalendar = Calendar.getInstance();
		dataCalendar.setTime(dataPrm);

		int dia = dataCalendar.get(Calendar.DAY_OF_MONTH);
		return dia;
	}

	public static int getMesData(Date dataPrm) {
		Calendar dataCalendar = Calendar.getInstance();
		dataCalendar.setTime(dataPrm);

		int mes = dataCalendar.get(Calendar.MONTH) + 1;
		return mes;
	}

	public static int getMesDataVencimento(Date dataPrm) {
		Calendar dataCalendar = Calendar.getInstance();
		dataCalendar.setTime(dataPrm);

		dataCalendar.add(Calendar.MONTH, 1);
		int mes = dataCalendar.get(Calendar.MONTH) + 1;
		return mes;
	}

	public static boolean isWorkingWeekDay(Date date) {
		int dayOfWeek = date.getDay();
		switch (dayOfWeek) {
		case 0: // SUNDAY
		case 6: // SATURDAY
			return false;
		}

		return true;
	}

	public static boolean isDiaDaSemana(Date date) {
		int dayOfWeek = date.getDay();
		switch (dayOfWeek) {
		case 0: // SUNDAY
		case 6: // SATURDAY
			return false;
		}

		return true;
	}

	public static int getAnoData(Date dataPrm) {
		Calendar dataCalendar = Calendar.getInstance();
		dataCalendar.setTime(dataPrm);

		int ano = dataCalendar.get(Calendar.YEAR);
		return ano;
	}

	public static String getValorMonetarioParaIntegracao_SemPontoNemVirgula(double valor) {
		String valorStr = String.valueOf(valor);

		String inteira = valorStr.substring(0, valorStr.indexOf("."));
		String extensao = valorStr.substring(valorStr.indexOf(".") + 1, valorStr.length());
		if (extensao.length() == 1) {
			extensao += "0";
		}
		valorStr = Uteis.removerMascara(inteira + extensao);
		return valorStr;
	}

	public static String getDiaSemanaData(Date data) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(data);
		int diaSemana = cal.get(Calendar.DAY_OF_WEEK);
		if (diaSemana < 10) {
			return "0" + diaSemana;
		}
		return "" + diaSemana;
	}

//	public static DiaSemana getDiaSemanaProximoDia(DiaSemana diaSemana) {
//		switch (diaSemana) {
//		case DOMINGO:
//			return diaSemana.SEGUNGA;
//		case SEGUNGA:
//			return DiaSemana.TERCA;
//		case TERCA:
//			return DiaSemana.QUARTA;
//		case QUARTA:
//			return DiaSemana.QUINTA;
//		case QUINTA:
//			return DiaSemana.SEXTA;
//		case SEXTA:
//			return DiaSemana.SABADO;
//		case SABADO:
//			return DiaSemana.DOMINGO;
//		default:
//			return DiaSemana.NENHUM;
//		}
//	}

//	public static DiaSemana getDiaSemana(Integer numeroDia) {
//		switch (numeroDia) {
//		case 0:
//			return DiaSemana.DOMINGO;
//		case 1:
//			return DiaSemana.SEGUNGA;
//		case 2:
//			return DiaSemana.TERCA;
//		case 3:
//			return DiaSemana.QUARTA;
//		case 4:
//			return DiaSemana.QUINTA;
//		case 5:
//			return DiaSemana.SEXTA;
//		case 6:
//			return DiaSemana.SABADO;
//		default:
//			return DiaSemana.NENHUM;
//		}
//	}

	public static int getDiaSemana(Date data) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(data);
		int diaSemana = cal.get(Calendar.DAY_OF_WEEK);
		return diaSemana;
	}

//	public static DiaSemana getDiaSemanaEnum(Date data) {
//		Calendar cal = Calendar.getInstance();
//		cal.setTime(data);
//		int diaSemana = cal.get(Calendar.DAY_OF_WEEK);
//		return DiaSemana.getEnum("0" + diaSemana);
//	}

	public static double arredondarDecimal(double number, int deep) {
		double multiplier = Math.pow(10, (double) deep);
		return (double) (Math.round(number * multiplier)) / multiplier;
	}

	public static int getMesDataAtual() {
		Locale aLocale = new Locale("pt", "BR");
		Date hoje;
		String hojeStr;
		DateFormat formatador;
		formatador = DateFormat.getDateInstance(DateFormat.SHORT, aLocale);
		hoje = new Date();
		hojeStr = formatador.format(hoje);
		hojeStr = hojeStr.substring(hojeStr.indexOf("/") + 1, hojeStr.lastIndexOf("/"));
		return (Integer.parseInt(hojeStr));
	}

	public static String getAnoDataAtual4Digitos() {
		GregorianCalendar gc = new GregorianCalendar();
		return String.valueOf(gc.get(GregorianCalendar.YEAR));
	}

	public static void liberarListaMemoria(List listaLiberar) {
		if (listaLiberar != null) {
			listaLiberar.clear();
			listaLiberar = null;
		}
	}

	public static String getDiaSemana_Apresentar() {
		try {
			GregorianCalendar gc = new GregorianCalendar();
			String diaSemana = gc.getDisplayName(GregorianCalendar.DAY_OF_WEEK, GregorianCalendar.LONG, new Locale("pt", "BR"));
			return diaSemana;
		} catch (Exception e) {
			return "";
		}
	}

	public static String getDiaSemana_Apresentar(Date data) {
		try {
			SimpleDateFormat df = new SimpleDateFormat("E", new Locale("pt", "BR"));
			return df.format(data);
		} catch (Exception e) {
			return "";
		}
	}

	public static String gerarSenhaRandomica(int quantidade, boolean somenteNumeros) {
		String senha = "";
		String[] caracteres = null;
		if (somenteNumeros) {
			caracteres = montarArray("0", "1", "2", "3", "4", "5", "6", "7", "8", "9");
		} else {
			caracteres = montarArray("0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z");
		}
		for (int x = 0; x < quantidade; x++) {
			int j = (int) (Math.random() * caracteres.length);
			senha += caracteres[j];
		}
		return senha;
	}

	public static <T> T[] montarArray(T... c) {
		return c;
	}

	public static boolean validarEnvioEmail(String hostNameConfiguracaoGeralSistema) throws Exception {
		InetAddress addr = null;
		try {
			addr = InetAddress.getLocalHost();
		} catch (UnknownHostException e) {
		}
		if (!hostNameConfiguracaoGeralSistema.equals("")) {
			if (addr.getHostName().equals(hostNameConfiguracaoGeralSistema)) {
				return true;
			}
		}		
		return false;
	}

//	public static void enviarEmail(String emailDest, String nomeDest, String emailRemet, String nomeRemet, String assunto, String corpo, String smtpPadrao, final String loginServidorSmtp, final String senhaServidorSmtp) throws Exception {
//		enviarEmail(emailDest, nomeDest, emailRemet, nomeRemet, assunto, corpo, smtpPadrao, loginServidorSmtp, senhaServidorSmtp, false, null, "", false);
//	}

//	public static void enviarEmail(String emailDest, String nomeDest, String emailRemet, String nomeRemet, String assunto, String corpo, String smtpPadrao, final String loginServidorSmtp, final String senhaServidorSmtp, Boolean anexarImagensPadrao, List<File> listaFileCorpoMensagem, String portaSmtpPadrao, Boolean servidorEmailUtilizaSSL) throws Exception {
//		Session session = null;
//		MimeMessage message = null;
//		MimeMultipart multipart = null;
//		BodyPart messageBodyPart = null;
//
//		try {
//			Properties props = System.getProperties();
//			props.put("mail.transport.protocol", "smtp");
//			props.put("mail.smtp.host", smtpPadrao);
//			if (portaSmtpPadrao != null || !portaSmtpPadrao.equals("")) {
//				props.put("mail.smtp.port", portaSmtpPadrao);
//			}
//			props.put("mail.smtp.auth", "true");
//			if (servidorEmailUtilizaSSL) {
//				final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
//				props.setProperty("mail.smtp.socketFactory.class", SSL_FACTORY);
//				props.setProperty("mail.smtp.socketFactory.port", portaSmtpPadrao);
//				// Stop socket from falling back to insecure connection)
//				props.setProperty("mail.smtp.socketFactory.fallback", "false");
//			} else {
//				props.remove("mail.smtp.socketFactory.class");
//				props.remove("mail.smtp.socketFactory.port");
//				props.remove("mail.smtp.socketFactory.fallback");
//			}
//			// props.put("mail.smtp.starttls.enable", "true");
//			// props.put("mail.smtp.timeout", "30000");
//			Authenticator auth = new Authenticator() {
//
//				@Override
//				public PasswordAuthentication getPasswordAuthentication() {
//					return new PasswordAuthentication(loginServidorSmtp, senhaServidorSmtp);
//				}
//			};
//			session = Session.getInstance(props, auth);
//			session.setDebug(false);
//			// Transport transport = session.getTransport("smtp");
//			message = new MimeMessage(session);
//			message.setSentDate(new Date());
//			message.setFrom(new InternetAddress(emailRemet, nomeRemet));
//			message.addRecipient(Message.RecipientType.TO, new InternetAddress(emailDest, nomeDest));
//			message.setSubject(assunto);
//			multipart = new MimeMultipart("related");
//			messageBodyPart = new MimeBodyPart();
//			messageBodyPart.setContent(corpo, "text/html; charset=ISO-8859-1");
//			multipart.addBodyPart(messageBodyPart);
//			if (anexarImagensPadrao) {
//				for (File imagem : listaFileCorpoMensagem) {
//					messageBodyPart = new MimeBodyPart();
//					DataSource fds = new FileDataSource(imagem);
//					messageBodyPart.setDataHandler(new DataHandler(fds));
//					messageBodyPart.setHeader("Content-ID", "<" + imagem.getName().substring(0, imagem.getName().lastIndexOf(".")) + ">");
//					multipart.addBodyPart(messageBodyPart);
//				}
//			}
//			message.setContent(multipart);
//			message.saveChanges();
//			// transport.connect(smtpPadrao, Integer.parseInt(portaSmtpPadrao),
//			// loginServidorSmtp, senhaServidorSmtp);
//			Transport.send(message);
//			// transport.close();
//		} catch (AuthenticationFailedException e) {
//			throw new ConsistirException("As configura��es de email est�o incorretas, entre em contato com o administrador.");
//		} catch (Exception e) {
//			// tratar
//			if (e.getMessage().contains("Access to default session denied")) {
//				throw new Exception("Ocorreu um erro durante o envio do e-mail. A permiss�o de acesso sess�o de email foi negada.");
//			}
//			throw e;
//		}
//	}

	// public static void enviarEmail(ConfiguracaoGeralSistemaVO cs, String
	// titulo, String mensagem) throws Exception {
	// enviarEmail(cs.getDiretorAcademico().getEmailPrincipal(),
	// cs.getDiretorAcademico().getNome(), cs.getEmailPadrao(),
	// "SEI - SISTEMA EDUCACIONAL INTEGRADO", titulo, mensagem,
	// cs.getMailServer(), cs.getLogin(), cs.getSenha());
	// }
	public static void enviarEmailAnexo(String emailDest, String nomeDest, String emailRemet, String nomeRemet, String assunto, String corpo, String nomeFile, File arquivo, String smtpPadrao, final String loginServidorSmtp, final String senhaServidorSmtp) throws Exception {
		enviarEmailAnexo(emailDest, nomeDest, emailRemet, nomeRemet, assunto, corpo, nomeFile, arquivo, smtpPadrao, loginServidorSmtp, senhaServidorSmtp, false, null, null, false);
	}

	public static void enviarEmailAnexo(String emailDest, String nomeDest, String emailRemet, String nomeRemet, String assunto, String corpo, String nomeFile, File arquivo, String smtpPadrao, final String loginServidorSmtp, final String senhaServidorSmtp, Boolean anexarImagensPadrao, List<File> listaFileCorpoMensagem, String portaSmtpPadrao, Boolean servidorEmailUtilizaSSL) throws Exception {
		MimeBodyPart messageBodyPart = null;
		try {
			Properties props = System.getProperties();
			props.put("mail.transport.protocol", "smtp");
			props.put("mail.smtp.host", smtpPadrao);
			if (portaSmtpPadrao != null || !portaSmtpPadrao.equals("")) {
				props.put("mail.smtp.port", portaSmtpPadrao);
			}
			props.put("mail.smtp.auth", "true");
			if (servidorEmailUtilizaSSL) {
				final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
				props.setProperty("mail.smtp.socketFactory.class", SSL_FACTORY);
				props.setProperty("mail.smtp.socketFactory.port", portaSmtpPadrao);
				// Stop socket from falling back to insecure connection)
				props.setProperty("mail.smtp.socketFactory.fallback", "false");
			} else {
				props.remove("mail.smtp.socketFactory.class");
				props.remove("mail.smtp.socketFactory.port");
				props.remove("mail.smtp.socketFactory.fallback");
			}
			// props.put("mail.smtp.starttls.enable", "true");
			// props.put("mail.smtp.timeout", "30000");
			Authenticator auth = new Authenticator() {

				@Override
				public PasswordAuthentication getPasswordAuthentication() {
					return new PasswordAuthentication(loginServidorSmtp, senhaServidorSmtp);
				}
			};
			Session session = Session.getInstance(props, auth);
			session.setDebug(false);
			// Transport transport = session.getTransport("smtp");
			MimeMessage message = new MimeMessage(session);
			message.setSentDate(new Date());
			message.setFrom(new InternetAddress(emailRemet, nomeRemet));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(emailDest, nomeDest));
			message.setSubject(assunto);
			// message.setContent(corpo, "text/html");

			MimeMultipart mpRoot = new MimeMultipart("mixed");
			MimeMultipart mpContent = new MimeMultipart("alternative");
			MimeBodyPart contentPartRoot = new MimeBodyPart();
			contentPartRoot.setContent(mpContent);
			mpRoot.addBodyPart(contentPartRoot);

			// enviando html
			MimeBodyPart mbp1 = new MimeBodyPart();
			mbp1.setContent(corpo, "text/html;");
			mpContent.addBodyPart(mbp1);

			// enviando anexo
			MimeBodyPart mbp2 = new MimeBodyPart();
			DataSource fds = new FileDataSource(arquivo);
			mbp2.setDisposition(Part.ATTACHMENT);
			mbp2.setDataHandler(new DataHandler(fds));
			mbp2.setFileName(nomeFile);

			mpRoot.addBodyPart(mbp2);

			if (anexarImagensPadrao) {
				for (File imagem : listaFileCorpoMensagem) {
					messageBodyPart = new MimeBodyPart();
					DataSource fds2 = new FileDataSource(imagem);
					messageBodyPart.setDataHandler(new DataHandler(fds2));
					messageBodyPart.setHeader("Content-ID", "<" + imagem.getName().substring(0, imagem.getName().lastIndexOf(".")) + ">");
					mpRoot.addBodyPart(messageBodyPart);
				}
			}

			message.setContent(mpRoot);
			message.saveChanges();
			// transport.connect(smtpPadrao, Integer.parseInt(portaSmtpPadrao),
			// loginServidorSmtp, senhaServidorSmtp);
			Transport.send(message);
			// transport.close();
		} catch (Exception e) {
			// tratar
			e.getMessage().contains("Access to default session denied");
			throw new Exception("O e-mail do remetente ou do destinatario est� inv�lido.");
		}
	}

	public static Integer getIntervaloEntreDatas(Date dataInicial, Date dataFinal) {
		long tempoInicial = dataInicial.getTime();
		long tempoFinal = dataFinal.getTime();
		long diferencaTempo = tempoFinal - tempoInicial;
		return (int) ((diferencaTempo) / (24L * 60 * 60 * 1000));
		// return (int) ((diferencaTempo + 60L * 60 * 1000) / (24L * 60 * 60 *
		// 1000));
		// return (int) diferencaTempo * 60 * 60 * 1000;

	}

	public static Integer contarQuantidadeDePontos(String frase, String sub) {
		int cont = 0;
		for (int i = 0; i < (frase.length() - sub.length() + 1); i++) {
			String res = frase.substring(i, (i + sub.length()));
			if (res.equals(sub)) {
				cont++;
			}
		}
		return cont;
	}

	public static String substituirPadraoString(String base, String padraoRemover, String padraoManter) {
		int pos = base.indexOf(padraoRemover);
		String tmp;
		while (pos != -1) {
			tmp = base.substring(0, pos);
			tmp += padraoManter;
			tmp += base.substring(pos + padraoRemover.length());
			base = tmp;
			pos = base.indexOf(padraoRemover);
		}
		return base;
	}

//	public static class OrdenaDiarioFrequenciaVOPorAluno implements Comparator<DiarioFrequenciaAulaVO> {
//
//		public int compare(DiarioFrequenciaAulaVO obj1, DiarioFrequenciaAulaVO obj2) {
//			Collator c = Collator.getInstance(new Locale("pt", "BR"));
//			try {
//				// return
//				// (obj1.getMatricula().getAluno().getNome().trim()).compareTo(obj2.getMatricula().getAluno().getNome().trim());
//				return c.compare(obj1.getMatricula().getAluno().getNome().trim(), obj2.getMatricula().getAluno().getNome().trim());
//			} finally {
//				c = null;
//			}
//		}
//	}
//
//	public static class OrdenaCronogramaAulaVOPorDataUnidade implements Comparator<CronogramaDeAulasRelVO> {
//
//		public int compare(CronogramaDeAulasRelVO obj1, CronogramaDeAulasRelVO obj2) {
//			Collator c = Collator.getInstance(new Locale("pt", "BR"));
//			try {
//				// return
//				// (obj1.getMatricula().getAluno().getNome().trim()).compareTo(obj2.getMatricula().getAluno().getNome().trim());
//				return c.compare(obj1.getDataUnidadeOrdenacao().trim(), obj2.getDataUnidadeOrdenacao().trim());
//			} finally {
//				c = null;
//			}
//		}
//	}
//
//	public static class OrdenaHistoricoVOsPorCodigoConfiguracaoAcademico implements Comparator<HistoricoVO> {
//
//		public int compare(HistoricoVO obj1, HistoricoVO obj2) {
//			return (obj1.getConfiguracaoAcademico().getCodigo() + "-" + obj1.getDisciplina().getNome()).compareTo(obj2.getConfiguracaoAcademico().getCodigo() + "-" + obj2.getDisciplina().getNome());
////			return (obj1.getConfiguracaoAcademico().getCodigo()).compareTo(obj2.getConfiguracaoAcademico().getCodigo());
//		}
//	}

	// public static String montarTagImageComFoto(PessoaVO pessoaVO) throws
	// Exception {
	// StringBuilder str = new
	// StringBuilder("<img style='width: 100px; height: 120px' src='fotos/imagemPadrao.jpg'/>");
	// try {
	// if (pessoaVO != null && pessoaVO.getFoto() != null &&
	// pessoaVO.getFoto().length != 0) {
	// restaurarFoto(pessoaVO);
	// String foto = "" + pessoaVO.getCodigo() + "";
	// str = new StringBuilder("");
	// str.append("<img style='width: 100px; height: 120px' src='fotos/" + foto
	// + "'/>");
	// }
	// } catch (Exception e) {
	// throw e;
	// }
	// return str.toString();
	// }
	//
	// public static void restaurarFoto(PessoaVO pessoaVO) throws Exception {
	// ServletContext sc = (ServletContext)
	// FacesContext.getCurrentInstance().getExternalContext().getContext();
	// String caminho = sc.getRealPath("fotos") + File.separator;
	// File file = new File(caminho + pessoaVO.getCodigo() + " - " +
	// pessoaVO.getDescricaoFoto());
	//
	// if (!file.exists()) {
	// FileOutputStream fos = new FileOutputStream(file);
	// fos.write(pessoaVO.getFoto());
	// fos.close();
	// fos = null;
	// }
	//
	// file = null;
	// }

	public static boolean possuiOperadoresMatematicos(String valor) {
		if ((valor.indexOf("+") != -1) || (valor.indexOf("-") != -1) || (valor.indexOf("*") != -1) || (valor.indexOf("/") != -1)) {
			return true;
		}
		return false;
	}

	public static Integer calcularIdadePessoa(Date dataAtualPrm, Date dataNascimentoPrm) {
		if ((dataNascimentoPrm == null) || (dataAtualPrm == null)) {
			return 0;
		}
		// Desmembrando a data de nascimento
		Calendar nascimentoCalendario = Calendar.getInstance();
		nascimentoCalendario.setTime(dataNascimentoPrm);
		int anoNascimento = nascimentoCalendario.get(Calendar.YEAR);
		String mesNascimentoStr = String.valueOf(nascimentoCalendario.get(Calendar.MONTH) + 1);
		if (mesNascimentoStr.length() == 1) {
			mesNascimentoStr = "0" + mesNascimentoStr;
		}
		String diaNascimentoStr = String.valueOf(nascimentoCalendario.get(Calendar.DAY_OF_MONTH));
		if (diaNascimentoStr.length() == 1) {
			diaNascimentoStr = "0" + diaNascimentoStr;
		}
		String mesDiaNascimento = mesNascimentoStr + diaNascimentoStr;

		// Desmembrando a data atual (passada como parametro)
		Calendar dataAtualCalendario = Calendar.getInstance();
		dataAtualCalendario.setTime(dataAtualPrm);

		int anoAtual = dataAtualCalendario.get(Calendar.YEAR);

		String mesAtualStr = String.valueOf(dataAtualCalendario.get(Calendar.MONTH) + 1);
		if (mesAtualStr.length() == 1) {
			mesAtualStr = "0" + mesAtualStr;
		}

		String diaAtualStr = String.valueOf(dataAtualCalendario.get(Calendar.DAY_OF_MONTH));
		if (diaAtualStr.length() == 1) {
			diaAtualStr = "0" + diaAtualStr;
		}

		String mesDiaAtual = mesAtualStr + diaAtualStr;

		int idade = anoAtual - anoNascimento;
		if (mesDiaAtual.compareTo(mesDiaNascimento) < 0) {
			idade--;
		}

		return idade;
	}

	public static String removerAcentuacao(String strRemoverAcento) {
		String input = strRemoverAcento;
		if (input != null && !input.equals("")) {
			input = Normalizer.normalize(input, Normalizer.Form.NFD);
			input = input.replaceAll("[^Ã‚Â´\\p{ASCII}]", "");
		}
		return input;
	}

	public static String corrigirApostrofo(String strComApostrofo) {
		int posicao = 0;
		String strCorrigida = null;
		if (strComApostrofo.contains("'")) {
			posicao = strComApostrofo.indexOf("'");
			String parteInicial = strComApostrofo.substring(0, posicao);
			String parteFinal = strComApostrofo.substring(posicao, strComApostrofo.length());
			strCorrigida = parteInicial + "\\" + parteFinal;
			return strCorrigida;
		} else {
			return strComApostrofo;
		}
	}

	public static int getHoraDaString(String hora) {
		if (hora.length() >= 5) {
			if (hora.contains(":")) {
				return Integer.parseInt(hora.substring(0, 2));
			}
		}
		return 0;
	}

	public static int getMinutosDaString(String hora) {
		if (hora.length() >= 5) {
			if (hora.contains(":")) {
				return Integer.parseInt(hora.substring(3, 5));
			}
		}
		return 0;
	}

	public static int getHoraMinutoSegundo(Date data, String retornar) throws Exception {
		Calendar cal = Calendar.getInstance();
		cal.setTime(data);
		if (retornar.equals("hora")) {
			return cal.get(Calendar.HOUR_OF_DAY);
		} else if (retornar.equals("minutos")) {
			return cal.get(Calendar.MINUTE);
		} else if (retornar.equals("segundos")) {
			return cal.get(Calendar.SECOND);
		} else {
			return 0;
		}
	}

	public static Integer validarSomenteNumeroString(String valorInteiro) throws Exception {
		try {
			return Integer.valueOf(valorInteiro.trim());
		} catch (Exception e) {
			throw new Exception("Digite apenas n�meros para o campo (C�DIGO) !");
		}
	}

	public static Boolean realizarValidacaoHora1MaiorHora2(String hora1, String hora2) {
		Integer h1 = Integer.valueOf(hora1.substring(0, 2));
		Integer h2 = Integer.valueOf(hora2.substring(0, 2));
		if (h1 > h2) {
			return true;
		} else if (h1.equals(h2)) {
			h1 = Integer.valueOf(hora1.substring(3, 5));
			h2 = Integer.valueOf(hora2.substring(3, 5));
			if (h1 > h2) {
				return true;
			}
		}
		return false;

	}

	/***
	 * MÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬
	 * Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€
	 * ™ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã‚Â ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å�
	 * �Ã‚Â¬ÃƒÂ¢Ã¢â‚�
	 * �Å¾Ã‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€�
	 * �Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚�
	 * �Å¡Ã‚Â¬Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ€š
	 * Ã‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â�
	 * �ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ�
	 * �Ã…Â¾Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã�
	 * �â€™Ãƒâ€ Ã¢â‚¬
	 * â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã�
	 * �â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã�
	 * �Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ€šÃ‚Â�
	 * �ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬
	 * â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚�
	 * �Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â�
	 * �ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€�
	 * �Â¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â©todo retornar
	 * no formato hora e minutos de acordo com a quantidade de minutos informado
	 * 
	 * @param minutos
	 *            - valor em minutos
	 * @return String
	 */
	public static String getMinutosEmHorasEMinuto(String minutos) {
		// Preparando hora zerada
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		Calendar cal = Calendar.getInstance();

		cal.set(Calendar.HOUR, 12);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);

		cal.set(Calendar.MINUTE, Integer.parseInt(minutos));

		return sdf.format(cal.getTime()).toString();
	}

	public static Double converterMinutosEmHoras(Double minutos) {
		Integer horas = Integer.valueOf(Double.valueOf(minutos / 60).intValue());
		Integer minuto = Double.valueOf(minutos).intValue();
		if (horas > 0) {
			minuto = Double.valueOf(Uteis.arrendondarForcando2CadasDecimais(Double.valueOf(minutos - (horas * 60)))).intValue();
		}
		while (minuto > 59) {
			horas = horas + 1;
			minuto = minuto - 60;
		}
		return arrendondarForcando2CadasDecimais(Double.valueOf((horas + Double.valueOf(Double.valueOf(minuto) / 100))));
	}

	public static String converterMinutosEmHorasStr(Double minutos) {
		Integer horas = Integer.valueOf(Double.valueOf(minutos / 60).intValue());
		Integer minuto = Double.valueOf(minutos).intValue();
		if (horas > 0) {
			minuto = Double.valueOf(Uteis.arrendondarForcando2CadasDecimais(Double.valueOf(minutos - (horas * 60)))).intValue();
		}
		while (minuto > 59) {
			horas = horas + 1;
			minuto = minuto - 60;
		}
		double horasComp = arrendondarForcando2CadasDecimais(Double.valueOf((horas + Double.valueOf(Double.valueOf(minuto) / 100))));
		String horaString = String.valueOf(horasComp);
		String[] horaStringN = horaString.split("\\.");
		if (horaStringN[1].length() != 2) {
			horaStringN[1] = horaStringN[1] + "0";
		}

		return horaStringN[0] + ":" + horaStringN[1];

	}

//	public static byte[] realizarUpload(UploadEvent upload) throws Exception {
//		UploadItem item = upload.getUploadItem();
//		File item1 = item.getFile();
//		String nome = item.getFileName();
//		String extensao = (nome.substring(nome.lastIndexOf('.') + 1));
//		try {
//			ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
//			byte buffer[] = new byte[4096];
//			int bytesRead = 0;
//			FileInputStream fi = new FileInputStream(item1.getAbsolutePath());
//			while ((bytesRead = fi.read(buffer)) != -1) {
//				arrayOutputStream.write(buffer, 0, bytesRead);
//			}
//			arrayOutputStream.close();
//			fi.close();
//			return arrayOutputStream.toByteArray();
//		} catch (Exception e) {
//			throw e;
//		}
//	}

	public static Double converterHorasEmMinutos(Double horas) {
		Integer parteInteira = horas.intValue();
		Integer minuto = Double.valueOf(Uteis.arrendondarForcando2CadasDecimais(parteInteira - horas) * 100).intValue();
		if (minuto < 0) {
			minuto = minuto * -1;
		}
		return arrendondarForcando2CadasDecimais(Double.valueOf((parteInteira * 60) + minuto));
	}

	public static Date getDataAdicionadaEmHoras(Date data, int horas) {
		Calendar calendario = new GregorianCalendar();
		calendario.setTime(data);
		calendario.add(Calendar.HOUR_OF_DAY, horas);
		return calendario.getTime();
	}

	public static boolean validarNomeParaCenso(String nome) {
		String letrasValidas = "AÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¯Ãƒâ€šÃ‚Â¿Ãƒâ€šÃ‚Â½ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬BCÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€šÃ‚Â¡DEÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€šÃ‚Â°ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â¹ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â FGHIÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¯Ãƒâ€šÃ‚Â¿Ãƒâ€šÃ‚Â½ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ€šÃ‚Â½ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â¦ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢YJKLMNOÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã¢â‚¬Å“ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€šÃ‚ï¿½ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÂ¢Ã¢â‚¬Å¾Ã‚Â¢PQRSTUÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ€šÃ‚Â¡ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€šÃ‚ÂºÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¾Ãƒâ€šÃ‚Â¢VWXYZ ";

		for (int i = 0; i < nome.length(); i++) {
			// if (!(letrasValidas.indexOf(nome.toUpperCase().charAt(i), 0) !=
			// -1)) {
			// throw new
			// Exception("O caractÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÂ¢Ã¢â‚¬Å¾Ã‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¾Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ€šÃ‚Â¡ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â©r '"
			// + nome.charAt(i) + "' na palavra '" + nome +
			// "' ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÂ¢Ã¢â‚¬Å¾Ã‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¾Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ€šÃ‚Â¡ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â© invÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÂ¢Ã¢â‚¬Å¾Ã‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¾Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ€šÃ‚Â¡ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¡lido");
			// }
			if (i > 1) {
				if ((nome.toUpperCase().charAt(i - 1) == ' ') && (nome.toUpperCase().charAt(i) == ' ')) {
					// nome.toUpperCase().charAt(i) = '';
				}
			}
		}

		return true;
	}

	public static String getObterParteStringMantendoPalavraFinalInteira(String value, int tam, boolean removerTagsHtml) {
		if (removerTagsHtml) {
			value = retiraTags(value);
		}
		value = value.trim();
		if (value.length() > tam) {
			if (value.indexOf(" ", tam) > 0) {
				tam = value.indexOf(" ", tam);
			} else {
				return value;
			}
		} else {
			return value;
		}

		return value.substring(0, tam) + " ...";
	}

	public static String retiraTags(String textoFormatado) {
		String[] textos = textoFormatado.split("<.*?>");
		StringBuilder sb = new StringBuilder();
		for (String s : textos) {
			s = s.replace("\\\\n", "");
			s = s.replace("&nbsp;", "");
			if (!s.contains("DOCTYPE") && !s.contains("Untitled document") && !s.trim().isEmpty() && !s.trim().equals("&nbsp;")) {
				sb.append(s);
			}
		}

		return sb.toString();
	}

	public static String retirarCodigoHtmlEspaco(String textoFormatado) {
		return textoFormatado.replaceAll("&nbsp;", "");
	}

	public static boolean getValidaEmail(String email) {
		Pattern p = Pattern.compile("^[\\w-]+(\\.[\\w-]+)*@([\\w-]+\\.)+[a-zA-Z]{2,7}$");
		Matcher m = p.matcher(email);
		return m.find();
	}

	public static Date alterarSomenteDia(Integer novoDia, Date dataAntiga) {
		GregorianCalendar novaData;
		Date dataComUltimoDiaMes = getDataUltimoDiaMes(dataAntiga);
		if (novoDia > getDiaMesData(dataComUltimoDiaMes)) {
			return dataComUltimoDiaMes;
		} else {
			novaData = new GregorianCalendar(Uteis.getAnoData(dataAntiga), Uteis.getMesData(dataAntiga) - 1, novoDia);
			return novaData.getTime();
		}
	}

	public static String getDataBD0000(Date data) {
		DateFormat df = null;
		Calendar calendar = new GregorianCalendar();
		try {
			calendar.setTimeInMillis(data.getTime());
			calendar.set(Calendar.HOUR_OF_DAY, 0);
			calendar.set(Calendar.MINUTE, 0);
			calendar.set(Calendar.SECOND, 0);
			calendar.set(Calendar.MILLISECOND, 0);
			df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			return df.format(calendar.getTime());
		} finally {
			df = null;
			calendar = null;
		}
	}

	public static String getDataBD2359(Date data) {
		DateFormat df = null;
		Calendar calendar = new GregorianCalendar();
		try {
			calendar.setTimeInMillis(data.getTime());
			calendar.set(Calendar.HOUR_OF_DAY, 23);
			calendar.set(Calendar.MINUTE, 59);
			calendar.set(Calendar.SECOND, 59);
			calendar.set(Calendar.MILLISECOND, 999);
			df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			return df.format(calendar.getTime());
		} finally {
			df = null;
			calendar = null;
		}
	}

	public static String getNomeResumidoPessoa(String nomePessoa) {
                if (nomePessoa == null) {
                    return "";
                }
		String[] nomes = nomePessoa.split(" ");
		String nomeResumido = "";
		Integer indice = 1;
		nomeResumido += nomes[0] + " ";
		while (indice < nomes.length - 1) {
			if (nomes[indice].length() > 2) {
				nomes[indice] = nomes[indice].substring(0, 1).concat(".");
			}
			nomeResumido += nomes[indice] + " ";
			indice = indice + 1;
		}
		nomeResumido += nomes[nomes.length - 1] + " ";
		return nomeResumido;
	}

	public static void aplicarHoraMinutoSegundoAtualEmData(Date data) {
		data.setHours(new Date().getHours());
		data.setMinutes(new Date().getMinutes());
		data.setSeconds(new Date().getSeconds());
	}

	public static void removerObjetoMemoria(Object object) {
		if (object != null) {
			Class classe = object.getClass();
			for (Field field : classe.getDeclaredFields()) {
				field.setAccessible(true);
				if (field != null) {
					try {
						if (field.getClass().getSuperclass().getSimpleName().equals("SuperVO")) {
							// removerObjetoMemoria(field.getClass().getSuperclass());
							removerObjetoMemoria(field);
						} else if (field.getType().getName().equals("java.lang.Integer") || field.getType().getName().equals("java.lang.String") || field.getType().getName().equals("java.lang.Double") || field.getType().getName().equals("java.lang.Boolean") || field.getType().getName().equals("java.lang.Long")) {
							field.set(object, null);
						} else if (field.getType().getName().equals("java.util.List") || field.getType().getName().equals("java.util.ArrayList")) {
							List<?> lista = (List<?>) field.get(object);
							// for(Object o: lista){
							// removerObjetoMemoria(o);
							// }
							if(lista != null){
								lista.clear();
								}
							field.set(object, null);
						} else {
							field.set(object, null);
						}
						field = null;
					} catch (Exception e) {
					}
				}
			}
			if (object.getClass().getSuperclass().getSimpleName().equals("SuperVO") || object.getClass().getSuperclass().getSimpleName().equals("SuperControle") || object.getClass().getSuperclass().getSimpleName().equals("SuperControleRelatorio")) {
				removerObjetoMemoria(object.getClass().getSuperclass());
			}
			classe = null;
		}

	}

	@SuppressWarnings("CallToThreadDumpStack")
	public static Boolean getArquivoExcluido(String caminhoWebService, String caminho) {
		try {
			HttpClient client = new HttpClient();
			PostMethod method = new PostMethod(caminhoWebService);

			int statusCode = client.executeMethod(method);
			InputStream rstream = null;
			rstream = method.getResponseBodyAsStream();
			// URL url = new URL(caminhoWebService + caminho);
			// HttpURLConnection connection = (HttpURLConnection)
			// url.openConnection();
			// connection.setRequestMethod("POST");
			// connection.connect();
			//
			// InputStream responseStream = connection.getInputStream();
			//
			// JSONJAXBContext jc = new JSONJAXBContext(ArquivoObject.class);
			// JSONUnmarshalle ju = jc.createJSONUnmarshaller();
			//
			// ArquivoObject obj = ju.unmarshalFromJSON(responseStream,
			// ArquivoObject.class);
			// connection.disconnect();

			return true;

		} catch (java.net.MalformedURLException mue) {
		} catch (java.io.IOException ioe) {
		}
		return Boolean.FALSE;
	}

//	public static String getCaminhoDownloadRelatorio(boolean fazerDownload, String caminhoRelatorio) {
//		if (fazerDownload) {
//			try {
//				if (UteisEmail.getURLAplicacao().endsWith("/SEI/") || UteisEmail.getURLAplicacao().endsWith("/SEI") || UteisEmail.getURLAplicacao().endsWith("/SEI/faces") || UteisEmail.getURLAplicacao().endsWith("/SEI/faces/")) {
//					return "location.href='/SEI/DownloadRelatorioSV?relatorio=" + caminhoRelatorio + "'";
//				}
//				return "location.href='../DownloadRelatorioSV?relatorio=" + caminhoRelatorio + "'";
//			} catch (Exception ex) {
//				Logger.getLogger("SuperControleRelatorio").log(Level.SEVERE, null, ex);
//			}
//		}
//		return "";
//	}

	public static String getTagImageComFotoPadrao() {
		return new StringBuilder("<img style='width: 100px; height: 120px' src='fotos/imagemPadrao.jpg'/>").toString();
	}

	public static String getDiretorioWebTemporario() {
		File dir = null;
		String retorno = null;
		ServletContext contexto = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
		try {
			dir = new File(contexto.getRealPath("temp"));
			if (!dir.exists()) {
				dir.mkdirs();
			}
			retorno = dir.getPath();
		} finally {
			dir = null;
		}
		return retorno;
	}

	public static Boolean getMesmoMesAno(Date dataInicio, Date dataTermino) {
		try {
			if ((Uteis.getAnoData(dataInicio) == Uteis.getAnoData(dataTermino)) && (Uteis.getMesData(dataInicio) == Uteis.getMesData(dataTermino))) {
				return true;
			}
			return false;
		} catch (Exception e) {
			return false;
		}
	}

	public static Date getObterDataPorDataDiaMesAno(java.util.Date dataConverter, String tipo, int quantidade) {
		Calendar c = Calendar.getInstance();
		c.setTime(dataConverter);
		if (tipo.equals("dia")) {
			c.add(Calendar.DAY_OF_MONTH, quantidade);
		} else if (tipo.equals("mes")) {
			c.add(Calendar.MONTH, quantidade);
		} else if (tipo.equals("ano")) {
			c.add(Calendar.YEAR, quantidade);
		}
		return c.getTime();
	}

	public static String getObterTempoEntreDuasData(Date dataFinal, Date dataInicial) {
		long sec = (dataFinal.getTime() - dataInicial.getTime());
		// variÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÂ¢Ã¢â‚¬Å¾Ã‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¾Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ€šÃ‚Â¡ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¡veis
		// auxiliares
		long second = 1000;
		long minute = 60 * second;
		long hour = 60 * minute;
		long day = 24 * hour;

		// resultado em horas decimais
		double rdh = Math.ceil(sec / hour);
		// resultado em minutos decimais
		double rdm = Math.ceil(sec / minute);
		// resultado em segundos
		double rds = Math.ceil(sec / second);

		Double days = Math.floor(sec / day);
		sec -= days * day;
		Double hours = Math.floor(sec / hour);
		sec -= hours * hour;
		Double minutes = Math.floor(sec / minute);
		// Caso queira saber o segunto
		sec -= minutes * minute;
		Double seconds = Math.floor(sec / second);

		return (hours.intValue() < 10 ? "0" + hours.intValue() : hours.intValue()) + ":" + (minutes.intValue() < 10 ? "0" + minutes.intValue() : minutes.intValue()) + ":" + (seconds.intValue() < 10 ? "0" + seconds.intValue() : seconds.intValue());

	}

	public static Integer getObterDiferencaDiasEntreDuasData(Date dataFinal, Date dataInicial) {
		double milliSec = (dataFinal.getTime() - dataInicial.getTime());
		Double dias = milliSec / 3600 / 24 / 1000;
		// variÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÂ¢Ã¢â‚¬Å¾Ã‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¾Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ€šÃ‚Â¡ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¡veis
		// auxiliares
		return dias.intValue();

	}

	public static Boolean getValidarTempoEsgotado(Date dataFinal, Date dataInicial) {
		long sec = (dataFinal.getTime() - dataInicial.getTime());
		// variÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬ÃƒÂ¢Ã¢â‚¬Å¾Ã‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¾Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ€šÃ‚Â¡ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â¡ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¡veis
		// auxiliares
		long second = 1000;
		long minute = 60 * second;
		long hour = 60 * minute;
		long day = 24 * hour;

		// resultado em horas decimais
		double rdh = Math.ceil(sec / hour);
		// resultado em minutos decimais
		double rdm = Math.ceil(sec / minute);
		// resultado em segundos
		double rds = Math.ceil(sec / second);

		Double days = Math.floor(sec / day);
		sec -= days * day;
		Double hours = Math.floor(sec / hour);
		sec -= hours * hour;
		Double minutes = Math.floor(sec / minute);
		// Caso queira saber o segunto
		sec -= minutes * minute;
		Double seconds = Math.floor(sec / second);
		if (hours.intValue() <= 0 && minutes.intValue() <= 0 && seconds.intValue() <= 0) {
			return true;
		}
		return false;

	}

	public static String formatarTelefoneParaEnvioSms(String telefone) throws Exception {
		String telefoneFormatado = "55" + telefone.replaceAll("\\(", "").replaceAll("\\)", "").replaceAll("-", "").replaceAll(" ", "");

		if (telefoneFormatado.length() < 12 || telefoneFormatado.length() > 13) {
			throw new Exception("numero invalido");
		}
		return telefoneFormatado;
	}

	public static String formatarTelefoneParaEnvioLocaSms(String telefone) throws Exception {
		String telefoneFormatado = "" + telefone.replaceAll("\\(", "").replaceAll("\\)", "").replaceAll("-", "").replaceAll(" ", "");
		if (telefoneFormatado.length() < 10 || telefoneFormatado.length() > 11) {
			throw new Exception("numero invalido");
		}
		return telefoneFormatado;
	}

	public static boolean compararDatasSemConsiderarHoraMinutoSegundo(Date data1, Date data2) throws Exception {
		SimpleDateFormat sd = new SimpleDateFormat("dd/MM/yyyy");
		return sd.parse(sd.format(data1)).equals(sd.parse(sd.format(data2)));
	}

	@SuppressWarnings("CallToThreadDumpStack")
	public static String realizarMontagemDeValoresSeparadosPorVirgula(List<?> objs, String nomeMetodo) {
		StringBuilder sb = new StringBuilder(" ");
		if (objs != null && !objs.isEmpty()) {
			try {
				Method metodo = objs.get(0).getClass().getMethod(nomeMetodo, null);
				for (Object obj : objs) {
					sb.append(" ").append(metodo.invoke(obj, new Object[] {})).append(",");
				}
			} catch (Exception e) {
				return "";
			}
		}
		return sb.toString().substring(0, sb.length() - 1);
	}

	public static int gerarDigitoVerificadorNossoNumeroCodigoCedenteITAU(String agencia, String contaCorrente, String carteira, String nossoNumero) {
		try {
			int dac = 0;
			if (contaCorrente.indexOf("-") != -1) {
				contaCorrente = contaCorrente.substring(0, contaCorrente.indexOf("-"));
			}
			String campo = String.valueOf(agencia) + contaCorrente + String.valueOf(carteira) + nossoNumero;

			int multiplicador = 1;
			int multiplicacao = 0;
			int soma_campo = 0;

			for (int i = 0; i < campo.length(); i++) {
				multiplicacao = Integer.parseInt(campo.substring(i, 1 + i)) * multiplicador;

				if (multiplicacao >= 10) {
					multiplicacao = Integer.parseInt(String.valueOf(multiplicacao).substring(0, 1)) + Integer.parseInt(String.valueOf(multiplicacao).substring(1));
				}
				soma_campo = soma_campo + multiplicacao;

				if (multiplicador == 2) {
					multiplicador = 1;
				} else {
					multiplicador = 2;
				}

			}

			dac = 10 - (soma_campo % 10);

			if (dac == 10) {
				dac = 0;
			}

			return dac;
		} catch (Exception e) {
			return 0;
		}
	}


	public static String gerarDigitoVerificadorNossoNumeroCodigoCedenteBradesco(String carteira, String nossoNumero) {
		try {
			String digitoVerificador = "";
			String numeroCalculo = carteira + nossoNumero;
			if (!numeroCalculo.equals("")) {
				String[] vetorNumeroCalculo = numeroCalculo.split("");
				int numMultiplicador = 2;
				int valor = 0;
				for (int num = vetorNumeroCalculo.length - 1; num >= 0; num--) {
					if (!vetorNumeroCalculo[num].equals("")) {
						valor = valor + (numMultiplicador * Integer.parseInt(vetorNumeroCalculo[num]));
						if (numMultiplicador == 7) {
							numMultiplicador = 2;
						} else {
							numMultiplicador++;
						}
					}
				}
				int resto = (valor % 11);
				if (resto == 1) {
					digitoVerificador = "P";
				} else if (resto == 0) {
					digitoVerificador = "0";					
				} else {
					digitoVerificador = String.valueOf(11 - (valor % 11));
				}
			}
			return digitoVerificador;
		} catch (Exception e) {
			return "0";
		}
	}
	
	public static String gerarDigitoVerificadorNossoNumeroCodigoCedenteCaixaEconomica(String carteira, String nossoNumero) {
		try {
			String digitoVerificador = "";
			String numeroCalculo = carteira + nossoNumero;
			if (!numeroCalculo.equals("")) {
				String[] vetorNumeroCalculo = numeroCalculo.split("");
				int numMultiplicador = 2;
				int valor = 0;
				for (int num = vetorNumeroCalculo.length - 1; num >= 0; num--) {
					if (!vetorNumeroCalculo[num].equals("")) {
						valor = valor + (numMultiplicador * Integer.parseInt(vetorNumeroCalculo[num]));
						if (numMultiplicador == 9) {
							numMultiplicador = 2;
						} else {
							numMultiplicador++;
						}
					}
				}
				if (valor < 11) {
					digitoVerificador = String.valueOf(11 - valor);
				} else {
					if ((11 - (valor % 11)) > 9) {
						digitoVerificador = "0";
					} else {
						digitoVerificador = String.valueOf(11 - (valor % 11));
					}
				}
			}
			return digitoVerificador;
		} catch (Exception e) {
			return "0";
		}
	}

	public static String gerarDigitoVerificadorGeralCodigoBarrasCaixaEconomica(String codigoBarras) {
		try {
			String digitoVerificador = "";
			String numeroCalculo = codigoBarras;
			if (!numeroCalculo.equals("")) {
				String[] vetorNumeroCalculo = numeroCalculo.split("");
				int numMultiplicador = 2;
				int valor = 0;
				for (int num = vetorNumeroCalculo.length - 1; num >= 0; num--) {
					if (!vetorNumeroCalculo[num].equals("")) {
						valor = valor + (numMultiplicador * Integer.parseInt(vetorNumeroCalculo[num]));
						if (numMultiplicador == 9) {
							numMultiplicador = 2;
						} else {
							numMultiplicador++;
						}
					}
				}
				if (((11 - (valor % 11)) > 9) || ((11 - (valor % 11)) == 0)) {
					digitoVerificador = "1";
				} else {
					digitoVerificador = String.valueOf(11 - (valor % 11));
				}
			}
			return digitoVerificador;
		} catch (Exception e) {
			return "0";
		}
	}

	public static String gerarDigitoVerificadorLinhaDigitavelCaixaEconomica(String numero, int multiplicador) {
		try {
			String digitoVerificador = "0";
			String numeroCalculo = numero;
			if (!numeroCalculo.equals("")) {
				String[] vetorNumeroCalculo = numeroCalculo.split("");
				int numMultiplicador = multiplicador;
				int valor = 0;
				int multiplicacao = 0;
				int resto = 0;
				for (int num = vetorNumeroCalculo.length - 1; num >= 0; num--) {
					if (!vetorNumeroCalculo[num].equals("")) {
						multiplicacao = (numMultiplicador * Integer.parseInt(vetorNumeroCalculo[num]));
						if (multiplicacao >= 10) {
							multiplicacao = Integer.parseInt(String.valueOf(multiplicacao).substring(0, 1)) + Integer.parseInt(String.valueOf(multiplicacao).substring(1));
						}
						valor = valor + multiplicacao;
						if (numMultiplicador == 2) {
							numMultiplicador = 1;
						} else {
							numMultiplicador = 2;
						}
					}
				}
				resto = valor % 10;
				if (resto == 0) {
					digitoVerificador = "0";
				} else if (valor < 10) {
					digitoVerificador = String.valueOf(10 - valor);
				} else {
					digitoVerificador = String.valueOf(10 - resto);
				}
			}
			return digitoVerificador;
		} catch (Exception e) {
			return "0";
		}
	}

	/**
	 * Transfere um arquivo para a pasta
	 * padrÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚�
	 * �â„¢ÃƒÆ’Ã¢â‚�
	 * �Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã�
	 * �â€™ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã‚Â ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚�
	 * �Å¡Ã‚Â¬ÃƒÂ¢Ã¢â‚�
	 * �Å¾Ã‚Â¢ÃƒÆ’Ã†â€™Ãƒâ�
	 * � Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚�
	 * �Å¡Ã‚Â¬Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ€š
	 * Ã‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â�
	 * �ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ�
	 * �Ã…Â¾Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã�
	 * �â€™Ãƒâ€ Ã¢â‚¬
	 * â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã�
	 * �â€™Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â�
	 * �šÂ¬Ã�
	 * �Â¡Ãƒâ€šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ€šÃ‚Â�
	 * �ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬
	 * â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚�
	 * �Å¡Ã‚Â¬Ãƒâ€¦Ã‚Â�
	 * �ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€�
	 * �Â¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â£o de upload,
	 * definida por um servidor web.
	 *
	 * @param origem
	 *            Nome do arquivo na pasta
	 *            temporÃƒÆ’Ã†â€™Ãƒâ€�
	 *            �Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚�
	 *            �Â ÃƒÂ¢Ã¢â€šÂ�
	 *            �Ã¢â€žÂ¢ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â�
	 *            �šÂ¬Ã‚Â�
	 *            �ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â�
	 *            �ÃƒÂ¢Ã¢â‚�
	 *            �Å¾Ã‚Â¢ÃƒÆ’Ã†â€™Ãƒâ�
	 *            � Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚�
	 *            �Å¡Ã‚Â�
	 *            �Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ�
	 *            �šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â�
	 *            �šÂ¬Ã…Â¡Ãƒâ�
	 *            �šÃ‚Â¬ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â�
	 *            �šÂ¬Ã…Â¾Ãƒâ�
	 *            �šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ�
	 *            � Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚�
	 *            �Â ÃƒÂ¢Ã¢â€�
	 *            �Â¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€™Ãƒâ�
	 *            �šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â�
	 *            �šÂ¬Ã…Â¡Ãƒâ�
	 *            �šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ�
	 *            �šÃ‚Â¡ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬
	 *            â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚�
	 *            �Å¡Ã‚Â¬Ãƒâ�
	 *            �¦Ã‚Â¡ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â�
	 *            �šÂ¬Ã…Â�
	 *            �ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â¡ria.
	 * @param destino
	 *            Nome do arquivo final na pasta definitiva de upload no
	 *            servidor web.
	 * @throws Exception
	 *             Caso haja algum problema na
	 *             transferÃƒÆ’Ã†â€™Ãƒâ�
	 *             � Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚�
	 *             �Â ÃƒÂ¢Ã¢â€
	 *             šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€
	 *             ™ÃƒÂ¢Ã¢â€š
	 *             Â¬Ã‚Â ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚�
	 *             �Å¡Ã‚Â�
	 *             �ÃƒÂ¢Ã¢â‚¬Å¾Ã‚Â¢ÃƒÆ’Ã†â�
	 *             �™Ãƒâ�
	 *             � Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚�
	 *             �Å¡
	 *             Ã‚Â¬Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ�
	 *             �š
	 *             Ã‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡
	 *             Ãƒâ�
	 *             �šÃ‚Â¬ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã�
	 *             �Â¾Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ 
	 *             Ã¢â‚�
	 *             �â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ�
	 *             �Ã¢â€žÂ¢ÃƒÆ’Ã†â€™Ãƒâ€š
	 *             Ã‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â�
	 *             �šÂ¬Ã…Â¡Ãƒâ�
	 *             �šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ�
	 *             �šÃ‚Â¡ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬
	 *             â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚�
	 *             �Å¡Ã‚Â¬Ãƒâ�
	 *             �¦Ã‚Â¡ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â�
	 *             �šÂ¬Ã…
	 *             Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Âªncia, ou as
	 *             configuraÃƒÆ’Ã�
	 *             �â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚�
	 *             �Â 
	 *             ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€
	 *             ™ÃƒÂ¢Ã¢â�
	 *             �šÂ¬Ã‚Â ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚�
	 *             �Å¡Ã‚Â�
	 *             �ÃƒÂ¢Ã¢â‚¬Å¾Ã‚Â¢ÃƒÆ’Ã†â�
	 *             �™Ãƒâ�
	 *             � Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚�
	 *             �Å¡
	 *             Ã‚Â¬Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ�
	 *             �š
	 *             Ã‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡
	 *             Ãƒâ�
	 *             �šÃ‚Â¬ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã�
	 *             �Â¾Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ 
	 *             Ã¢â‚�
	 *             �â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ�
	 *             �Ã¢â€žÂ¢ÃƒÆ’Ã†â€™Ãƒâ€š
	 *             Ã‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â�
	 *             �šÂ¬Ã…Â¡Ãƒâ�
	 *             �šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ�
	 *             �šÃ‚Â¡ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬
	 *             â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚�
	 *             �Å¡Ã‚Â¬Ãƒâ�
	 *             �¦Ã‚Â¡ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â�
	 *             �šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â§
	 *             ÃƒÆ’Ã�
	 *             �â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚�
	 *             �Â 
	 *             ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€
	 *             ™ÃƒÂ¢Ã¢â�
	 *             �šÂ¬Ã‚Â ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚�
	 *             �Å¡Ã‚Â�
	 *             �ÃƒÂ¢Ã¢â‚¬Å¾Ã‚Â¢ÃƒÆ’Ã†â�
	 *             �™Ãƒâ�
	 *             � Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚�
	 *             �Å¡
	 *             Ã‚Â¬Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ�
	 *             �š
	 *             Ã‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡
	 *             Ãƒâ�
	 *             �šÃ‚Â¬ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã�
	 *             �Â¾Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ 
	 *             Ã¢â‚�
	 *             �â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ�
	 *             �Ã¢â€žÂ¢ÃƒÆ’Ã†â€™Ãƒâ€š
	 *             Ã‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â�
	 *             �šÂ¬Ã…Â¡Ãƒâ�
	 *             �šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ�
	 *             �šÃ‚Â¡ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬
	 *             â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚�
	 *             �Å¡Ã‚Â¬Ãƒâ�
	 *             �¦Ã‚Â¡ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â�
	 *             �šÂ¬Ã…
	 *             Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Âµes do sistema
	 *             nÃƒÆ’Ã�
	 *             �â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚�
	 *             �Â 
	 *             ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€
	 *             ™ÃƒÂ¢Ã¢â�
	 *             �šÂ¬Ã‚Â ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚�
	 *             �Å¡Ã‚Â�
	 *             �ÃƒÂ¢Ã¢â‚¬Å¾Ã‚Â¢ÃƒÆ’Ã†â�
	 *             �™Ãƒâ�
	 *             � Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚�
	 *             �Å¡
	 *             Ã‚Â¬Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ�
	 *             �š
	 *             Ã‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡
	 *             Ãƒâ�
	 *             �šÃ‚Â¬ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã�
	 *             �Â¾Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ 
	 *             Ã¢â‚�
	 *             �â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ�
	 *             �Ã¢â€žÂ¢ÃƒÆ’Ã†â€™Ãƒâ€š
	 *             Ã‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â�
	 *             �šÂ¬Ã…Â¡Ãƒâ�
	 *             �šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ�
	 *             �šÃ‚Â¡ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬
	 *             â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚�
	 *             �Å¡Ã‚Â¬Ãƒâ�
	 *             �¦Ã‚Â¡ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â�
	 *             �šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â£o
	 *             estÃƒÆ’Ã�
	 *             �â€™Ãƒâ€ Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚�
	 *             �Â 
	 *             ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã†â€
	 *             ™ÃƒÂ¢Ã¢â�
	 *             �šÂ¬Ã‚Â ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚�
	 *             �Å¡Ã‚Â�
	 *             �ÃƒÂ¢Ã¢â‚¬Å¾Ã‚Â¢ÃƒÆ’Ã†â�
	 *             �™Ãƒâ�
	 *             � Ã¢â‚¬â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚�
	 *             �Å¡
	 *             Ã‚Â¬Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ�
	 *             �š
	 *             Ã‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡
	 *             Ãƒâ�
	 *             �šÃ‚Â¬ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â€šÂ¬Ã�
	 *             �Â¾Ãƒâ€šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ€ 
	 *             Ã¢â‚�
	 *             �â„¢ÃƒÆ’Ã¢â‚¬Â ÃƒÂ¢Ã¢â€šÂ�
	 *             �Ã¢â€žÂ¢ÃƒÆ’Ã†â€™Ãƒâ€š
	 *             Ã‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â�
	 *             �šÂ¬Ã…Â¡Ãƒâ�
	 *             �šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ�
	 *             �šÃ‚Â¡ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬
	 *             â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚�
	 *             �Å¡Ã‚Â¬Ãƒâ�
	 *             �¦Ã‚Â¡ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â�
	 *             �šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â£o
	 *             definidas corretamente.
	 * @since 12/07/2011
	 * @author VinÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬
	 *         â„¢ÃƒÆ’Ã¢â‚�
	 *         �Â ÃƒÂ¢Ã¢â€šÂ¬Ã¢â€žÂ¢ÃƒÆ’Ã�
	 *         �â€™ÃƒÂ¢Ã¢â�
	 *         �šÂ¬Ã‚Â ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚�
	 *         �Å¡Ã‚Â¬
	 *         ÃƒÂ¢Ã¢â‚¬Å¾Ã‚Â¢ÃƒÆ’Ã†â€™Ãƒâ�
	 *         � Ã¢â‚�
	 *         �â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â�
	 *         �Ãƒâ€šÃ‚Â ÃƒÆ’Ã†â€™Ãƒâ€š
	 *         Ã‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â�
	 *         �šÂ¬Ã…Â¡Ãƒâ�
	 *         �šÃ‚Â¬ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â�
	 *         �šÂ¬Ã…Â¾Ãƒâ�
	 *         �šÃ‚Â¢ÃƒÆ’Ã†â€™Ãƒâ�
	 *         � Ã¢â‚¬â„¢ÃƒÆ’Ã¢â‚�
	 *         �Â ÃƒÂ¢Ã¢â€šÂ�
	 *         �Ã¢â€žÂ¢ÃƒÆ’Ã†â€™Ãƒâ�
	 *         �šÃ‚Â¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â�
	 *         �šÂ¬Ã…Â¡Ãƒâ�
	 *         �šÃ‚Â¬ÃƒÆ’Ã¢â‚¬Â¦Ãƒâ�
	 *         �šÃ‚Â¡ÃƒÆ’Ã†â€™Ãƒâ€ Ã¢â‚¬
	 *         â„¢ÃƒÆ’Ã‚Â¢ÃƒÂ¢Ã¢â‚�
	 *         �Å¡Ã‚Â¬Ãƒâ€
	 *         ¦Ã‚Â¡ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â�
	 *         �šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Â­cius
	 *         Bueno
	 */
//	public static String fazerUploadArquivoPastaServidor(final String origem, final String destino, ConfiguracaoGeralSistemaVO configuracaoGeralSistemaVO) throws Exception {
//		URL url = null;
//		File src = null;
//		String urlPastaServidor = null;
//		byte[] buffer = new byte[4096];
//		BufferedInputStream bis = null;
//		BufferedOutputStream bos = null;
//		HttpURLConnection httpUrl = null;
//		BASE64Encoder codificador = new BASE64Encoder();
//		try {
//			src = new File(getDiretorioWebTemporario().concat("/").concat(origem));
//			if (!src.exists()) {
//				return null;
//			}
//			bis = new BufferedInputStream(new FileInputStream(src));
//			urlPastaServidor = configuracaoGeralSistemaVO.getUrlExternoDownloadArquivo();
//			url = new URL(urlPastaServidor.concat("/").concat(destino));
//			httpUrl = (HttpURLConnection) url.openConnection();
//			httpUrl.setDoOutput(true);
//			httpUrl.setRequestMethod("PUT");
//			httpUrl.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
//			httpUrl.connect();
//			bos = new BufferedOutputStream(httpUrl.getOutputStream());
//			// Escreve dados na rede.
//			int tamanho;
//			while ((tamanho = bis.read(buffer)) > 0) {
//				bos.write(buffer, 0, tamanho);
//			}
//			bis.close();
//			bos.flush();
//			bos.close();
//			httpUrl.disconnect();
//			src.delete();
//			return httpUrl.getResponseMessage();
//		} finally {
//			bis = null;
//			src = null;
//			url = null;
//			buffer = null;
//			httpUrl = null;
//			codificador = null;
//			urlPastaServidor = null;
//			// tokenAutenticacao = null;
//		}
//	}

	public static Integer getParteInteiraDouble(Double valor) {
		String string = valor.toString();
		Integer parteInteira = 0;
		if (string.contains(".")) {
			int pos = string.indexOf(".");
			parteInteira = Integer.parseInt(string.substring(0, pos));
		}
		return parteInteira;
	}

	public static Integer getParteDecimalDoubleDuasCasas(Double valor) {
		String string = valor.toString();
		Integer parteDecimal = 0;
		if (string.contains(".")) {
			int pos = string.indexOf(".");
			String parteDecimalString = string.substring(pos + 1, string.length());
			if (parteDecimalString.length() > 1) {
				parteDecimal = Integer.parseInt(parteDecimalString.substring(0, 2));
			} else {
				parteDecimal = Integer.parseInt(parteDecimalString + 0);
			}
		}
		return parteDecimal;
	}

	public static Long getParteDecimalDouble(Double valor) {
		String string = valor.toString();
		Long parteDecimal = (long) 0;
		if (string.contains(".")) {
			int pos = string.indexOf(".");
			parteDecimal = Long.parseLong(string.substring(pos + 1, string.length()));
		}
		return parteDecimal;
	}

//	@SuppressWarnings("static-access")
//	public static Date getNextWorkingDay(Date aDate, List<FeriadoVO> listaFeriadoVOs) {
//		Date dataIncremental = aDate;
//		Date dataAuxIncremental = aDate;
//		while (!isWorkingDay(dataAuxIncremental, listaFeriadoVOs)) {
//			dataAuxIncremental = new Date(dataAuxIncremental.getTime() + ONE_DAY);
//		}
//		Calendar cal1 = new GregorianCalendar();
//		cal1.setTime(dataAuxIncremental);
//		Calendar cal2 = new GregorianCalendar();
//		cal2.setTime(dataIncremental);
//		if (cal1.get(cal1.MONTH) != cal2.get(cal2.MONTH)) {
//			Date dataRetroativo = aDate;
//			while (!isWorkingDay(dataRetroativo, listaFeriadoVOs)) {
//				dataRetroativo = new Date(dataRetroativo.getTime() - ONE_DAY);
//			}
//			return dataRetroativo;
//		} else {
//			dataIncremental = dataAuxIncremental;
//			return dataIncremental;
//		}
//	}

//	public static boolean isWorkingDay(Date date, List<FeriadoVO> listaFeriadoVOs) {
//		if (!Uteis.isWorkingWeekDay(date)) {
//			return false;
//		}
//
//		for (FeriadoVO holiday : listaFeriadoVOs) {
//			if (holiday.isHoliday(date)) {
//				return false;
//			}
//		}
//
//		return true;
//	}

	public static double truncar(double valor, int casasDecimais) {
		BigDecimal bigDecimal = new BigDecimal(valor);
		bigDecimal = bigDecimal.setScale(casasDecimais, BigDecimal.ROUND_HALF_UP);
		return bigDecimal.doubleValue();
		// double multiplicador = Double.parseDouble(String.format("1%0" +
		// casasDecimais + "d", 0));
		// long aux = (long) (valor * multiplicador);
		// return (double) aux / multiplicador;
	}

//	public static String realizarMontagemDadosGraficoPizza(List<LegendaGraficoVO> listaDadosAcesso) {
//
//		StringBuilder resultado = new StringBuilder("");
//
//		double total = 0.0;
//		for (LegendaGraficoVO obj : listaDadosAcesso) {
//			total += obj.getValor();
//		}
//		for (LegendaGraficoVO obj : listaDadosAcesso) {
//			if (!resultado.toString().isEmpty()) {
//				resultado.append(", ");
//			}
//			resultado.append("['").append(Uteis.removerAcentos(obj.getNome()).replaceAll("ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Âª", "").replaceAll("ÃƒÆ’Ã†â€™ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡ÃƒÆ’Ã¢â‚¬Å¡Ãƒâ€šÃ‚Âº", "")).append("', ");
//			resultado.append(Uteis.truncar(obj.getValor() * 100.0 / total, 2)).append(", '").append("R$" + Uteis.getDoubleFormatado(obj.getValor()));
//			resultado.append("', '").append(obj.getCor()).append("', '").append(obj.getNivel()).append("', ").append(obj.getCodigo()).append("]");
//		}
//		return resultado.toString();
//
//	}

	/**
	 * getModulo11
	 * 
	 * @param String
	 *            value
	 * @param int type
	 * @return int dac
	 * @author Mario Grigioni
	 */
	public static String getModulo11Santander(String campo, int type) {
		// Modulo 11 - 234567 (type = 7)
		// Modulo 11 - 23456789 (type = 9)

		int multiplicador = 2;
		int multiplicacao = 0;
		int soma_campo = 0;

		for (int i = campo.length(); i > 0; i--) {
			multiplicacao = Integer.parseInt(campo.substring(i - 1, i)) * multiplicador;

			soma_campo = soma_campo + multiplicacao;

			multiplicador++;
			if (multiplicador > 7 && type == 7) {
				multiplicador = 2;
			} else if (multiplicador > 9 && type == 9) {
				multiplicador = 2;
			}
		}

		int dac = (soma_campo / 11) * 11;
		dac = soma_campo - dac;

		if (dac == 10) {
			dac = 1;
		} else if ((dac == 0 || dac == 1) && type == 9) {
			dac = 0;
		} else {
			dac = 11 - dac;
		}

		return ((Integer) dac).toString();
	}

	public static double arredondarMultiploDeCincoParaCima(Double valorBase) {
		Integer valorInt = valorBase.intValue();
		Double decimal = Uteis.arrendondarForcando2CadasDecimais(valorBase - valorInt);
		if (decimal >= 0.01 && decimal <= 0.49) {
			return valorInt + 0.50;
		} else if (decimal >= 0.51 && decimal <= 0.99) {
			return valorInt + 1;
		}
		return valorBase;
	}

	/*
	 * Mï¿½todo responsï¿½vel em copiar parte do valor de entrada
	 * 
	 * @author Wendel Rodrigues
	 */
	public static String copiarDelimitandoTamanhoDoTexto(String texto, int tamanhoMaximo) {
		int tamanhoTexto = texto.length();

		if (tamanhoTexto >= tamanhoMaximo) {
			return texto.substring(0, tamanhoMaximo);
		} else {
			return texto;
		}

	}

	/*
	 * Mï¿½todo responsï¿½vel em preencher com espaï¿½os vazios a
	 * direita do valor de entrada, limitando ao tamanho mï¿½ximo de
	 * caracteres.
	 * 
	 * @author Wendel Rodrigues
	 */
	public static String preencherComVazioLimitandoTamanho(String texto, int tamanhoMaximo) {
		int tamanhoTexto = texto.length();

		if (tamanhoTexto >= tamanhoMaximo) {
			return texto.substring(0, tamanhoMaximo);
		} else {
			int quantidadeZeros = tamanhoMaximo - tamanhoTexto;
			for (int i = 0; i < quantidadeZeros; i++) {
				texto += " ";
			}
			return texto;
		}

	}

	/*
	 * Mï¿½todo responsï¿½vel em preencher com zeros a direita do valor de
	 * entrada, limitando ao tamanho mï¿½ximo de caracteres.
	 * 
	 * @author Wendel Rodrigues
	 */
	public static String preencherComZerosLimitandoTamanho(Integer numero, int tamanhoMaximo) {
		int tamanhoNumero = numero.toString().length();

		if (tamanhoNumero >= tamanhoMaximo) {
			return numero.toString().substring(0, tamanhoMaximo);
		} else {
			int quantidadeZeros = tamanhoMaximo - tamanhoNumero;
			String numeroConvertido = numero.toString();

			for (int i = 0; i < quantidadeZeros; i++) {
				numeroConvertido += "0";
			}
			return numeroConvertido;
		}

	}

	public static String retornarDuasCasasDecimais(String valor) {
		String decimal = valor.substring(valor.lastIndexOf(".") + 1);
		if (decimal.length() == 1) {
			return valor + "0";
		}
		return valor;
	}

	/*
	 * Mï¿½todo responsï¿½vel em retornar prefixo ou sufixo do CEP.
	 * 
	 * @author Wendel Rodrigues
	 */
	public static String retornarPrefixoOuSufixoDoCep(String cep, String retorno) {
		if (cep.equals("")) {
			return "";
		}
		String s[] = cep.split("-");

		if (s.length == 1) {
			String c = cep.substring(0, 5) + "-" + cep.substring(5, cep.length());
			s = c.split("-");
		}

		if (retorno.equals("prefixo")) {
			return s[0];
		} else {
			return s[1];
		}
	}

	/*
	 * Mï¿½todo responsï¿½vel em retornar a URL da imagem da tag img do
	 * HTML.
	 * 
	 * @author Wendel Rodrigues
	 */
	public static String retornarURLDaImagemHtml(String linha) {
		try {
			Pattern p = null;
			Matcher m = null;
			String word0 = "";

			p = Pattern.compile(".*<img[^>]*src=\"([^\"]*)", Pattern.CASE_INSENSITIVE);
			m = p.matcher(linha);
			while (m.find()) {
				word0 = m.group(1);
				return word0.toString();
			}
			return "";
		} catch (Exception e) {
			return "";
		}
	}

	public static String retornarNomeDaImagem(String link) {
		String[] texto = link.split("/");
		String nomeArquivo = "";
		for (int i = 0; i < texto.length; i++) {
			if ((i + 1) == texto.length) {
				nomeArquivo = texto[i];
			}
		}
		return nomeArquivo;
	}

	/*
	 * Mï¿½todo responsï¿½vel em fazer download da imagem passada por
	 * parametro, salvando dento do diretorio padrï¿½o de Upload.
	 * 
	 * @return Retorna onde o arquivo de imagem foi salvo.
	 * 
	 * @author Wendel Rodrigues
	 */
	public String executarDownloadImagemDiretorioPadraoUpload(String link, String diretorio) {
		try {

			String diretorioArquivo = diretorio + File.separator + retornarNomeDaImagem(link);

			final int BUFFER_SIZE = 1024 * 1024;
			URL url = new URL(link);
			BufferedInputStream stream = new BufferedInputStream(url.openStream(), BUFFER_SIZE);
			BufferedOutputStream fos = new BufferedOutputStream(new FileOutputStream(diretorioArquivo));
			byte buf[] = new byte[BUFFER_SIZE];
			int numBytesRead;
			do {
				numBytesRead = stream.read(buf);
				if (numBytesRead > 0) {
					fos.write(buf, 0, numBytesRead);
				}
			} while (numBytesRead > 0);
			fos.flush();
			fos.close();
			stream.close();
			buf = null;
			return diretorioArquivo;

		} catch (MalformedURLException e1) {
			e1.printStackTrace();
			return "";
		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}

	public static String removeHTML(String htmlString) {
		String noHTMLString = htmlString.replaceAll("<.*?>", "");
		noHTMLString = noHTMLString.replaceAll("r", "<br/>");
		noHTMLString = noHTMLString.replaceAll("\n", " ");
		noHTMLString = noHTMLString.replaceAll("'", "&#39;");
		noHTMLString = noHTMLString.replaceAll("\"/", "&quot;");
		noHTMLString = noHTMLString.replaceAll("Untitled document", "");
		return noHTMLString;
	}

	public static Integer arredondarDivisaoEntreNumeros(Double valor) {
		String decimos = String.valueOf(valor).substring(String.valueOf(valor).indexOf(".") + 1, String.valueOf(valor).length());
		if (Double.parseDouble(decimos) > 0) {
			valor = valor + 1;
		}
		return valor.intValue();
	}

	public static Date getDataPassadaPorDataAtual(Integer quantidadeDias) throws Exception {
		try {
			Calendar data = Calendar.getInstance();
			data.add(Calendar.DAY_OF_MONTH, -quantidadeDias);
			data.set(Calendar.HOUR_OF_DAY, 0);
			data.set(Calendar.MINUTE, 0);
			data.set(Calendar.SECOND, 0);
			data.set(Calendar.MILLISECOND, 0);
			return data.getTime();
		} catch (Exception e) {
			return new Date();
		}
	}

	public static String getValorTruncadoDeDoubleParaString(Double val, int precision) {
		return trunc(new BigDecimal(val), precision).toString();
	}

	public static BigDecimal trunc(BigDecimal val, int precision) {
		if (precision <= 0) {
			throw new IllegalArgumentException("Precisão deve ser maior que zero");
		}
		return val.setScale(precision, BigDecimal.ROUND_DOWN);
	}

//	public static double arrendondarForcando2CasasDecimais(double valor) {
//		boolean numeroNegativo = false;
//		if (valor < 0) {
//			numeroNegativo = true;
//		}
//		valor = Uteis.arredondar(valor, 2, 1);
//		String valorStr = String.valueOf(valor);
//
//		String inteira = valorStr.substring(0, valorStr.indexOf("."));
//		String extensao = valorStr.substring(valorStr.indexOf(".") + 1, valorStr.length());
//		if (extensao.length() == 1) {
//			extensao += "0";
//		}
//		valorStr = UteisTexto.removerMascara(inteira) + "." + extensao;
//		if (!numeroNegativo) {
//			return Double.parseDouble(valorStr);
//		} else {
//			return (-1.0 * Double.parseDouble(valorStr));
//		}
//	}

	public static String adicionarMascaraCodigoBarraNFE(String valor) {
		String mascara = "XXXX.XXXX.XXXX.XXXX.XXXX.XXXX.XXXX.XXXX.XXXX.XXXX.XXXX";
		if (valor.length() == 44) {
			String parte1 = valor.substring(0, 4);
			String parte2 = valor.substring(4, 8);
			String parte3 = valor.substring(8, 12);
			String parte4 = valor.substring(12, 16);
			String parte5 = valor.substring(16, 20);
			String parte6 = valor.substring(20, 24);
			String parte7 = valor.substring(24, 28);
			String parte8 = valor.substring(28, 32);
			String parte9 = valor.substring(32, 36);
			String parte10 = valor.substring(36, 40);
			String parte11 = valor.substring(40, 44);
			return parte1 + "." + parte2 + "." + parte3 + "." + parte4 + "." + parte5 + "." + parte6 + "." + parte7 + "." + parte8 + "." + parte9 + "." + parte10 + "." + parte11;
		} else {
			return mascara;
		}
	}

//	public static String getMontarCodigoBarra(String codigo, int tamanhoCodigoBarra) throws ConsistirException {
//		int tamanhoCodigo = codigo.length();
//		String codigoBarra = "";
//		if (tamanhoCodigoBarra < tamanhoCodigo) {
//			throw new ConsistirException("A Quantidade de caracteres do C�digo de Barra esta pequeno");
//		}
//		int diferenca = tamanhoCodigoBarra - tamanhoCodigo;
//		while (diferenca > 0) {
//			codigoBarra = codigoBarra + "0";
//			diferenca--;
//		}
//
//		return codigoBarra + codigo;
//	}

	public static void realizarCopiaArquivo(String diretorioTemp, String diretorioOriginal, Boolean validarDiretorio) throws Exception {
		File arquivoTemp = new File(diretorioTemp);
		File arquivoOriginal = new File(diretorioOriginal);
		if (validarDiretorio) {
			if (!arquivoTemp.exists()) {
				arquivoTemp.getParentFile().mkdir();
			}
			if (!arquivoOriginal.exists()) {
				arquivoOriginal.getParentFile().mkdir();
			}
		}
		if (!arquivoOriginal.exists()) {
			InputStream in = new FileInputStream(arquivoTemp);
			OutputStream out = new FileOutputStream(arquivoOriginal, true);
			byte[] buf = new byte[1024];
			int len;
			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}
			in.close();
			out.close();
		}
	}

	public static String getCasasDecimais(String numero, int quantidadeCasas) throws Exception {
		int tamanhoNumero = numero.length();
		int posicaoPonto = numero.indexOf(".");
		int quantidadeZeros = tamanhoNumero - (posicaoPonto + 1);
		while (quantidadeZeros < quantidadeCasas) {
			numero = numero + "0";
			quantidadeZeros++;
		}
		return numero;
	}

	public static String getPreencherComZeroEsquerda(String codigo, int tamanhoCodigoBarra) throws Exception {
		int tamanhoCodigo = codigo.length();
		String codigoBarra = "";

		int diferenca = tamanhoCodigoBarra - tamanhoCodigo;
		while (diferenca > 0) {
			codigoBarra = codigoBarra + "0";
			diferenca--;
		}

		return codigoBarra + codigo;
	}

	public static double arrendondarForcando4CadasDecimais(double valor) {
		valor = arredondar(valor, 4, 1);
		String valorStr = String.valueOf(valor);

		String inteira = valorStr.substring(0, valorStr.indexOf("."));
		String extensao = valorStr.substring(valorStr.indexOf(".") + 1, valorStr.length());
		if (extensao.length() == 1) {
			extensao += "0";
		}
		valorStr = removerMascara(inteira) + "." + extensao;
		return Double.parseDouble(valorStr);
	}

	public static String getFusoHorario() {
		Calendar c = Calendar.getInstance(TimeZone.getDefault());
		c.setTime(new Date());
		Integer offset = c.get(Calendar.DST_OFFSET);
		Integer fuso = 0;

		if (!offset.equals(0)) {
			fuso = TimeZone.getTimeZone("Brazil/DeNoronha").getRawOffset() / (60 * 60 * 1000);
		} else
			fuso = TimeZone.getDefault().getRawOffset() / (60 * 60 * 1000);

		if (fuso < 0) {
			if (fuso < -9) {
				return "" + String.valueOf(fuso) + ":00";
			} else {
				return "-0" + String.valueOf(fuso * -1) + ":00";
			}

		} else if (fuso > 9) {
			return "" + String.valueOf(fuso) + ":00";
		} else {
			return "0" + String.valueOf(fuso) + ":00";
		}
	}

	public static String getDataComHoraCompleta(java.util.Date dataConverter) {
		DateFormat formatador = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS");
		String dataStr = formatador.format(dataConverter);
		return dataStr;
	}

	public static java.util.Date getDateHoraInicialDia(Date dataPrm) throws Exception {
		java.util.Date valorData = null;
		DateFormat formatador = DateFormat.getDateInstance(DateFormat.SHORT);
		valorData = formatador.parse(Uteis.getData(dataPrm));
		Calendar cal = Calendar.getInstance();
		cal.setTime(valorData);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}

	public static Double realizarCalculoFormula(String formulaCalculoMediaFinal) {
		ScriptEngineManager factory = new ScriptEngineManager();
		// create a JavaScript engine
		ScriptEngine engine = factory.getEngineByName("JavaScript");
		// evaluate JavaScript code from String
		try {			
			Object result = engine.eval(formulaCalculoMediaFinal);
			if (result instanceof Double) {
				return (Double) result;
			}
		} catch (ScriptException e) {
		}
		return null;
	}

	public static boolean validaCPF(String CPF) {
		// considera-se erro CPF's formados por uma sequencia de numeros iguais
		if (CPF.equals("00000000000") || CPF.equals("11111111111") || CPF.equals("22222222222") || CPF.equals("33333333333") || CPF.equals("44444444444") || CPF.equals("55555555555") || CPF.equals("66666666666") || CPF.equals("77777777777") || CPF.equals("88888888888") || CPF.equals("99999999999") || (CPF.length() != 11))
			return (false);

		char dig10, dig11;
		int sm, i, r, num, peso;

		// "try" - protege o codigo para eventuais erros de conversao de tipo
		// (int)
		try {
			// Calculo do 1o. Digito Verificador
			sm = 0;
			peso = 10;
			for (i = 0; i < 9; i++) {
				// converte o i-esimo caractere do CPF em um numero:
				// por exemplo, transforma o caractere '0' no inteiro 0
				// (48 eh a posicao de '0' na tabela ASCII)
				num = (int) (CPF.charAt(i) - 48);
				sm = sm + (num * peso);
				peso = peso - 1;
			}

			r = 11 - (sm % 11);
			if ((r == 10) || (r == 11))
				dig10 = '0';
			else
				dig10 = (char) (r + 48); // converte no respectivo caractere
											// numerico

			// Calculo do 2o. Digito Verificador
			sm = 0;
			peso = 11;
			for (i = 0; i < 10; i++) {
				num = (int) (CPF.charAt(i) - 48);
				sm = sm + (num * peso);
				peso = peso - 1;
			}

			r = 11 - (sm % 11);
			if ((r == 10) || (r == 11))
				dig11 = '0';
			else
				dig11 = (char) (r + 48);

			// Verifica se os digitos calculados conferem com os digitos
			// informados.
			if ((dig10 == CPF.charAt(9)) && (dig11 == CPF.charAt(10)))
				return (true);
			else
				return (false);
		} catch (InputMismatchException erro) {
			return (false);
		}
	}

	public static Date getDataInicioSemestreAno(Integer ano, Integer semestre) {
		if (ano > 0) {
			if (semestre > 0) {
				if (semestre.equals(1)) {
					return new GregorianCalendar(ano, 0, 1).getTime();
				} else {
					return new GregorianCalendar(ano, 6, 21).getTime();
				}
			}
			return new GregorianCalendar(ano, 0, 1).getTime();
		}
		return null;
	}

	public static Date getDataFimSemestreAno(Integer ano, Integer semestre) {
		if (ano > 0) {
			if (semestre > 0) {
				if (semestre.equals(1)) {
					return new GregorianCalendar(ano, 6, 20).getTime();
				} else {
					return new GregorianCalendar(ano, 11, 31).getTime();
				}
			}
			return new GregorianCalendar(ano, 11, 31).getTime();
		}
		return null;
	}

//	public static boolean verificarFormulaEstaCorreta(String formula, boolean retornoTipoBoolean) {
//		ScriptEngineManager factory = new ScriptEngineManager();
//		// create a JavaScript engine
//		ScriptEngine engine = factory.getEngineByName("JavaScript");
//		// evaluate JavaScript code from String
//		if (!formula.trim().isEmpty()) {
//			Object result;
//			try {
//				formula = formula.replaceAll("e", "&&");
//				formula = formula.replaceAll("E", "&&");
//				formula = formula.replaceAll("ou", "||");
//				formula = formula.replaceAll("OU", "||");
//				formula = formula.replaceAll("=", "==");
//				formula = formula.replaceAll("====", "==");
//				formula = formula.replaceAll(">==", ">=");
//				formula = formula.replaceAll("<==", "<=");
//				formula = formula.replaceAll("!==", "!=");
//				try {
//					formula = ConfiguracaoAcademicoVO.validarUsoFuncaoMaior(formula, 0, "");
//				} catch (ConsistirException e) {
//					return false;
//				}
//				result = engine.eval(formula);
//				if (retornoTipoBoolean) {
//					return (result instanceof Boolean);
//				}
//				return (result instanceof Double);
//			} catch (ScriptException e) {
//				return false;
//			}
//
//		}
//		return true;
//	}

	/**
	 * @author Victor Hugo
	 */
	public static int arredondarParaMais(double valor) {
		valor = (new BigDecimal(valor).setScale(0, BigDecimal.ROUND_UP).intValue());
		return (int) valor;
	}

	public static java.util.Date getDateComHoraAntesDiaSeguinte(Date dataPrm) throws Exception {
		java.util.Date valorData = null;
		DateFormat formatador = DateFormat.getDateInstance(DateFormat.SHORT);
		valorData = formatador.parse(Uteis.getData(dataPrm));
		Calendar cal = Calendar.getInstance();

		cal.setTime(valorData);
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);

		return cal.getTime();
	}

	public static long getDataQuantidadeDiasEntreDatasDesconsiderandoDiasUteis(Date dataIni, Date dataFim) throws Exception {
		GregorianCalendar ini = new GregorianCalendar();
		GregorianCalendar fim = new GregorianCalendar();
		ini.setTime(dataIni);
		fim.setTime(dataFim);
		long dt1 = ini.getTimeInMillis();
		long dt2 = fim.getTimeInMillis();
		long result = (int) (((dt2 - dt1) / 86400000) + 1);

		return result;
	}

	public static boolean isDate(Date dataValidar) {
		String dia;
		String mes;
		String ano;

		StringTokenizer token = new StringTokenizer(getData(dataValidar, "dd/MM/yyyy"), "/");
		try {
			dia = token.nextToken();
			mes = token.nextToken();
			ano = token.nextToken();

			if (dia.length() < 2 || dia.length() > 2)
				return false;
			if (mes.length() < 2 || mes.length() > 2)
				return false;
			if (ano.length() < 4 || dia.length() > 4)
				return false;

			int intDia = Integer.parseInt(dia);
			int intMes = Integer.parseInt(mes);
			int intAno = Integer.parseInt(ano);

			if (String.valueOf(intAno).length() < 4 || String.valueOf(intAno).length() > 4)
				return false;

			if (intMes < 1 || intMes > 12)
				return false;

			if (intMes == 1 || intMes == 3 || intMes == 5 || intMes == 7 || intMes == 8 || intMes == 12) {
				if (intDia < 1 || intDia > 31)
					return false;
			} else if (intMes == 4 || intMes == 6 || intMes == 9 || intMes == 10 || intMes == 11) {
				if (intDia < 1 || intDia > 30)
					return false;
			} else if (intMes == 2) {
				if (intDia < 1 || intDia > 29)
					return false;
			}
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	
	public static String formataMascaraCEP(String cep) {
		String ret = "";
		int tam;
		try {
			if (cep != null) {
				tam = cep.length();
				if (tam == 8) {
					ret = cep.substring(0, tam - 6) + "." + cep.substring(tam - 6, tam - 3) + "-" + cep.substring(tam - 3);
				}
			}
		} catch (Exception err) {
		}
		return ret;
	}

	public static Boolean validarMascaraCEP(String CEP) {
		return CEP.matches("\\d{2}.\\d{3}-\\d{3}");
	}
	
	/**
	 * Respons�vel por realizar a formata��o de um valor Double de acordo com a
	 * quantidade m�xima de casas decimais.
	 * 
	 * @author Wellington 19/02/2015
	 * @param valor
	 * @param quantidadeCasasDecimaisPermitirAposVirgula
	 * @return
	 */
	public static String formatarDeAcordoQuantidadeCasasDecimaisAposVirgula(Double valor, int quantidadeCasasDecimaisPermitirAposVirgula) {
		if (valor == null) {
			return "";
		}
		final NumberFormat nf = NumberFormat.getInstance();
		nf.setMinimumFractionDigits(1);
		nf.setMaximumFractionDigits(quantidadeCasasDecimaisPermitirAposVirgula);
		return nf.format(valor);
	}
	
	public static Date getDataFuturaConsiderandoDataAtual(Date dataInicial, int quantidadeAvancar) throws Exception {
		dataInicial = getDateSemHora(dataInicial);
		if (quantidadeAvancar == 0) {
			return dataInicial;
		}
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(getDataPassada(dataInicial, 0));
		gc.add(GregorianCalendar.DAY_OF_MONTH, quantidadeAvancar);
		return getDateSemHora(gc.getTime());
	}
	

//	public static Integer getIdObjetoPersistir(Object objeto, String keyName) throws Exception {
//	
//			if (objeto != null) {
//				Integer i = (Integer) UtilReflexao.invocarMetodoGet(objeto, keyName);
//				return (i != null && !i.equals(0)) ? i : null;
//			}
//			return null;
//	}
	
	public static Double realizarCalculoFormulaCalculo(String formula) throws ScriptException {
		ScriptEngineManager factory = new ScriptEngineManager();
		// create a JavaScript engine
		ScriptEngine engine = factory.getEngineByName("JavaScript");
		// evaluate JavaScript code from String
		Object result;		
		result = engine.eval(formula);
		if (result instanceof Double) {
			return (Double) result;
		}
		return null;
	}
	
	/**
	 * Respons�vel por realizar a formata��o de um valor Double de acordo com a
	 * quantidade m�xima de casas decimais.
	 * 
	 * @author Wellington Rodrigues - 17/06/2015 
	 * @param valor
	 * @param quantidadeCasasDecimaisPermitirAposVirgula
	 * @return
	 */
	public static Double formatarDeAcordoQuantidadeCasasDecimaisAposVirgulaDouble(Double valor, int quantidadeCasasDecimaisPermitirAposVirgula) {
		if (valor == null) {
			return null;
		}
		final NumberFormat nf = NumberFormat.getInstance();
		nf.setMinimumFractionDigits(1);
		nf.setMaximumFractionDigits(quantidadeCasasDecimaisPermitirAposVirgula);
		return Double.parseDouble(nf.format(valor).replaceAll("\\.", "").replace(",", "."));
	}
	
	public static String removerCaracteresEspeciais3(String valor) throws Exception {
		String retorno = valor;
		retorno = Normalizer.normalize(retorno, Normalizer.Form.NFD);
		retorno = retorno.replaceAll("[^\\p{ASCII}]", "");
		return retorno;
	}

	public static String generoEstado(String estado) throws Exception {
		String generode = "Estado de ";
		String generodo = "Estado do ";
		String generoda = "Estado da ";
		String generoretorno = "";
		if (estado.equalsIgnoreCase("AC") || estado.equalsIgnoreCase("AP") || estado.equalsIgnoreCase("AM")
				|| estado.equalsIgnoreCase("ES") || estado.equalsIgnoreCase("CE")|| estado.equalsIgnoreCase("MA")
				|| estado.equalsIgnoreCase("PR") || estado.equalsIgnoreCase("PI")|| estado.equalsIgnoreCase("RJ")
				|| estado.equalsIgnoreCase("RN") || estado.equalsIgnoreCase("RS")|| estado.equalsIgnoreCase("TO")
				|| estado.equalsIgnoreCase("MT") || estado.equalsIgnoreCase("MS")|| estado.equalsIgnoreCase("PA")) {
			generoretorno = generodo; 
		} else if (estado.equalsIgnoreCase("BA") || estado.equalsIgnoreCase("PB")) {
			generoretorno = generoda;
		} else if (estado.equalsIgnoreCase("DF")) {
			generoretorno = "";
		} else {
			generoretorno = generode;
		}
		return generoretorno;
	}

	public static Date getDataYYYMMDD(String dataStr) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date data = sdf.parse(dataStr);			
			return data;
		} catch (Exception e) {
			return new Date();
		}
	}
	
	public static String formatarDeAcordoQuantidadeCasasDecimaisAposVirgulaStr(Double valor, int quantidadeCasasDecimaisPermitirAposVirgula) {
		if (!isAtributoPreenchido(valor)) {
			return null;
		}
		final NumberFormat nf = NumberFormat.getInstance();
		nf.setMinimumFractionDigits(1);
		nf.setMaximumFractionDigits(quantidadeCasasDecimaisPermitirAposVirgula);
		return nf.format(valor).replace(",", ".");
	}
	
	public static BigDecimal formatarDeAcordoQuantidadeCasasDecimaisAposVirgula2(Double valor, int quantidadeCasasDecimaisPermitirAposVirgula) {
		if (valor == null || valor == 0.0) {
			return new BigDecimal(0.00).setScale(quantidadeCasasDecimaisPermitirAposVirgula, RoundingMode.HALF_UP);
		}
		final NumberFormat nf = NumberFormat.getInstance();
		nf.setMinimumFractionDigits(2);
		nf.setMaximumFractionDigits(quantidadeCasasDecimaisPermitirAposVirgula);
		return new BigDecimal(valor).setScale(quantidadeCasasDecimaisPermitirAposVirgula, RoundingMode.HALF_UP);
	}
	
	public static String substituirVogaisAcentuadasPoUnicode(String texto) {
		char[] chars = texto.toCharArray();
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < chars.length; i++) {
			int unipoint = Character.codePointAt(chars, i);
			if ((unipoint < 32) || (unipoint > 127)) {
				StringBuilder hexString = new StringBuilder();
				for (int k = 0; k < 4; k++) {
					hexString.insert(0, Integer.toHexString(unipoint % 16));
					unipoint = unipoint / 16;
				}
				sb.append("\\u" + hexString);
			} else {
				sb.append(chars[i]);
			}
		}
		return sb.toString();
	}

	public static String formatarDeAcordoQuantidadeCasasDecimaisAposVirgulaStr2(Double valor, int quantidadeCasasDecimaisPermitirAposVirgula) {
		if (!isAtributoPreenchido(valor)) {
			return null;
		}
		final NumberFormat nf = NumberFormat.getInstance();
		nf.setMinimumFractionDigits(1);
		nf.setMaximumFractionDigits(quantidadeCasasDecimaisPermitirAposVirgula);
		return nf.format(valor);
	}
	
	public static String removerZeroEsquerda(String valor) {
		return valor.replaceFirst("0*", "");
	}

}
