package negocio.comuns.utilitarias;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class UteisFinanceiro {
	
	public static BigDecimal arredondar(Double value, int decimais) {
		int decimalPlace = 2;
		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);		
		return bd;
	}
	
	public static BigDecimal arredondar(BigDecimal value, int decimais) {		
		return value.setScale(decimais, BigDecimal.ROUND_HALF_UP);
	}

	public static BigDecimal truncar(BigDecimal valor, int casasDecimais) {		
		return valor.setScale(casasDecimais);
	}
	
	public static String converterBigDecimalString(BigDecimal valor, Integer nrCasasDecimais){	
		return String.format("%."+nrCasasDecimais+"f", valor.doubleValue());
	}
	
	 public static String converterValorEmStringArrendondarForcando3CasasDecimais(BigDecimal valor) {
	        boolean numeroNegativo = false;
	        if (valor.compareTo(BigDecimal.ZERO) < 0) {
	            numeroNegativo = true;
	        }
	        valor = arredondar(valor, 2);
	        String valorStr = String.valueOf(valor);

	        String inteira = valorStr.substring(0, valorStr.indexOf("."));
	        String extensao = valorStr.substring(valorStr.indexOf(".") + 1, valorStr.length());
	        if (extensao.length() == 1) {
	            extensao += "00";
	        }
	        if (extensao.length() == 2) {
	            extensao += "0";
	        }
	        valorStr = Uteis.removerMascara(inteira) + "." + extensao;
	        if (numeroNegativo) {
	            valorStr = "-" + valorStr;
	        }
	        return valorStr;
	    }
	 

	 
	 public static String converterValorEmStringArrendondarForcando2CasasDecimais(BigDecimal valor) {
		 boolean numeroNegativo = false;
		 if (valor.compareTo(BigDecimal.ZERO) < 0) {
			 numeroNegativo = true;
		 }
		 valor = arredondar(valor, 2);
		 String valorStr = String.valueOf(valor);
		 
		 String inteira = valorStr.substring(0, valorStr.indexOf("."));
		 String extensao = valorStr.substring(valorStr.indexOf(".") + 1, valorStr.length());
		 if (extensao.length() == 1) {
			 extensao += "0";
		 }
		 valorStr = Uteis.removerMascara(inteira) + "." + extensao;
		 if (numeroNegativo) {
			 valorStr = "-" + valorStr;
		 }
		 return valorStr;
	 }
	 
	 public static BigDecimal converterStringBigDecimal(String valor){
		 return BigDecimal.valueOf(Double.parseDouble(valor));
	 }
	 
	private static BigDecimal CEM = new BigDecimal(100);
	
	public static BigDecimal getPorcentagem(BigDecimal valorBase, BigDecimal valorCalcular){
		    return BigDecimal.valueOf((valorCalcular.doubleValue() * CEM.doubleValue())/valorBase.doubleValue()).setScale(2, RoundingMode.HALF_UP);
	}
	
	public static BigDecimal getAcrescentarPorcentagem(BigDecimal valorBase, BigDecimal porcentagem){
		return BigDecimal.valueOf(valorBase.doubleValue()+((valorBase.doubleValue()* porcentagem.doubleValue())/CEM.doubleValue())).setScale(2, RoundingMode.HALF_UP);
	}
	
	public static BigDecimal getReduzirPorcentagem(BigDecimal valorBase, BigDecimal porcentagem){
		if(porcentagem.doubleValue() >= 100){
			return BigDecimal.ZERO;
		}
		return BigDecimal.valueOf(valorBase.doubleValue()-((valorBase.doubleValue()* porcentagem.doubleValue())/CEM.doubleValue())).setScale(2, RoundingMode.HALF_UP);
	}

}
