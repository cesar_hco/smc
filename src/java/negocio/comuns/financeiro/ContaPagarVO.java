package negocio.comuns.financeiro;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import negocio.comuns.administrativo.EmpresaVO;
import negocio.comuns.arquitetura.SuperVO;
import negocio.comuns.basico.FornecedorVO;
import negocio.comuns.financeiro.enumeradores.SituacaoContaPagarEnum;
import negocio.comuns.utilitarias.Uteis;

public class ContaPagarVO extends SuperVO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer codigo;
	private EmpresaVO empresaVO;
	private Date data;
	private FornecedorVO fornecedorVO;
	private SituacaoContaPagarEnum situacao;
	private Date dataVencimento;
	private Date dataCompetencia;
	private BigDecimal valor;
	private BigDecimal valorTotal;
	private BigDecimal valorPago;
	private BigDecimal juro;
	private BigDecimal multa;
	private BigDecimal valorJuro;
	private BigDecimal valorMulta;
	private BigDecimal desconto;
	private String nrDocumento;
	private String codigoBarra;
	private String parcela;
	private String descricao;
	private CategoriaDespesaVO categoriaDespesaVO;
	private Date dataPagamento;

	public ContaPagarVO() {
		super();
	}
	
	public Boolean getPago() {
		return getSituacao().equals(SituacaoContaPagarEnum.PAGO);
	}

	public Integer getCodigo() {
		if (codigo == null) {
			codigo = 0;
		}
		return codigo;
	}

	public Date getData() {
		if (data == null) {
			data = new Date();
		}
		return data;
	}

	public SituacaoContaPagarEnum getSituacao() {
		if (situacao == null) {
			situacao = SituacaoContaPagarEnum.A_PAGAR;
		}
		return situacao;
	}

	public Date getDataVencimento() {
		if (dataVencimento == null) {
			dataVencimento = new Date();
		}
		return dataVencimento;
	}

	public Date getDataCompetencia() {
		if (dataCompetencia == null) {
			dataCompetencia = new Date();
		}
		return dataCompetencia;
	}

	public BigDecimal getValor() {
		if (valor == null) {
			valor = BigDecimal.ZERO;
		}
		return valor;
	}

	public BigDecimal getValorPago() {
		return valorPago;
	}

	public BigDecimal getJuro() {
		if (juro == null) {
			juro = BigDecimal.ZERO;
		}
		return juro;
	}

	public BigDecimal getMulta() {
		if (multa == null) {
			multa = BigDecimal.ZERO;
		}
		return multa;
	}

	public BigDecimal getValorJuro() {
		if (valorJuro == null) {
			valorJuro = BigDecimal.ZERO;
		}
		return valorJuro;
	}

	public BigDecimal getValorMulta() {
		if (valorMulta == null) {
			valorMulta = BigDecimal.ZERO;
		}
		return valorMulta;
	}

	public BigDecimal getDesconto() {
		if (desconto == null) {
			desconto = BigDecimal.ZERO;
		}
		return desconto;
	}

	public String getNrDocumento() {
		if (nrDocumento == null) {
			nrDocumento = "";
		}
		return nrDocumento;
	}

	public String getCodigoBarra() {
		if (codigoBarra == null) {
			codigoBarra = "";
		}
		return codigoBarra;
	}

	public String getParcela() {
		if (parcela == null) {
			parcela = "";
		}
		return parcela;
	}

	public String getDescricao() {
		if (descricao == null) {
			descricao = "";
		}
		return descricao;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public void setSituacao(SituacaoContaPagarEnum situacao) {
		this.situacao = situacao;
	}

	public void setDataVencimento(Date dataVencimento) {
		this.dataVencimento = dataVencimento;
	}

	public void setDataCompetencia(Date dataCompetencia) {
		this.dataCompetencia = dataCompetencia;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public void setValorPago(BigDecimal valorPago) {
		this.valorPago = valorPago;
	}

	public void setJuro(BigDecimal juro) {
		this.juro = juro;
	}

	public void setMulta(BigDecimal multa) {
		this.multa = multa;
	}

	public void setValorJuro(BigDecimal valorJuro) {
		this.valorJuro = valorJuro;
	}

	public void setValorMulta(BigDecimal valorMulta) {
		this.valorMulta = valorMulta;
	}

	public void setDesconto(BigDecimal desconto) {
		this.desconto = desconto;
	}

	public void setNrDocumento(String nrDocumento) {
		this.nrDocumento = nrDocumento;
	}

	public void setCodigoBarra(String codigoBarra) {
		this.codigoBarra = codigoBarra;
	}

	public void setParcela(String parcela) {
		this.parcela = parcela;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public FornecedorVO getFornecedorVO() {
		if (fornecedorVO == null) {
			fornecedorVO = new FornecedorVO();
		}
		return fornecedorVO;
	}

	public void setFornecedorVO(FornecedorVO fornecedorVO) {
		this.fornecedorVO = fornecedorVO;
	}

	public String getDataVencimento_Apresentar() {
		return (Uteis.getData(dataVencimento));
	}

	public String getDataCompetencia_Apresentar() {
		return (Uteis.getData(dataCompetencia));
	}

	public String getData_Apresentar() {
		return (Uteis.getData(data));
	}

	public BigDecimal getValorTotalCalculado() {
		BigDecimal valorDesconto = BigDecimal.ZERO;
		valorDesconto = getDesconto();
		realizarCalculoValorJuro();
		realizarCalculoValorMulta();
		setValorTotal(getValor().add(getValorMulta()).add(getValorJuro()).subtract(valorDesconto));
		return getValorTotal();
	}

	public void realizarCalculoValorMulta() {
		if (this.getValor().compareTo(BigDecimal.ZERO) != 0) {
			if (this.getMulta().compareTo(BigDecimal.ZERO) != 0) {
				this.setValorMulta(this.getValor().multiply(this.getMulta()).divide(BigDecimal.valueOf(100)));
				return;
			}
		}
		this.setValorMulta(BigDecimal.ZERO);
	}

	public void realizarCalculoValorJuro() {
		if (this.getValor().compareTo(BigDecimal.ZERO) != 0) {
			if (this.getJuro().compareTo(BigDecimal.ZERO) != 0) {
				this.setValorJuro(this.getValor().multiply(this.getJuro()).divide(BigDecimal.valueOf(100)));
				return;
			}
		}
		this.setValorJuro(BigDecimal.ZERO);
	}

	public BigDecimal getValorTotal() {
		if (valorTotal == null) {
			valorTotal = BigDecimal.ZERO;
		}
		return valorTotal;
	}

	public void setValorTotal(BigDecimal valorTotal) {
		this.valorTotal = valorTotal;
	}

	public CategoriaDespesaVO getCategoriaDespesaVO() {
		if (categoriaDespesaVO == null) {
			categoriaDespesaVO = new CategoriaDespesaVO();
		}
		return categoriaDespesaVO;
	}

	public void setCategoriaDespesaVO(CategoriaDespesaVO categoriaDespesaVO) {
		this.categoriaDespesaVO = categoriaDespesaVO;
	}

	public Date getDataPagamento() {
		return dataPagamento;
	}

	public void setDataPagamento(Date dataPagamento) {
		this.dataPagamento = dataPagamento;
	}

	public EmpresaVO getEmpresaVO() {
		if (empresaVO == null) {
			empresaVO = new EmpresaVO();
		}
		return empresaVO;
	}

	public void setEmpresaVO(EmpresaVO empresaVO) {
		this.empresaVO = empresaVO;
	}
}
