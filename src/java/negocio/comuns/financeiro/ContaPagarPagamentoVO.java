/**
 * 
 */
package negocio.comuns.financeiro;

import java.io.Serializable;

import negocio.comuns.arquitetura.SuperVO;

/**
 * @author Carlos Eug�nio
 *
 */
public class ContaPagarPagamentoVO extends SuperVO implements Serializable{

	private static final long serialVersionUID = 1L;
	private Integer codigo;
	private ContaPagarVO contaPagarVO;
	private PagamentoVO pagamentoVO;
	

	public ContaPagarPagamentoVO() {
		super();
	}


	public Integer getCodigo() {
		if (codigo == null) {
			codigo = 0;
		}
		return codigo;
	}


	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}


	public ContaPagarVO getContaPagarVO() {
		if (contaPagarVO == null) {
			contaPagarVO = new ContaPagarVO();
		}
		return contaPagarVO;
	}


	public void setContaPagarVO(ContaPagarVO contaPagarVO) {
		this.contaPagarVO = contaPagarVO;
	}


	public PagamentoVO getPagamentoVO() {
		if (pagamentoVO == null) {
			pagamentoVO = new PagamentoVO();
		}
		return pagamentoVO;
	}


	public void setPagamentoVO(PagamentoVO pagamentoVO) {
		this.pagamentoVO = pagamentoVO;
	}
	
}
