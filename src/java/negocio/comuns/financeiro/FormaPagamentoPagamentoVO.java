/**
 * 
 */
package negocio.comuns.financeiro;

import java.io.Serializable;
import java.math.BigDecimal;

import negocio.comuns.arquitetura.SuperVO;

/**
 * @author Carlos Eug�nio
 *
 */
public class FormaPagamentoPagamentoVO extends SuperVO implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer codigo;
	private FormaPagamentoVO formaPagamentoVO;
	private PagamentoVO pagamentoVO;
	private ContaCorrenteVO contaCorrenteVO;
	private BigDecimal valor;
	private ChequeVO chequeVO;

	public FormaPagamentoPagamentoVO() {
		super();
	}

	public Integer getCodigo() {
		if (codigo == null) {
			codigo = 0;
		}
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public FormaPagamentoVO getFormaPagamentoVO() {
		if (formaPagamentoVO == null) {
			formaPagamentoVO = new FormaPagamentoVO();
		}
		return formaPagamentoVO;
	}

	public void setFormaPagamentoVO(FormaPagamentoVO formaPagamentoVO) {
		this.formaPagamentoVO = formaPagamentoVO;
	}

	public PagamentoVO getPagamentoVO() {
		if (pagamentoVO == null) {
			pagamentoVO = new PagamentoVO();
		}
		return pagamentoVO;
	}

	public void setPagamentoVO(PagamentoVO pagamentoVO) {
		this.pagamentoVO = pagamentoVO;
	}

	public ContaCorrenteVO getContaCorrenteVO() {
		if (contaCorrenteVO == null) {
			contaCorrenteVO = new ContaCorrenteVO();
		}
		return contaCorrenteVO;
	}

	public void setContaCorrenteVO(ContaCorrenteVO contaCorrenteVO) {
		this.contaCorrenteVO = contaCorrenteVO;
	}

	public BigDecimal getValor() {
		if (valor == null) {
			valor = BigDecimal.ZERO;
		}
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public ChequeVO getChequeVO() {
		if (chequeVO == null) {
			chequeVO = new ChequeVO();
		}
		return chequeVO;
	}

	public void setChequeVO(ChequeVO chequeVO) {
		this.chequeVO = chequeVO;
	}
}
