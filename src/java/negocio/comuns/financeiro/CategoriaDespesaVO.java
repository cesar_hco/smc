package negocio.comuns.financeiro;

import java.io.Serializable;

import negocio.comuns.arquitetura.SuperVO;
import negocio.comuns.utilitarias.Uteis;

public class CategoriaDespesaVO extends SuperVO implements Serializable {

	private Integer codigo;
	private String descricao;
	private CategoriaDespesaVO categoriaDespesaPrincipalVO;
	private Boolean isSubCategoria;
	public static final long serialVersionUID = 1L;

	public CategoriaDespesaVO() {
		super();
	}


	public String getDescricao() {
		return (descricao);
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Integer getCodigo() {
		if (this.codigo == null) {
			return 0;
		}
		return (codigo);
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public Boolean getIsSubCategoria() {
		if (isSubCategoria == null) {
			isSubCategoria = false;
		}
		return isSubCategoria;
	}

	public void setIsSubCategoria(Boolean isSubCategoria) {
		this.isSubCategoria = isSubCategoria;
	}


	public CategoriaDespesaVO getCategoriaDespesaPrincipalVO() {
		if (categoriaDespesaPrincipalVO == null) {
			categoriaDespesaPrincipalVO = new CategoriaDespesaVO();
		}
		return categoriaDespesaPrincipalVO;
	}


	public void setCategoriaDespesaPrincipalVO(CategoriaDespesaVO categoriaDespesaPrincipalVO) {
		this.categoriaDespesaPrincipalVO = categoriaDespesaPrincipalVO;
	}

}
