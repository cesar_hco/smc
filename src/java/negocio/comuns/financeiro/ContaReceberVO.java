package negocio.comuns.financeiro;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.arquitetura.SuperVO;
import negocio.comuns.basico.ClienteVO;
import negocio.comuns.financeiro.enumeradores.SituacaoContaReceberEnum;
import negocio.comuns.financeiro.enumeradores.TipoDescontoEnum;
import negocio.comuns.utilitarias.Uteis;

public class ContaReceberVO extends SuperVO implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer codigo;
	private Date dataCadastro;
	private ClienteVO clienteVO;
	private SituacaoContaReceberEnum situacaoContaReceber;
	private String descricaoPagamento;
	private Date dataVencimento;
	private Date dataCompetencia;
	private BigDecimal valor;
	private BigDecimal desconto;
	private BigDecimal acrescimo;
	private BigDecimal multa;
	private BigDecimal juro;
	private BigDecimal valorMulta;
	private BigDecimal valorJuro;
	private BigDecimal valorRecebido;
	private Date dataRecebimento;
	private String parcela;
	private String observacao;
	private UsuarioVO usuarioVO;
	private ContaCorrenteVO contaCorrenteVO;
	private BigDecimal valorTotal;
	private TipoDescontoEnum TipoDesconto;
	private String servico;
	private String nossoNumero;
	private String nrDocumento;
	private String codigoBarra;
	private String linhaDigitavel;
	private Integer nrNotaFiscal;

	public ContaReceberVO() {
		super();
	}

	public Integer getCodigo() {
		if (codigo == null) {
			codigo = 0;
		}
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public Date getDataCadastro() {
		if (dataCadastro == null) {
			dataCadastro = new Date();
		}
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public ClienteVO getClienteVO() {
		if (clienteVO == null) {
			clienteVO = new ClienteVO();
		}
		return clienteVO;
	}

	public void setClienteVO(ClienteVO clienteVO) {
		this.clienteVO = clienteVO;
	}

	public SituacaoContaReceberEnum getSituacaoContaReceber() {
		if (situacaoContaReceber == null) {
			situacaoContaReceber = SituacaoContaReceberEnum.A_RECEBER;
		}
		return situacaoContaReceber;
	}

	public void setSituacaoContaReceber(SituacaoContaReceberEnum situacaoContaReceber) {
		this.situacaoContaReceber = situacaoContaReceber;
	}

	public String getDescricaoPagamento() {
		if (descricaoPagamento == null) {
			descricaoPagamento = "";
		}
		return descricaoPagamento;
	}

	public void setDescricaoPagamento(String descricaoPagamento) {
		this.descricaoPagamento = descricaoPagamento;
	}

	public Date getDataVencimento() {
		if (dataVencimento == null) {
			dataVencimento = new Date();
		}
		return dataVencimento;
	}
	
	public String getDataVencimento_Apresentar() {
		return Uteis.getDataAno4Digitos(getDataVencimento());
	}

	public void setDataVencimento(Date dataVencimento) {
		this.dataVencimento = dataVencimento;
	}

	public Date getDataCompetencia() {
		if (dataCompetencia == null) {
			dataCompetencia = new Date();
		}
		return dataCompetencia;
	}

	public void setDataCompetencia(Date dataCompetencia) {
		this.dataCompetencia = dataCompetencia;
	}

	public BigDecimal getValor() {
		if (valor == null) {
			valor = BigDecimal.ZERO;
		}
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public BigDecimal getDesconto() {
		if (desconto == null) {
			desconto = BigDecimal.ZERO;
		}
		return desconto;
	}

	public void setDesconto(BigDecimal desconto) {
		this.desconto = desconto;
	}

	public BigDecimal getAcrescimo() {
		if (acrescimo == null) {
			acrescimo = BigDecimal.ZERO;
		}
		return acrescimo;
	}

	public void setAcrescimo(BigDecimal acrescimo) {
		this.acrescimo = acrescimo;
	}

	public BigDecimal getMulta() {
		if (multa == null) {
			multa = BigDecimal.ZERO;
		}
		return multa;
	}

	public void setMulta(BigDecimal multa) {
		this.multa = multa;
	}

	public BigDecimal getJuro() {
		if (juro == null) {
			juro = BigDecimal.ZERO;
		}
		return juro;
	}

	public void setJuro(BigDecimal juro) {
		this.juro = juro;
	}

	public BigDecimal getValorMulta() {
		if (valorMulta == null) {
			valorMulta = BigDecimal.ZERO;
		}
		return valorMulta;
	}

	public void setValorMulta(BigDecimal valorMulta) {
		this.valorMulta = valorMulta;
	}

	public BigDecimal getValorJuro() {
		if (valorJuro == null) {
			valorJuro = BigDecimal.ZERO;
		}
		return valorJuro;
	}

	public void setValorJuro(BigDecimal valorJuro) {
		this.valorJuro = valorJuro;
	}

	public BigDecimal getValorRecebido() {
		if (valorRecebido == null) {
			valorRecebido = BigDecimal.ZERO;
		}
		return valorRecebido;
	}

	public void setValorRecebido(BigDecimal valorRecebido) {
		this.valorRecebido = valorRecebido;
	}

	public Date getDataRecebimento() {
		return dataRecebimento;
	}

	public void setDataRecebimento(Date dataRecebimento) {
		this.dataRecebimento = dataRecebimento;
	}

	public String getParcela() {
		if (parcela == null) {
			parcela = "";
		}
		return parcela;
	}

	public void setParcela(String parcela) {
		this.parcela = parcela;
	}

	public String getObservacao() {
		if (observacao == null) {
			observacao = "";
		}
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public UsuarioVO getUsuarioVO() {
		if (usuarioVO == null) {
			usuarioVO = new UsuarioVO();
		}
		return usuarioVO;
	}

	public void setUsuarioVO(UsuarioVO usuarioVO) {
		this.usuarioVO = usuarioVO;
	}

	public ContaCorrenteVO getContaCorrenteVO() {
		if (contaCorrenteVO == null) {
			contaCorrenteVO = new ContaCorrenteVO();
		}
		return contaCorrenteVO;
	}

	public void setContaCorrenteVO(ContaCorrenteVO contaCorrenteVO) {
		this.contaCorrenteVO = contaCorrenteVO;
	}

	public BigDecimal getValorTotal() {
		if (valorTotal == null) {
			valorTotal = BigDecimal.ZERO;
		}
		return valorTotal;
	}

	public void setValorTotal(BigDecimal valorTotal) {
		this.valorTotal = valorTotal;
	}

	public void calcularValorTotal() {
		BigDecimal valorDesconto = BigDecimal.ZERO;
		if (getTipoDesconto().equals(TipoDesconto.PERCENTUAL)) {
			valorDesconto = getValor().multiply(getDesconto()).divide(BigDecimal.valueOf(100));
		} else {
			valorDesconto = getDesconto();
		}
		setValorTotal(getValor().add(getValorMulta()).add(getValorJuro()).add(getAcrescimo()).subtract(valorDesconto));
	}

	public TipoDescontoEnum getTipoDesconto() {
		if (TipoDesconto == null) {
			TipoDesconto = TipoDesconto.PERCENTUAL;
		}
		return TipoDesconto;
	}

	public void setTipoDesconto(TipoDescontoEnum tipoDesconto) {
		TipoDesconto = tipoDesconto;
	}

	public String getServico() {
		if (servico == null) {
			servico = "";
		}
		return servico;
	}

	public void setServico(String servico) {
		this.servico = servico;
	}

	public String getNossoNumero() {
		if (nossoNumero == null) {
			nossoNumero = "";
		}
		return nossoNumero;
	}

	public void setNossoNumero(String nossoNumero) {
		this.nossoNumero = nossoNumero;
	}

	public String getNrDocumento() {
		if (nrDocumento == null) {
			nrDocumento = "";
		}
		return nrDocumento;
	}

	public void setNrDocumento(String nrDocumento) {
		this.nrDocumento = nrDocumento;
	}

	public String getCodigoBarra() {
		if (codigoBarra == null) {
			codigoBarra = "";
		}
		return codigoBarra;
	}

	public void setCodigoBarra(String codigoBarra) {
		this.codigoBarra = codigoBarra;
	}

	public String getLinhaDigitavel() {
		if (linhaDigitavel == null) {
			linhaDigitavel = "";
		}
		return linhaDigitavel;
	}

	public void setLinhaDigitavel(String linhaDigitavel) {
		this.linhaDigitavel = linhaDigitavel;
	}

	public Integer getNrNotaFiscal() {
		if (nrNotaFiscal == null) {
			nrNotaFiscal = 0;
		}
		return nrNotaFiscal;
	}

	public void setNrNotaFiscal(Integer nrNotaFiscal) {
		this.nrNotaFiscal = nrNotaFiscal;
	}

}
