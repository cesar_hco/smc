/**
 * 
 */
package negocio.comuns.financeiro;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import negocio.comuns.administrativo.EmpresaVO;
import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.arquitetura.SuperVO;
import negocio.comuns.basico.ClienteVO;
import negocio.comuns.basico.FornecedorVO;
import negocio.comuns.financeiro.enumeradores.LocalizacaoChequeEnum;

/**
 * @author Carlos Eug�nio
 *
 */
public class ChequeVO extends SuperVO implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer codigo;
	private ClienteVO clienteVO;
	private FornecedorVO fornecedorVO;
	private EmpresaVO empresaVO;
	private ContaCorrenteVO contaCorrentelocalizacaoChequeVO;
	private Date dataCadastro;
	private Date dataVencimento;
	private String banco;
	private String agencia;
	private String contaCorrente;
	private String numero;
	private BigDecimal valor;
	private LocalizacaoChequeEnum localizacao;
	private Integer codigoOrigem;
	private Date dataCompensacao;
	private UsuarioVO responsavelRegistroCompensacaoVO;
	private String titularConta;
	private Boolean chequeProprio;
	private ContaCorrenteVO contaCorrenteChequeProprioVO;
	private ContaCorrenteVO localizacaoChequeVO;

	public ChequeVO() {
		super();
	}

	public Integer getCodigo() {
		if (codigo == null) {
			codigo = 0;
		}
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public ClienteVO getClienteVO() {
		if (clienteVO == null) {
			clienteVO = new ClienteVO();
		}
		return clienteVO;
	}

	public void setClienteVO(ClienteVO clienteVO) {
		this.clienteVO = clienteVO;
	}

	public FornecedorVO getFornecedorVO() {
		if (fornecedorVO == null) {
			fornecedorVO = new FornecedorVO();
		}
		return fornecedorVO;
	}

	public void setFornecedorVO(FornecedorVO fornecedorVO) {
		this.fornecedorVO = fornecedorVO;
	}


	public Date getDataCadastro() {
		if (dataCadastro == null) {
			dataCadastro = new Date();
		}
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Date getDataVencimento() {
		if (dataVencimento == null) {
			dataVencimento = new Date();
		}
		return dataVencimento;
	}

	public void setDataVencimento(Date dataVencimento) {
		this.dataVencimento = dataVencimento;
	}

	public String getBanco() {
		if (banco == null) {
			banco = "";
		}
		return banco;
	}

	public void setBanco(String banco) {
		this.banco = banco;
	}

	public String getAgencia() {
		if (agencia == null) {
			agencia = "";
		}
		return agencia;
	}

	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	public String getContaCorrente() {
		if (contaCorrente == null) {
			contaCorrente = "";
		}
		return contaCorrente;
	}

	public void setContaCorrente(String contaCorrente) {
		this.contaCorrente = contaCorrente;
	}

	public String getNumero() {
		if (numero == null) {
			numero = "";
		}
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public BigDecimal getValor() {
		if (valor == null) {
			valor = BigDecimal.ZERO;
		}
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public LocalizacaoChequeEnum getLocalizacao() {
		return localizacao;
	}

	public void setLocalizacao(LocalizacaoChequeEnum localizacao) {
		this.localizacao = localizacao;
	}

	public Integer getCodigoOrigem() {
		if (codigoOrigem == null) {
			codigoOrigem = 0;
		}
		return codigoOrigem;
	}

	public void setCodigoOrigem(Integer codigoOrigem) {
		this.codigoOrigem = codigoOrigem;
	}

	public Date getDataCompensacao() {
		return dataCompensacao;
	}

	public void setDataCompensacao(Date dataCompensacao) {
		this.dataCompensacao = dataCompensacao;
	}

	public String getTitularConta() {
		if (titularConta == null) {
			titularConta = "";
		}
		return titularConta;
	}

	public void setTitularConta(String titularConta) {
		this.titularConta = titularConta;
	}

	public Boolean getChequeProprio() {
		if (chequeProprio == null) {
			chequeProprio = false;
		}
		return chequeProprio;
	}

	public void setChequeProprio(Boolean chequeProprio) {
		this.chequeProprio = chequeProprio;
	}

	public EmpresaVO getEmpresaVO() {
		if (empresaVO == null) {
			empresaVO = new EmpresaVO();
		}
		return empresaVO;
	}

	public void setEmpresaVO(EmpresaVO empresaVO) {
		this.empresaVO = empresaVO;
	}

	public ContaCorrenteVO getContaCorrentelocalizacaoChequeVO() {
		if (contaCorrentelocalizacaoChequeVO == null) {
			contaCorrentelocalizacaoChequeVO = new ContaCorrenteVO();
		}
		return contaCorrentelocalizacaoChequeVO;
	}

	public void setContaCorrentelocalizacaoChequeVO(ContaCorrenteVO contaCorrentelocalizacaoChequeVO) {
		this.contaCorrentelocalizacaoChequeVO = contaCorrentelocalizacaoChequeVO;
	}

	public UsuarioVO getResponsavelRegistroCompensacaoVO() {
		if (responsavelRegistroCompensacaoVO == null) {
			responsavelRegistroCompensacaoVO = new UsuarioVO();
		}
		return responsavelRegistroCompensacaoVO;
	}

	public void setResponsavelRegistroCompensacaoVO(UsuarioVO responsavelRegistroCompensacaoVO) {
		this.responsavelRegistroCompensacaoVO = responsavelRegistroCompensacaoVO;
	}


	public ContaCorrenteVO getContaCorrenteChequeProprioVO() {
		if (contaCorrenteChequeProprioVO == null) {
			contaCorrenteChequeProprioVO = new ContaCorrenteVO();
		}
		return contaCorrenteChequeProprioVO;
	}

	public void setContaCorrenteChequeProprioVO(ContaCorrenteVO contaCorrenteChequeProprioVO) {
		this.contaCorrenteChequeProprioVO = contaCorrenteChequeProprioVO;
	}

	public ContaCorrenteVO getLocalizacaoChequeVO() {
		if (localizacaoChequeVO == null) {
			localizacaoChequeVO = new ContaCorrenteVO();
		}
		return localizacaoChequeVO;
	}

	public void setLocalizacaoChequeVO(ContaCorrenteVO localizacaoChequeVO) {
		this.localizacaoChequeVO = localizacaoChequeVO;
	}

	public String getLocalizacaoChequeApresentar() {
		if (getChequeProprio()) {
			return getContaCorrenteChequeProprioVO().getDescricaoApresentar();
		} else if (getLocalizacaoChequeVO() != null
				&& !getLocalizacaoChequeVO().getCodigo().equals(0)) {
			return getLocalizacaoChequeVO().getDescricaoApresentar();
		}
		return "";
	}
	
}
