package negocio.comuns.financeiro.enumeradores;

import negocio.comuns.utilitarias.UteisJSF;

public enum SituacaoContaPagarEnum {
	A_PAGAR,
	PAGO;
	
	public String getValorApresentar(){
		return UteisJSF.internacionalizar("enum_SituacaoContaPagarEnum_"+this.name());
	}
}
