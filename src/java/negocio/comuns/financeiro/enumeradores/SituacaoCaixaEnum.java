package negocio.comuns.financeiro.enumeradores;

import negocio.comuns.utilitarias.UteisJSF;

/**
*
* @author Carlos
*/

public enum SituacaoCaixaEnum {

 ABERTO, FECHADO, TODAS;

  public String getValorApresentar(){        
         return UteisJSF.internacionalizar("enum_SituacaoCaixaEnum_"+this.name());
     }
}
