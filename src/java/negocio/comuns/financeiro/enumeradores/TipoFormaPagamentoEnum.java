/**
 * 
 */
package negocio.comuns.financeiro.enumeradores;

import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;


/**
 * @author Carlos Eug�nio
 *
 */
public enum TipoFormaPagamentoEnum {
	DINHEIRO("Dinheiro"),
	CHEQUE("Cheque"),
	BOLETO("Boleto"),
	DEPOSITO("Dep�sito"),
	CARTAO_DEBITO("Cart�o de D�bito"),
	CARTAO_CREDITO("Cart�o de Cr�dito");
	
    String descricao;

    TipoFormaPagamentoEnum(String descricao) {
        this.descricao = descricao;
    }


    public static String getDescricao(TipoFormaPagamentoEnum tipoFormaPagamento) {
    	TipoFormaPagamentoEnum obj = tipoFormaPagamento;
        if (obj != null) {
            return obj.getDescricao();
        }
        return null;
    }
    
    public static List<SelectItem> getListaSelectItemFormPagamentoVOs(Boolean obrigatorio) {
    	List<SelectItem> itens = new ArrayList<SelectItem>(0);
    	if (!obrigatorio) {
    		itens.add(new SelectItem("", ""));
    	}
    	TipoFormaPagamentoEnum[] valores = values();
        for (TipoFormaPagamentoEnum obj : valores) {
        	itens.add(new SelectItem(obj, obj.getDescricao()));
        }
        return itens;
    }


    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
