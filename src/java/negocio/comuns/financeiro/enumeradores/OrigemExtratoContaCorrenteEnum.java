package negocio.comuns.financeiro.enumeradores;

public enum OrigemExtratoContaCorrenteEnum {

	RECEBIMENTO, PAGAMENTO, MOVIMENTACAO_FINANCEIRA, COMPENSACAO
}
