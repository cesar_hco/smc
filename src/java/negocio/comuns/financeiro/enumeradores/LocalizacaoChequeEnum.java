package negocio.comuns.financeiro.enumeradores;

import negocio.comuns.utilitarias.UteisJSF;


/**
*
* @author SCW-GenerateCode
*/

public enum LocalizacaoChequeEnum {

 EM_CAIXA, BANCO, PAGAMENTO, ESTORNADO, DEVOLVIDO, RESGATADO, COMPENSADO;
 
 public String getValorApresentar(){
	 return UteisJSF.internacionalizar("enum_LocalizacaoChequeEnum_"+this.name());
 }

}
