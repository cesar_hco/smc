package negocio.comuns.financeiro.enumeradores;


/**
 *
 * @author Carlos
 */

public enum CedenteEnum {

	FORNECEDOR, CLIENTE;

//	public String getValorApresentar() {
		// return Uteis.internacionalizar("enum_CedenteEnum_"+name());
//	}

	public Boolean getIsCliente() {
		return name().equals(CedenteEnum.CLIENTE.name());
	}

	public Boolean getIsFornecedor() {
		return name().equals(CedenteEnum.FORNECEDOR.name());
	}

}
