package negocio.comuns.financeiro.enumeradores;

public enum TipoMovimentacaoExtratoContaCorrenteEnum {
	ENTRADA, SAIDA;
	
//	public String getValorApresentar(){
//		return Uteis.internacionalizar("enum_TipoMovimentacaoExtratoContaCorrenteEnum_"+this.name());
//	}
	
	public Boolean getEntrada(){
		return this.equals(TipoMovimentacaoExtratoContaCorrenteEnum.ENTRADA);
	}
}
