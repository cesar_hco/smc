package negocio.comuns.financeiro.enumeradores;

import negocio.comuns.utilitarias.UteisJSF;

public enum TipoDescontoEnum {
	PERCENTUAL, VALOR;
	
	public String getValorApresentar(){
		return UteisJSF.internacionalizar("enum_TipoDescontoEnum_"+this.name());
	}
}
