/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio.comuns.financeiro.enumeradores;

import negocio.comuns.utilitarias.UteisJSF;


/**
 *
 * @author Carlos
 */
public enum TipoMovimentacaoCaixaEnum {
    
    ENTRADA, SAIDA;
    
	public String getValorApresentar() {
		return UteisJSF.internacionalizar("enum_TipoMovimentacaoCaixaEnum_" + this.name());
	}
    
}
