package negocio.comuns.financeiro.enumeradores;

import negocio.comuns.utilitarias.UteisJSF;

public enum SituacaoContaReceberEnum {
	A_RECEBER, RECEBIDO;
	
	public String getValorApresentar(){
		return UteisJSF.internacionalizar("enum_SituacaoContaReceberEnum_"+this.name());
	}
}
