package negocio.comuns.financeiro;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import negocio.comuns.administrativo.EmpresaVO;
import negocio.comuns.arquitetura.SuperVO;
import negocio.comuns.financeiro.enumeradores.CedenteEnum;
import negocio.comuns.financeiro.enumeradores.OrigemExtratoContaCorrenteEnum;
import negocio.comuns.financeiro.enumeradores.TipoMovimentacaoExtratoContaCorrenteEnum;

public class ExtratoContaCorrenteVO extends SuperVO implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer codigo;
	private Date data;
	private BigDecimal valor;
	private EmpresaVO empresaVO;
	private ChequeVO chequeVO;
	private ContaCorrenteVO contaCorrenteVO;
	private FormaPagamentoVO formaPagamentoVO;
	private Integer codigoOrigem;
	private OrigemExtratoContaCorrenteEnum origemExtratoContaCorrente;
	private TipoMovimentacaoExtratoContaCorrenteEnum tipoMovimentacaoExtratoContaCorrente;
	private String nomeSacado;
	private Integer codigoSacado;
	private CedenteEnum cedenteEnum;

	public ExtratoContaCorrenteVO() {
		super();
	}

	public ExtratoContaCorrenteVO(Date data, BigDecimal valor, EmpresaVO empresaVO, ContaCorrenteVO contaCorrenteVO, FormaPagamentoVO formaPagamentoVO, Integer codigoOrigem, OrigemExtratoContaCorrenteEnum origemExtratoContaCorrente, TipoMovimentacaoExtratoContaCorrenteEnum tipoMovimentacaoExtratoContaCorrente, ChequeVO chequeVO, String nomeSacado, Integer codigoSacado, CedenteEnum cedenteEnum) {
		super();
		this.data = data;
		this.valor = valor;
		this.empresaVO = empresaVO;
		this.contaCorrenteVO = contaCorrenteVO;
		this.formaPagamentoVO = formaPagamentoVO;
		this.codigoOrigem = codigoOrigem;
		this.origemExtratoContaCorrente = origemExtratoContaCorrente;
		this.tipoMovimentacaoExtratoContaCorrente = tipoMovimentacaoExtratoContaCorrente;
		this.chequeVO = chequeVO;
		this.cedenteEnum = cedenteEnum;
		this.nomeSacado = nomeSacado;
		this.codigoSacado = codigoSacado;
	}

	public Integer getCodigo() {
		if (codigo == null) {
			codigo = 0;
		}
		return codigo;
	}

	public Date getData() {
		if (data == null) {
			data = new Date();
		}
		return data;
	}

	public BigDecimal getValor() {
		if (valor == null) {
			valor = BigDecimal.ZERO;
		}
		return valor;
	}

	public EmpresaVO getEmpresaVO() {
		if (empresaVO == null) {
			empresaVO = new EmpresaVO();
		}
		return empresaVO;
	}

	public ChequeVO getChequeVO() {
		if (chequeVO == null) {
			chequeVO = new ChequeVO();
		}
		return chequeVO;
	}

	public ContaCorrenteVO getContaCorrenteVO() {
		if (contaCorrenteVO == null) {
			contaCorrenteVO = new ContaCorrenteVO();
		}
		return contaCorrenteVO;
	}

	public FormaPagamentoVO getFormaPagamentoVO() {
		if (formaPagamentoVO == null) {
			formaPagamentoVO = new FormaPagamentoVO();
		}
		return formaPagamentoVO;
	}

	public Integer getCodigoOrigem() {
		if (codigoOrigem == null) {
			codigoOrigem = 0;
		}
		return codigoOrigem;
	}

	public OrigemExtratoContaCorrenteEnum getOrigemExtratoContaCorrente() {
		return origemExtratoContaCorrente;
	}

	public TipoMovimentacaoExtratoContaCorrenteEnum getTipoMovimentacaoExtratoContaCorrente() {
		return tipoMovimentacaoExtratoContaCorrente;
	}

	public String getNomeSacado() {
		if (nomeSacado == null) {
			nomeSacado = "";
		}
		return nomeSacado;
	}

	public Integer getCodigoSacado() {
		if (codigoSacado == null) {
			codigoSacado = 0;
		}
		return codigoSacado;
	}

	public CedenteEnum getCedenteEnum() {
		if (cedenteEnum == null) {
			cedenteEnum = CedenteEnum.FORNECEDOR;
		}
		return cedenteEnum;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public void setEmpresaVO(EmpresaVO empresaVO) {
		this.empresaVO = empresaVO;
	}

	public void setChequeVO(ChequeVO chequeVO) {
		this.chequeVO = chequeVO;
	}

	public void setContaCorrenteVO(ContaCorrenteVO contaCorrenteVO) {
		this.contaCorrenteVO = contaCorrenteVO;
	}

	public void setFormaPagamentoVO(FormaPagamentoVO formaPagamentoVO) {
		this.formaPagamentoVO = formaPagamentoVO;
	}

	public void setCodigoOrigem(Integer codigoOrigem) {
		this.codigoOrigem = codigoOrigem;
	}

	public void setOrigemExtratoContaCorrente(OrigemExtratoContaCorrenteEnum origemExtratoContaCorrente) {
		this.origemExtratoContaCorrente = origemExtratoContaCorrente;
	}

	public void setTipoMovimentacaoExtratoContaCorrente(TipoMovimentacaoExtratoContaCorrenteEnum tipoMovimentacaoExtratoContaCorrente) {
		this.tipoMovimentacaoExtratoContaCorrente = tipoMovimentacaoExtratoContaCorrente;
	}

	public void setNomeSacado(String nomeSacado) {
		this.nomeSacado = nomeSacado;
	}

	public void setCodigoSacado(Integer codigoSacado) {
		this.codigoSacado = codigoSacado;
	}

	public void setCedenteEnum(CedenteEnum cedenteEnum) {
		this.cedenteEnum = cedenteEnum;
	}

}
