/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio.comuns.financeiro;

import java.io.Serializable;
import java.math.BigDecimal;

import negocio.comuns.arquitetura.SuperVO;
import negocio.comuns.financeiro.enumeradores.TipoFormaPagamentoEnum;

/**
 *
 * @author Carlos
 */
public class MovimentacaoCaixaFormaPagamentoVO extends SuperVO implements Serializable {
    
	private static final long serialVersionUID = -1684690133170562391L;
    private TipoFormaPagamentoEnum tipoFormaPagamento;
    private BigDecimal entrada;
    private BigDecimal saida;
    private BigDecimal saldoInicialDinheiro;
    private BigDecimal saldoInicialCheque;
    
//    public String getTipoFormaPagamentoApresentar(){
//    	return getTipoFormaPagamento().getValorApresentar();
//    }

    public TipoFormaPagamentoEnum getTipoFormaPagamento() {
        if(tipoFormaPagamento == null){
            tipoFormaPagamento = TipoFormaPagamentoEnum.DINHEIRO;
        }
        return tipoFormaPagamento;
    }

    public void setTipoFormaPagamento(TipoFormaPagamentoEnum tipoFormaPagamento) {
        this.tipoFormaPagamento = tipoFormaPagamento;
    }
    
    public BigDecimal getSaldoInicial() {
        if(getTipoFormaPagamento().equals(TipoFormaPagamentoEnum.CHEQUE)){
        return getSaldoInicialCheque();
        }
        if(getTipoFormaPagamento().equals(TipoFormaPagamentoEnum.DINHEIRO)){
        return getSaldoInicialDinheiro();
        }
        return BigDecimal.ZERO;
    }
    
    public BigDecimal getSaldo() {
        if(getTipoFormaPagamento().equals(TipoFormaPagamentoEnum.CHEQUE)){
        return getEntrada().add(getSaldoInicialCheque()).subtract(getSaida());
        }
        if(getTipoFormaPagamento().equals(TipoFormaPagamentoEnum.DINHEIRO)){
        return getEntrada().add(getSaldoInicialDinheiro()).subtract(getSaida());
        }
        return getEntrada().subtract(getSaida());
    }

    public BigDecimal getEntrada() {
        if(entrada == null){
            entrada = BigDecimal.ZERO;
        }
        
        return entrada;
    }

    public void setEntrada(BigDecimal entrada) {
        this.entrada = entrada;
    }

    public BigDecimal getSaida() {
        if(saida == null){
            saida = BigDecimal.ZERO;
        }
        return saida;
    }

    public void setSaida(BigDecimal saida) {
        this.saida = saida;
    }

    public BigDecimal getSaldoInicialCheque() {
        if(saldoInicialCheque==null){
            saldoInicialCheque = BigDecimal.ZERO;
        }
        return saldoInicialCheque;
    }

    public void setSaldoInicialCheque(BigDecimal saldoInicialCheque) {
        this.saldoInicialCheque = saldoInicialCheque;
    }

    public BigDecimal getSaldoInicialDinheiro() {
        if(saldoInicialDinheiro==null){
            saldoInicialDinheiro = BigDecimal.ZERO;
        }
        return saldoInicialDinheiro;
    }

    public void setSaldoInicialDinheiro(BigDecimal saldoInicialDinheiro) {
        this.saldoInicialDinheiro = saldoInicialDinheiro;
    }

    @Override
    public boolean equals(Object movimentacaoCaixaFormaPagamento){
        if(movimentacaoCaixaFormaPagamento instanceof MovimentacaoCaixaFormaPagamentoVO){
        return (this.getTipoFormaPagamento().equals(((MovimentacaoCaixaFormaPagamentoVO)movimentacaoCaixaFormaPagamento).getTipoFormaPagamento()));
        }
        return false;
    }
    
    @Override
    public int hashCode(){
        return getTipoFormaPagamento().hashCode();
    }
    
//    @Override
//    public int compareTo(MovimentacaoCaixaFormaPagamentoVO movimentacaoCaixaFormaPagamento){
//       if(movimentacaoCaixaFormaPagamento instanceof MovimentacaoCaixaFormaPagamentoVO){
//        return (this.getTipoFormaPagamento().compareTo(((MovimentacaoCaixaFormaPagamentoVO)movimentacaoCaixaFormaPagamento).getTipoFormaPagamento()));
//        }
//        return -1;
//    }
    
    public Boolean getIsCheque(){
        return getTipoFormaPagamento().equals(TipoFormaPagamentoEnum.CHEQUE);
    }
    
    public Boolean getIsDinheiro(){
        return getTipoFormaPagamento().equals(TipoFormaPagamentoEnum.DINHEIRO);
    }
    
    public Boolean getIsPossuiSaldoMovimentarFinanceiramente(){
        return getSaldo().compareTo(BigDecimal.ZERO) > 0 && (getIsCheque() || getIsDinheiro());
    }
    
    
}
