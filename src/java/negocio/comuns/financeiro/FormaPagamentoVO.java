/**
 * 
 */
package negocio.comuns.financeiro;

import java.io.Serializable;

import negocio.comuns.arquitetura.SuperVO;
import negocio.comuns.financeiro.enumeradores.TipoFormaPagamentoEnum;

/**
 * @author Carlos Eug�nio
 *
 */
public class FormaPagamentoVO extends SuperVO implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer codigo;
	private String nome;
	private TipoFormaPagamentoEnum tipoFormaPagamento;

	public FormaPagamentoVO() {
		super();
	}

	public Integer getCodigo() {
		if (codigo == null) {
			codigo = 0;
		}
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		if (nome == null) {
			nome = "";
		}
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public TipoFormaPagamentoEnum getTipoFormaPagamento() {
		if (tipoFormaPagamento == null) {
			tipoFormaPagamento = TipoFormaPagamentoEnum.DINHEIRO;
		}
		return tipoFormaPagamento;
	}
	
	public String getTipoFormaPagamento_Apresentar() {
		return getTipoFormaPagamento().getDescricao();
	}

	public void setTipoFormaPagamento(TipoFormaPagamentoEnum tipoFormaPagamento) {
		this.tipoFormaPagamento = tipoFormaPagamento;
	}

	public Boolean getIsTipoDinheiro() {
		return getTipoFormaPagamento().equals(TipoFormaPagamentoEnum.DINHEIRO);
	}
	
	public Boolean getIsTipoCheque() {
		return getTipoFormaPagamento().equals(TipoFormaPagamentoEnum.CHEQUE);
	}
	
	public Boolean getIsTipoBoletoBancario() {
		return getTipoFormaPagamento().equals(TipoFormaPagamentoEnum.BOLETO);
	}
	
}
