/**
 * 
 */
package negocio.comuns.financeiro;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import negocio.comuns.administrativo.EmpresaVO;
import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.arquitetura.SuperVO;
import negocio.comuns.basico.FornecedorVO;
import negocio.comuns.financeiro.enumeradores.CedenteEnum;
import negocio.comuns.financeiro.enumeradores.TipoSacadoEnum;
import negocio.comuns.utilitarias.Uteis;

/**
 * @author Carlos Eug�nio
 *
 */
public class PagamentoVO extends SuperVO implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer codigo;
	private Date dataPagamento;
	private FornecedorVO fornecedorVO;
	private ContaCorrenteVO contaCaixaVO;
	private UsuarioVO responsavelVO;
	private EmpresaVO empresaVO;
	private List<ContaPagarPagamentoVO> listaContaPagarPagamentoVOs;
	private List<FormaPagamentoPagamentoVO> listaFormaPagamentoPagamentoVOs;

	private BigDecimal valorTotalPagar;
	private BigDecimal valorTotalJuro;
	private BigDecimal valorTotalMulta;
	private BigDecimal valorTotalDesconto;
	private BigDecimal valorTotalPago;
	private BigDecimal valorTotalContaPagar;
	private Boolean estornado;

	public PagamentoVO() {
		super();
	}

	/**
	 * @author Carlos Eug�nio - 17/01/2017
	 */

	public Integer getCodigo() {
		if (codigo == null) {
			codigo = 0;
		}
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public Date getDataPagamento() {
		if (dataPagamento == null) {
			dataPagamento = new Date();
		}
		return dataPagamento;
	}
	
	public String getDataPagamento_Apresentar() {
		return Uteis.getDataAno4Digitos(getDataPagamento());
	}

	public void setDataPagamento(Date dataPagamento) {
		this.dataPagamento = dataPagamento;
	}

	public FornecedorVO getFornecedorVO() {
		if (fornecedorVO == null) {
			fornecedorVO = new FornecedorVO();
		}
		return fornecedorVO;
	}

	public void setFornecedorVO(FornecedorVO fornecedorVO) {
		this.fornecedorVO = fornecedorVO;
	}

	public ContaCorrenteVO getContaCaixaVO() {
		if (contaCaixaVO == null) {
			contaCaixaVO = new ContaCorrenteVO();
		}
		return contaCaixaVO;
	}

	public void setContaCaixaVO(ContaCorrenteVO contaCaixaVO) {
		this.contaCaixaVO = contaCaixaVO;
	}

	public UsuarioVO getResponsavelVO() {
		if (responsavelVO == null) {
			responsavelVO = new UsuarioVO();
		}
		return responsavelVO;
	}

	public void setResponsavelVO(UsuarioVO responsavelVO) {
		this.responsavelVO = responsavelVO;
	}

	public List<ContaPagarPagamentoVO> getListaContaPagarPagamentoVOs() {
		if (listaContaPagarPagamentoVOs == null) {
			listaContaPagarPagamentoVOs = new ArrayList<ContaPagarPagamentoVO>(0);
		}
		return listaContaPagarPagamentoVOs;
	}

	public void setListaContaPagarPagamentoVOs(List<ContaPagarPagamentoVO> listaContaPagarPagamentoVOs) {
		this.listaContaPagarPagamentoVOs = listaContaPagarPagamentoVOs;
	}

	public List<FormaPagamentoPagamentoVO> getListaFormaPagamentoPagamentoVOs() {
		if (listaFormaPagamentoPagamentoVOs == null) {
			listaFormaPagamentoPagamentoVOs = new ArrayList<FormaPagamentoPagamentoVO>(0);
		}
		return listaFormaPagamentoPagamentoVOs;
	}

	public void setListaFormaPagamentoPagamentoVOs(List<FormaPagamentoPagamentoVO> listaFormaPagamentoPagamentoVOs) {
		this.listaFormaPagamentoPagamentoVOs = listaFormaPagamentoPagamentoVOs;
	}

	public BigDecimal getValorPago() {
		BigDecimal valorPago = BigDecimal.ZERO;
		for (FormaPagamentoPagamentoVO formaPagamentoPagamentoVO : getListaFormaPagamentoPagamentoVOs()) {
			valorPago = valorPago.add(formaPagamentoPagamentoVO.getValor());
		}
		return valorPago;
	}

	public TipoSacadoEnum getCedenteVO() {
		return TipoSacadoEnum.FORNECEDOR;
	}

	public String getNomeCedente() {
		return getFornecedorVO().getNome();
	}

	public Integer getCodigoCedente() {
		return getFornecedorVO().getCodigo();
	}

	public EmpresaVO getEmpresaVO() {
		if (empresaVO == null) {
			empresaVO = new EmpresaVO();
		}
		return empresaVO;
	}

	public void setEmpresaVO(EmpresaVO empresaVO) {
		this.empresaVO = empresaVO;
	}

	public CedenteEnum getCedente() {
		return CedenteEnum.FORNECEDOR;
	}

	public BigDecimal getValorTotalPagar() {
		if (valorTotalPagar == null) {
			valorTotalPagar = BigDecimal.ZERO;
		}
		return valorTotalPagar;
	}

	public void setValorTotalPagar(BigDecimal valorTotalPagar) {
		this.valorTotalPagar = valorTotalPagar;
	}

	public BigDecimal getValorTotalJuro() {
		if (valorTotalJuro == null) {
			valorTotalJuro = BigDecimal.ZERO;
		}
		return valorTotalJuro;
	}

	public void setValorTotalJuro(BigDecimal valorTotalJuro) {
		this.valorTotalJuro = valorTotalJuro;
	}

	public BigDecimal getValorTotalMulta() {
		if (valorTotalMulta == null) {
			valorTotalMulta = BigDecimal.ZERO;
		}
		return valorTotalMulta;
	}

	public void setValorTotalMulta(BigDecimal valorTotalMulta) {
		this.valorTotalMulta = valorTotalMulta;
	}

	public BigDecimal getValorTotalDesconto() {
		if (valorTotalDesconto == null) {
			valorTotalDesconto = BigDecimal.ZERO;
		}
		return valorTotalDesconto;
	}

	public void setValorTotalDesconto(BigDecimal valorTotalDesconto) {
		this.valorTotalDesconto = valorTotalDesconto;
	}

	public BigDecimal getValorTotalPago() {
		if (valorTotalPago == null) {
			valorTotalPago = BigDecimal.ZERO;
		}
		return valorTotalPago;
	}

	public void setValorTotalPago(BigDecimal valorTotalPago) {
		this.valorTotalPago = valorTotalPago;
	}

	public BigDecimal getValorTotalContaPagar() {
		if (valorTotalContaPagar == null) {
			valorTotalContaPagar = BigDecimal.ZERO;
		}
		return valorTotalContaPagar;
	}

	public void setValorTotalContaPagar(BigDecimal valorTotalContaPagar) {
		this.valorTotalContaPagar = valorTotalContaPagar;
	}

	public Boolean getEstornado() {
		if (estornado == null) {
			estornado = false;
		}
		return estornado;
	}

	public void setEstornado(Boolean estornado) {
		this.estornado = estornado;
	}

	  public BigDecimal getValorPendente(){
		  if(getValorTotalPago().compareTo(getValorTotalPagar()) < 0){
			  return  getValorTotalPagar().subtract(getValorTotalPago());
		  }
		  return BigDecimal.ZERO;
	  }
	  
	  public BigDecimal getTroco(){
	        if(Uteis.arrendondarForcando2CadasDecimais(getValorTotalPagar().doubleValue()) < (Uteis.arrendondarForcando2CadasDecimais(getValorTotalPago().doubleValue()))){
	            return  getValorTotalPago().subtract(getValorTotalPagar());
	        }
	        return BigDecimal.ZERO;
	  }

}
