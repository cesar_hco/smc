package negocio.comuns.financeiro;

import java.io.Serializable;
import java.math.BigDecimal;

import negocio.comuns.administrativo.EmpresaVO;
import negocio.comuns.arquitetura.SuperVO;
import negocio.comuns.utilitarias.UteisJSF;

public class ContaCorrenteVO extends SuperVO implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer codigo;
	private String descricao;
	private String digitoContaCorrente;
	private String numero;
	private BigDecimal saldo;
	private String carteira;
	private String cedente;
	private String convenio;
	
	private String banco;
	private String nrBanco;
	private String agencia;
	private String digitoAgencia;
	private Boolean contaCaixa;
	private EmpresaVO empresaVO;

	public ContaCorrenteVO() {
		super();
	}

	public Integer getCodigo() {
		if (codigo == null) {
			codigo = 0;
		}
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		if (descricao == null) {
			descricao = "";
		}
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getDigitoContaCorrente() {
		if (digitoContaCorrente == null) {
			digitoContaCorrente = "";
		}
		return digitoContaCorrente;
	}

	public void setDigitoContaCorrente(String digitoContaCorrente) {
		this.digitoContaCorrente = digitoContaCorrente;
	}

	public String getNumero() {
		if (numero == null) {
			numero = "";
		}
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public BigDecimal getSaldo() {
		if (saldo == null) {
			saldo = BigDecimal.ZERO;
		}
		return saldo;
	}

	public void setSaldo(BigDecimal saldo) {
		this.saldo = saldo;
	}

	public String getCarteira() {
		if (carteira == null) {
			carteira = "";
		}
		return carteira;
	}

	public void setCarteira(String carteira) {
		this.carteira = carteira;
	}

	public String getCedente() {
		if (cedente == null) {
			cedente = "";
		}
		return cedente;
	}

	public void setCedente(String cedente) {
		this.cedente = cedente;
	}

	public String getConvenio() {
		if (convenio == null) {
			convenio = "";
		}
		return convenio;
	}

	public void setConvenio(String convenio) {
		this.convenio = convenio;
	}

	public String getBanco() {
		if (banco == null) {
			banco = "";
		}
		return banco;
	}

	public void setBanco(String banco) {
		this.banco = banco;
	}

	public String getNrBanco() {
		if (nrBanco == null) {
			nrBanco = "";
		}
		return nrBanco;
	}

	public void setNrBanco(String nrBanco) {
		this.nrBanco = nrBanco;
	}

	public String getAgencia() {
		if (agencia == null) {
			agencia = "";
		}
		return agencia;
	}

	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}

	public String getDigitoAgencia() {
		if (digitoAgencia == null) {
			digitoAgencia = "";
		}
		return digitoAgencia;
	}

	public void setDigitoAgencia(String digitoAgencia) {
		this.digitoAgencia = digitoAgencia;
	}

	public String getAgenciaBanco_Apresentar() {
		return getAgencia() + " - " + getNrBanco() + " " + getBanco();
	}

	public Boolean getContaCaixa() {
		if (contaCaixa == null) {
			contaCaixa = false;
		}
		return contaCaixa;
	}

	public void setContaCaixa(Boolean contaCaixa) {
		this.contaCaixa = contaCaixa;
	}

	public String getDescricaoApresentar() {
        if (getContaCaixa()) {
            return getDescricao();
        }
        if (!getBanco().isEmpty()) {
            return "Banco: " + getBanco() + " - Ag�ncia: " + getAgencia() +"-" +getDigitoAgencia()+ " - Conta Corrente: " + getNumero() + "-" + getDigitoContaCorrente();
        }
        return "";
    }

	public EmpresaVO getEmpresaVO() {
		if (empresaVO == null) {
			empresaVO = new EmpresaVO();
		}
		return empresaVO;
	}

	public void setEmpresaVO(EmpresaVO empresaVO) {
		this.empresaVO = empresaVO;
	}
	
	public String getContaCaixa_Apresentar() {
		if (getContaCaixa()) {
			return "Sim";
		}
		return "N�o";
	}
}
