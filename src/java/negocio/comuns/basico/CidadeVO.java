/**
 * 
 */
package negocio.comuns.basico;

import java.io.Serializable;

import negocio.comuns.arquitetura.SuperVO;

/**
 * @author Carlos Eug�nio
 *
 */
public class CidadeVO extends SuperVO implements Serializable {
	
	private Integer codigo;
	private String nome;
	private EstadoVO estado;
	private String cep;
	private Integer codigoInep;
	private String codigoIBGE;
	private Integer codigoDistrito;
	private static final long serialVersionUID = 1L;

	public CidadeVO() {
		super();
	}

	public Integer getCodigo() {
		if (codigo == null) {
			codigo = 0;
		}
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		if (nome == null) {
			nome = "";
		}
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public EstadoVO getEstado() {
		if (estado == null) {
			estado = new EstadoVO();
		}
		return estado;
	}

	public void setEstado(EstadoVO estado) {
		this.estado = estado;
	}

	public String getCep() {
		if (cep == null) {
			cep = "";
		}
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public Integer getCodigoInep() {
		if (codigoInep == null) {
			codigoInep = 0;
		}
		return codigoInep;
	}

	public void setCodigoInep(Integer codigoInep) {
		this.codigoInep = codigoInep;
	}

	public String getCodigoIBGE() {
		if (codigoIBGE == null) {
			codigoIBGE = "";
		}
		return codigoIBGE;
	}

	public void setCodigoIBGE(String codigoIBGE) {
		this.codigoIBGE = codigoIBGE;
	}

	public Integer getCodigoDistrito() {
		if (codigoDistrito == null) {
			codigoDistrito = 0;
		}
		return codigoDistrito;
	}

	public void setCodigoDistrito(Integer codigoDistrito) {
		this.codigoDistrito = codigoDistrito;
	}
}
