package negocio.comuns.basico;

import java.io.Serializable;
import java.math.BigDecimal;

import negocio.comuns.arquitetura.SuperVO;

public class ClienteVO extends SuperVO implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer codigo;
	private PessoaVO pessoaVO;
	private String observacao;
	private BigDecimal limiteCredito;
	private BigDecimal percentualMaximoDesconto;
	private BigDecimal icms;
	private BigDecimal percentualLimiteCliente;

	public ClienteVO() {
		super();
	}

	public String getObservacao() {
		if (observacao == null) {
			observacao = "";
		}
		return observacao;
	}

	public BigDecimal getLimiteCredito() {
		if (limiteCredito == null) {
			limiteCredito = BigDecimal.ZERO;
		}
		return limiteCredito;
	}

	public BigDecimal getPercentualMaximoDesconto() {
		if (percentualMaximoDesconto == null) {
			percentualMaximoDesconto = BigDecimal.ZERO;
		}
		return percentualMaximoDesconto;
	}

	public BigDecimal getIcms() {
		return icms;
	}

	public BigDecimal getPercentualLimiteCliente() {
		if (percentualLimiteCliente == null) {
			percentualLimiteCliente = BigDecimal.ZERO;
		}
		return percentualLimiteCliente;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public void setLimiteCredito(BigDecimal limiteCredito) {
		this.limiteCredito = limiteCredito;
	}

	public void setPercentualMaximoDesconto(BigDecimal percentualMaximoDesconto) {
		this.percentualMaximoDesconto = percentualMaximoDesconto;
	}

	public void setIcms(BigDecimal icms) {
		this.icms = icms;
	}

	public void setPercentualLimiteCliente(BigDecimal percentualLimiteCliente) {
		this.percentualLimiteCliente = percentualLimiteCliente;
	}

	public PessoaVO getPessoaVO() {
		if (pessoaVO == null) {
			pessoaVO = new PessoaVO();
		}
		return pessoaVO;
	}

	public void setPessoaVO(PessoaVO pessoaVO) {
		this.pessoaVO = pessoaVO;
	}

	public Integer getCodigo() {
		if (codigo == null) {
			codigo = 0;
		}
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

}
