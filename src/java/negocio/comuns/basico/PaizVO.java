package negocio.comuns.basico;

import negocio.comuns.arquitetura.SuperVO;

/**
 * Repons�vel por manter os dados da entidade Paiz. Classe do tipo VO - Value
 * Object composta pelos atributos da entidade com visibilidade protegida e os
 * m�todos de acesso a estes atributos. Classe utilizada para apresentar e
 * manter em mem�ria os dados desta entidade.
 * 
 * @see SuperVO
 */
 public class PaizVO extends SuperVO  {

	private Integer codigo;
	private String nome;
	private boolean nacao;
	private String siglaInep;
	private String nacionalidade;
        public static final long serialVersionUID = 1L;

	/**
	 * Construtor padr�o da classe <code>Paiz</code>. Cria uma nova inst�ncia
	 * desta entidade, inicializando automaticamente seus atributos (Classe VO).
	 */
	public PaizVO() {
		super();
	}

	/**
	 * Opera��o respons�vel por validar os dados de um objeto da classe
	 * <code>PaizVO</code>. Todos os tipos de consist�ncia de dados s�o e devem
	 * ser implementadas neste m�todo. S�o valida��es t�picas: verifica��o de
	 * campos obrigat�rios, verifica��o de valores v�lidos para os atributos.
	 * 
	 * @exception ConsistirExecption
	 *                Se uma inconsist�ncia for encontrada aumaticamente �
	 *                gerada uma exce��o descrevendo o atributo e o erro
	 *                ocorrido.
	 */
	public static void validarDados(PaizVO obj) throws Exception {
		if (obj.getNome().equals("")) {
			throw new Exception("O campo NOME (Pa�s) deve ser informado.");
		}
		if (obj.getNacionalidade().equals("")) {
			throw new Exception("O campo NACIONALIDADE (Pa�s) deve ser informado.");
		}
	}

	public String getNome() {
		if (nome == null) {
			nome = "";
		}
		return (nome);
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getCodigo() {
		if (codigo == null) {
			codigo = 0;
		}
		return (codigo);
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public boolean isNacao() {
		return nacao;
	}

	public void setNacao(boolean nacao) {
		this.nacao = nacao;
	}

	public String getSiglaInep() {
            if(siglaInep == null){
                siglaInep = "";
            }
		return siglaInep;
	}

	public String getSiglaInepTratado() {
		if (isNacao() && (siglaInep == null || siglaInep.equals(""))) {
			siglaInep = "BRA";
		}
		if (!isNacao() && (siglaInep == null) || siglaInep.equals("")) {
			siglaInep = "ARG";
		}
		return siglaInep;
	}

	public void setSiglaInep(String siglaInep) {
		this.siglaInep = siglaInep;
	}

	/**
	 * @return the nacionalidade
	 */
	public String getNacionalidade() {
		if (nacionalidade == null) {
			nacionalidade = "";
		}
		return nacionalidade;
	}

	/**
	 * @param nacionalidade
	 *            the nacionalidade to set
	 */
	public void setNacionalidade(String nacionalidade) {
		this.nacionalidade = nacionalidade;
	}
}