package negocio.comuns.basico;

import java.io.Serializable;

import negocio.comuns.arquitetura.SuperVO;

public class FornecedorVO extends SuperVO implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer codigo;
	private String nome;
    private String cnpj;
    private String inscricaoEstadual;
    private String cep;
    private String endereco;
    private String bairro;
    private String complemento;
    private CidadeVO cidadeVO;
    private String telefone1;
    private String telefone2;

    public FornecedorVO() {}

    public String getNome() {
        if(nome == null ) {
            nome = "";
         }
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCnpj() {
        if(cnpj == null ) {
            cnpj = "";
         }
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getInscricaoEstadual() {
        if(inscricaoEstadual == null ) {
            inscricaoEstadual = "";
         }
        return inscricaoEstadual;
    }

    public void setInscricaoEstadual(String inscricaoEstadual) {
        this.inscricaoEstadual = inscricaoEstadual;
    }

    public String getCep() {
        if(cep == null ) {
            cep = "";
         }
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getEndereco() {
        if(endereco == null ) {
            endereco = "";
         }
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getBairro() {
        if(bairro == null ) {
            bairro = "";
         }
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getComplemento() {
        if(complemento == null ) {
            complemento = "";
         }
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public CidadeVO getCidadeVO() {
        if(cidadeVO == null ) {
            cidadeVO = new CidadeVO();
         }
        return cidadeVO;
    }

    public void setCidadeVO(CidadeVO cidadeVO) {
        this.cidadeVO = cidadeVO;
    }

    public String getTelefone1() {
        if(telefone1 == null ) {
            telefone1 = "";
         }
        return telefone1;
    }

    public void setTelefone1(String telefone1) {
        this.telefone1 = telefone1;
    }

    public Integer getCodigo() {
		if (codigo == null) {
			codigo = 0;
		}
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getTelefone2() {
        if(telefone2 == null ) {
            telefone2 = "";
         }
        return telefone2;
    }

    public void setTelefone2(String telefone2) {
        this.telefone2 = telefone2;
    }


}
