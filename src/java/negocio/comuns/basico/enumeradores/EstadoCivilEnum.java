package negocio.comuns.basico.enumeradores;

public enum EstadoCivilEnum {

	CASADO("Casado(a)") ,
	SOLTEIRO("Solteiro(a)") ,
	DIVORCIADO("Divorciado(a)") ,
	VIUVO("Viuvo(a)") ,
	SEPARADO("Separado(a)"),
	COMPANHEIRO("Companheiro(a)");
	
	
	private String descricao;

	EstadoCivilEnum(String descricao) {
		this.descricao = descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
}
