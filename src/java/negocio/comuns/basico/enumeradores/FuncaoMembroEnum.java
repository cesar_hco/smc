/**
 * 
 */
package negocio.comuns.basico.enumeradores;

/**
 * @author Carlos Eug�nio
 *
 */
public enum FuncaoMembroEnum {
	PASTOR,
	DISCIPULADOR,
	LIDER,
	LIDER_TREINAMENTO;
}
