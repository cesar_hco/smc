package negocio.comuns.basico;

import java.util.List;

import negocio.comuns.arquitetura.SuperVO;
import negocio.comuns.basico.enumeradores.RegiaoEnum;

public class EstadoVO extends SuperVO {

	private Integer codigo;
	private String sigla;
	private String nome;
	/**
	 * Atributo respons�vel por manter o objeto relacionado da classe
	 * <code>Paiz </code>.
	 */
	private PaizVO paiz;
	private Integer codigoInep;
	private RegiaoEnum regiao;
	public static final long serialVersionUID = 1L;
    private String codigoIBGE;

	/**
	 * Construtor padr�o da classe <code>Estado</code>. Cria uma nova inst�ncia
	 * desta entidade, inicializando automaticamente seus atributos (Classe VO).
	 */
	public EstadoVO() {
		super();
	}



	
	public static void validarDados(EstadoVO obj) throws Exception {
		if (!obj.isValidarDados().booleanValue()) {
			return;
		}
		// if (obj.getSigla().equals("")) {
		// throw new
		// ConsistirException("O campo SIGLA (ESTADO) deve ser informado.");
		// }
		if (obj.getNome().equals("")) {
			throw new Exception("O campo NOME (ESTADO) deve ser informado.");
		}
		if ((obj.getPaiz() == null) || (obj.getPaiz().getCodigo().intValue() == 0)) {
			throw new Exception("O campo PA�S (ESTADO) deve ser informado.");
		}
	}

	
	public PaizVO getPaiz() {
		if (paiz == null) {
			paiz = new PaizVO();
		}
		return (paiz);
	}

	
	public void setPaiz(PaizVO obj) {
		this.paiz = obj;
	}

	public String getNome() {
		if (nome == null) {
			nome = "";
		}
		return (nome);
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSigla() {
		if (sigla == null) {
			sigla = "";
		}
		return (sigla);
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public Integer getCodigo() {
		if (codigo == null) {
			codigo = 0;
		}
		return (codigo);
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public Integer getCodigoInep() {
		if (codigoInep == null) {
			codigoInep = 0;
		}
		return codigoInep;
	}

	public void setCodigoInep(Integer codigoInep) {
		this.codigoInep = codigoInep;
	}

	public RegiaoEnum getRegiao() {
		return regiao;
	}

	public void setRegiao(RegiaoEnum regiao) {
		this.regiao = regiao;
	}

	public String getCodigoIBGE() {
		if (codigoIBGE == null) {
			codigoIBGE = "";
		}
		return codigoIBGE;
	}

	public void setCodigoIBGE(String codigoIBGE) {
		this.codigoIBGE = codigoIBGE;
	}
    
    
}
