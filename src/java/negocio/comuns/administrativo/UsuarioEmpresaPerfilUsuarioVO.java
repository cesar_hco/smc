/**
 * 
 */
package negocio.comuns.administrativo;

import java.io.Serializable;

import negocio.comuns.arquitetura.SuperVO;

/**
 * @author Carlos Eug�nio
 *
 */
public class UsuarioEmpresaPerfilUsuarioVO extends SuperVO implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer codigo;
	private EmpresaVO empresaVO;
	private UsuarioVO usuarioVO;
	private PerfilUsuarioVO perfilUsuarioVO;

	public UsuarioEmpresaPerfilUsuarioVO() {
		super();
	}

	public Integer getCodigo() {
		if (codigo == null) {
			codigo = 0;
		}
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public EmpresaVO getEmpresaVO() {
		if (empresaVO == null) {
			empresaVO = new EmpresaVO();
		}
		return empresaVO;
	}

	public void setEmpresaVO(EmpresaVO empresaVO) {
		this.empresaVO = empresaVO;
	}

	public UsuarioVO getUsuarioVO() {
		if (usuarioVO == null) {
			usuarioVO = new UsuarioVO();
		}
		return usuarioVO;
	}

	public void setUsuarioVO(UsuarioVO usuarioVO) {
		this.usuarioVO = usuarioVO;
	}

	public PerfilUsuarioVO getPerfilUsuarioVO() {
		if (perfilUsuarioVO == null) {
			perfilUsuarioVO = new PerfilUsuarioVO();
		}
		return perfilUsuarioVO;
	}

	public void setPerfilUsuarioVO(PerfilUsuarioVO perfilUsuarioVO) {
		this.perfilUsuarioVO = perfilUsuarioVO;
	}
}
