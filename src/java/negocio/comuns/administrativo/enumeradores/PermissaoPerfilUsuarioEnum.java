package negocio.comuns.administrativo.enumeradores;

import java.util.ArrayList;
import java.util.List;

public enum PermissaoPerfilUsuarioEnum {
	MENU_ADMINISTRATIVO("MENU_ADMINISTRATIVO", "MenuAdministrativo", true, "Administrativo"),
	DISCIPULADOR("MENU_ADMINISTRATIVO", "Discipulador", false, "Discipulador"),
	MEMBRO("MENU_ADMINISTRATIVO", "Membro", false, "Membro"),
	PASTOR("MENU_ADMINISTRATIVO", "Pastor", false, "Pastor"),
	PREDIO("MENU_ADMINISTRATIVO", "Predio", false, "Pr�dio"),
	REGISTRO_FREQUENCIA_CELULA("MENU_ADMINISTRATIVO", "RegistroFrequenciaCelula", false, "Registro Frequ�ncia C�lula"),
	
	MENU_BASICO("MENU_BASICO", "MenuBasico", true, "B�sico"),
	PAIS("MENU_BASICO", "Pais", false, "Pa�s"),
	ESTADO("MENU_BASICO", "Estado", false, "Estado"),
	CIDADE("MENU_BASICO", "Cidade", false, "Cidade"),
	CONFIGURACAO_GERAL("MENU_BASICO", "ConfiguracaoGeral", false, "Configura��o Geral"),
	
	MENU_USUARIO("MENU_USUARIO", "MenuUsuario", true, "Usu�rio"),
	PERFIL_USUARIO("MENU_USUARIO", "PerfilUsuario", false, "Perfil Usu�rio"),
	USUARIO("MENU_USUARIO", "Usuario", false, "Usu�rio"),
	
	MENU_RELATORIO("MENU_RELATORIO", "MenuRelatorio", true, "Relat�rio"),
	RELATORIO_CELULA("MENU_RELATORIO", "RelatorioCelula", false, "Relat�rio da C�lula"),
	
	MENU_ENCONTRO("MENU_ENCONTRO", "MenuEncontro", true, "Encontro com Deus"),
	ENCONTRO_COM_DEUS("MENU_ENCONTRO", "EncontroComDeus", false, "Encontro com Deus");
	
	
	String menu;
	String key;
	Boolean modulo;
    String descricao;

    PermissaoPerfilUsuarioEnum(String menu, String key, Boolean modulo, String descricao) {
    	this.menu = menu;
    	this.key = key;
    	this.modulo = modulo;
        this.descricao = descricao;
    }

    public static PermissaoPerfilUsuarioEnum getEnum(String menu) {
    	PermissaoPerfilUsuarioEnum[] valores = values();
        for (PermissaoPerfilUsuarioEnum obj : valores) {
            if (obj.getMenu().equals(menu)) {
                return obj;
            }
        }
        return null;
    }
    
	public static List<PermissaoPerfilUsuarioEnum> getListaPorMenuEnum(String menu) {
		List<PermissaoPerfilUsuarioEnum> listaEnumVOs = new ArrayList<PermissaoPerfilUsuarioEnum>(0);
		PermissaoPerfilUsuarioEnum[] valores = values();
		for (PermissaoPerfilUsuarioEnum obj : valores) {
			if (!menu.equals("")) {
				if (obj.getMenu().equals(menu)) {
					listaEnumVOs.add(obj);
				}
			} else {
				listaEnumVOs.add(obj);
			}

		}
		return listaEnumVOs;
	}
	
	public static List<PermissaoPerfilUsuarioEnum> getObterListaModulo(String nomeModulo) {
		List<PermissaoPerfilUsuarioEnum> listaEnumVOs = new ArrayList<PermissaoPerfilUsuarioEnum>(0);
		PermissaoPerfilUsuarioEnum[] valores = values();
		for (PermissaoPerfilUsuarioEnum obj : valores) {
			if (!nomeModulo.equals("")) {
				if (obj.getMenu().equals(nomeModulo)) {
					if (obj.getModulo()) {
						listaEnumVOs.add(obj);
					}
				}
			} else {
				if (obj.getModulo()) {
					listaEnumVOs.add(obj);
				}
			}

		}
		return listaEnumVOs;
	}
	
	public static List<PermissaoPerfilUsuarioEnum> getObterListaSubModulo(String nomeModulo) {
		List<PermissaoPerfilUsuarioEnum> listaEnumVOs = new ArrayList<PermissaoPerfilUsuarioEnum>(0);
		PermissaoPerfilUsuarioEnum[] valores = values();
		for (PermissaoPerfilUsuarioEnum obj : valores) {
			if (!nomeModulo.equals("")) {
				if (obj.getMenu().equals(nomeModulo)) {
					if (!obj.getModulo()) {
						listaEnumVOs.add(obj);
					}
				}
			} else {
				if (!obj.getModulo()) {
					listaEnumVOs.add(obj);
				}
			}

		}
		return listaEnumVOs;
	}

    public static String getDescricao(String valor) {
    	PermissaoPerfilUsuarioEnum obj = getEnum(valor);
        if (obj != null) {
            return obj.getDescricao();
        }
        return valor;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

	public String getMenu() {
		if (menu == null) {
			menu = "";
		}
		return menu;
	}

	public void setMenu(String menu) {
		this.menu = menu;
	}

	public String getKey() {
		if (key == null) {
			key = "";
		}
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Boolean getModulo() {
		if (modulo == null) {
			modulo = false;
		}
		return modulo;
	}

	public void setModulo(Boolean modulo) {
		this.modulo = modulo;
	}
}
