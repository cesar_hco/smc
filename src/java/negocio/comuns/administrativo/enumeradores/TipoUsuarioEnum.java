package negocio.comuns.administrativo.enumeradores;

public enum TipoUsuarioEnum {
	ADMINISTRADOR(""),
	FUNCIONARIO("Funcionário");
	
	
	String descricao;
	TipoUsuarioEnum(String descricao) {
		this.descricao = descricao;
	}
	public String getDescricao() {
		if (descricao == null) {
			descricao = "";
		}
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
}
