/**
 * 
 */
package negocio.comuns.administrativo;

import java.io.Serializable;

import negocio.comuns.arquitetura.SuperVO;

/**
 * @author Carlos Eug�nio
 *
 */
public class PermissaoUsuarioMenuSubModuloVO extends SuperVO implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer codigo;
	private PermissaoUsuarioMenuVO permissaoUsuarioMenuVO;
	private String nomeSubModulo;
	private Boolean permissaoSubModulo;
	private String descricaoSubModulo;
	private String nomeMenu;

	public PermissaoUsuarioMenuSubModuloVO() {
		super();
	}

	public Integer getCodigo() {
		if (codigo == null) {
			codigo = 0;
		}
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getNomeSubModulo() {
		if (nomeSubModulo == null) {
			nomeSubModulo = "";
		}
		return nomeSubModulo;
	}

	public Boolean getPermissaoSubModulo() {
		if (permissaoSubModulo == null) {
			permissaoSubModulo = false;
		}
		return permissaoSubModulo;
	}

	public void setNomeSubModulo(String nomeSubModulo) {
		this.nomeSubModulo = nomeSubModulo;
	}

	public void setPermissaoSubModulo(Boolean permissaoSubModulo) {
		this.permissaoSubModulo = permissaoSubModulo;
	}

	public PermissaoUsuarioMenuVO getPermissaoUsuarioMenuVO() {
		if (permissaoUsuarioMenuVO == null) {
			permissaoUsuarioMenuVO = new PermissaoUsuarioMenuVO();
		}
		return permissaoUsuarioMenuVO;
	}

	public void setPermissaoUsuarioMenuVO(PermissaoUsuarioMenuVO permissaoUsuarioMenuVO) {
		this.permissaoUsuarioMenuVO = permissaoUsuarioMenuVO;
	}

	public String getDescricaoSubModulo() {
		if (descricaoSubModulo == null) {
			descricaoSubModulo = "";
		}
		return descricaoSubModulo;
	}

	public void setDescricaoSubModulo(String descricaoSubModulo) {
		this.descricaoSubModulo = descricaoSubModulo;
	}

	public String getNomeMenu() {
		if (nomeMenu == null) {
			nomeMenu = "";
		}
		return nomeMenu;
	}

	public void setNomeMenu(String nomeMenu) {
		this.nomeMenu = nomeMenu;
	}

	
}
