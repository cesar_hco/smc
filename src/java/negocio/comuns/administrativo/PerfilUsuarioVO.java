/**
 * 
 */
package negocio.comuns.administrativo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import negocio.comuns.arquitetura.SuperVO;

/**
 * @author Carlos Eug�nio
 *
 */
public class PerfilUsuarioVO extends SuperVO implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer codigo;
	private String nome;
	private List<PermissaoUsuarioMenuVO> permissaoUsuarioMenuVOs;

	public PerfilUsuarioVO() {
		super();
	}


	public Integer getCodigo() {
		if (codigo == null) {
			codigo = 0;
		}
		return codigo;
	}


	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}


	public String getNome() {
		if (nome == null) {
			nome = "";
		}
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}


	public List<PermissaoUsuarioMenuVO> getPermissaoUsuarioMenuVOs() {
		if (permissaoUsuarioMenuVOs == null) {
			permissaoUsuarioMenuVOs = new ArrayList<PermissaoUsuarioMenuVO>(0);
		}
		return permissaoUsuarioMenuVOs;
	}


	public void setPermissaoUsuarioMenuVOs(List<PermissaoUsuarioMenuVO> permissaoUsuarioMenuVOs) {
		this.permissaoUsuarioMenuVOs = permissaoUsuarioMenuVOs;
	}
	
	

}
