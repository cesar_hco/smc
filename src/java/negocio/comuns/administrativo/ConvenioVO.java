package negocio.comuns.administrativo;

import java.io.Serializable;

import negocio.comuns.arquitetura.SuperVO;

public class ConvenioVO extends SuperVO implements Serializable{

	
	private Integer codigo;
	private String descricao;
	private Boolean ativo;
	private EmpresaVO empresa;

	
	public Integer getCodigo() {
		if (codigo == null) {
			codigo = 0;
		}
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public String getDescricao() {
		if (descricao == null) {
			descricao = "";
		}
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Boolean getAtivo() {
		if (ativo == null) {
			ativo = false;
		}
		return ativo;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	public EmpresaVO getEmpresa() {
		if (empresa == null) {
			empresa = new EmpresaVO();
		}
		return empresa;
	}
	public void setEmpresa(EmpresaVO empresa) {
		this.empresa = empresa;
	}
		
	
	
	
	
}
