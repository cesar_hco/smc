/**
 * 
 */
package negocio.comuns.administrativo;

import java.io.Serializable;

import negocio.comuns.arquitetura.SuperVO;
import negocio.comuns.basico.CidadeVO;

/**
 * @author Carlos Eug�nio
 *
 */
public class EmpresaVO extends SuperVO implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer codigo;
	private String descricao;
	private String nomeFantasia;
	private String cep;
	private String endereco;
	private String setor;
	private String complemento;
	private CidadeVO cidadeVO;
	private String inscricaoEstadual;
	private String cnpj;
	private String telefone1;
	private String telefone2;
	/**
	 * 
	 */
	public EmpresaVO()  {
		super();
	}
	/**
	 * @author Carlos Eug�nio - 26/10/2016
	 */
	public Integer getCodigo() {
		if (codigo == null) {
			codigo = 0;
		}
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public String getDescricao() {
		if (descricao == null) {
			descricao = "";
		}
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getNomeFantasia() {
		if (nomeFantasia == null) {
			nomeFantasia = "";
		}
		return nomeFantasia;
	}
	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}
	public String getCep() {
		if (cep == null) {
			cep = "";
		}
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getEndereco() {
		if (endereco == null) {
			endereco = "";
		}
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public String getSetor() {
		if (setor == null) {
			setor = "";
		}
		return setor;
	}
	public void setSetor(String setor) {
		this.setor = setor;
	}
	public String getComplemento() {
		if (complemento == null) {
			complemento = "";
		}
		return complemento;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public CidadeVO getCidadeVO() {
		if (cidadeVO == null) {
			cidadeVO = new CidadeVO();
		}
		return cidadeVO;
	}
	public void setCidadeVO(CidadeVO cidadeVO) {
		this.cidadeVO = cidadeVO;
	}
	public String getInscricaoEstadual() {
		if (inscricaoEstadual == null) {
			inscricaoEstadual = "";
		}
		return inscricaoEstadual;
	}
	public void setInscricaoEstadual(String inscricaoEstadual) {
		this.inscricaoEstadual = inscricaoEstadual;
	}
	public String getCnpj() {
		if (cnpj == null) {
			cnpj = "";
		}
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public String getTelefone1() {
		if (telefone1 == null) {
			telefone1 = "";
		}
		return telefone1;
	}
	public void setTelefone1(String telefone1) {
		this.telefone1 = telefone1;
	}
	public String getTelefone2() {
		if (telefone2 == null) {
			telefone2 = "";
		}
		return telefone2;
	}
	public void setTelefone2(String telefone2) {
		this.telefone2 = telefone2;
	}
}
