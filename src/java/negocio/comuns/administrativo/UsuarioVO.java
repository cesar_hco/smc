
/**
 * 
 */
package negocio.comuns.administrativo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import negocio.comuns.administrativo.enumeradores.TipoUsuarioEnum;
import negocio.comuns.arquitetura.SuperVO;
import negocio.comuns.basico.PessoaVO;

/**
 * @author Carlos Eug�nio
 *
 */
public class UsuarioVO extends SuperVO implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer codigo;
	private PessoaVO pessoaVO;
	private TipoUsuarioEnum tipoUsuario;
	private String userName;
	private String senha;
	private List<UsuarioEmpresaPerfilUsuarioVO> usuarioEmpresaPerfilUsuarioVOs;
	private Boolean exerceCargoAdministrativo;
	private Boolean alterarSenha;
	private FuncionarioVO funcionarioLogadoVO;

	/*
	 * Atributo Trasient
	 */
	private PerfilUsuarioVO perfilUsuarioVO;
	private EmpresaVO empresaLogado;

	/**
	 * 
	 */
	public UsuarioVO() {
		super();
	}

	/**
	 * @author Carlos Eug�nio - 20/06/2016
	 */

	public Integer getCodigo() {
		if (codigo == null) {
			codigo = 0;
		}
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public PessoaVO getPessoaVO() {
		if (pessoaVO == null) {
			pessoaVO = new PessoaVO();
		}
		return pessoaVO;
	}

	public void setPessoaVO(PessoaVO pessoaVO) {
		this.pessoaVO = pessoaVO;
	}

	public String getUserName() {
		if (userName == null) {
			userName = "";
		}
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getSenha() {
		if (senha == null) {
			senha = "";
		}
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public TipoUsuarioEnum getTipoUsuario() {
		if (tipoUsuario == null) {
			tipoUsuario = TipoUsuarioEnum.FUNCIONARIO;
		}
		return tipoUsuario;
	}

	public void setTipoUsuario(TipoUsuarioEnum tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}

	public PerfilUsuarioVO getPerfilUsuarioVO() {
		return perfilUsuarioVO;
	}

	public void setPerfilUsuarioVO(PerfilUsuarioVO perfilUsuarioVO) {
		this.perfilUsuarioVO = perfilUsuarioVO;
	}

	public PerfilUsuarioVO obterPerfilUsuarioPorEmpresa(Integer empresa) {
		for (UsuarioEmpresaPerfilUsuarioVO usuarioEmpresaPerfilUsuarioVO : getUsuarioEmpresaPerfilUsuarioVOs()) {
			if (usuarioEmpresaPerfilUsuarioVO.getEmpresaVO().getCodigo().equals(empresa)) {
				return usuarioEmpresaPerfilUsuarioVO.getPerfilUsuarioVO();
			}
		}
		return new PerfilUsuarioVO();
	}


	public Boolean getExerceCargoAdministrativo() {
		if (exerceCargoAdministrativo == null) {
			exerceCargoAdministrativo = false;
		}
		return exerceCargoAdministrativo;
	}

	public void setExerceCargoAdministrativo(Boolean exerceCargoAdministrativo) {
		this.exerceCargoAdministrativo = exerceCargoAdministrativo;
	}

	public Boolean getAlterarSenha() {
		if (alterarSenha == null) {
			alterarSenha = false;
		}
		return alterarSenha;
	}

	public void setAlterarSenha(Boolean alterarSenha) {
		this.alterarSenha = alterarSenha;
	}

	public List<UsuarioEmpresaPerfilUsuarioVO> getUsuarioEmpresaPerfilUsuarioVOs() {
		if (usuarioEmpresaPerfilUsuarioVOs == null) {
			usuarioEmpresaPerfilUsuarioVOs = new ArrayList<UsuarioEmpresaPerfilUsuarioVO>(0);
		}
		return usuarioEmpresaPerfilUsuarioVOs;
	}

	public void setUsuarioEmpresaPerfilUsuarioVOs(List<UsuarioEmpresaPerfilUsuarioVO> usuarioEmpresaPerfilUsuarioVOs) {
		this.usuarioEmpresaPerfilUsuarioVOs = usuarioEmpresaPerfilUsuarioVOs;
	}

	public FuncionarioVO getFuncionarioLogadoVO() {
		if (funcionarioLogadoVO == null) {
			funcionarioLogadoVO = new FuncionarioVO();
		}
		return funcionarioLogadoVO;
	}

	public void setFuncionarioLogadoVO(FuncionarioVO funcionarioLogadoVO) {
		this.funcionarioLogadoVO = funcionarioLogadoVO;
	}

	public EmpresaVO getEmpresaLogado() {
		if (empresaLogado == null) {
			empresaLogado = new EmpresaVO();
		}
		return empresaLogado;
	}

	public void setEmpresaLogado(EmpresaVO empresaLogado) {
		this.empresaLogado = empresaLogado;
	}

}
