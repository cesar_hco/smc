package negocio.comuns.administrativo;

import java.io.Serializable;

import negocio.comuns.arquitetura.SuperVO;
import negocio.comuns.basico.PessoaVO;

public class PacienteVO extends SuperVO implements Serializable{

	private Integer codigo;
	private String observacao;
	private String numeroConvenio;
	private ConvenioVO convenio;
	private PessoaVO pessoa;
	
	public PacienteVO() {
		super();
	}
	
	
	public Integer getCodigo() {
		if (codigo == null) {
			codigo = 0;
		}
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public String getObservacao() {
		if (observacao == null) {
			observacao = "";
		}
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
	public String getNumeroConvenio() {
		if (numeroConvenio == null) {
			numeroConvenio = "";
		}
		return numeroConvenio;
	}
	public void setNumeroConvenio(String numeroConvenio) {
		this.numeroConvenio = numeroConvenio;
	}
	public ConvenioVO getConvenio() {
		if (convenio == null) {
			convenio = new ConvenioVO();
		}
		return convenio;
	}
	public void setConvenio(ConvenioVO convenio) {
		this.convenio = convenio;
	}
	public PessoaVO getPessoa() {
		if (pessoa == null) {
			pessoa = new PessoaVO();
		}
		return pessoa;
	}
	public void setPessoa(PessoaVO pessoa) {
		this.pessoa = pessoa;
	}
	
	
	
	
}
