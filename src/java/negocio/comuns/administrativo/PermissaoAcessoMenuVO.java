package negocio.comuns.administrativo;

import java.io.Serializable;

import negocio.comuns.arquitetura.SuperVO;

public class PermissaoAcessoMenuVO implements Serializable{

	private Boolean menuAdministrativo = false;
	private Boolean discipulador = false;
	private Boolean membro = false;
	private Boolean pastor = false;
	private Boolean predio = false;
	private Boolean registroFrequenciaCelula = false;
	
	private Boolean menuBasico = false;
	private Boolean pais = false;
	private Boolean estado = false;
	private Boolean cidade = false;
	private Boolean configuracaoGeral = false;
	
	private Boolean menuUsuario = false;
	private Boolean perfilUsuario = false;
	private Boolean usuario = false;
	
	private Boolean menuRelatorio = false;
	private Boolean relatorioCelula = false;
	
	private Boolean menuEncontro = false;
	private Boolean encontroComDeus = false;
	
	private static final long serialVersionUID = 1L;

	public PermissaoAcessoMenuVO()  {
		super();
	}

	public Boolean getMenuAdministrativo() {
		return menuAdministrativo;
	}

	public Boolean getDiscipulador() {
		return discipulador;
	}

	public Boolean getMembro() {
		return membro;
	}

	public Boolean getPastor() {
		return pastor;
	}

	public Boolean getPredio() {
		return predio;
	}

	public Boolean getRegistroFrequenciaCelula() {
		return registroFrequenciaCelula;
	}

	public Boolean getMenuBasico() {
		return menuBasico;
	}

	public Boolean getPais() {
		return pais;
	}

	public Boolean getEstado() {
		return estado;
	}

	public Boolean getCidade() {
		return cidade;
	}

	public Boolean getMenuUsuario() {
		return menuUsuario;
	}

	public Boolean getPerfilUsuario() {
		return perfilUsuario;
	}

	public Boolean getUsuario() {
		return usuario;
	}

	public Boolean getMenuRelatorio() {
		return menuRelatorio;
	}

	public Boolean getRelatorioCelula() {
		return relatorioCelula;
	}

	public Boolean getMenuEncontro() {
		return menuEncontro;
	}

	public Boolean getEncontroComDeus() {
		return encontroComDeus;
	}

	public void setMenuAdministrativo(Boolean menuAdministrativo) {
		this.menuAdministrativo = menuAdministrativo;
	}

	public void setDiscipulador(Boolean discipulador) {
		this.discipulador = discipulador;
	}

	public void setMembro(Boolean membro) {
		this.membro = membro;
	}

	public void setPastor(Boolean pastor) {
		this.pastor = pastor;
	}

	public void setPredio(Boolean predio) {
		this.predio = predio;
	}

	public void setRegistroFrequenciaCelula(Boolean registroFrequenciaCelula) {
		this.registroFrequenciaCelula = registroFrequenciaCelula;
	}

	public void setMenuBasico(Boolean menuBasico) {
		this.menuBasico = menuBasico;
	}

	public void setPais(Boolean pais) {
		this.pais = pais;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public void setCidade(Boolean cidade) {
		this.cidade = cidade;
	}

	public void setMenuUsuario(Boolean menuUsuario) {
		this.menuUsuario = menuUsuario;
	}

	public void setPerfilUsuario(Boolean perfilUsuario) {
		this.perfilUsuario = perfilUsuario;
	}

	public void setUsuario(Boolean usuario) {
		this.usuario = usuario;
	}

	public void setMenuRelatorio(Boolean menuRelatorio) {
		this.menuRelatorio = menuRelatorio;
	}

	public void setRelatorioCelula(Boolean relatorioCelula) {
		this.relatorioCelula = relatorioCelula;
	}

	public void setMenuEncontro(Boolean menuEncontro) {
		this.menuEncontro = menuEncontro;
	}

	public void setEncontroComDeus(Boolean encontroComDeus) {
		this.encontroComDeus = encontroComDeus;
	}

	public Boolean getConfiguracaoGeral() {
		return configuracaoGeral;
	}

	public void setConfiguracaoGeral(Boolean configuracaoGeral) {
		this.configuracaoGeral = configuracaoGeral;
	}

	

}
