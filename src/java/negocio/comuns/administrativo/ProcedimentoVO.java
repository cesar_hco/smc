package negocio.comuns.administrativo;

import java.io.Serializable;
import java.util.List;

import negocio.comuns.arquitetura.SuperVO;

public class ProcedimentoVO extends SuperVO implements Serializable{
	
	private Integer codigo;
	private List<ConvenioVO> convenios;
	private Double valor;
	private Boolean ativo;
	
	
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public List<ConvenioVO> getConvenios() {
		return convenios;
	}
	public void setConvenios(List<ConvenioVO> convenios) {
		this.convenios = convenios;
	}
	public Double getValor() {
		return valor;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public Boolean getAtivo() {
		return ativo;
	}
	public void setAtivo(Boolean ativo) {
		this.ativo = ativo;
	}
	
	
	
	

}
