/**
 * 
 */
package negocio.comuns.administrativo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import negocio.comuns.arquitetura.SuperVO;

/**
 * @author Carlos Eug�nio
 *
 */
public class PermissaoUsuarioMenuVO extends SuperVO implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer codigo;
	private PerfilUsuarioVO perfilUsuarioVO;
	private String nomeModulo;
	private Boolean permissaoModulo;
	private List<PermissaoUsuarioMenuSubModuloVO> permissaoUsuarioMenuSubModuloVOs;
	private Boolean apresentarModulo;
	private String descricaoModulo;
	private String nomeMenu;

	public PermissaoUsuarioMenuVO() {
		super();
	}

	public Integer getCodigo() {
		if (codigo == null) {
			codigo = 0;
		}
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}


	public String getNomeModulo() {
		if (nomeModulo == null) {
			nomeModulo = "";
		}
		return nomeModulo;
	}

	public void setNomeModulo(String nomeModulo) {
		this.nomeModulo = nomeModulo;
	}

	public Boolean getPermissaoModulo() {
		if (permissaoModulo == null) {
			permissaoModulo = false;
		}
		return permissaoModulo;
	}

	public void setPermissaoModulo(Boolean permissaoModulo) {
		this.permissaoModulo = permissaoModulo;
	}

	public List<PermissaoUsuarioMenuSubModuloVO> getPermissaoUsuarioMenuSubModuloVOs() {
		if (permissaoUsuarioMenuSubModuloVOs == null) {
			permissaoUsuarioMenuSubModuloVOs = new ArrayList<PermissaoUsuarioMenuSubModuloVO>(0);
		}
		return permissaoUsuarioMenuSubModuloVOs;
	}

	public void setPermissaoUsuarioMenuSubModuloVOs(List<PermissaoUsuarioMenuSubModuloVO> permissaoUsuarioMenuSubModuloVOs) {
		this.permissaoUsuarioMenuSubModuloVOs = permissaoUsuarioMenuSubModuloVOs;
	}

	public Boolean getApresentarModulo() {
		if (apresentarModulo == null) {
			apresentarModulo = true;
		}
		return apresentarModulo;
	}

	public void setApresentarModulo(Boolean apresentarModulo) {
		this.apresentarModulo = apresentarModulo;
	}

	public PerfilUsuarioVO getPerfilUsuarioVO() {
		if (perfilUsuarioVO == null) {
			perfilUsuarioVO = new PerfilUsuarioVO();
		}
		return perfilUsuarioVO;
	}

	public void setPerfilUsuarioVO(PerfilUsuarioVO perfilUsuarioVO) {
		this.perfilUsuarioVO = perfilUsuarioVO;
	}

	public String getDescricaoModulo() {
		if (descricaoModulo == null) {
			descricaoModulo = "";
		}
		return descricaoModulo;
	}

	public void setDescricaoModulo(String descricaoModulo) {
		this.descricaoModulo = descricaoModulo;
	}

	public String getNomeMenu() {
		if (nomeMenu == null) {
			nomeMenu = "";
		}
		return nomeMenu;
	}

	public void setNomeMenu(String nomeMenu) {
		this.nomeMenu = nomeMenu;
	}
}
