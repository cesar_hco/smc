package negocio.interfaces.basico;

import java.util.List;

import negocio.comuns.administrativo.EmpresaVO;
import negocio.comuns.administrativo.FuncionarioVO;
import negocio.comuns.administrativo.PerfilUsuarioVO;
import negocio.comuns.administrativo.UsuarioEmpresaPerfilUsuarioVO;
import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;


public interface UsuarioInterfaceFacade {

	/**
	 * @author Carlos Eug�nio - 31/01/2017
	 * @param obj
	 * @param usuarioVO
	 * @throws Exception
	 */
	void persistir(UsuarioVO obj, UsuarioVO usuarioVO) throws Exception;

	/**
	 * @author Carlos Eug�nio - 31/01/2017
	 * @param obj
	 * @param usuario
	 * @throws Exception
	 */
	void excluir(UsuarioVO obj, UsuarioVO usuario) throws Exception;

	/**
	 * @author Carlos Eug�nio - 31/01/2017
	 * @param campoConsulta
	 * @param valorConsulta
	 * @param usuarioVO
	 * @return
	 */
	List<UsuarioVO> consultar(String campoConsulta, String valorConsulta, UsuarioVO usuarioVO);

	/**
	 * @author Carlos Eug�nio - 31/01/2017
	 * @param predio
	 * @param nivelMontarDadosEnum
	 * @param usuarioVO
	 * @return
	 */
	List<UsuarioVO> consultarPorEmpresa(Integer predio, NivelMontarDadosEnum nivelMontarDadosEnum, UsuarioVO usuarioVO);

	/**
	 * @author Carlos Eug�nio - 31/01/2017
	 * @param obj
	 * @param nivelMontarDados
	 * @param usuarioVO
	 */
	void carregarDados(UsuarioVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);

	/**
	 * @author Carlos Eug�nio - 31/01/2017
	 * @param listaUsuarioEmpresaPerfilUsuarioVOs
	 * @param empresaVO
	 * @param perfilUsuarioVO
	 * @param usuarioLogado
	 * @throws Exception
	 */
	void adicionarEmpresaPerfilUsuario(List<UsuarioEmpresaPerfilUsuarioVO> listaUsuarioEmpresaPerfilUsuarioVOs, EmpresaVO empresaVO, PerfilUsuarioVO perfilUsuarioVO, UsuarioVO usuarioLogado) throws Exception;

	/**
	 * @author Carlos Eug�nio - 31/01/2017
	 * @param usuarioVO
	 * @param funcionarioVO
	 */
	void inicializarDadosPessoaUsuario(UsuarioVO usuarioVO, FuncionarioVO funcionarioVO);

	/**
	 * @author Carlos Eug�nio - 31/01/2017
	 * @param listaUsuarioEmpresaPerfilUsuarioVOs
	 * @param obj
	 * @param usuarioVO
	 */
	void removerEmpresaPerfilUsuario(List<UsuarioEmpresaPerfilUsuarioVO> listaUsuarioEmpresaPerfilUsuarioVOs, UsuarioEmpresaPerfilUsuarioVO obj, UsuarioVO usuarioVO);

	
	
	void carregarDadosTeste(UsuarioVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);
}
