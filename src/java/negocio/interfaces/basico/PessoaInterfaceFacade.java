/**
 * 
 */
package negocio.interfaces.basico;

import java.util.List;

import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.comuns.basico.PessoaVO;

/**
 * @author Cesar Henrique
 *
 */
public interface PessoaInterfaceFacade {

	void persistir(PessoaVO obj, UsuarioVO usuarioVO) throws Exception;
	
	
	List<PessoaVO> consultar(String campoConsulta, String valorConsulta, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);
	
	
	List<PessoaVO> consultarFuncionarioUsuario(String campoConsulta, String valorConsulta, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);
	

	void excluir(PessoaVO obj, UsuarioVO usuario) throws Exception;

	void alterar(PessoaVO obj, UsuarioVO usuario) throws Exception;

	void incluirFuncionario(PessoaVO obj, UsuarioVO usuario) throws Exception;
	
	void incluirCliente(PessoaVO obj, UsuarioVO usuario) throws Exception;

}
