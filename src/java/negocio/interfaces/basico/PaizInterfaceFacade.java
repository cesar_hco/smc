package negocio.interfaces.basico;
import java.util.List;

import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.basico.PaizVO;

/**
 * Interface repons�vel por criar uma estrutura padr�o de comunida��o entre a camada de controle
 * e camada de neg�cio (em especial com a classe Fa�ade). Com a utiliza��o desta interface 
 * � poss�vel substituir tecnologias de uma camada da aplica��o com m�nimo de impacto nas demais.
 * Al�m de padronizar as funcionalidades que devem ser disponibilizadas pela camada de neg�cio, por interm�dio
 * de sua classe Fa�ade (respons�vel por persistir os dados das classes VO).
*/
public interface PaizInterfaceFacade {
	

//    public PaizVO novo() throws Exception;
    public void incluir(PaizVO obj,UsuarioVO usuario) throws Exception;
    public void alterar(PaizVO obj) throws Exception;
    public void excluir(PaizVO obj) throws Exception;
    public PaizVO consultarPorChavePrimaria(Integer codigo, boolean controlarAcesso,UsuarioVO usuario) throws Exception;
    public List<PaizVO> consultarPorCodigo(Integer valorConsulta, boolean controlarAcesso,UsuarioVO usuario) throws Exception;
    public List<PaizVO> consultarPorNome(String valorConsulta, boolean controlarAcesso,UsuarioVO usuario) throws Exception;
    public void setIdEntidade(String aIdEntidade);
}