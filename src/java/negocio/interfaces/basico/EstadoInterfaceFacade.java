package negocio.interfaces.basico;
import java.util.List;

import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.basico.EstadoVO;

/**
 * Interface repons�vel por criar uma estrutura padr�o de comunida��o entre a camada de controle
 * e camada de neg�cio (em especial com a classe Fa�ade). Com a utiliza��o desta interface 
 * � poss�vel substituir tecnologias de uma camada da aplica��o com m�nimo de impacto nas demais.
 * Al�m de padronizar as funcionalidades que devem ser disponibilizadas pela camada de neg�cio, por interm�dio
 * de sua classe Fa�ade (respons�vel por persistir os dados das classes VO).
*/
public interface EstadoInterfaceFacade {

    public EstadoVO novo() throws Exception;
    public void incluir(EstadoVO obj, UsuarioVO usuarioVO) throws Exception;
    public void alterar(EstadoVO obj, UsuarioVO usuarioVO) throws Exception;
    public void excluir(EstadoVO obj, UsuarioVO usuarioVO) throws Exception;
    public EstadoVO consultarPorChavePrimaria(Integer codigo, UsuarioVO usuario ) throws Exception;
    public List<EstadoVO> consultarPorCodigo(Integer valorConsulta, boolean controlarAcesso, int nivelMontarDados,UsuarioVO usuario ) throws Exception;
    public List<EstadoVO> consultarPorSigla(String valorConsulta, boolean controlarAcesso, int nivelMontarDados,UsuarioVO usuario ) throws Exception;
    public List<EstadoVO> consultarPorNome(String valorConsulta, boolean controlarAcesso, int nivelMontarDados,UsuarioVO usuario ) throws Exception;
    public List<EstadoVO> consultarPorNomePaiz(String valorConsulta, boolean controlarAcesso, int nivelMontarDados,UsuarioVO usuario ) throws Exception;
    public List<EstadoVO> consultarPorCodigoPaiz(Integer valorConsulta, boolean controlarAcesso, int nivelMontarDados,UsuarioVO usuario) throws Exception;
    public EstadoVO consultarPorCodigoCidade(Integer valorConsulta, boolean controlarAcesso, int nivelMontarDados,UsuarioVO usuario) throws Exception;
    public void setIdEntidade(String aIdEntidade);
}