package negocio.interfaces.basico;

import java.util.List;

import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.comuns.basico.FornecedorVO;

public interface FornecedorInterfaceFacade {

	void persistir(FornecedorVO obj, UsuarioVO usuarioVO) throws Exception;

	void excluir(FornecedorVO obj, UsuarioVO usuario) throws Exception;

	List<FornecedorVO> consultar(String campoConsulta, String valorConsulta, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);

	void carregarDados(FornecedorVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);

}
