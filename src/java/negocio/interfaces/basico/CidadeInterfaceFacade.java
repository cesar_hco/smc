package negocio.interfaces.basico;
import java.util.List;

import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.basico.CidadeVO;

/**
 * Interface repons�vel por criar uma estrutura padr�o de comunida��o entre a camada de controle
 * e camada de neg�cio (em especial com a classe Fa�ade). Com a utiliza��o desta interface 
 * � poss�vel substituir tecnologias de uma camada da aplica��o com m�nimo de impacto nas demais.
 * Al�m de padronizar as funcionalidades que devem ser disponibilizadas pela camada de neg�cio, por interm�dio
 * de sua classe Fa�ade (respons�vel por persistir os dados das classes VO).
*/
public interface CidadeInterfaceFacade {
	

    public CidadeVO novo() throws Exception;
    public void incluir(CidadeVO obj,  UsuarioVO usuarioVO) throws Exception;
    public void alterar(CidadeVO obj,  UsuarioVO usuarioVO) throws Exception;
    public void alterarCodigoIBGECidade(CidadeVO obj) throws Exception;
    public void excluir(CidadeVO obj,  UsuarioVO usuarioVO) throws Exception;
    public CidadeVO consultarPorChavePrimaria(Integer codigo, boolean controlarAcesso, UsuarioVO usuario) throws Exception;
    public List consultarPorCodigo(Integer valorConsulta, boolean controlarAcesso, UsuarioVO usuario) throws Exception;
    public List consultarPorNome(String valorConsulta,  boolean controlarAcesso, UsuarioVO usuario) throws Exception;
    public List consultarPorNomeCodigoEstado(String valorConsulta, boolean controlarAcesso, Integer estado, UsuarioVO usuario) throws Exception;
    public List consultarPorSiglaEstado(String valorConsulta, boolean controlarAcesso, int nivelMontarDados, UsuarioVO usuario ) throws Exception;
    public List consultarPorCodigoEstado(Integer valorConsulta, boolean controlarAcesso, UsuarioVO usuario) throws Exception;
    public List consultarPorCep(String valorConsulta, boolean controlarAcesso, int nivelMontarDados, UsuarioVO usuario ) throws Exception;
    public List consultarPorEstado(String valorConsulta, boolean controlarAcesso, UsuarioVO usuario) throws Exception;
    public List<CidadeVO> consultaRapidaPorNomeAutoComplete(String valorConsulta, int limit,  boolean controlarAcesso, int nivelMontarDados, UsuarioVO usuario) throws Exception;
    public void setIdEntidade(String aIdEntidade);
    public List<CidadeVO> consultaRapidaPorCodigo(Integer codCidade, boolean controlarAcesso, UsuarioVO usuario) throws Exception;
    public List<CidadeVO> consultaRapidaPorNome(String valorConsulta, boolean controlarAcesso, UsuarioVO usuario) throws Exception;
}