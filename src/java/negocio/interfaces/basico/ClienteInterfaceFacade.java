package negocio.interfaces.basico;

import java.util.List;

import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.comuns.basico.ClienteVO;

public interface ClienteInterfaceFacade {

	void persistir(ClienteVO obj, UsuarioVO usuarioVO) throws Exception;

	void excluir(ClienteVO obj, UsuarioVO usuario) throws Exception;

	void carregarDados(ClienteVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);

	List<ClienteVO> consultar(String campoConsulta, String valorConsulta, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);

}
