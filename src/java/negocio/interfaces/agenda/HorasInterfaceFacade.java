package negocio.interfaces.agenda;

import java.util.List;

import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.agenda.HorasVO;



public interface HorasInterfaceFacade {

	public HorasVO novo() throws Exception;
    public void incluir(HorasVO obj,  UsuarioVO usuarioVO) throws Exception;
    public void alterar(HorasVO obj,  UsuarioVO usuarioVO) throws Exception;
   
    public void excluir(HorasVO obj,  UsuarioVO usuarioVO) throws Exception;
    
    public List consultarPorCodigo(Integer valorConsulta, boolean controlarAcesso, UsuarioVO usuario) throws Exception;
    public List consultarPorNome(String valorConsulta,  boolean controlarAcesso, UsuarioVO usuario) throws Exception;
	
}
