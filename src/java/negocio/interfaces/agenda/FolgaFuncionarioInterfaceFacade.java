package negocio.interfaces.agenda;

import java.util.List;

import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.agenda.FolgaFuncionarioVO;

public interface FolgaFuncionarioInterfaceFacade {

	
    public void incluir(FolgaFuncionarioVO obj,  UsuarioVO usuarioVO) throws Exception;
    
    public void alterar(FolgaFuncionarioVO obj,  UsuarioVO usuarioVO) throws Exception;
   
    public void excluir(FolgaFuncionarioVO obj,  UsuarioVO usuarioVO) throws Exception;
    
    public List consultarPorCodigo(Integer valorConsulta, boolean controlarAcesso, UsuarioVO usuario) throws Exception;
    
    public List consultarPorNome(String valorConsulta,  boolean controlarAcesso, UsuarioVO usuario) throws Exception;
	
	
	
}
