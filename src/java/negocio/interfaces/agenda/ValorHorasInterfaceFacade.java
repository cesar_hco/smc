package negocio.interfaces.agenda;

import java.util.List;

import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.agenda.ValorHorasVO;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;

public interface ValorHorasInterfaceFacade {

	
 
	public void incluir(ValorHorasVO obj,  UsuarioVO usuarioVO) throws Exception;
    
	public void alterar(ValorHorasVO obj,  UsuarioVO usuarioVO) throws Exception;
   
    public void excluir(ValorHorasVO obj,  UsuarioVO usuarioVO) throws Exception;
    
    public List consultarPorCodigo(String campoConsulta, Integer valorConsulta, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) throws Exception;
    
   
	
	
}
