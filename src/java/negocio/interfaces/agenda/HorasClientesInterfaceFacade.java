package negocio.interfaces.agenda;

import java.util.List;

import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.agenda.HorasClientesVO;


public interface HorasClientesInterfaceFacade {

	public HorasClientesVO novo() throws Exception;
    public void incluir(HorasClientesVO obj,  UsuarioVO usuarioVO) throws Exception;
    public void alterar(HorasClientesVO obj,  UsuarioVO usuarioVO) throws Exception;
   
    public void excluir(HorasClientesVO obj,  UsuarioVO usuarioVO) throws Exception;
    
    public List consultarPorCodigo(Integer valorConsulta, boolean controlarAcesso, UsuarioVO usuario) throws Exception;
    public List consultarPorNome(String valorConsulta,  boolean controlarAcesso, UsuarioVO usuario) throws Exception;
	


}
