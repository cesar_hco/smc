/**
 * 
 */
package negocio.interfaces.administrativo;

import java.util.List;

import negocio.comuns.administrativo.PerfilUsuarioVO;
import negocio.comuns.administrativo.PermissaoAcessoMenuVO;
import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;

/**
 * @author Carlos Eug�nio
 *
 */
public interface PerfilUsuarioInterfaceFacade {

	/**
	 * @author Carlos Eug�nio - 26/10/2016
	 * @param obj
	 * @param usuarioVO
	 * @throws Exception
	 */
	void persistir(PerfilUsuarioVO obj, UsuarioVO usuarioVO) throws Exception;

	/**
	 * @author Carlos Eug�nio - 26/10/2016
	 * @param obj
	 * @param usuario
	 * @throws Exception
	 */
	void excluir(PerfilUsuarioVO obj, UsuarioVO usuario) throws Exception;

	/**
	 * @author Carlos Eug�nio - 26/10/2016
	 * @param campoConsulta
	 * @param valorConsulta
	 * @param nivelMontarDados
	 * @param usuarioVO
	 * @return
	 */
	List<PerfilUsuarioVO> consultar(String campoConsulta, String valorConsulta, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);

	/**
	 * @author Carlos Eug�nio - 26/10/2016
	 * @param obj
	 * @param nivelMontarDados
	 * @param usuarioVO
	 */
	void carregarDados(PerfilUsuarioVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);

	PermissaoAcessoMenuVO montarPermissoesMenu(PerfilUsuarioVO perfilUsuarioVO);

	PermissaoAcessoMenuVO montarPermissoesMenuCargoAdministrativo();
	
}
