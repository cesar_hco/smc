package negocio.interfaces.administrativo;

import java.util.List;

import negocio.comuns.administrativo.PacienteVO;
import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;

/**
 * @author Cesar Henrique - 09/11/2019
 * @param obj
 * @param usuario
 * @throws Exception
 */

public interface PacienteInterfaceFacade {

	void persistir(PacienteVO obj, UsuarioVO usuarioVO) throws Exception;

	void excluir(PacienteVO obj, UsuarioVO usuario) throws Exception;

	List<PacienteVO> consultar(String campoConsulta, String valorConsulta, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);

	void carregarDados(PacienteVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);


	

	
}
