/**
 * 
 */
package negocio.interfaces.administrativo;

import java.util.List;

import negocio.comuns.administrativo.EmpresaVO;
import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;

/**
 * @author Carlos Eug�nio
 *
 */
public interface EmpresaInterfaceFacade {

	/**
	 * @author Carlos Eug�nio - 26/10/2016
	 * @param obj
	 * @param usuario
	 * @throws Exception
	 */
	void excluir(EmpresaVO obj, UsuarioVO usuario) throws Exception;


	/**
	 * @author Carlos Eug�nio - 26/10/2016
	 * @param obj
	 * @param nivelMontarDados
	 * @param usuarioVO
	 */
	void carregarDados(EmpresaVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);

	/**
	 * @author Carlos Eug�nio - 26/10/2016
	 * @param obj
	 * @param usuarioVO
	 * @throws Exception
	 */
	void persistir(EmpresaVO obj, UsuarioVO usuarioVO) throws Exception;

	List<EmpresaVO> consultarPorDescricao(String nome, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);

	/**
	 * @author Carlos Eug�nio - 31/01/2017
	 * @param campoConsulta
	 * @param valorConsulta
	 * @param nivelMontarDados
	 * @param empresaLogado
	 * @param usuarioVO
	 * @return
	 */
	List<EmpresaVO> consultar(String campoConsulta, String valorConsulta, NivelMontarDadosEnum nivelMontarDados, Integer empresaLogado, UsuarioVO usuarioVO);
}
