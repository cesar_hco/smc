/**
 * 
 */
package negocio.interfaces.administrativo;

import java.util.List;

import negocio.comuns.administrativo.PerfilUsuarioVO;
import negocio.comuns.administrativo.PermissaoUsuarioMenuVO;
import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;

/**
 * @author Carlos Eug�nio
 *
 */
public interface PermissaoUsuarioMenuInterfaceFacade {

	/**
	 * @author Carlos Eug�nio - 26/10/2016
	 * @param obj
	 * @param usuarioVO
	 * @throws Exception
	 */
	void persistir(PermissaoUsuarioMenuVO obj, UsuarioVO usuarioVO) throws Exception;

	/**
	 * @author Carlos Eug�nio - 26/10/2016
	 * @param obj
	 * @param usuario
	 * @throws Exception
	 */
	void excluir(PermissaoUsuarioMenuVO obj, UsuarioVO usuario) throws Exception;


	/**
	 * @author Carlos Eug�nio - 26/10/2016
	 * @param obj
	 * @param nivelMontarDados
	 * @param usuarioVO
	 */
	void carregarDados(PermissaoUsuarioMenuVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);

	void incluirPermissaoUsuarioMenuPorPerfilUsuario(List<PermissaoUsuarioMenuVO> listaPermissaoUsuarioMenuVOs, PerfilUsuarioVO perfilUsuarioVO, UsuarioVO usuarioVO) throws Exception;

	void alterarPermissaoUsuarioMenuPorPerfilUsuario(List<PermissaoUsuarioMenuVO> listaPermissaoUsuarioMenuVOs, PerfilUsuarioVO perfilUsuarioVO, UsuarioVO usuarioVO) throws Exception;

	List<PermissaoUsuarioMenuVO> consultarPorPerfilUsuario(Integer perfilUsuario, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);
	
}
