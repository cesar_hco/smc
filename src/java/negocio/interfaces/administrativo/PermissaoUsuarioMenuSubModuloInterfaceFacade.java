/**
 * 
 */
package negocio.interfaces.administrativo;

import java.util.List;

import negocio.comuns.administrativo.PermissaoUsuarioMenuSubModuloVO;
import negocio.comuns.administrativo.PermissaoUsuarioMenuVO;
import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;

/**
 * @author Carlos Eug�nio
 *
 */
public interface PermissaoUsuarioMenuSubModuloInterfaceFacade {

	/**
	 * @author Carlos Eug�nio - 26/10/2016
	 * @param obj
	 * @param usuarioVO
	 * @throws Exception
	 */
	void persistir(PermissaoUsuarioMenuSubModuloVO obj, UsuarioVO usuarioVO) throws Exception;

	/**
	 * @author Carlos Eug�nio - 26/10/2016
	 * @param obj
	 * @param usuario
	 * @throws Exception
	 */
	void excluir(PermissaoUsuarioMenuSubModuloVO obj, UsuarioVO usuario) throws Exception;


	/**
	 * @author Carlos Eug�nio - 26/10/2016
	 * @param obj
	 * @param nivelMontarDados
	 * @param usuarioVO
	 */
	void carregarDados(PermissaoUsuarioMenuSubModuloVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);

	void incluirPermissaoUsuarioMenuPorPerfilUsuario(List<PermissaoUsuarioMenuSubModuloVO> listaPermissaoUsuarioMenuSubModuloVOs, PermissaoUsuarioMenuVO permissaoUsuarioMenuVO, UsuarioVO usuarioVO) throws Exception;

	void alterarPermissaoUsuarioMenuPorPerfilUsuario(List<PermissaoUsuarioMenuSubModuloVO> listaPermissaoUsuarioMenuSubModuloVOs, PermissaoUsuarioMenuVO permissaoUsuarioMenuVO, UsuarioVO usuarioVO) throws Exception;

	List<PermissaoUsuarioMenuSubModuloVO> consultarPorPermissaoUsuarioMenu(Integer permissaoUsuarioMenu, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);
	
}
