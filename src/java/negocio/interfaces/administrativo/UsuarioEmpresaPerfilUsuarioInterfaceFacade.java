package negocio.interfaces.administrativo;

import java.util.List;

import negocio.comuns.administrativo.UsuarioEmpresaPerfilUsuarioVO;
import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;

public interface UsuarioEmpresaPerfilUsuarioInterfaceFacade {

	void persistir(UsuarioEmpresaPerfilUsuarioVO obj, UsuarioVO usuarioVO) throws Exception;


	void excluir(UsuarioEmpresaPerfilUsuarioVO obj, UsuarioVO usuario) throws Exception;

	/**
	 * @author Carlos Eug�nio - 31/10/2016
	 * @param empresa
	 * @param nivelMontarDadosEnum
	 * @param usuarioVO
	 * @return
	 */

	void carregarDados(UsuarioEmpresaPerfilUsuarioVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);


	void incluirUsuarioEmpresaPerfilUsuarioVOs(List<UsuarioEmpresaPerfilUsuarioVO> listaUsuarioEmpresaPerfilUsuarioVOs, UsuarioVO usuarioVO, UsuarioVO usuarioLogado) throws Exception;


	void alterarUsuarioEmpresaPerfilUsuarioVOs(List<UsuarioEmpresaPerfilUsuarioVO> listaUsuarioEmpresaPerfilUsuarioVOs, UsuarioVO usuarioVO, UsuarioVO usuarioLogado) throws Exception;


	List<UsuarioEmpresaPerfilUsuarioVO> consultarPorUsuario(Integer usuario, NivelMontarDadosEnum nivelMontarDadosEnum, UsuarioVO usuarioVO);

}
