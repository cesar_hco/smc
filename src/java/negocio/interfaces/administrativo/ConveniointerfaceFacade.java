package negocio.interfaces.administrativo;

import java.util.List;

import negocio.comuns.administrativo.ConvenioVO;
import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;

public interface ConveniointerfaceFacade {
	
	
	List<ConvenioVO> consultar(String campoConsulta, String valorConsulta, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);

	void carregarDados(ConvenioVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);

}
