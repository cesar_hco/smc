package negocio.interfaces.administrativo;

import java.util.List;

import negocio.comuns.administrativo.FuncionarioVO;
import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.comuns.financeiro.ContaCorrenteVO;

public interface FuncionarioInterfaceFacade {

	/**
	 * @author Carlos Eug�nio - 31/01/2017
	 * @param obj
	 * @param usuarioVO
	 * @throws Exception
	 */
	void persistir(FuncionarioVO obj, UsuarioVO usuarioVO) throws Exception;

	/**
	 * @author Carlos Eug�nio - 31/01/2017
	 * @param obj
	 * @param usuario
	 * @throws Exception
	 */
	void excluir(FuncionarioVO obj, UsuarioVO usuario) throws Exception;

	/**
	 * @author Carlos Eug�nio - 31/01/2017
	 * @param campoConsulta
	 * @param valorConsulta
	 * @param nivelMontarDados
	 * @param usuarioVO
	 * @return
	 */
	List<FuncionarioVO> consultar(String campoConsulta, String valorConsulta, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);


	/**
	 * @author Carlos Eug�nio - 31/01/2017
	 * @param obj
	 * @param nivelMontarDados
	 * @param usuarioVO
	 */
	void carregarDados(FuncionarioVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);

	/**
	 * @author Carlos Eug�nio - 31/01/2017
	 * @param usuario
	 * @param nivelMontarDados
	 * @param usuarioVO
	 * @return
	 */
	FuncionarioVO consultarFuncionarioPorCodigoUsuario(Integer usuario, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);

	

}
