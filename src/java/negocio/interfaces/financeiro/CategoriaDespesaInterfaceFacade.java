package negocio.interfaces.financeiro;

import java.util.List;

import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.comuns.financeiro.CategoriaDespesaVO;

public interface CategoriaDespesaInterfaceFacade {

	void persistir(CategoriaDespesaVO obj, UsuarioVO usuarioVO) throws Exception;

	void excluir(CategoriaDespesaVO obj, UsuarioVO usuario) throws Exception;

	List<CategoriaDespesaVO> consultar(String campoConsulta, String valorConsulta, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);

	void carregarDados(CategoriaDespesaVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);


	List<CategoriaDespesaVO> consultarCategoriaPrincipalPorDescricao(String descricao, Integer codigoCategoriaDespesa, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);

	List<CategoriaDespesaVO> consultarPorDescricao(String nome, Boolean trazerCategoriaPrincial, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);


}