package negocio.interfaces.financeiro;

import java.math.BigDecimal;
import java.util.List;

import controle.financeiro.MovimentacaoCaixaItemVO;
import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.comuns.financeiro.ChequeVO;
import negocio.comuns.financeiro.ContaCorrenteVO;
import negocio.comuns.financeiro.FormaPagamentoVO;
import negocio.comuns.financeiro.enumeradores.TipoMovimentacaoCaixaEnum;
import negocio.comuns.financeiro.enumeradores.TipoOrigemMovimentacaoCaixaEnum;
import negocio.comuns.financeiro.enumeradores.TipoSacadoEnum;

public interface MovimentacaoCaixaItemInterfaceFacade {

	void persistir(MovimentacaoCaixaItemVO obj, UsuarioVO usuarioVO) throws Exception;

	void excluir(MovimentacaoCaixaItemVO obj, UsuarioVO usuario) throws Exception;

	List<MovimentacaoCaixaItemVO> consultar(String campoConsulta, String valorConsulta, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);

	void carregarDados(MovimentacaoCaixaItemVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);

	void criarMovimentacaoCaixaItem(TipoMovimentacaoCaixaEnum tipoMovimentacaoCaixaEnum, TipoSacadoEnum tipoSacadoEnum, Integer codigoSacado, ContaCorrenteVO caixaVO, FormaPagamentoVO formaPagamentoVO, ChequeVO chequeVO, BigDecimal valor, TipoOrigemMovimentacaoCaixaEnum tipoOrigemMovimentacaoCaixaEnum, ContaCorrenteVO contaCorrenteVO, Integer codigoOrigem, UsuarioVO usuarioVO) throws Exception;

	/**
	 * @author Carlos Eug�nio - 02/02/2017
	 * @param movimentacaoCaixa
	 * @param listaMovimentacaoCaixaItemVOs
	 * @param usuario
	 * @throws Exception
	 */
	void incluirMovimentacaoCaixaItemVOs(Integer movimentacaoCaixa, List<MovimentacaoCaixaItemVO> listaMovimentacaoCaixaItemVOs, UsuarioVO usuario) throws Exception;


}
