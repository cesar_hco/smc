package negocio.interfaces.financeiro;

import java.math.BigDecimal;
import java.util.List;

import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.comuns.financeiro.ContaCorrenteVO;
import negocio.comuns.financeiro.enumeradores.TipoMovimentacaoCaixaEnum;

public interface ContaCorrenteInterfaceFacade {

	void carregarDados(ContaCorrenteVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);

	List<ContaCorrenteVO> consultar(String campoConsulta, String valorConsulta, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);

	void excluir(ContaCorrenteVO obj, UsuarioVO usuario) throws Exception;

	void persistir(ContaCorrenteVO obj, UsuarioVO usuarioVO) throws Exception;

	List<ContaCorrenteVO> consultarContaCorrente(NivelMontarDadosEnum nivelMontarDados, Boolean contaCaixa, UsuarioVO usuarioVO);

	/**
	 * @author Carlos Eug�nio - 02/02/2017
	 * @param contaCorrente
	 * @param nivelMontarDados
	 * @param usuarioVO
	 * @return
	 */
	ContaCorrenteVO consultarPorChavePrimaria(Integer contaCorrente, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);

	/**
	 * @author Carlos Eug�nio - 03/02/2017
	 * @param contaCorrente
	 * @param saldo
	 * @param usuario
	 * @throws Exception
	 */
	void alterarSaldoContaCorrente(Integer contaCorrente, BigDecimal saldo, UsuarioVO usuario) throws Exception;

	/**
	 * @author Carlos Eug�nio - 03/02/2017
	 * @param contaCorrente
	 * @param tipoMovimentacao
	 * @param valor
	 * @param usuario
	 * @throws Exception
	 */
	void movimentarSaldoContaCorrente(Integer contaCorrente, TipoMovimentacaoCaixaEnum tipoMovimentacao, BigDecimal valor, UsuarioVO usuario) throws Exception;

}
