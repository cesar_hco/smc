/**
 * 
 */
package negocio.interfaces.financeiro;

import java.util.Date;
import java.util.List;

import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.comuns.financeiro.ContaPagarPagamentoVO;
import negocio.comuns.financeiro.ContaPagarVO;
import negocio.comuns.financeiro.FormaPagamentoPagamentoVO;
import negocio.comuns.financeiro.FormaPagamentoVO;
import negocio.comuns.financeiro.PagamentoVO;

/**
 * @author Carlos Eug�nio
 *
 */
public interface PagamentoInterfaceFacade {

	/**
	 * @author Carlos Eug�nio - 17/01/2017
	 * @param obj
	 * @param usuarioVO
	 * @throws Exception
	 */
	void persistir(PagamentoVO obj, UsuarioVO usuarioVO) throws Exception;

	/**
	 * @author Carlos Eug�nio - 17/01/2017
	 * @param obj
	 * @param usuario
	 * @throws Exception
	 */
	void excluir(PagamentoVO obj, UsuarioVO usuario) throws Exception;


	/**
	 * @author Carlos Eug�nio - 17/01/2017
	 * @param obj
	 * @param nivelMontarDados
	 * @param usuarioVO
	 */
	void carregarDados(PagamentoVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);

	/**
	 * @author Carlos Eug�nio - 18/01/2017
	 * @param listaContaContaPagarPagamentoVOs
	 * @param contaPagarVO
	 * @param usuarioVO
	 */
	void adicionarContaPagarPagamento(List<ContaPagarPagamentoVO> listaContaContaPagarPagamentoVOs, ContaPagarVO contaPagarVO, UsuarioVO usuarioVO);

	/**
	 * @author Carlos Eug�nio - 18/01/2017
	 * @param listaContaContaPagarPagamentoVOs
	 * @param obj
	 */
	void removerContaPagarPagamento(List<ContaPagarPagamentoVO> listaContaContaPagarPagamentoVOs, ContaPagarPagamentoVO obj);

	/**
	 * @author Carlos Eug�nio - 19/01/2017
	 * @param listaFormaPagamentoPagamentoVOs
	 * @param formaPagamentoVO
	 * @param usuarioVO
	 */
	void adicionarFormaPagamento(PagamentoVO pagamentoVO, List<FormaPagamentoPagamentoVO> listaFormaPagamentoPagamentoVOs, FormaPagamentoPagamentoVO formaPagamentoPagamentoIncluirVO, UsuarioVO usuarioVO) throws Exception;

	/**
	 * @author Carlos Eug�nio - 19/01/2017
	 * @param listaFormaPagamentoPagamentoVOs
	 * @param obj
	 */
	void removerFormaPagamento(List<FormaPagamentoPagamentoVO> listaFormaPagamentoPagamentoVOs, FormaPagamentoPagamentoVO obj);

	/**
	 * @author Carlos Eug�nio - 23/01/2017
	 * @param pagamentoVO
	 */
	void calcularTotalPagamento(PagamentoVO pagamentoVO);

	/**
	 * @author Carlos Eug�nio - 26/01/2017
	 * @param campoConsulta
	 * @param valorConsulta
	 * @param dataInicio
	 * @param dataFim
	 * @param nivelMontarDados
	 * @param usuarioVO
	 * @return
	 */
	List<PagamentoVO> consultar(String campoConsulta, String valorConsulta, Date dataInicio, Date dataFim, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);

	/**
	 * @author Carlos Eug�nio - 27/01/2017
	 * @param pagamentoVO
	 * @param usuarioVO
	 * @throws Exception
	 */
	void estornarPagamento(PagamentoVO pagamentoVO, UsuarioVO usuarioVO) throws Exception;
}
