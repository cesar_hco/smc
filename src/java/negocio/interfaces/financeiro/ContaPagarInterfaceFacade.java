package negocio.interfaces.financeiro;

import java.util.Date;
import java.util.List;

import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.comuns.financeiro.ContaPagarVO;

public interface ContaPagarInterfaceFacade {

	void persistir(ContaPagarVO obj, UsuarioVO usuarioVO) throws Exception;

	void excluir(ContaPagarVO obj, UsuarioVO usuario) throws Exception;

	List<ContaPagarVO> consultar(String campoConsulta, String valorConsulta, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);

	void carregarDados(ContaPagarVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);

	void realizarCalculoValorMulta(ContaPagarVO obj, UsuarioVO usuarioVO);

	void realizarCalculoValorJuro(ContaPagarVO obj, UsuarioVO usuarioVO);

	/**
	 * @author Carlos Eug�nio - 18/01/2017
	 * @param situacao
	 * @param dataInicio
	 * @param dataFinal
	 * @param nivelMontarDadosEnum
	 * @param usuarioVO
	 * @return
	 */
	List<ContaPagarVO> consultarContaAPagarPorFornecedorSituacaoDataVencimento(Integer fornecedor, String situacao, Date dataInicio, Date dataFinal, NivelMontarDadosEnum nivelMontarDadosEnum, UsuarioVO usuarioVO);

	void alterar(ContaPagarVO obj, UsuarioVO usuario) throws Exception;

	/**
	 * @author Carlos Eug�nio - 01/02/2017
	 * @param contaPagarVO
	 * @param usuarioVO
	 */
	void calcularValorPagar(ContaPagarVO contaPagarVO, UsuarioVO usuarioVO);


	

}
