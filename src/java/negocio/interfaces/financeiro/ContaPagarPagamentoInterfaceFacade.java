package negocio.interfaces.financeiro;

import java.util.List;

import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.comuns.financeiro.ContaCorrenteVO;
import negocio.comuns.financeiro.ContaPagarPagamentoVO;
import negocio.comuns.financeiro.ContaPagarVO;
import negocio.comuns.financeiro.PagamentoVO;

public interface ContaPagarPagamentoInterfaceFacade {

	/**
	 * @author Carlos Eug�nio - 18/01/2017
	 * @param obj
	 * @param usuarioVO
	 * @throws Exception
	 */
	void persistir(ContaPagarPagamentoVO obj, UsuarioVO usuarioVO) throws Exception;

	/**
	 * @author Carlos Eug�nio - 18/01/2017
	 * @param obj
	 * @param usuario
	 * @throws Exception
	 */
	void excluir(ContaPagarPagamentoVO obj, UsuarioVO usuario) throws Exception;

	/**
	 * @author Carlos Eug�nio - 18/01/2017
	 * @param campoConsulta
	 * @param valorConsulta
	 * @param nivelMontarDados
	 * @param usuarioVO
	 * @return
	 */
	List<ContaPagarPagamentoVO> consultar(String campoConsulta, String valorConsulta, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);

	/**
	 * @author Carlos Eug�nio - 18/01/2017
	 * @param obj
	 * @param nivelMontarDados
	 * @param usuarioVO
	 */
	void carregarDados(ContaPagarPagamentoVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);

	/**
	 * @author Carlos Eug�nio - 24/01/2017
	 * @param pagamentoVO
	 * @param listaContaPagarPagamentoVOs
	 * @param usuarioVO
	 * @throws Exception
	 */
	void incluirContaPagarPagamentoVOs(PagamentoVO pagamentoVO, List<ContaPagarPagamentoVO> listaContaPagarPagamentoVOs, UsuarioVO usuarioVO) throws Exception;

	/**
	 * @author Carlos Eug�nio - 24/01/2017
	 * @param pagamentoVO
	 * @param listaContaPagarPagamentoVOs
	 * @param usuarioVO
	 * @throws Exception
	 */
	void alterarContaPagarPagamentoVOs(PagamentoVO pagamentoVO, List<ContaPagarPagamentoVO> listaContaPagarPagamentoVOs, UsuarioVO usuarioVO) throws Exception;

	/**
	 * @author Carlos Eug�nio - 26/01/2017
	 * @param pagamento
	 * @param nivelMontarDados
	 * @param usuarioVO
	 * @return
	 */
	List<ContaPagarPagamentoVO> consultarPorPagamento(Integer pagamento, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);

	/**
	 * @author Carlos Eug�nio - 27/01/2017
	 * @param pagamentoVO
	 * @param usuarioVO
	 * @throws Exception
	 */
	void executarEstornoPagamentoContaPagar(PagamentoVO pagamentoVO, UsuarioVO usuarioVO) throws Exception;

	


}
