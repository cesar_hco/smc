package negocio.interfaces.financeiro;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import negocio.comuns.administrativo.EmpresaVO;
import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.comuns.financeiro.ChequeVO;
import negocio.comuns.financeiro.ContaCorrenteVO;
import negocio.comuns.financeiro.ExtratoContaCorrenteVO;
import negocio.comuns.financeiro.FormaPagamentoVO;
import negocio.comuns.financeiro.enumeradores.CedenteEnum;
import negocio.comuns.financeiro.enumeradores.OrigemExtratoContaCorrenteEnum;
import negocio.comuns.financeiro.enumeradores.TipoMovimentacaoExtratoContaCorrenteEnum;

public interface ExtratoContaCorrenteInterfaceFacade {

	void persistir(ExtratoContaCorrenteVO obj, UsuarioVO usuarioVO) throws Exception;

	void excluir(ExtratoContaCorrenteVO obj, UsuarioVO usuario) throws Exception;

	List<ExtratoContaCorrenteVO> consultar(String campoConsulta, String valorConsulta, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);

	void carregarDados(ExtratoContaCorrenteVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);

	/**
	 * @author Carlos Eug�nio - 23/01/2017
	 * @param data
	 * @param valor
	 * @param empresaVO
	 * @param contaCorrenteVO
	 * @param formaPagamentoVO
	 * @param codigoOrigem
	 * @param origemExtratoContaCorrente
	 * @param tipoMovimentacaoExtratoContaCorrente
	 * @param chequeVO
	 * @param nomeSacado
	 * @param codigoSacado
	 * @param cedenteEnum
	 * @param usuarioVO
	 * @throws Exception
	 */
	void executarCriacaoExtratoContaCorrente(Date data, BigDecimal valor, EmpresaVO empresaVO, ContaCorrenteVO contaCorrenteVO, FormaPagamentoVO formaPagamentoVO, Integer codigoOrigem, OrigemExtratoContaCorrenteEnum origemExtratoContaCorrente, TipoMovimentacaoExtratoContaCorrenteEnum tipoMovimentacaoExtratoContaCorrente, ChequeVO chequeVO, String nomeSacado, Integer codigoSacado, CedenteEnum cedenteEnum, UsuarioVO usuarioVO) throws Exception;

	

}
