package negocio.interfaces.financeiro;

import java.util.List;

import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.comuns.financeiro.FormaPagamentoVO;
import negocio.comuns.financeiro.enumeradores.TipoFormaPagamentoEnum;

public interface FormaPagamentoInterfaceFacade {

	/**
	 * @author Carlos Eug�nio - 19/01/2017
	 * @param obj
	 * @param usuarioVO
	 * @throws Exception
	 */
	void persistir(FormaPagamentoVO obj, UsuarioVO usuarioVO) throws Exception;

	/**
	 * @author Carlos Eug�nio - 19/01/2017
	 * @param obj
	 * @param usuario
	 * @throws Exception
	 */
	void excluir(FormaPagamentoVO obj, UsuarioVO usuario) throws Exception;

	/**
	 * @author Carlos Eug�nio - 19/01/2017
	 * @param campoConsulta
	 * @param valorConsulta
	 * @param nivelMontarDados
	 * @param usuarioVO
	 * @return
	 */
	List<FormaPagamentoVO> consultar(String campoConsulta, String valorConsulta, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);

	/**
	 * @author Carlos Eug�nio - 19/01/2017
	 * @param nivelMontarDados
	 * @param usuarioVO
	 * @return
	 */
	List<FormaPagamentoVO> consultarFormaPagamento(NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);

	/**
	 * @author Carlos Eug�nio - 19/01/2017
	 * @param obj
	 * @param nivelMontarDados
	 * @param usuarioVO
	 */
	void carregarDados(FormaPagamentoVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);

	/**
	 * @author Carlos Eug�nio - 23/01/2017
	 * @param tipoFormaPagamentoEnum
	 * @param usuarioVO
	 * @return
	 */
	FormaPagamentoVO consultarPorTipoPagamentoUnico(TipoFormaPagamentoEnum tipoFormaPagamentoEnum, UsuarioVO usuarioVO);


}
