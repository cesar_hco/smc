package negocio.interfaces.financeiro;

import java.util.List;

import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.comuns.financeiro.ContaReceberVO;

public interface ContaReceberInterfaceFacade {

	void persistir(ContaReceberVO obj, UsuarioVO usuarioVO) throws Exception;

	void alterar(ContaReceberVO obj, UsuarioVO usuario) throws Exception;

	void excluir(ContaReceberVO obj, UsuarioVO usuario) throws Exception;

	List<ContaReceberVO> consultar(String campoConsulta, String valorConsulta, UsuarioVO usuarioVO);

	void carregarDados(ContaReceberVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);

	void realizarCalculoValorMulta(ContaReceberVO obj, UsuarioVO usuarioVO);

	void realizarCalculoValorJuro(ContaReceberVO obj, UsuarioVO usuarioVO);

}
