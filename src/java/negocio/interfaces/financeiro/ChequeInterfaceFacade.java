package negocio.interfaces.financeiro;

import java.util.Date;
import java.util.List;

import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.comuns.financeiro.ChequeVO;
import negocio.comuns.financeiro.ContaCorrenteVO;
import negocio.comuns.financeiro.FormaPagamentoPagamentoVO;
import negocio.comuns.financeiro.PagamentoVO;

public interface ChequeInterfaceFacade {

	void carregarDados(ChequeVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);

	List<ChequeVO> consultar(String campoConsulta, String valorConsulta, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);

	void excluir(ChequeVO obj, UsuarioVO usuario) throws Exception;

	void persistir(ChequeVO obj, UsuarioVO usuarioVO) throws Exception;

	List<ChequeVO> consultarCheque(NivelMontarDadosEnum nivelMontarDados, Boolean contaCaixa, UsuarioVO usuarioVO);

	List<ChequeVO> consultarPorNumeroDataVencimentoCheque(String numero, Date dataInicio, Date dataFim, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);

	void validarDados(ChequeVO obj, UsuarioVO usuarioVO) throws Exception;

	void inicializarDadosChequeProprio(PagamentoVO pagamentoVO, FormaPagamentoPagamentoVO formaPagamentoPagamentoVO, UsuarioVO usuarioVO) throws Exception;

}
