package negocio.interfaces.financeiro;

import java.util.List;

import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.comuns.financeiro.ChequeVO;
import negocio.comuns.financeiro.FormaPagamentoPagamentoVO;
import negocio.comuns.financeiro.PagamentoVO;

public interface FormaPagamentoPagamentoInterfaceFacade {

	void persistir(FormaPagamentoPagamentoVO obj, UsuarioVO usuarioVO) throws Exception;

	void excluir(FormaPagamentoPagamentoVO obj, UsuarioVO usuario) throws Exception;

	List<FormaPagamentoPagamentoVO> consultar(String campoConsulta, String valorConsulta, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);

	void carregarDados(FormaPagamentoPagamentoVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);

	void validarDados(FormaPagamentoPagamentoVO obj, UsuarioVO usuarioVO) throws Exception;

	/**
	 * @author Carlos Eug�nio - 24/01/2017
	 * @param pagamentoVO
	 * @param listaFormaPagamentoPagamentoVOs
	 * @param usuarioVO
	 * @throws Exception
	 */
	void incluirFormaPagamentoPagamentoVOs(PagamentoVO pagamentoVO, List<FormaPagamentoPagamentoVO> listaFormaPagamentoPagamentoVOs, UsuarioVO usuarioVO) throws Exception;

	/**
	 * @author Carlos Eug�nio - 24/01/2017
	 * @param pagamentoVO
	 * @param listaFormaPagamentoPagamentoVOs
	 * @param usuarioVO
	 * @throws Exception
	 */
	void alterarFormaPagamentoPagamentoVOs(PagamentoVO pagamentoVO, List<FormaPagamentoPagamentoVO> listaFormaPagamentoPagamentoVOs, UsuarioVO usuarioVO) throws Exception;

	/**
	 * @author Carlos Eug�nio - 26/01/2017
	 * @param pagamento
	 * @param nivelMontarDados
	 * @param usuarioVO
	 * @return
	 */
	List<FormaPagamentoPagamentoVO> consultarPorPagamento(Integer pagamento, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);

	/**
	 * @author Carlos Eug�nio - 27/01/2017
	 * @param pagamentoVO
	 * @param usuarioVO
	 * @throws Exception
	 */
	void estornarPagamentoFormaPagamento(PagamentoVO pagamentoVO, UsuarioVO usuarioVO) throws Exception;

	void inicalzarDadosChequeFormaPagamentoPagamento(FormaPagamentoPagamentoVO formaPagamentoPagamentoVO, ChequeVO chequeVO, UsuarioVO usuarioVO);


}
