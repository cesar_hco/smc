package negocio.interfaces.financeiro;

import java.util.Date;
import java.util.List;

import controle.financeiro.MovimentacaoCaixaVO;
import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.comuns.financeiro.enumeradores.SituacaoCaixaEnum;

public interface MovimentacaoCaixaInterfaceFacade {

	void persistir(MovimentacaoCaixaVO obj, UsuarioVO usuarioVO) throws Exception;

	void excluir(MovimentacaoCaixaVO obj, UsuarioVO usuario) throws Exception;

	void carregarDados(MovimentacaoCaixaVO obj, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO) throws Exception;

	void consultarSaldoMovimentacaoCaixa(MovimentacaoCaixaVO movimentacaoCaixaVO) throws Exception;

	MovimentacaoCaixaVO consultarMovimentacaoCaixaPorContaCorrenteSituacao(Integer contaCorrente, SituacaoCaixaEnum situacaoCaixaEnum, UsuarioVO usuarioVO) throws Exception;

	/**
	 * @author Carlos Eug�nio - 26/01/2017
	 * @param obj
	 * @param usuarioVO
	 * @throws Exception
	 */
	void realizarAberturaCaixa(MovimentacaoCaixaVO obj, UsuarioVO usuarioVO) throws Exception;

	/**
	 * @author Carlos Eug�nio - 26/01/2017
	 * @param obj
	 * @param usuarioVO
	 * @throws Exception
	 */
	void realizarFechamentoCaixa(MovimentacaoCaixaVO obj, UsuarioVO usuarioVO) throws Exception;

	List<MovimentacaoCaixaVO> consultar(String campoConsulta, String valorConsulta, Date dataInicio, Date dataFim, NivelMontarDadosEnum nivelMontarDados, UsuarioVO usuarioVO);

	/**
	 * @author Carlos Eug�nio - 03/02/2017
	 * @param contaCaixa
	 * @param nivelMontarDadosEnum
	 * @param usuarioVO
	 * @return
	 * @throws Exception
	 */
	MovimentacaoCaixaVO consultarUltimoCaixaPorContaCaixa(Integer contaCaixa, NivelMontarDadosEnum nivelMontarDadosEnum, UsuarioVO usuarioVO) throws Exception;

	

}
