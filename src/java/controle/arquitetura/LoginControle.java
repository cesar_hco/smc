/**
 * 
 */
package controle.arquitetura;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;

import negocio.comuns.administrativo.EmpresaVO;
import negocio.comuns.administrativo.PerfilUsuarioVO;
import negocio.comuns.administrativo.PermissaoAcessoMenuVO;
import negocio.comuns.administrativo.UsuarioEmpresaPerfilUsuarioVO;
import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.administrativo.enumeradores.TipoUsuarioEnum;
import negocio.comuns.arquitetura.enumeradores.MensagemAlertaEnum;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.comuns.utilitarias.Uteis;
import negocio.facade.jdbc.arquitetura.ControleAcesso;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

/**
 * @author Carlos Eug�nio
 *
 */
@Controller("loginControle")
@Scope("viewScope")
@Lazy
public class LoginControle extends SuperControle implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private UsuarioVO usuarioVO;
	private String userName;
	private String senha;
	protected PermissaoAcessoMenuVO permissaoAcessoMenuVO;
	private List<SelectItem> listaSelectItemEmpresaVOs;
	private Boolean deveAbrirModalEmpresa;
	

	public LoginControle() {
		super();	
	}
	
	public String logar() {
		UsuarioVO usuarioVO = null;
		try {
			usuarioVO = ControleAcesso.verificarLoginUsuario(getUserName(), getSenha(), true, Uteis.NIVELMONTARDADOS_DADOSLOGIN);
			setUsuarioVO(usuarioVO);
			getFacadeFactory().getUsuarioFacade().carregarDados(getUsuarioVO(), NivelMontarDadosEnum.COMPLETO, usuarioVO);
			inicializarDadosUsuario(getUsuarioVO());
			context().getExternalContext().getSessionMap().put("usuarioLogado", getUsuarioVO());
			if (getUsuarioVO().getExerceCargoAdministrativo()) {
				setPermissaoAcessoMenuVO(getFacadeFactory().getPerfilUsuarioFacade().montarPermissoesMenuCargoAdministrativo());
			} else {
				if (getUsuarioVO().getUsuarioEmpresaPerfilUsuarioVOs().isEmpty()) {
					throw new Exception("Usu�rio Inativado.");
				}
				if (getUsuarioVO().getUsuarioEmpresaPerfilUsuarioVOs().size() > 1) {
					montarListaSelectItemEmpresa();
					setDeveAbrirModalEmpresa(true);
					return "";
				} else {
					getUsuarioLogado().setEmpresaLogado(getUsuarioVO().getUsuarioEmpresaPerfilUsuarioVOs().get(0).getEmpresaVO());
					inicializarDadosPerfilUsuarioPermissaoAcesso();
				}
			}
		} catch (Exception e) {
			setMensagemDetalhada("msg_erro", e.getMessage());
			return "";
		}
		setMensagemID("msg_entre_login", MensagemAlertaEnum.ATENCAO);
		return "home?faces-redirect=true";
	}
	
	public void inicializarDadosUsuario(UsuarioVO usuarioVO) {
		if (usuarioVO.getTipoUsuario().equals(TipoUsuarioEnum.FUNCIONARIO)) {
			usuarioVO.setFuncionarioLogadoVO(getFacadeFactory().getFuncionarioFacade().consultarFuncionarioPorCodigoUsuario(usuarioVO.getCodigo(), NivelMontarDadosEnum.COMPLETO, usuarioVO));
		} 
	}
	
	public void inicializarDadosPerfilUsuarioPermissaoAcesso() {
		getUsuarioVO().setPerfilUsuarioVO(getUsuarioVO().obterPerfilUsuarioPorEmpresa(getEmpresaLogado().getCodigo()));
		montarPermissoesMenu(getUsuarioVO().getPerfilUsuarioVO());
	}
	
	public void montarListaSelectItemEmpresa() {
		List<SelectItem> itens = new ArrayList<SelectItem>(0);
		for (UsuarioEmpresaPerfilUsuarioVO usuarioEmpresaPerfilUsuarioVO : getUsuarioVO().getUsuarioEmpresaPerfilUsuarioVOs()) {
			itens.add(new SelectItem(usuarioEmpresaPerfilUsuarioVO.getEmpresaVO().getCodigo(), usuarioEmpresaPerfilUsuarioVO.getEmpresaVO().getDescricao()));
		}
		setListaSelectItemEmpresaVOs(itens);
	}
	
	public String logarAposSelecionarEmpresa() {
		getFacadeFactory().getEmpresaFacade().carregarDados(getEmpresaVO(), NivelMontarDadosEnum.COMPLETO, usuarioVO);
		getUsuarioLogado().setEmpresaLogado(getEmpresaVO());
		inicializarDadosPerfilUsuarioPermissaoAcesso();
		setDeveAbrirModalEmpresa(false);
		setMensagemID("msg_entre_login", MensagemAlertaEnum.ATENCAO);
		return "home?faces-redirect=true";
	}
	
	public void montarPermissoesMenu(PerfilUsuarioVO perfilUsuarioVO) {
		setPermissaoAcessoMenuVO(getFacadeFactory().getPerfilUsuarioFacade().montarPermissoesMenu(perfilUsuarioVO));
	}

	public String getAbrirModalEmpresa() {
		if (getDeveAbrirModalEmpresa()) {
			return "$('#modalEmpresaLogar').modal('show');";
		}
		return "";
	}

	public UsuarioVO getUsuarioVO() {
		if (usuarioVO == null) {
			usuarioVO = new UsuarioVO();
		}
		return usuarioVO;
	}

	public void setUsuarioVO(UsuarioVO usuarioVO) {
		this.usuarioVO = usuarioVO;
	}

	public String getUserName() {
		if (userName == null) {
			userName = "";
		}
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getSenha() {
		if (senha == null) {
			senha = "";
		}
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String logout() {
		try {
			HttpSession session = (HttpSession) context().getExternalContext().getSession(false);
			session.invalidate();			
			Uteis.removerObjetoMemoria(this);
			setUsuarioVO(new UsuarioVO());
			setMensagemID("msg_entre_prmlogout");
			return "index?faces-redirect=true";
		} catch (Exception e) {
			setMensagemDetalhada("msg_erro", e.getMessage());
			return "home?faces-redirect=true";
		}
	}

	public PermissaoAcessoMenuVO getPermissaoAcessoMenuVO() {
		if (permissaoAcessoMenuVO == null) {
			permissaoAcessoMenuVO = new PermissaoAcessoMenuVO();
		}
		return permissaoAcessoMenuVO;
	}

	public void setPermissaoAcessoMenuVO(PermissaoAcessoMenuVO permissaoAcessoMenuVO) {
		this.permissaoAcessoMenuVO = permissaoAcessoMenuVO;
	}

	public List<SelectItem> getListaSelectItemEmpresaVOs() {
		if (listaSelectItemEmpresaVOs == null) {
			listaSelectItemEmpresaVOs = new ArrayList<SelectItem>(0);
		}
		return listaSelectItemEmpresaVOs;
	}

	public void setListaSelectItemEmpresaVOs(List<SelectItem> listaSelectItemEmpresaVOs) {
		this.listaSelectItemEmpresaVOs = listaSelectItemEmpresaVOs;
	}

	public Boolean getDeveAbrirModalEmpresa() {
		if (deveAbrirModalEmpresa == null) {
			deveAbrirModalEmpresa = false;
		}
		return deveAbrirModalEmpresa;
	}

	public void setDeveAbrirModalEmpresa(Boolean deveAbrirModalEmpresa) {
		this.deveAbrirModalEmpresa = deveAbrirModalEmpresa;
	}
	
	
}
