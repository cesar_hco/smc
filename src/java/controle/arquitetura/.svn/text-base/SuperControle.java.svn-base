/**
 * 
 */
package controle.arquitetura;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

import negocio.comuns.administrativo.EmpresaVO;
import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.arquitetura.enumeradores.MensagemAlertaEnum;
import negocio.facade.jdbc.arquitetura.FacadeFactory;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Carlos Eug�nio
 *
 */
public abstract class SuperControle implements Serializable {

	private static final long serialVersionUID = 1L;
	private static FacadeFactory facadeFactory;
	protected String mensagem = null;
	private String mensagemDetalhada = null;
	private String mensagemID = null;
	private Boolean apresentarMensagemSucesso;
	private Boolean apresentarMensagemAtencao;
	private Boolean apresentarMensagemErro;
	private String diretorioPastaWeb;
	private List listaConsultaVOs;
	private String valorConsulta;
	private String campoConsulta;
	private EmpresaVO empresaVO;

	public SuperControle() {
		super();
	}

	@Autowired
	public void setFacadeFactory(FacadeFactory facadeFactory) {
		SuperControle.facadeFactory = facadeFactory;
	}

	public static FacadeFactory getFacadeFactory() {
		return facadeFactory;
	}

	public UsuarioVO getUsuarioLogado() {
		UsuarioVO user = (UsuarioVO) context().getExternalContext().getSessionMap().get("usuarioLogado");
		return user;
	}

	protected FacesContext context() {
		return (FacesContext.getCurrentInstance());
	}

	public String getMensagem() {
		if ((getMensagemID() != null) && (!getMensagemID().equals(""))) {
			mensagem = getMensagemInternalizacao(getMensagemID());
			return mensagem;
		}
		return "";
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public String getMensagemID() {
		if (mensagemID == null) {
			mensagemID = "";
		}
		return mensagemID;
	}

	public void setMensagemID(String mensagemID) {
		this.mensagemID = mensagemID;
		this.mensagemDetalhada = "";
	}
	
	public void setMensagemID(String mensagemID, MensagemAlertaEnum mensagemAlerta) {
		this.mensagemID = mensagemID;
		this.mensagemDetalhada = "";
		if (mensagemAlerta.equals(MensagemAlertaEnum.SUCESSO)) {
			setApresentarMensagemSucesso(true);
			setApresentarMensagemAtencao(false);
			setApresentarMensagemErro(false);
		} else if (mensagemAlerta.equals(MensagemAlertaEnum.ATENCAO)) {
			setApresentarMensagemSucesso(false);
			setApresentarMensagemAtencao(true);
			setApresentarMensagemErro(false);
		}
	}

	public String getMensagemDetalhada() {
		if (mensagemDetalhada == null) {
			mensagemDetalhada = "";
		}
		return mensagemDetalhada;
	}

	public void setMensagemDetalhada(String mensagemDetalhada) {
		this.mensagemDetalhada = mensagemDetalhada;
	}

	protected String getMensagemInternalizacao(String mensagemID) {
		String mensagem = "(" + mensagemID + ") Mensagem n�o localizada nas propriedades de internaliza��o.";
		ResourceBundle bundle = null;
		Locale locale = null;
		String nomeBundle = context().getApplication().getMessageBundle();
		if (nomeBundle != null) {
			locale = context().getViewRoot().getLocale();
			bundle = ResourceBundle.getBundle(nomeBundle, locale, getCurrentLoader(nomeBundle));
			try {
				mensagem = bundle.getString(mensagemID);
				return mensagem;
			} catch (MissingResourceException e) {
				return mensagem;
			}
		}
		return mensagem;
	}

	public Locale getLocale() {
		return context().getViewRoot().getLocale();
	}

	protected static ClassLoader getCurrentLoader(Object fallbackClass) {
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		if (loader == null) {
			loader = fallbackClass.getClass().getClassLoader();
		}
		return loader;
	}
	
	public void setMensagemDetalhada(String mensagemID, String mensagemDetalhada) {
		this.mensagemID = mensagemID;
		this.mensagemDetalhada = tratarMensagemErroDetalhada(mensagemDetalhada);
		this.setApresentarMensagemErro(true);
	}

	public void setMensagemDetalhada(String mensagemID, String mensagemDetalhada1, Boolean sucesso) {
		if (sucesso) {
			setMensagemID("");
		} else {
			this.mensagemID = mensagemID;
		}
		this.mensagemDetalhada = tratarMensagemErroDetalhada(mensagemDetalhada1);
	}

	
	public String tratarMensagemErroDetalhada(String mensagemDetalhada) {
		String novaMensagem = mensagemDetalhada;
		// TODO Alberto 15/12/2010 Corrigido para aparecer a mensagem de erro do
		// c�digo somente no console
		String erroCodigo = "Motivo do Erro: >>>" + mensagemDetalhada;
		try {
			if (mensagemDetalhada == null) {
				novaMensagem = "Aconteceu um problema inesperado, informe ao administrador e o mesmo ser� solucionado.";
				// mensagemID = "msg_erro_inesperado";
				throw new Exception("A mensagem detalhada 'e.getMessage()' no bloco try/catch esta nula devido a um NullPointerException!");
			}
			if ((novaMensagem.indexOf("duplicar chave viola") != -1) || (novaMensagem.indexOf("duplicate key") != -1)) {
				if (novaMensagem.lastIndexOf("_") != -1) {
					if (novaMensagem.contains("contapagar_nrdocumento")) {
						novaMensagem = "J� existe um registro com este valor para o campo N�mero Documento.";
					} else {
						String campo = novaMensagem.substring(novaMensagem.lastIndexOf("_") + 1, novaMensagem.length() - 1);
						novaMensagem = "J� existe um registro com este valor para o campo " + campo.toUpperCase() + ".";
					}
				} else {
					novaMensagem = "J� existe um registro gravados com estes valores (" + mensagemDetalhada + ")";
				}
			}
			if (novaMensagem.contains("paiz_nome_key")) {
				novaMensagem = "J� existe um Pa�s cadastrado com este nome.";
			}
			if ((novaMensagem.indexOf("violates foreign key constraint") != -1) || (novaMensagem.indexOf("de chave estrangeira") != -1)) {
				novaMensagem = "Este registro � referenciado por outro cadastro, por isto n�o pode ser exclu�do e/ou modificado.";
				// System.out.println();
				// System.out.println(erroCodigo + "<<<");
				// TODO Alberto 15/12/2010 Corrigido para aparecer a mensagem de
				// erro do c�digo somente no console
			}

		} catch (Exception e) {
			// System.out.println("MENSAGEM => " + e.getMessage());
			;
		}
		return novaMensagem;
	}
	
	public Boolean getApresentarMensagem() {
		return !getMensagem().equals("");
	}
	
	public Boolean getApresentarMensagemDetalhada() {
		return !getMensagemDetalhada().equals("");
	}

	public Boolean getApresentarMensagemSucesso() {
		if (apresentarMensagemSucesso == null) {
			apresentarMensagemSucesso = false;
		}
		return apresentarMensagemSucesso;
	}

	public void setApresentarMensagemSucesso(Boolean apresentarMensagemSucesso) {
		this.apresentarMensagemSucesso = apresentarMensagemSucesso;
	}

	public Boolean getApresentarMensagemAtencao() {
		if (apresentarMensagemAtencao == null) {
			apresentarMensagemAtencao = false;
		}
		return apresentarMensagemAtencao;
	}

	public void setApresentarMensagemAtencao(Boolean apresentarMensagemAtencao) {
		this.apresentarMensagemAtencao = apresentarMensagemAtencao;
	}

	public Boolean getApresentarMensagemErro() {
		if (apresentarMensagemErro == null) {
			apresentarMensagemErro = false;
		}
		return apresentarMensagemErro;
	}

	public void setApresentarMensagemErro(Boolean apresentarMensagemErro) {
		this.apresentarMensagemErro = apresentarMensagemErro;
	}

	public String getCaminhoPastaWeb() {
		if (diretorioPastaWeb == null || diretorioPastaWeb.equals("")) {
			ServletContext servletContext = (ServletContext) this.context().getExternalContext().getContext();
			diretorioPastaWeb = servletContext.getRealPath("");
		}
		return diretorioPastaWeb;
	}
	
	public List getListaConsultaVOs() {
		if (listaConsultaVOs == null) {
			listaConsultaVOs = new ArrayList<>(0);
		}
		return listaConsultaVOs;
	}

	public void setListaConsultaVOs(List listaConsultaVOs) {
		this.listaConsultaVOs = listaConsultaVOs;
	}

	public String getValorConsulta() {
		if (valorConsulta == null) {
			valorConsulta = "";
		}
		return valorConsulta;
	}

	public void setValorConsulta(String valorConsulta) {
		this.valorConsulta = valorConsulta;
	}

	public String getCampoConsulta() {
		if (campoConsulta == null) {
			campoConsulta = "";
		}
		return campoConsulta;
	}

	public void setCampoConsulta(String campoConsulta) {
		this.campoConsulta = campoConsulta;
	}

	public EmpresaVO getEmpresaVO() {
		if (empresaVO == null) {
			empresaVO = new EmpresaVO();
		}
		return empresaVO;
	}

	public void setEmpresaVO(EmpresaVO empresaVO) {
		this.empresaVO = empresaVO;
	}
	
	public EmpresaVO getEmpresaLogado() {
		EmpresaVO empresa2VO = ((UsuarioVO) context().getExternalContext().getSessionMap().get("usuarioLogado")).getEmpresaLogado();
		if (empresa2VO != null) {
			empresaVO = new EmpresaVO();
			empresaVO = empresa2VO;
		}
		return empresaVO;
	}


}
