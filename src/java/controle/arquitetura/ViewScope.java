package controle.arquitetura;

import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.context.Flash;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.config.Scope;
import org.springframework.web.context.request.FacesRequestAttributes;

public class ViewScope implements Scope {
	
	public static final String VIEW_SCOPE_CALLBACKS = "viewScope.callbacks";

	@Override
	public synchronized Object get(String name, ObjectFactory<?> objectFactory) {
		Flash flash = getFlashMemoria();
		Object instanceFlash = flash.get(name);
		// System.out.println("ViewScope recebe nome: " + name);
		Object instanceView = getViewMap().get(name);
		if (instanceFlash == null && instanceView == null) {
			instanceView = objectFactory.getObject();
			// System.out.println("ADICIONANDO FLASH:");
			flash.put(name, instanceView);
			getViewMap().put(name, instanceView);
		} else if (instanceView != null && instanceFlash == null) {
			// System.out.println("LIMPANDO FLASH:");
			flash.clear();
			flash.put(name, instanceView);
		} else if (instanceFlash != null && instanceView == null) {
			getViewMap().put(name, instanceFlash);
			instanceView = instanceFlash;
			// System.out.println("LIMPANDO FLASH 2:");
			flash.clear();
			objectFactory = null;
		}
		// exibirMemoriaFlash();
		return instanceView;
	}

	public Flash getFlashMemoria() {
		return FacesContext.getCurrentInstance().getExternalContext().getFlash();
	}

	public void exibirMemoriaFlash() {
		System.out.println("MEMORIA FLASH:");
		System.out.println(getFlashMemoria().toString());

	}

	public Object remove(String name) {
		if (FacesContext.getCurrentInstance().getViewRoot() != null) {
			return FacesContext.getCurrentInstance().getViewRoot().getViewMap().remove(name);
		} else {
			return null;
		}

	}

	@SuppressWarnings("unchecked")
	public void registerDestructionCallback(String name, Runnable runnable) {
		Map<String, Runnable> callbacks = (Map<String, Runnable>) getViewMap().get(VIEW_SCOPE_CALLBACKS);
		if (callbacks != null) {
			callbacks.put(name, runnable);
		}
	}

	public Object resolveContextualObject(String name) {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		FacesRequestAttributes facesRequestAttributes = new FacesRequestAttributes(facesContext);
		return facesRequestAttributes.resolveReference(name);

	}

	public String getConversationId() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		FacesRequestAttributes facesRequestAttributes = new FacesRequestAttributes(facesContext);
		return facesRequestAttributes.getSessionId() + "-" + facesContext.getViewRoot().getViewId();

	}

	private Map<String, Object> getViewMap() {
		if (FacesContext.getCurrentInstance().getViewRoot() != null) {
			return (Map<String, Object>) FacesContext.getCurrentInstance().getViewRoot().getViewMap();
		}
		return null;

	}

}