package controle.basico;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import controle.arquitetura.SuperControle;
import negocio.comuns.administrativo.FuncionarioVO;
import negocio.comuns.arquitetura.enumeradores.MensagemAlertaEnum;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.comuns.basico.CidadeVO;

import negocio.comuns.utilitarias.Uteis;

/**
 * @author Cesar Henrique
 *
 */
@Controller("funcionarioControle")
@Scope("viewScope")
@Lazy


public class FuncionarioControle extends SuperControle implements Serializable{

	private static final long serialVersionUID = 1L;
	private FuncionarioVO funcionarioVO;
	private String valorConsultaCidade;
	private String campoConsultaCidade;
	private List<CidadeVO> listaConsultaCidadeVOs;
	
	private FuncionarioControle(){
		super();
    }
	
	public String novo() {
		Uteis.removerObjetoMemoria(this);
		setFuncionarioVO(new FuncionarioVO());
		setMensagemID("msg_entre_dados", MensagemAlertaEnum.ATENCAO);
		return "funcionarioForm?faces-redirect=true";
	}
	
	public String consultar() {
		try {
			setListaConsultaVOs(getFacadeFactory().getFuncionarioFacade().consultar(getCampoConsulta(), getValorConsulta(), NivelMontarDadosEnum.BASICO, getUsuarioLogado()));
			setMensagemID("msg_dados_consultados", MensagemAlertaEnum.ATENCAO);
		} catch (Exception e) {
			setMensagemDetalhada("msg_erro", e.getMessage());
		}
		return "funcionarioCons?faces-redirect=true";
	}
	
	public String irTelaConsulta() {
		getListaConsultaVOs().clear();
		setValorConsulta("");
		setMensagemID("msg_entre_prmconsulta", MensagemAlertaEnum.ATENCAO);
		return "funcionarioCons?faces-redirect=true";
	}
	
	public void persistir() {
		try {
			getFacadeFactory().getFuncionarioFacade().persistir(getFuncionarioVO(), getUsuarioLogado());
			setMensagemID("msg_dados_gravados", MensagemAlertaEnum.SUCESSO);
		} catch (Exception e) {
			setMensagemDetalhada("", e.getMessage());
		}
	}
	
	public String editar() {
		FuncionarioVO obj = (FuncionarioVO) context().getExternalContext().getRequestMap().get("funcionarioItem");
		obj.setNovoObj(Boolean.FALSE);
		setFuncionarioVO(obj);
		setMensagemID("msg_dados_selecionados", MensagemAlertaEnum.ATENCAO);
		return "funcionarioForm?faces-redirect=true";
	}
	
	public List<SelectItem> getListaSelectItemVOs() {
		List<SelectItem> itens = new ArrayList<SelectItem>(0);
		itens.add(new SelectItem("NOME", "Nome"));
		itens.add(new SelectItem("CODIGO", "Codigo"));
		return itens;
	}
	
	public String excluir() {
		try {
			getFacadeFactory().getFuncionarioFacade().excluir(getFuncionarioVO(), getUsuarioLogado());
			setMensagemID("msg_dados_excluidos", MensagemAlertaEnum.SUCESSO);
		} catch (Exception e) {
			setMensagemDetalhada("", e.getMessage());
		}
		return "funcionarioForm?faces-redirect=true";
	}
	
	public void consultarCidade() {
		try {
			setListaConsultaCidadeVOs(getFacadeFactory().getCidadeFacade().consultaRapidaPorNome(getValorConsultaCidade(), false, getUsuarioLogado()));
			setMensagemID("msg_dados_consultados", MensagemAlertaEnum.ATENCAO);
		} catch (Exception e) {
			setMensagemDetalhada("", e.getMessage());
		}
	}
	
	public void selecionarCidade() {
		CidadeVO obj = (CidadeVO) context().getExternalContext().getRequestMap().get("cidadeItem");
		getFuncionarioVO().getPessoaVO().setCidadeVO(obj);
		setMensagemID("msg_dados_selecionados", MensagemAlertaEnum.ATENCAO);
	}
	
	public void limparCidade() {
		getFuncionarioVO().getPessoaVO().setCidadeVO(null);
	}
	
	public void limparDadosConsultaModalCidade() {
		setListaConsultaCidadeVOs(null);
	}


	public String getValorConsultaCidade() {
		if (valorConsultaCidade == null) {
			valorConsultaCidade = "";
		}
		return valorConsultaCidade;
	}

	public void setValorConsultaCidade(String valorConsultaCidade) {
		this.valorConsultaCidade = valorConsultaCidade;
	}

	public String getCampoConsultaCidade() {
		if (campoConsultaCidade == null) {
			campoConsultaCidade = "";
		}
		return campoConsultaCidade;
	}

	public void setCampoConsultaCidade(String campoConsultaCidade) {
		this.campoConsultaCidade = campoConsultaCidade;
	}

	public List<CidadeVO> getListaConsultaCidadeVOs() {
		if (listaConsultaCidadeVOs == null) {
			listaConsultaCidadeVOs = new ArrayList<CidadeVO>(0);
		}
		return listaConsultaCidadeVOs;
	}

	public void setListaConsultaCidadeVOs(List<CidadeVO> listaConsultaCidadeVOs) {
		this.listaConsultaCidadeVOs = listaConsultaCidadeVOs;
	}

	public FuncionarioVO getFuncionarioVO() {
		return funcionarioVO;
	}

	public void setFuncionarioVO(FuncionarioVO funcionarioVO) {
		this.funcionarioVO = funcionarioVO;
	}


	

}
