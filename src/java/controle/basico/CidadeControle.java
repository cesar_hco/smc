package controle.basico;

/**
 * Classe respons�vel por implementar a intera��o entre os componentes JSF das p�ginas 
 * cidadeForm.jsp cidadeCons.jsp) com as funcionalidades da classe <code>Cidade</code>.
 * Implemta��o da camada controle (Backing Bean).
 * @see SuperControle
 * @see Cidade
 * @see CidadeVO
 */
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import negocio.comuns.basico.CidadeVO;
import negocio.comuns.basico.EstadoVO;
import negocio.comuns.basico.PaizVO;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import controle.arquitetura.SuperControle;

@Controller("CidadeControle")
@Scope("request")
@Lazy
public class CidadeControle extends SuperControle implements Serializable {

    private CidadeVO cidadeVO;
    /**
     * Interface <code>CidadeInterfaceFacade</code> respons�vel pela interconex�o da camada de controle com a camada de neg�cio.
     * Criando uma independ�ncia da camada de controle com rela��o a tenologia de persist�ncia dos dados (DesignPatter: Fa�ade).
     */
    protected List listaSelectItemEstado;
    protected List listaSelectItemPaiz;

    public CidadeControle() throws Exception {
        //obterUsuarioLogado();
        setMensagemID("msg_entre_prmconsulta");

    }

    @PostConstruct
    public void init() throws Exception {
        //obterUsuarioLogado();
        setMensagemID("msg_entre_prmconsulta");
    }

    /**
     * Rotina respons�vel por disponibilizar um novo objeto da classe <code>Cidade</code>
     * para edi��o pelo usu�rio da aplica��o.
     */
    public void executarThread() throws Exception {
    }

    public String novo() throws Exception {
        setCidadeVO(new CidadeVO());
        inicializarListasSelectItemTodosComboBox();
        setMensagemID("msg_entre_dados");
        return "editar";
    }

    public void enviarMensagemSms() {
        try {
//            HumanSms.enviarMensagem(getConfiguracaoGeralPadraoSistema(),Uteis.formatarTelefoneParaEnvioSms("(62)91813128"), "TESTE OTIMIZE: "
//                    + " seu resultado (registro:01) ja esta disponivel em nosso site (www.otimize-ti.com.br)");
        } catch (Exception e) {
        }
    }

    /**
     * Rotina respons�vel por disponibilizar os dados de um objeto da classe <code>Cidade</code> para altera��o.
     * O objeto desta classe � disponibilizado na session da p�gina (request) para que o JSP correspondente possa disponibiliz�-lo para edi��o.
     */
    public String editar() throws Exception {
        CidadeVO obj = (CidadeVO) context().getExternalContext().getRequestMap().get("cidade");
        context().getExternalContext().getSessionMap();
        context().getExternalContext().getRequestMap();
        obj.setNovoObj(Boolean.FALSE);
        setCidadeVO(obj);
        inicializarListasSelectItemTodosComboBox();
        setMensagemID("msg_dados_editar");
        return "editar";
    }

    /**
     * Rotina respons�vel por gravar no BD os dados editados de um novo objeto da classe <code>Cidade</code>.
     * Caso o objeto seja novo (ainda n�o gravado no BD) � acionado a opera��o <code>incluir()</code>. Caso contr�rio � acionado o <code>alterar()</code>.
     * Se houver alguma inconsist�ncia o objeto n�o � gravado, sendo re-apresentado para o usu�rio juntamente com uma mensagem de erro.
     */
    public String gravar() {
        try {
            if (cidadeVO.isNovoObj().booleanValue()) {
                getFacadeFactory().getCidadeFacade().incluir(cidadeVO, getUsuarioLogado());
            } else {
                getFacadeFactory().getCidadeFacade().alterar(cidadeVO, getUsuarioLogado());
            }
            setMensagemID("msg_dados_gravados");
            return "editar";
        } catch (Exception e) {
            setMensagemDetalhada("msg_erro", e.getMessage());
            return "editar";
        }
    }

    /**
     * Rotina responsavel por executar as consultas disponiveis no JSP CidadeCons.jsp.
     * Define o tipo de consulta a ser executada, por meio de ComboBox denominado campoConsulta, disponivel neste mesmo JSP.
     * Como resultado, disponibiliza um List com os objetos selecionados na sessao da pagina.
     */
    public String consultar() {
        try {
//            super.consultar();
//            List objs = new ArrayList(0);
//            if (getControleConsulta().getCampoConsulta().equals("codigo")) {
//                if (getControleConsulta().getValorConsulta().equals("")) {
//                    getControleConsulta().setValorConsulta("0");
//                }
//                int valorInt = Integer.parseInt(getControleConsulta().getValorConsulta());
//                objs = getFacadeFactory().getCidadeFacade().consultarPorCodigo(new Integer(valorInt), true, getUsuarioLogado());
//            }
//            if (getControleConsulta().getCampoConsulta().equals("nome")) {
//                if (getControleConsulta().getValorConsulta().length() < 2) {
//                    throw new Exception(getMensagemInternalizacao("msg_ParametroConsulta_vazio"));
//                }
//                objs = getFacadeFactory().getCidadeFacade().consultarPorNome(getControleConsulta().getValorConsulta(), true, getUsuarioLogado());
//            }
//            //if (getControleConsulta().getCampoConsulta().equals("estado")) {
//            //    objs = getFacadeFactory().getCidadeFacade().consultarPorEstado(getControleConsulta().getValorConsulta(), true);
//            //}
//            if (getControleConsulta().getCampoConsulta().equals("siglaEstado")) {
//                objs = getFacadeFactory().getCidadeFacade().consultarPorSiglaEstado(getControleConsulta().getValorConsulta(), true, Uteis.NIVELMONTARDADOS_TODOS, getUsuarioLogado());
//            }
//            if (getControleConsulta().getCampoConsulta().equals("estado")) {
//                objs = getFacadeFactory().getCidadeFacade().consultarPorEstado(getControleConsulta().getValorConsulta(), true, getUsuarioLogado());
//            }
//            if (getControleConsulta().getCampoConsulta().equals("cep")) {
//                objs = getFacadeFactory().getCidadeFacade().consultarPorCep(getControleConsulta().getValorConsulta(), true, Uteis.NIVELMONTARDADOS_TODOS, getUsuarioLogado());
//            }
//
//            //objs = ControleConsulta.obterSubListPaginaApresentar(objs, controleConsulta);
//            //definirVisibilidadeLinksNavegacao(controleConsulta.getPaginaAtual(), controleConsulta.getNrTotalPaginas());
//            setListaConsulta(objs);
            setMensagemID("msg_dados_consultados");
            return "consultar";
        } catch (Exception e) {
            setMensagemDetalhada("msg_erro", e.getMessage());
            return "consultar";
        }
    }

    /**
     * Opera��o respons�vel por processar a exclus�o um objeto da classe <code>CidadeVO</code>
     * Ap�s a exclus�o ela automaticamente aciona a rotina para uma nova inclus�o.
     */
    public String excluir() {
        try {            
            getFacadeFactory().getCidadeFacade().excluir(cidadeVO, getUsuarioLogado());
            setCidadeVO(new CidadeVO());
            setMensagemID("msg_dados_excluidos");
            return "editar";
        } catch (Exception e) {
            setMensagemDetalhada("msg_erro", e.getMessage());
            return "editar";
        }
    }

    public void irPaginaInicial() throws Exception {
//        JobNotificacaoRegistroAulaNota job = new JobNotificacaoRegistroAulaNota();
//        job.realizarNotificacaoRegistroAulaNota();
        //controleConsulta.setPaginaAtual(1);
        this.consultar();
    }

    public void irPaginaAnterior() throws Exception {
//        controleConsulta.setPaginaAtual(controleConsulta.getPaginaAtual() - 1);
        this.consultar();
    }

    public void irPaginaPosterior() throws Exception {
//        controleConsulta.setPaginaAtual(controleConsulta.getPaginaAtual() + 1);
        this.consultar();
    }

    public void irPaginaFinal() throws Exception {
//        controleConsulta.setPaginaAtual(controleConsulta.getNrTotalPaginas());
        this.consultar();
    }

    /**
     * M�todo respons�vel por consultar dados da entidade <code><code> e montar o atributo <code>nome</code>
     * Este atributo � uma lista (<code>List</code>) utilizada para definir os valores a serem apresentados no ComboBox correspondente
     */
    public List consultarPaizPorNome(String nomePrm) throws Exception {
        List lista = getFacadeFactory().getPaizFacade().consultarPorNome(nomePrm, false, getUsuarioLogado());
        return lista;
    }

    /**
     * M�todo respons�vel por gerar uma lista de objetos do tipo <code>SelectItem</code> para preencher
     * o comboBox relativo ao atributo <code>Paiz</code>.
     */
    public void montarListaSelectItemPaiz(String prm) throws Exception {
        List resultadoConsulta = null;
        Iterator i = null;
        try {
            resultadoConsulta = consultarPaizPorNome(prm);
            i = resultadoConsulta.iterator();
            List objs = new ArrayList(0);
            objs.add(new SelectItem(0, ""));
            while (i.hasNext()) {
                PaizVO obj = (PaizVO) i.next();
                objs.add(new SelectItem(obj.getCodigo(), obj.getNome().toString()));
            }
            setListaSelectItemPaiz(objs);
        } catch (Exception e) {
            throw e;
        } finally {
            i = null;
        }
    }

    /**
     * M�todo respons�vel por atualizar o ComboBox relativo ao atributo <code>Paiz</code>.
     * Buscando todos os objetos correspondentes a entidade <code>Paiz</code>. Esta rotina n�o recebe par�metros para filtragem de dados, isto �
     * importante para a inicializa��o dos dados da tela para o acionamento por meio requisi��es Ajax.
     */
    public void montarListaSelectItemPaiz() {
        try {
            montarListaSelectItemPaiz("");
        } catch (Exception e) {
            //System.out.println("MENSAGEM => " + e.getMessage());;
        }
    }

    /* M�todo respons�vel por inicializar List<SelectItem> de valores do 
     * ComboBox correspondente ao atributo <code>estado</code>
     */
    public void montarListaSelectItemEstado() throws Exception {
        List objs = new ArrayList(0);
        objs.add(new SelectItem(0, ""));
        Integer codigoPaiz = null;
        try {
            codigoPaiz = cidadeVO.getEstado().getPaiz().getCodigo();
            if (codigoPaiz == null || codigoPaiz.equals(0)) {
                setListaSelectItemEstado(objs);
                return;
            }
        } catch (NullPointerException e) {
            setListaSelectItemEstado(objs);
            return;
        }
//        List<EstadoVO> resultadoConsulta = getFacadeFactory().getEstadoFacade().consultarPorCodigoPaiz(codigoPaiz, false, Uteis.NIVELMONTARDADOS_DADOSBASICOS, getUsuarioLogado());
//
//        for (EstadoVO obj : resultadoConsulta) {
//            objs.add(new SelectItem(obj.getCodigo(), obj.getSigla().toString()));
//        }
        setListaSelectItemEstado(objs);
    }

    public void inicializarListasSelectItemTodosComboBox() throws Exception {
        montarListaSelectItemPaiz();
        montarListaSelectItemEstado();
    }

    /**
     * Rotina respons�vel por preencher a combo de consulta da telas.
     */
    public List getTipoConsultaCombo() {
        List itens = new ArrayList(0);
        itens.add(new SelectItem("nome", "Nome"));
        itens.add(new SelectItem("estado", "Estado"));
        return itens;
    }

    /**
     * Rotina respons�vel por organizar a pagina��o entre as p�ginas resultantes de uma consulta.
     */
    public String inicializarConsultar() {
       
        setMensagemID("msg_entre_prmconsulta");
        return "consultar";
    }

    public CidadeVO getCidadeVO() {
        return cidadeVO;
    }

    public void setCidadeVO(CidadeVO cidadeVO) {
        this.cidadeVO = cidadeVO;
    }

    public List getListaSelectItemEstado() {
        return listaSelectItemEstado;
    }

    public void setListaSelectItemEstado(List listaSelectItemEstado) {
        this.listaSelectItemEstado = listaSelectItemEstado;
    }

    public List getListaSelectItemPaiz() {
        return listaSelectItemPaiz;
    }

    public void setListaSelectItemPaiz(List listaSelectItemPaiz) {
        this.listaSelectItemPaiz = listaSelectItemPaiz;
    }
}
