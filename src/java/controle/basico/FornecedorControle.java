package controle.basico;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import controle.arquitetura.SuperControle;
import negocio.comuns.arquitetura.enumeradores.MensagemAlertaEnum;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.comuns.basico.CidadeVO;
import negocio.comuns.basico.FornecedorVO;
import negocio.comuns.utilitarias.Uteis;

@Controller("fornecedorControle")
@Scope("viewScope")
@Lazy
public class FornecedorControle extends SuperControle implements Serializable {

	private static final long serialVersionUID = 1L;
	private FornecedorVO fornecedorVO;
	private String valorConsultaCidade;
	private String campoConsultaCidade;
	private List<CidadeVO> listaConsultaCidadeVOs;

	public FornecedorControle() {
		super();
	}
	
	public String novo() {
		Uteis.removerObjetoMemoria(this);
		setFornecedorVO(new FornecedorVO());
		setMensagemID("msg_entre_dados", MensagemAlertaEnum.ATENCAO);
		return "fornecedorForm?faces-redirect=true";
	}
	
	public String consultar() {
		try {
			setListaConsultaVOs(getFacadeFactory().getFornecedorFacade().consultar(getCampoConsulta(), getValorConsulta(), NivelMontarDadosEnum.BASICO, getUsuarioLogado()));
			setMensagemID("msg_dados_consultados", MensagemAlertaEnum.ATENCAO);
		} catch (Exception e) {
			setMensagemDetalhada("msg_erro", e.getMessage());
		}
		return "fornecedorCons?faces-redirect=true";
	}
	
	public String irTelaConsulta() {
		getListaConsultaVOs().clear();
		setValorConsulta("");
		setMensagemID("msg_entre_prmconsulta", MensagemAlertaEnum.ATENCAO);
		return "fornecedorCons?faces-redirect=true";
	}
	
	public void persistir() {
		try {
			getFacadeFactory().getFornecedorFacade().persistir(getFornecedorVO(), getUsuarioLogado());
			setMensagemID("msg_dados_gravados", MensagemAlertaEnum.SUCESSO);
		} catch (Exception e) {
			setMensagemDetalhada("", e.getMessage());
		}
	}
	
	public String editar() {
		FornecedorVO obj = (FornecedorVO) context().getExternalContext().getRequestMap().get("fornecedorItem");
		obj.setNovoObj(Boolean.FALSE);
		setFornecedorVO(obj);
		setMensagemID("msg_dados_selecionados", MensagemAlertaEnum.ATENCAO);
		return "fornecedorForm?faces-redirect=true";
	}
	
	public List<SelectItem> getListaSelectItemVOs() {
		List<SelectItem> itens = new ArrayList<SelectItem>(0);
		itens.add(new SelectItem("NOME", "Nome"));
		return itens;
	}
	
	public String excluir() {
		try {
			getFacadeFactory().getFornecedorFacade().excluir(getFornecedorVO(), getUsuarioLogado());
			setMensagemID("msg_dados_excluidos", MensagemAlertaEnum.SUCESSO);
		} catch (Exception e) {
			setMensagemDetalhada("", e.getMessage());
		}
		return "fornecedorForm?faces-redirect=true";
	}
	
	public void consultarCidade() {
		try {
			setListaConsultaCidadeVOs(getFacadeFactory().getCidadeFacade().consultaRapidaPorNome(getValorConsultaCidade(), false, getUsuarioLogado()));
			setMensagemID("msg_dados_consultados", MensagemAlertaEnum.ATENCAO);
		} catch (Exception e) {
			setMensagemDetalhada("", e.getMessage());
		}
	}
	
	public void selecionarCidade() {
		CidadeVO obj = (CidadeVO) context().getExternalContext().getRequestMap().get("cidadeItem");
		getFornecedorVO().setCidadeVO(obj);
		setMensagemID("msg_dados_selecionados", MensagemAlertaEnum.ATENCAO);
	}
	
	public void limparCidade() {
		getFornecedorVO().setCidadeVO(null);
	}
	
	public void limparDadosConsultaModalCidade() {
		setListaConsultaCidadeVOs(null);
	}


	public String getValorConsultaCidade() {
		if (valorConsultaCidade == null) {
			valorConsultaCidade = "";
		}
		return valorConsultaCidade;
	}

	public void setValorConsultaCidade(String valorConsultaCidade) {
		this.valorConsultaCidade = valorConsultaCidade;
	}

	public String getCampoConsultaCidade() {
		if (campoConsultaCidade == null) {
			campoConsultaCidade = "";
		}
		return campoConsultaCidade;
	}

	public void setCampoConsultaCidade(String campoConsultaCidade) {
		this.campoConsultaCidade = campoConsultaCidade;
	}

	public List<CidadeVO> getListaConsultaCidadeVOs() {
		if (listaConsultaCidadeVOs == null) {
			listaConsultaCidadeVOs = new ArrayList<CidadeVO>(0);
		}
		return listaConsultaCidadeVOs;
	}

	public void setListaConsultaCidadeVOs(List<CidadeVO> listaConsultaCidadeVOs) {
		this.listaConsultaCidadeVOs = listaConsultaCidadeVOs;
	}

	public FornecedorVO getFornecedorVO() {
		if (fornecedorVO == null) {
			fornecedorVO = new FornecedorVO();
		}
		return fornecedorVO;
	}

	public void setFornecedorVO(FornecedorVO fornecedorVO) {
		this.fornecedorVO = fornecedorVO;
	}


	

}
