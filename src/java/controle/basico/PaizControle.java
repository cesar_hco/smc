package controle.basico;
import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;

import negocio.comuns.basico.PaizVO;
import negocio.facade.jdbc.basico.Paiz;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import controle.arquitetura.SuperControle;

/**
 * Classe respons�vel por implementar a intera��o entre os componentes JSF das p�ginas 
 * paizForm.jsp paizCons.jsp) com as funcionalidades da classe <code>Paiz</code>.
 * Implemta��o da camada controle (Backing Bean).
 * @see SuperControle
 * @see Paiz
 * @see PaizVO
*/

@Controller("PaizControle")
@Scope("request")
@Lazy
public class PaizControle extends SuperControle{

    private PaizVO paizVO;

    public PaizControle() throws Exception {
        setMensagemID("msg_entre_prmconsulta");
    }

    /**
    * Rotina respons�vel por disponibilizar um novo objeto da classe <code>Paiz</code>
    * para edi��o pelo usu�rio da aplica��o.
    */
    public String novo() {
        setPaizVO(new PaizVO());
        setMensagemID("msg_entre_dados");
        return "editar";
    }

    /**
    * Rotina respons�vel por disponibilizar os dados de um objeto da classe <code>Paiz</code> para altera��o.
    * O objeto desta classe � disponibilizado na session da p�gina (request) para que o JSP correspondente possa disponibiliz�-lo para edi��o.
    */
    public String editar() {
        PaizVO obj = (PaizVO)context().getExternalContext().getRequestMap().get("paiz");
        obj.setNovoObj(Boolean.FALSE);
        setPaizVO(obj);
        setMensagemID("msg_dados_editar");
        return "editar";
    }

    /**
    * Rotina respons�vel por gravar no BD os dados editados de um novo objeto da classe <code>Paiz</code>.
    * Caso o objeto seja novo (ainda n�o gravado no BD) � acionado a opera��o <code>incluir()</code>. Caso contr�rio � acionado o <code>alterar()</code>.
    * Se houver alguma inconsist�ncia o objeto n�o � gravado, sendo re-apresentado para o usu�rio juntamente com uma mensagem de erro.
    */
    public String gravar() {
        try {
            if (getPaizVO().isNovoObj().booleanValue()) {
                getFacadeFactory().getPaizFacade().incluir(getPaizVO(), getUsuarioLogado());
            } else {
                getFacadeFactory().getPaizFacade().alterar(getPaizVO());
            }
            setMensagemID("msg_dados_gravados");
            return "editar";
        } catch (Exception e) {
            setMensagemDetalhada("msg_erro", e.getMessage());
            return "editar";
        }
    }

    /**
    * Rotina responsavel por executar as consultas disponiveis no JSP PaizCons.jsp.
    * Define o tipo de consulta a ser executada, por meio de ComboBox denominado campoConsulta, disponivel neste mesmo JSP.
    * Como resultado, disponibiliza um List com os objetos selecionados na sessao da pagina.
    */
    public String consultar() {
        try {            
//            super.consultar();
//            List objs = new ArrayList(0);
//            if (getControleConsulta().getCampoConsulta().equals("codigo")) {
//                if (getControleConsulta().getValorConsulta().equals("")) {
//                    getControleConsulta().setValorConsulta("0");
//                }
//                int valorInt = Integer.parseInt(getControleConsulta().getValorConsulta());
//                objs = getFacadeFactory().getPaizFacade().consultarPorCodigo(new Integer(valorInt), true, getUsuarioLogado());
//            }
//            if (getControleConsulta().getCampoConsulta().equals("nome")) {
//                objs = getFacadeFactory().getPaizFacade().consultarPorNome(getControleConsulta().getValorConsulta(), true, getUsuarioLogado());
//            }
//            //objs = ControleConsulta.obterSubListPaginaApresentar(objs, controleConsulta);
//            //definirVisibilidadeLinksNavegacao(controleConsulta.getPaginaAtual(), controleConsulta.getNrTotalPaginas());
//            setListaConsulta(objs);
            setMensagemID("msg_dados_consultados");
            return "consultar";
        } catch (Exception e) {
//            setListaConsulta(new ArrayList(0));
            setMensagemDetalhada("msg_erro", e.getMessage());
            return "consultar";
        }
    }

    /**
     * Opera��o respons�vel por processar a exclus�o um objeto da classe <code>PaizVO</code>
     * Ap�s a exclus�o ela automaticamente aciona a rotina para uma nova inclus�o.
     */
    public String excluir() {
        try {
            getFacadeFactory().getPaizFacade().excluir(getPaizVO());
            setPaizVO( new PaizVO());
            setMensagemID("msg_dados_excluidos");
            return "editar";
        } catch (Exception e) {
            setMensagemDetalhada("msg_erro", e.getMessage());
            return "editar";
        }
    }

    /**
    * Rotina respons�vel por preencher a combo de consulta da telas.
    */
    public List getTipoConsultaCombo() {
        List itens = new ArrayList(0);        
        itens.add(new SelectItem("nome", "Nome"));
        return itens;
    }

    /**
    * Rotina respons�vel por organizar a pagina��o entre as p�ginas resultantes de uma consulta.
    */
    public String inicializarConsultar() {
        setMensagemID("msg_entre_prmconsulta");
        return "consultar";
    }

    public PaizVO getPaizVO() {
        if (paizVO == null) {
            paizVO = new PaizVO();
        }
        return paizVO;
    }
     
    public void setPaizVO(PaizVO paizVO) {
        this.paizVO = paizVO;
    }
}