package controle.basico;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.faces.model.SelectItem;

import negocio.comuns.basico.EstadoVO;
import negocio.comuns.basico.PaizVO;
import negocio.comuns.basico.enumeradores.RegiaoEnum;
import negocio.facade.jdbc.basico.Estado;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import controle.arquitetura.SuperControle;

/**
 * Classe respons�vel por implementar a intera��o entre os componentes JSF das
 * p�ginas estadoForm.jsp estadoCons.jsp) com as funcionalidades da classe
 * <code>Estado</code>. Implemta��o da camada controle (Backing Bean).
 * 
 * @see SuperControle
 * @see Estado
 * @see EstadoVO
 */

@Controller("EstadoControle")
@Scope("request")
@Lazy
public class EstadoControle extends SuperControle implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6129362327635420829L;
	private EstadoVO estadoVO;
	protected List<SelectItem> listaSelectItemPaiz;
	protected List<SelectItem> listaSelectItemRegiao;


	public EstadoControle() throws Exception {
		// obterUsuarioLogado();
		setMensagemID("msg_entre_prmconsulta");
	}

	public String novo() {
		setEstadoVO(new EstadoVO());
		inicializarListasSelectItemTodosComboBox();
		setMensagemID("msg_entre_dados");
		return "editar";
	}

	public String editar() {
		EstadoVO obj = (EstadoVO) context().getExternalContext().getRequestMap().get("estado");
		obj.setNovoObj(Boolean.FALSE);
		setEstadoVO(obj);
		inicializarListasSelectItemTodosComboBox();
		setMensagemID("msg_dados_editar");
		return "editar";
	}

	public String gravar() {
		try {
			if (estadoVO.isNovoObj().booleanValue()) {
				getFacadeFactory().getEstadoFacade().incluir(estadoVO, getUsuarioLogado());
			} else {
				getFacadeFactory().getEstadoFacade().alterar(estadoVO, getUsuarioLogado());
			}
			setMensagemID("msg_dados_gravados");
			return "editar";
		} catch (Exception e) {
			setMensagemDetalhada("msg_erro", e.getMessage());
			return "editar";
		}
	}

	public String consultar() {
		try {
//			super.consultar();
//			List<EstadoVO> objs = new ArrayList<EstadoVO>(0);
//			if (getControleConsulta().getCampoConsulta().equals("codigo")) {
//				if (getControleConsulta().getValorConsulta().equals("")) {
//					getControleConsulta().setValorConsulta("0");
//				}
//				int valorInt = Integer.parseInt(getControleConsulta().getValorConsulta());
//				objs = getFacadeFactory().getEstadoFacade().consultarPorCodigo(new Integer(valorInt), true, Uteis.NIVELMONTARDADOS_TODOS, getUsuarioLogado());
//			}
//			if (getControleConsulta().getCampoConsulta().equals("sigla")) {
//				objs = getFacadeFactory().getEstadoFacade().consultarPorSigla(getControleConsulta().getValorConsulta(), true, Uteis.NIVELMONTARDADOS_TODOS, getUsuarioLogado());
//			}
//			if (getControleConsulta().getCampoConsulta().equals("nome")) {
//				objs = getFacadeFactory().getEstadoFacade().consultarPorNome(getControleConsulta().getValorConsulta(), true, Uteis.NIVELMONTARDADOS_TODOS, getUsuarioLogado());
//			}
//			if (getControleConsulta().getCampoConsulta().equals("nomePaiz")) {
//				objs = getFacadeFactory().getEstadoFacade().consultarPorNomePaiz(getControleConsulta().getValorConsulta(), true, Uteis.NIVELMONTARDADOS_TODOS, getUsuarioLogado());
//			}
//			setListaConsulta(objs);
			setMensagemID("msg_dados_consultados");
			return "consultar";
		} catch (Exception e) {
			setMensagemDetalhada("msg_erro", e.getMessage());
			return "consultar";
		}
	}

	public String excluir() {
		try {
			getFacadeFactory().getEstadoFacade().excluir(estadoVO, getUsuarioLogado());
			novo();
			setMensagemID("msg_dados_excluidos");
			return "editar";
		} catch (Exception e) {
			setMensagemDetalhada("msg_erro", e.getMessage());
			return "editar";
		}
	}

	public void montarListaSelectItemPaiz(String prm) throws Exception {
		List<PaizVO> resultadoConsulta = null;
		Iterator<PaizVO> i = null;
		try {
			resultadoConsulta = consultarPaizPorNome(prm);
			i = resultadoConsulta.iterator();
			List<SelectItem> objs = new ArrayList<SelectItem>(0);
			objs.add(new SelectItem(0, ""));
			while (i.hasNext()) {
				PaizVO obj = (PaizVO) i.next();
				objs.add(new SelectItem(obj.getCodigo(), obj.getNome().toString()));
			}
			setListaSelectItemPaiz(objs);
		} catch (Exception e) {
			throw e;
		} finally {
			i = null;
		}
	}


	public void montarListaSelectItemPaiz() {
		try {
			montarListaSelectItemPaiz("");
		} catch (Exception e) {
			// System.out.println("MENSAGEM => " + e.getMessage());;
		}
	}

	public List<PaizVO> consultarPaizPorNome(String nomePrm) throws Exception {
		List<PaizVO> lista = getFacadeFactory().getPaizFacade().consultarPorNome(nomePrm, false, getUsuarioLogado());
		return lista;
	}

	public void inicializarListasSelectItemTodosComboBox() {
		montarListaSelectItemPaiz();
	}

	public String getMascaraConsulta() {
		return "";
	}

	public List<SelectItem> getTipoConsultaCombo() {
		List<SelectItem> itens = new ArrayList<SelectItem>(0);
		itens.add(new SelectItem("nome", "Nome"));
		itens.add(new SelectItem("sigla", "Sigla"));
		itens.add(new SelectItem("codigo", "C�digo"));
		itens.add(new SelectItem("nomePaiz", "Pa�s"));
		return itens;
	}

	public String inicializarConsultar() {
		setMensagemID("msg_entre_prmconsulta");
		return "consultar";
	}

	protected void limparRecursosMemoria() {
	}

	public List<SelectItem> getListaSelectItemPaiz() {
		if (listaSelectItemPaiz == null) {
			listaSelectItemPaiz = new ArrayList<SelectItem>(0);
		}
		return (listaSelectItemPaiz);
	}

	public List<SelectItem> getListaSelectItemRegiao() {
		if (listaSelectItemRegiao == null) {
			listaSelectItemRegiao = new ArrayList<SelectItem>(0);
			listaSelectItemRegiao.add(new SelectItem("", ""));
			for (RegiaoEnum regiaoEnum : RegiaoEnum.values()) {
				listaSelectItemRegiao.add(new SelectItem(regiaoEnum.name(), regiaoEnum.getValorApresentar()));
			}
		}
		return (listaSelectItemRegiao);
	}

	public void setListaSelectItemPaiz(List<SelectItem> listaSelectItemPaiz) {
		this.listaSelectItemPaiz = listaSelectItemPaiz;
	}

	public EstadoVO getEstadoVO() {
		return estadoVO;
	}

	public void setEstadoVO(EstadoVO estadoVO) {
		this.estadoVO = estadoVO;
	}
}
