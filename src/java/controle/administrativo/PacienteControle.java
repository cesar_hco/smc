package controle.administrativo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.faces.model.SelectItem;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import controle.arquitetura.SuperControle;
import negocio.comuns.administrativo.ConvenioVO;
import negocio.comuns.administrativo.PacienteVO;
import negocio.comuns.arquitetura.enumeradores.MensagemAlertaEnum;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.comuns.basico.CidadeVO;
import negocio.comuns.basico.enumeradores.EstadoCivilEnum;
import negocio.comuns.financeiro.ContaCorrenteVO;
import negocio.comuns.financeiro.enumeradores.TipoDescontoEnum;
import negocio.comuns.utilitarias.Uteis;

/**
 * @author Cesar Henrique
 *
 */
@Controller("pacienteControle")
@Scope("viewScope")
@Lazy


public class PacienteControle extends SuperControle implements Serializable{

	private static final long serialVersionUID = 1L;
	private PacienteVO pacienteVO;
	private String valorConsultaCidade;
	private String campoConsultaCidade;
	private List<CidadeVO> listaConsultaCidadeVOs;
	private List<PacienteVO> listaConsultaPacienteVOs;
	private List<SelectItem> listaSelectItemConvenioVOs;
	
	private PacienteControle(){
		super();
    }
	
	public String novo() {
		Uteis.removerObjetoMemoria(this);
		setPacienteVO(new PacienteVO());
		montarListaSelectItemConvenio();
		setMensagemID("msg_entre_dados", MensagemAlertaEnum.ATENCAO);
		return "pacienteForm?faces-redirect=true";
	}
	
	public String irTelaConsulta() {
		getListaConsultaVOs().clear();
		getListaSelectItemConvenioVOs().clear();
		setValorConsulta("");
		setMensagemID("msg_entre_prmconsulta", MensagemAlertaEnum.ATENCAO);
		return "pacienteCons?faces-redirect=true";
	}
	
	public void persistir() {
		try {
			getFacadeFactory().getPacienteFacade().persistir(getPacienteVO(), getUsuarioLogado());
			setMensagemID("msg_dados_gravados", MensagemAlertaEnum.SUCESSO);
		} catch (Exception e) {
			setMensagemDetalhada("", e.getMessage());
		}
	}
	
	public String editar() {
		PacienteVO obj = (PacienteVO) context().getExternalContext().getRequestMap().get("pacienteItem");
		obj.setNovoObj(Boolean.FALSE);
		montarListaSelectItemConvenio();
		setPacienteVO(obj);
		setMensagemID("msg_dados_selecionados", MensagemAlertaEnum.ATENCAO);
		return "pacienteForm?faces-redirect=true";
	}
	
	public List<SelectItem> getListaSelectItemVOs() {
		List<SelectItem> itens = new ArrayList<SelectItem>(0);
		itens.add(new SelectItem("NOME", "Nome"));
		itens.add(new SelectItem("CODIGO", "Codigo"));
		return itens;
	}
	
	public String excluir() {
		try {
			getFacadeFactory().getPacienteFacade().excluir(getPacienteVO(), getUsuarioLogado());
			setMensagemID("msg_dados_excluidos", MensagemAlertaEnum.SUCESSO);
		} catch (Exception e) {
			setMensagemDetalhada("", e.getMessage());
		}
		return "pacienteForm?faces-redirect=true";
	}
	
	public String consultar() {
		try {
			setListaConsultaPacienteVOs(getFacadeFactory().getPacienteFacade().consultar(getCampoConsulta(), getValorConsulta(), NivelMontarDadosEnum.BASICO,  getUsuarioLogado()));
			setMensagemID("msg_dados_consultados", MensagemAlertaEnum.ATENCAO);
		} catch (Exception e) {
			setMensagemDetalhada("msg_erro", e.getMessage());
		}
		return "pacienteCons?faces-redirect=true";
	}
	
	
	public void consultarCidade() {
		try {
			setListaConsultaCidadeVOs(getFacadeFactory().getCidadeFacade().consultaRapidaPorNome(getValorConsultaCidade(), false, getUsuarioLogado()));
			setMensagemID("msg_dados_consultados", MensagemAlertaEnum.ATENCAO);
		} catch (Exception e) {
			setMensagemDetalhada("", e.getMessage());
		}
	}
	
	public void selecionarCidade() {
		CidadeVO obj = (CidadeVO) context().getExternalContext().getRequestMap().get("cidadeItem");
		getPacienteVO().getPessoa().setCidadeVO(obj);
		setMensagemID("msg_dados_selecionados", MensagemAlertaEnum.ATENCAO);
	}
	
	public void limparCidade() {
		getPacienteVO().getPessoa().setCidadeVO(null);
	}
	
	public void limparDadosConsultaModalCidade() {
		setListaConsultaCidadeVOs(null);
	}


	public PacienteVO getPacienteVO() {
		return pacienteVO;
	}

	public void setPacienteVO(PacienteVO pacienteVO) {
		this.pacienteVO = pacienteVO;
	}

	public String getValorConsultaCidade() {
		return valorConsultaCidade;
	}

	public void setValorConsultaCidade(String valorConsultaCidade) {
		this.valorConsultaCidade = valorConsultaCidade;
	}

	public String getCampoConsultaCidade() {
		return campoConsultaCidade;
	}

	public void setCampoConsultaCidade(String campoConsultaCidade) {
		this.campoConsultaCidade = campoConsultaCidade;
	}

	public List<CidadeVO> getListaConsultaCidadeVOs() {
		return listaConsultaCidadeVOs;
	}

	public void setListaConsultaCidadeVOs(List<CidadeVO> listaConsultaCidadeVOs) {
		this.listaConsultaCidadeVOs = listaConsultaCidadeVOs;
	}
	
	public List<SelectItem> getListaSelectItemConvenioVOs() {
		if (listaSelectItemConvenioVOs == null) {
			listaSelectItemConvenioVOs = new ArrayList<SelectItem>(0);
		}
		return listaSelectItemConvenioVOs;
	}

	public void setListaSelectItemConvenioVOs(List<SelectItem> listaSelectItemConvenioVOs) {
		this.listaSelectItemConvenioVOs = listaSelectItemConvenioVOs;
	}
	
	public List<PacienteVO> getListaConsultaPacienteVOs() {
		if (listaConsultaPacienteVOs == null) {
			listaConsultaPacienteVOs = new ArrayList<PacienteVO>(0);
		}
		return listaConsultaPacienteVOs;
	}

	public void setListaConsultaPacienteVOs(List<PacienteVO> listaConsultaPacienteVOs) {
		this.listaConsultaPacienteVOs = listaConsultaPacienteVOs;
	}

	public List<SelectItem> getListaSelectItemEstadoCivilVOs() {
		List<SelectItem> itens = new ArrayList<SelectItem>(0);
		itens.add(new SelectItem(EstadoCivilEnum.CASADO , EstadoCivilEnum.CASADO.getDescricao()));
		itens.add(new SelectItem(EstadoCivilEnum.COMPANHEIRO, EstadoCivilEnum.COMPANHEIRO.getDescricao()));
		itens.add(new SelectItem(EstadoCivilEnum.DIVORCIADO , EstadoCivilEnum.DIVORCIADO.getDescricao()));
		itens.add(new SelectItem(EstadoCivilEnum.SEPARADO, EstadoCivilEnum.SEPARADO.getDescricao()));
		itens.add(new SelectItem(EstadoCivilEnum.SOLTEIRO , EstadoCivilEnum.SOLTEIRO.getDescricao()));
		itens.add(new SelectItem(EstadoCivilEnum.VIUVO, EstadoCivilEnum.VIUVO.getDescricao()));
		
		return itens;
	}
	
	public void montarListaSelectItemConvenio() {
		List<ConvenioVO> listaConvenioVOs = getFacadeFactory().getConveniointerfaceFacade().consultar(getCampoConsulta(), getValorConsulta(), NivelMontarDadosEnum.BASICO,  getUsuarioLogado());
		List<SelectItem> itens = new ArrayList<SelectItem>(0);
		itens.add(new SelectItem("", ""));
		for (ConvenioVO convenioVO : listaConvenioVOs) {
			itens.add(new SelectItem(convenioVO.getCodigo(), convenioVO.getDescricao()));
		}
		setListaSelectItemConvenioVOs(itens);
	}
	
	
}
