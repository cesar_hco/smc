/**
 * 
 */
package controle.administrativo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;

import negocio.comuns.administrativo.EmpresaVO;
import negocio.comuns.administrativo.FuncionarioVO;
import negocio.comuns.administrativo.PerfilUsuarioVO;
import negocio.comuns.administrativo.UsuarioEmpresaPerfilUsuarioVO;
import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.administrativo.enumeradores.TipoUsuarioEnum;
import negocio.comuns.arquitetura.enumeradores.MensagemAlertaEnum;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.comuns.basico.enumeradores.FuncaoMembroEnum;
import negocio.comuns.utilitarias.Uteis;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import controle.arquitetura.SuperControle;

/**
 * @author Carlos Eugênio
 *
 */
@Controller("usuarioControle")
@Scope("viewScope")
@Lazy
public class UsuarioControle extends SuperControle implements Serializable {

	private UsuarioVO usuarioVO;
	private EmpresaVO empresaVO;
	private PerfilUsuarioVO perfilUsuarioVO;
	private FuncionarioVO funcionarioVO;
	
	private String valorConsultaEmpresa;
	private String campoConsultaEmpresa;
	private List<EmpresaVO> listaConsultaEmpresaVOs;
	
	private String valorConsultaPerfilUsuario;
	private String campoConsultaPerfilUsuario;
	private List<PerfilUsuarioVO> listaConsultaPerfilUsuarioVOs;
	
	private String valorConsultaFuncionario;
	private String campoConsultaFuncionario;
	private List<FuncionarioVO> listaConsultaFuncionarioVOs;
	
	private static final long serialVersionUID = 1L;

	public UsuarioControle() {
		super();
	}
	
	public String novo() {
		Uteis.removerObjetoMemoria(this);
		setUsuarioVO(new UsuarioVO());
		setMensagemID("msg_entre_dados", MensagemAlertaEnum.ATENCAO);
		return "usuarioForm?faces-redirect=true";
	}
	
	public String consultar() {
		try {
			setListaConsultaVOs(getFacadeFactory().getUsuarioFacade().consultar(getCampoConsulta(), getValorConsulta(), getUsuarioLogado()));
			setMensagemID("msg_dados_consultados", MensagemAlertaEnum.ATENCAO);
		} catch (Exception e) {
			setMensagemDetalhada("msg_erro", e.getMessage());
		}
		return "usuarioCons?faces-redirect=true";
	}
	
	public String irTelaConsulta() {
		getListaConsultaVOs().clear();
		setValorConsulta("");
		setMensagemID("msg_entre_prmconsulta", MensagemAlertaEnum.ATENCAO);
		return "usuarioCons?faces-redirect=true";
	}
	
	public void persistir() {
		try {
			getFacadeFactory().getUsuarioFacade().inicializarDadosPessoaUsuario(getUsuarioVO(), getFuncionarioVO());
			getFacadeFactory().getUsuarioFacade().persistir(getUsuarioVO(), getUsuarioLogado());
			setMensagemID("msg_dados_gravados", MensagemAlertaEnum.SUCESSO);
		} catch (Exception e) {
			setMensagemDetalhada("", e.getMessage());
		}
	}
	
	public String editar() {
		UsuarioVO obj = (UsuarioVO) context().getExternalContext().getRequestMap().get("usuarioItem");
		obj.setNovoObj(Boolean.FALSE);
		setUsuarioVO(obj);
		getFacadeFactory().getUsuarioFacade().carregarDados(getUsuarioVO(), NivelMontarDadosEnum.COMPLETO, getUsuarioLogado());
		inicializarDadosTipoUsuarioEdicao(getUsuarioVO());
		setMensagemID("msg_dados_selecionados", MensagemAlertaEnum.ATENCAO);
		return "usuarioForm?faces-redirect=true";
	}
	
	public void inicializarDadosTipoUsuarioEdicao(UsuarioVO usuarioVO) {
		if (usuarioVO.getTipoUsuario().equals(TipoUsuarioEnum.FUNCIONARIO)) {
			getFuncionarioVO().getPessoaVO().setCodigo(usuarioVO.getPessoaVO().getCodigo());
			getFuncionarioVO().getPessoaVO().setNome(usuarioVO.getPessoaVO().getNome());
		}
	}
	
	public List<SelectItem> getListaSelectItemVOs() {
		List<SelectItem> itens = new ArrayList<SelectItem>(0);
		itens.add(new SelectItem("NOME", "Nome"));
		return itens;
	}
	
	public String excluir() {
		try {
			getFacadeFactory().getUsuarioFacade().excluir(getUsuarioVO(), getUsuarioLogado());
			novo();
			setMensagemID("msg_dados_excluidos", MensagemAlertaEnum.SUCESSO);
		} catch (Exception e) {
			setMensagemDetalhada("", e.getMessage());
		}
		return "usuarioForm?faces-redirect=true";
	}


	public void consultarEmpresa() {
		setListaConsultaEmpresaVOs(getFacadeFactory().getEmpresaFacade().consultar(getCampoConsultaEmpresa(), getValorConsultaEmpresa(), NivelMontarDadosEnum.BASICO, getEmpresaLogado().getCodigo(), getUsuarioLogado()));
		setMensagemID("msg_dados_consultados", MensagemAlertaEnum.ATENCAO);
	}
	
	public void selecionarEmpresa() {
		EmpresaVO obj = (EmpresaVO) context().getExternalContext().getRequestMap().get("empresaItem");
		setEmpresaVO(obj);
		setMensagemID("msg_dados_selecionados", MensagemAlertaEnum.ATENCAO);
	}
	
	public void limparEmpresa() {
		setEmpresaVO(null);
	}
	
	public void limparDadosModalEmpresa() {
		getListaConsultaEmpresaVOs().clear();
	}
	
	public List<SelectItem> getListaSelectItemEmpresa() {
		List<SelectItem> itens = new ArrayList<SelectItem>(0);
		itens.add(new SelectItem("NOME", "Nome"));
		return itens;
	}
	
	public void consultarPerfilUsuario() {
		setListaConsultaPerfilUsuarioVOs(getFacadeFactory().getPerfilUsuarioFacade().consultar(getCampoConsultaPerfilUsuario(), getValorConsultaPerfilUsuario(), NivelMontarDadosEnum.BASICO, getUsuarioLogado()));
		setMensagemID("msg_dados_consultados", MensagemAlertaEnum.ATENCAO);
	}
	
	public void selecionarPerfilUsuario() {
		PerfilUsuarioVO obj = (PerfilUsuarioVO) context().getExternalContext().getRequestMap().get("perfilUsuarioItem");
		setPerfilUsuarioVO(obj);
		setMensagemID("msg_dados_selecionados", MensagemAlertaEnum.ATENCAO);
	}
	
	public void limparPerfilUsuario() {
		setPerfilUsuarioVO(null);
	}
	
	public void limparDadosModalPerfilUsuario() {
		getListaConsultaPerfilUsuarioVOs().clear();
	}
	
	public List<SelectItem> getListaSelectItemPerfilUsuario() {
		List<SelectItem> itens = new ArrayList<SelectItem>(0);
		itens.add(new SelectItem("NOME", "Nome"));
		return itens;
	}
	
	public void consultarFuncionario() {
		setListaConsultaFuncionarioVOs(getFacadeFactory().getFuncionarioFacade().consultar(getCampoConsultaFuncionario(), getValorConsultaFuncionario(), NivelMontarDadosEnum.BASICO, getUsuarioLogado()));
		setMensagemID("msg_dados_consultados", MensagemAlertaEnum.ATENCAO);
	}
	
	public void selecionarFuncionario() {
		FuncionarioVO obj = (FuncionarioVO) context().getExternalContext().getRequestMap().get("funcionarioItem");
		setFuncionarioVO(obj);
	}
	
	public void limparFuncionario() {
		setFuncionarioVO(null);
	}
	
	public List<SelectItem> getListaSelectItemFuncionarioVOs() {
		List<SelectItem> itens = new ArrayList<SelectItem>(0);
		itens.add(new SelectItem("NOME", "Nome"));
		itens.add(new SelectItem("RESPONSAVEL_REDE", "Responsável Rede"));
		return itens;
	}
	
	public void limparDadosModalFuncionario() {
		getListaConsultaFuncionarioVOs().clear();
	}

	
	public void adicionarEmpresaPerfilUsuario() {
		try {
			getFacadeFactory().getUsuarioFacade().adicionarEmpresaPerfilUsuario(getUsuarioVO().getUsuarioEmpresaPerfilUsuarioVOs(), getEmpresaVO(), getPerfilUsuarioVO(), getUsuarioLogado());
			setEmpresaVO(new EmpresaVO());
			setPerfilUsuarioVO(new PerfilUsuarioVO());
			setMensagemID("msg_dados_adicionados", MensagemAlertaEnum.ATENCAO);
		} catch (Exception e) {
			setMensagemDetalhada("", e.getMessage());
		}
	}
	
	public void removerEmpresaPerfilUsuario() {
		UsuarioEmpresaPerfilUsuarioVO obj = (UsuarioEmpresaPerfilUsuarioVO) context().getExternalContext().getRequestMap().get("empresaPerfilUsuarioItem");
		getFacadeFactory().getUsuarioFacade().removerEmpresaPerfilUsuario(getUsuarioVO().getUsuarioEmpresaPerfilUsuarioVOs(), obj, getUsuarioLogado());
	}
	
	public void editarEmpresaPerfilUsuario() {
		UsuarioEmpresaPerfilUsuarioVO obj = (UsuarioEmpresaPerfilUsuarioVO) context().getExternalContext().getRequestMap().get("empresaPerfilUsuarioItem");
		setEmpresaVO(obj.getEmpresaVO());
		setPerfilUsuarioVO(obj.getPerfilUsuarioVO());
		getFacadeFactory().getUsuarioFacade().removerEmpresaPerfilUsuario(getUsuarioVO().getUsuarioEmpresaPerfilUsuarioVOs(), obj, getUsuarioLogado());
	}
	
	public List<SelectItem> getListaSelectItemMembroVOs() {
		List<SelectItem> itens = new ArrayList<SelectItem>(0);
		itens.add(new SelectItem("NOME", "Nome"));
		return itens;
	}
	
	public UsuarioVO getUsuarioVO() {
		if (usuarioVO == null) {
			usuarioVO = new UsuarioVO();
		}
		return usuarioVO;
	}

	public void setUsuarioVO(UsuarioVO usuarioVO) {
		this.usuarioVO = usuarioVO;
	}

	public EmpresaVO getEmpresaVO() {
		if (empresaVO == null) {
			empresaVO = new EmpresaVO();
		}
		return empresaVO;
	}

	public PerfilUsuarioVO getPerfilUsuarioVO() {
		if (perfilUsuarioVO == null) {
			perfilUsuarioVO = new PerfilUsuarioVO();
		}
		return perfilUsuarioVO;
	}

	public void setEmpresaVO(EmpresaVO empresaVO) {
		this.empresaVO = empresaVO;
	}

	public void setPerfilUsuarioVO(PerfilUsuarioVO perfilUsuarioVO) {
		this.perfilUsuarioVO = perfilUsuarioVO;
	}

	public String getValorConsultaEmpresa() {
		if (valorConsultaEmpresa == null) {
			valorConsultaEmpresa = "";
		}
		return valorConsultaEmpresa;
	}

	public String getCampoConsultaEmpresa() {
		if (campoConsultaEmpresa == null) {
			campoConsultaEmpresa = "";
		}
		return campoConsultaEmpresa;
	}

	public List<EmpresaVO> getListaConsultaEmpresaVOs() {
		if (listaConsultaEmpresaVOs == null) {
			listaConsultaEmpresaVOs = new ArrayList<EmpresaVO>(0);
		}
		return listaConsultaEmpresaVOs;
	}

	public void setValorConsultaEmpresa(String valorConsultaEmpresa) {
		this.valorConsultaEmpresa = valorConsultaEmpresa;
	}

	public void setCampoConsultaEmpresa(String campoConsultaEmpresa) {
		this.campoConsultaEmpresa = campoConsultaEmpresa;
	}

	public void setListaConsultaEmpresaVOs(List<EmpresaVO> listaConsultaEmpresaVOs) {
		this.listaConsultaEmpresaVOs = listaConsultaEmpresaVOs;
	}

	public String getValorConsultaPerfilUsuario() {
		if (valorConsultaPerfilUsuario == null) {
			valorConsultaPerfilUsuario = "";
		}
		return valorConsultaPerfilUsuario;
	}

	public String getCampoConsultaPerfilUsuario() {
		if (campoConsultaPerfilUsuario == null) {
			campoConsultaPerfilUsuario = "";
		}
		return campoConsultaPerfilUsuario;
	}

	public List<PerfilUsuarioVO> getListaConsultaPerfilUsuarioVOs() {
		if (listaConsultaPerfilUsuarioVOs == null) {
			listaConsultaPerfilUsuarioVOs = new ArrayList<PerfilUsuarioVO>(0);
		}
		return listaConsultaPerfilUsuarioVOs;
	}

	public void setValorConsultaPerfilUsuario(String valorConsultaPerfilUsuario) {
		this.valorConsultaPerfilUsuario = valorConsultaPerfilUsuario;
	}

	public void setCampoConsultaPerfilUsuario(String campoConsultaPerfilUsuario) {
		this.campoConsultaPerfilUsuario = campoConsultaPerfilUsuario;
	}

	public void setListaConsultaPerfilUsuarioVOs(List<PerfilUsuarioVO> listaConsultaPerfilUsuarioVOs) {
		this.listaConsultaPerfilUsuarioVOs = listaConsultaPerfilUsuarioVOs;
	}
	
	public List<SelectItem> getListaSelectItemTipoUsuarioVOs() {
		List<SelectItem> itens = new ArrayList<SelectItem>(0);
		itens.add(new SelectItem(TipoUsuarioEnum.FUNCIONARIO, "Funcionário"));
		return itens;
	}

	public FuncionarioVO getFuncionarioVO() {
		if (funcionarioVO == null) {
			funcionarioVO = new FuncionarioVO();
		}
		return funcionarioVO;
	}

	public String getValorConsultaFuncionario() {
		if (valorConsultaFuncionario == null) {
			valorConsultaFuncionario = "";
		}
		return valorConsultaFuncionario;
	}

	public String getCampoConsultaFuncionario() {
		if (campoConsultaFuncionario == null) {
			campoConsultaFuncionario = "";
		}
		return campoConsultaFuncionario;
	}

	public List<FuncionarioVO> getListaConsultaFuncionarioVOs() {
		if (listaConsultaFuncionarioVOs == null) {
			listaConsultaFuncionarioVOs = new ArrayList<FuncionarioVO>(0);
		}
		return listaConsultaFuncionarioVOs;
	}

	public void setFuncionarioVO(FuncionarioVO funcionarioVO) {
		this.funcionarioVO = funcionarioVO;
	}

	public void setValorConsultaFuncionario(String valorConsultaFuncionario) {
		this.valorConsultaFuncionario = valorConsultaFuncionario;
	}

	public void setCampoConsultaFuncionario(String campoConsultaFuncionario) {
		this.campoConsultaFuncionario = campoConsultaFuncionario;
	}

	public void setListaConsultaFuncionarioVOs(List<FuncionarioVO> listaConsultaFuncionarioVOs) {
		this.listaConsultaFuncionarioVOs = listaConsultaFuncionarioVOs;
	}

	public Boolean getApresentarDadosFuncionario() {
		return getUsuarioVO().getTipoUsuario().equals(TipoUsuarioEnum.FUNCIONARIO);
	}

}
