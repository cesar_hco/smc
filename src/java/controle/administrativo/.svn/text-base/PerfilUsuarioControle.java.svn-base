/**
 * 
 */
package controle.administrativo;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.sun.scenario.effect.impl.state.LinearConvolveRenderState;

import controle.arquitetura.SuperControle;
import negocio.comuns.administrativo.PerfilUsuarioVO;
import negocio.comuns.administrativo.PermissaoUsuarioMenuSubModuloVO;
import negocio.comuns.administrativo.PermissaoUsuarioMenuVO;
import negocio.comuns.administrativo.enumeradores.PermissaoPerfilUsuarioEnum;
import negocio.comuns.arquitetura.enumeradores.MensagemAlertaEnum;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.comuns.utilitarias.Uteis;

/**
 * @author Carlos Eug�nio
 *
 */
@Controller("perfilUsuarioControle")
@Scope("viewScope")
@Lazy
public class PerfilUsuarioControle extends SuperControle implements Serializable {

	private static final long serialVersionUID = 1L;
	private PerfilUsuarioVO perfilUsuarioVO;
	private String nomeMenuCarregar;
	private Boolean todasPermissoes;

	public PerfilUsuarioControle() {
		super();
	}

	public String novo() {
		Uteis.removerObjetoMemoria(this);
		setPerfilUsuarioVO(new PerfilUsuarioVO());
		inicializarDadosModulo();
		setMensagemID("msg_entre_dados", MensagemAlertaEnum.ATENCAO);
		return "perfilUsuarioForm?faces-redirect=true";
	}

	public String consultar() {
		try {
			setListaConsultaVOs(getFacadeFactory().getPerfilUsuarioFacade().consultar(getCampoConsulta(), getValorConsulta(), NivelMontarDadosEnum.BASICO, getUsuarioLogado()));
			setMensagemID("msg_dados_consultados", MensagemAlertaEnum.ATENCAO);
		} catch (Exception e) {
			setMensagemDetalhada("msg_erro", e.getMessage());
		}
		return "perfilUsuarioCons?faces-redirect=true";
	}

	public String irTelaConsulta() {
		getListaConsultaVOs().clear();
		setValorConsulta("");
		setMensagemID("msg_entre_prmconsulta", MensagemAlertaEnum.ATENCAO);
		return "perfilUsuarioCons?faces-redirect=true";
	}

	public void persistir() {
		try {
			getFacadeFactory().getPerfilUsuarioFacade().persistir(getPerfilUsuarioVO(), getUsuarioLogado());
			setMensagemID("msg_dados_gravados", MensagemAlertaEnum.SUCESSO);
		} catch (Exception e) {
			setMensagemDetalhada("", e.getMessage());
		}
	}

	public String editar() {
		PerfilUsuarioVO obj = (PerfilUsuarioVO) context().getExternalContext().getRequestMap().get("perfilUsuarioItem");
		obj.setNovoObj(Boolean.FALSE);
		setPerfilUsuarioVO(obj);
		getFacadeFactory().getPerfilUsuarioFacade().carregarDados(getPerfilUsuarioVO(), NivelMontarDadosEnum.COMPLETO, getUsuarioLogado());
		editarDadosModulo();
		setMensagemID("msg_dados_selecionados", MensagemAlertaEnum.ATENCAO);
		return "perfilUsuarioForm?faces-redirect=true";
	}

	public void marcarTodasOpcoesPermissao() {
		for (PermissaoUsuarioMenuVO permissaoUsuarioMenuVO : getPerfilUsuarioVO().getPermissaoUsuarioMenuVOs()) {
			permissaoUsuarioMenuVO.setPermissaoModulo(getTodasPermissoes());
			for (PermissaoUsuarioMenuSubModuloVO permissaoUsuarioMenuSubModuloVO : permissaoUsuarioMenuVO.getPermissaoUsuarioMenuSubModuloVOs()) {
				permissaoUsuarioMenuSubModuloVO.setPermissaoSubModulo(getTodasPermissoes());
			}
		}
	}

	public List<SelectItem> getListaSelectItemVOs() {
		List<SelectItem> itens = new ArrayList<SelectItem>(0);
		itens.add(new SelectItem("NOME", "Nome"));
		return itens;
	}

	public String excluir() {
		try {
			getFacadeFactory().getPerfilUsuarioFacade().excluir(getPerfilUsuarioVO(), getUsuarioLogado());
			novo();
			setMensagemID("msg_dados_excluidos", MensagemAlertaEnum.SUCESSO);
		} catch (Exception e) {
			setMensagemDetalhada("", e.getMessage());
		}
		return "perfilUsuarioForm?faces-redirect=true";
	}

	public void inicializarDadosModulo() {
		getPerfilUsuarioVO().getPermissaoUsuarioMenuVOs().clear();
		List<PermissaoPerfilUsuarioEnum> listaModuloEnumVOs = (PermissaoPerfilUsuarioEnum.getObterListaModulo(""));
		for (PermissaoPerfilUsuarioEnum permissaoPerfilUsuarioModuloEnum : listaModuloEnumVOs) {
			PermissaoUsuarioMenuVO permissaoUsuarioMenuVO = new PermissaoUsuarioMenuVO();
			permissaoUsuarioMenuVO.setNomeModulo(permissaoPerfilUsuarioModuloEnum.toString());
			permissaoUsuarioMenuVO.setDescricaoModulo(permissaoPerfilUsuarioModuloEnum.getDescricao());
			permissaoUsuarioMenuVO.setNomeMenu(permissaoPerfilUsuarioModuloEnum.getKey());
			permissaoUsuarioMenuVO.getPermissaoUsuarioMenuSubModuloVOs().addAll(inicializarDadosSubModulo(permissaoPerfilUsuarioModuloEnum.getMenu()));
			getPerfilUsuarioVO().getPermissaoUsuarioMenuVOs().add(permissaoUsuarioMenuVO);
		}

	}

	public List<PermissaoUsuarioMenuSubModuloVO> inicializarDadosSubModulo(String nomeModulo) {
		List<PermissaoUsuarioMenuSubModuloVO> listaSubModuloMenuVOs = new ArrayList<PermissaoUsuarioMenuSubModuloVO>(0);
		List<PermissaoPerfilUsuarioEnum> listaSubModuloEnumVOs = (PermissaoPerfilUsuarioEnum.getObterListaSubModulo(nomeModulo));
		for (PermissaoPerfilUsuarioEnum permissaoPerfilEnum : listaSubModuloEnumVOs) {
			PermissaoUsuarioMenuSubModuloVO permissaoUsuarioMenuSubModuloVO = new PermissaoUsuarioMenuSubModuloVO();
			permissaoUsuarioMenuSubModuloVO.setNomeSubModulo(permissaoPerfilEnum.toString());
			permissaoUsuarioMenuSubModuloVO.setDescricaoSubModulo(permissaoPerfilEnum.getDescricao());
			permissaoUsuarioMenuSubModuloVO.setNomeMenu(permissaoPerfilEnum.getKey());
			listaSubModuloMenuVOs.add(permissaoUsuarioMenuSubModuloVO);
		}
		return listaSubModuloMenuVOs;
	}

	public void inicializarDadosModuloSelecionado() {
		for (PermissaoUsuarioMenuVO permissaoUsuarioMenuVO : getPerfilUsuarioVO().getPermissaoUsuarioMenuVOs()) {
			if (permissaoUsuarioMenuVO.getNomeModulo().toUpperCase().equals(getNomeMenuCarregar()) || getNomeMenuCarregar().equals("TODOS")) {
				permissaoUsuarioMenuVO.setApresentarModulo(true);
			} else {
				permissaoUsuarioMenuVO.setApresentarModulo(false);
			}
		}
	}

	public void editarDadosModulo() {
		List<PermissaoPerfilUsuarioEnum> listaModuloEnumVOs = (PermissaoPerfilUsuarioEnum.getObterListaModulo(""));
		for (PermissaoPerfilUsuarioEnum permissaoPerfilUsuarioEnum : listaModuloEnumVOs) {
			Boolean moduloEncontrado = false;
			for (PermissaoUsuarioMenuVO permissaoUsuarioMenuVO : getPerfilUsuarioVO().getPermissaoUsuarioMenuVOs()) {
				if (permissaoPerfilUsuarioEnum.toString().equals(permissaoUsuarioMenuVO.getNomeModulo())) {
					moduloEncontrado = true;
					break;
				}
			}
			if (!moduloEncontrado) {
				PermissaoUsuarioMenuVO permissaoUsuarioMenuVO = new PermissaoUsuarioMenuVO();
				permissaoUsuarioMenuVO.setNomeModulo(permissaoPerfilUsuarioEnum.toString());
				permissaoUsuarioMenuVO.setDescricaoModulo(permissaoPerfilUsuarioEnum.getDescricao());
				permissaoUsuarioMenuVO.getPermissaoUsuarioMenuSubModuloVOs().addAll(inicializarDadosSubModulo(permissaoPerfilUsuarioEnum.getMenu()));
				getPerfilUsuarioVO().getPermissaoUsuarioMenuVOs().add(permissaoUsuarioMenuVO);
			}
		}
	}

	public void realizarDesmarcacaoSubModuloAoDesmarcarModuloPrincipal() {
		PermissaoUsuarioMenuVO obj = (PermissaoUsuarioMenuVO) context().getExternalContext().getRequestMap().get("moduloItem");
		if (!obj.getPermissaoModulo()) {
			for (PermissaoUsuarioMenuVO permissaoUsuarioMenuVO : getPerfilUsuarioVO().getPermissaoUsuarioMenuVOs()) {
				if (obj.getNomeModulo().equals(permissaoUsuarioMenuVO.getNomeModulo())) {
					for (PermissaoUsuarioMenuSubModuloVO permissaoUsuarioMenuSubModuloVO : permissaoUsuarioMenuVO.getPermissaoUsuarioMenuSubModuloVOs()) {
						permissaoUsuarioMenuSubModuloVO.setPermissaoSubModulo(false);
					}
				}
			}
		}
	}

	public PerfilUsuarioVO getPerfilUsuarioVO() {
		if (perfilUsuarioVO == null) {
			perfilUsuarioVO = new PerfilUsuarioVO();
		}
		return perfilUsuarioVO;
	}

	public void setPerfilUsuarioVO(PerfilUsuarioVO perfilUsuarioVO) {
		this.perfilUsuarioVO = perfilUsuarioVO;
	}

	public String getNomeMenuCarregar() {
		if (nomeMenuCarregar == null) {
			nomeMenuCarregar = "";
		}
		return nomeMenuCarregar;
	}

	public void setNomeMenuCarregar(String nomeMenuCarregar) {
		this.nomeMenuCarregar = nomeMenuCarregar;
	}

	public Boolean getTodasPermissoes() {
		if (todasPermissoes == null) {
			todasPermissoes = false;
		}
		return todasPermissoes;
	}

	public void setTodasPermissoes(Boolean todasPermissoes) {
		this.todasPermissoes = todasPermissoes;
	}

}
