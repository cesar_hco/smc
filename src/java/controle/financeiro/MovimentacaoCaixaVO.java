package controle.financeiro;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import negocio.comuns.administrativo.EmpresaVO;
import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.arquitetura.SuperVO;
import negocio.comuns.financeiro.ContaCorrenteVO;
import negocio.comuns.financeiro.MovimentacaoCaixaFormaPagamentoVO;
import negocio.comuns.financeiro.enumeradores.SituacaoCaixaEnum;
import negocio.comuns.utilitarias.Uteis;

public class MovimentacaoCaixaVO extends SuperVO implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer codigo;
	private Date dataAbertura;
	private UsuarioVO responsavelAberturaVO;
	private EmpresaVO empresaVO;
	private ContaCorrenteVO contaCaixaVO;
	private BigDecimal saldoInicialCheque;
	private BigDecimal saldoInicialDinheiro;
	private BigDecimal saldoFinalCheque;
	private BigDecimal saldoFinalDinheiro;
	private Date dataFechamento;
	private UsuarioVO responsavelFechamentoVO;
	private SituacaoCaixaEnum situacaoCaixa;
	private List<MovimentacaoCaixaItemVO> movimentacaoCaixaItemVOs;
	private List<MovimentacaoCaixaFormaPagamentoVO> movimentacaoCaixaFormaPagamentosVOs;

	public MovimentacaoCaixaVO() {
		super();
	}

	public Integer getCodigo() {
		if (codigo == null) {
			codigo = 0;
		}
		return codigo;
	}

	public Date getDataAbertura() {
		if (dataAbertura == null) {
			dataAbertura = new Date();
		}
		return dataAbertura;
	}
	
	public String getDataAbertura_Apresentar() {
		return Uteis.getDataAno4Digitos(getDataAbertura());
	}

	public UsuarioVO getResponsavelAberturaVO() {
		if (responsavelAberturaVO == null) {
			responsavelAberturaVO = new UsuarioVO();
		}
		return responsavelAberturaVO;
	}

	public EmpresaVO getEmpresaVO() {
		if (empresaVO == null) {
			empresaVO = new EmpresaVO();
		}
		return empresaVO;
	}

	public ContaCorrenteVO getContaCaixaVO() {
		if (contaCaixaVO == null) {
			contaCaixaVO = new ContaCorrenteVO();
		}
		return contaCaixaVO;
	}

	public BigDecimal getSaldoInicialCheque() {
		if (saldoInicialCheque == null) {
			saldoInicialCheque = BigDecimal.ZERO;
		}
		return saldoInicialCheque;
	}

	public BigDecimal getSaldoInicialDinheiro() {
		if (saldoInicialDinheiro == null) {
			saldoInicialDinheiro = BigDecimal.ZERO;
		}
		return saldoInicialDinheiro;
	}

	public BigDecimal getSaldoFinalCheque() {
		if (saldoFinalCheque == null) {
			saldoFinalCheque = BigDecimal.ZERO;
		}
		return saldoFinalCheque;
	}

	public BigDecimal getSaldoFinalDinheiro() {
		if (saldoFinalDinheiro == null) {
			saldoFinalDinheiro = BigDecimal.ZERO;
		}
		return saldoFinalDinheiro;
	}

	public Date getDataFechamento() {
		if (dataFechamento == null) {
			dataFechamento = new Date();
		}
		return dataFechamento;
	}
	
	public String getDataFechamento_Apresentar() {
		return Uteis.getDataAno4Digitos(getDataFechamento());
	}

	public UsuarioVO getResponsavelFechamentoVO() {
		if (responsavelFechamentoVO == null) {
			responsavelFechamentoVO = new UsuarioVO();
		}
		return responsavelFechamentoVO;
	}

	public SituacaoCaixaEnum getSituacaoCaixa() {
		if (situacaoCaixa == null) {
			situacaoCaixa = SituacaoCaixaEnum.ABERTO;
		}
		return situacaoCaixa;
	}
	
	public String getSituacaoCaixa_Apresentar() {
		return getSituacaoCaixa().getValorApresentar();
	}

	public List<MovimentacaoCaixaItemVO> getMovimentacaoCaixaItemVOs() {
		if (movimentacaoCaixaItemVOs == null) {
			movimentacaoCaixaItemVOs = new ArrayList<MovimentacaoCaixaItemVO>(0);
		}
		return movimentacaoCaixaItemVOs;
	}

	public List<MovimentacaoCaixaFormaPagamentoVO> getMovimentacaoCaixaFormaPagamentosVOs() {
		if (movimentacaoCaixaFormaPagamentosVOs == null) {
			movimentacaoCaixaFormaPagamentosVOs = new ArrayList<MovimentacaoCaixaFormaPagamentoVO>(0);
		}
		return movimentacaoCaixaFormaPagamentosVOs;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public void setDataAbertura(Date dataAbertura) {
		this.dataAbertura = dataAbertura;
	}

	public void setResponsavelAberturaVO(UsuarioVO responsavelAberturaVO) {
		this.responsavelAberturaVO = responsavelAberturaVO;
	}

	public void setEmpresaVO(EmpresaVO empresaVO) {
		this.empresaVO = empresaVO;
	}

	public void setContaCaixaVO(ContaCorrenteVO contaCaixaVO) {
		this.contaCaixaVO = contaCaixaVO;
	}

	public void setSaldoInicialCheque(BigDecimal saldoInicialCheque) {
		this.saldoInicialCheque = saldoInicialCheque;
	}

	public void setSaldoInicialDinheiro(BigDecimal saldoInicialDinheiro) {
		this.saldoInicialDinheiro = saldoInicialDinheiro;
	}

	public void setSaldoFinalCheque(BigDecimal saldoFinalCheque) {
		this.saldoFinalCheque = saldoFinalCheque;
	}

	public void setSaldoFinalDinheiro(BigDecimal saldoFinalDinheiro) {
		this.saldoFinalDinheiro = saldoFinalDinheiro;
	}

	public void setDataFechamento(Date dataFechamento) {
		this.dataFechamento = dataFechamento;
	}

	public void setResponsavelFechamentoVO(UsuarioVO responsavelFechamentoVO) {
		this.responsavelFechamentoVO = responsavelFechamentoVO;
	}

	public void setSituacaoCaixa(SituacaoCaixaEnum situacaoCaixa) {
		this.situacaoCaixa = situacaoCaixa;
	}

	public void setMovimentacaoCaixaItemVOs(List<MovimentacaoCaixaItemVO> movimentacaoCaixaItemVOs) {
		this.movimentacaoCaixaItemVOs = movimentacaoCaixaItemVOs;
	}

	public void setMovimentacaoCaixaFormaPagamentosVOs(List<MovimentacaoCaixaFormaPagamentoVO> movimentacaoCaixaFormaPagamentosVOs) {
		this.movimentacaoCaixaFormaPagamentosVOs = movimentacaoCaixaFormaPagamentosVOs;
	}
	
}
