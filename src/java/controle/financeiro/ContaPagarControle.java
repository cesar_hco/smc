package controle.financeiro;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.faces.model.SelectItem;

import negocio.comuns.arquitetura.enumeradores.MensagemAlertaEnum;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.comuns.basico.CidadeVO;
import negocio.comuns.basico.FornecedorVO;
import negocio.comuns.financeiro.CategoriaDespesaVO;
import negocio.comuns.financeiro.ContaPagarVO;
import negocio.comuns.utilitarias.Uteis;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import controle.arquitetura.SuperControle;

@Controller("contaPagarControle")
@Scope("viewScope")
@Lazy
public class ContaPagarControle extends SuperControle implements Serializable {

	private static final long serialVersionUID = 1L;
	private ContaPagarVO contaPagarVO;
	private String campoConsultaFornecedor;
	private String valorConsultaFornecedor;
	private List<FornecedorVO> listaConsultaFornecedorVOs;
	private List<SelectItem> listaSelectItemCategoriaDespesaVOs;
	private FornecedorVO fornecedorVO;
	private String valorConsultaCidade;
	private String campoConsultaCidade;
	private List<CidadeVO> listaConsultaCidadeVOs;
	private BigDecimal valorTotalContaPagar;
	
	private CategoriaDespesaVO categoriaDespesaVO;
	private List<SelectItem> listaSelectItemCategoriaDespesaPrincipal;

	public ContaPagarControle() {
		super();
	}
	
	public String novo() {
		Uteis.removerObjetoMemoria(this);
		setContaPagarVO(new ContaPagarVO());
		montarListaSelectItemCategoriaDespesa();
		setMensagemID("msg_entre_dados", MensagemAlertaEnum.ATENCAO);
		return "contaPagarForm?faces-redirect=true";
	}
	
	public String consultar() {
		try {
			setListaConsultaVOs(getFacadeFactory().getContaPagarFacade().consultar(getCampoConsulta(), getValorConsulta(), NivelMontarDadosEnum.BASICO, getUsuarioLogado()));
			calcularValorTotal();
			setMensagemID("msg_dados_consultados", MensagemAlertaEnum.ATENCAO);
		} catch (Exception e) {
			setMensagemDetalhada("msg_erro", e.getMessage());
		}
		return "contaPagarCons?faces-redirect=true";
	}
	
	public void calcularValorTotal() {
		BigDecimal valorTotal = BigDecimal.ZERO; 
		for (Iterator i = getListaConsultaVOs().iterator(); i.hasNext();) {
			ContaPagarVO obj = (ContaPagarVO) i.next();
			valorTotal = valorTotal.add(obj.getValor());
		}
		setValorTotalContaPagar(valorTotal);
	}
	
	public String irTelaConsulta() {
		getListaConsultaVOs().clear();
		setValorConsulta("");
		setMensagemID("msg_entre_prmconsulta", MensagemAlertaEnum.ATENCAO);
		return "contaPagarCons?faces-redirect=true";
	}
	
	public void persistir() {
		try {
			getFacadeFactory().getContaPagarFacade().persistir(getContaPagarVO(), getUsuarioLogado());
			setMensagemID("msg_dados_gravados", MensagemAlertaEnum.SUCESSO);
		} catch (Exception e) {
			setMensagemDetalhada("", e.getMessage());
		}
	}
	
	public String editar() {
		ContaPagarVO obj = (ContaPagarVO) context().getExternalContext().getRequestMap().get("contaPagarItem");
		getFacadeFactory().getContaPagarFacade().carregarDados(obj, NivelMontarDadosEnum.COMPLETO, getUsuarioLogado());
		obj.setNovoObj(Boolean.FALSE);
		setContaPagarVO(obj);
		montarListaSelectItemCategoriaDespesa();
		setMensagemID("msg_dados_selecionados", MensagemAlertaEnum.ATENCAO);
		return "contaPagarForm?faces-redirect=true";
	}
	
	public List<SelectItem> getListaSelectItemVOs() {
		List<SelectItem> itens = new ArrayList<SelectItem>(0);
		itens.add(new SelectItem("FORNECEDOR", "Fornecedor"));
		itens.add(new SelectItem("NR_DOCUMENTO", "Nr. Documento"));
		return itens;
	}
	
	public String excluir() {
		try {
			getFacadeFactory().getContaPagarFacade().excluir(getContaPagarVO(), getUsuarioLogado());
			novo();
			setMensagemID("msg_dados_excluidos", MensagemAlertaEnum.SUCESSO);
		} catch (Exception e) {
			setMensagemDetalhada("", e.getMessage());
		}
		return "contaPagarForm?faces-redirect=true";
	}
	
	public void consultarFornecedor() {
		try {
			setListaConsultaFornecedorVOs(getFacadeFactory().getFornecedorFacade().consultar(getCampoConsultaFornecedor(), getValorConsultaFornecedor(), NivelMontarDadosEnum.BASICO, getUsuarioLogado()));
			setMensagemID("msg_dados_consultados", MensagemAlertaEnum.ATENCAO);
		} catch (Exception e) {
			setMensagemDetalhada("", e.getMessage());
		}
	}
	
	public void selecionarFornecedor() {
		FornecedorVO obj = (FornecedorVO) context().getExternalContext().getRequestMap().get("fornecedorItem");
		getContaPagarVO().setFornecedorVO(obj);
		setMensagemID("msg_dados_selecionados", MensagemAlertaEnum.ATENCAO);
	}
	
	public void limparFornecedor() {
		getContaPagarVO().setFornecedorVO(null);
	}
	
	public void limparDadosConsultaModalFornecedor() {
		setListaConsultaFornecedorVOs(null);
	}
	
	public String realizarPagamentoContaPagarTelaConsulta() {
		ContaPagarVO obj = (ContaPagarVO) context().getExternalContext().getRequestMap().get("contaPagarItem");
		if (obj != null && !obj.getCodigo().equals(0)) {
			getFacadeFactory().getContaPagarFacade().carregarDados(obj, NivelMontarDadosEnum.TODOS, getUsuarioLogado());
			context().getExternalContext().getSessionMap().put("pagarContaPagar", obj);
		}
		return "pagamentoForm?faces-redirect=true";
	}
	
	public String realizarPagamentoContaPagar() {
		if (getContaPagarVO() != null && !getContaPagarVO().getCodigo().equals(0)) {
			getFacadeFactory().getContaPagarFacade().carregarDados(getContaPagarVO(), NivelMontarDadosEnum.TODOS, getUsuarioLogado());
			context().getExternalContext().getSessionMap().put("pagarContaPagar", getContaPagarVO());
		}
		return "pagamentoForm?faces-redirect=true";
	}
	
	public void consultarCidade() {
		try {
			setListaConsultaCidadeVOs(getFacadeFactory().getCidadeFacade().consultaRapidaPorNome(getValorConsultaCidade(), false, getUsuarioLogado()));
			setMensagemID("msg_dados_consultados", MensagemAlertaEnum.ATENCAO);
		} catch (Exception e) {
			setMensagemDetalhada("", e.getMessage());
		}
	}
	
	public void selecionarCidade() {
		CidadeVO obj = (CidadeVO) context().getExternalContext().getRequestMap().get("cidadeItem");
		getFornecedorVO().setCidadeVO(obj);
		setMensagemID("msg_dados_selecionados", MensagemAlertaEnum.ATENCAO);
	}
	
	public void limparCidade() {
		getFornecedorVO().setCidadeVO(null);
	}
	
	public void limparDadosConsultaModalCidade() {
		setListaConsultaCidadeVOs(null);
	}
	
	public void realizarCalculoValorTotal() {
//		getContaPagarVO().calcularValorTotal();
	}
	
	public void realizarCalculoValorMulta() {
//		getFacadeFactory().getContaPagarFacade().realizarCalculoValorMulta(getContaPagarVO(), getUsuarioLogado());
//		getContaPagarVO().calcularValorTotal();
	}
	
	public void realizarCalculoValorJuro() {
//		getFacadeFactory().getContaPagarFacade().realizarCalculoValorJuro(getContaPagarVO(), getUsuarioLogado());
//		getContaPagarVO().calcularValorTotal();
	}

	public ContaPagarVO getContaPagarVO() {
		if (contaPagarVO == null) {
			contaPagarVO = new ContaPagarVO();
		}
		return contaPagarVO;
	}

	public void setContaPagarVO(ContaPagarVO contaPagarVO) {
		this.contaPagarVO = contaPagarVO;
	}

	public String getCampoConsultaFornecedor() {
		if (campoConsultaFornecedor == null) {
			campoConsultaFornecedor = "";
		}
		return campoConsultaFornecedor;
	}

	public String getValorConsultaFornecedor() {
		if (valorConsultaFornecedor == null) {
			valorConsultaFornecedor = "";
		}
		return valorConsultaFornecedor;
	}


	public void setCampoConsultaFornecedor(String campoConsultaFornecedor) {
		this.campoConsultaFornecedor = campoConsultaFornecedor;
	}

	public void setValorConsultaFornecedor(String valorConsultaFornecedor) {
		this.valorConsultaFornecedor = valorConsultaFornecedor;
	}


	public List<SelectItem> getListaSelectItemFornecedorVOs() {
		List<SelectItem> itens = new ArrayList<SelectItem>(0);
		itens.add(new SelectItem("NOME", "Nome"));
		return itens;
	}
	
	public void montarListaSelectItemCategoriaDespesa() {
		try {
			List<CategoriaDespesaVO> resultadoConsulta = getFacadeFactory().getCategoriaDespesaFacade().consultarPorDescricao("", false, NivelMontarDadosEnum.BASICO, getUsuarioLogado());
			List<SelectItem> objs = new ArrayList<SelectItem>(0);
			objs.add(new SelectItem(0, ""));
			for (CategoriaDespesaVO obj : resultadoConsulta) {
				objs.add(new SelectItem(obj.getCodigo(), obj.getDescricao()));
			}
			setListaSelectItemCategoriaDespesaVOs(objs);
		} catch (Exception e) {
			throw e;
		}
	}
	
	public void inicializarDadosCadastroFornecedor() {
		setFornecedorVO(new FornecedorVO());
	}
	
	public void persistirFornecedor() {
		try {
			getFacadeFactory().getFornecedorFacade().persistir(getFornecedorVO(), getUsuarioLogado());
			setMensagemID("msg_dados_selecionados", MensagemAlertaEnum.ATENCAO);
		} catch (Exception e) {
			setMensagemDetalhada("msg_erro", e.getMessage());
		}
	}
	
	public void inicializarDadosCadastroCategoriaDespesa() {
		setCategoriaDespesaVO(new CategoriaDespesaVO());
		montarListaSelectItemCategoriaDespesaPrincipal();
	}
	
	public void persistirCategoriaDespesa() {
		try {
			getFacadeFactory().getCategoriaDespesaFacade().persistir(getCategoriaDespesaVO(), getUsuarioLogado());
			montarListaSelectItemCategoriaDespesa();
			setMensagemID("msg_dados_selecionados", MensagemAlertaEnum.ATENCAO);
		} catch (Exception e) {
			setMensagemDetalhada("msg_erro", e.getMessage());
		}
	}
	
	public void montarListaSelectItemCategoriaDespesaPrincipal() {
		try {
			List<CategoriaDespesaVO> resultadoConsulta = getFacadeFactory().getCategoriaDespesaFacade().consultarCategoriaPrincipalPorDescricao("", getCategoriaDespesaVO().getCodigo(), NivelMontarDadosEnum.BASICO, getUsuarioLogado());
			List<SelectItem> objs = new ArrayList<SelectItem>(0);
			objs.add(new SelectItem(0, ""));
			for (CategoriaDespesaVO obj : resultadoConsulta) {
				objs.add(new SelectItem(obj.getCodigo(), obj.getDescricao()));
			}
			setListaSelectItemCategoriaDespesaPrincipal(objs);
		} catch (Exception e) {
			throw e;
		}
	}
	
	public List<SelectItem> getListaSelectItemCategoriaDespesaPrincipal() {
		if (listaSelectItemCategoriaDespesaPrincipal == null) {
			listaSelectItemCategoriaDespesaPrincipal = new ArrayList<SelectItem>(0);
		}
		return listaSelectItemCategoriaDespesaPrincipal;
	}

	public void setListaSelectItemCategoriaDespesaPrincipal(List<SelectItem> listaSelectItemCategoriaDespesaPrincipal) {
		this.listaSelectItemCategoriaDespesaPrincipal = listaSelectItemCategoriaDespesaPrincipal;
	}


	public List<FornecedorVO> getListaConsultaFornecedorVOs() {
		if (listaConsultaFornecedorVOs == null) {
			listaConsultaFornecedorVOs = new ArrayList<FornecedorVO>(0);
		}
		return listaConsultaFornecedorVOs;
	}

	public void setListaConsultaFornecedorVOs(List<FornecedorVO> listaConsultaFornecedorVOs) {
		this.listaConsultaFornecedorVOs = listaConsultaFornecedorVOs;
	}

	public List<SelectItem> getListaSelectItemCategoriaDespesaVOs() {
		if (listaSelectItemCategoriaDespesaVOs == null) {
			listaSelectItemCategoriaDespesaVOs = new ArrayList<SelectItem>(0);
		}
		return listaSelectItemCategoriaDespesaVOs;
	}

	public void setListaSelectItemCategoriaDespesaVOs(List<SelectItem> listaSelectItemCategoriaDespesaVOs) {
		this.listaSelectItemCategoriaDespesaVOs = listaSelectItemCategoriaDespesaVOs;
	}

	public FornecedorVO getFornecedorVO() {
		if (fornecedorVO == null) {
			fornecedorVO = new FornecedorVO();
		}
		return fornecedorVO;
	}

	public void setFornecedorVO(FornecedorVO fornecedorVO) {
		this.fornecedorVO = fornecedorVO;
	}

	public String getValorConsultaCidade() {
		if (valorConsultaCidade == null) {
			valorConsultaCidade = "";
		}
		return valorConsultaCidade;
	}

	public String getCampoConsultaCidade() {
		if (campoConsultaCidade == null) {
			campoConsultaCidade = "";
		}
		return campoConsultaCidade;
	}

	public List<CidadeVO> getListaConsultaCidadeVOs() {
		if (listaConsultaCidadeVOs == null) {
			listaConsultaCidadeVOs = new ArrayList<CidadeVO>(0);
		}
		return listaConsultaCidadeVOs;
	}

	public void setValorConsultaCidade(String valorConsultaCidade) {
		this.valorConsultaCidade = valorConsultaCidade;
	}

	public void setCampoConsultaCidade(String campoConsultaCidade) {
		this.campoConsultaCidade = campoConsultaCidade;
	}

	public void setListaConsultaCidadeVOs(List<CidadeVO> listaConsultaCidadeVOs) {
		this.listaConsultaCidadeVOs = listaConsultaCidadeVOs;
	}

	public BigDecimal getValorTotalContaPagar() {
		if (valorTotalContaPagar == null) {
			valorTotalContaPagar = BigDecimal.ZERO;
		}
		return valorTotalContaPagar;
	}

	public void setValorTotalContaPagar(BigDecimal valorTotalContaPagar) {
		this.valorTotalContaPagar = valorTotalContaPagar;
	}

	public CategoriaDespesaVO getCategoriaDespesaVO() {
		if (categoriaDespesaVO == null) {
			categoriaDespesaVO = new CategoriaDespesaVO();
		}
		return categoriaDespesaVO;
	}

	public void setCategoriaDespesaVO(CategoriaDespesaVO categoriaDespesaVO) {
		this.categoriaDespesaVO = categoriaDespesaVO;
	}



}
