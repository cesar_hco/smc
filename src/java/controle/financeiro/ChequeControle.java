package controle.financeiro;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import controle.arquitetura.SuperControle;
import negocio.comuns.arquitetura.enumeradores.MensagemAlertaEnum;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.comuns.financeiro.ChequeVO;
import negocio.comuns.utilitarias.Uteis;

@Controller("chequeControle")
@Scope("viewScope")
@Lazy
public class ChequeControle extends SuperControle implements Serializable {

	private static final long serialVersionUID = 1L;
	private ChequeVO chequeVO;

	public ChequeControle() {
		super();
	}
	
	public String novo() {
		Uteis.removerObjetoMemoria(this);
		setChequeVO(new ChequeVO());
		setMensagemID("msg_entre_dados", MensagemAlertaEnum.ATENCAO);
		return "chequeForm?faces-redirect=true";
	}
	
	public String consultar() {
		try {
			setListaConsultaVOs(getFacadeFactory().getChequeFacade().consultar(getCampoConsulta(), getValorConsulta(), NivelMontarDadosEnum.BASICO, getUsuarioLogado()));
			setMensagemID("msg_dados_consultados", MensagemAlertaEnum.ATENCAO);
		} catch (Exception e) {
			setMensagemDetalhada("msg_erro", e.getMessage());
		}
		return "chequeCons?faces-redirect=true";
	}
	
	public String irTelaConsulta() {
		getListaConsultaVOs().clear();
		setValorConsulta("");
		setMensagemID("msg_entre_prmconsulta", MensagemAlertaEnum.ATENCAO);
		return "chequeCons?faces-redirect=true";
	}
	
	public void persistir() {
		try {
			getFacadeFactory().getChequeFacade().persistir(getChequeVO(), getUsuarioLogado());
			setMensagemID("msg_dados_gravados", MensagemAlertaEnum.SUCESSO);
		} catch (Exception e) {
			setMensagemDetalhada("", e.getMessage());
		}
	}
	
	public String editar() {
		ChequeVO obj = (ChequeVO) context().getExternalContext().getRequestMap().get("chequeItem");
		obj.setNovoObj(Boolean.FALSE);
		setChequeVO(obj);
		setMensagemID("msg_dados_selecionados", MensagemAlertaEnum.ATENCAO);
		return "chequeForm?faces-redirect=true";
	}
	
	public List<SelectItem> getListaSelectItemVOs() {
		List<SelectItem> itens = new ArrayList<SelectItem>(0);
		itens.add(new SelectItem("NUMERO_CONTA_CORRENTE", "N�mero Conta Corrente"));
		itens.add(new SelectItem("DESCRICAO", "Descri��o"));
		return itens;
	}
	
	public String excluir() {
		try {
			getFacadeFactory().getChequeFacade().excluir(getChequeVO(), getUsuarioLogado());
			setMensagemID("msg_dados_excluidos", MensagemAlertaEnum.SUCESSO);
		} catch (Exception e) {
			setMensagemDetalhada("", e.getMessage());
		}
		return "chequeForm?faces-redirect=true";
	}

	public ChequeVO getChequeVO() {
		if (chequeVO == null) {
			chequeVO = new ChequeVO();
		}
		return chequeVO;
	}

	public void setChequeVO(ChequeVO chequeVO) {
		this.chequeVO = chequeVO;
	}


}
