package controle.financeiro;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import controle.arquitetura.SuperControle;
import negocio.comuns.arquitetura.enumeradores.MensagemAlertaEnum;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.comuns.financeiro.CategoriaDespesaVO;
import negocio.comuns.utilitarias.Uteis;

@Controller("categoriaDespesaControle")
@Scope("viewScope")
@Lazy
public class CategoriaDespesaControle extends SuperControle implements Serializable {

	private static final long serialVersionUID = 1L;
	private CategoriaDespesaVO categoriaDespesaVO;
	private List<SelectItem> listaSelectItemCategoriaDespesaPrincipal;

	public CategoriaDespesaControle() {
		super();
	}
	
	public String novo() {
		Uteis.removerObjetoMemoria(this);
		setCategoriaDespesaVO(new CategoriaDespesaVO());
		montarListaSelectItemCategoriaDespesaPrincipal();
		setMensagemID("msg_entre_dados", MensagemAlertaEnum.ATENCAO);
		return "categoriaDespesaForm?faces-redirect=true";
	}
	
	public String consultar() {
		try {
			setListaConsultaVOs(getFacadeFactory().getCategoriaDespesaFacade().consultar(getCampoConsulta(), getValorConsulta(), NivelMontarDadosEnum.BASICO, getUsuarioLogado()));
			setMensagemID("msg_dados_consultados", MensagemAlertaEnum.ATENCAO);
		} catch (Exception e) {
			setMensagemDetalhada("msg_erro", e.getMessage());
		}
		return "categoriaDespesaCons?faces-redirect=true";
	}
	
	public String irTelaConsulta() {
		getListaConsultaVOs().clear();
		setValorConsulta("");
		setMensagemID("msg_entre_prmconsulta", MensagemAlertaEnum.ATENCAO);
		return "categoriaDespesaCons?faces-redirect=true";
	}
	
	public void persistir() {
		try {
			getFacadeFactory().getCategoriaDespesaFacade().persistir(getCategoriaDespesaVO(), getUsuarioLogado());
			setMensagemID("msg_dados_gravados", MensagemAlertaEnum.SUCESSO);
		} catch (Exception e) {
			setMensagemDetalhada("", e.getMessage());
		}
	}
	
	public String editar() {
		CategoriaDespesaVO obj = (CategoriaDespesaVO) context().getExternalContext().getRequestMap().get("categoriaDespesaItem");
		getFacadeFactory().getCategoriaDespesaFacade().carregarDados(obj, NivelMontarDadosEnum.COMPLETO, getUsuarioLogado());
		obj.setNovoObj(Boolean.FALSE);
		setCategoriaDespesaVO(obj);
		montarListaSelectItemCategoriaDespesaPrincipal();
		setMensagemID("msg_dados_selecionados", MensagemAlertaEnum.ATENCAO);
		return "categoriaDespesaForm?faces-redirect=true";
	}
	
	public List<SelectItem> getListaSelectItemVOs() {
		List<SelectItem> itens = new ArrayList<SelectItem>(0);
		itens.add(new SelectItem("DESCRICAO", "Descri��o"));
		itens.add(new SelectItem("CATEGORIA_DESPESA_PRINCIPAL", "Categoria Despesa Principal"));
		itens.add(new SelectItem("SUB_CATEGORIA_POR_CATEGORIA_DESPESA_PRINCIPAL", "Sub Categoria Por Categoria Despesa Principal"));
		return itens;
	}
	
	public String excluir() {
		try {
			getFacadeFactory().getCategoriaDespesaFacade().excluir(getCategoriaDespesaVO(), getUsuarioLogado());
			setMensagemID("msg_dados_excluidos", MensagemAlertaEnum.SUCESSO);
		} catch (Exception e) {
			setMensagemDetalhada("", e.getMessage());
		}
		return "categoriaDespesaForm?faces-redirect=true";
	}
	
	public void montarListaSelectItemCategoriaDespesaPrincipal() {
		try {
			List<CategoriaDespesaVO> resultadoConsulta = getFacadeFactory().getCategoriaDespesaFacade().consultarCategoriaPrincipalPorDescricao("", getCategoriaDespesaVO().getCodigo(), NivelMontarDadosEnum.BASICO, getUsuarioLogado());
			List<SelectItem> objs = new ArrayList<SelectItem>(0);
			objs.add(new SelectItem(0, ""));
			for (CategoriaDespesaVO obj : resultadoConsulta) {
				objs.add(new SelectItem(obj.getCodigo(), obj.getDescricao()));
			}
			setListaSelectItemCategoriaDespesaPrincipal(objs);
		} catch (Exception e) {
			throw e;
		}
	}

	public CategoriaDespesaVO getCategoriaDespesaVO() {
		if (categoriaDespesaVO == null) {
			categoriaDespesaVO = new CategoriaDespesaVO();
		}
		return categoriaDespesaVO;
	}

	public void setCategoriaDespesaVO(CategoriaDespesaVO categoriaDespesaVO) {
		this.categoriaDespesaVO = categoriaDespesaVO;
	}

	public List<SelectItem> getListaSelectItemCategoriaDespesaPrincipal() {
		if (listaSelectItemCategoriaDespesaPrincipal == null) {
			listaSelectItemCategoriaDespesaPrincipal = new ArrayList<SelectItem>(0);
		}
		return listaSelectItemCategoriaDespesaPrincipal;
	}

	public void setListaSelectItemCategoriaDespesaPrincipal(List<SelectItem> listaSelectItemCategoriaDespesaPrincipal) {
		this.listaSelectItemCategoriaDespesaPrincipal = listaSelectItemCategoriaDespesaPrincipal;
	}


}
