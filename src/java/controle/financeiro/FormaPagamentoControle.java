package controle.financeiro;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import controle.arquitetura.SuperControle;
import negocio.comuns.arquitetura.enumeradores.MensagemAlertaEnum;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.comuns.financeiro.FormaPagamentoVO;
import negocio.comuns.financeiro.enumeradores.TipoFormaPagamentoEnum;
import negocio.comuns.utilitarias.Uteis;

@Controller("formaPagamentoControle")
@Scope("viewScope")
@Lazy
public class FormaPagamentoControle extends SuperControle implements Serializable {

	private static final long serialVersionUID = 1L;
	private FormaPagamentoVO formaPagamentoVO;

	public FormaPagamentoControle() {
		super();
	}
	
	public String novo() {
		Uteis.removerObjetoMemoria(this);
		setFormaPagamentoVO(new FormaPagamentoVO());
		setMensagemID("msg_entre_dados", MensagemAlertaEnum.ATENCAO);
		return "formaPagamentoForm?faces-redirect=true";
	}
	
	public String consultar() {
		try {
			setListaConsultaVOs(getFacadeFactory().getFormaPagamentoFacade().consultar(getCampoConsulta(), getValorConsulta(), NivelMontarDadosEnum.BASICO, getUsuarioLogado()));
			setMensagemID("msg_dados_consultados", MensagemAlertaEnum.ATENCAO);
		} catch (Exception e) {
			setMensagemDetalhada("msg_erro", e.getMessage());
		}
		return "formaPagamentoCons?faces-redirect=true";
	}
	
	public String irTelaConsulta() {
		getListaConsultaVOs().clear();
		setValorConsulta("");
		setMensagemID("msg_entre_prmconsulta", MensagemAlertaEnum.ATENCAO);
		return "formaPagamentoCons?faces-redirect=true";
	}
	
	public void persistir() {
		try {
			getFacadeFactory().getFormaPagamentoFacade().persistir(getFormaPagamentoVO(), getUsuarioLogado());
			setMensagemID("msg_dados_gravados", MensagemAlertaEnum.SUCESSO);
		} catch (Exception e) {
			setMensagemDetalhada("", e.getMessage());
		}
	}
	
	public String editar() {
		FormaPagamentoVO obj = (FormaPagamentoVO) context().getExternalContext().getRequestMap().get("formaPagamentoItem");
		obj.setNovoObj(Boolean.FALSE);
		setFormaPagamentoVO(obj);
		setMensagemID("msg_dados_selecionados", MensagemAlertaEnum.ATENCAO);
		return "formaPagamentoForm?faces-redirect=true";
	}
	
	public List<SelectItem> getListaSelectItemVOs() {
		List<SelectItem> itens = new ArrayList<SelectItem>(0);
		itens.add(new SelectItem("NOME", "Nome"));
		return itens;
	}
	
	public String excluir() {
		try {
			getFacadeFactory().getFormaPagamentoFacade().excluir(getFormaPagamentoVO(), getUsuarioLogado());
			setMensagemID("msg_dados_excluidos", MensagemAlertaEnum.SUCESSO);
		} catch (Exception e) {
			setMensagemDetalhada("", e.getMessage());
		}
		return "formaPagamentoForm?faces-redirect=true";
	}

	public FormaPagamentoVO getFormaPagamentoVO() {
		if (formaPagamentoVO == null) {
			formaPagamentoVO = new FormaPagamentoVO();
		}
		return formaPagamentoVO;
	}

	public void setFormaPagamentoVO(FormaPagamentoVO formaPagamentoVO) {
		this.formaPagamentoVO = formaPagamentoVO;
	}

	public List<SelectItem> getListaSelectItemFormaPagamentoVOs() {
		return TipoFormaPagamentoEnum.getListaSelectItemFormPagamentoVOs(true);
	}

}
