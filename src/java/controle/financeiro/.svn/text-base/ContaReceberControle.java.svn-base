package controle.financeiro;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import controle.arquitetura.SuperControle;
import negocio.comuns.arquitetura.enumeradores.MensagemAlertaEnum;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.comuns.basico.ClienteVO;
import negocio.comuns.financeiro.ContaCorrenteVO;
import negocio.comuns.financeiro.ContaReceberVO;
import negocio.comuns.financeiro.enumeradores.TipoDescontoEnum;
import negocio.comuns.utilitarias.Uteis;

@Controller("contaReceberControle")
@Scope("viewScope")
@Lazy
public class ContaReceberControle extends SuperControle implements Serializable {

	private static final long serialVersionUID = 1L;
	private ContaReceberVO contaReceberVO;
	private String valorConsultaCliente;
	private String campoConsultaCliente;
	private List<ClienteVO> listaConsultaClienteVOs;
	private List<SelectItem> listaSelectItemContaCorrenteVOs;

	public ContaReceberControle() {
		super();
	}
	
	public String novo() {
		Uteis.removerObjetoMemoria(this);
		setContaReceberVO(new ContaReceberVO());
		montarListaSelectItemContaCorrente();
		setMensagemID("msg_entre_dados", MensagemAlertaEnum.ATENCAO);
		return "contaReceberForm?faces-redirect=true";
	}
	
	public String consultar() {
		try {
			setListaConsultaVOs(getFacadeFactory().getContaReceberFacade().consultar(getCampoConsulta(), getValorConsulta(), getUsuarioLogado()));
			setMensagemID("msg_dados_consultados", MensagemAlertaEnum.ATENCAO);
		} catch (Exception e) {
			setMensagemDetalhada("msg_erro", e.getMessage());
		}
		return "contaReceberCons?faces-redirect=true";
	}
	
	public String irTelaConsulta() {
		getListaConsultaVOs().clear();
		setValorConsulta("");
		setMensagemID("msg_entre_prmconsulta", MensagemAlertaEnum.ATENCAO);
		return "contaReceberCons?faces-redirect=true";
	}
	
	public void persistir() {
		try {
			getFacadeFactory().getContaReceberFacade().persistir(getContaReceberVO(), getUsuarioLogado());
			setMensagemID("msg_dados_gravados", MensagemAlertaEnum.SUCESSO);
		} catch (Exception e) {
			setMensagemDetalhada("", e.getMessage());
		}
	}
	
	public String editar() {
		ContaReceberVO obj = (ContaReceberVO) context().getExternalContext().getRequestMap().get("contaReceberItem");
		setContaReceberVO(obj);
		getFacadeFactory().getContaReceberFacade().carregarDados(getContaReceberVO(), NivelMontarDadosEnum.COMPLETO, getUsuarioLogado());
		montarListaSelectItemContaCorrente();
		obj.setNovoObj(Boolean.FALSE);
		setMensagemID("msg_dados_selecionados", MensagemAlertaEnum.ATENCAO);
		return "contaReceberForm?faces-redirect=true";
	}
	
	public List<SelectItem> getListaSelectItemVOs() {
		List<SelectItem> itens = new ArrayList<SelectItem>(0);
		itens.add(new SelectItem("NOME_CLIENTE", "Cliente"));
		itens.add(new SelectItem("CODIGO", "C�digo"));
		return itens;
	}
	
	public String excluir() {
		try {
			getFacadeFactory().getContaReceberFacade().excluir(getContaReceberVO(), getUsuarioLogado());
			setMensagemID("msg_dados_excluidos", MensagemAlertaEnum.SUCESSO);
		} catch (Exception e) {
			setMensagemDetalhada("", e.getMessage());
		}
		return "contaReceberForm?faces-redirect=true";
	}
	
	public void consultarCliente() {
		try {
			setListaConsultaClienteVOs(getFacadeFactory().getClienteFacade().consultar(getCampoConsultaCliente(), getValorConsultaCliente(), NivelMontarDadosEnum.BASICO, getUsuarioLogado()));
			setMensagemID("msg_dados_consultados", MensagemAlertaEnum.ATENCAO);
		} catch (Exception e) {
			setMensagemDetalhada("", e.getMessage());
		}
	}
	
	public void selecionarCliente() {
		ClienteVO obj = (ClienteVO) context().getExternalContext().getRequestMap().get("clienteItem");
		getContaReceberVO().setClienteVO(obj);
		setMensagemID("msg_dados_selecionados", MensagemAlertaEnum.ATENCAO);
	}
	
	public void limparCliente() {
		getContaReceberVO().setClienteVO(null);
	}
	
	public void limparDadosConsultaModalCliente() {
		setListaConsultaClienteVOs(null);
	}
	
	public List<SelectItem> getListaSelectItemClienteVOs() {
		List<SelectItem> itens = new ArrayList<SelectItem>(0);
		itens.add(new SelectItem("NOME", "Nome"));
		return itens;
	}


	public ContaReceberVO getContaReceberVO() {
		if (contaReceberVO == null) {
			contaReceberVO = new ContaReceberVO();
		}
		return contaReceberVO;
	}

	public void setContaReceberVO(ContaReceberVO contaReceberVO) {
		this.contaReceberVO = contaReceberVO;
	}

	public String getValorConsultaCliente() {
		if (valorConsultaCliente == null) {
			valorConsultaCliente = "";
		}
		return valorConsultaCliente;
	}

	public void setValorConsultaCliente(String valorConsultaCliente) {
		this.valorConsultaCliente = valorConsultaCliente;
	}

	public String getCampoConsultaCliente() {
		if (campoConsultaCliente == null) {
			campoConsultaCliente = "";
		}
		return campoConsultaCliente;
	}

	public void setCampoConsultaCliente(String campoConsultaCliente) {
		this.campoConsultaCliente = campoConsultaCliente;
	}

	public List<ClienteVO> getListaConsultaClienteVOs() {
		if (listaConsultaClienteVOs == null) {
			listaConsultaClienteVOs = new ArrayList<ClienteVO>(0);
		}
		return listaConsultaClienteVOs;
	}

	public void setListaConsultaClienteVOs(List<ClienteVO> listaConsultaClienteVOs) {
		this.listaConsultaClienteVOs = listaConsultaClienteVOs;
	}

	public List<SelectItem> getListaSelectItemContaCorrenteVOs() {
		if (listaSelectItemContaCorrenteVOs == null) {
			listaSelectItemContaCorrenteVOs = new ArrayList<SelectItem>(0);
		}
		return listaSelectItemContaCorrenteVOs;
	}

	public void setListaSelectItemContaCorrenteVOs(List<SelectItem> listaSelectItemContaCorrenteVOs) {
		this.listaSelectItemContaCorrenteVOs = listaSelectItemContaCorrenteVOs;
	}

	public void montarListaSelectItemContaCorrente() {
		List<ContaCorrenteVO> listaContaCorrenteVOs = getFacadeFactory().getContaCorrenteFacade().consultarContaCorrente(NivelMontarDadosEnum.BASICO, false, getUsuarioLogado());
		List<SelectItem> itens = new ArrayList<SelectItem>(0);
		itens.add(new SelectItem("", ""));
		for (ContaCorrenteVO contaCorrenteVO : listaContaCorrenteVOs) {
			itens.add(new SelectItem(contaCorrenteVO.getCodigo(), contaCorrenteVO.getNumero() + " / " + contaCorrenteVO.getAgencia() + " - " + contaCorrenteVO.getBanco()));
		}
		setListaSelectItemContaCorrenteVOs(itens);
	}
	
	public void realizarCalculoValorMulta() {
		getFacadeFactory().getContaReceberFacade().realizarCalculoValorMulta(getContaReceberVO(), getUsuarioLogado());
		getContaReceberVO().calcularValorTotal();
	}
	
	public void realizarCalculoValorJuro() {
		getFacadeFactory().getContaReceberFacade().realizarCalculoValorJuro(getContaReceberVO(), getUsuarioLogado());
		getContaReceberVO().calcularValorTotal();
	}
	
	public List<SelectItem> getListaSelectItemTipoDescontoVOs() {
		List<SelectItem> itens = new ArrayList<SelectItem>(0);
		itens.add(new SelectItem(TipoDescontoEnum.PERCENTUAL, TipoDescontoEnum.PERCENTUAL.getValorApresentar()));
		itens.add(new SelectItem(TipoDescontoEnum.VALOR, TipoDescontoEnum.VALOR.getValorApresentar()));
		return itens;
	}
	
	public void realizarCalculoValorTotal() {
		getContaReceberVO().calcularValorTotal();
	}
}
