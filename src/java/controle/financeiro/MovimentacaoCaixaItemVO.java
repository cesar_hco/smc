package controle.financeiro;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.arquitetura.SuperVO;
import negocio.comuns.basico.ClienteVO;
import negocio.comuns.basico.FornecedorVO;
import negocio.comuns.financeiro.ChequeVO;
import negocio.comuns.financeiro.ContaCorrenteVO;
import negocio.comuns.financeiro.FormaPagamentoVO;
import negocio.comuns.financeiro.enumeradores.TipoMovimentacaoCaixaEnum;
import negocio.comuns.financeiro.enumeradores.TipoOrigemMovimentacaoCaixaEnum;
import negocio.comuns.financeiro.enumeradores.TipoSacadoEnum;
import negocio.facade.jdbc.financeiro.ContaCorrente;

public class MovimentacaoCaixaItemVO extends SuperVO implements Serializable {

	private static final long serialVersionUID = 1L;
	private Integer codigo;
	private MovimentacaoCaixaVO movimentacaoCaixaVO;
    private UsuarioVO responsavelVO;
    private FormaPagamentoVO formaPagamentoVO;
    private ClienteVO clienteVO;
    private FornecedorVO fornecedorVO;
    private Date dataMovimentacaoCaixa;
    private BigDecimal valor;
    private Integer codigoOrigem;
    private TipoMovimentacaoCaixaEnum tipoMovimentacaoCaixa;
    private TipoSacadoEnum tipoSacado;
    private TipoOrigemMovimentacaoCaixaEnum tipoOrigem;
    private ChequeVO chequeVO;
    private ContaCorrenteVO contaCorrenteVO;

	public MovimentacaoCaixaItemVO() {
		super();
	}

	public Integer getCodigo() {
		if (codigo == null) {
			codigo = 0;
		}
		return codigo;
	}

	public MovimentacaoCaixaVO getMovimentacaoCaixaVO() {
		if (movimentacaoCaixaVO == null) {
			movimentacaoCaixaVO = new MovimentacaoCaixaVO();
		}
		return movimentacaoCaixaVO;
	}

	public UsuarioVO getResponsavelVO() {
		if (responsavelVO == null) {
			responsavelVO = new UsuarioVO();
		}
		return responsavelVO;
	}

	public FormaPagamentoVO getFormaPagamentoVO() {
		if (formaPagamentoVO == null) {
			formaPagamentoVO = new FormaPagamentoVO();
		}
		return formaPagamentoVO;
	}

	public ClienteVO getClienteVO() {
		if (clienteVO == null) {
			clienteVO = new ClienteVO();
		}
		return clienteVO;
	}

	public FornecedorVO getFornecedorVO() {
		if (fornecedorVO == null) {
			fornecedorVO = new FornecedorVO();
		}
		return fornecedorVO;
	}

	public Date getDataMovimentacaoCaixa() {
		if (dataMovimentacaoCaixa == null) {
			dataMovimentacaoCaixa = new Date();
		}
		return dataMovimentacaoCaixa;
	}

	public BigDecimal getValor() {
		if (valor == null) {
			valor = BigDecimal.ZERO;
		}
		return valor;
	}

	public Integer getCodigoOrigem() {
		if (codigoOrigem == null) {
			codigoOrigem = 0;
		}
		return codigoOrigem;
	}

	public TipoMovimentacaoCaixaEnum getTipoMovimentacaoCaixa() {
		if (tipoMovimentacaoCaixa == null) {
			tipoMovimentacaoCaixa = TipoMovimentacaoCaixaEnum.ENTRADA;
		}
		return tipoMovimentacaoCaixa;
	}

	public TipoSacadoEnum getTipoSacado() {
		if (tipoSacado == null) {
			tipoSacado = TipoSacadoEnum.FORNECEDOR;
		}
		return tipoSacado;
	}

	public TipoOrigemMovimentacaoCaixaEnum getTipoOrigem() {
		return tipoOrigem;
	}

	public ChequeVO getChequeVO() {
		if (chequeVO == null) {
			chequeVO = new ChequeVO();
		}
		return chequeVO;
	}

	public ContaCorrenteVO getContaCorrenteVO() {
		if (contaCorrenteVO == null) {
			contaCorrenteVO = new ContaCorrenteVO();
		}
		return contaCorrenteVO;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public void setMovimentacaoCaixaVO(MovimentacaoCaixaVO movimentacaoCaixaVO) {
		this.movimentacaoCaixaVO = movimentacaoCaixaVO;
	}

	public void setResponsavelVO(UsuarioVO responsavelVO) {
		this.responsavelVO = responsavelVO;
	}

	public void setFormaPagamentoVO(FormaPagamentoVO formaPagamentoVO) {
		this.formaPagamentoVO = formaPagamentoVO;
	}

	public void setClienteVO(ClienteVO clienteVO) {
		this.clienteVO = clienteVO;
	}

	public void setFornecedorVO(FornecedorVO fornecedorVO) {
		this.fornecedorVO = fornecedorVO;
	}

	public void setDataMovimentacaoCaixa(Date dataMovimentacaoCaixa) {
		this.dataMovimentacaoCaixa = dataMovimentacaoCaixa;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public void setCodigoOrigem(Integer codigoOrigem) {
		this.codigoOrigem = codigoOrigem;
	}

	public void setTipoMovimentacaoCaixa(TipoMovimentacaoCaixaEnum tipoMovimentacaoCaixa) {
		this.tipoMovimentacaoCaixa = tipoMovimentacaoCaixa;
	}

	public void setTipoSacado(TipoSacadoEnum tipoSacado) {
		this.tipoSacado = tipoSacado;
	}

	public void setTipoOrigem(TipoOrigemMovimentacaoCaixaEnum tipoOrigem) {
		this.tipoOrigem = tipoOrigem;
	}

	public void setChequeVO(ChequeVO chequeVO) {
		this.chequeVO = chequeVO;
	}

	public void setContaCorrenteVO(ContaCorrenteVO contaCorrenteVO) {
		this.contaCorrenteVO = contaCorrenteVO;
	}

}
