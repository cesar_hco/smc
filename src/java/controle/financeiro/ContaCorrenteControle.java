package controle.financeiro;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import controle.arquitetura.SuperControle;
import negocio.comuns.arquitetura.enumeradores.MensagemAlertaEnum;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.comuns.financeiro.ContaCorrenteVO;
import negocio.comuns.utilitarias.Uteis;

@Controller("contaCorrenteControle")
@Scope("viewScope")
@Lazy
public class ContaCorrenteControle extends SuperControle implements Serializable {

	private static final long serialVersionUID = 1L;
	private ContaCorrenteVO contaCorrenteVO;

	public ContaCorrenteControle() {
		super();
	}
	
	public String novo() {
		Uteis.removerObjetoMemoria(this);
		setContaCorrenteVO(new ContaCorrenteVO());
		setMensagemID("msg_entre_dados", MensagemAlertaEnum.ATENCAO);
		return "contaCorrenteForm?faces-redirect=true";
	}
	
	public String consultar() {
		try {
			setListaConsultaVOs(getFacadeFactory().getContaCorrenteFacade().consultar(getCampoConsulta(), getValorConsulta(), NivelMontarDadosEnum.BASICO, getUsuarioLogado()));
			setMensagemID("msg_dados_consultados", MensagemAlertaEnum.ATENCAO);
		} catch (Exception e) {
			setMensagemDetalhada("msg_erro", e.getMessage());
		}
		return "contaCorrenteCons?faces-redirect=true";
	}
	
	public String irTelaConsulta() {
		getListaConsultaVOs().clear();
		setValorConsulta("");
		setMensagemID("msg_entre_prmconsulta", MensagemAlertaEnum.ATENCAO);
		return "contaCorrenteCons?faces-redirect=true";
	}
	
	public void persistir() {
		try {
			getFacadeFactory().getContaCorrenteFacade().persistir(getContaCorrenteVO(), getUsuarioLogado());
			setMensagemID("msg_dados_gravados", MensagemAlertaEnum.SUCESSO);
		} catch (Exception e) {
			setMensagemDetalhada("", e.getMessage());
		}
	}
	
	public String editar() {
		ContaCorrenteVO obj = (ContaCorrenteVO) context().getExternalContext().getRequestMap().get("contaCorrenteItem");
		obj.setNovoObj(Boolean.FALSE);
		setContaCorrenteVO(obj);
		setMensagemID("msg_dados_selecionados", MensagemAlertaEnum.ATENCAO);
		return "contaCorrenteForm?faces-redirect=true";
	}
	
	public List<SelectItem> getListaSelectItemVOs() {
		List<SelectItem> itens = new ArrayList<SelectItem>(0);
		itens.add(new SelectItem("NUMERO_CONTA_CORRENTE", "N�mero Conta Corrente"));
		itens.add(new SelectItem("DESCRICAO", "Descri��o"));
		return itens;
	}
	
	public String excluir() {
		try {
			getFacadeFactory().getContaCorrenteFacade().excluir(getContaCorrenteVO(), getUsuarioLogado());
			setMensagemID("msg_dados_excluidos", MensagemAlertaEnum.SUCESSO);
		} catch (Exception e) {
			setMensagemDetalhada("", e.getMessage());
		}
		return "contaCorrenteForm?faces-redirect=true";
	}

	public ContaCorrenteVO getContaCorrenteVO() {
		if (contaCorrenteVO == null) {
			contaCorrenteVO = new ContaCorrenteVO();
		}
		return contaCorrenteVO;
	}

	public void setContaCorrenteVO(ContaCorrenteVO contaCorrenteVO) {
		this.contaCorrenteVO = contaCorrenteVO;
	}


}
