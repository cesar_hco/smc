package controle.financeiro;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.model.SelectItem;

import negocio.comuns.administrativo.EmpresaVO;
import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.arquitetura.enumeradores.MensagemAlertaEnum;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.comuns.financeiro.ContaCorrenteVO;
import negocio.comuns.financeiro.enumeradores.SituacaoCaixaEnum;
import negocio.comuns.utilitarias.Uteis;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import controle.arquitetura.SuperControle;

@Controller("movimentacaoCaixaControle")
@Scope("viewScope")
@Lazy
public class MovimentacaoCaixaControle extends SuperControle implements Serializable {

	private static final long serialVersionUID = 1L;
	private MovimentacaoCaixaVO movimentacaoCaixaVO;
	private List<SelectItem> listaSelectItemContaCaixaVOs;
	private Date dataInicio;
	private Date dataFim;
	private List<SelectItem> listaSelectItemEmpresaVOs;
	private SituacaoCaixaEnum situacaoCaixa;

	public MovimentacaoCaixaControle() {
		super();
	}
	
	public String novo() {
		Uteis.removerObjetoMemoria(this);
		setMovimentacaoCaixaVO(new MovimentacaoCaixaVO());
		getMovimentacaoCaixaVO().setResponsavelAberturaVO(getUsuarioLogado());
		montarListaSelectItemContaCaixa();
		montarListaSelectItemEmpresa();
		setMensagemID("msg_entre_dados", MensagemAlertaEnum.ATENCAO);
		return "movimentacaoCaixaForm?faces-redirect=true";
	}
	
	public String consultar() {
		try {
			setListaConsultaVOs(getFacadeFactory().getMovimentacaoCaixaFacade().consultar(getCampoConsulta(), getValorConsulta(), getDataInicio(), getDataFim(), NivelMontarDadosEnum.BASICO, getUsuarioLogado()));
			setMensagemID("msg_dados_consultados", MensagemAlertaEnum.ATENCAO);
		} catch (Exception e) {
			setMensagemDetalhada("msg_erro", e.getMessage());
		}
		return "movimentacaoCaixaCons?faces-redirect=true";
	}
	
	public String irTelaConsulta() {
		getListaConsultaVOs().clear();
		setValorConsulta("");
		setMensagemID("msg_entre_prmconsulta", MensagemAlertaEnum.ATENCAO);
		return "movimentacaoCaixaCons?faces-redirect=true";
	}
	
	public void persistir() {
		try {
			getFacadeFactory().getMovimentacaoCaixaFacade().persistir(getMovimentacaoCaixaVO(), getUsuarioLogado());
			setMensagemID("msg_dados_gravados", MensagemAlertaEnum.SUCESSO);
		} catch (Exception e) {
			getMovimentacaoCaixaVO().setResponsavelFechamentoVO(new UsuarioVO());
			getMovimentacaoCaixaVO().setDataFechamento(null);
			getMovimentacaoCaixaVO().setSituacaoCaixa(SituacaoCaixaEnum.ABERTO);
			setMensagemDetalhada("", e.getMessage());
		}
	}
	
	public String editar() {
		
		try {
			MovimentacaoCaixaVO obj = (MovimentacaoCaixaVO) context().getExternalContext().getRequestMap().get("movimentacaoCaixaItens");
			getFacadeFactory().getMovimentacaoCaixaFacade().carregarDados(obj, NivelMontarDadosEnum.COMPLETO, getUsuarioLogado());
			obj.setNovoObj(Boolean.FALSE);
			setMovimentacaoCaixaVO(obj);
			montarListaSelectItemContaCaixa();
			montarListaSelectItemEmpresa();
			setMensagemID("msg_dados_selecionados", MensagemAlertaEnum.ATENCAO);
		} catch (Exception e) {
			setMensagemDetalhada("", e.getMessage());		}
		return "movimentacaoCaixaForm?faces-redirect=true";
	}
	
	public List<SelectItem> getListaSelectItemVOs() {
		List<SelectItem> itens = new ArrayList<SelectItem>(0);
		itens.add(new SelectItem("DESCRICAO_CONTA_CAIXA", "Descri��o Conta Caixa"));
		itens.add(new SelectItem("DATA_ABERTURA", "Data Abertura"));
		return itens;
	}
	
	public String excluir() {
		try {
			getFacadeFactory().getMovimentacaoCaixaFacade().excluir(getMovimentacaoCaixaVO(), getUsuarioLogado());
			setMensagemID("msg_dados_excluidos", MensagemAlertaEnum.SUCESSO);
		} catch (Exception e) {
			setMensagemDetalhada("", e.getMessage());
		}
		return "movimentacaoCaixaForm?faces-redirect=true";
	}
	
	public void montarListaSelectItemContaCaixa() {
		List<ContaCorrenteVO> listaContaCaixaVOs = getFacadeFactory().getContaCorrenteFacade().consultarContaCorrente(NivelMontarDadosEnum.BASICO, true, getUsuarioLogado());
		List<SelectItem> itens = new ArrayList<SelectItem>(0);
		itens.add(new SelectItem("", ""));
		for (ContaCorrenteVO contaCorrenteVO : listaContaCaixaVOs) {
			itens.add(new SelectItem(contaCorrenteVO.getCodigo(), contaCorrenteVO.getDescricao()));
		}
		setListaSelectItemContaCaixaVOs(itens);
 	}
	
	public String realizarAberturaCaixa() {
		try {
			getFacadeFactory().getMovimentacaoCaixaFacade().realizarAberturaCaixa(getMovimentacaoCaixaVO(), getUsuarioLogado());
			setMensagemID("msg_Caixa_caixaAbertoSucesso", MensagemAlertaEnum.SUCESSO);
		} catch (Exception e) {
			setMensagemDetalhada("", e.getMessage());
		}
		return "movimentacaoCaixaForm?faces-redirect=true";
	}
	
	public String realizarFechamentoCaixa() {
		try {
			getFacadeFactory().getMovimentacaoCaixaFacade().realizarFechamentoCaixa(getMovimentacaoCaixaVO(), getUsuarioLogado());
			setMensagemID("msg_Caixa_caixaFechadoSucesso", MensagemAlertaEnum.SUCESSO);
		} catch (Exception e) {
			setMensagemDetalhada("", e.getMessage());
		}
		return "movimentacaoCaixaForm?faces-redirect=true";
	}
	
	public void atualizarDadosSaldoInicial() {
		try {
			if (getMovimentacaoCaixaVO().getContaCaixaVO().getCodigo().intValue() != 0) {
				MovimentacaoCaixaVO movimentacaoCaixaUltimo = getFacadeFactory().getMovimentacaoCaixaFacade().consultarUltimoCaixaPorContaCaixa(getMovimentacaoCaixaVO().getContaCaixaVO().getCodigo(), NivelMontarDadosEnum.BASICO, getUsuarioLogado());
//				ContaCorrenteVO contaCorrenteVO = getFacadeFactory().getContaCorrenteFacade().consultarPorChavePrimaria(getMovimentacaoCaixaVO().getContaCaixaVO().getCodigo(), NivelMontarDadosEnum.COMPLETO, getUsuarioLogado());
				if (movimentacaoCaixaUltimo.getCodigo().equals(0) ) {
					getMovimentacaoCaixaVO().setSaldoInicialDinheiro(BigDecimal.ZERO);
					getMovimentacaoCaixaVO().setSaldoFinalDinheiro(BigDecimal.ZERO);
					getMovimentacaoCaixaVO().setSaldoInicialCheque(BigDecimal.ZERO);
					getMovimentacaoCaixaVO().setSaldoFinalCheque(BigDecimal.ZERO);
				} else {
					getMovimentacaoCaixaVO().setSaldoInicialDinheiro(movimentacaoCaixaUltimo.getSaldoFinalDinheiro());
					getMovimentacaoCaixaVO().setSaldoFinalDinheiro(movimentacaoCaixaUltimo.getSaldoFinalDinheiro());
					getMovimentacaoCaixaVO().setSaldoInicialCheque(movimentacaoCaixaUltimo.getSaldoFinalCheque());
					getMovimentacaoCaixaVO().setSaldoFinalCheque(movimentacaoCaixaUltimo.getSaldoFinalCheque());
					
				}
//				getMovimentacaoCaixaVO().setUnidadeEnsino(getContaCorrenteVO().getUnidadeEnsinoContaCorrenteVOs().get(0).getUnidadeEnsino().getCodigo());
			} else {
				getMovimentacaoCaixaVO().setSaldoInicialDinheiro(BigDecimal.ZERO);
				getMovimentacaoCaixaVO().setSaldoFinalDinheiro(BigDecimal.ZERO);
				getMovimentacaoCaixaVO().setSaldoInicialCheque(BigDecimal.ZERO);
				getMovimentacaoCaixaVO().setSaldoFinalCheque(BigDecimal.ZERO);
			}
		} catch (Exception e) {
		}
	}

	public MovimentacaoCaixaVO getMovimentacaoCaixaVO() {
		if (movimentacaoCaixaVO == null) {
			movimentacaoCaixaVO = new MovimentacaoCaixaVO();
		}
		return movimentacaoCaixaVO;
	}

	public void setMovimentacaoCaixaVO(MovimentacaoCaixaVO movimentacaoCaixaVO) {
		this.movimentacaoCaixaVO = movimentacaoCaixaVO;
	}

	public List<SelectItem> getListaSelectItemContaCaixaVOs() {
		if (listaSelectItemContaCaixaVOs == null) {
			listaSelectItemContaCaixaVOs = new ArrayList<SelectItem>(0);
		}
		return listaSelectItemContaCaixaVOs;
	}

	public void setListaSelectItemContaCaixaVOs(List<SelectItem> listaSelectItemContaCaixaVOs) {
		this.listaSelectItemContaCaixaVOs = listaSelectItemContaCaixaVOs;
	}

	public Boolean getApresentarBotaoAbrirCaixa() {
		return getMovimentacaoCaixaVO().getCodigo().equals(0);
	}
	
	public Boolean getApresentarBotaoFecharCaixa() {
		return !getMovimentacaoCaixaVO().getCodigo().equals(0) && getMovimentacaoCaixaVO().getSituacaoCaixa().equals(SituacaoCaixaEnum.ABERTO);
	}

	public Date getDataInicio() {
		if (dataInicio == null) {
			dataInicio = new Date();
		}
		return dataInicio;
	}

	public Date getDataFim() {
		if (dataFim == null) {
			dataFim = new Date();
		}
		return dataFim;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	public Boolean getApresentarDataInicioDataFim() {
		return getCampoConsulta().equals("DATA_ABERTURA");
	}
	
	public List<SelectItem> getListaSelectItemEmpresaVOs() {
		if (listaSelectItemEmpresaVOs == null) {
			listaSelectItemEmpresaVOs = new ArrayList<SelectItem>(0);
		}
		return listaSelectItemEmpresaVOs;
	}

	public void setListaSelectItemEmpresaVOs(List<SelectItem> listaSelectItemEmpresaVOs) {
		this.listaSelectItemEmpresaVOs = listaSelectItemEmpresaVOs;
	}
	
	public void montarListaSelectItemEmpresa() {
		List<EmpresaVO> listaEmpresaVOs = getFacadeFactory().getEmpresaFacade().consultarPorDescricao("", NivelMontarDadosEnum.BASICO, getUsuarioLogado());
		List<SelectItem> itens = new ArrayList<SelectItem>(0);
		itens.add(new SelectItem("", ""));
		for (EmpresaVO empresaVO : listaEmpresaVOs) {
			itens.add(new SelectItem(empresaVO.getCodigo(), empresaVO.getDescricao()));
		}
		setListaSelectItemEmpresaVOs(itens);
	}
	
	public List<SelectItem> getListaSelectItemSituacaoVOs() {
		List<SelectItem> itens = new ArrayList<SelectItem>(0);
		itens.add(new SelectItem(SituacaoCaixaEnum.ABERTO, "ABERTO"));
		itens.add(new SelectItem(SituacaoCaixaEnum.FECHADO, "FECHADO"));
		itens.add(new SelectItem(SituacaoCaixaEnum.TODAS, "Todas"));
		return itens;
	}

	public SituacaoCaixaEnum getSituacaoCaixa() {
		if (situacaoCaixa == null) {
			situacaoCaixa = SituacaoCaixaEnum.ABERTO;
		}
		return situacaoCaixa;
	}

	public void setSituacaoCaixa(SituacaoCaixaEnum situacaoCaixa) {
		this.situacaoCaixa = situacaoCaixa;
	}
}
