package controle.financeiro;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.model.SelectItem;

import negocio.comuns.administrativo.EmpresaVO;
import negocio.comuns.arquitetura.enumeradores.MensagemAlertaEnum;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.comuns.basico.FornecedorVO;
import negocio.comuns.financeiro.ChequeVO;
import negocio.comuns.financeiro.ContaCorrenteVO;
import negocio.comuns.financeiro.ContaPagarPagamentoVO;
import negocio.comuns.financeiro.ContaPagarVO;
import negocio.comuns.financeiro.FormaPagamentoPagamentoVO;
import negocio.comuns.financeiro.FormaPagamentoVO;
import negocio.comuns.financeiro.PagamentoVO;
import negocio.comuns.utilitarias.Uteis;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import controle.arquitetura.SuperControle;

@Controller("pagamentoControle")
@Scope("viewScope")
@Lazy
public class PagamentoControle extends SuperControle implements Serializable {

	private static final long serialVersionUID = 1L;
	private PagamentoVO pagamentoVO;
	private String campoConsultaFornecedor;
	private String valorConsultaFornecedor;
	private List<FornecedorVO> listaConsultaFornecedorVOs;
	private String situacaoContaPagar;
	private List<ContaPagarVO> listaConsultaContaPagarVOs;
	private Date dataInicioContaPagar;
	private Date dataFinalContaPagar;
	private List<SelectItem> listaSelectItemContaCorrenteVOs;
	private FormaPagamentoPagamentoVO formaPagamentoPagamentoVO;
	private List<SelectItem> listaSelectItemContaCorrenteCaixaVOs;
	private List<SelectItem> listaSelectItemFormaPagamentoVOs;
	private List<SelectItem> listaSelectItemEmpresaVOs;
	private Date dataInicio;
	private Date dataFim;
	private List<ChequeVO> listaConsultaChequeVOs;
	private String numeroCheque;
	private Date dataInicioCheque;
	private Date dataFimCheque;

	public PagamentoControle() {
		super();
	}
	
	@PostConstruct
	public void realizarPagamentoContaPagarVindoTelaContaPagar() {
		ContaPagarVO obj = (ContaPagarVO) context().getExternalContext().getSessionMap().get("pagarContaPagar");
		if (obj != null && !obj.getCodigo().equals(0)) {
			novo();
			getPagamentoVO().setEmpresaVO(obj.getEmpresaVO());
			getPagamentoVO().setFornecedorVO(obj.getFornecedorVO());
			getFacadeFactory().getPagamentoFacade().adicionarContaPagarPagamento(getPagamentoVO().getListaContaPagarPagamentoVOs(), obj, getUsuarioLogado());
			getFacadeFactory().getPagamentoFacade().calcularTotalPagamento(getPagamentoVO());
			getFacadeFactory().getContaPagarFacade().calcularValorPagar(obj, getUsuarioLogado());
		}
		
	}

	public String novo() {
		Uteis.removerObjetoMemoria(this);
		setPagamentoVO(new PagamentoVO());
		getPagamentoVO().setResponsavelVO(getUsuarioLogado());
		montarListaSelectItemContaCorrente();
		montarListaSelectItemContaCorrenteCaixa();
		montarListaSelectItemFormaPagamento();
		montarListaSelectItemEmpresa();
		setMensagemID("msg_entre_dados", MensagemAlertaEnum.ATENCAO);
		return "pagamentoForm?faces-redirect=true";
	}

	public String consultar() {
		try {
			setListaConsultaVOs(getFacadeFactory().getPagamentoFacade().consultar(getCampoConsulta(), getValorConsulta(), getDataInicio(), getDataFim(), NivelMontarDadosEnum.BASICO, getUsuarioLogado()));
			setMensagemID("msg_dados_consultados", MensagemAlertaEnum.ATENCAO);
		} catch (Exception e) {
			setMensagemDetalhada("msg_erro", e.getMessage());
		}
		return "pagamentoCons?faces-redirect=true";
	}

	public String irTelaConsulta() {
		getListaConsultaVOs().clear();
		setValorConsulta("");
		setMensagemID("msg_entre_prmconsulta", MensagemAlertaEnum.ATENCAO);
		return "pagamentoCons?faces-redirect=true";
	}

	public String persistir() {
		try {
			getFacadeFactory().getPagamentoFacade().persistir(getPagamentoVO(), getUsuarioLogado());
			setMensagemID("msg_dados_gravados", MensagemAlertaEnum.SUCESSO);
		} catch (Exception e) {
			setMensagemDetalhada("", e.getMessage());
		}
		return "pagamentoForm?faces-redirect=true";
	}

	public String editar() {
		PagamentoVO obj = (PagamentoVO) context().getExternalContext().getRequestMap().get("pagamentoItem");
		getFacadeFactory().getPagamentoFacade().carregarDados(obj, NivelMontarDadosEnum.COMPLETO, getUsuarioLogado());
		montarListaSelectItemEmpresa();
		obj.setNovoObj(Boolean.FALSE);
		setPagamentoVO(obj);
		setMensagemID("msg_dados_selecionados", MensagemAlertaEnum.ATENCAO);
		return "pagamentoForm?faces-redirect=true";
	}

	public List<SelectItem> getListaSelectItemVOs() {
		List<SelectItem> itens = new ArrayList<SelectItem>(0);
		itens.add(new SelectItem("FORNECEDOR", "Fornecedor"));
		itens.add(new SelectItem("DATA_PAGAMENTO", "Data de Pagamento"));
		itens.add(new SelectItem("EMPRESA", "Empresa"));
		return itens;
	}

	public String excluir() {
		try {
			getFacadeFactory().getPagamentoFacade().excluir(getPagamentoVO(), getUsuarioLogado());
			setMensagemID("msg_dados_excluidos", MensagemAlertaEnum.SUCESSO);
		} catch (Exception e) {
			setMensagemDetalhada("", e.getMessage());
		}
		return "pagamentoForm?faces-redirect=true";
	}

	public void estornarPagamento() {
		try {
			 getFacadeFactory().getPagamentoFacade().estornarPagamento(getPagamentoVO(), getUsuarioLogado());
			setFormaPagamentoPagamentoVO(null);
			setMensagemID("msg_dados_estornado", MensagemAlertaEnum.ATENCAO);
		} catch (Exception e) {
			try {
				getFacadeFactory().getPagamentoFacade().carregarDados(getPagamentoVO(), NivelMontarDadosEnum.COMPLETO, getUsuarioLogado());
				setMensagemDetalhada("", e.getMessage());
			} catch (Exception ex) {
				setMensagemDetalhada("", ex.getMessage());

			}
		}
	}

	public void consultarFornecedor() {
		try {
			setListaConsultaFornecedorVOs(getFacadeFactory().getFornecedorFacade().consultar(getCampoConsultaFornecedor(), getValorConsultaFornecedor(), NivelMontarDadosEnum.BASICO, getUsuarioLogado()));
			setMensagemID("msg_dados_consultados", MensagemAlertaEnum.ATENCAO);
		} catch (Exception e) {
			setMensagemDetalhada("", e.getMessage());
		}
	}

	public void selecionarFornecedor() {
		FornecedorVO obj = (FornecedorVO) context().getExternalContext().getRequestMap().get("fornecedorItem");
		getPagamentoVO().setFornecedorVO(obj);
		consultarContaPagar();
		setMensagemID("msg_dados_selecionados", MensagemAlertaEnum.ATENCAO);
	}

	public void limparFornecedor() {
		getPagamentoVO().setFornecedorVO(null);
	}

	public void limparDadosConsultaModalFornecedor() {
		setListaConsultaFornecedorVOs(null);
	}

	public List<SelectItem> getListaSelectItemFornecedorVOs() {
		List<SelectItem> itens = new ArrayList<SelectItem>(0);
		itens.add(new SelectItem("NOME", "Nome"));
		return itens;
	}

	public List<SelectItem> getListaSelectItemContaPagarVOs() {
		List<SelectItem> itens = new ArrayList<SelectItem>(0);
		itens.add(new SelectItem("TODAS", "Todas"));
		itens.add(new SelectItem("VENCIDAS", "Vencidas"));
		itens.add(new SelectItem("A_VENCER", "A vencer"));
		itens.add(new SelectItem("DATA_VENCIMENTO", "Data Vencimento"));
		return itens;
	}

	public void consultarContaPagar() {
		if (getPagamentoVO().getFornecedorVO().getCodigo().equals(0)) {
			try {
				throw new Exception("Por favor informe um FORNECEDOR!");
			} catch (Exception e) {
				setMensagemDetalhada("", e.getMessage());
			}
		} else {
			setListaConsultaContaPagarVOs(getFacadeFactory().getContaPagarFacade().consultarContaAPagarPorFornecedorSituacaoDataVencimento(getPagamentoVO().getFornecedorVO().getCodigo(), getSituacaoContaPagar(), getDataInicioContaPagar(), getDataInicioContaPagar(), NivelMontarDadosEnum.BASICO, getUsuarioLogado()));
			setMensagemID("msg_dados_consultados", MensagemAlertaEnum.ATENCAO);
		}
	}

	public void adicionarContaPagarIndividual() {
		ContaPagarVO obj = (ContaPagarVO) context().getExternalContext().getRequestMap().get("contaPagarItem");
		if (obj != null && !obj.getCodigo().equals(0)) {
			getFacadeFactory().getPagamentoFacade().adicionarContaPagarPagamento(getPagamentoVO().getListaContaPagarPagamentoVOs(), obj, getUsuarioLogado());
			getFacadeFactory().getPagamentoFacade().calcularTotalPagamento(getPagamentoVO());
			getFacadeFactory().getContaPagarFacade().calcularValorPagar(obj, getUsuarioLogado());
			int index = 0;
			for (ContaPagarVO contaPagarVO : getListaConsultaContaPagarVOs()) {
				if (contaPagarVO.getCodigo().equals(obj.getCodigo())) {
					getListaConsultaContaPagarVOs().remove(index);
					return;
				}
				index++;
			}
			setMensagemID("msg_dados_adicionados", MensagemAlertaEnum.ATENCAO);
		}
	}

	public void removerContaPagarPagamento() {
		ContaPagarPagamentoVO obj = (ContaPagarPagamentoVO) context().getExternalContext().getRequestMap().get("contaPagarPagamentoItem");
		getFacadeFactory().getPagamentoFacade().removerContaPagarPagamento(getPagamentoVO().getListaContaPagarPagamentoVOs(), obj);
		setMensagemID("msg_dados_removidos", MensagemAlertaEnum.ATENCAO);
	}

	public void adicionarFormaPagamento() {
		try {
			getFacadeFactory().getPagamentoFacade().adicionarFormaPagamento(getPagamentoVO(), getPagamentoVO().getListaFormaPagamentoPagamentoVOs(), getFormaPagamentoPagamentoVO(), getUsuarioLogado());
			setFormaPagamentoPagamentoVO(new FormaPagamentoPagamentoVO());
			getFormaPagamentoPagamentoVO().setValor(getPagamentoVO().getValorPendente());
			setMensagemID("msg_dados_adicionados", MensagemAlertaEnum.ATENCAO);
		} catch (Exception e) {
			setMensagemDetalhada("", e.getMessage());
		}
	}

	public void removerFormaPagamento() {
		FormaPagamentoPagamentoVO obj = (FormaPagamentoPagamentoVO) context().getExternalContext().getRequestMap().get("formaPagamentoPagamentoItem");
		getFacadeFactory().getPagamentoFacade().removerFormaPagamento(getPagamentoVO().getListaFormaPagamentoPagamentoVOs(), obj);
	}

	public void calcularValorTotalContaPagar() {
		try {
			ContaPagarPagamentoVO contaPagarPagamentoVO = (ContaPagarPagamentoVO) context().getExternalContext().getRequestMap().get("contaPagarPagamentoItem");
			getFacadeFactory().getContaPagarFacade().calcularValorPagar(contaPagarPagamentoVO.getContaPagarVO(), getUsuarioLogado());
			getFacadeFactory().getPagamentoFacade().calcularTotalPagamento(getPagamentoVO());
		} catch (Exception e) {
			setMensagemDetalhada("", e.getMessage());
		}
	}
	
	public void consultarContaCaixa() {
		if (getPagamentoVO().getContaCaixaVO() != null && !getPagamentoVO().getContaCaixaVO().getCodigo().equals(0)) {
			getFacadeFactory().getContaCorrenteFacade().carregarDados(getPagamentoVO().getContaCaixaVO(), NivelMontarDadosEnum.BASICO, getUsuarioLogado());
		}
	}

	public PagamentoVO getPagamentoVO() {
		if (pagamentoVO == null) {
			pagamentoVO = new PagamentoVO();
		}
		return pagamentoVO;
	}

	public void setPagamentoVO(PagamentoVO pagamentoVO) {
		this.pagamentoVO = pagamentoVO;
	}

	public String getCampoConsultaFornecedor() {
		if (campoConsultaFornecedor == null) {
			campoConsultaFornecedor = "";
		}
		return campoConsultaFornecedor;
	}

	public void setCampoConsultaFornecedor(String campoConsultaFornecedor) {
		this.campoConsultaFornecedor = campoConsultaFornecedor;
	}

	public String getValorConsultaFornecedor() {
		if (valorConsultaFornecedor == null) {
			valorConsultaFornecedor = "";
		}
		return valorConsultaFornecedor;
	}

	public void setValorConsultaFornecedor(String valorConsultaFornecedor) {
		this.valorConsultaFornecedor = valorConsultaFornecedor;
	}

	public List<FornecedorVO> getListaConsultaFornecedorVOs() {
		if (listaConsultaFornecedorVOs == null) {
			listaConsultaFornecedorVOs = new ArrayList<FornecedorVO>(0);
		}
		return listaConsultaFornecedorVOs;
	}

	public void setListaConsultaFornecedorVOs(List<FornecedorVO> listaConsultaFornecedorVOs) {
		this.listaConsultaFornecedorVOs = listaConsultaFornecedorVOs;
	}

	public List<ContaPagarVO> getListaConsultaContaPagarVOs() {
		if (listaConsultaContaPagarVOs == null) {
			listaConsultaContaPagarVOs = new ArrayList<ContaPagarVO>(0);
		}
		return listaConsultaContaPagarVOs;
	}

	public void setListaConsultaContaPagarVOs(List<ContaPagarVO> listaConsultaContaPagarVOs) {
		this.listaConsultaContaPagarVOs = listaConsultaContaPagarVOs;
	}

	public Date getDataInicioContaPagar() {
		if (dataInicioContaPagar == null) {
			dataInicioContaPagar = Uteis.getDataPrimeiroDiaMes(new Date());
		}
		return dataInicioContaPagar;
	}

	public void setDataInicioContaPagar(Date dataInicioContaPagar) {
		this.dataInicioContaPagar = dataInicioContaPagar;
	}

	public Date getDataFinalContaPagar() {
		if (dataFinalContaPagar == null) {
			dataFinalContaPagar = Uteis.getDataUltimoDiaMes(new Date());
		}
		return dataFinalContaPagar;
	}

	public void setDataFinalContaPagar(Date dataFinalContaPagar) {
		this.dataFinalContaPagar = dataFinalContaPagar;
	}

	public String getSituacaoContaPagar() {
		if (situacaoContaPagar == null) {
			situacaoContaPagar = "TODAS";
		}
		return situacaoContaPagar;
	}

	public void setSituacaoContaPagar(String situacaoContaPagar) {
		this.situacaoContaPagar = situacaoContaPagar;
	}

	public void carregarDadosAposEscolhaFormaPagamento() {
		montarListaSelectItemContaCorrenteCaixa();
	}

	public void montarListaSelectItemContaCorrenteCaixa() {
		List<ContaCorrenteVO> listaContaCorrenteVOs = getFacadeFactory().getContaCorrenteFacade().consultarContaCorrente(NivelMontarDadosEnum.BASICO, true, getUsuarioLogado());
		List<SelectItem> itens = new ArrayList<SelectItem>(0);
		itens.add(new SelectItem("", ""));
		for (ContaCorrenteVO contaCorrenteVO : listaContaCorrenteVOs) {
			if (contaCorrenteVO.getContaCaixa()) {
				itens.add(new SelectItem(contaCorrenteVO.getCodigo(), contaCorrenteVO.getDescricao()));
			}
		}
		setListaSelectItemContaCorrenteCaixaVOs(itens);
	}

	public void montarListaSelectItemContaCorrente() {
		List<ContaCorrenteVO> listaContaCorrenteVOs = getFacadeFactory().getContaCorrenteFacade().consultarContaCorrente(NivelMontarDadosEnum.BASICO, false, getUsuarioLogado());
		List<SelectItem> itens = new ArrayList<SelectItem>(0);
		itens.add(new SelectItem("", ""));
		for (ContaCorrenteVO contaCorrenteVO : listaContaCorrenteVOs) {
			if (!contaCorrenteVO.getContaCaixa()) {
				itens.add(new SelectItem(contaCorrenteVO.getCodigo(), contaCorrenteVO.getDescricaoApresentar()));
			}
		}
		setListaSelectItemContaCorrenteVOs(itens);
	}

	public List<SelectItem> getListaSelectItemContaCorrenteVOs() {
		if (listaSelectItemContaCorrenteVOs == null) {
			listaSelectItemContaCorrenteVOs = new ArrayList<SelectItem>(0);
		}
		return listaSelectItemContaCorrenteVOs;
	}

	public void setListaSelectItemContaCorrenteVOs(List<SelectItem> listaSelectItemContaCorrenteVOs) {
		this.listaSelectItemContaCorrenteVOs = listaSelectItemContaCorrenteVOs;
	}

	public void montarListaSelectItemFormaPagamento() {
		List<FormaPagamentoVO> listaFormaPagamentoVOs = getFacadeFactory().getFormaPagamentoFacade().consultarFormaPagamento(NivelMontarDadosEnum.BASICO, getUsuarioLogado());
		List<SelectItem> itens = new ArrayList<SelectItem>(0);
		itens.add(new SelectItem("", ""));
		for (FormaPagamentoVO formaPagamentoVO : listaFormaPagamentoVOs) {
			itens.add(new SelectItem(formaPagamentoVO.getCodigo(), formaPagamentoVO.getNome()));
		}
		setListaSelectItemFormaPagamentoVOs(itens);
	}

	public void consultarFormaPagamento() {
		if (!getFormaPagamentoPagamentoVO().getFormaPagamentoVO().getCodigo().equals(0)) {
			getFacadeFactory().getFormaPagamentoFacade().carregarDados(getFormaPagamentoPagamentoVO().getFormaPagamentoVO(), NivelMontarDadosEnum.BASICO, getUsuarioLogado());
		}
	}

	public void consultarCheque() {
		setListaConsultaChequeVOs(getFacadeFactory().getChequeFacade().consultarPorNumeroDataVencimentoCheque(getNumeroCheque(), getDataInicioCheque(), getDataFimCheque(), NivelMontarDadosEnum.COMPLETO, getUsuarioLogado()));
		setMensagemID("msg_dados_consultados", MensagemAlertaEnum.ATENCAO);
	}

	public void selecionarChequeTerceiro() {
		try {
			ChequeVO chequeVO = (ChequeVO) context().getExternalContext().getRequestMap().get("chequeItem");
			FormaPagamentoVO formaPagamentoVO = getFormaPagamentoPagamentoVO().getFormaPagamentoVO();
			getFacadeFactory().getFormaPagamentoPagamentoFacade().inicalzarDadosChequeFormaPagamentoPagamento(getFormaPagamentoPagamentoVO(), chequeVO, getUsuarioLogado());
			getFacadeFactory().getPagamentoFacade().adicionarFormaPagamento(getPagamentoVO(), getPagamentoVO().getListaFormaPagamentoPagamentoVOs(), getFormaPagamentoPagamentoVO(), getUsuarioLogado());
			setFormaPagamentoPagamentoVO(new FormaPagamentoPagamentoVO());
			getFormaPagamentoPagamentoVO().setValor(getPagamentoVO().getValorPendente());
			getFormaPagamentoPagamentoVO().setFormaPagamentoVO(formaPagamentoVO);
			getFormaPagamentoPagamentoVO().getChequeVO().setChequeProprio(false);
		} catch (Exception e) {
			setMensagemDetalhada("", e.getMessage());
		}

	}

	public void montarListaSelectItemEmpresa() {
		List<EmpresaVO> listaEmpresaVOs = getFacadeFactory().getEmpresaFacade().consultarPorDescricao("", NivelMontarDadosEnum.BASICO, getUsuarioLogado());
		List<SelectItem> itens = new ArrayList<SelectItem>(0);
		itens.add(new SelectItem("", ""));
		for (EmpresaVO empresaVO : listaEmpresaVOs) {
			itens.add(new SelectItem(empresaVO.getCodigo(), empresaVO.getDescricao()));
		}
		setListaSelectItemEmpresaVOs(itens);
	}

	public FormaPagamentoPagamentoVO getFormaPagamentoPagamentoVO() {
		if (formaPagamentoPagamentoVO == null) {
			formaPagamentoPagamentoVO = new FormaPagamentoPagamentoVO();
		}
		return formaPagamentoPagamentoVO;
	}

	public void setFormaPagamentoPagamentoVO(FormaPagamentoPagamentoVO formaPagamentoPagamentoVO) {
		this.formaPagamentoPagamentoVO = formaPagamentoPagamentoVO;
	}

	public List<SelectItem> getListaSelectItemContaCorrenteCaixaVOs() {
		if (listaSelectItemContaCorrenteCaixaVOs == null) {
			listaSelectItemContaCorrenteCaixaVOs = new ArrayList<SelectItem>(0);
		}
		return listaSelectItemContaCorrenteCaixaVOs;
	}

	public void setListaSelectItemContaCorrenteCaixaVOs(List<SelectItem> listaSelectItemContaCorrenteCaixaVOs) {
		this.listaSelectItemContaCorrenteCaixaVOs = listaSelectItemContaCorrenteCaixaVOs;
	}

	public Boolean getIsApresentarComboboxContaCorrente() {
		return getFormaPagamentoPagamentoVO() != null && getFormaPagamentoPagamentoVO().getFormaPagamentoVO() != null && !getFormaPagamentoPagamentoVO().getFormaPagamentoVO().getCodigo().equals(0) && !getFormaPagamentoPagamentoVO().getFormaPagamentoVO().getIsTipoDinheiro();
	}

	public Boolean getIsApresentarComboboxContaCaixa() {
		return getFormaPagamentoPagamentoVO() != null && getFormaPagamentoPagamentoVO().getFormaPagamentoVO() != null && !getFormaPagamentoPagamentoVO().getFormaPagamentoVO().getCodigo().equals(0) && getFormaPagamentoPagamentoVO().getFormaPagamentoVO().getIsTipoDinheiro();
	}

	public List<SelectItem> getListaSelectItemFormaPagamentoVOs() {
		if (listaSelectItemFormaPagamentoVOs == null) {
			listaSelectItemFormaPagamentoVOs = new ArrayList<SelectItem>(0);
		}
		return listaSelectItemFormaPagamentoVOs;
	}

	public void setListaSelectItemFormaPagamentoVOs(List<SelectItem> listaSelectItemFormaPagamentoVOs) {
		this.listaSelectItemFormaPagamentoVOs = listaSelectItemFormaPagamentoVOs;
	}

	public List<SelectItem> getListaSelectItemEmpresaVOs() {
		if (listaSelectItemEmpresaVOs == null) {
			listaSelectItemEmpresaVOs = new ArrayList<SelectItem>(0);
		}
		return listaSelectItemEmpresaVOs;
	}

	public void setListaSelectItemEmpresaVOs(List<SelectItem> listaSelectItemEmpresaVOs) {
		this.listaSelectItemEmpresaVOs = listaSelectItemEmpresaVOs;
	}

	public Date getDataInicio() {
		if (dataInicio == null) {
			dataInicio = Uteis.getDataPrimeiroDiaMes(new Date());
		}
		return dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Date getDataFim() {
		if (dataFim == null) {
			dataFim = Uteis.getDataUltimoDiaMes(new Date());
		}
		return dataFim;
	}

	public void setDataFim(Date dataFim) {
		this.dataFim = dataFim;
	}

	public Boolean getApresentarDataInicioDataFim() {
		return getCampoConsulta().equals("DATA_PAGAMENTO");
	}

	public List<ChequeVO> getListaConsultaChequeVOs() {
		if (listaConsultaChequeVOs == null) {
			listaConsultaChequeVOs = new ArrayList<ChequeVO>(0);
		}
		return listaConsultaChequeVOs;
	}

	public void setListaConsultaChequeVOs(List<ChequeVO> listaConsultaChequeVOs) {
		this.listaConsultaChequeVOs = listaConsultaChequeVOs;
	}

	public String getNumeroCheque() {
		if (numeroCheque == null) {
			numeroCheque = "";
		}
		return numeroCheque;
	}

	public Date getDataInicioCheque() {
		if (dataInicioCheque == null) {
			dataInicioCheque = new Date();
		}
		return dataInicioCheque;
	}

	public Date getDataFimCheque() {
		if (dataFimCheque == null) {
			dataFimCheque = new Date();
		}
		return dataFimCheque;
	}

	public void setNumeroCheque(String numeroCheque) {
		this.numeroCheque = numeroCheque;
	}

	public void setDataInicioCheque(Date dataInicioCheque) {
		this.dataInicioCheque = dataInicioCheque;
	}

	public void setDataFimCheque(Date dataFimCheque) {
		this.dataFimCheque = dataFimCheque;
	}

	public Boolean getIsPagamentoComChequeProprio() {
		return getFormaPagamentoPagamentoVO().getFormaPagamentoVO().getIsTipoCheque() && getFormaPagamentoPagamentoVO().getChequeVO().getChequeProprio();
	}

	public Boolean getIsPagamentoComChequeTerceiro() {
		return getFormaPagamentoPagamentoVO().getFormaPagamentoVO().getIsTipoCheque() && !getFormaPagamentoPagamentoVO().getChequeVO().getChequeProprio();
	}

	public Boolean getApresentarBotaoAdicionarFormaPagamentoContaPagar() {
		return !(getFormaPagamentoPagamentoVO().getFormaPagamentoVO().getIsTipoCheque() && !getFormaPagamentoPagamentoVO().getChequeVO().getChequeProprio());
	}

	public void inicializarDadosChequeTerceiro() {

	}
}
