package controle.agenda;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import controle.arquitetura.SuperControle;
import negocio.comuns.agenda.ValorHorasVO;
import negocio.comuns.arquitetura.enumeradores.MensagemAlertaEnum;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.comuns.financeiro.ContaCorrenteVO;
import negocio.comuns.utilitarias.Uteis;

@Controller("valorHorasControle")
@Scope("viewScope")
@Lazy
public class ValorHorasControle extends SuperControle implements Serializable {

	/**
	 * @author Cesar Henrique
	 */
	private static final long serialVersionUID = 1L;

	private ValorHorasVO valorHorasVO;
	
	
	public ValorHorasControle(){
		super();
	}
	
	public String novo() {
		Uteis.removerObjetoMemoria(this);
		setValorHorasVO(new ValorHorasVO());
		setMensagemID("msg_entre_dados", MensagemAlertaEnum.ATENCAO);
		return "valorHorasForm?faces-redirect=true";
	}
	
	
	
	public String consultar(){																				
		try {
			setListaConsultaVOs(getFacadeFactory().getvalorHorasFacade().consultarPorCodigo(getCampoConsulta(), getValorConsultaCodigo(), NivelMontarDadosEnum.BASICO, getUsuarioLogado()));
			setMensagemID("msg_entre_dados", MensagemAlertaEnum.ATENCAO);
		} catch (Exception e) {
			setMensagemDetalhada("msg_erro", e.getMessage());
		}
		
		return "valorHorasCons?faces-redirect=true";
		
	}
	
	public String irTelaConsulta() {
		
		getListaConsultaVOs().clear();
		setValorConsultaCodigo(0);
		setMensagemID("msg_entre_prmconsulta", MensagemAlertaEnum.ATENCAO);
		
		return "valorHorasCons?faces-redirect=true";	
	}
	
	public void incluir() {
		try {
			getFacadeFactory().getvalorHorasFacade().incluir(getValorHorasVO(), getUsuarioLogado());
			setMensagemID("msg_dados_gravados", MensagemAlertaEnum.SUCESSO);
		} catch (Exception e) {
			setMensagemDetalhada("", e.getMessage());
		}
	}
	
	public String editar() {
		ValorHorasVO obj = (ValorHorasVO) context().getExternalContext().getRequestMap().get("contaCorrenteItem");
		obj.setNovoObj(Boolean.FALSE);
		setValorHorasVO(obj);
		setMensagemID("msg_dados_selecionados", MensagemAlertaEnum.ATENCAO);
		return "valorHorasForm?faces-redirect=true";
	}
	
    public List<SelectItem> getListaSelectItemVOs() {
		List<SelectItem> itens = new ArrayList<SelectItem>(0);
		itens.add(new SelectItem("CODIGO", "Codigo"));
		return itens;
	}
	
	public String excluir() {
		try {
			getFacadeFactory().getvalorHorasFacade().excluir(getValorHorasVO(), getUsuarioLogado());
			setMensagemID("msg_dados_excluidos", MensagemAlertaEnum.SUCESSO);
		} catch (Exception e) {
			setMensagemDetalhada("", e.getMessage());
		}
		return "valorHorasForm?faces-redirect=true";
	}

	public ValorHorasVO getValorHorasVO() {
		if (valorHorasVO == null) {
			valorHorasVO = new ValorHorasVO();
		}
		return valorHorasVO;
	}

	public void setValorHorasVO(ValorHorasVO valorHorasVO) {
		this.valorHorasVO = valorHorasVO;
	}
	
	
	
}
