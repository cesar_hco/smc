package relatorio.negocio.jdbc.financeiro;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.financeiro.enumeradores.SituacaoContaPagarEnum;
import negocio.comuns.utilitarias.Uteis;
import negocio.facade.jdbc.arquitetura.ControleAcesso;
import relatorio.negocio.comuns.financeiro.ContaPagarRelVO;
import relatorio.negocio.interfaces.financeiro.ContaPagarRelInterfaceFacade;

@Repository
@Scope("singleton")
@Lazy
public class ContaPagarRel extends ControleAcesso implements ContaPagarRelInterfaceFacade {

	private static final long serialVersionUID = 1L;

	public ContaPagarRel() {
		super();
	}

	@Override
	public List<ContaPagarRelVO> executarConsultaParametrizada(Integer empresa, Integer fornecedor, Integer categoriaDespesa, Integer formaPagamento, Integer contaCorrente, String filtroData, Boolean trazerContaPaga, Boolean trazerContaAPagar, Date dataInicioVencimento, Date dataFinalVencimento, Date dataInicioCompetencia, Date dataFinalCompetencia, Date dataInicioPagamento, Date dataFinalPagamento, UsuarioVO usuarioVO) {
		List<ContaPagarRelVO> listaContaPagarRelVOs = consultarPorFiltros(empresa, fornecedor, categoriaDespesa, formaPagamento, contaCorrente, filtroData, trazerContaPaga, trazerContaAPagar, dataInicioVencimento, dataFinalVencimento, dataInicioCompetencia, dataFinalCompetencia, dataInicioPagamento, dataFinalPagamento, usuarioVO);
		return listaContaPagarRelVOs;
	}

	public List<ContaPagarRelVO> consultarPorFiltros(Integer empresa, Integer fornecedor, Integer categoriaDespesa, Integer formaPagamento, Integer contaCorrente, String filtroData, Boolean trazerContaPaga, Boolean trazerContaAPagar, Date dataInicioVencimento, Date dataFinalVencimento, Date dataInicioCompetencia, Date dataFinalCompetencia, Date dataInicioPagamento, Date dataFinalPagamento, UsuarioVO usuarioVO) {
		StringBuilder sb = new StringBuilder();
		sb.append("select distinct contapagar.codigo, contaPagar.data, contaPagar.situacao, contapagar.dataVencimento, contapagar.dataCompetencia, ");
		sb.append(" contapagar.valor, contapagar.valorTotal, contapagar.valorPago, contapagar.juro, contapagar.multa, contapagar.valorjuro, ");
		sb.append(" contapagar.valormulta, contapagar.desconto, contapagar.nrdocumento, contapagar.codigobarra, contapagar.parcela, contapagar.datapagamento, ");
		sb.append(" categoriaDespesa.codigo AS \"categoriaDespesa.codigo\", categoriaDespesa.descricao AS \"categoriaDespesa.descricao\", ");
		sb.append(" empresa.codigo AS \"empresa.codigo\", empresa.descricao, ");
		sb.append(" fornecedor.codigo AS \"fornecedor.codigo\", fornecedor.nome AS \"fornecedor.nome\" ");
		sb.append(" from contapagar ");
		sb.append(" left join empresa on empresa.codigo = contapagar.empresa ");
		sb.append(" inner join fornecedor on fornecedor.codigo = contapagar.fornecedor ");
		sb.append(" left join categoriaDespesa on categoriaDespesa.codigo = contapagar.categoriaDespesa ");
		sb.append(" left join contaPagarPagamento on contaPagarPagamento.contaPagar = contaPagar.codigo ");
		sb.append(" left join pagamento on pagamento.codigo = contaPagarPagamento.pagamento ");
		sb.append("");
		sb.append("");
		sb.append(" where 1=1 ");
		if (!empresa.equals(0)) {
			sb.append(" and empresa.codigo = ").append(empresa);
		}
		if (!fornecedor.equals(0)) {
			sb.append(" and fornecedor.codigo = ").append(fornecedor);
		}
		if (!categoriaDespesa.equals(0)) {
			sb.append(" and categoriaDespesa.codigo = ").append(categoriaDespesa);
		}
		if (!formaPagamento.equals(0)) {
			sb.append(" and exists(select formapagamentopagamento.formapagamento from contapagarpagamento ");
			sb.append(" inner join formapagamentopagamento on formapagamentopagamento.pagamento = contapagarpagamento.pagamento ");
			sb.append(" where contapagarpagamento.contapagar = contapagar.codigo ");
			sb.append(" and formapagamentopagamento.formapagamento = ").append(formaPagamento);
			sb.append(" limit 1 )");
		}
		if (!filtroData.equals("")) {
			if (filtroData.equals("DATA_VENCIMENTO")) {
				if (dataInicioVencimento != null) {
					sb.append(" and contaPagar.dataVencimento >= '").append(Uteis.getDataJDBC(dataInicioVencimento)).append("' ");
				}
				if (dataFinalVencimento != null) {
					sb.append(" and contaPagar.dataVencimento <= '").append(Uteis.getDataJDBC(dataFinalVencimento)).append("' ");
				}
			} else if (filtroData.equals("DATA_VENCIMENTO_&_DATA_COMPETENCIA")) {
				if (dataInicioVencimento != null) {
					sb.append(" and contaPagar.dataVencimento >= '").append(Uteis.getDataJDBC(dataInicioVencimento)).append("' ");
				}
				if (dataFinalVencimento != null) {
					sb.append(" and contaPagar.dataVencimento <= '").append(Uteis.getDataJDBC(dataFinalVencimento)).append("' ");
				}
				if (dataInicioCompetencia != null) {
					sb.append(" and contaPagar.dataCompetencia >= '").append(Uteis.getDataJDBC(dataInicioCompetencia)).append("' ");
				}
				if (dataFinalCompetencia != null) {
					sb.append(" and contaPagar.dataCompetencia <= '").append(Uteis.getDataJDBC(dataFinalCompetencia)).append("' ");
				}

			} else if (filtroData.equals("DATA_VENCIMENTO_&_DATA_PAGAMENTO")) {
				if (dataInicioVencimento != null) {
					sb.append(" and contaPagar.dataVencimento >= '").append(Uteis.getDataJDBC(dataInicioVencimento)).append("' ");
				}
				if (dataFinalVencimento != null) {
					sb.append(" and contaPagar.dataVencimento <= '").append(Uteis.getDataJDBC(dataFinalVencimento)).append("' ");
				}
				if (dataInicioPagamento != null) {
					sb.append(" and pagamento.dataPagamento >= '").append(Uteis.getDataJDBC(dataInicioPagamento)).append("' ");
				}
				if (dataFinalPagamento != null) {
					sb.append(" and pagamento.dataPagamento <= '").append(Uteis.getDataJDBC(dataFinalPagamento)).append("' ");
				}

			} else if (filtroData.equals("DATA_COMPETENCIA")) {
				if (dataInicioCompetencia != null) {
					sb.append(" and contaPagar.dataCompetencia >= '").append(Uteis.getDataJDBC(dataInicioCompetencia)).append("' ");
				}
				if (dataFinalCompetencia != null) {
					sb.append(" and contaPagar.dataCompetencia <= '").append(Uteis.getDataJDBC(dataFinalCompetencia)).append("' ");
				}
			} else if (filtroData.equals("DATA_COMPETENCIA_&_DATA_PAGAMENTO")) {
				if (dataInicioCompetencia != null) {
					sb.append(" and contaPagar.dataCompetencia >= '").append(Uteis.getDataJDBC(dataInicioCompetencia)).append("' ");
				}
				if (dataFinalCompetencia != null) {
					sb.append(" and contaPagar.dataCompetencia <= '").append(Uteis.getDataJDBC(dataFinalCompetencia)).append("' ");
				}
				if (dataInicioPagamento != null) {
					sb.append(" and pagamento.dataPagamento >= '").append(Uteis.getDataJDBC(dataInicioPagamento)).append("' ");
				}
				if (dataFinalPagamento != null) {
					sb.append(" and pagamento.dataPagamento <= '").append(Uteis.getDataJDBC(dataFinalPagamento)).append("' ");
				}

			} else if (filtroData.equals("DATA_PAGAMENTO")) {
				if (dataInicioPagamento != null) {
					sb.append(" and pagamento.dataPagamento >= '").append(Uteis.getDataJDBC(dataInicioPagamento)).append("' ");
				}
				if (dataFinalPagamento != null) {
					sb.append(" and pagamento.dataPagamento <= '").append(Uteis.getDataJDBC(dataFinalPagamento)).append("' ");
				}
			}
		}
		if (trazerContaAPagar || trazerContaPaga) {
			boolean virgula = false;
			sb.append(" and contaPagar.situacao in(");
			if (trazerContaAPagar) {
				if (!virgula) {
					sb.append(" '").append(SituacaoContaPagarEnum.A_PAGAR.name()).append("' ");
					virgula = true;
				} else {
					sb.append(", '").append(SituacaoContaPagarEnum.A_PAGAR.name()).append("' ");
				}
			}
			if (trazerContaPaga) {
				if (!virgula) {
					sb.append(" '").append(SituacaoContaPagarEnum.PAGO.name()).append("' ");
					virgula = true;
				} else {
					sb.append(", '").append(SituacaoContaPagarEnum.PAGO.name()).append("' ");
				}
			}
			sb.append(") ");
		}
		SqlRowSet tabelaResultado = getConexao().getJdbcTemplate().queryForRowSet(sb.toString());
		return montarDadosLista(tabelaResultado, usuarioVO);
	}

	public List<ContaPagarRelVO> montarDadosLista(SqlRowSet dadosSQL, UsuarioVO usuarioVO) {
		List<ContaPagarRelVO> listaContaPagarRelVOs = new ArrayList<ContaPagarRelVO>(0);
		while (dadosSQL.next()) {
			ContaPagarRelVO obj = new ContaPagarRelVO();
			montarDados(obj, dadosSQL, usuarioVO);
			listaContaPagarRelVOs.add(obj);
		}
		return listaContaPagarRelVOs;
	}

	public void montarDados(ContaPagarRelVO obj, SqlRowSet dadosSQL, UsuarioVO usuarioVO) {

	}

	public static String getDesignIReportRelatorio() {
		return ("relatorio" + File.separator + "designRelatorio" + File.separator + "financeiro" + File.separator + "ContaPagarRel" + ".jrxml");
	}

	public static String getCaminhoBaseRelatorio() {
		return "relatorio" + File.separator + "designRelatorio" + File.separator + "financeiro" + File.separator;
	}

}
