package relatorio.negocio.jdbc.arquitetura;

import java.io.StringWriter;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import negocio.comuns.administrativo.UsuarioVO;
import negocio.comuns.utilitarias.Uteis;
import negocio.facade.jdbc.arquitetura.ControleAcesso;

import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.jdbc.support.rowset.SqlRowSetMetaData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * SuperRelatorio � uma classe padr�o para que encapsulam as opera��es b�sicas
 * relativas a emiss�o de relat�rios. Verificar a permiss�o do usu�rio para
 * realizar a emiss�o de um determinado relat�rio.
 */
public class SuperRelatorio extends ControleAcesso {

	protected String xmlRelatorio;
	protected Vector ordenacoesRelatorio;
	protected int ordenarPor;
	private String descricaoFiltros;

	public SuperRelatorio() {
		setXmlRelatorio("");
		setOrdenacoesRelatorio(new Vector());
		setOrdenarPor(0);
		setDescricaoFiltros("");
	}

	public void adicionarDescricaoFiltro(String filtro) {
		if (!descricaoFiltros.equals("")) {
			descricaoFiltros = descricaoFiltros + ";" + filtro;
		} else {
			descricaoFiltros = filtro;
		}
	}

	/**
	 * Opera��o padr�o para realizar o EMITIR UM RELAT�RIO de dados de uma
	 * entidade no BD. Verifica e inicializa (se necess�rio) a conex�o com o BD.
	 * Verifica se o usu�rio logado possui permiss�o de acesso a opera��o
	 * EMITIRRELATORIO.
	 * 
	 * @param idEntidade
	 *            Nome da entidade para a qual se deseja realizar a opera��o.
	 * @exception Exception
	 *                Caso haja problemas de conex�o ou restri��o de acesso a
	 *                esta opera��o.
	 */
	public static void emitirRelatorio(String idEntidade, boolean verificarAcesso, UsuarioVO usuarioVO) throws Exception {
		
	}

	public String adicionarCondicionalWhere(String whereStr, String filtro, boolean operadorAND) {
		if (!operadorAND) {
			return filtro;
		} else {
			return whereStr + " AND " + filtro;
		}
	}

	public String getStringFromDocument(Document doc) {
		try {
			DOMSource domSource = new DOMSource(doc);
			StringWriter writer = new StringWriter();
			StreamResult result = new StreamResult(writer);
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			transformer.transform(domSource, result);
			String xmlConvertido = writer.toString();
			xmlConvertido = xmlConvertido.replaceFirst("UTF-8", "ISO-8859-1");
			return xmlConvertido;
		} catch (TransformerException ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public void converterResultadoConsultaParaXML(SqlRowSet resultadoConsulta, String nomeRelatorio, String nomeRegistro) throws Exception {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.newDocument();
		// Criando o n� raiz do XML - com o nome padr�o do relat�rio
		Element root = doc.createElement(nomeRelatorio);
		// Obtendo as informa��es de estrutura do ResultSet - METADados
		SqlRowSetMetaData metaDadosConsulta = resultadoConsulta.getMetaData();

		Element linhaXML;
		Element e;

		int contador = 0;

		int cols = metaDadosConsulta.getColumnCount();
		resultadoConsulta.beforeFirst();
		while (resultadoConsulta.next()) {
			contador++;
			linhaXML = doc.createElement(nomeRegistro);

			for (int j = 1; j <= cols; j++) {
				String nomeTabela = metaDadosConsulta.getTableName(j);
				String nomeColuna = metaDadosConsulta.getColumnName(j);
				String nomeClasse = metaDadosConsulta.getColumnClassName(j);
				String valorColuna = " ";
				if (nomeClasse.equals("java.sql.Timestamp")) {
					valorColuna = Uteis.getData(resultadoConsulta.getDate(j));
					if ((valorColuna == null) || (valorColuna.equals("")) || (valorColuna.equals("null"))) {
						valorColuna = " ";
					}
				} else if (nomeClasse.equals("java.lang.Double")) {
					valorColuna = String.valueOf(resultadoConsulta.getDouble(j));
					if (valorColuna == null) {
						valorColuna = "0.0";
					}
				} else if (nomeClasse.equals("java.lang.Float")) {
					valorColuna = String.valueOf(resultadoConsulta.getFloat(j));
					if (valorColuna == null) {
						valorColuna = "0.0";
					}
				} else {
					valorColuna = resultadoConsulta.getString(j);
					// if (nomeColuna.equals("dia") || nomeColuna.equals("mes"))
					// {
					// int posicao = valorColuna.lastIndexOf(".");
					// valorColuna.substring(0,posicao + 1);
					// valorColuna = " ";
					// }
					if ((valorColuna == null) || (valorColuna.equals("")) || (valorColuna.equals("null"))) {
						valorColuna = " ";
					}
				}

				e = doc.createElement(nomeColuna);
				e.appendChild(doc.createTextNode(valorColuna));
				linhaXML.appendChild(e);
			}
			root.appendChild(linhaXML);
		}
		if (contador == 0) {
			throw new Exception("N�o h� resultados a serem exibidos neste relat�rio.");
		}
		doc.appendChild(root);
		this.setXmlRelatorio(getStringFromDocument(doc));

		/*
		 * //perform XSL transformation DOMSource source = new DOMSource(doc);
		 * //OutputStream outputStream = new OutputStream(); OutputStream buffer
		 * = new OutputStream(); OutputStreamWriter outSW = new
		 * OutputStreamWriter(buffer, "ISO-8859-1"); StreamResult result = new
		 * StreamResult("customers.xml"); TransformerFactory tmf =
		 * TransformerFactory.newInstance();
		 * tmf.newTransformer().transform(source,result); //result.getWriter()
		 * FileOutputStream outFile = new
		 * FileOutputStream(this.getArquivoCodigo()); String conteudo =
		 * this.getText(); for (int i =0; i < conteudo.length(); i++) {
		 * outSW.append(conteudo.charAt(i)); } outSW.close();
		 */
	}

	public String getXmlRelatorio() {
		return xmlRelatorio;
	}

	public void setXmlRelatorio(String xmlRelatorio) {
		this.xmlRelatorio = xmlRelatorio;
	}

	public Vector getOrdenacoesRelatorio() {
		return ordenacoesRelatorio;
	}

	public void setOrdenacoesRelatorio(Vector ordenacoesRelatorio) {
		this.ordenacoesRelatorio = ordenacoesRelatorio;
	}

	public int getOrdenarPor() {
		return ordenarPor;
	}

	public void setOrdenarPor(int ordenarPor) {
		this.ordenarPor = ordenarPor;
	}

	public String getDescricaoFiltros() {
		return descricaoFiltros;
	}

	public void setDescricaoFiltros(String descricaoFiltros) {
		this.descricaoFiltros = descricaoFiltros;
	}
	
	

//	public String getFiltroSituacaoParcelaMatricula(FiltroRelatorioAcademicoVO filtroRelatorioAcademicoVO, String campo) {
//		StringBuilder sqlStr = new StringBuilder();
//		campo = campo.trim();
//		if (filtroRelatorioAcademicoVO.getMatriculaAReceber() && !filtroRelatorioAcademicoVO.getMatriculaRecebida()) {
//			sqlStr.append("  ").append(campo).append(".situacao = 'PF'");
//		} else if (filtroRelatorioAcademicoVO.getMatriculaAReceber() && !filtroRelatorioAcademicoVO.getMatriculaRecebida()) {
//			sqlStr.append("  ").append(campo).append(".situacao != 'PF'");
//		}else{
//			sqlStr.append("  ").append(campo).append(".situacao != ''");
//		}
//		return sqlStr.toString();
//	}

//	public String getFiltroSituacaoAcademica(FiltroRelatorioAcademicoVO filtroRelatorioAcademicoVO, String campo) {
//		StringBuilder sqlStr = new StringBuilder();
//		campo = campo.trim();
//		sqlStr.append(" ").append(campo).append(".situacaomatriculaperiodo in (''");
//
//		if (filtroRelatorioAcademicoVO.getAtivo()) {
//			sqlStr.append(", 'AT'");
//		}
//		if (filtroRelatorioAcademicoVO.getPreMatricula()) {
//			sqlStr.append(", 'PR'");
//		}
//		if (filtroRelatorioAcademicoVO.getPreMatriculaCancelada()) {
//			sqlStr.append(", 'PC'");
//		}
//		if (filtroRelatorioAcademicoVO.getTrancado()) {
//			sqlStr.append(", 'TR'");
//		}
//		if (filtroRelatorioAcademicoVO.getConcluido()) {
//			sqlStr.append(", 'FI'");
//		}
//		if (filtroRelatorioAcademicoVO.getFormado()) {
//			sqlStr.append(", 'FO'");
//		}
//		if (filtroRelatorioAcademicoVO.getTransferenciaExterna()) {
//			sqlStr.append(", 'TS'");
//		}
//		if (filtroRelatorioAcademicoVO.getTransferenciaInterna()) {
//			sqlStr.append(", 'TI'");
//		}
//		if (filtroRelatorioAcademicoVO.getAbandonado()) {
//			sqlStr.append(", 'AC'");
//		}
//		if (filtroRelatorioAcademicoVO.getCancelado()) {
//			sqlStr.append(", 'CA'");
//		}
//		sqlStr.append(") ");
//		return sqlStr.toString();		
//	}
}
