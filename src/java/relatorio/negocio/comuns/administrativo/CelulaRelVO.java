/**
 * 
 */
package relatorio.negocio.comuns.administrativo;

import java.io.Serializable;

import negocio.comuns.arquitetura.SuperVO;

/**
 * @author Carlos Eug�nio
 *
 */
public class CelulaRelVO extends SuperVO implements Serializable {

	private static final long serialVersionUID = 1L;
	private String celula;

	public CelulaRelVO() {
		super();
	}
}
