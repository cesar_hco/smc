/*
 * To change this template, choose Tools | Templates and open the template in the editor.
 */
package relatorio.arquitetura;

import java.io.File;
import java.io.Serializable;

import negocio.facade.jdbc.arquitetura.SuperArquitetura;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanArrayDataSource;
import net.sf.jasperreports.engine.export.JRHtmlExporter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporterParameter;
import net.sf.jasperreports.engine.export.ooxml.JRPptxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import relatorio.negocio.comuns.arquitetura.SuperParametroRelVO;
import relatorio.negocio.comuns.arquitetura.enumeradores.TipoRelatorioEnum;

/**
 * 
 * @author brethener
 */
public class GeradorRelatorio extends SuperArquitetura implements Serializable {

	public JasperPrint gerarRelatorioJasperPrintObjeto(SuperParametroRelVO superRelVO) throws Exception {
		try {
			JRDataSource jr = new JRBeanArrayDataSource(superRelVO.getListaObjetos().toArray());
			String nomeJasperReportDesignIReport = superRelVO.getNomeDesignIreport().substring(0, superRelVO.getNomeDesignIreport().lastIndexOf(".")) + ".jasper";
			File arquivoIReport = new File(getCaminhoClassesAplicacao() + File.separator + nomeJasperReportDesignIReport);
			JasperReport jasperReport = (JasperReport) JRLoader.loadObject(arquivoIReport);
			JasperPrint print = JasperFillManager.fillReport(jasperReport, superRelVO.getParametros(), jr);
			jr = null;
			nomeJasperReportDesignIReport = null;
			arquivoIReport = null;
			jasperReport = null;
			return print;
		} catch (Exception e) {
			throw e;
		}

	}

	public String realizarExportacaoRelatorio(SuperParametroRelVO superRelVO) throws Exception {
		JasperPrint print = gerarRelatorioJasperPrintObjeto(superRelVO);
		if (superRelVO.getTipoRelatorioEnum().equals(TipoRelatorioEnum.PDF)) {
			return realizarExportacaoPDF(superRelVO, print);
		} else if (superRelVO.getTipoRelatorioEnum().equals(TipoRelatorioEnum.EXCEL)) {
			return realizarExportacaoEXCEL(superRelVO, print);
		} else if (superRelVO.getTipoRelatorioEnum().equals(TipoRelatorioEnum.HTML)) {
			return realizarExportacaoHTML(superRelVO, print);
		} else if (superRelVO.getTipoRelatorioEnum().equals(TipoRelatorioEnum.DOC)) {
			return realizarExportacaoDOC(superRelVO, print);
		} else if (superRelVO.getTipoRelatorioEnum().equals(TipoRelatorioEnum.IMAGEM)) {
			return realizarExportacaoIMAGEM(superRelVO, print);
		} else {
			return realizarExportacaoPPT(superRelVO, print);
		}
	}

	public static String realizarExportacaoPDF(SuperParametroRelVO superParametroRelVO, String caminhoBaseSalvarArquivo, String nomeArquivo) throws Exception {

		File pdfFile = new File(caminhoBaseSalvarArquivo);
		if (!pdfFile.exists()) {
			pdfFile.mkdirs();
		}
		pdfFile = new File(caminhoBaseSalvarArquivo + File.separator + nomeArquivo);
		if (pdfFile.exists()) {
			try {
				pdfFile.delete();
			} catch (Exception e) {
				throw new Exception("Erro ao exportar arquivo em PDF.");
			}
		}

		JRPdfExporter jrpdfexporter = new JRPdfExporter();
		jrpdfexporter.setParameter(JRExporterParameter.JASPER_PRINT, new GeradorRelatorio().gerarRelatorioJasperPrintObjeto(superParametroRelVO));
		jrpdfexporter.setParameter(JRExporterParameter.OUTPUT_FILE, pdfFile);
		jrpdfexporter.exportReport();
		jrpdfexporter = null;
		return pdfFile.getName();
		// return pdfFile.getAbsolutePath();
	}

	public String realizarExportacaoPDF(SuperParametroRelVO superRelVO, JasperPrint print) throws Exception {
		String nome = superRelVO.getNomeRelatorio() + ".pdf";
		String nomeRel = "relatorio" + File.separator + nome;
		File pdfFile = new File(getCaminhoPastaWeb() + File.separator + nomeRel);
		if (pdfFile.exists()) {
			try {
				pdfFile.delete();
			} catch (Exception e) {
				throw new Exception("Erro ao exportar arquivo em PDF.");
			}
		}

		JRPdfExporter jrpdfexporter = new JRPdfExporter();
		jrpdfexporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
		jrpdfexporter.setParameter(JRExporterParameter.OUTPUT_FILE, pdfFile);
		jrpdfexporter.exportReport();
		nome = null;
		nomeRel = null;
		jrpdfexporter = null;
		return pdfFile.getName();
		// return pdfFile.getAbsolutePath();
	}

	public static String realizarExportacaoEXCEL(SuperParametroRelVO superParametroRelVO, String caminhoBaseSalvarArquivo, String nomeArquivo) throws Exception {
		File excelFile = new File(caminhoBaseSalvarArquivo + File.separator + nomeArquivo);
		if (excelFile.exists()) {
			try {
				excelFile.delete();
			} catch (Exception e) {
				throw new Exception("Erro ao exportar arquivo em Excel.");
			}
		}

		JRXlsxExporter jrpdfexporter = new JRXlsxExporter();
		jrpdfexporter.setParameter(JRExporterParameter.JASPER_PRINT, new GeradorRelatorio().gerarRelatorioJasperPrintObjeto(superParametroRelVO));
		jrpdfexporter.setParameter(JRExporterParameter.OUTPUT_FILE, excelFile);
		jrpdfexporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
		jrpdfexporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, Boolean.TRUE);
		jrpdfexporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);
		jrpdfexporter.exportReport();
		jrpdfexporter = null;
		return excelFile.getName();

	}

	public String realizarExportacaoEXCEL(SuperParametroRelVO superRelVO, JasperPrint print) throws Exception {
		String nome = superRelVO.getNomeRelatorio() + ".xlsx";
		String nomeRel = "relatorio" + File.separator + nome;
		File excelFile = new File(getCaminhoPastaWeb() + File.separator + nomeRel);
		if (excelFile.exists()) {
			try {
				excelFile.delete();
			} catch (Exception e) {
				throw new Exception("Erro ao exportar arquivo em EXCEL.");
			}
		}

		JRXlsxExporter jrpdfexporter = new JRXlsxExporter();
		jrpdfexporter.setParameter(JRExporterParameter.OUTPUT_FILE, excelFile);
		jrpdfexporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
		jrpdfexporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
		jrpdfexporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_COLUMNS, Boolean.TRUE);

		jrpdfexporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.FALSE);

		jrpdfexporter.exportReport();
		nome = null;
		nomeRel = null;
		jrpdfexporter = null;
		return excelFile.getName();
		// return excelFile.getAbsolutePath();
	}

	public String realizarExportacaoHTML(SuperParametroRelVO superRelVO, JasperPrint print) throws Exception {
		String nome = superRelVO.getNomeRelatorio() + ".html";
		String nomeRel = "relatorio" + File.separator + nome;
		File htmlFile = new File(getCaminhoPastaWeb() + File.separator + nomeRel);
		if (htmlFile.exists()) {
			try {
				htmlFile.delete();
			} catch (Exception e) {
				throw new Exception("Erro ao exportar arquivo em HTML.");
			}
		}

		JRHtmlExporter jrhtmlexporter = new JRHtmlExporter();
		jrhtmlexporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
		jrhtmlexporter.setParameter(JRExporterParameter.OUTPUT_FILE, htmlFile);
		jrhtmlexporter.setParameter(JRExporterParameter.CHARACTER_ENCODING, "ISO-8859-1");
		jrhtmlexporter.exportReport();
		nome = null;
		nomeRel = null;
		jrhtmlexporter = null;
		return htmlFile.getName();
		// return htmlFile.getAbsolutePath();
	}

	public String realizarExportacaoDOC(SuperParametroRelVO superRelVO, JasperPrint print) throws Exception {
		String nome = superRelVO.getNomeRelatorio() + ".docx";
		String nomeRel = "relatorio" + File.separator + nome;
		File docFile = new File(getCaminhoPastaWeb() + File.separator + nomeRel);
		if (docFile.exists()) {
			try {
				docFile.delete();
			} catch (Exception e) {
				throw new Exception("Erro ao exportar arquivo em DOC.");
			}
		}
		JRDocxExporter jrDocxExporter = new JRDocxExporter();
		jrDocxExporter.setParameter(JRDocxExporterParameter.JASPER_PRINT, print);
		jrDocxExporter.setParameter(JRDocxExporterParameter.OUTPUT_FILE, docFile);
		jrDocxExporter.exportReport();
		nome = null;
		nomeRel = null;
		jrDocxExporter = null;
		return docFile.getName();
		// return docFile.getAbsolutePath();
	}

	public String realizarExportacaoIMAGEM(SuperParametroRelVO superRelVO, JasperPrint print) throws Exception {
		String nome = superRelVO.getNomeRelatorio() + ".jpg";
		String nomeRel = "relatorio" + File.separator + nome;
		File pdfFile = new File(getCaminhoPastaWeb() + File.separator + nomeRel);
		if (pdfFile.exists()) {
			try {
				pdfFile.delete();
			} catch (Exception e) {
				throw new Exception("Erro ao exportar arquivo em JPG.");
			}
		}
		JRPdfExporter jrpdfexporter = new JRPdfExporter();
		jrpdfexporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
		jrpdfexporter.setParameter(JRExporterParameter.OUTPUT_FILE, pdfFile);
		jrpdfexporter.exportReport();

		// //File pdfFile = new File(fileDir);
		// RandomAccessFile raf = new RandomAccessFile(pdfFile, "r");
		// FileChannel channel = raf.getChannel();
		// ByteBuffer buf = channel.map(FileChannel.MapMode.READ_ONLY, 0,
		// channel.size());
		// PDFFile pdf = new PDFFile(buf);
		// PDFPage page = pdf.getPage(1);
		//
		// Rectangle rect = new Rectangle(0, 0, (int) page.getBBox().getWidth(),
		// (int) page.getBBox().getHeight());
		// BufferedImage bufferedImage = new BufferedImage(rect.width,
		// rect.height,
		// BufferedImage.TYPE_INT_RGB);
		//
		// Image image = page.getImage(rect.width, rect.height,
		// rect,
		// null,
		// true,
		// true);
		// Graphics2D bufImageGraphics = bufferedImage.createGraphics();
		// bufImageGraphics.drawImage(image, 0, 0, null);
		// ImageIO.write(bufferedImage, "jpg", new File(dir + "imagem.jpg"));
		//
		// nome = null;
		// nomeRel = null;
		// jrpdfexporter = null;
		//
		return pdfFile.getName();

		// return docFile.getAbsolutePath();
	}

	public String realizarExportacaoPPT(SuperParametroRelVO superRelVO, JasperPrint print) throws Exception {
		String nome = superRelVO.getNomeRelatorio() + ".pptx";
		String nomeRel = "relatorio" + File.separator + nome;
		File pptFile = new File(getCaminhoPastaWeb() + File.separator + nomeRel);
		if (pptFile.exists()) {
			try {
				pptFile.delete();
			} catch (Exception e) {
				throw new Exception("Erro ao exportar arquivo em PPT.");
			}
		}
		JRPptxExporter jrPptxExporter = new JRPptxExporter();
		jrPptxExporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
		jrPptxExporter.setParameter(JRExporterParameter.OUTPUT_FILE, pptFile);
		jrPptxExporter.exportReport();
		nome = null;
		nomeRel = null;
		jrPptxExporter = null;
		return pptFile.getName();
		// return pptFile.getAbsolutePath();
	}
}
