package relatorio.controle.financeiro;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.model.SelectItem;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import negocio.comuns.administrativo.EmpresaVO;
import negocio.comuns.arquitetura.enumeradores.MensagemAlertaEnum;
import negocio.comuns.arquitetura.enumeradores.NivelMontarDadosEnum;
import negocio.comuns.basico.FornecedorVO;
import negocio.comuns.financeiro.CategoriaDespesaVO;
import negocio.comuns.financeiro.FormaPagamentoVO;
import negocio.comuns.utilitarias.Uteis;
import relatorio.controle.arquitetura.SuperControleRelatorio;
import relatorio.negocio.comuns.arquitetura.enumeradores.TipoRelatorioEnum;
import relatorio.negocio.comuns.financeiro.ContaPagarRelVO;
import relatorio.negocio.jdbc.financeiro.ContaPagarRel;

@Controller("contaPagarRelControle")
@Scope("viewScope")
@Lazy
public class ContaPagarRelControle extends SuperControleRelatorio{

	private static final long serialVersionUID = 1L;
	private Date dataInicioVencimento;
	private Date dataFinalVencimento;
	private Date dataInicioCompetencia;
	private Date dataFinalCompetencia;
	private Date dataInicioPagamento;
	private Date dataFinalPagamento;
	private EmpresaVO empresaVO;
	private FornecedorVO fornecedorVO;
	private CategoriaDespesaVO categoriaDespesaVO;
	private FormaPagamentoVO formaPagamentoVO;
	private String tipoConta;
	private String filtroData;
	private Boolean trazerContaPaga;
	private Boolean trazerContaAPagar;
	private List<SelectItem> listaSelectItemEmpresaVOs;
	private String campoConsultaFornecedor;
	private String valorConsultaFornecedor;
	private List<FornecedorVO> listaConsultaFornecedorVOs;

	public ContaPagarRelControle() {
		super();
	}
	
	public void imprimirPDF() {
        List<ContaPagarRelVO> listaContaPagarRelVOs = new ArrayList<ContaPagarRelVO>(0);
        try {
            listaContaPagarRelVOs = getFacadeFactory().getContaPagarRelFacade().executarConsultaParametrizada(getEmpresaVO().getCodigo(), getFornecedorVO().getCodigo(), getCategoriaDespesaVO().getCodigo(), getFormaPagamentoVO().getCodigo(), 0, getFiltroData(), getTrazerContaPaga(), getTrazerContaAPagar(), getDataInicioVencimento(), getDataFinalVencimento(), getDataInicioCompetencia(), getDataFinalCompetencia(), getDataInicioPagamento(), getDataFinalPagamento(), getUsuarioLogado());
         
            if (!listaContaPagarRelVOs.isEmpty()) {
            	getSuperParametroRelVO().setNomeDesignIreport(ContaPagarRel.getDesignIReportRelatorio());
    			getSuperParametroRelVO().setTipoRelatorioEnum(TipoRelatorioEnum.PDF);
    			getSuperParametroRelVO().setSubReport_Dir(ContaPagarRel.getCaminhoBaseRelatorio());
    			getSuperParametroRelVO().setNomeUsuario(getUsuarioLogado().getPessoaVO().getNome());
    			getSuperParametroRelVO().setTituloRelatorio("Relat�rio - Cadastro da C�lula");
    			getSuperParametroRelVO().setListaObjetos(listaContaPagarRelVOs);
    			getSuperParametroRelVO().setCaminhoBaseRelatorio(ContaPagarRel.getCaminhoBaseRelatorio());
    			getSuperParametroRelVO().setQuantidade(listaContaPagarRelVOs.size());
    			realizarImpressaoRelatorio();
    			setMensagemID("msg_relatorio_ok", MensagemAlertaEnum.SUCESSO);
            } else {
                setMensagemID("msg_relatorio_sem_dados", MensagemAlertaEnum.ATENCAO);
            }
        } catch (Exception e) {
            setMensagemDetalhada("", e.getMessage());
        } finally {
        }
    }
	
	public void montarListaSelectItemEmpresa() {
		List<EmpresaVO> listaEmpresaVOs = getFacadeFactory().getEmpresaFacade().consultarPorDescricao("", NivelMontarDadosEnum.BASICO, getUsuarioLogado());
		List<SelectItem> itens = new ArrayList<SelectItem>(0);
		itens.add(new SelectItem("", ""));
		for (EmpresaVO empresaVO : listaEmpresaVOs) {
			itens.add(new SelectItem(empresaVO.getCodigo(), empresaVO.getDescricao()));
		}
		setListaSelectItemEmpresaVOs(itens);
	}
	
	public void consultarFornecedor() {
		try {
			setListaConsultaFornecedorVOs(getFacadeFactory().getFornecedorFacade().consultar(getCampoConsultaFornecedor(), getValorConsultaFornecedor(), NivelMontarDadosEnum.BASICO, getUsuarioLogado()));
			setMensagemID("msg_dados_consultados", MensagemAlertaEnum.ATENCAO);
		} catch (Exception e) {
			setMensagemDetalhada("", e.getMessage());
		}
	}

	public void selecionarFornecedor() {
		FornecedorVO obj = (FornecedorVO) context().getExternalContext().getRequestMap().get("fornecedorItem");
		setFornecedorVO(obj);
		setMensagemID("msg_dados_selecionados", MensagemAlertaEnum.ATENCAO);
	}

	public void limparFornecedor() {
		setFornecedorVO(null);
	}

	public void limparDadosConsultaModalFornecedor() {
		setListaConsultaFornecedorVOs(null);
	}

	public List<SelectItem> getListaSelectItemFornecedorVOs() {
		List<SelectItem> itens = new ArrayList<SelectItem>(0);
		itens.add(new SelectItem("NOME", "Nome"));
		return itens;
	}

	public Date getDataInicioVencimento() {
		if (dataInicioVencimento == null) {
			dataInicioVencimento = Uteis.getDataPrimeiroDiaMes(new Date());
		}
		return dataInicioVencimento;
	}

	public Date getDataFinalVencimento() {
		if (dataFinalVencimento == null) {
			dataFinalVencimento = new Date();
		}
		return dataFinalVencimento;
	}

	public Date getDataInicioCompetencia() {
		return dataInicioCompetencia;
	}

	public Date getDataFinalCompetencia() {
		return dataFinalCompetencia;
	}

	public EmpresaVO getEmpresaVO() {
		if (empresaVO == null) {
			empresaVO = new EmpresaVO();
		}
		return empresaVO;
	}

	public FornecedorVO getFornecedorVO() {
		if (fornecedorVO == null) {
			fornecedorVO = new FornecedorVO();
		}
		return fornecedorVO;
	}

	public CategoriaDespesaVO getCategoriaDespesaVO() {
		if (categoriaDespesaVO == null) {
			categoriaDespesaVO = new CategoriaDespesaVO();
		}
		return categoriaDespesaVO;
	}

	public String getTipoConta() {
		if (tipoConta == null) {
			tipoConta = "";
		}
		return tipoConta;
	}

	public String getFiltroData() {
		if (filtroData == null) {
			filtroData = "";
		}
		return filtroData;
	}

	public Boolean getTrazerContaPaga() {
		if (trazerContaPaga == null) {
			trazerContaPaga = true;
		}
		return trazerContaPaga;
	}

	public Boolean getTrazerContaAPagar() {
		if (trazerContaAPagar == null) {
			trazerContaAPagar = true;
		}
		return trazerContaAPagar;
	}

	public void setDataInicioVencimento(Date dataInicioVencimento) {
		this.dataInicioVencimento = dataInicioVencimento;
	}

	public void setDataFinalVencimento(Date dataFinalVencimento) {
		this.dataFinalVencimento = dataFinalVencimento;
	}

	public void setDataInicioCompetencia(Date dataInicioCompetencia) {
		this.dataInicioCompetencia = dataInicioCompetencia;
	}

	public void setDataFinalCompetencia(Date dataFinalCompetencia) {
		this.dataFinalCompetencia = dataFinalCompetencia;
	}

	public void setEmpresaVO(EmpresaVO empresaVO) {
		this.empresaVO = empresaVO;
	}

	public void setFornecedorVO(FornecedorVO fornecedorVO) {
		this.fornecedorVO = fornecedorVO;
	}

	public void setCategoriaDespesaVO(CategoriaDespesaVO categoriaDespesaVO) {
		this.categoriaDespesaVO = categoriaDespesaVO;
	}

	public void setTipoConta(String tipoConta) {
		this.tipoConta = tipoConta;
	}

	public void setFiltroData(String filtroData) {
		this.filtroData = filtroData;
	}

	public void setTrazerContaPaga(Boolean trazerContaPaga) {
		this.trazerContaPaga = trazerContaPaga;
	}

	public void setTrazerContaAPagar(Boolean trazerContaAPagar) {
		this.trazerContaAPagar = trazerContaAPagar;
	}

	public FormaPagamentoVO getFormaPagamentoVO() {
		if (formaPagamentoVO == null) {
			formaPagamentoVO = new FormaPagamentoVO();
		}
		return formaPagamentoVO;
	}

	public void setFormaPagamentoVO(FormaPagamentoVO formaPagamentoVO) {
		this.formaPagamentoVO = formaPagamentoVO;
	}

	public Date getDataInicioPagamento() {
		return dataInicioPagamento;
	}

	public Date getDataFinalPagamento() {
		return dataFinalPagamento;
	}

	public void setDataInicioPagamento(Date dataInicioPagamento) {
		this.dataInicioPagamento = dataInicioPagamento;
	}

	public void setDataFinalPagamento(Date dataFinalPagamento) {
		this.dataFinalPagamento = dataFinalPagamento;
	}

	public List<SelectItem> getListaSelectItemEmpresaVOs() {
		if (listaSelectItemEmpresaVOs == null) {
			listaSelectItemEmpresaVOs = new ArrayList<SelectItem>(0);
		}
		return listaSelectItemEmpresaVOs;
	}

	public void setListaSelectItemEmpresaVOs(List<SelectItem> listaSelectItemEmpresaVOs) {
		this.listaSelectItemEmpresaVOs = listaSelectItemEmpresaVOs;
	}

	public String getCampoConsultaFornecedor() {
		if (campoConsultaFornecedor == null) {
			campoConsultaFornecedor = "";
		}
		return campoConsultaFornecedor;
	}

	public String getValorConsultaFornecedor() {
		if (valorConsultaFornecedor == null) {
			valorConsultaFornecedor = "";
		}
		return valorConsultaFornecedor;
	}

	public List<FornecedorVO> getListaConsultaFornecedorVOs() {
		if (listaConsultaFornecedorVOs == null) {
			listaConsultaFornecedorVOs = new ArrayList<FornecedorVO>(0);
		}
		return listaConsultaFornecedorVOs;
	}

	public void setCampoConsultaFornecedor(String campoConsultaFornecedor) {
		this.campoConsultaFornecedor = campoConsultaFornecedor;
	}

	public void setValorConsultaFornecedor(String valorConsultaFornecedor) {
		this.valorConsultaFornecedor = valorConsultaFornecedor;
	}

	public void setListaConsultaFornecedorVOs(List<FornecedorVO> listaConsultaFornecedorVOs) {
		this.listaConsultaFornecedorVOs = listaConsultaFornecedorVOs;
	}
}
