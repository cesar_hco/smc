package relatorio.controle.arquitetura;

import java.io.File;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;

import negocio.comuns.utilitarias.UteisEmail;
import relatorio.arquitetura.GeradorRelatorio;
import relatorio.negocio.comuns.arquitetura.SuperParametroRelVO;
import controle.arquitetura.LoginControle;
import controle.arquitetura.SuperControle;

public abstract class SuperControleRelatorio extends SuperControle implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 6779076600525663253L;
	protected Integer opcaoOrdenacao;
    protected List<SelectItem> listaSelectItemOrdenacoesRelatorio;
    private SuperParametroRelVO superParametroRelVO;
    protected String usarTargetBlank;
    protected Boolean fazerDownload;
    private GeradorRelatorio geradorRelatorio = new GeradorRelatorio();
    String caminhoRelatorio = null;

    public String getCaminhoRelatorio() {
        if (caminhoRelatorio == null) {
            caminhoRelatorio = "";
        }
        return caminhoRelatorio;
    }

    public void setCaminhoRelatorio(String caminhoRelatorio) {
        this.caminhoRelatorio = caminhoRelatorio;
    }

    public void setSuperParametroRelVO(SuperParametroRelVO superParametroRelVO) {
        this.superParametroRelVO = superParametroRelVO;
    }

    public SuperControleRelatorio() {
    }

    
    
    public void apresentarRelatorio(String nomeRelatorio, String xml, String tituloRelatorio, String nomeEmpresa, String mensagemRel, String tipoRelatorio, String parserBuscaTags, String designIReport, String nomeUsuario, String filtros) throws Exception {
        HttpServletRequest request = (HttpServletRequest) context().getExternalContext().getRequest();
        request.setAttribute("xmlRelatorio", xml);
        request.setAttribute("tipoRelatorio", tipoRelatorio);
        request.setAttribute("nomeRelatorio", nomeRelatorio);
        request.setAttribute("nomeEmpresa", nomeEmpresa);
        request.setAttribute("mensagemRel", mensagemRel);
        request.setAttribute("tituloRelatorio", tituloRelatorio);
        request.setAttribute("caminhoParserXML", parserBuscaTags);
        request.setAttribute("nomeDesignIReport", designIReport);
        request.setAttribute("nomeUsuario", nomeUsuario);
        if (filtros.equals("")) {
            filtros = "nenhum";
        }
        request.setAttribute("filtros", filtros);
        context().getExternalContext().dispatch("/VisualizadorRelatorio");
        FacesContext.getCurrentInstance().responseComplete();
    }

    @SuppressWarnings("rawtypes")
	public void apresentarRelatorioObjetos(String nomeRelatorio, String tituloRelatorio, String nomeEmpresa, String mensagemRel, String tipoRelatorio, String parserBuscaTags, String designIReport, String nomeUsuario, String filtros, List listaObjetos, String caminhoBaseRelatorio) throws Exception {
        try {

            if (listaObjetos == null || listaObjetos.isEmpty()) {
                throw new Exception("N�o h� resultados a serem exibidos neste relat�rio.");
            }
            HttpServletRequest request = (HttpServletRequest) context().getExternalContext().getRequest();
            request.setAttribute("tipoRelatorio", tipoRelatorio);
            request.setAttribute("nomeRelatorio", nomeRelatorio);
            request.setAttribute("nomeEmpresa", nomeEmpresa);
            request.setAttribute("mensagemRel", mensagemRel);
            request.setAttribute("tituloRelatorio", tituloRelatorio);
            request.setAttribute("caminhoParserXML", parserBuscaTags);
            request.setAttribute("nomeDesignIReport", designIReport);
            request.setAttribute("nomeUsuario", nomeUsuario);
            request.setAttribute("listaObjetos", listaObjetos);
            request.setAttribute("tipoImplementacao", "OBJETO");
            request.setAttribute("caminhoBaseRelatorio", caminhoBaseRelatorio);
            if (filtros.equals("")) {
                filtros = "nenhum";
            }
            request.setAttribute("filtros", filtros);
            context().getExternalContext().dispatch("/VisualizadorRelatorio");
            FacesContext.getCurrentInstance().responseComplete();

        } catch (Exception e) {
            setMensagemDetalhada("msg_erro", e.getMessage());
        }

    }

    @SuppressWarnings("rawtypes")
	public void apresentarRelatorioObjetosTotalizador(String nomeRelatorio, String tituloRelatorio, String nomeEmpresa, String mensagemRel, String tipoRelatorio, String parserBuscaTags, String designIReport, String nomeUsuario, String filtros, List listaObjetos, Integer total, String caminhoBaseRelatorio) throws Exception {
        try {

            if (listaObjetos == null || listaObjetos.isEmpty()) {
                throw new Exception("N�o h� resultados a serem exibidos neste relat�rio.");
            }
            HttpServletRequest request = (HttpServletRequest) context().getExternalContext().getRequest();
            request.setAttribute("tipoRelatorio", tipoRelatorio);
            request.setAttribute("nomeRelatorio", nomeRelatorio);
            request.setAttribute("nomeEmpresa", nomeEmpresa);
            request.setAttribute("mensagemRel", mensagemRel);
            request.setAttribute("tituloRelatorio", tituloRelatorio);
            request.setAttribute("caminhoParserXML", parserBuscaTags);
            request.setAttribute("nomeDesignIReport", designIReport);
            request.setAttribute("nomeUsuario", nomeUsuario);
            request.setAttribute("listaObjetos", listaObjetos);
            request.setAttribute("tipoImplementacao", "OBJETO");
            request.setAttribute("caminhoBaseRelatorio", caminhoBaseRelatorio);
            request.setAttribute("total", total);
            if (filtros.equals("")) {
                filtros = "nenhum";
            }
            request.setAttribute("filtros", filtros);
            context().getExternalContext().dispatch("/VisualizadorRelatorio");
            FacesContext.getCurrentInstance().responseComplete();

        } catch (Exception e) {
            setMensagemDetalhada("msg_erro", e.getMessage());
        }

    }

    @SuppressWarnings("rawtypes")
	public void apresentarRelatorioObjetosBoleto(String nomeRelatorio, String tituloRelatorio, String nomeEmpresa, String mensagemRel, String tipoRelatorio, String parserBuscaTags, String designIReport, String nomeUsuario, String filtros, List listaObjetos, String caminhoBaseRelatorio, List<InputStream> imagemBoleto) throws Exception {

        HttpServletRequest request = (HttpServletRequest) context().getExternalContext().getRequest();
        request.setAttribute("imagemBoleto", imagemBoleto);
        apresentarRelatorioObjetos(nomeRelatorio, tituloRelatorio, nomeEmpresa, mensagemRel, tipoRelatorio, parserBuscaTags, designIReport, nomeUsuario, filtros,
                listaObjetos, caminhoBaseRelatorio);
    }

    public void apresentarRelatorio(String nomeRelatorio, String xml, String tituloRelatorio, String nomeEmpresa, String mensagemRel, String tipoRelatorio, String parserBuscaTags, String designIReport, String nomeUsuario, String filtros, String parametro1, String parametro2, String parametro3) throws Exception {
        HttpServletRequest request = (HttpServletRequest) context().getExternalContext().getRequest();
        request.setAttribute("xmlRelatorio", xml);
        request.setAttribute("tipoRelatorio", tipoRelatorio);
        request.setAttribute("nomeRelatorio", nomeRelatorio);
        request.setAttribute("nomeEmpresa", nomeEmpresa);
        request.setAttribute("mensagemRel", mensagemRel);
        request.setAttribute("tituloRelatorio", tituloRelatorio);
        request.setAttribute("caminhoParserXML", parserBuscaTags);
        request.setAttribute("nomeDesignIReport", designIReport);
        request.setAttribute("nomeUsuario", nomeUsuario);
        request.setAttribute("parametro1", parametro1);
        request.setAttribute("parametro2", parametro2);
        request.setAttribute("parametro3", parametro3);

        if (filtros.equals("")) {
            filtros = "nenhum";
        }
        request.setAttribute("filtros", filtros);
        context().getExternalContext().dispatch("/VisualizadorRelatorio");
        FacesContext.getCurrentInstance().responseComplete();
    }

    public void realizarImpressaoRelatorio() throws Exception {
        try {
            caminhoRelatorio = geradorRelatorio.realizarExportacaoRelatorio(getSuperParametroRelVO());
//                request = (HttpServletRequest) context().getExternalContext().getRequest();
//                request.setAttribute("relatorio", caminhoRelatorio);
//                context().getExternalContext().dispatch("/DownloadRelatorioSV");
//                FacesContext.getCurrentInstance().responseComplete();
            setFazerDownload(true);
        } catch (Exception ex) {
            setFazerDownload(false);
            throw ex;
        } finally {
        }
    }

    public String getDownload() {
        if (getFazerDownload()) {
            try {
                if(UteisEmail.getURLAplicacao().endsWith("/SEI/") || UteisEmail.getURLAplicacao().endsWith("/SEI")
                        || UteisEmail.getURLAplicacao().endsWith("/SEI/faces")|| UteisEmail.getURLAplicacao().endsWith("/SEI/faces/")){
                    return "location.href='/SEI/DownloadRelatorioSV?relatorio=" + getCaminhoRelatorio() + "'";
                }
                return "location.href='../DownloadRelatorioSV?relatorio=" + getCaminhoRelatorio() + "'";
            } catch (Exception ex) {
                Logger.getLogger(SuperControleRelatorio.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                setFazerDownload(false);
            }
        }
        return "";
    }
    
    public String getDownloadMatriculaExterna() {
        if (getFazerDownload()) {
            try {
                return "location.href='../DownloadRelatorioSV?relatorio=" + getCaminhoRelatorio() + "'";
            } catch (Exception ex) {
                Logger.getLogger(SuperControleRelatorio.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                setFazerDownload(false);
            }
        }
        return "";
    }

    public Boolean getFazerDownload() {
        if (fazerDownload == null) {
            fazerDownload = false;
        }
        return fazerDownload;
    }

    public void setFazerDownload(Boolean fazerDownload) {
        this.fazerDownload = fazerDownload;
    }

    public void realizarImpressaoRelatorio(SuperParametroRelVO superParametroRe) throws Exception {
        try {
            caminhoRelatorio = geradorRelatorio.realizarExportacaoRelatorio(superParametroRe);
            setFazerDownload(true);
        } catch (Exception ex) {
            setFazerDownload(false);
            throw ex;
        } finally {
        }
//                
//            HttpServletRequest request = (HttpServletRequest) context().getExternalContext().getRequest();
//            request.setAttribute("relatorio", caminhoRelatorio);
//            context().getExternalContext().dispatch("/DownloadRelatorioSV");
//            FacesContext.getCurrentInstance().responseComplete();
//        } catch (Exception ex) {
//            Logger.getLogger(SuperControleRelatorio.class.getName()).log(Level.SEVERE, null, ex);
//        }
    }

    public List<SelectItem> getListaSelectItemOrdenacoesRelatorio() {
        return listaSelectItemOrdenacoesRelatorio;
    }

    public void setListaSelectItemOrdenacoesRelatorio(List<SelectItem> listaSelectItemOrdenacoesRelatorio) {
        this.listaSelectItemOrdenacoesRelatorio = listaSelectItemOrdenacoesRelatorio;
    }

    public Integer getOpcaoOrdenacao() {
        if(opcaoOrdenacao == null){
            opcaoOrdenacao = 0;
        }
        return opcaoOrdenacao;
    }

    public void setOpcaoOrdenacao(Integer opcaoOrdenacao) {
        this.opcaoOrdenacao = opcaoOrdenacao;
    }

    public class Inicio {
    }

    public SuperParametroRelVO getSuperParametroRelVO() {
        if (superParametroRelVO == null) {
            superParametroRelVO = new SuperParametroRelVO();
        }
        return superParametroRelVO;
    }

    public String getUsarTargetBlank() {
        if (usarTargetBlank == null) {
            usarTargetBlank = "_self";
        }
        return usarTargetBlank;
    }

    public void setUsarTargetBlank(String usarTargetBlank) {
        this.usarTargetBlank = usarTargetBlank;
    }
    
    public String getLogoPadraoRelatorio(){
		if (FacesContext.getCurrentInstance() != null) {
			LoginControle loginControle = (LoginControle) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("LoginControle");
//			if (loginControle != null && !loginControle.getUrlFisicoLogoUnidadeEnsinoRelatorio().trim().isEmpty()) {				
//				return loginControle.getUrlFisicoLogoUnidadeEnsinoRelatorio();
//			}else{
//				return getCaminhoPastaWeb() + File.separator + "imagens" + File.separator + "logoPadraoRelatorio.png";
//			}
			return getCaminhoPastaWeb() + File.separator + "imagens" + File.separator + "logoPadraoRelatorio.png";
		}else{
			return getCaminhoPastaWeb() + File.separator + "imagens" + File.separator + "logoPadraoRelatorio.png";
		}
	}
    
    public String getDownloadComprovanteRecebimentoMatriculaExterna() {
        if (getFazerDownload()) {
            try {
                return "location.href='../DownloadRelatorioSV?relatorio=" + getCaminhoRelatorio() + "'";
            } catch (Exception ex) {
                Logger.getLogger(SuperControleRelatorio.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                setFazerDownload(false);
            }
        }
        return "";
    }
    
    private Integer numeroCopias;
    private Integer coluna;
    private Integer linha;
    private List<SelectItem> listaSelectItemlayoutEtiqueta;
    private List<SelectItem> listaSelectItemColuna;
    private List<SelectItem> listaSelectItemLinha;
    private Boolean removerEspacoTAGVazia;
    
    public Boolean getRemoverEspacoTAGVazia() {
		if (removerEspacoTAGVazia == null) {
			removerEspacoTAGVazia = Boolean.FALSE;
		}
		return removerEspacoTAGVazia;
	}

	public void setRemoverEspacoTAGVazia(Boolean removerEspacoTAGVazia) {
		this.removerEspacoTAGVazia = removerEspacoTAGVazia;
	}
    

    public void setListaSelectItemlayoutEtiqueta(List<SelectItem> listaSelectItemlayoutEtiqueta) {
        this.listaSelectItemlayoutEtiqueta = listaSelectItemlayoutEtiqueta;
    }
    
    public List<SelectItem> getListaSelectItemColuna() {
        if (listaSelectItemColuna == null) {
            listaSelectItemColuna = new ArrayList<SelectItem>(0);
        }
        return listaSelectItemColuna;
    }

    public void setListaSelectItemColuna(List<SelectItem> listaSelectItemColuna) {
        this.listaSelectItemColuna = listaSelectItemColuna;
    }

    public List<SelectItem> getListaSelectItemLinha() {
        if (listaSelectItemLinha == null) {
            listaSelectItemLinha = new ArrayList<SelectItem>(0);
        }
        return listaSelectItemLinha;
    }

    public void setListaSelectItemLinha(List<SelectItem> listaSelectItemLinha) {
        this.listaSelectItemLinha = listaSelectItemLinha;
    }

    public Integer getNumeroCopias() {
        if (numeroCopias == null) {
            numeroCopias = 1;
        }
        return numeroCopias;
    }

    public void setNumeroCopias(Integer numeroCopias) {
        this.numeroCopias = numeroCopias;
    }

    public Integer getColuna() {
        if (coluna == null) {
            coluna = 1;
        }
        return coluna;
    }

    public void setColuna(Integer coluna) {
        this.coluna = coluna;
    }

    public Integer getLinha() {
        if (linha == null) {
            linha = 1;
        }
        return linha;
    }

    public void setLinha(Integer linha) {
        this.linha = linha;
    }
	 
}
