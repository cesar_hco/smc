﻿
CREATE OR REPLACE FUNCTION sem_acentos(character varying)
  RETURNS character varying AS
$BODY$


SELECT translate($1, 'áéíóúàèìòùãõâêîôôäëïöüçÁÉÍÓÚÀÈÌÒÙÃÕÂÊÎÔÛÄËÏÖÜÇ', 'aeiouaeiouaoaeiooaeioucAEIOUAEIOUAOAEIOOAEIOUC')


$BODY$
  LANGUAGE sql VOLATILE
  COST 100;
ALTER FUNCTION sem_acentos(character varying)
  OWNER TO postgres;



CREATE TABLE pessoa(
	codigo serial not null,
	nome varchar(250) not null,
	dataNascimento date,
	cpf varchar(20) not null,
	rg varchar(15),
	celular varchar(20),
	telefone1 varchar(20),
	telefone2 varchar(20),
	cep varchar(15),
	endereco varchar(255),
	setor varchar(100),
	cidade integer,
	primary key (codigo)
);

      
ALTER TABLE pessoa
  ADD CONSTRAINT fk_pessoa_cidade FOREIGN KEY (cidade)
      REFERENCES cidade (codigo) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;
	  
	  
alter table contaReceber add column contaCorrente integer;

ALTER TABLE contaReceber
  ADD CONSTRAINT fk_contaReceber_contaCorrente FOREIGN KEY (contacorrente)
      REFERENCES contaCorrente (codigo) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;

	  	  

create table cliente(
	codigo serial not null,
	pessoa integer not null,
	observacao text,
	limiteCredito numeric(20,2),
	percentualMaximoDesconto numeric(20),
	icms numeric(20,2),
	percentualLimiteCliente numeric(20),
	primary key(codigo)
);


ALTER TABLE public.cliente
  ADD CONSTRAINT fk_cliente_pessoa FOREIGN KEY (pessoa)
      REFERENCES public.pessoa (codigo) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;


 create table contaReceber(
	codigo serial not null,
	dataCadastro date not null,
	cliente integer not null,
	situacaoContaReceber varchar(15) not null,
	descricaoPagamento text,
	dataVencimento date not null,
	dataCompetencia date,
	valor numeric(20,2) not null,
	desconto numeric(20,2),
	acrescimo numeric(20,2),
	multa numeric(20,2),
	juro numeric(20,2),
	valorMulta numeric(20,2),
	valorJuro numeric(20,2),
	valorRecebido numeric(20,2),
	parcela varchar(5) not null,
	observacao text,
	usuario integer,
	primary key (codigo)
 );

 ALTER TABLE public.contaReceber
  ADD CONSTRAINT fk_contaReceber_cliente FOREIGN KEY (cliente)
      REFERENCES public.cliente (codigo) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;

	  

create table usuario (
	codigo serial not null,
	pessoa integer not null,
	username varchar(50),
	senha varchar(150),
	primary key (codigo)
);
insert into pessoa (codigo, nome, cpf) values (1, 'Administrador', '0');
insert into usuario (pessoa, username, senha) values (1, 'admin', encript('admin'));


CREATE OR REPLACE FUNCTION public.digest(
    text,
    text)
  RETURNS bytea AS
'$libdir/pgcrypto', 'pg_digest'
  LANGUAGE c IMMUTABLE STRICT
  COST 1;
ALTER FUNCTION public.digest(text, text)
  OWNER TO postgres;


CREATE OR REPLACE FUNCTION public.encript(text)
  RETURNS text AS
$BODY$
    SELECT encode(digest($1, 'sha256'), 'hex')
  $BODY$
  LANGUAGE sql IMMUTABLE STRICT
  COST 100;
ALTER FUNCTION public.encript(text)
  OWNER TO postgres;

  
create table empresa(
	codigo serial not null,
	descricao varchar(250) not null,
	nomeFantasia varchar (250),
	cep varchar(15),
	endereco varchar(250),
	setor varchar(200),
	complemento varchar(200), 
	cidade integer,
	inscricaoEstadual varchar(25),
	cnpj varchar(20),
	telefone1 varchar(20),
	telefone2 varchar(20),
	primary key(codigo)
);


ALTER TABLE public.empresa
  ADD CONSTRAINT fk_empresa_cidade FOREIGN KEY (cidade)
      REFERENCES public.cidade (codigo) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;


  
  create table contapagar(
	codigo serial not null,
	data date not null,
	fornecedor integer not null,
	situacao varchar(10) not null,
	dataVencimento date not null,
	dataCompetencia date not null,
	valor numeric(20,2),
	valorTotal numeric(20,2),
	desconto numeric(20,2),
	multa numeric(20,2),
	valorMulta numeric(20,2),
	juro numeric(20,2),
	valorJuro numeric(20,2),
	valorPago numeric(20,2),
	nrDocumento varchar(255),
	codigoBarra varchar(255),
	parcela varchar(15),
	descricao text,
	Primary Key(codigo)
);

ALTER TABLE public.contaPagar
  ADD CONSTRAINT fk_contaPagar_fornecedor FOREIGN KEY (fornecedor)
      REFERENCES public.fornecedor (codigo) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;


create table categoriaDespesa(
	codigo serial not null,
	descricao varchar(255),
	categoriaDespesaPrincipal integer,
	primary key(codigo)
);

ALTER TABLE public.categoriaDespesa
  ADD CONSTRAINT fk_categoriaDespesa_categoriaDespesaPrincipal FOREIGN KEY (categoriaDespesaPrincipal)
      REFERENCES public.categoriaDespesa (codigo) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;


alter table contapagar add column categoriaDespesa integer;

ALTER TABLE public.contaPagar
  ADD CONSTRAINT fk_contaPagar_categoriaDespesa FOREIGN KEY (categoriaDespesa)
      REFERENCES public.categoriaDespesa (codigo) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;

alter table contareceber add column servico text;      
alter table contareceber add column nrNotaFiscal integer;      
alter table contareceber add column nossoNumero varchar(50);  
alter table contareceber add column nrDocumento varchar(25);          
alter table contareceber add column codigoBarra varchar(255);      
alter table contareceber add column linhaDigitavel varchar(255);      



create table contaPagarPagamento(
	codigo serial not null,
	contaPagar integer not null,
	pagamento integer not null,
	primary key(codigo)
);


alter table contapagar alter column datacompetencia drop not null
      
create table formapagamento(
	codigo serial not null,
	nome varchar(255) not null,
	tipoFormapagamento varchar(255) not null,
	primary key (codigo)
);

create table formapagamento
(
	codigo serial not null,
	nome varchar(255) not null,
	tipoFormaPagamento varchar(20) not null,
	primary key(codigo)
);


create table empresa
(
	codigo serial not null,
	descricao varchar(255) not null,
	nomeFantasia varchar(255),
	cep varchar(15),
	endereco varchar(255),
	setor varchar(200),
	complemento varchar(255),
	cidade integer,
	inscricaoEstadual varchar(15),
	cnpj varchar(20),
	telefone1 varchar(20),
	telefone2 varchar(20),
	primary key (codigo)
);

alter table contacorrente add contaCaixa boolean default false;

create table cheque(
	codigo serial not null,
	fornecedor integer,
	empresa integer,
	contacorrentelocalizacaocheque integer,
	datacadastro date not null,
	datavencimento date,
	banco varchar(150) not null,
	agencia varchar(100) not null,
	contacorrente varchar(100),
	numero varchar(50),
	valor numeric(20,2) not null,
	localizacao varchar(50),
	codigoorigem integer,
	datacompensacao date,
	responsavelRegistroCompensacao integer,
	titularConta varchar(100),
	chequeProprio boolean,
	contacorrentechequeproprio integer,
	localizacaocheque integer,
	primary key (codigo)
);

create table pagamento(
	codigo serial not null,
	dataPagamento timestamp not null,
	fornecedor integer,
	contacaixa integer,
	responsavel integer,
	empresa integer,
	valorTotalPagar numeric(20,2),
	valorTotalJuro numeric(20,2),
	valorTotalMulta numeric(20,2),
	valorTotalDesconto numeric(20,2),
	valorTotalPago numeric(20,2),
	valorTotalContaPagar numeric(20,2),
	valorPendente numeric(20,2),
	troco numeric(20,2),
	primary key (codigo)
);


ALTER TABLE public.pagamento
  ADD CONSTRAINT fk_pagamento_fornecedor FOREIGN KEY (fornecedor)
      REFERENCES public.fornecedor (codigo) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;


ALTER TABLE public.pagamento
  ADD CONSTRAINT fk_pagamento_contacaixa FOREIGN KEY (contaCaixa)
      REFERENCES public.contaCorrente (codigo) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;      


ALTER TABLE public.pagamento
  ADD CONSTRAINT fk_pagamento_responsavel FOREIGN KEY (responsavel)
      REFERENCES public.usuario (codigo) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;        


ALTER TABLE public.pagamento
  ADD CONSTRAINT fk_pagamento_empresa FOREIGN KEY (empresa)
      REFERENCES public.empresa (codigo) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;  


create table formaPagamentopagamento(
	codigo serial not null,
	formapagamento integer not null,
	pagamento integer not null,
	contacorrente integer,
	valor numeric(20,2) not null,
	cheque integer,
	primary key (codigo)
);      


ALTER TABLE public.formaPagamentopagamento
  ADD CONSTRAINT fk_formaPagamentopagamento_formaPagamento FOREIGN KEY (formaPagamento)
      REFERENCES public.formaPagamento (codigo) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT; 


ALTER TABLE public.formaPagamentopagamento
  ADD CONSTRAINT fk_formaPagamentopagamento_pagamento FOREIGN KEY (pagamento)
      REFERENCES public.pagamento (codigo) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT; 


ALTER TABLE public.formaPagamentopagamento
  ADD CONSTRAINT fk_formaPagamentopagamento_contacorrente FOREIGN KEY (contacorrente)
      REFERENCES public.contacorrente (codigo) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;   

ALTER TABLE public.formaPagamentopagamento
  ADD CONSTRAINT fk_formaPagamentopagamento_cheque FOREIGN KEY (cheque)
      REFERENCES public.cheque (codigo) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;          

create table movimentacaocaixa(
	codigo serial not null,
	dataabertura timestamp not null,
	responsavelAbertura integer,
	empresa integer not null,
	contacaixa integer not null,
	saldoinicialcheque numeric(20,2),
	saldoinicialdinheiro numeric(20,2),
	saldofinalcheque numeric(20,2),
	saldofinaldinheiro numeric(20,2),
	datafechamento timestamp,
	responsavelFechamento integer,
	situacaocaixa varchar(20) not null,
	primary key(codigo)
);


ALTER TABLE public.movimentacaocaixa
  ADD CONSTRAINT fk_movimentacaocaixa_responsavelAbertura FOREIGN KEY (responsavelAbertura)
      REFERENCES public.usuario (codigo) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT; 


ALTER TABLE public.movimentacaocaixa
  ADD CONSTRAINT fk_movimentacaocaixa_empresa FOREIGN KEY (empresa)
      REFERENCES public.empresa (codigo) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;  


ALTER TABLE public.movimentacaocaixa
  ADD CONSTRAINT fk_movimentacaocaixa_contacaixa FOREIGN KEY (contacaixa)
      REFERENCES public.contacorrente (codigo) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;                  	  
	  
create table movimentacaocaixaitem(
	codigo serial not null,
	movimentacaocaixa integer not null,
	responsavel integer,
	fornecedor integer,
	datamovimentacaocaixa timestamp,
	valor numeric(20,2) not null,
	codigoOrigem integer,
	tipoMovimentacaocaixa varchar(20) not null,
	tipoSacado varchar(50),
	tipoOrigem varchar(30),
	cheque integer,
	contacorrente integer,
	primary key (codigo)
);


ALTER TABLE public.movimentacaocaixaitem
  ADD CONSTRAINT fk_movimentacaocaixaitem_movimentacaocaixa FOREIGN KEY (movimentacaocaixa)
      REFERENCES public.movimentacaocaixa (codigo) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT; 


ALTER TABLE public.movimentacaocaixaitem
  ADD CONSTRAINT fk_movimentacaocaixaitem_responsavel FOREIGN KEY (responsavel)
      REFERENCES public.usuario (codigo) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;      


ALTER TABLE public.movimentacaocaixaitem
  ADD CONSTRAINT fk_movimentacaocaixaitem_fornecedor FOREIGN KEY (fornecedor)
      REFERENCES public.fornecedor (codigo) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT; 


ALTER TABLE public.movimentacaocaixaitem
  ADD CONSTRAINT fk_movimentacaocaixaitem_cheque FOREIGN KEY (cheque)
      REFERENCES public.cheque (codigo) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;   


ALTER TABLE public.movimentacaocaixaitem
  ADD CONSTRAINT fk_movimentacaocaixaitem_contaCorrente FOREIGN KEY (contaCorrente)
      REFERENCES public.contaCorrente (codigo) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;                                                                     	  

alter table movimentacaocaixaitem add column formapagamento integer not null;

ALTER TABLE public.movimentacaocaixaitem
  ADD CONSTRAINT fk_movimentacaocaixaitem_formaPagamento FOREIGN KEY (formaPagamento)
      REFERENCES public.formaPagamento (codigo) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;   	  
	  
create table extratocontacorrente(
	codigo serial not null,
	data timestamp not null,
	valor numeric(20,2) not null,
	empresa integer,
	cheque integer,
	contacorrente integer,
	formapagamento integer,
	codigoorigem integer,
	origemExtratoContaCorrente varchar(20) not null,
	tipoMovimentacaoExtratoContaCorrente varchar(15) not null,
	nomeSacado varchar(255),
	codigoSacado integer,
	cedente varchar(30) not null,
	primary key (codigo)
);

ALTER TABLE public.extratocontacorrente
  ADD CONSTRAINT fk_extratocontacorrente_empresa FOREIGN KEY (empresa)
      REFERENCES public.empresa (codigo) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;  

ALTER TABLE public.extratocontacorrente
  ADD CONSTRAINT fk_extratocontacorrente_cheque FOREIGN KEY (cheque)
      REFERENCES public.cheque (codigo) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;   
      
ALTER TABLE public.extratocontacorrente
  ADD CONSTRAINT fk_extratocontacorrente_contaCorrente FOREIGN KEY (contaCorrente)
      REFERENCES public.contaCorrente (codigo) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;         
      
ALTER TABLE public.extratocontacorrente
  ADD CONSTRAINT fk_extratocontacorrente_formapagamento FOREIGN KEY (formapagamento)
      REFERENCES public.formapagamento (codigo) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;               
	  
alter table contacorrente add column empresa integer;
alter table cheque add column cliente integer;	  


alter table usuario add column tipousuario character varying(30);
alter table usuario add column exercecargoadministrativo boolean DEFAULT false;

  

CREATE TABLE public.perfilusuario
(
  codigo integer NOT NULL,
  nome character varying(250) NOT NULL,
  CONSTRAINT perfilusuario_pkey PRIMARY KEY (codigo)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.perfilusuario
  OWNER TO postgres;


CREATE TABLE public.permissaousuariomenu
(
  codigo integer NOT NULL,
  perfilusuario integer NOT NULL,
  nomemodulo character varying(50) NOT NULL,
  descricaomodulo character varying(50) NOT NULL,
  permissaomodulo boolean,
  nomemenu character varying(50),
  CONSTRAINT permissaousuariomenu_pkey PRIMARY KEY (codigo),
  CONSTRAINT fk_permissaousuariomenu_perfilusuario FOREIGN KEY (perfilusuario)
      REFERENCES public.perfilusuario (codigo) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.permissaousuariomenu
  OWNER TO postgres;


 
CREATE TABLE public.permissaousuariomenusubmodulo
(
  codigo integer NOT NULL,
  permissaousuariomenu integer NOT NULL,
  nomesubmodulo character varying(50) NOT NULL,
  descricaosubmodulo character varying(50) NOT NULL,
  permissaosubmodulo boolean,
  nomemenu character varying(50),
  CONSTRAINT permissaousuariomenusubmodulo_pkey PRIMARY KEY (codigo),
  CONSTRAINT fk_permissaousuariomenusubmodulo_permissaousuariomenu FOREIGN KEY (permissaousuariomenu)
      REFERENCES public.permissaousuariomenu (codigo) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.permissaousuariomenusubmodulo
  OWNER TO postgres;



CREATE TABLE public.usuarioempresaperfilusuario
(
  codigo integer NOT NULL,
  usuario integer NOT NULL,
  empresa integer NOT NULL,
  perfilusuario integer NOT NULL,
  CONSTRAINT usuarioempresaperfilusuario_pkey PRIMARY KEY (codigo),
  CONSTRAINT fk_usuarioempresaperfilusuario_perfilusuario FOREIGN KEY (perfilusuario)
      REFERENCES public.perfilusuario (codigo) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE NO ACTION,
  CONSTRAINT fk_usuarioempresaperfilusuario_empresa FOREIGN KEY (empresa)
      REFERENCES public.empresa (codigo) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE NO ACTION,
  CONSTRAINT fk_usuarioempresaperfilusuario_usuario FOREIGN KEY (usuario)
      REFERENCES public.usuario (codigo) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.usuarioempresaperfilusuario
  OWNER TO postgres;
 
 
 
 

create table funcionario(
	codigo serial not null,
	pessoa integer not null,
	datacadastro date not null,
	primary key (codigo)
);


ALTER TABLE public.funcionario
  ADD CONSTRAINT fk_funcionario_pessoa FOREIGN KEY (pessoa)
      REFERENCES public.pessoa (codigo) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;



ALTER TABLE public.usuario
  ADD CONSTRAINT fk_usuario_pessoa FOREIGN KEY (pessoa)
      REFERENCES public.pessoa (codigo) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;

alter table pagamento add column estornado boolean default false;	  

create table convenio(
	codigo serial not null,
	descricao varchar(255),
	ativo boolean ,
	empresa integer not null,
	primary key (codigo)
);


ALTER TABLE public.convenio
  ADD CONSTRAINT fk_convenio_empresa FOREIGN KEY (empresa)
      REFERENCES public.empresa (codigo) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;
      

create table paciente(
	codigo serial not null,
	pessoa integer not null,
	observacao varchar(255),
	numeroConvenio varchar(60),
	convenio integer ,
	primary key (codigo)
);

ALTER TABLE public.paciente
  ADD CONSTRAINT fk_paciente_pessoa FOREIGN KEY (pessoa)
      REFERENCES public.pessoa (codigo) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;
      
ALTER TABLE public.paciente
  ADD CONSTRAINT fk_paciente_convenio FOREIGN KEY (convenio)
      REFERENCES public.convenio (codigo) MATCH SIMPLE
      ON UPDATE RESTRICT ON DELETE RESTRICT;     