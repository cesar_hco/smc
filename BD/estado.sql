--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.1
-- Dumped by pg_dump version 9.5.1

-- Started on 2016-07-06 13:15:23

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 648 (class 1259 OID 985767)
-- Name: estado; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE estado (
    paiz integer NOT NULL,
    nome character varying(150) NOT NULL,
    sigla character varying(2) NOT NULL,
    codigo integer NOT NULL,
    codigoinep integer,
    regiao character varying(30),
    codigoibge character varying(30)
);


ALTER TABLE estado OWNER TO postgres;

--
-- TOC entry 649 (class 1259 OID 985770)
-- Name: estado_codigo_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE estado_codigo_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE estado_codigo_seq OWNER TO postgres;

--
-- TOC entry 5611 (class 0 OID 0)
-- Dependencies: 649
-- Name: estado_codigo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE estado_codigo_seq OWNED BY estado.codigo;


--
-- TOC entry 5480 (class 2604 OID 988795)
-- Name: codigo; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY estado ALTER COLUMN codigo SET DEFAULT nextval('estado_codigo_seq'::regclass);


--
-- TOC entry 5605 (class 0 OID 985767)
-- Dependencies: 648
-- Data for Name: estado; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO estado VALUES (2, 'New York', 'NY', 29, NULL, NULL, NULL);
INSERT INTO estado VALUES (11, 'ASSUNÇÃO', 'AS', 31, 0, NULL, NULL);
INSERT INTO estado VALUES (11, 'ASSUNÇÃO', 'AS', 32, 0, NULL, NULL);
INSERT INTO estado VALUES (10, 'SUDÃO', 'SU', 33, 0, NULL, NULL);
INSERT INTO estado VALUES (1, 'PARAÍBA', 'PB', 16, NULL, NULL, '25');
INSERT INTO estado VALUES (1, 'ACRE', 'AC', 1, NULL, 'NORTE', '12');
INSERT INTO estado VALUES (1, 'AMAPÁ', 'AP', 3, NULL, 'NORTE', '16');
INSERT INTO estado VALUES (1, 'AMAZONAS', 'AM', 4, NULL, 'NORTE', '13');
INSERT INTO estado VALUES (1, 'RORAIMA', 'RR', 9, NULL, 'NORTE', '14');
INSERT INTO estado VALUES (1, 'RONDÔNIA', 'RO', 23, NULL, 'NORTE', '11');
INSERT INTO estado VALUES (1, 'TOCANTINS', 'TO', 24, NULL, 'NORTE', '17');
INSERT INTO estado VALUES (1, 'ALAGOAS', 'AL', 2, NULL, 'NORDESTE', '27');
INSERT INTO estado VALUES (1, 'BAHIA', 'BA', 5, NULL, 'NORDESTE', '29');
INSERT INTO estado VALUES (1, 'CEARÁ', 'CE', 6, NULL, 'NORDESTE', '23');
INSERT INTO estado VALUES (1, 'MARANHÃO', 'MA', 11, NULL, 'NORDESTE', '21');
INSERT INTO estado VALUES (1, 'PERNAMBUCO', 'PE', 18, NULL, 'NORDESTE', '26');
INSERT INTO estado VALUES (1, 'PIAUÍ', 'PI', 19, NULL, 'NORDESTE', '22');
INSERT INTO estado VALUES (1, 'RIO GRANDE DO NORTE', 'RN', 21, NULL, 'NORDESTE', '24');
INSERT INTO estado VALUES (1, 'SERGIPE', 'SE', 27, NULL, 'NORDESTE', '28');
INSERT INTO estado VALUES (1, 'PARÁ', 'PA', 15, NULL, 'NORDESTE', '15');
INSERT INTO estado VALUES (1, 'MATO GROSSO', 'MT', 12, NULL, 'CENTRO_OESTE', '51');
INSERT INTO estado VALUES (1, 'MATO GROSSO DO SUL', 'MS', 13, NULL, 'CENTRO_OESTE', '50');
INSERT INTO estado VALUES (1, 'GOIÁS', 'GO', 10, 0, 'CENTRO_OESTE', '52');
INSERT INTO estado VALUES (1, 'ESPÍRITO SANTO', 'ES', 8, NULL, 'SUDESTE', '32');
INSERT INTO estado VALUES (1, 'MINAS GERAIS', 'MG', 14, NULL, 'SUDESTE', '31');
INSERT INTO estado VALUES (1, 'RIO DE JANEIRO', 'RJ', 20, NULL, 'SUDESTE', '33');
INSERT INTO estado VALUES (1, 'SÃO PAULO', 'SP', 26, NULL, 'SUDESTE', '35');
INSERT INTO estado VALUES (1, 'PARANÁ', 'PR', 17, NULL, 'SUL', '41');
INSERT INTO estado VALUES (1, 'RIO GRANDE DO SUL', 'RS', 22, NULL, 'SUL', '43');
INSERT INTO estado VALUES (1, 'SANTA CATARINA', 'SC', 25, NULL, 'SUL', '42');
INSERT INTO estado VALUES (1, 'DISTRITO FEDERAL', 'DF', 7, 53, 'CENTRO_OESTE', '53');


--
-- TOC entry 5612 (class 0 OID 0)
-- Dependencies: 649
-- Name: estado_codigo_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('estado_codigo_seq', 33, true);


--
-- TOC entry 5485 (class 2606 OID 996365)
-- Name: estado_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY estado
    ADD CONSTRAINT estado_pkey PRIMARY KEY (codigo);


--
-- TOC entry 5481 (class 1259 OID 997795)
-- Name: ch_estado_nome; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ch_estado_nome ON estado USING btree (nome);


--
-- TOC entry 5482 (class 1259 OID 997796)
-- Name: ch_estado_paiz; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ch_estado_paiz ON estado USING btree (paiz);


--
-- TOC entry 5483 (class 1259 OID 997797)
-- Name: ch_estado_sigla; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX ch_estado_sigla ON estado USING btree (sigla);


--
-- TOC entry 5486 (class 2606 OID 1001474)
-- Name: fk_estado_paiz; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY estado
    ADD CONSTRAINT fk_estado_paiz FOREIGN KEY (paiz) REFERENCES paiz(codigo) ON UPDATE RESTRICT ON DELETE RESTRICT;


-- Completed on 2016-07-06 13:15:23

--
-- PostgreSQL database dump complete
--

